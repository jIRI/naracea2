//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by nsUnzip.rc
//
#define IDD_ENCRYPT                     1001
#define IDD_DECRYPT                     1002
#define IDC_GROUP_LABEL                 1101
#define IDC_PASS1_LABEL                 1102
#define IDC_PASS1                       1103
#define IDC_PASS2_LABEL                 1104
#define IDC_PASS2                       1105
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        11001
#define _APS_NEXT_COMMAND_VALUE         12001
#define _APS_NEXT_CONTROL_VALUE         13001
#define _APS_NEXT_SYMED_VALUE           14001
#endif
#endif
