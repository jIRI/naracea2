﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Controller {
	public interface ICurrentVersion {
		Version Version { get; }
		bool IsVersionValid { get; }
	}
}
