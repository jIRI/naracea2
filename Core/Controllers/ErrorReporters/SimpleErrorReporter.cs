﻿using MemBus;
using Naracea.View;
using System;
using System.Diagnostics;

namespace Naracea.Controller.ErrorReporters {
	public class SimpleErrorReporter : IErrorReporter {
		IErrorOccuredDialog _dialog;
		IBus _bus;
		IDisposable _subscription;
		public SimpleErrorReporter(IBus bus, IErrorOccuredDialog dialog) {
			_bus = bus;
			_dialog = dialog;
			_subscription = _bus.Subscribe<Messages.Error>(m => this.ReportError(m.Message, m.Details));
		}

		public void ReportError(string shortMessage, string detail) {
			_dialog.ShortMessage = shortMessage;
			_dialog.DetailedMessage = detail;
			_dialog.ShowDialog();
		}

		#region IDisposable Members
		~SimpleErrorReporter() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
			Debug.Assert(false, this.GetType().FullName);
		}

		private bool _disposed = false;
		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if (!_disposed) {
				if (disposing) {
					if (_subscription != null) {
						_subscription.Dispose();
					}
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
