﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.Controller;
using System.Diagnostics;

namespace Naracea.View.Requests {
	#region Application requests
	public class Exit {
		public bool Cancel { get; set; }
		public Exit() {
			this.Cancel = false;
		}
	}

	public class NotifyUserFileOpsAreLocked {
	}
	#endregion

	#region Document requests
	public abstract class DocumentRequestBase {
		public IDocumentController DocumentController { get; private set; }
		public IDocumentView DocumentView { get; private set; }

		public DocumentRequestBase(IDocumentView documentView) {
			this.DocumentView = documentView;
			this.DocumentController = documentView.Controller as IDocumentController;
		}
	}

	public class OpenDocumentOrDocuments {
	}

	public class OpenDocument {
		public string FileName { get; private set; }
		public OpenDocument(string fileName) {
			this.FileName = fileName;
		}
	}

	public class CreateDocument {
	}

	public class SaveAllDocuments {
	}

	public class SaveDocument : DocumentRequestBase {
		public SaveDocument(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class SaveAsDocument : DocumentRequestBase {
		public SaveAsDocument(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class CloseDocument : DocumentRequestBase {
		public CloseDocument(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class ActivateDocument : DocumentRequestBase {
		public ActivateDocument(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class ShowDocumentProperties : DocumentRequestBase {
		public ShowDocumentProperties(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class CommitDocumentProperties : DocumentRequestBase {
		public IDocumentPropertiesView PropertiesView { get; private set; }
		public CommitDocumentProperties(IDocumentView documentView, IDocumentPropertiesView propertiesView)
			: base(documentView) {
			this.PropertiesView = propertiesView;
		}
	}

	public class PrintDocument : DocumentRequestBase {
		public PrintDocument(IDocumentView documentView)
			: base(documentView) {
		}
	}

	public class ShowHelp {
	}
	#endregion

	#region Branch requests
	public abstract class BranchRequestBase : DocumentRequestBase {
		public IBranchController BranchController { get; private set; }
		public IBranchView BranchView { get; private set; }

		public BranchRequestBase(IDocumentView documentView, IBranchView branchView)
			: base(documentView) {
			if( branchView != null ) {
				this.BranchView = branchView;
				this.BranchController = branchView.Controller as IBranchController;
			}
		}
	}

	public class ExportCurrentBranch : BranchRequestBase {
		public ExportFormatItem Format { get; private set; }
		public ExportCurrentBranch(IDocumentView documentView, IBranchView branchView, ExportFormatItem format)
			: base(documentView, branchView) {
			this.Format = format;
		}
	}

	public class CreateEmptyBranch : BranchRequestBase {
		public CreateEmptyBranch(IDocumentView documentView)
			: base(documentView, null) {
		}
	}

	public class CreateBranch : BranchRequestBase {
		public CreateBranch(IDocumentView documentView)
			: base(documentView, null) {
		}
	}

	public class ActivateBranch : BranchRequestBase {
		public ActivateBranch(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class ShowBranchProperties : BranchRequestBase {
		public ShowBranchProperties(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class DeleteBranch : BranchRequestBase {
		public DeleteBranch(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class ShowBranchTextViewer : BranchRequestBase {
		public ShowBranchTextViewer(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class OpenBrowser : BranchRequestBase {
		public OpenBrowser(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class CommitBranchProperties : BranchRequestBase {
		public IBranchPropertiesView PropertiesView { get; private set; }
		public CommitBranchProperties(IDocumentView documentView, IBranchView branchView, IBranchPropertiesView propertiesView)
			: base(documentView, branchView) {
			this.PropertiesView = propertiesView;
		}
	}

	public class ShowTimelineOnly : BranchRequestBase {
		public ShowTimelineOnly(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class ShowTimelineAndChangeStream : BranchRequestBase {
		public ShowTimelineAndChangeStream(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class ShowChangeStreamOnly : BranchRequestBase {
		public ShowChangeStreamOnly(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class SpellcheckSetLanguage : BranchRequestBase {
		public SpellcheckDictionaryItem Item { get; private set; }

		public SpellcheckSetLanguage(IDocumentView documentView, IBranchView branchView, SpellcheckDictionaryItem item)
			: base(documentView, branchView) {
			this.Item = item;
		}
	}

	public class CopyToClipboardAsHtml : BranchRequestBase {
		public CopyToClipboardAsHtml(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}

	public class RefreshBranchVisualOrder : BranchRequestBase {
		public RefreshBranchVisualOrder(IDocumentView documentView, IBranchView branchView)
			: base(documentView, branchView) {
		}
	}
	#endregion

	#region Rewind/Replay
	public abstract class RewindReplayRequestBase {
		public IBranchController BranchController { get; private set; }
		public IBranchView BranchView { get; private set; }

		public RewindReplayRequestBase(IBranchView branchView) {
			this.BranchView = branchView;
			this.BranchController = branchView.Controller as IBranchController;
		}
	}

	//public class GoToDateAndTime : RewindReplayRequestBase {
	//  public DateTime When { get; private set; }

	//  public GoToDateAndTime(IBranchView branchView, DateTime dateTime)
	//    : base(branchView) {
	//    this.When = dateTime;
	//  }
	//}

	public class TimelineChangeSelected {
		public ITimelineView TimelineView { get; private set; }
		public ITimelineController TimelineController { get; private set; }
		public int ChangeIndex { get; private set; }

		public TimelineChangeSelected(ITimelineView timelineView, int changeIndex) {
			this.TimelineView = timelineView;
			this.TimelineController = timelineView.Controller as ITimelineController;
			this.ChangeIndex = changeIndex;
		}
	}

	public class ChangeStreamChangeSelected {
		public IChangeStreamView ChangeStreamView { get; private set; }
		public IChangeStreamController ChangeStreamController { get; private set; }
		public int ChangeIndex { get; private set; }

		public ChangeStreamChangeSelected(IChangeStreamView changeStreamView, int changeIndex) {
			this.ChangeStreamView = changeStreamView;
			this.ChangeStreamController = changeStreamView.Controller as IChangeStreamController;
			this.ChangeIndex = changeIndex;
		}
	}



	public class RewindChange : RewindReplayRequestBase {
		public RewindChange(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class RewindWord : RewindReplayRequestBase {
		public RewindWord(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class RewindSentence : RewindReplayRequestBase {
		public RewindSentence(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class RewindParagraph : RewindReplayRequestBase {
		public RewindParagraph(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class RewindAll : RewindReplayRequestBase {
		public RewindAll(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class ReplayChange : RewindReplayRequestBase {
		public ReplayChange(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class ReplayWord : RewindReplayRequestBase {
		public ReplayWord(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class ReplaySentence : RewindReplayRequestBase {
		public ReplaySentence(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class ReplayParagraph : RewindReplayRequestBase {
		public ReplayParagraph(IBranchView branchView)
			: base(branchView) {
		}
	}

	public class ReplayAll : RewindReplayRequestBase {
		public ReplayAll(IBranchView branchView)
			: base(branchView) {
		}
	}
	#endregion

	#region TextEditor
	public abstract class TextEditorRequestMessageBase {
		public ITextEditorView TextEditorView { get; private set; }
		public ITextEditorController TextEditorController { get; private set; }

		public TextEditorRequestMessageBase(ITextEditorView textEditorView) {
			this.TextEditorView = textEditorView;
			this.TextEditorController = textEditorView.Controller as ITextEditorController;
		}
	}

	public abstract class TextChangedRequestBase : TextEditorRequestMessageBase {
		public int Position { get; private set; }
		public string Text { get; private set; }

		public TextChangedRequestBase(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView) {
			Debug.Assert(position >= 0);
			this.Position = position;
			Debug.Assert(text != null);
			this.Text = text;
		}
	}

	public class ToggleWrapLines : TextEditorRequestMessageBase {
		public ToggleWrapLines(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class ToggleShowWhitespaces : TextEditorRequestMessageBase {
		public ToggleShowWhitespaces(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class ToggleKeepTabs : TextEditorRequestMessageBase {
		public ToggleKeepTabs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class SetTabSize : TextEditorRequestMessageBase {
		public int Size { get; private set; }
		public SetTabSize(ITextEditorView textEditorView, int size)
			: base(textEditorView) {
			this.Size = size;
		}
	}

	public class SetSyntaxHighlighting : TextEditorRequestMessageBase {
		public string SyntaxHighlighter { get; private set; }
		public SetSyntaxHighlighting(ITextEditorView textEditorView, string highlighter)
			: base(textEditorView) {
			this.SyntaxHighlighter = highlighter;
		}
	}

	public class StoreGroupBegin : TextEditorRequestMessageBase {
		public StoreGroupBegin(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreGroupEnd : TextEditorRequestMessageBase {
		public StoreGroupEnd(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreTextInserted : TextChangedRequestBase {
		public StoreTextInserted(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreTextDeleted : TextChangedRequestBase {
		public StoreTextDeleted(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreTextBackspaced : TextChangedRequestBase {
		public StoreTextBackspaced(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreUndo : TextEditorRequestMessageBase {
		public StoreUndo(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreUndoWord : TextEditorRequestMessageBase {
		public StoreUndoWord(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreUndoSentence : TextEditorRequestMessageBase {
		public StoreUndoSentence(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreRedo : TextEditorRequestMessageBase {
		public StoreRedo(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class PasteCharsAsChanges : TextChangedRequestBase {
		public PasteCharsAsChanges(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class UpdateIndicators : TextEditorRequestMessageBase {
		public UpdateIndicators(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class SuggestSpelling : TextEditorRequestMessageBase {
		public int WordPosition { get; private set; }
		public int WordLength { get; private set; }
		public SuggestSpelling(ITextEditorView textEditorView, int position, int length)
			: base(textEditorView) {
			this.WordPosition = position;
			this.WordLength = length;
		}
	}

	public class AddToSpellingDictionary : TextEditorRequestMessageBase {
		public int WordPosition { get; private set; }
		public int WordLength { get; private set; }
		public AddToSpellingDictionary(ITextEditorView textEditorView, int position, int length)
			: base(textEditorView) {
			this.WordPosition = position;
			this.WordLength = length;
		}
	}

	public class SpellcheckRecheckAll : TextEditorRequestMessageBase {
		public SpellcheckRecheckAll(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}
	#endregion

	#region Find text
	public class ShowFindTextWindow {
		public string SelectedText { get; private set; }
		public ShowFindTextWindow(string selectedText) {
			this.SelectedText = selectedText;
		}

		public ShowFindTextWindow()
			: this("") {
		}
	}

	public class ViewReplaceOptions : ViewFindOptions {
		public string ReplacePattern { get; set; }
	}

	public abstract class FindBase {
		public ViewFindOptions Options { get; private set; }
		public FindBase(ViewFindOptions options) {
			this.Options = options;
		}
	}

	public abstract class ReplaceBase {
		public ViewReplaceOptions Options { get; private set; }
		public ReplaceBase(ViewReplaceOptions options) {
			this.Options = options;
		}
	}

	public class FindAll : FindBase {
		public FindAll(ViewFindOptions options)
			: base(options) {
		}
	}

	public class FindNext : FindBase {
		public FindNext(ViewFindOptions options)
			: base(options) {
		}
	}

	public class FindPrevious : FindBase {
		public FindPrevious(ViewFindOptions options)
			: base(options) {
		}
	}

	public class ReplaceAll : ReplaceBase {
		public ReplaceAll(ViewReplaceOptions options)
			: base(options) {
		}
	}

	public class ReplaceNext : ReplaceBase {
		public ReplaceNext(ViewReplaceOptions options)
			: base(options) {
		}
	}

	public class ReplaceItem : ReplaceBase {
		public ISearchMatchItem Item { get; private set; }

		public ReplaceItem(ISearchMatchItem item, ViewReplaceOptions options)
			: base(options) {
			this.Item = item;
		}
	}

	public class FindResultSelected {
		public ISearchMatchItem SelectedItem { get; private set; }

		public FindResultSelected(ISearchMatchItem selectedItem) {
			this.SelectedItem = selectedItem;
		}
	}

	public class ClearAllFindResults {
	}

	public class ClearCurrentBranchFindResults {
	}
	#endregion

	#region Spellchecker
	public class SpellcheckInstallLanguage {
	}
	#endregion
}

