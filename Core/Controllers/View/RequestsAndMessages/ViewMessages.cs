﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;
using Naracea.Common;
using Naracea.Controller;
using Naracea.View.Requests;

namespace Naracea.View.Messages {
	#region Timeline
	public class TimelineMessageBase {
		public ITimelineView TimelineView { get; private set; }
		public ITimelineController TimelineController { get; private set; }

		public TimelineMessageBase(ITimelineView view) {
			this.TimelineView = view;
			this.TimelineController = this.TimelineView.Controller as ITimelineController;
		}
	}

	public class TimelineSettingsChanged : TimelineMessageBase {
		public bool ShowEmptyBars { get; private set; }
		public IUnitOfTime Granularity { get; private set; }

		public TimelineSettingsChanged(ITimelineView view, IUnitOfTime granularity, bool showEmptyBars)
			: base(view) {
			this.ShowEmptyBars = showEmptyBars;
			this.Granularity = granularity;
		}
	}

	public class TimelineSizeChanged : TimelineMessageBase {
		public System.Windows.Size Size { get; private set; }
		public TimelineSizeChanged(ITimelineView view, System.Windows.Size size)
			: base(view) {
			this.Size = size;
		}
	}
	#endregion


	#region App messages
	public class ClosingAppView {
		public ClosingAppView() {
		}
	}
	#endregion


	#region Text editor messages
	public class TextEditorTextChanged : TextEditorRequestMessageBase {
		public int Position { get; private set; }
		public int AddedLength { get; private set; }
		public int RemovedLength { get; private set; }
		public TextEditorTextChanged(ITextEditorView textEditorView, int pos, int added, int removed)
			: base(textEditorView) {
			this.Position = pos;
			this.AddedLength = added;
			this.RemovedLength = removed;
		}
	}
	#endregion


	#region Branch messages
	public class BranchTextViewerClosed {
		public IBranchTextViewerView TextViewer { get; private set; }
		public BranchTextViewerClosed(IBranchTextViewerView textViewer) {
			this.TextViewer = textViewer;
		}
	}
	#endregion
}
