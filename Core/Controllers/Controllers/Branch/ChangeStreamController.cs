﻿using Naracea.Common;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.View;
using System.Collections.Generic;

namespace Naracea.Controller {
	public class ChangeStreamController : ControllerBase, IChangeStreamController {
		#region Attributes
		#endregion

		#region ctors
		public ChangeStreamController(IBranchController branch, IControllerContext context)
			: base(context) {

			this.IsEditing = false;

			this.BranchController = branch;

			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.BranchController.Settings, this.BranchController.Settings.Data);

			this.View = this.Context.View.CreateView<IChangeStreamView>();
			this.View.Controller = this;
			this.View.Open(this.Settings);
		}
		#endregion

		#region ITimelineController Members
		public IChangeStreamView View { get; private set; }
		public ISettings Settings { get; private set; }
		public bool IsEditing { get; private set; }

		int _currentChangeIndex = -1;
		public int CurrentChangeIndex {
			get {
				return _currentChangeIndex;
			}
			set {
				_currentChangeIndex = value;
				if (!this.IsEditing) {
					this.View.CurrentChangeIndex = _currentChangeIndex;
				}
			}
		}

		public void Close() {
			this.BranchController.Branch.CurrentChangeChanged -= this.BranchController_CurrentChangeChanged;
			this.BranchController.Branch.Changes.ChangeAdded -= this.Changes_ChangeAdded;
			this.Dispose();
			if (this.View != null) {
				this.View.Close();
				this.View = null;
			}
			this.Bus.Publish(new Messages.ChangeStreamClosed(this));
		}

		public void BeginEdit() {
			this.IsEditing = true;
			this.View.BeginUpdate();
		}

		public void EndEdit() {
			//update view
			this.IsEditing = false;
			this.View.EndUpdate();
			if (this.View.CurrentChangeIndex != _currentChangeIndex) {
				this.View.CurrentChangeIndex = _currentChangeIndex;
			}
		}

		public void Initialize() {
			// this needs to refactored, if takes to long to all all these execs on ui thread
			var items = new List<ChangeStreamItem>(this.BranchController.Branch.Changes.Count);
			for (int i = 0; i < this.BranchController.Branch.Changes.Count; i++) {
				var item = this.CreateViewItem(this.BranchController.Branch.Changes.At(i));
				if (item != null) {
					items.Add(item);
				}
			}
			this.View.BeginUpdate();
			this.View.AddChanges(items);
			this.View.EndUpdate();
			//get view updated after end of update
			this.CurrentChangeIndex = this.BranchController.Branch.CurrentChangeIndex;
			this.View.Refresh();

			this.InstallHandlers();
		}
		#endregion

		#region Private methods
		protected IBranchController BranchController { get; set; }

		void InstallHandlers() {
			this.BranchController.Branch.CurrentChangeChanged += this.BranchController_CurrentChangeChanged;
			this.BranchController.Branch.Changes.ChangeAdded += this.Changes_ChangeAdded;
		}

		void BranchController_CurrentChangeChanged(object sender, CurrentChangeChangedEventArgs e) {
			this.CurrentChangeIndex = e.ChangeIndex;
		}

		void Changes_ChangeAdded(object sender, ChangeAddedEventArgs e) {
			this.AddChange(e.Change);
		}

		private void AddChange(IChange change) {
			ChangeStreamItem item = this.CreateViewItem(change);
			if (item != null) {
				this.View.AddChange(item);
			}
			//add properties to change stream item so it can mark undone/redone item
			//add also support for groups
		}

		private ChangeStreamItem CreateViewItem(IChange change) {
			ChangeStreamItem item = null;
			if (change is IChangeWithText) {
				var textChange = change as IChangeWithText;
				if (textChange.AddedTextLength > 0) {
					item = new ChangeStreamItem(ChangeStreamItemType.Insert, change.Id, textChange.VisualText);
				} else {
					item = new ChangeStreamItem(ChangeStreamItemType.Delete, change.Id, textChange.VisualText);
				}
			} else if (change is IChangeWithChange) {
				var coupledChange = change as IChangeWithChange;
				item = new ChangeStreamItem(ChangeStreamItemType.WithChange, change.Id, coupledChange.VisualText);
			} else if (change is IChangeWithChangeCollection) {
				var changeWithCollection = change as IChangeWithChangeCollection;
				item = new ChangeStreamItem(ChangeStreamItemType.WithChangeList, change.Id, changeWithCollection.VisualText);
			} else {
				item = new ChangeStreamItem(ChangeStreamItemType.Other, change.Id, change.VisualText);
			}
			return item;
		}
		#endregion
	}
}
