﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Shifter;
using Naracea.Core.Spellcheck;
using Naracea.Plugin.Export;
using Naracea.View;
using Naracea.View.Components;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Naracea.Controller {
	public class BranchController : ControllerBase, IBranchController {
		#region Attributes
		Task _shiftTask = null;
		IUndoRedoFinder _undoRedoFinder;
		bool _useEditingMode = false;
		IBranchTextViewerView _textViewer;
		int _viewUpdateSuppressionCount = 0;
		object _shiftingPadlock = new object();
		Stack<Tuple<IChange, int>> _beginGroupStack = new Stack<Tuple<IChange, int>>();
		DictionaryDescriptor _spellcheckDictionary;
		string _temporaryFileForBrowseView = null;
		bool _pendingOpenBrowser = false;
		#endregion

		#region ctors
		/// <summary>
		/// ctor.
		/// Creates also view and text editor bridge.
		/// </summary>
		/// <param name="documentController"></param>
		/// <param name="branch"></param>
		/// <param name="context"></param>
		public BranchController(IDocumentController documentController, IBranch branch, IControllerContext context)
			: base(context) {
			Debug.Assert(documentController != null);
			this.DocumentController = documentController;

			//misc config
			this.EditModeThreshold = 1024;

			//set branch
			Debug.Assert(branch != null);
			this.Branch = branch;

			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.DocumentController.Settings, this.Branch.Metadata.Data);

			this.View = this.Context.View.CreateView<IBranchView>();
			this.View.Controller = this;
			this.View.Name = branch.Name;
			//create editor
			this.Editor = this.Context.Controller.CreateTextEditorController(this.DocumentController, this, this.Context);
			this.View.Editor = this.Editor.View;
			//create timeline
			this.Timeline = this.Context.Controller.CreateTimelineController(this, this.Context);
			this.View.Timeline = this.Timeline.View;
			//create changestream
			this.ChangeStream = this.Context.Controller.CreateChangeStreamController(this, this.Context);
			this.View.ChangeStream = this.ChangeStream.View;

			//open brach view
			//must be done here, because we need all parts to be initialized
			this.View.Open(this.Settings);

			//create finders
			_undoRedoFinder = this.Context.Core.CreateUndoRedoFinder();

			//New branch is not dirty by default. Only when something happens on it, it gets dirty.
			this.MarkAsNotDirty();

			this.InstallBranchEventHandlers();

			//update indicators
			this.UpdateIndicators();
			//restore timeline height
			this.RestoreTimelineHeight();
			//restore timeline/change stream settings
			this.RestoreTimelineAndChangeStream();
			//restore spellchecker settings
			this.RestoreSpellchecker();
			//restore exporter
			this.RestoreExporter();
		}
		#endregion

		#region IBranchController Members
		public string Name {
			get {
				return this.Branch.Name;
			}
			set {
				this.CheckDisposeState();
				this.Branch.Name = value;
				this.View.Name = this.Branch.Name;
				this.DocumentController.View.UpdateBranch(this.View);
			}
		}

		public string Comment {
			get {
				return this.Branch.Comment;
			}
			set {
				this.CheckDisposeState();
				this.Branch.Comment = value;
			}
		}

		public string ExportFileName {
			get {
				this.CheckDisposeState();
				string filename = null;
				if (this.Settings.HasValue(Naracea.Common.Settings.BranchAutoExportFilename)) {
					filename = this.Settings.Get<string>(Naracea.Common.Settings.BranchAutoExportFilename);
				} else {
					string extension = this.Exporter.Extension;
					if (this.Settings.HasValue(Naracea.Common.Settings.BranchAutoExportExtension)) {
						extension = this.Settings.Get<string>(Naracea.Common.Settings.BranchAutoExportExtension);
					}
					if (this.DocumentController.HasRepositoryContext) {
						filename = this.Context.FileNameValidator.GetValidFileName(Path.GetFileNameWithoutExtension(this.DocumentController.RepositoryPath) + " - " + this.Name) + extension;
					} else {
						filename = this.Context.FileNameValidator.GetValidFileName(this.DocumentController.Name + " - " + this.Name) + extension;
					}
				}
				return filename;
			}
		}
		public IBranchView View { get; private set; }
		public ITextEditorController Editor { get; private set; }
		public ITimelineController Timeline { get; private set; }
		public IChangeStreamController ChangeStream { get; private set; }
		public IBranch Branch { get; private set; }
		public IExportPlugin Exporter { get; private set; }
		public bool IsAlwaysExporting { get; set; }
		public bool IsDirty { get; private set; }
		public bool IsActivated { get; private set; }
		public ISettings Settings { get; private set; }
		public int EditModeThreshold { get; set; }
		public bool IsDocumentActive {
			get {
				return this.DocumentController.IsActive;
			}
		}

		public bool IsEditing {
			get {
				return _viewUpdateSuppressionCount > 0;
			}
		}

		public bool IsRewound {
			get {
				this.CheckDisposeState();
				return this.Branch.CurrentChangeIndex != this.Branch.Changes.Count - 1;
			}
		}

		public bool CanBeDeleted {
			get {
				return false;
			}
		}

		public bool IsTextChangeReportingEnabled {
			get {
				return this.Editor.IsTextChangeReportingEnabled;
			}
			set {
				this.Editor.IsTextChangeReportingEnabled = value;
			}
		}

		public int PreferredVisualOrder {
			get {
				if (this.Settings.HasValue(Naracea.Common.Settings.BranchVisualOrder)) {
					return this.Settings.Get<int>(Naracea.Common.Settings.BranchVisualOrder);
				} else {
					return -1;
				}
			}
		}


		/// <summary>
		/// Closes branch.
		/// For now it only unregisters event handlers.
		/// </summary>
		public void Close() {
			if (this.IsDisposed) {
				return;
			}

			// closing, document cannot become dirty from now
			if (this.Settings != null) {
				this.Settings.SettingsChanged -= this.Settings_SettingsChanged;
			}

			lock (_shiftingPadlock) {
				if (_shiftTask != null && !_shiftTask.IsCompleted) {
					_shiftTask.Wait();
				}
			}

			// there is temporary file, get rid of it
			if (_temporaryFileForBrowseView != null) {
				var fileChecker = this.Context.Core.CreateFileChecker();
				var fileRemover = this.Context.Core.CreateFileRemover();
				try {
					if (fileChecker.Exists(_temporaryFileForBrowseView)) {
						fileRemover.Remove(_temporaryFileForBrowseView);
					}
				} catch (Exception ex) {
					Debug.WriteLine("Unable to remove branch temp file: {0}", ex.Message);
					// do nothing, just log error
				}
			}

			this.Editor.Close();
			this.ChangeStream.Close();
			this.Timeline.Close();
			if (this.View != null) {
				this.View.Close();
				this.View = null;
			}
			this.DocumentController.UnregisterChild(this);
			this.Dispose();
			this.Bus.Publish(new Messages.BranchClosed(this.DocumentController, this));
		}

		/// <summary>
		/// Stores begin group change
		/// </summary>
		public void StoreGroupBegin() {
			this.CheckDisposeState();
			// we group all replace changes, so store group begin
			var groupBegin = this.Context.Core.CreateGroupBegin(this.DocumentController.AuthorController.CurrentAuthor, this.Context.DateTimeProvider.Now);
			this.Store(groupBegin);
			// store begin group and the current change index on the stack
			_beginGroupStack.Push(new Tuple<IChange, int>(groupBegin, this.Branch.CurrentChangeIndex));
		}

		/// <summary>
		/// Stores end group change and updates matching begin group
		/// </summary>
		public void StoreGroupEnd() {
			this.CheckDisposeState();

			var storedGroupBeginInfo = _beginGroupStack.Pop();
			int currentChangeIndex = this.Branch.CurrentChangeIndex;
			var groupedChanges = new List<IChange>();
			for (int i = storedGroupBeginInfo.Item2 + 1; i <= currentChangeIndex; i++) {
				groupedChanges.Add(this.Branch.Changes.At(i));
			}
			// there are some changes, store group end
			var groupEnd = this.Context.Core.CreateGroupEnd(groupedChanges, this.DocumentController.AuthorController.CurrentAuthor, this.Context.DateTimeProvider.Now);
			this.Store(groupEnd);
			// and update the group begin so it knows how long the group is
			(storedGroupBeginInfo.Item1 as ICanSetShiftCount).SetShiftCount((currentChangeIndex - storedGroupBeginInfo.Item2) + 1);
		}

		/// <summary>
		/// Add change to the branch.
		/// </summary>-
		/// <param name="change"></param>
		public void Store(Naracea.Core.Model.Changes.IChange change) {
			this.CheckDisposeState();

			lock (this.Branch) {
				//in this case we need to undo everything to the last change
				this.StoreMultiUndoIfNeccessary(this.Branch.Changes.Count - 1);
				this.Branch.Changes.Add(change);
				this.Branch.CurrentChangeIndex = this.Branch.Changes.Count - 1;
			}
			this.MarkAsDirty();
			if (!this.IsEditing) {
				this.UpdateViewFlags();
			}
		}

		public Core.Model.Changes.IChange FindUndoableChange() {
			this.CheckDisposeState();

			if (this.IsRewound) {
				// undo doesn't work when document is rewound
				// the change to be undone is being found before the multuiundo is stored, and it would need some trickery here to prevent problems.
				// in future, if it is needed to undo rewinded branch, we need to store multi undo here before the search of next undoable change.
				return null;
			}
			return _undoRedoFinder.FindNextUndoableChange(this.Branch.Changes);
		}

		public Core.Model.Changes.IChange FindRedoableChange() {
			this.CheckDisposeState();

			if (this.IsRewound) {
				//redo doesn't work when document is rewinded
				return null;
			}
			return _undoRedoFinder.FindNextRedoableChange(this.Branch.Changes);
		}

		public void UndoWordIfPossible() {
			this.CheckDisposeState();

			this.DoSpecialUndo(this.Context.Core.CreateWordFinder());
		}

		public void UndoSentenceIfPossible() {
			this.CheckDisposeState();

			this.DoSpecialUndo(this.Context.Core.CreateSentenceFinder());
		}

		public void BeginEdit() {
			this.CheckDisposeState();

			_viewUpdateSuppressionCount++;
			this.Editor.BeginEdit();
			this.Timeline.BeginEdit();
			this.ChangeStream.BeginEdit();
			if (_viewUpdateSuppressionCount == 1) {
				// this is first change from noediting to editing state
				//update flags.
				this.UpdateViewFlags();
			}
		}

		public void EndEdit() {
			this.CheckDisposeState();

			_viewUpdateSuppressionCount--;
			if (_viewUpdateSuppressionCount < 0) {
				_viewUpdateSuppressionCount = 0;
			}
			if (!this.IsEditing) {
				this.Editor.EndEdit();
				this.ChangeStream.EndEdit();
				this.Timeline.EndEdit();
				this.UpdateViewFlags();
				this.UpdateIndicators();
				this.View.Timeline.EndBusy();
			}
		}

		public void StoreBranchVisualOrder() {
			this.Settings.Set(Naracea.Common.Settings.BranchVisualOrder, this.DocumentController.View.GetBranchVisualOrder(this.View));
		}

		/// <summary>
		/// Executes shifter with progress
		/// </summary>
		/// <param name="shifter"></param>
		public void ExecuteShifter(IShifter shifter) {
			if (_useEditingMode) {
				this.ExecuteShifterAsync(shifter);
			} else {
				this.ShiftingDo(shifter);
				this.UpdateViewFlags();
				this.UpdateIndicators();
				this.Bus.Publish(new Messages.BranchShiftingFinished(this.DocumentController, this));
			}
		}
		#endregion

		#region Private methods
		protected IDocumentController DocumentController { get; set; }

		/// <summary>
		/// Install event handlers
		/// </summary>
		void InstallBranchEventHandlers() {
			//document messages
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaved>(
				m => this.MarkAsNotDirty(),
				new ShapeToFilter<Messages.DocumentSaved>(msg => msg.DocumentController == this.DocumentController)
			));

			//banch activation
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchActivated>(
				m => this.OnBranchActivated(),
				new ShapeToFilter<Messages.BranchActivated>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDeactivated>(
				m => this.OnBranchDeactivated(),
				new ShapeToFilter<Messages.BranchDeactivated>(msg => msg.BranchController == this)
			));

			//shifters
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.RewindChange>(
				m => this.RewindChange(),
				new ShapeToFilter<Naracea.View.Requests.RewindChange>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.RewindWord>(
				m => this.RewindWord(),
				new ShapeToFilter<Naracea.View.Requests.RewindWord>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.RewindSentence>(
				m => this.RewindSentence(),
				new ShapeToFilter<Naracea.View.Requests.RewindSentence>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.RewindParagraph>(
				m => this.RewindParagraph(),
				new ShapeToFilter<Naracea.View.Requests.RewindParagraph>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.RewindAll>(
				m => this.RewindAll(),
				new ShapeToFilter<Naracea.View.Requests.RewindAll>(msg => msg.BranchController == this)
			));

			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ReplayChange>(
				m => this.ReplayChange(),
				new ShapeToFilter<Naracea.View.Requests.ReplayChange>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ReplayWord>(
				m => this.ReplayWord(),
				new ShapeToFilter<Naracea.View.Requests.ReplayWord>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ReplaySentence>(
				m => this.ReplaySentence(),
				new ShapeToFilter<Naracea.View.Requests.ReplaySentence>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ReplayParagraph>(
				m => this.ReplayParagraph(),
				new ShapeToFilter<Naracea.View.Requests.ReplayParagraph>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ReplayAll>(
				m => this.ReplayAll(),
				new ShapeToFilter<Naracea.View.Requests.ReplayAll>(msg => msg.BranchController == this)
			));

			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.TimelineChangeSelected>(
				m => this.ShiftToChange(m.ChangeIndex),
				new ShapeToFilter<Naracea.View.Requests.TimelineChangeSelected>(msg => msg.TimelineController == this.Timeline)
			));

			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ChangeStreamChangeSelected>(
				m => this.ShiftToChange(m.ChangeIndex),
				new ShapeToFilter<Naracea.View.Requests.ChangeStreamChangeSelected>(msg => msg.ChangeStreamController == this.ChangeStream)
			));

			this.RegisterForDisposal(this.Context.Bus.Subscribe<Requests.ShiftToChange>(
				m => this.ShiftToChange(m.ChangeIndex),
				new ShapeToFilter<Requests.ShiftToChange>(msg => msg.BranchController == this)
			));

			//branch properties
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ShowBranchProperties>(
				m => this.ShowBranchProperties(),
				new ShapeToFilter<Naracea.View.Requests.ShowBranchProperties>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.CommitBranchProperties>(
				m => this.CommitBranchProperties(m.PropertiesView),
				new ShapeToFilter<Naracea.View.Requests.CommitBranchProperties>(msg => msg.BranchController == this)
			));

			//branch text viewer
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ShowBranchTextViewer>(
				m => this.ShowBranchTextViewer(),
				new ShapeToFilter<Naracea.View.Requests.ShowBranchTextViewer>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Messages.BranchTextViewerClosed>(
				m => _textViewer = null,
				new ShapeToFilter<Naracea.View.Messages.BranchTextViewerClosed>(msg => msg.TextViewer == _textViewer)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.OpenBrowser>(
				m => this.OpenBrowser(),
				new ShapeToFilter<Naracea.View.Requests.OpenBrowser>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchExported>(
				m => this.OpenBrowser(),
				new ShapeToFilter<Messages.BranchExported>(msg => msg.BranchController == this && _pendingOpenBrowser)
			));
			//refresh export for browser when document is saved
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaved>(
				m => {
					if (this.Context.Exporters.ContainsKey("HTML")) {
						this.Bus.Publish(new Requests.ExportBranchWithName(
							this.DocumentController, this, this.Context.Exporters["HTML"], _temporaryFileForBrowseView
						));
					}
				},
				new ShapeToFilter<Messages.DocumentSaved>(msg => msg.DocumentController == this.DocumentController && _temporaryFileForBrowseView != null)
			));

			//timeline properties
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Messages.TimelineSizeChanged>(
				m => this.StoreTimelineHeight(m.Size.Height),
				new ShapeToFilter<Naracea.View.Messages.TimelineSizeChanged>(msg => msg.TimelineController == this.Timeline)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ShowTimelineOnly>(
				m => {
					this.View.IsTimelineVisibleOnly = true;
					this.StoreTimelineAndChangeStreamVisibility();
				},
				new ShapeToFilter<Naracea.View.Requests.ShowTimelineOnly>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ShowTimelineAndChangeStream>(
				m => {
					this.View.IsTimelineAndChangeStreamVisible = true;
					this.StoreTimelineAndChangeStreamVisibility();
				},

				new ShapeToFilter<Naracea.View.Requests.ShowTimelineAndChangeStream>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ShowChangeStreamOnly>(
				m => {
					this.View.IsChangeStreamVisibleOnly = true;
					this.StoreTimelineAndChangeStreamVisibility();
				},
				new ShapeToFilter<Naracea.View.Requests.ShowChangeStreamOnly>(msg => msg.BranchController == this)
			));
			//spellcheck
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SpellcheckSetLanguage>(
				m => this.SetSpellcheckerLanguage(m.Item.Data as DictionaryDescriptor),
				new ShapeToFilter<Naracea.View.Requests.SpellcheckSetLanguage>(msg => msg.BranchController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Requests.SetSpellingLanguage>(
				m => this.SetSpellcheckerLanguage(m.Language),
				new ShapeToFilter<Requests.SetSpellingLanguage>(msg => msg.BranchController == this)
			));
			// clipboard
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.CopyToClipboardAsHtml>(
				m => this.PutTextToClipboardAsHtml(),
				new ShapeToFilter<Naracea.View.Requests.CopyToClipboardAsHtml>(msg => msg.BranchController == this)
			));

			// branch visual order
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.RefreshBranchVisualOrder>(
					m => this.StoreBranchVisualOrder(),
					new ShapeToFilter<Naracea.View.Requests.RefreshBranchVisualOrder>(msg => this.DocumentController == msg.DocumentController)
			));
		}

		private void SetSpellcheckerLanguage(DictionaryDescriptor language) {
			_spellcheckDictionary = language;
			if (_spellcheckDictionary != null) {
				this.View.Editor.SpellcheckLanguageName = _spellcheckDictionary.DictionaryName;
			} else {
				this.View.Editor.SpellcheckLanguageName = null;
			}
			this.StoreSpellcheckLanguage();
		}

		object _activationLock = new object();
		/// <summary>
		/// Called when branch is activated in parent document controller
		/// Still need to check whether this instance is the chosen one from event args...
		/// 
		/// What we do here is mainly:
		///		- unwinding the branch on first activation (everything gets unwinded unless we find it is better to unwind just to last active change...)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnBranchActivated() {
			lock (_activationLock) {
				if (!this.IsActivated) {
					this.View.Open(this.Settings);
					this.Branch.ResetState();
					this.ReplayAll();
					this.Timeline.Initialize();
					this.ChangeStream.Initialize();
					this.IsActivated = true;
					// all setup, now we are ready to subscribe for settings changes
					if (this.Settings != null) {
						this.Settings.SettingsChanged += this.Settings_SettingsChanged;
					}
				}
			}
		}

		/// <summary>
		/// Called when branch is deactivated in parent document handler.
		/// Still need to check whether this instance is the chosen one from event args...
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void OnBranchDeactivated() {
		}

		void ShiftToChange(int changeIndex) {
			if (changeIndex == this.Branch.CurrentChangeIndex) {
				return;
			}

			this.SetEditingModeFromChangeDistance(changeIndex);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeIndex));
		}

		void SetEditingModeFromChangeDistance(int changeIndex) {
			_useEditingMode = Math.Abs(changeIndex - this.Branch.CurrentChangeIndex) > this.EditModeThreshold;
		}

		void UpdateIndicators() {
			try {
				this.Bus.Publish(new Messages.IndicatorsUpdateNeeded(this.DocumentController, this));
			} catch (Exception ex) {
				Debug.WriteLine("UpdateIndicators exception: " + ex.Message);
			}
		}

		/// <summary>
		/// Marks branch as dirty.
		/// </summary>
		void MarkAsDirty() {
			this.IsDirty = true;
			this.UpdateViewDirtyStatus();
			this.Bus.Publish(new Messages.BranchDirty(this.DocumentController, this));
		}

		/// <summary>
		/// Marks branch as not dirty
		/// </summary>
		void MarkAsNotDirty() {
			this.IsDirty = false;
			this.UpdateViewDirtyStatus();
		}

		void UpdateViewDirtyStatus() {
			if (this.View != null) {
				this.View.DirtyStatusChanged(this.IsDirty);
			}
		}

		/// <summary>
		/// Starts shifting whenever needed.
		/// The actual shifter is received from worker's RunAsync() method.
		/// </summary>
		/// <param name="shifter"></param>
		void ShiftingDo(IShifter shifter) {
			Debug.Assert(shifter != null);
			if (_useEditingMode) {
				this.BeginEdit();
			}
			this.Editor.BeginUpdate();
			shifter.Shift(this.Branch, this.Editor.CoreBridge);
			this.Editor.EndUpdate();
			if (_useEditingMode) {
				this.EndEdit();
			}
			_useEditingMode = false;
		}

		/// <summary>
		/// Called when shifting completes
		/// </summary>
		void ShiftingCompleted() {
		}

		private void ExecuteShifterAsync(IShifter shifter) {
			if (Monitor.TryEnter(_shiftingPadlock)) {
				if (_shiftTask != null) {
					return;
				}
				Monitor.Exit(_shiftingPadlock);
			} else {
				return;
			}

			if (!this.IsEditing) {
				this.Editor.View.CanEdit = false;
			}
			//create progressbar
			var progressIndicator = this.Context.View.CreateView<IProgressIndicator>();
			progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.RewindingReplaying;
			progressIndicator.DetailedMessage = string.Format("{0} / {1}", this.DocumentController.Name, this.Name);
			progressIndicator.IsIndeterminate = true;
			progressIndicator.Open(this.Context.View.ApplicationView.ProgressIndicators);
			this.View.Timeline.BeginBusy();
			//this.View.ChangeStream.BeginBusy();
			lock (_shiftingPadlock) {
				_shiftTask = new Task(() => {
					//do actual shifting
					lock (_shiftingPadlock) {
						this.ShiftingDo(shifter);
						_shiftTask = null;

						if (!this.IsEditing) {
							this.UpdateViewFlags();
							this.UpdateIndicators();
							//this.View.ChangeStream.EndBusy();
							this.View.Timeline.EndBusy();
							//close progressbar
							progressIndicator.Close();
						}
					}
					this.Bus.Publish(new Messages.BranchShiftingFinished(this.DocumentController, this));
				});
			}
			this.UpdateViewFlags();
			lock (_shiftingPadlock) {
				if (_shiftTask != null) {
					_shiftTask.Start();
				}
			}
		}

		/// <summary>
		/// Updates view based on current state
		/// </summary>
		void UpdateViewFlags() {
			//buffer task because it can change asyncly
			var task = _shiftTask;

			var branchOnLastChange = this.Branch.CurrentChangeIndex == this.Branch.Changes.Count - 1;
			if (this.Editor.View != null) {
				this.Editor.View.CanEdit = !this.IsEditing && task == null;
				this.Editor.View.CanUndo = this.Editor.View.CanEdit && this.FindUndoableChange() != null;
				this.Editor.View.CanRedo = this.Editor.View.CanEdit && this.FindRedoableChange() != null;
			}
			var hasChanges = this.Branch.Changes.Count > 0;
			if (this.View != null) {
				this.View.CanRewind = this.Editor.View.CanEdit && task == null && hasChanges && this.Branch.CurrentChangeIndex >= 0;
				this.View.CanReplay = this.Editor.View.CanEdit && task == null && hasChanges && !branchOnLastChange;
			}
		}

		void StoreMultiUndoIfNeccessary(int lastUndoableChangeIndex) {
			//just sanity check
			Debug.Assert(this.Branch.CurrentChangeIndex <= this.Branch.Changes.Count);

			if (this.Branch.CurrentChangeIndex < this.Branch.Changes.Count - 1) {
				//branch is rewinded, therefore we need to ensure it is in cosistent state for editing,
				//which means we create multiundo to undo all changes from the head to current point, and then 
				//change current change to head
				int undoIndex = this.Branch.CurrentChangeIndex + 1;
				int undoCount = lastUndoableChangeIndex - undoIndex + 1;
				var changesToBeUndone = from index in Enumerable.Range(undoIndex, undoCount)
																select this.Branch.Changes.At(index);
				var multiUndo = this.Context.Core.CreateMultiUndo(
					changesToBeUndone.ToList(),
					this.DocumentController.AuthorController.CurrentAuthor,
					this.Context.DateTimeProvider.Now
				);
				this.Branch.Changes.Add(multiUndo);
				this.Branch.CurrentChangeIndex = this.Branch.Changes.Count - 1;
				this.MarkAsDirty();
			}
		}
		#endregion

		#region Rewind/Replay
		void RewindChange() {
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(this.Branch.CurrentChangeIndex - 1));
		}

		void RewindWord() {
			var finder = this.Context.Core.CreateWordFinder();
			int changeId = finder.FindPrevious(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			//this.SetEditingModeFromChangeDistance(changeId);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void RewindSentence() {
			var finder = this.Context.Core.CreateSentenceFinder();
			int changeId = finder.FindPrevious(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			//this.SetEditingModeFromChangeDistance(changeId);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void RewindParagraph() {
			var finder = this.Context.Core.CreateParagraphFinder();
			int changeId = finder.FindPrevious(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			this.SetEditingModeFromChangeDistance(changeId);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void RewindAll() {
			_useEditingMode = true;
			this.ExecuteShifter(this.Context.Core.CreateShifterToStart());
		}

		void ReplayChange() {
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(this.Branch.CurrentChangeIndex + 1));
		}

		void ReplayWord() {
			var finder = this.Context.Core.CreateWordFinder();
			int changeId = finder.FindNext(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void ReplaySentence() {
			var finder = this.Context.Core.CreateSentenceFinder();
			int changeId = finder.FindNext(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void ReplayParagraph() {
			var finder = this.Context.Core.CreateParagraphFinder();
			int changeId = finder.FindNext(this.Branch.Changes, this.Branch.CurrentChangeIndex);
			this.SetEditingModeFromChangeDistance(changeId);
			this.ExecuteShifter(this.Context.Core.CreateShifterToChange(changeId));
		}

		void ReplayAll() {
			_useEditingMode = true;
			this.ExecuteShifter(this.Context.Core.CreateShifterToEnd());
		}
		#endregion

		#region Special undo
		void DoSpecialUndo(ITextBoundaryFinder textBoundaryFinder) {
			lock (this.Branch) {
				//first get branch to consistent state - undo everything what was revinded
				this.StoreMultiUndoIfNeccessary(this.Branch.Changes.Count - 1);

				var nextUndoableChange = this.FindUndoableChange();
				//if no change can be undone, return
				if (nextUndoableChange == null) {
					//if there are no undoable changes available, and branch was rewinded, we stored at least multi undo for rewinding
					return;
				}
				var nextUndoableChangeIndex = this.Branch.Changes.IndexOf(nextUndoableChange);
				//find boundary and shif to it
				int matchingUndoableChangeIndex = textBoundaryFinder.FindPrevious(this.Branch.Changes, nextUndoableChangeIndex);
				var shifter = this.Context.Core.CreateShifterToChange(matchingUndoableChangeIndex);
				shifter.Shift(this.Branch, this.Editor.CoreBridge);
				//now store actual multiundo, but limit undoing to the part we really need to undo
				this.StoreMultiUndoIfNeccessary(nextUndoableChangeIndex);
				//update view so redo is enabled
				this.UpdateViewFlags();
			}
		}

		#endregion

		#region Other private methods
		private void PutTextToClipboardAsHtml() {
			var converter = this.Context.Core.CreateMarkupConverter();
			string convertedText = converter.ToHtmlFragment(this.Editor.Text);
			this.View.SetClipboard(convertedText);
		}

		void ShowBranchProperties() {
			var view = this.Context.View.CreateView<IBranchPropertiesView>();
			view.Name = this.Branch.Name;
			view.ExportFileName = this.ExportFileName;
			view.Comment = this.Branch.Comment;
			view.Author = this.Branch.Author.ToString();
			view.BranchingDateTime = this.Branch.BranchingDateTime;
			view.ExportFormats = this.Context.View.ApplicationView.ExportFormats;
			var currentFormat = from f in this.Context.View.ApplicationView.ExportFormats
													where f.Id == this.Exporter.ExporterId
													select f;
			if (currentFormat.Any()) {
				view.ExportFormat = currentFormat.First();
			} else {
				this.Context.View.ApplicationView.ExportFormats.First();
			}
			view.IsAlwaysExporting = this.IsAlwaysExporting;
			view.Parent = this.Branch.Parent != null ? this.Branch.Parent.Name : string.Empty;
			view.Open(this.Settings);
		}

		void CommitBranchProperties(IBranchPropertiesView propertiesView) {
			//process export filename
			// this needs to be processed before name, because otherwise export path changes (because name changed)
			var exportFilename = propertiesView.ExportFileName;
			if (string.IsNullOrWhiteSpace(exportFilename)) {
				//keeping field empty clears the name and uses default name
				this.Settings.Clear(Naracea.Common.Settings.BranchAutoExportFilename);
			} else {
				// if the entered name is the same as current name, do nothing
				// this ensures we get filename updated when branch name changes or document is renamed, 
				// because no name is stored and it is generated
				if (!exportFilename.Equals(this.ExportFileName)) {
					// ok, there is change to the name
					if (!this.Context.FileNameValidator.IsValid(exportFilename)
							&& !this.Context.FileNameValidator.IsValid(Path.Combine(Path.GetDirectoryName(this.DocumentController.RepositoryPath), exportFilename))) {
						// it is not valid, change it to valid filename
						exportFilename = this.Context.FileNameValidator.GetValidFileName(exportFilename);
					}
					// set is branch export filename
					this.Settings.Set(Naracea.Common.Settings.BranchAutoExportFilename, exportFilename);
				}
			}
			this.Name = propertiesView.Name;
			this.Branch.Comment = propertiesView.Comment;

			var exporterId = propertiesView.ExportFormat.Id;
			if (this.Context.Exporters.ContainsKey(exporterId)) {
				this.Exporter = this.Context.Exporters[exporterId];
			} else {
				Debug.Fail("Unknown exporter type.");
				this.Exporter = this.Context.Exporters.Values.First();
			}
			this.Settings.Set(Naracea.Common.Settings.BranchAutoExportFormat, this.Exporter.ExporterId);

			this.IsAlwaysExporting = propertiesView.IsAlwaysExporting;
			if (this.IsAlwaysExporting) {
				this.Settings.Set(Naracea.Common.Settings.BranchAutoExportAlwaysExport, this.IsAlwaysExporting);
			} else {
				this.Settings.Clear(Naracea.Common.Settings.BranchAutoExportAlwaysExport);
			}

			this.MarkAsDirty();
			propertiesView.Close();
			this.Context.View.ApplicationView.UpdateDocument(this.DocumentController.View);
		}

		void ShowBranchTextViewer() {
			if (_textViewer == null) {
				_textViewer = this.Context.View.CreateView<IBranchTextViewerView>();
				_textViewer.Open(this.Settings);
			} else {
				_textViewer.Show();
			}
			_textViewer.Title = this.DocumentController.Name + " - " + this.Name;
			var converter = this.Context.Core.CreateMarkupConverter();
			var xamlText = converter.ToXamlDocument(this.Editor.Text);
			_textViewer.UpdateText(xamlText, this.Editor.View.CurrentParagraph);
		}

		void OpenBrowser() {
			if (_temporaryFileForBrowseView == null) {
				//prep temp filename for browser view export
				_temporaryFileForBrowseView = Path.ChangeExtension(Path.GetTempFileName(), ".html");
			}

			if (!_pendingOpenBrowser) {
				_pendingOpenBrowser = true;
				if (this.Context.Exporters.ContainsKey("HTML")) {
					this.Bus.Publish(new Requests.ExportBranchWithName(this.DocumentController, this, this.Context.Exporters["HTML"], _temporaryFileForBrowseView));
				}
			} else {
				_pendingOpenBrowser = false;
				Process.Start(new ProcessStartInfo(_temporaryFileForBrowseView));
			}
		}

		void StoreTimelineHeight(double height) {
			this.Settings.Set(Naracea.Common.Settings.ViewBranchTimelineHeight, height);
		}

		void RestoreTimelineHeight() {
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewBranchTimelineHeight)) {
				var height = this.Settings.Get<double>(Naracea.Common.Settings.ViewBranchTimelineHeight);
				this.View.SetTimelineHeight(height);
			}
		}

		void StoreTimelineAndChangeStreamVisibility() {
			this.Settings.Set(Naracea.Common.Settings.TimelineShowTimelineOnly, this.View.IsTimelineVisibleOnly);
			this.Settings.Set(Naracea.Common.Settings.TimelineShowTimelineAndChangeStream, this.View.IsTimelineAndChangeStreamVisible);
			this.Settings.Set(Naracea.Common.Settings.TimelineShowChangeStreamOnly, this.View.IsChangeStreamVisibleOnly);
		}

		void RestoreTimelineAndChangeStream() {
			if (this.Settings.HasValue(Naracea.Common.Settings.TimelineShowTimelineOnly)) {
				this.View.IsTimelineVisibleOnly = this.Settings.Get<bool>(Naracea.Common.Settings.TimelineShowTimelineOnly);
			}
			if (this.Settings.HasValue(Naracea.Common.Settings.TimelineShowTimelineAndChangeStream)) {
				this.View.IsTimelineAndChangeStreamVisible = this.Settings.Get<bool>(Naracea.Common.Settings.TimelineShowTimelineAndChangeStream);
			}
			if (this.Settings.HasValue(Naracea.Common.Settings.TimelineShowChangeStreamOnly)) {
				this.View.IsChangeStreamVisibleOnly = this.Settings.Get<bool>(Naracea.Common.Settings.TimelineShowChangeStreamOnly);
			}
		}

		void StoreSpellcheckLanguage() {
			if (_spellcheckDictionary != null) {
				this.Settings.Set(Naracea.Common.Settings.SpellcheckLanguage, _spellcheckDictionary);
			} else {
				this.Settings.Clear(Naracea.Common.Settings.SpellcheckLanguage);
			}
		}

		void RestoreSpellchecker() {
			if (this.Settings.HasValue(Naracea.Common.Settings.SpellcheckLanguage)) {
				_spellcheckDictionary = this.Settings.Get<DictionaryDescriptor>(Naracea.Common.Settings.SpellcheckLanguage);
				this.View.Editor.SpellcheckLanguageName = _spellcheckDictionary.DictionaryName;
			}
		}

		void RestoreExporter() {
			string exporterId = this.Context.Exporters.Keys.FirstOrDefault();
			if (this.Settings.HasValue(Naracea.Common.Settings.BranchAutoExportFormat)) {
				exporterId = this.Settings.Get<string>(Naracea.Common.Settings.BranchAutoExportFormat);
			}
			if (this.Context.Exporters.ContainsKey(exporterId)) {
				this.Exporter = this.Context.Exporters[exporterId];
			} else {
				this.Exporter = this.Context.Exporters.Values.FirstOrDefault();
			}

			if (this.Settings.HasValue(Naracea.Common.Settings.BranchAutoExportAlwaysExport)) {
				this.IsAlwaysExporting = this.Settings.Get<bool>(Naracea.Common.Settings.BranchAutoExportAlwaysExport);
			}
		}

		void Settings_SettingsChanged(object sender, EventArgs e) {
			this.MarkAsDirty();
		}
		#endregion
	}
}
