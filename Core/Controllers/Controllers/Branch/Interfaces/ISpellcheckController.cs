﻿using MemBus;
using Naracea.Common;

namespace Naracea.Controller {
	public interface ISpellcheckController {
		IBus Bus { get; }
		ISettings Settings { get; }

		void ResetSpellingIntervals();

		void Close();
	}
}
