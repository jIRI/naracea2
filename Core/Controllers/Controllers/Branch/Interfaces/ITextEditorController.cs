﻿using System;
using MemBus;
using Naracea.Common;
using Naracea.Core;
using Naracea.Core.Model.Changes;
using Naracea.View;

namespace Naracea.Controller {
	public interface ITextEditorController {
		IBus Bus { get; }
		ITextEditorView View { get; }
		ITextEditor CoreBridge { get; }
		int CurrentLine { get; set; }
		int CurrentColumn { get; }
		int CaretPosition { get; set; }
		int TextLength { get; }
		bool IsOverwriteMode { get; }
		bool IsTextChangeReportingEnabled { get; set; }
		string Text { get; set; }
		string SelectedText { get; }
		ISettings Settings { get; }

		event EventHandler<TextChangedEventArgs> TextChanged;
		event EventHandler<TextChangedEventArgs> RawTextChanged;

		void Store(IChange change);

		void Insert(int position, string text);
		void Insert(string text);
		void Delete(int position, int length);
		void Delete(int length);
		void Select(int position, int length);
		void ReplaceSelection(string replaceWith);

		/// <summary>
		/// Begins editing mode.
		/// Applied when batch of changes is applied.
		/// </summary>
		void BeginEdit();
		/// <summary>
		/// Ends editing mode.
		/// Applied when batch of changes is applied.
		/// </summary>
		void EndEdit();

		/// <summary>
		/// Begins update mode.
		/// This is meant for text editor view for performance improvements
		/// </summary>
		void BeginUpdate();
		/// <summary>
		/// Ends update mode.
		/// This is meant for text editor view for performance improvements
		/// </summary>
		void EndUpdate();

		void Close();
	}
}
