﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Context;
using Naracea.Core.Model.Changes;
using Naracea.Core;
using Naracea.Common;
using MemBus;

namespace Naracea.Controller {
	public interface ITimelineController {
		IBus Bus { get; }
		ITimelineView View { get; }
		bool ShowEmptyBars { get; set; }
		IUnitOfTime Granularity { get; set; }
		IList<IUnitOfTime> AvailableGranularities { get;}
		ISettings Settings { get; }
		bool IsEditing { get; }

		void BeginEdit();
		void EndEdit();

		void Initialize();

		int CurrentChangeIndex { get; set; }

		void Close();
	}
}
