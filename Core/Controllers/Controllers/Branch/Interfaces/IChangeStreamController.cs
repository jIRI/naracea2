﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Context;
using Naracea.Core.Model.Changes;
using Naracea.Core;
using Naracea.Common;
using MemBus;

namespace Naracea.Controller {
	public interface IChangeStreamController {
		IBus Bus { get; }
		IChangeStreamView View { get; }
		ISettings Settings { get; }
		bool IsEditing { get; }

		void BeginEdit();
		void EndEdit();

		void Initialize();

		int CurrentChangeIndex { get; set; }

		void Close();
	}
}
