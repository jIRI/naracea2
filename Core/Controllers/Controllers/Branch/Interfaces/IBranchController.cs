﻿using MemBus;
using Naracea.Common;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Shifter;
using Naracea.Plugin.Export;
using Naracea.View;

namespace Naracea.Controller {
	public interface IBranchController {
		string Name { get; set; }
		string Comment { get; set; }
		string ExportFileName { get; }
		IBus Bus { get; }
		IBranchView View { get; }
		ITextEditorController Editor { get; }
		ITimelineController Timeline { get; }
		IChangeStreamController ChangeStream { get; }
		IBranch Branch { get; }
		IExportPlugin Exporter { get; }
		bool IsAlwaysExporting { get; set; }
		bool IsDirty { get; }
		bool IsActivated { get; }
		bool IsRewound { get; }
		bool IsDocumentActive { get; }
		ISettings Settings { get; }
		int EditModeThreshold { get; set; }
		bool IsTextChangeReportingEnabled { get; set; }
		bool IsEditing { get; }
		bool CanBeDeleted { get; }
		int PreferredVisualOrder { get; }

		void BeginEdit();
		void EndEdit();

		void StoreGroupBegin();
		void StoreGroupEnd();
		void Store(IChange change);
		
		IChange FindUndoableChange();
		IChange FindRedoableChange();
		void UndoWordIfPossible();
		void UndoSentenceIfPossible();
		
		void StoreBranchVisualOrder();
		void ExecuteShifter(IShifter shifter);

		void Close();
	}
}
