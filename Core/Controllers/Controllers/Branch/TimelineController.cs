﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.View;
using Naracea.View.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Naracea.Controller {
	public class TimelineController : ControllerBase, ITimelineController {
		#region Attributes
		List<TimelineBar> _bins = new List<TimelineBar>();
		#endregion

		#region ctors
		public TimelineController(IBranchController branch, IList<IUnitOfTime> availableGranularities, IControllerContext context)
			: base(context) {

			this.IsEditing = false;

			this.BranchController = branch;

			Debug.Assert(availableGranularities.Count > 0);
			this.AvailableGranularities = availableGranularities;


			this.View = this.Context.View.CreateView<ITimelineView>();
			this.View.Controller = this;
			this.View.AvailableGranularities = this.AvailableGranularities;

			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.BranchController.Settings, this.BranchController.Settings.Data);

			//Apply settings before intializing
			this.ApplySettings();

			//publich changes
			this.Bus.Publish(new TimelineSettingsChanged(this.View, this.Granularity, this.ShowEmptyBars));
		}
		#endregion

		#region ITimelineController Members
		public ITimelineView View { get; private set; }
		public ISettings Settings { get; private set; }
		public bool IsEditing { get; private set; }

		bool _showAllBars = false;
		public bool ShowEmptyBars {
			get {
				return _showAllBars;
			}
			set {
				if (_showAllBars != value) {
					_showAllBars = value;
					this.RefreshTimeline();
				}
			}
		}

		IUnitOfTime _granularity = null;
		public IUnitOfTime Granularity {
			get {
				return _granularity;
			}
			set {
				_granularity = value;
				this.RefreshTimeline();
			}
		}

		public IList<IUnitOfTime> AvailableGranularities { get; private set; }

		int _currentChangeIndex = -1;
		public int CurrentChangeIndex {
			get {
				return _currentChangeIndex;
			}
			set {
				_currentChangeIndex = value;
				if (!this.IsEditing) {
					this.View.CurrentChangeIndex = _currentChangeIndex;
				}
			}
		}

		public void Close() {
			this.BranchController.Branch.CurrentChangeChanged -= this.BranchController_CurrentChangeChanged;
			this.BranchController.Branch.Changes.ChangeAdded -= this.Changes_ChangeAdded;
			this.Dispose();
			if (this.View != null) {
				this.View.Close();
				this.View = null;
			}
			this.Bus.Publish(new Messages.TimelineClosed(this));
		}

		public void BeginEdit() {
			this.IsEditing = true;
		}

		public void EndEdit() {
			//update view
			this.IsEditing = false;
			if (_bins.Count > 0) {
				//we need to update very last bin (if there is any) so it is drawn properly
				this.View.UpdateLastBar(_bins[_bins.Count - 1]);
			}
			this.CurrentChangeIndex = this.CurrentChangeIndex;
		}

		public void Initialize() {
			if (this.View == null) {
				return;
			}

			this.View.Open(this.Settings);
			this.RefreshTimeline();
			this.InstallHandlers();
		}
		#endregion

		#region Private methods
		protected IBranchController BranchController { get; set; }

		void InitializeEmptyBranch() {
			var now = this.Granularity.Trim(this.Context.DateTimeProvider.Now);
			var newBin = new TimelineBar(now, this.Granularity.Advance(now), new List<int>());
			_bins.Add(newBin);
			this.View.AddBar(newBin);
		}

		void InitializeBranchWithChanges() {
			_bins.Clear();
			var firstBinTime = this.Granularity.Trim(this.BranchController.Branch.Changes.At(0).DateTime);
			_bins.Add(new TimelineBar(firstBinTime, this.Granularity.Advance(firstBinTime), new List<int>()));
			this.View.SetBars(_bins);

			var changeCount = this.BranchController.Branch.Changes.Count;
			for (int i = 0; i < changeCount; i++) {
				this.AddChangeWithoutUpdate(this.BranchController.Branch.Changes.At(i));
			}

			if (_bins.Count > 0) {
				// well, we do not update the view during last bin update, so we need to do update here
				this.View.UpdateLastBar(_bins[_bins.Count - 1]);
			}
		}

		void InstallHandlers() {
			this.BranchController.Branch.CurrentChangeChanged += this.BranchController_CurrentChangeChanged;
			this.BranchController.Branch.Changes.ChangeAdded += this.Changes_ChangeAdded;

			this.RegisterForDisposal(this.Bus.Subscribe<TimelineSettingsChanged>(
				m => {
					if (this.Granularity != m.Granularity) {
						this.ChangeGranularity(m.Granularity);
					}
					if (this.ShowEmptyBars != m.ShowEmptyBars) {
						this.ChangeShowEmptyBars(m.ShowEmptyBars);
					}
				},
				new ShapeToFilter<TimelineSettingsChanged>(
					msg => msg.TimelineController == this
									&& (msg.Granularity != this.Granularity || msg.ShowEmptyBars != this.ShowEmptyBars)
				)
			));
		}

		void ChangeGranularity(IUnitOfTime granularity) {
			this.ChangeGranularity(
				this.AvailableGranularities.IndexOf(granularity)
			);
		}

		void ChangeGranularity(int index) {
			int validIndex = index;
			if (index < 0 || index >= this.AvailableGranularities.Count) {
				validIndex = 0;
			}
			this.Granularity = this.AvailableGranularities[validIndex];
			this.Settings.Set(Naracea.Common.Settings.TimelineGranularityIndex, this.AvailableGranularities.IndexOf(this.Granularity));
			this.Bus.Publish(new TimelineSettingsChanged(this.View, this.Granularity, this.ShowEmptyBars));
		}

		void ChangeShowEmptyBars(bool showEmptyBars) {
			this.ShowEmptyBars = showEmptyBars;
			this.Settings.Set(Naracea.Common.Settings.TimelineShowEmptyBars, this.ShowEmptyBars);
			this.Bus.Publish(new TimelineSettingsChanged(this.View, this.Granularity, this.ShowEmptyBars));
		}

		void BranchController_CurrentChangeChanged(object sender, CurrentChangeChangedEventArgs e) {
			this.CurrentChangeIndex = e.ChangeIndex;
		}

		void Changes_ChangeAdded(object sender, ChangeAddedEventArgs e) {
			if (this.IsEditing) {
				this.AddChangeWithoutUpdate(e.Change);
			} else {
				this.AddChange(e.Change);
			}
		}

		private void AddChange(IChange change) {
			if (change.DateTime < _bins[_bins.Count - 1].To) {
				//if end of last bin is in future for added change, we can update last bin
				this.UpdateLastBar(change);
			} else {
				//otherwise we need to add at least one new bin
				this.AddNewBar(change);
			}
		}

		private void AddChangeWithoutUpdate(IChange change) {
			if (change.DateTime < _bins[_bins.Count - 1].To) {
				var lastBin = _bins[_bins.Count - 1];
				lastBin.Ids.Add(change.Id);
			} else {
				if (_bins.Count > 0) {
					// well, we do not update the view during last bin update, so we need to do update here
					this.View.UpdateLastBar(_bins[_bins.Count - 1]);
				}
				//otherwise we need to add at least one new bin
				this.AddNewBar(change);
			}
		}

		void AddNewBar(IChange change) {
			//first add empty bins if needed
			DateTimeOffset? emptyBinStart = null;
			int columnSpan = 1;
			for (var lastBinEnd = _bins[_bins.Count - 1].To; change.DateTime > lastBinEnd; lastBinEnd = this.Granularity.Advance(lastBinEnd)) {
				TimelineBar newBin = null;
				var nextBinEnd = this.Granularity.Advance(lastBinEnd);
				bool changeAdded = false;
				if (change.DateTime < nextBinEnd) {
					newBin = new TimelineBar(lastBinEnd, nextBinEnd, new List<int>());
					newBin.Ids.Add(change.Id);
					changeAdded = true;
				} else if (!emptyBinStart.HasValue) {
					emptyBinStart = lastBinEnd;
				} else {
					columnSpan++;
				}
				if (_showAllBars && changeAdded && emptyBinStart.HasValue) {
					var emptyBin = new TimelineBar(emptyBinStart.Value, lastBinEnd, columnSpan, new List<int>());
					_bins.Add(emptyBin);
					this.View.AddBar(emptyBin);
					columnSpan = 1;
					emptyBinStart = null;
				}
				if (changeAdded) {
					_bins.Add(newBin);
					this.View.AddBar(newBin);
				}
			}
		}

		void UpdateLastBar(IChange change) {
			var lastBin = _bins[_bins.Count - 1];
			lastBin.Ids.Add(change.Id);
			this.View.UpdateLastBar(lastBin);
		}

		void RefreshTimeline() {
			if (this.View == null) {
				return;
			}

			this.View.ClearBars();
			if (this.BranchController.Branch.Changes.Count == 0) {
				this.InitializeEmptyBranch();
			} else {
				this.InitializeBranchWithChanges();
			}
			this.CurrentChangeIndex = this.BranchController.Branch.CurrentChangeIndex;

			// ok, we want to set view position also, which doesn't happen in previous statement when in editing mode
			if (this.View != null) {
				this.View.CurrentChangeIndex = this.CurrentChangeIndex;
			}
		}

		private void ApplySettings() {
			_showAllBars = this.Settings.Get<bool>(Naracea.Common.Settings.TimelineShowEmptyBars);

			var granularityIndex = this.Settings.Get<int>(Naracea.Common.Settings.TimelineGranularityIndex);
			Debug.Assert(granularityIndex >= 0 && granularityIndex < this.AvailableGranularities.Count);
			_granularity = this.AvailableGranularities[granularityIndex];
		}
		#endregion
	}
}
