﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;
using Naracea.Core.Spellcheck;

namespace Naracea.Controller.Messages {
	public class BranchMessageBase {
		public IBranchController BranchController { get; private set; }
		public IDocumentController DocumentController { get; private set; }
		public BranchMessageBase(IDocumentController documentController, IBranchController branchController) {
			this.BranchController = branchController;
			this.DocumentController = documentController;
		}
	}

	public class BranchClosed : BranchMessageBase {
		public BranchClosed(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchDirty : BranchMessageBase {
		public BranchDirty(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class IndicatorsUpdateNeeded : BranchMessageBase {
		public IndicatorsUpdateNeeded(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}


	public class TextEditorMessageBase : BranchMessageBase {
		public ITextEditorController TextEditorController { get; private set; }
		public TextEditorMessageBase(IDocumentController documentController, IBranchController branchController, ITextEditorController textEditorController)
			: base(documentController, branchController) {
			this.TextEditorController = textEditorController;
		}
	}

	public class TextEditorClosed : TextEditorMessageBase {
		public TextEditorClosed(IDocumentController documentController, IBranchController branchController, ITextEditorController textEditorController)
			: base(documentController, branchController, textEditorController) {
		}
	}

	public class SpellcheckerCustomDictionaryUpdated : TextEditorMessageBase {
		public string Word { get; private set; }
		public DictionaryDescriptor TargetLanguage { get; private set; }

		public SpellcheckerCustomDictionaryUpdated(IDocumentController documentController, IBranchController branchController, ITextEditorController textEditorController, DictionaryDescriptor targetLanguage, string word)
			: base(documentController, branchController, textEditorController) {
			this.TargetLanguage = targetLanguage;
			this.Word = word;
		}
	}

	public class TimelineClosed {
		public ITimelineController TimelineController { get; private set; }
		public TimelineClosed(ITimelineController timelineController) {
			this.TimelineController = timelineController;
		}
	}

	public class ChangeStreamClosed {
		public IChangeStreamController ChangeStreamController { get; private set; }
		public ChangeStreamClosed(IChangeStreamController changeStreamController) {
			this.ChangeStreamController = changeStreamController;
		}
	}

	public class BranchShiftingFinished : BranchMessageBase {
		public BranchShiftingFinished(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}
}
