﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View;
using Naracea.Core.Spellcheck;
using Naracea.Plugin.Export;

namespace Naracea.Controller.Requests {
	public class ShiftToChange : Naracea.Controller.Messages.BranchMessageBase {
		public int ChangeIndex { get; set; }
		public ShiftToChange(IDocumentController documentController, IBranchController branchController, int changeIndex)
			: base(documentController, branchController) {
			this.ChangeIndex = changeIndex;
		}
	}

	public class ExportBranchWithName : Naracea.Controller.Messages.BranchMessageBase {
		public string FileName { get; private set; }
		public IExportPlugin ExportPlugin { get; private set; }
		public ExportBranchWithName(IDocumentController documentController, IBranchController branch, IExportPlugin exportPlugin, string name) :
			base(documentController, branch) {
			this.ExportPlugin = exportPlugin;
			this.FileName = name;
		}
	}

	public class SetSpellingLanguage : Naracea.Controller.Messages.BranchMessageBase {
		public DictionaryDescriptor Language { get; private set; }
		public SetSpellingLanguage(IDocumentController documentController, IBranchController branchController, DictionaryDescriptor language)
			: base(documentController, branchController) {
			this.Language = language;
		}
	}
}
