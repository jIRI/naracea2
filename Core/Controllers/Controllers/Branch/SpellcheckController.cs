﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Naracea.Controller {
	public class SpellcheckController : ControllerBase, ISpellcheckController {
		#region Embedded types
		class SpellingInterval {
			public int Start { get; set; }
			public int End { get; set; }
			public int Length {
				get {
					return this.End - this.Start + 1;
				}
			}
			public override string ToString() {
				return this.Start + ":" + this.End;
			}
		}

		class SpellingError {
			public int Start { get; set; }
			public int End { get; set; }
			public int Length {
				get {
					return this.End - this.Start + 1;
				}
			}
		}
		#endregion

		#region Attributes
		object _viewUpdateLock = new object();
		object _syncLock = new object();
		List<SpellingInterval> _intervals = new List<SpellingInterval>();
		object _spellerTaskLock = new object();
		Task _spellerTask = null;
		DictionaryDescriptor _dictionary = DictionaryDescriptor.None;
		ISpellchecker _spellchecker = null;
		string _text;
		bool _textUpdateNeeded = true;
		List<SpellingError> _spellingErrors = new List<SpellingError>();
		#endregion

		#region ctors
		public SpellcheckController(
				IDocumentController documentController,
				IBranchController branchController,
				ITextEditorController textEditorController,
				IControllerContext context
		)
			: base(context) {
			Debug.Assert(textEditorController != null);
			this.TextEditorController = textEditorController;

			Debug.Assert(branchController != null);
			this.BranchController = branchController;

			Debug.Assert(documentController != null);
			this.DocumentController = documentController;

			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.TextEditorController.Settings, this.TextEditorController.Settings.Data);

			//get spellchecker
			_spellchecker = this.Context.Core.CreateSpellchecker();

			//restore settings
			this.RestoreSpellcheckLanguage();

			//view handlers
			this.InstallEventHandlers();
		}
		#endregion

		#region ITextEditorController Members
		public ISettings Settings { get; private set; }

		public void ResetSpellingIntervals() {
			lock (_viewUpdateLock) {
				this.TextEditorController.View.ClearSpellingErrors();
			}

			lock (_syncLock) {
				_intervals.Clear();
				_spellingErrors.Clear();
				if (_dictionary == DictionaryDescriptor.None) {
					return;
				}

				if (this.TextEditorController.Text.Length > 0) {
					_intervals.Add(new SpellingInterval {
						Start = 0,
						End = this.TextEditorController.Text.Length - 1
					});
					_textUpdateNeeded = true;
					this.ScheduleSpellcheck();
				}
			}
		}

		/// <summary>
		/// Closes editor.
		/// Uninstalls event handlers.
		/// </summary>
		public void Close() {
			if (this.IsDisposed) {
				return;
			}

			if (_spellchecker.CustomDictionary != null) {
				_spellchecker.CustomDictionary.Save();
			}
			_spellchecker.Dispose();
			this.Dispose();
		}
		#endregion


		#region Private methods
		protected IBranchController BranchController { get; set; }

		protected IDocumentController DocumentController { get; set; }

		WeakReference _textEditorController;
		protected ITextEditorController TextEditorController {
			get {
				if (_textEditorController.IsAlive) {
					return _textEditorController.Target as ITextEditorController;
				} else {
					throw new InvalidOperationException();
				}
			}
			set {
				_textEditorController = new WeakReference(value);
			}
		}

		/// <summary>
		/// Install event handlers
		/// </summary>
		void InstallEventHandlers() {
			this.TextEditorController.RawTextChanged += this.TextEditorController_RawTextChanged;

			this.RegisterForDisposal(this.Context.Bus.Subscribe<Requests.SetSpellingLanguage>(
				m => {
					this.SetLanguage(m.Language);
					this.ResetSpellingIntervals();
				},
				new ShapeToFilter<Requests.SetSpellingLanguage>(msg => msg.BranchController != null && msg.BranchController.Editor == this.TextEditorController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SpellcheckSetLanguage>(
				m => {
					this.SetLanguage(m.Item.Data as DictionaryDescriptor);
					this.ResetSpellingIntervals();
				},
				new ShapeToFilter<Naracea.View.Requests.SpellcheckSetLanguage>(msg => msg.BranchController != null && msg.BranchController.Editor == this.TextEditorController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SuggestSpelling>(
				m => this.SuggestSpelling(m.WordPosition, m.WordLength),
				new ShapeToFilter<Naracea.View.Requests.SuggestSpelling>(msg => msg.TextEditorController != null && msg.TextEditorController == this.TextEditorController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.AddToSpellingDictionary>(
				m => this.AddWordToCustomDictionary(m.WordPosition, m.WordLength),
				new ShapeToFilter<Naracea.View.Requests.AddToSpellingDictionary>(msg => msg.TextEditorController != null && msg.TextEditorController == this.TextEditorController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchShiftingFinished>(
				m => this.RescanWholeText(),
				new ShapeToFilter<Messages.BranchShiftingFinished>(msg => msg.BranchController != null && msg.BranchController.Editor == this.TextEditorController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.SpellcheckerCustomDictionaryUpdated>(
				m => this.UpdateCustomDictionary(m.Word),
				new ShapeToFilter<Messages.SpellcheckerCustomDictionaryUpdated>(msg => msg.TextEditorController != this.TextEditorController && msg.TargetLanguage.Equals(_dictionary))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SpellcheckRecheckAll>(
				m => this.ResetSpellingIntervals(),
				new ShapeToFilter<Naracea.View.Requests.SpellcheckRecheckAll>(msg => msg.TextEditorController == this.TextEditorController)
			));
		}

		void RescanWholeText() {
			lock (_syncLock) {
				if (this.TextEditorController.Text.Length > 0) {
					_intervals.Add(new SpellingInterval {
						Start = 0,
						End = this.TextEditorController.Text.Length - 1
					});
					this.ScheduleSpellcheck();
				}
			}
		}

		void AddWordToCustomDictionary(int offset, int len) {
			var wordToBeAdded = _text.Substring(offset, len);
			_spellchecker.Add(wordToBeAdded);
			if (_spellchecker.CustomDictionary != null) {
				_spellchecker.CustomDictionary.Save();
			}
			this.RescanWholeText();
			this.Bus.Publish(new Messages.SpellcheckerCustomDictionaryUpdated(this.DocumentController, this.BranchController, this.TextEditorController, _dictionary, wordToBeAdded));
		}

		void UpdateCustomDictionary(string word) {
			_spellchecker.Add(word);
			this.RescanWholeText();
		}

		void SuggestSpelling(int offset, int len) {
			var suggestionSource = _text.Substring(offset, len);
			Task.Factory.StartNew(() => {
				this.TextEditorController.View.SetSpellingSuggestions(_spellchecker.Suggest(suggestionSource));
			});
		}

		void SetLanguage(DictionaryDescriptor dict) {
			if (dict != null) {
				_dictionary = dict;
			} else {
				_dictionary = DictionaryDescriptor.None;
			}

			if (_spellchecker.CustomDictionary != null) {
				_spellchecker.CustomDictionary.Save();
			}

			try {
				_spellchecker.CurrentLanguage = _dictionary;
			} catch (Exception) {
				_dictionary = DictionaryDescriptor.None;
			}
		}

		void TextEditorController_RawTextChanged(object sender, TextChangedEventArgs e) {
			lock (_syncLock) {
				_textUpdateNeeded = true;
				if (_dictionary == DictionaryDescriptor.None) {
					return;
				}

				this.UpdateSpellingIntervals(e);
				this.UpdateSpellingErrors(e);

				if (_intervals.Count > 0) {
					this.ScheduleSpellcheck();
				}
			}
		}

		private void UpdateSpellingErrors(TextChangedEventArgs e) {
			//relocate spelling errors
			for (int i = 0; i < _spellingErrors.Count; ) {
				var error = _spellingErrors[i];
				if (error.Start == e.Position || (error.Start < e.Position && error.End >= e.Position)) {
					// edit at the error position, end needs to be updated
					// remove error, whole original text was removed even partially
					// we can do some smartery here, but it can be the case, that some more words are inserted to the beginning of the error
					// and we would have some really strange spelling errors
					_spellingErrors.RemoveAt(i);
				} else if (error.Start > e.Position) {
					error.Start += e.AddedLength - e.RemovedLength;
					error.End += e.AddedLength - e.RemovedLength;
					if (error.Start < 0 || error.End < 0 || error.Length <= 0) {
						//something strange happend, remove error
						_spellingErrors.RemoveAt(i);
					} else {
						i++;
					}
				} else {
					i++;
				}
			}
		}

		private void UpdateSpellingIntervals(TextChangedEventArgs e) {
			var removals = new List<SpellingInterval>();
			for (int i = 0; i < _intervals.Count; i++) {
				var interval = _intervals[i];
				if (interval.Start == e.Position) {
					// edit at the interval position, end needs to be updated
					interval.End += e.AddedLength - e.RemovedLength;
				} else if (interval.Start > e.Position) {
					// edit in front of the interval, both start and end needs to be updated
					interval.Start += e.AddedLength - e.RemovedLength;
					interval.End += e.AddedLength - e.RemovedLength;
				} else {
					if ((interval.End + 1) >= e.Position) {
						// there was edit at the end or inside of the interval, update end of the interval
						interval.End += e.AddedLength - e.RemovedLength;
					}
				}

				if ((interval.Length != 0 && interval.Length < 2) || interval.Start < 0 || interval.End < 0) {
					// this interval is single char -> we do not spell such short words
					removals.Add(interval);
				}
			}

			// remove intervals which should be removed
			foreach (var interval in removals) {
				_intervals.Remove(interval);
			}

			//if we do not have current change coverd already in any interval, create new interval for it
			if (!_intervals.Any(i => e.Position >= i.Start && e.Position <= i.End)) {
				// we will find correct interval span in spelling task
				_intervals.Add(new SpellingInterval {
					Start = e.Position,
					End = e.Position
				});
			}
		}

		Tuple<int, int> FindWordBoundariesWithOverlap(string text, int position) {
			int start = 0;
			int end = text.Length - 1;
			bool wasNonWhiteSpaceChar = false;
			for (int i = position; i >= 0 && i <= end; i--) {
				char c = text[i];
				wasNonWhiteSpaceChar |= !char.IsWhiteSpace(c);
				if (char.IsWhiteSpace(c) && wasNonWhiteSpaceChar) {
					start = i;
					break;
				}
			}

			wasNonWhiteSpaceChar = false;
			for (int i = position + 1; i >= 0 && i <= end; i++) {
				char c = text[i];
				wasNonWhiteSpaceChar |= !char.IsWhiteSpace(c);
				if (char.IsWhiteSpace(c) && wasNonWhiteSpaceChar) {
					end = i;
					break;
				}
			}
			return new Tuple<int, int>(start, end);
		}

		Tuple<int, int> FindWordBoundaries(string text, int position) {
			int start = 0;
			int end = text.Length - 1;
			for (int i = position; i >= 0 && i <= end; i--) {
				char c = text[i];
				if (char.IsWhiteSpace(c)) {
					start = i;
					break;
				}
			}
			for (int i = position + 1; i >= 0 && i <= end; i++) {
				char c = text[i];
				if (char.IsWhiteSpace(c)) {
					end = i;
					break;
				}
			}
			return new Tuple<int, int>(start, end);
		}

		void ScheduleSpellcheck() {
			lock (_spellerTaskLock) {
				if (_spellerTask == null) {
					_spellerTask = Task.Factory.StartNew(() => {
						bool shouldContinue = true;
						for (int i = 0; i < 3 && shouldContinue; i++) {
							shouldContinue = this.DoOneSpellingStep();
						}

						if (!shouldContinue) {
							this.UpdateView();
						}

						lock (_spellerTaskLock) {
							_spellerTask = null;
						}
						if (shouldContinue) {
							this.ScheduleSpellcheck();
						}
					});
				}
			}
		}

		bool DoOneSpellingStep() {
			string word = null;
			int startOffset = 0;
			int endOffset = 0;

			// if there was text change, we need to get the text
			bool doUpdate = false;
			lock (_syncLock) {
				doUpdate = _textUpdateNeeded;
				_textUpdateNeeded = false;
			}

			if (doUpdate || _text == null) {
				_text = this.TextEditorController.Text;
			}

			lock (_syncLock) {
				// first merge overlapping intervals
				this.MergeIntervals();

				// check whether there is something to spell
				if (_intervals.Count == 0 || _dictionary == DictionaryDescriptor.None) {
					return false;
				}
				// get first available word from first available interval
				this.GetWordBoundaries(0, out startOffset, out endOffset);
				word = _text.Substring(startOffset, endOffset - startOffset + 1);
			}

			int startTrimCorrection = 0;
			int endTrimCorrection = 0;
			//get rid of spaces and punctuation
			word = SpellcheckController.TrimPunctuation(word, out startTrimCorrection, out endTrimCorrection);
			startOffset += startTrimCorrection;
			endOffset -= endTrimCorrection;

			// if what we have found is word, check it
			if (!string.IsNullOrEmpty(word) && word.Length > 1 && SpellcheckController.IsStrictWord(word)) {
				bool isCorrect = _spellchecker.Spell(word);
				if (!isCorrect) {
					this.HandleWrongSpelling(startOffset, endOffset, startTrimCorrection, endTrimCorrection);
				} else {
					this.HandleGoodSpelling(startOffset, endOffset, startTrimCorrection, endTrimCorrection);
				}
			}

			return true;
		}

		private void HandleGoodSpelling(int startOffset, int endOffset, int startTrim, int endTrim) {
			SpellingError[] errors = null;
			lock (_syncLock) {
				//find and remove error
				var query = from err in _spellingErrors
										where err.Start >= (startOffset - startTrim) && err.Start < (endOffset + endTrim) || err.Start >= _text.Length
										select err;

				errors = query.ToArray();
				foreach (var err in errors) {
					//remove it from the list
					_spellingErrors.Remove(err);
				}
			}

			if (errors.Length > 0) {
				this.UpdateView();
			}
		}

		private void HandleWrongSpelling(int startOffset, int endOffset, int startTrim, int endTrim) {
			lock (_syncLock) {
				var overlappingErrors = from err in _spellingErrors
																where err.Start >= (startOffset - startTrim) && err.Start < (endOffset + endTrim) || err.Start >= _text.Length
																select err;

				var removal = overlappingErrors.ToArray();
				foreach (var err in removal) {
					//remove it from the list
					_spellingErrors.Remove(err);
				}
			}

			lock (_syncLock) {
				_spellingErrors.Add(new SpellingError {
					Start = startOffset,
					End = endOffset
				});
			}
		}

		void GetWordBoundaries(int intervalIndex, out int startOffset, out int endOffset) {
			var interval = _intervals[intervalIndex];
			// this is uninitialized interval, update it
			if (interval.Start == interval.End) {
				var overlappingBoundaries = this.FindWordBoundariesWithOverlap(_text, interval.Start);
				interval.Start = overlappingBoundaries.Item1;
				interval.End = overlappingBoundaries.Item2;
			}

			var wordBoundaries = this.FindWordBoundaries(_text, interval.Start);
			startOffset = wordBoundaries.Item1;
			endOffset = wordBoundaries.Item2;

			if (startOffset <= interval.Start && endOffset >= interval.End) {
				_intervals.RemoveAt(intervalIndex);
			} else {
				// we can safely add 1 here, we will correct it eventually in FindWordBoundaries()
				interval.Start = endOffset + 1;
				if (interval.Start >= interval.End) {
					_intervals.RemoveAt(intervalIndex);
				}
			}
		}

		private void MergeIntervals() {
			// merge overlapping intervals
			var removals = new List<SpellingInterval>();
			// this is not perfect marge, we will probably skip some mergeabla items here, 
			// but this is sufficient for our purposes, and this is run before each spellcheck, so we will eventually merge everything
			for (int i = 0; i < _intervals.Count; i++) {
				// get current item
				var current = _intervals[i];
				// find whether there are any intervals starting inside of current interval
				var overlappingQuery = from interval in _intervals
															 where interval != current && (interval.Start >= current.Start && interval.Start <= current.End)
															 select interval;
				var overlappingItems = overlappingQuery.ToArray();
				// if so...
				if (overlappingItems.Length > 0) {
					//... find max end value and set it as current interval's end
					current.End = Math.Max(current.End, overlappingItems.Max(item => item.End));
					// remove all overlapping intervals
					foreach (var item in overlappingItems) {
						_intervals.Remove(item);
					}
				}
			}
		}

		static bool IsStrictWord(string word) {
			return word.All(c => {
				var category = char.GetUnicodeCategory(c);
				return char.IsLetterOrDigit(c)
					|| category == System.Globalization.UnicodeCategory.DashPunctuation
					|| category == System.Globalization.UnicodeCategory.InitialQuotePunctuation
					|| category == System.Globalization.UnicodeCategory.FinalQuotePunctuation
					|| c == '\''
					;
			});
		}

		static string TrimPunctuation(string word, out int startTrimCorrection, out int endTrimCorrection) {
			string trimed = word.TrimStart();
			startTrimCorrection = word.Length - trimed.Length;
			//get len for end trim correction
			endTrimCorrection = trimed.Length;
			trimed = trimed.TrimEnd();
			endTrimCorrection -= trimed.Length;
			var start = 0;
			var end = trimed.Length - 1;
			for (; start < end; start++) {
				if (char.IsPunctuation(trimed[start])) {
					startTrimCorrection++;
					continue;
				} else {
					break;
				}
			}

			for (; end > start; end--) {
				if (char.IsPunctuation(trimed[end])) {
					endTrimCorrection++;
					continue;
				} else {
					break;
				}
			}

			var result = trimed.Substring(start, end - start + 1);
			return result;
		}

		void RestoreSpellcheckLanguage() {
			if (this.Settings.HasValue(Naracea.Common.Settings.SpellcheckLanguage)) {
				var dict = this.Settings.Get<DictionaryDescriptor>(Naracea.Common.Settings.SpellcheckLanguage);
				this.SetLanguage(dict);
			}
		}

		void UpdateView() {
			lock (_viewUpdateLock) {
				if (this.TextEditorController.View != null) {
					this.TextEditorController.View.SetSpellingErrors(_spellingErrors.Select(err => new Tuple<int, int>(err.Start, err.Length)));
				}
			}
		}
		#endregion
	}
}
