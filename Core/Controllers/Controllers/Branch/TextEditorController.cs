﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Common.Extensions;
using Naracea.Core;
using Naracea.Core.Model.Changes;
using Naracea.View;
using Naracea.View.Components;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Naracea.Controller {
	public class TextEditorController : ControllerBase, ITextEditorController {
		#region Attributes
		IEditorIndicators _indicators;
		ITextEditor _textEditor;
		ISpellcheckController _spellchecker;
		ITextEditorProxy _textEditorProxy;
		#endregion

		#region ctors
		/// <summary>
		/// ctor.
		/// Creates view and text editor bridge which is needed by changes, so they can do what ever they want to do 
		/// during OnStoring() call.
		/// </summary>
		/// <param name="documentController"></param>
		/// <param name="branchController"></param>
		/// <param name="context"></param>
		public TextEditorController(
				IDocumentController documentController,
				IBranchController branchController,
				IControllerContext context
		)
			: base(context) {

			this.IsTextChangeReportingEnabled = false;

			Debug.Assert(branchController != null);
			this.BranchController = branchController;

			Debug.Assert(documentController != null);
			this.DocumentController = documentController;

			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.BranchController.Settings, this.BranchController.Settings.Data);

			this.View = this.Context.View.CreateView<ITextEditorView>();
			this.View.Open(this.Settings);

			// get spellchecker
			_spellchecker = this.Context.Controller.CreateSpellcheckController(this.DocumentController, this.BranchController, this, this.Context);

			//view related stuff
			_textEditor = this.Context.Controller.CreateTextEditorBridge(this.View);
			_textEditorProxy = this.Context.Core.CreateTextEditorProxy(_textEditor);
			this.CoreBridge = _textEditorProxy;
			this.View.Controller = this;

			//create indicators
			_indicators = this.Context.View.CreateView<IEditorIndicators>();
			_indicators.Hide();
			_indicators.Open(this.Context.View.ApplicationView.DocumentStatusBar);

			//view handlers
			this.InstallEventHandlers();

			//now, when all is done, restore editor settings
			this.RestoreLineWrapping();
			this.RestoreShowWhitespaces();
			this.RestoreKeepTabs();
			this.RestoreTabSize();
			this.RestoreSyntaxHighlighting();
		}
		#endregion

		#region ITextEditorController Members
		public event EventHandler<TextChangedEventArgs> TextChanged;
		public event EventHandler<TextChangedEventArgs> RawTextChanged;

		public ITextEditor CoreBridge { get; private set; }
		public ITextEditorView View { get; private set; }
		public ISettings Settings { get; private set; }
		public bool IsTextChangeReportingEnabled { get; set; }

		public int CurrentLine {
			get {
				return this.View.CurrentLine;
			}
			set {
				this.View.CurrentLine = value;
			}
		}
		public int CurrentParagraph {
			get {
				return this.View.CurrentParagraph;
			}
			set {
				this.View.CurrentParagraph = value;
			}
		}
		public int CurrentColumn {
			get {
				return this.View.CurrentColumn;
			}
		}
		public int CaretPosition {
			get {
				return this.View.CaretPosition;
			}
			set {
				this.View.CaretPosition = value;
			}
		}
		public int TextLength {
			get {
				return this.View.TextLength;
			}
		}
		public bool IsOverwriteMode {
			get {
				return this.View.IsOverwriteMode;
			}
		}

		public string Text {
			get {
				return this.CoreBridge.Text;
			}
			set {
				this.CoreBridge.Text = value;
			}
		}

		public string SelectedText {
			get {
				return this.View.SelectedText;
			}
		}

		/// <summary>
		/// Closes editor.
		/// Uninstalls event handlers.
		/// </summary>
		public void Close() {
			if (this.IsDisposed) {
				return;
			}

			_spellchecker.Close();
			_spellchecker = null;

			_textEditorProxy = null;
			_indicators.Close();
			_indicators = null;
			this.Dispose();
			if (this.View != null) {
				this.View.RawTextChanged -= this.View_CombinedTextChanged;
				this.View.StoreTextInserted -= this.View_StoreTextInserted;
				this.View.StoreTextDeleted -= this.View_StoreTextDeleted;
				this.View.StoreTextBackspaced -= this.View_StoreTextBackspaced;
				this.View.StoreGroupBegin -= this.View_StoreGroupBegin;
				this.View.StoreGroupEnd -= this.View_StoreGroupEnd;
				this.View.StoreUndo -= this.View_StoreUndo;
				this.View.StoreUndoWord -= this.View_StoreUndoWord;
				this.View.StoreUndoSentence -= this.View_StoreUndoSentence;
				this.View.StoreRedo -= this.View_StoreRedo;
				this.View.PasteCharsAsChanges -= this.View_PasteCharsAsChanges;
				this.View.UpdateIndicators -= this.View_UpdateIndicators;
				this.View.Close();
				this.View = null;
			}
			this.Bus.Publish(new Messages.TextEditorClosed(this.DocumentController, this.BranchController, this));
		}

		/// <summary>
		/// Stores change.
		/// Actually it executes change's OnStoring() method (which may do something important)
		/// and forwards storing to branch, which holds actual document's branch.
		/// </summary>
		/// <param name="change"></param>
		public void Store(IChange change) {
			this.CheckDisposeState();
			change.OnStoring(this.CoreBridge);
			this.BranchController.Store(change);
		}

		public void Insert(int position, string text) {
			this.CheckDisposeState();
			this.View.Insert(position, text);
		}

		public void Insert(string text) {
			this.CheckDisposeState();
			this.View.Insert(text);
		}

		public void Delete(int position, int length) {
			this.CheckDisposeState();
			this.View.Delete(position, length);
		}

		public void Delete(int length) {
			this.CheckDisposeState();
			this.View.Delete(length);
		}

		public void Select(int position, int length) {
			this.CheckDisposeState();
			this.View.Select(position, length);
		}

		public void ReplaceSelection(string replaceWith) {
			this.CheckDisposeState();
			this.View.ReplaceSelection(replaceWith);
		}

		public void BeginEdit() {
			this.CheckDisposeState();
			// do not report changes
			_textEditorProxy.BeginEdit();
		}

		public void EndEdit() {
			this.CheckDisposeState();
			// start reporting changes
			_textEditorProxy.EndEdit();
			_spellchecker.ResetSpellingIntervals();
		}

		public void BeginUpdate() {
			this.CheckDisposeState();
			// do not repaint
			_textEditorProxy.BeginUpdate();
		}

		public void EndUpdate() {
			this.CheckDisposeState();
			// start repainting repaint
			_textEditorProxy.EndUpdate();
		}
		#endregion

		#region Private methods
		protected IBranchController BranchController { get; set; }

		protected IDocumentController DocumentController { get; set; }

		/// <summary>
		/// Install event handlers
		/// </summary>
		void InstallEventHandlers() {
			// text editor specific messages
			this.View.RawTextChanged += this.View_CombinedTextChanged;
			this.View.StoreTextInserted += this.View_StoreTextInserted;
			this.View.StoreTextDeleted += this.View_StoreTextDeleted;
			this.View.StoreTextBackspaced += this.View_StoreTextBackspaced;
			this.View.StoreGroupBegin += this.View_StoreGroupBegin;
			this.View.StoreGroupEnd += this.View_StoreGroupEnd;
			this.View.StoreUndo += this.View_StoreUndo;
			this.View.StoreUndoWord += this.View_StoreUndoWord;
			this.View.StoreUndoSentence += this.View_StoreUndoSentence;
			this.View.StoreRedo += this.View_StoreRedo;
			this.View.PasteCharsAsChanges += this.View_PasteCharsAsChanges;
			this.View.UpdateIndicators += this.View_UpdateIndicators;

			// other editor handlers
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ToggleWrapLines>(
				m => this.ToggleLineWrapping(),
				new ShapeToFilter<Naracea.View.Requests.ToggleWrapLines>(msg => msg.TextEditorController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ToggleShowWhitespaces>(
				m => this.ToggleShowWhitespaces(),
				new ShapeToFilter<Naracea.View.Requests.ToggleShowWhitespaces>(msg => msg.TextEditorController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.ToggleKeepTabs>(
				m => this.ToggleKeepTabs(),
				new ShapeToFilter<Naracea.View.Requests.ToggleKeepTabs>(msg => msg.TextEditorController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SetTabSize>(
				m => this.SetTabSize(m.Size),
				new ShapeToFilter<Naracea.View.Requests.SetTabSize>(msg => msg.TextEditorController == this)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.SetSyntaxHighlighting>(
				m => this.SetSyntaxHighlighting(m.SyntaxHighlighter),
				new ShapeToFilter<Naracea.View.Requests.SetSyntaxHighlighting>(msg => msg.TextEditorController == this)
			));

			_textEditorProxy.ProxiedTextChanged += (s, e) => {
				if (!_textEditorProxy.IsEditing) {
					this.TextChanged.Raise(this, e);
				}
				if (this.IsTextChangeReportingEnabled) {
					this.Context.Bus.Publish(
						new View.Messages.TextEditorTextChanged(this.View, e.Position, e.AddedLength, e.RemovedLength)
					);
				}
			};

			//branch specific messages
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchActivated>(
				m => {
					_indicators.Show();
				},
				new ShapeToFilter<Messages.BranchActivated>(msg => msg.BranchController == this.BranchController && this.BranchController.IsDocumentActive && this.DocumentController.ActiveBranch == this.BranchController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDeactivated>(
				m => {
					_indicators.Hide();
				},
				new ShapeToFilter<Messages.BranchDeactivated>(msg => msg.BranchController == this.BranchController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.IndicatorsUpdateNeeded>(
				m => this.UpdateIndicators(),
				new ShapeToFilter<Messages.IndicatorsUpdateNeeded>(msg => msg.BranchController == this.BranchController)
			));
		}

		void View_CombinedTextChanged(object sender, View.Events.RawTextChangedArgs e) {
			this.RawTextChanged.Raise(this, new TextChangedEventArgs(e.Position, e.AddedLength, e.RemovedLength));
		}

		void View_UpdateIndicators(object sender, View.Events.UpdateIndicatorsArgs e) {
			this.UpdateIndicators();
		}

		void View_PasteCharsAsChanges(object sender, View.Events.PasteCharsAsChangesArgs e) {
			this.PasteCharsAsChanges(e.Position, e.Text);
		}

		void View_StoreRedo(object sender, View.Events.StoreRedoArgs e) {
			this.FindStoreRedoIfAvailable();
		}

		void View_StoreUndoSentence(object sender, View.Events.StoreUndoSentenceArgs e) {
			this.BranchController.UndoSentenceIfPossible();
		}

		void View_StoreUndoWord(object sender, View.Events.StoreUndoWordArgs e) {
			this.BranchController.UndoWordIfPossible();
		}

		void View_StoreUndo(object sender, View.Events.StoreUndoArgs e) {
			this.FindAndStoreUndoIfAvailable();
		}

		void View_StoreGroupEnd(object sender, View.Events.StoreGroupEndArgs e) {
			this.BranchController.StoreGroupEnd();
		}

		void View_StoreGroupBegin(object sender, View.Events.StoreGroupBeginArgs e) {
			this.BranchController.StoreGroupBegin();
		}

		void View_StoreTextBackspaced(object sender, View.Events.StoreTextBackspacedArgs e) {
			this.StoreBackspaceText(e.Position, e.Text);
		}

		void View_StoreTextDeleted(object sender, View.Events.StoreTextDeletedArgs e) {
			this.StoreDeleteText(e.Position, e.Text);
		}

		void View_StoreTextInserted(object sender, View.Events.StoreTextInsertedArgs e) {
			this.StoreTextInserted(e.Position, e.Text);
		}

		void UpdateIndicators() {
			if (_indicators != null && _indicators.IsOpen && _indicators.IsVisible) {
				_indicators.CurrentLine = this.CurrentLine;
				_indicators.CurrentParagraph = this.CurrentParagraph;
				_indicators.CurrentColumn = this.CurrentColumn;
				_indicators.CurrentCaretPosition = this.CaretPosition;
				_indicators.TextLength = this.TextLength;
				_indicators.IsOverWriteMode = this.IsOverwriteMode;
			}
		}

		void ToggleLineWrapping() {
			bool newValue = !this.View.WrapsLines;
			this.Settings.Set(Naracea.Common.Settings.ViewTextEditorWrapsLines, newValue);
			this.View.WrapsLines = newValue;
		}

		void ToggleShowWhitespaces() {
			bool newValue = !this.View.ShowWhitespaces;
			this.Settings.Set(Naracea.Common.Settings.ViewTextEditorShowWhitespaces, newValue);
			this.View.ShowWhitespaces = newValue;
		}

		void ToggleKeepTabs() {
			bool newValue = !this.View.KeepTabs;
			this.Settings.Set(Naracea.Common.Settings.ViewTextEditorKeepTabs, newValue);
			this.View.KeepTabs = newValue;
		}

		void SetTabSize(int size) {
			if (size > 0) {
				this.Settings.Set(Naracea.Common.Settings.ViewTextEditorTabSize, size);
				this.View.TabSize = size;
			}
		}

		void SetSyntaxHighlighting(string highlighterName) {
			if (highlighterName != null) {
				this.Settings.Set(Naracea.Common.Settings.ViewTextEditorSyntaxHighlighting, highlighterName);
				this.View.SyntaxHighlighting = highlighterName;
			} else {
				this.Settings.Clear(Naracea.Common.Settings.ViewTextEditorSyntaxHighlighting);
			}
		}

		void RestoreLineWrapping() {
			bool wrapsLines = true;
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewTextEditorWrapsLines)) {
				wrapsLines = this.Settings.Get<bool>(Naracea.Common.Settings.ViewTextEditorWrapsLines);
			}
			this.View.WrapsLines = wrapsLines;
		}

		void RestoreShowWhitespaces() {
			bool showWhitespaces = false;
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewTextEditorShowWhitespaces)) {
				showWhitespaces = this.Settings.Get<bool>(Naracea.Common.Settings.ViewTextEditorShowWhitespaces);
			}
			this.View.ShowWhitespaces = showWhitespaces;
		}

		void RestoreKeepTabs() {
			bool keepTabs = false;
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewTextEditorKeepTabs)) {
				keepTabs = this.Settings.Get<bool>(Naracea.Common.Settings.ViewTextEditorKeepTabs);
			}
			this.View.KeepTabs = keepTabs;
		}

		void RestoreTabSize() {
			int tabSize = 4;
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewTextEditorTabSize)) {
				tabSize = this.Settings.Get<int>(Naracea.Common.Settings.ViewTextEditorTabSize);
			}
			if (tabSize > 0) {
				this.View.TabSize = tabSize;
			}
		}

		void RestoreSyntaxHighlighting() {
			string highlighterName = null;
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewTextEditorSyntaxHighlighting)) {
				highlighterName = this.Settings.Get<string>(Naracea.Common.Settings.ViewTextEditorSyntaxHighlighting);
			}
			this.View.SyntaxHighlighting = highlighterName;
		}
		#endregion

		#region Change storing methods
		void StoreBackspaceText(int position, string text) {
			_textEditorProxy.StoringBackspace(position, text);
			this.Store(
				this.Context.Core.CreateBackspaceText(
						position,
						text,
						this.DocumentController.AuthorController.CurrentAuthor,
						this.Context.DateTimeProvider.Now
			));
			// backspacing new line at the beginning of the line doesn't not update selection
			// therefore we need update indicators manually
			this.UpdateIndicators();
		}

		void StoreDeleteText(int position, string text) {
			_textEditorProxy.StoringDelete(position, text);
			this.Store(
				this.Context.Core.CreateDeleteText(
					position,
					text,
					this.DocumentController.AuthorController.CurrentAuthor,
					this.Context.DateTimeProvider.Now
			));
		}

		void StoreTextInserted(int position, string text) {
			this.StoreTextInsert(position, text);
		}

		private void StoreTextInsert(int pos, string text) {
			_textEditorProxy.StoringInsert(pos, text);
			this.Store(
				this.Context.Core.CreateInsertText(
					pos,
					text,
					this.DocumentController.AuthorController.CurrentAuthor,
					this.Context.DateTimeProvider.Now
			));
		}

		void FindStoreRedoIfAvailable() {
			var change = this.BranchController.FindRedoableChange();
			if (change != null) {
				this.Store(
					this.Context.Core.CreateRedo(
						change,
						this.DocumentController.AuthorController.CurrentAuthor,
						this.Context.DateTimeProvider.Now
				));
			}
		}

		void FindAndStoreUndoIfAvailable() {
			var change = this.BranchController.FindUndoableChange();
			if (change != null) {
				this.Store(
					this.Context.Core.CreateUndo(
						change,
						this.DocumentController.AuthorController.CurrentAuthor,
						this.Context.DateTimeProvider.Now
				));
			}
		}

		void PasteCharsAsChanges(int position, string text) {
			Debug.Assert(!string.IsNullOrEmpty(text));
			this.StoreLongText(position, text);
		}

		void StoreLongText(int startPos, string text) {
			bool isLongRunning = text.Length > 1000;

			Action action = () => {
				var selectedText = this.View.SelectedText;
				if (!string.IsNullOrEmpty(selectedText)) {
					this.View.ReplaceSelection("");
				}

				IProgressIndicator progressIndicator = null;
				bool cancelled = false;
				if (isLongRunning) {
					this.BranchController.BeginEdit();
					progressIndicator = this.OpenProgressIndicator();
					progressIndicator.CancelRequested += (s, e) => cancelled = true;
				}
				int pos = startPos;
				int percentDone = 0;
				int step = (int)((double)text.Length / 100.0) + 1;
				foreach (var ch in text) {
					var textToStore = ch.ToString();
					switch (ch) {
						case '\r':
							continue;
						case '\n':
							textToStore = Constants.NewLine;
							break;
					}
					this.StoreTextInsert(pos, textToStore);
					pos++;
					if (isLongRunning && pos % step == 0) {
						percentDone++;
						progressIndicator.Value = percentDone;
					}
					if (cancelled) {
						break;
					}
				}
				var pastedText = text;
				if (cancelled) {
					pastedText = text.Substring(0, pos - startPos);
				}
				this.View.InsertTextBeforePosition(startPos, pastedText);
				this.View.CaretPosition = startPos + pastedText.Length;
				if (isLongRunning) {
					progressIndicator.Close();
					this.BranchController.EndEdit();
				}
			};

			if (isLongRunning) {
				Task.Factory.StartNew(action);
			} else {
				action();
			}
		}

		IProgressIndicator OpenProgressIndicator() {
			var progressIndicator = this.Context.View.CreateView<IProgressIndicator>();
			progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.AddingChanges;
			progressIndicator.IsIndeterminate = false;
			progressIndicator.To = 100;
			progressIndicator.IsCancellable = true;
			progressIndicator.Open(this.Context.View.ApplicationView.ProgressIndicators);
			return progressIndicator;
		}
		#endregion
	}
}
