﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core;
using Naracea.View;
using System.Diagnostics;

namespace Naracea.Controller {
	/// <summary>
	/// This class is used to allow core's changes to update text document when they need it.
	/// So it is forwarding commands from changes to view. 	
	/// </summary>
	public class TextEditorBridge : ITextEditor {
		ITextEditorView _textEditorView;

		public TextEditorBridge(ITextEditorView editorView) {
			Debug.Assert(editorView != null);
			_textEditorView = editorView;
		}

		#region ITextEditor Members
		public string Text {
			get {
				return _textEditorView.Text;
			}
			set {
				_textEditorView.Text = value;
			}
		}

		public int CaretPosition {
			get {
				return _textEditorView.CaretPosition;
			}
			set {
				_textEditorView.CaretPosition = value;
			}
		}

		public void InsertTextBeforePosition(int pos, string text) {
			_textEditorView.InsertTextBeforePosition(pos, text);
		}

		public void InsertTextBehindPosition(int pos, string text) {
			_textEditorView.InsertTextBehindPosition(pos, text);
		}

		public void DeleteText(int pos, string text) {
			_textEditorView.DeleteText(pos, text);
		}

		public void BackspaceText(int pos, string text) {
			_textEditorView.BackspaceText(pos, text);
		}

		public void SetTextSilently(string text) {
			_textEditorView.SetTextSilently(text);
		}

		public void BeginUpdate() {
			_textEditorView.BeginUpdate();
		}

		public void EndUpdate() {
			_textEditorView.EndUpdate();
		}
		#endregion
	}
}
