﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;

namespace Naracea.Controller {
	public class HistorySearchMatchItem {
		public int Position { get; set; }
		public string MatchedText { get; set; }
		public bool WasOverwritten { get; set; }
		public int CurrentChangeIndex { get; set; }
		public ISearchMatchItem SearchMatchItem { get; set; }
	}
}
