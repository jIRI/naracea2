﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Core.Model;
using Naracea.Core;
using Naracea.View;
using Naracea.Controller.ErrorReporters;
using Naracea.Common.Exceptions;
using Naracea.Common;
using MemBus;
using Naracea.Plugin;
using Naracea.Controller.Plugins;

namespace Naracea.Controller {
	public class ControllerFactories : IControllerFactories {
		Func<IController, IDocumentController> _document;
		Func<IDocumentController, IBranch, IControllerContext, IBranchController> _branch;
		Func<IDocumentController, IBranchController, IControllerContext, ITextEditorController> _textEditor;
		Func<IDocumentController, IBranchController, ITextEditorController, IControllerContext, ISpellcheckController> _spellchecker;
		Func<IBranchController, IList<IUnitOfTime>, IControllerContext, ITimelineController> _timeline;
		Func<IBranchController, IControllerContext, IChangeStreamController> _changeStream;
		Func<ITextEditorView, ITextEditor> _textEditorBridge;
		Func<IEnumerable<IAuthor>, IControllerContext, IAuthorController> _author;
		Func<IBus, IErrorOccuredDialog, SimpleErrorReporter> _simpleReporter;
		Func<ISettings, IDictionary<string, string>, ISettings> _settings;
		Func<IController, IControllerContext, IApplicationPluginAdapter> _applicationPluginAdapter;
		Func<IDocumentController, IControllerContext, IDocumentPluginAdapter> _documentPluginAdapter;
		Func<IBranchController, IControllerContext, IBranchPluginAdapter> _branchPluginAdapter;
		Func<ITextEditorController, IControllerContext, IEditorPluginAdapter> _editorPluginAdapter;
		Func<IDocumentPluginAdapter, IBranchPluginAdapter, ISearchMatchItem, ISearchResultPluginAdapter> _searchResultPluginAdapter;

		#region ctor
		public ControllerFactories(
			Func<IController, IDocumentController> document,
			Func<IDocumentController, IBranch, IControllerContext, IBranchController> branch,
			Func<IDocumentController, IBranchController, IControllerContext, ITextEditorController> textEditor,
			Func<IDocumentController, IBranchController, ITextEditorController, IControllerContext, ISpellcheckController> spellchecker,
			Func<IBranchController, IList<IUnitOfTime>, IControllerContext, ITimelineController> timeline,
			Func<IBranchController, IControllerContext, IChangeStreamController> changeStream,
			Func<ITextEditorView, ITextEditor> textEditorBridge,
			Func<IEnumerable<IAuthor>, IControllerContext, IAuthorController> author,
			Func<IBus, IErrorOccuredDialog, SimpleErrorReporter> simpleReporter,
			Func<ISettings, IDictionary<string, string>, ISettings> settings,
			Func<IController, IControllerContext, IApplicationPluginAdapter> applicationPluginAdapter,
			Func<IDocumentController, IControllerContext, IDocumentPluginAdapter> documentPluginAdapter,
			Func<IBranchController, IControllerContext, IBranchPluginAdapter> branchPluginAdapter,
			Func<ITextEditorController, IControllerContext, IEditorPluginAdapter> editorPluginAdapter,
			Func<IDocumentPluginAdapter, IBranchPluginAdapter, ISearchMatchItem, ISearchResultPluginAdapter> searchResultPluginAdapter
		) {
			_document = document;
			_branch = branch;
			_textEditor = textEditor;
			_spellchecker = spellchecker;
			_timeline = timeline;
			_changeStream = changeStream;
			_textEditorBridge = textEditorBridge;
			_author = author;
			_simpleReporter = simpleReporter;
			_settings = settings;

			_applicationPluginAdapter = applicationPluginAdapter;
			_documentPluginAdapter = documentPluginAdapter;
			_branchPluginAdapter = branchPluginAdapter;
			_editorPluginAdapter = editorPluginAdapter;
			_searchResultPluginAdapter = searchResultPluginAdapter;
		}
		#endregion

		#region IFactories Members
		public IDocumentController CreateDocumentController(
			IController controller
		) {
			return _document(controller);
		}

		public IBranchController CreateBranchController(
			IDocumentController documentController,
			IBranch branch,
			IControllerContext context
		) {
			return _branch(documentController, branch, context);
		}

		public ITextEditorController CreateTextEditorController(
			IDocumentController documentController,
			IBranchController branchController,
			IControllerContext context
		) {
			return _textEditor(documentController, branchController, context);
		}

		public ISpellcheckController CreateSpellcheckController(
			IDocumentController documentController,
			IBranchController branchController,
			ITextEditorController textEditorController,
			IControllerContext context
		) {
			return _spellchecker(documentController, branchController, textEditorController, context);
		}

		public ITimelineController CreateTimelineController(
			IBranchController branchController,
			IControllerContext context
		) {
			return _timeline(
				branchController,
				new List<IUnitOfTime> {
#if DEBUG
					//new Second(),
#endif
					new Minute(),
					new Minute { Count = 5 },
					new Minute { Count = 10 },
					new Hour(),
					new Day(),
					new Week(),
					new Month(),
					new Year(),
				},
				context
			);
		}

		public IChangeStreamController CreateChangeStreamController(
			IBranchController branchController,
			IControllerContext context
		) {
			return _changeStream(
				branchController,
				context
			);
		}

		public ITextEditor CreateTextEditorBridge(
			ITextEditorView editorView
		) {
			return _textEditorBridge(editorView);
		}

		public IAuthorController CreateAuthorController(
			IEnumerable<IAuthor> authors,
			IControllerContext context
		) {
			return _author(authors, context);
		}

		public IErrorReporter CreateSimpleReporter(IBus bus, IErrorOccuredDialog dialog) {
			return _simpleReporter(bus, dialog);
		}

		public ISettings CreateSettings(ISettings parent, IDictionary<string, string> container) {
			return _settings(parent, container);
		}

		public IApplicationPluginAdapter CreateApplicationPluginAdapter(IController controller, IControllerContext context) {
			return _applicationPluginAdapter(controller, context);
		}

		public IDocumentPluginAdapter CreateDocumentPluginAdapter(
				IDocumentController documentController,
				IControllerContext context
		) {
			return _documentPluginAdapter(documentController, context);
		}

		public IBranchPluginAdapter CreateBranchPluginAdapter(
			IBranchController branchController,
			IControllerContext context
		) {
			return _branchPluginAdapter(branchController, context);
		}

		public IEditorPluginAdapter CreateEditorPluginAdapter(
			ITextEditorController editorController,
				IControllerContext context
		) {
			return _editorPluginAdapter(editorController, context);
		}

		public ISearchResultPluginAdapter CreateSearchResultPluginAdapter(
			IDocumentPluginAdapter document,
			IBranchPluginAdapter branch,
			ISearchMatchItem item
		) {
			return _searchResultPluginAdapter(document, branch, item);
		}
		#endregion
	}
}
