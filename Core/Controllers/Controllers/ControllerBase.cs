﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Context;
using System.Diagnostics;
using System.ComponentModel;
using Naracea.View;
using Naracea.Common.Exceptions;
using Naracea.View.Compounds;
using MemBus;

namespace Naracea.Controller {
	public abstract class ControllerBase : IDisposable {
		List<IDisposable> _disposables = new List<IDisposable>();
#if DEBUG
		StackTrace _stack;
		static int _ctorCount = 0;
		static int _disposeCount = 0;
		static int _finalizeCount = 0;
#endif

		public IControllerContext Context { get; private set; }
		public IBus Bus {
			get {
				return this.Context.Bus;
			}
		}

		public ControllerBase(IControllerContext context) {
			Debug.Assert(context != null);
			this.Context = context;
#if DEBUG
			_stack = new StackTrace(true);
			Debug.WriteLine("Creating controller count={0}", _ctorCount++);
#endif
		}

		public void ReportError(string shortMessage, NaraceaException ex) {
			var dialog = this.Context.View.CreateView<IErrorOccuredDialog>();
			dialog.ShortMessage = shortMessage;
			string detailedMsg = ex.Message;
#if DEBUG
			detailedMsg += Environment.NewLine
				+ ex.ToString()
				+ Environment.NewLine
				+ ex.InnerException != null ? ex.InnerException.ToString() : "No inner exception"
			;
#endif
			dialog.DetailedMessage = detailedMsg;
			dialog.ShowDialog();
		}

		protected void RegisterForDisposal(IDisposable subscription) {
			_disposables.Add(subscription);
		}

		protected void DisposeDisposables() {
			foreach( var subscription in _disposables ) {
				subscription.Dispose();
			}
			_disposables.Clear();
		}

		protected void CheckDisposeState() {
			if( this.IsDisposed ) {
				throw new InvalidOperationException("Using disposed object.");
			}
		}


		#region IDisposable Members
		~ControllerBase() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
#if DEBUG
			Debug.WriteLine("Finalizing controller count={0}", _finalizeCount++);
			Debug.WriteLine(_stack.ToString());
#endif
			Debug.Assert(false, this.GetType().FullName);
		}

		public bool IsDisposed { get; private set; }

		public void Dispose() {
#if DEBUG
			Debug.WriteLine("Disposing controller count={0}", _disposeCount++);
#endif
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !this.IsDisposed ) {
				if( disposing ) {
					// native resources
				}
				// other managed resources which needs to bedisposed
				this.DisposeDisposables();

				this.IsDisposed = true;
			}
		}
		#endregion
	}
}
