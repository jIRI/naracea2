﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;

namespace Naracea.Controller.Requests {
	public class UpdateDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public UpdateDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class RegisterDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public RegisterDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class UnregisterDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public UnregisterDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class CreateBranch : Naracea.Controller.Messages.DocumentMessageBase {
		public CreateBranch(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class CreateEmptyBranch : Naracea.Controller.Messages.DocumentMessageBase {
		public CreateEmptyBranch(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class DeleteBranch : Naracea.Controller.Messages.DocumentMessageBase {
		public IBranchController BranchController { get; private set; }
		public DeleteBranch(IDocumentController documentController, IBranchController branchController)
			: base(documentController) {
			this.BranchController = branchController;
		}
	}

}
