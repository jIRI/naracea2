﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;

namespace Naracea.Controller.Messages {
	public class DocumentMessageBase {
		public IDocumentController DocumentController { get; private set; }
		public DocumentMessageBase(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}


	public class DocumentClosed : DocumentMessageBase {
		public DocumentClosed(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class DocumentSaved : DocumentMessageBase {
		public DocumentSaved(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class DocumentOpened : DocumentMessageBase {
		public DocumentOpened(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class DocumentExported : DocumentMessageBase {
		public DocumentExported(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class BranchOpened : BranchMessageBase {
		public BranchOpened(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchDeleted : BranchMessageBase {
		public BranchDeleted(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchDeleteCancelled : BranchMessageBase {
		public BranchDeleteCancelled(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchActivated : BranchMessageBase {
		public BranchActivated(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchDeactivated : BranchMessageBase {
		public BranchDeactivated(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}

	public class BranchExported : BranchMessageBase {
		public BranchExported(IDocumentController documentController, IBranchController branchController)
			: base(documentController, branchController) {
		}
	}
}
