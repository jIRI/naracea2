﻿using System;
using System.Collections.Generic;
using Naracea.Common;
using Naracea.Core.Repository;
using Naracea.View;

namespace Naracea.Controller {
	public interface IDocumentController : ICanHaveChildren<IBranchController> {
		bool IsDirty { get; }
		string Name { get; set; }
		string Comment { get; set; }

		IEnumerable<IBranchController> Branches { get; }
		IBranchController ActiveBranch { get; set; }
		IAuthorController AuthorController { get; set; }
		IDocumentView View { get; }
		bool HasRepositoryContext { get; }
		string RepositoryPath { get; }
		ISettings Settings { get; }
		bool IsAutosaveEnabled { get; set; }
		bool IsActive { get; }
		bool IsLocked { get; }
		TimeSpan AutosavePeriod { get; set; }
		Naracea.Controller.Controllers.Document.ExportOnSaveMode ExportOnSaveMode { get; set; }

		void Initialize();
		void ChangeRepository(IRepositoryContext repoContext);

		void Open(IRepositoryContext repoContext);
		void Save();
		void Close();
		void Lock();
		void Unlock();

		IBranchController CreateBranch();
		IBranchController CreateEmptyBranch();
		void DeleteBranch(IBranchController branchController);
	}
}
