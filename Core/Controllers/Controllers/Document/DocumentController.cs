﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Spellcheck;
using Naracea.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Naracea.Controller {
	public class DocumentController : ControllerBase, IDocumentController {
		#region Attributes
		IRepositoryContext _repositoryContext;
		IDocument _document;
		IRepository<IDocument> _repository;
		bool _isDirty;
		System.Timers.Timer _autosaveTimer;
		int _lockCounter = 0;
		#endregion

		/// <summary>
		/// Ctor.
		/// Creates view, registers to the parent controller.
		/// </summary>
		/// <param name="reposContext"></param>
		/// <param name="context"></param>
		public DocumentController(
			IController controller
		)
			: base(controller.Context) {
			this.AppController = controller;

			this.View = this.Context.View.CreateView<IDocumentView>();
			this.View.Controller = this;

			_autosaveTimer = new System.Timers.Timer();
			_autosaveTimer.Elapsed += new System.Timers.ElapsedEventHandler(AutosaveTimer_Elapsed);
			//disable autoreset, we will start timer on document saved message
			_autosaveTimer.AutoReset = false;

			this.InstallEventHandlers();
			this.Bus.Publish(new Requests.RegisterDocument(this));
		}

		#region IDocumentController Members
		public ISettings Settings { get; private set; }
		public Controllers.Document.ExportOnSaveMode ExportOnSaveMode { get; set; }

		bool _isAutosaveEnabled = false;
		public bool IsAutosaveEnabled {
			get {
				return _isAutosaveEnabled;
			}
			set {
				this.CheckDisposeState();

				_isAutosaveEnabled = value;
				_autosaveTimer.Interval = this.AutosavePeriod.TotalMilliseconds;
				_autosaveTimer.Enabled = _isAutosaveEnabled;
			}
		}

		public bool IsActive {
			get {
				return this.AppController.ActiveDocument == this;
			}
		}

		TimeSpan _autosavePeriod = new TimeSpan(0, 7, 0);
		public TimeSpan AutosavePeriod {
			get {
				return _autosavePeriod;
			}
			set {
				this.CheckDisposeState();

				_autosavePeriod = value;
				_autosaveTimer.Interval = _autosavePeriod.TotalMilliseconds;
			}
		}

		public string Name {
			get {
				return _document.Name;
			}
			set {
				this.CheckDisposeState();

				_document.Name = value;
				this.View.Name = _document.Name;
				this.Bus.Publish(new Requests.UpdateDocument(this));
			}
		}

		public string Comment {
			get {
				return _document.Comment;
			}
			set {
				this.CheckDisposeState();
				_document.Comment = value;
			}
		}

		List<IBranchController> _branchControllers = new List<IBranchController>();
		public IEnumerable<IBranchController> Branches { get { return _branchControllers; } }

		public IAuthorController AuthorController { get; set; }

		public IDocumentView View { get; private set; }

		public bool HasRepositoryContext { get { return _repositoryContext != null; } }

		public string RepositoryPath {
			get {
				string path = null;
				if (_repositoryContext != null) {
					path = _repositoryContext.Path;
				}
				return path;
			}
		}

		IBranchController _activeBranchController;
		public IBranchController ActiveBranch {
			get {
				return _activeBranchController;
			}
			set {
				this.CheckDisposeState();

				if (_activeBranchController == value) {
					return;
				}
				Debug.Assert(value != null);
				var prevBranch = _activeBranchController;
				_activeBranchController = value;
				_document.CurrentBranch = _activeBranchController.Branch as Naracea.Core.Model.IBranch;
				this.View.ActivateBranch(_activeBranchController.View);
				this.View.Focus();
				if (prevBranch != null && this.Branches.Contains(prevBranch)) {
					this.Bus.Publish(new Messages.BranchDeactivated(this, prevBranch));
				}
				this.Bus.Publish(new Messages.BranchActivated(this, _activeBranchController));
			}
		}

		/// <summary>
		/// Gets dirty status.
		/// Document is dirty when document is dirty, or any of branches is dirty.
		/// </summary>
		public bool IsDirty {
			get {
				return _isDirty || _branchControllers.Any(b => b.IsDirty);
			}
		}

		/// <summary>
		/// Sets up the document controller and all of it's components. 
		/// Until this method is called no branch view or model objects are created.
		/// </summary>
		public void Initialize() {
			this.CreateDocument();
			this.IntializeFromDocument();
			this.View.Name = _document.Name;
			this.View.Open(this.Settings);
			this.MarkAsNotDirty();
			this.Bus.Publish(new Messages.DocumentOpened(this));
		}

		/// <summary>
		/// Opens document.
		/// Loads document from repository, restores all branches and their views.
		/// Only empty document can be open.
		/// </summary>
		/// <param name="repoContext"></param>
		public void Open(IRepositoryContext repoContext) {
			this.CheckDisposeState();

			Debug.Assert(_repository == null);
			Debug.Assert(repoContext != null);

			//store repo context
			_repositoryContext = repoContext;
			//create or load document
			this.CreateDocument();
			this.IntializeFromDocument();
			//update view
			this.View.Name = _document.Name;
			this.View.FullPath = this.RepositoryPath;
			this.View.Open(this.Settings);
			this.Bus.Publish(new Messages.DocumentOpened(this));
		}

		/// <summary>
		/// Changes repository document uses.
		/// Existing repository is closed without saving.
		/// </summary>
		/// <param name="repoContext"></param>
		public void ChangeRepository(IRepositoryContext repoContext) {
			this.CheckDisposeState();

			Debug.Assert(repoContext != null);
			if (_repository != null) {
				_repository.Dispose();
				_repository = null;
			} else {
				//well, there wasn't any repo context, so this is 1st save of the document
				//therefore we need to rename it so the name matches the file 
				//subsequesnt saves won't change the name
				this.Name = System.IO.Path.GetFileNameWithoutExtension(repoContext.Path);
			}
			_repositoryContext = repoContext;
		}

		object _saveLock = new object();
		/// <summary>
		/// Saves the document. 
		/// </summary>
		public void Save() {
			this.CheckDisposeState();

			Debug.Assert(_repositoryContext != null);

			// we need to lock here, because there can be close&save performed while still saving
			lock (_saveLock) {
				if (_repository == null) {
					_repository = this.Context.Core.CreateDocumentRepository(_repositoryContext);
				}

				//must be marked as nondirty prior actual saving, otherwise we will get some weird changes in dirty status
				//remember - it is async, and user can type while saving
				this.MarkAsNotDirty();
				_repository.Save(_document);
				this.View.FullPath = this.RepositoryPath;
				this.Bus.Publish(new Requests.UpdateDocument(this));
				this.Bus.Publish(new Messages.DocumentSaved(this));
			}
		}

		/// <summary>
		/// Closes document.
		/// </summary>
		public void Close() {
			if (this.IsDisposed) {
				return;
			}

			this.Bus.Publish(new Requests.UnregisterDocument(this));

			_autosaveTimer.Dispose();

			int count = _branchControllers.Count;
			int index = 0;
			while (count-- > 0) {
				if (_activeBranchController != _branchControllers[index]) {
					_branchControllers[index].Close();
				} else {
					index++;
				}
			}
			if (_activeBranchController != null) {
				_activeBranchController.Close();
				_activeBranchController = null;
			}

			if (_repository != null) {
				_repository.Dispose();
			}

			if (this.AuthorController != null) {
				this.AuthorController.Close();
			}

			this.Dispose();
			this.View.Close();
			this.View = null;

			this.Bus.Publish(new Messages.DocumentClosed(this));
		}

		public bool IsLocked {
			get { return _lockCounter > 0; }
		}

		public void Lock() {
			this.CheckDisposeState();

			_lockCounter++;
			this.View.IsLocked = this.IsLocked;
		}

		public void Unlock() {
			this.CheckDisposeState();

			_lockCounter--;
			this.View.IsLocked = this.IsLocked;
		}

		public IBranchController CreateEmptyBranch() {
			this.CheckDisposeState();

			var branch = this.Context.Core.BuildEmptyBranch(
				_document,
				this.AuthorController.CurrentAuthor
			);
			var branchController = this.Context.Controller.CreateBranchController(this, branch, this.Context);
			this.RegisterChild(branchController);
			this.ActivateBranch(branchController);
			this.UpdateBranchViewsDeleteFlag();
			this.MarkAsDirty();
			this.Bus.Publish(new Messages.BranchOpened(this, branchController));
			return branchController;
		}

		/// <summary>
		/// Creates branch new branch.
		/// It takes active branch and creates new branch based on it.
		/// Also creates view and registers it self to the document. And since the register operation
		/// activates registered branch, new branch gets activated.
		/// </summary>
		/// <returns></returns>
		public IBranchController CreateBranch() {
			this.CheckDisposeState();

			var sourceBranchController = this.ActiveBranch;
			var branch = this.Context.Core.BuildBranch(
				_document,
				sourceBranchController != null ? sourceBranchController.Branch : null,
				this.AuthorController.CurrentAuthor
			);
			var branchController = this.Context.Controller.CreateBranchController(this, branch, this.Context);
			this.RegisterChild(branchController);
			this.ActivateBranch(branchController);
			this.UpdateBranchViewsDeleteFlag();
			this.MarkAsDirty();
			this.Bus.Publish(new Messages.BranchOpened(this, branchController));
			if (sourceBranchController.Settings.HasValue(Naracea.Common.Settings.SpellcheckLanguage)) {
				this.Bus.Publish(new Requests.SetSpellingLanguage(
					this,
					branchController,
					sourceBranchController.Settings.Get<DictionaryDescriptor>(Naracea.Common.Settings.SpellcheckLanguage)
				));
			}
			return branchController;
		}

		/// <summary>
		/// Deletes given branch from the document
		/// </summary>
		/// <param name="branchController"></param>
		public void DeleteBranch(IBranchController branchController) {
			this.CheckDisposeState();

			if (branchController.Branch.Parent == null) {
				// default branch cannot be deleted
				return;
			}

			var childBranches = from branch in _document.Branches
													where branch.Parent == branchController.Branch
													select branch;
			if (childBranches.Any()) {
				// branches with child branches cannot be deleted
				return;
			}

			// go ahead with deletion
			var branchToDelete = branchController.Branch;
			this.View.RemoveBranch(branchController.View);
			branchController.Close();
			_document.Branches.Remove(branchToDelete);
			this.UpdateBranchViewsDeleteFlag();
			// deletion done
			this.Bus.Publish(new Messages.BranchDeleted(this, branchController));
		}

		/// <summary>
		/// Registers the new branch to the document.
		/// Adds its view to document view also.
		/// </summary>
		/// <param name="branchController"></param>
		public void RegisterChild(IBranchController branchController) {
			this.CheckDisposeState();

			branchController.View.Name = branchController.Branch.Name;
			_branchControllers.Add(branchController);
			this.View.AddBranch(branchController.View);
			//here we have branch in the view and can get its visual position. 
			//since new branches are always added to the end of the branch view collection, we are safe here
			branchController.StoreBranchVisualOrder();
		}

		/// <summary>
		/// Unregisters branch from this document.
		/// Removes also the view.
		/// </summary>
		/// <param name="branchController"></param>
		public void UnregisterChild(IBranchController branchController) {
			this.CheckDisposeState();

			this.View.RemoveBranch(branchController.View);
			_branchControllers.Remove(branchController);
		}
		#endregion

		#region Private methods
		protected IController AppController { get; set; }

		/// <summary>
		/// Creates dpcument based on what is available. When there is repository context, it opens existing
		/// document and reads it, otherwise it uses core document builder to get new doc.
		/// It also initializes author (there is only one now, since multiauthoring is a future).
		/// </summary>
		private void CreateDocument() {
			if (_repositoryContext == null) {
				_document = this.Context.Core.BuildDocument();
			} else {
				_repository = this.Context.Core.CreateDocumentRepository(_repositoryContext);
				_document = _repository.Load();
			}
			///TODO: there is no support for multiuser editing now
			Debug.Assert(_document.Authors.Count == 1);
			this.AuthorController = this.Context.Controller.CreateAuthorController(_document.Authors, this.Context);

			//New document is not dirty. Only when something happens on it it gets dirty.
			//Tha means, that default branch creation doesn't set the dirty flag.
			this.MarkAsNotDirty();
		}

		/// <summary>
		/// Goes thought branches in controlled document and instantiates all branches.
		/// </summary>
		private void IntializeFromDocument() {
			//we need document settings before any branch is created, since branches rely on document settings
			this.Settings = this.Context.Controller.CreateSettings(
				this.Context.Settings,
				_document.Metadata.Data
			);
			//apply settings
			this.RestoreSettings();

			//intialize document branches
			//we go in thos steps here - first create all branches...
			var branchControllers = new List<IBranchController>();
			foreach (var branch in _document.Branches) {
				branchControllers.Add(this.Context.Controller.CreateBranchController(this, branch, this.Context));
			}
			//...then add them to the UI
			//this way we can preserve visual order of branches
			foreach (var branch in branchControllers.OrderBy(b => b.PreferredVisualOrder)) {
				this.RegisterChild(branch);
				if (branch.Branch == _document.CurrentBranch) {
					this.ActivateBranch(branch);
				}
			}
			// update delete flag on branches
			this.UpdateBranchViewsDeleteFlag();
		}

		/// <summary>
		/// Installs event handlers
		/// </summary>
		void InstallEventHandlers() {
			//branch activation
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.ActivateBranch>(
				m => this.ActivateBranch(m.BranchController),
				new ShapeToFilter<View.Requests.ActivateBranch>(msg => this == msg.DocumentController)
			));
			//create empty branch
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.CreateEmptyBranch>(
				m => this.CreateEmptyBranch(),
				new ShapeToFilter<View.Requests.CreateEmptyBranch>(msg => this == msg.DocumentController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Requests.CreateEmptyBranch>(
				m => this.CreateEmptyBranch(),
				new ShapeToFilter<Requests.CreateEmptyBranch>(msg => this == msg.DocumentController)
			));
			//create new branch
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.CreateBranch>(
				m => this.CreateBranch(),
				new ShapeToFilter<View.Requests.CreateBranch>(msg => this == msg.DocumentController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Requests.CreateBranch>(
				m => this.CreateBranch(),
				new ShapeToFilter<Requests.CreateBranch>(msg => this == msg.DocumentController)
			));

			// document activation
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentActivated>(
				m => {
					//when document is created, we get DocumentActivated before there are any branches.
					if (this.ActiveBranch != null) {
						//re-activate active branch hwne document gets back its focus.
						this.Bus.Publish(new Messages.BranchActivated(this, this.ActiveBranch));
					}
				},
				new ShapeToFilter<Messages.DocumentActivated>(msg => msg.DocumentController == this)
			));
			//document deactivation
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentDeactivated>(
				m => {
					//when document is deactivated, deactivate also the active branch
					//it is not really deactivated - it still stays active since there must be one branch active in document
					//but we need to handle some UI changes when documents losts focus.
					this.Bus.Publish(new Messages.BranchDeactivated(this, this.ActiveBranch));
				},
				new ShapeToFilter<Messages.DocumentDeactivated>(msg => msg.DocumentController == this)
			));

			//show properties
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.ShowDocumentProperties>(
				m => this.ShowDocumentProperties(),
				new ShapeToFilter<View.Requests.ShowDocumentProperties>(msg => this == msg.DocumentController)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.CommitDocumentProperties>(
				m => this.CommitDocumentProperties(m.PropertiesView),
				new ShapeToFilter<View.Requests.CommitDocumentProperties>(msg => this == msg.DocumentController)
			));

			// delete branch
			this.RegisterForDisposal(this.Context.Bus.Subscribe<View.Requests.DeleteBranch>(
				m => this.HandleDeleteBranchRequest(m.BranchController),
				new ShapeToFilter<View.Requests.DeleteBranch>(msg => this == msg.DocumentController)
			));

			// branch dirty state changed
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDirty>(
				m => this.MarkAsDirty(),
				new ShapeToFilter<Messages.BranchDirty>(msg => _branchControllers.Contains(msg.BranchController))
			));

			//autosave handling
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaved>(
				m => {
					//when ever document is saved, reset autosave timer
					_autosaveTimer.Stop();
					_autosaveTimer.Start();
				},
				new ShapeToFilter<Messages.DocumentSaved>(msg => this == msg.DocumentController)
			));

			// update branch visual order for this document
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Naracea.View.Requests.RefreshBranchVisualOrder>(
					m => this.MarkAsDirty(),
					new ShapeToFilter<Naracea.View.Requests.RefreshBranchVisualOrder>(msg => this == msg.DocumentController)
			));
		}

		/// <summary>
		/// Marks this document as dirty.
		/// </summary>
		void MarkAsDirty() {
			_isDirty = true;
			this.UpdateViewDirtyStatus();
		}

		/// <summary>
		/// Marks document as not dirty.
		/// </summary>
		void MarkAsNotDirty() {
			_isDirty = false;
			this.UpdateViewDirtyStatus();
		}

		private void UpdateViewDirtyStatus() {
			if (this.View != null) {
				this.View.DirtyStatusChanged(_isDirty);
			}
		}

		void RestoreSettings() {
			if (this.Settings.HasValue(Naracea.Common.Settings.DocumentSavingAutosaveEnabled)) {
				this.IsAutosaveEnabled = this.Settings.Get<bool>(Naracea.Common.Settings.DocumentSavingAutosaveEnabled);
			}
			if (this.Settings.HasValue(Naracea.Common.Settings.DocumentSavingAutosavePeriod)) {
				this.AutosavePeriod = new TimeSpan(0, this.Settings.Get<int>(Naracea.Common.Settings.DocumentSavingAutosavePeriod), 0);
			}
			this.ConvertBoolsToExportOnSaveMode(
				this.Settings.Get<bool>(Naracea.Common.Settings.DocumentSavingExportOnSaveNothing),
				this.Settings.Get<bool>(Naracea.Common.Settings.DocumentSavingExportOnSaveActiveBranch),
				this.Settings.Get<bool>(Naracea.Common.Settings.DocumentSavingExportOnSaveAllTouchedBranches)
			);
		}

		private void StoreSettings() {
			this.Settings.Set(Naracea.Common.Settings.DocumentSavingAutosaveEnabled, this.IsAutosaveEnabled);
			this.Settings.Set(Naracea.Common.Settings.DocumentSavingAutosavePeriod, this.AutosavePeriod.Minutes);
			//reset all exports
			this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveNothing, false);
			this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveActiveBranch, false);
			this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveAllTouchedBranches, false);
			//now store the right one
			switch (this.ExportOnSaveMode) {
				case Controllers.Document.ExportOnSaveMode.Nothing:
					this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveNothing, true);
					break;
				case Controllers.Document.ExportOnSaveMode.ActiveBranch:
					this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveActiveBranch, true);
					break;
				case Controllers.Document.ExportOnSaveMode.AllTouchedBranches:
					this.Settings.Set(Naracea.Common.Settings.DocumentSavingExportOnSaveAllTouchedBranches, true);
					break;
				default:
					Debug.Fail("Unexpected ExportOnSave value.");
					break;
			}
		}

		private void ConvertBoolsToExportOnSaveMode(bool nothing, bool activeBranch, bool allBranches) {
			if (activeBranch) {
				this.ExportOnSaveMode = Controllers.Document.ExportOnSaveMode.ActiveBranch;
			} else if (allBranches) {
				this.ExportOnSaveMode = Controllers.Document.ExportOnSaveMode.AllTouchedBranches;
			} else {
				//falback, handles also nothing
				this.ExportOnSaveMode = Controllers.Document.ExportOnSaveMode.Nothing;
			}
		}

		void AutosaveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {
			if (this.IsDirty && this.HasRepositoryContext) {
				this.Bus.Publish(new Requests.SaveDocument(this));
			}
			_autosaveTimer.Start();
		}
		#endregion

		#region Event handlers
		void ActivateBranch(IBranchController branchController) {
			Debug.Assert(branchController != null);
			if (this.ActiveBranch != branchController) {
				Task.Factory.StartNew(() => this.ActiveBranch = branchController);
			}
		}

		void ShowDocumentProperties() {
			var view = this.Context.View.CreateView<IDocumentPropertiesView>();
			view.Name = _document.Name;
			view.Comment = _document.Comment;
			view.IsAutosaveEnabled = this.IsAutosaveEnabled;
			switch (this.ExportOnSaveMode) {
				case Controllers.Document.ExportOnSaveMode.Nothing:
					view.ExportOnSaveNothing = true;
					break;
				case Controllers.Document.ExportOnSaveMode.ActiveBranch:
					view.ExportOnSaveActiveBranch = true;
					break;
				case Controllers.Document.ExportOnSaveMode.AllTouchedBranches:
					view.ExportOnSaveAllBranches = true;
					break;
				default:
					Debug.Fail("Unexpected ExportOnSave value.");
					break;
			}
			view.Open(this.Settings);
		}

		void CommitDocumentProperties(IDocumentPropertiesView propertiesView) {
			this.Name = propertiesView.Name;
			_document.Comment = propertiesView.Comment;
			this.IsAutosaveEnabled = propertiesView.IsAutosaveEnabled;
			this.ConvertBoolsToExportOnSaveMode(
				propertiesView.ExportOnSaveNothing,
				propertiesView.ExportOnSaveActiveBranch,
				propertiesView.ExportOnSaveAllBranches
			);
			this.StoreSettings();
			this.MarkAsDirty();
			propertiesView.Close();
			this.Context.View.ApplicationView.UpdateDocument(this.View);
		}

		private void HandleDeleteBranchRequest(IBranchController branchController) {
			Task.Factory.StartNew(() => {
				var dialog = this.Context.View.CreateView<IConfirmBranchDeletionDialog>();
				dialog.OkSelected += (s, e) => this.DeleteBranch(branchController);
				dialog.CancelSelected += (s, e) => this.Bus.Publish(new Messages.BranchDeleteCancelled(this, branchController));
				dialog.ShowDialog();
			});
		}

		private void UpdateBranchViewsDeleteFlag() {
			foreach (var branchController in this.Branches) {
				if (branchController.Branch.Parent != null) {
					var childBranches = from branch in _document.Branches
															where branch.Parent == branchController.Branch
															select branch;
					if (childBranches.Any()) {
						//mark branch with children as nondeletable
						branchController.View.CanBeDeleted = false;
					} else {
						branchController.View.CanBeDeleted = true;
					}
				} else {
					//mark default branch as nondeletable
					branchController.View.CanBeDeleted = false;
				}
			}
		}
		#endregion
	}
}
