﻿using System.Collections.Generic;
using MemBus;
using Naracea.Common;
using Naracea.Controller.Plugins;
using Naracea.View;

namespace Naracea.Controller {
	public interface IController {
		IApplicationView View { get; }
		IFindReplaceTextView FindTextView { get; }
		IHostPluginAdapter PluginHost { get; }

		IControllerContext Context { get; }
		IEnumerable<IDocumentController> Documents { get; }
		IDocumentController ActiveDocument { get; set; }
		ISettings Settings { get; }

		bool AreFileOperationsLocked { get; }
		void LockFileOperations();
		void UnlockFileOperations();

		IBus Bus { get; }

		void Close();
	}
}
