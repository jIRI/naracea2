﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;

namespace Naracea.Controller.Messages {
	public class Error {
		public string Message { get; private set; }
		public string Details { get; private set; }

		public Error(string message, string details) {
			this.Message = message;
			this.Details = details;
		}

		public Error(string message, NaraceaException exception) {
			this.Message = message;
			this.Details = exception.Message
#if DEBUG
					+ Environment.NewLine
					+ exception.ToString()
					+ Environment.NewLine
					+ ( exception.InnerException != null ? exception.InnerException.ToString() : "No inner exception" )
#endif
			;
		}
	}
	
	public class Started {
	}

	public class StartFailed {
	}

	public class Stopped {
	}

	public class StopCancelled {
	}
	
	public class DocumentOpenFailed {
		public DocumentOpenFailed() {
		}
	}

	public class DocumentCloseCancelled {
		public IDocumentController DocumentController { get; private set; }
		public DocumentCloseCancelled(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}
	
	public class DocumentSaveCancelled {
		public IDocumentController DocumentController { get; private set; }
		public DocumentSaveCancelled(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}
	
	public class DocumentSaveFailed {
		public IDocumentController DocumentController { get; private set; }

		public DocumentSaveFailed(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}

	public class DocumentSaveCompleted {
		public IDocumentController DocumentController { get; private set; }

		public DocumentSaveCompleted(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}

	public class DocumentActivated {
		public IDocumentController DocumentController { get; private set; }
		public DocumentActivated(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}

	public class DocumentDeactivated {
		public IDocumentController DocumentController { get; private set; }
		public DocumentDeactivated(IDocumentController documentController) {
			this.DocumentController = documentController;
		}
	}
}
