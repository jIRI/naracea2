﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Finders.Text;
using Naracea.View.Compounds;

namespace Naracea.Controller.Requests {
	public class Start {
		public string[] Args { get; private set; }
		public Start(string[] args) {
			this.Args = args;
		}
	}

	public class SecondInstanceStartAttempt {
		public IList<string> Args { get; private set; }
		public SecondInstanceStartAttempt(IList<string> args) {
			this.Args = args;
		}
	}

	public class Stop {
		public Stop() {
		}
	}

	public class OpenDocumentOrDocuments {
		public OpenDocumentOrDocuments() {
		}
	}


	public class OpenDocument {
		public string FileName { get; private set; }
		public OpenDocument(string path) {
			this.FileName = path;
		}
	}

	public class OpenDocuments {
		public IEnumerable<string> Paths { get; private set; }
		public OpenDocuments(IEnumerable<string> paths) {
			this.Paths = paths;
		}
	}

	public class CloseDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public CloseDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class SaveDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public SaveDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class SaveAsDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public SaveAsDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class PrintDocument : Naracea.Controller.Messages.DocumentMessageBase {
		public PrintDocument(IDocumentController documentController)
			: base(documentController) {
		}
	}

	public class SaveAllDocuments {
	}

	public class CreateDocument {
	}

	public class ClearAllFindResults {
	}

	public class FindAll {
		public TextSearchOptions Options { get; private set; }
		public string Pattern { get; private set; }
		public FindAll(TextSearchOptions options, string pattern) {
			this.Options = options;
			this.Pattern = pattern;
		}
	}

	public class FindAllHistory : FindAll {
		public FindAllHistory(TextSearchOptions options, string pattern)
			: base(options, pattern) {
		}
	}

	public class ReplaceSearchResult {
		public Naracea.View.ISearchMatchItem Item { get; private set; }
		public TextSearchOptions Options { get; private set; }
		public string ReplacePattern { get; private set; }

		public ReplaceSearchResult(Naracea.View.ISearchMatchItem item, string replacePattern, TextSearchOptions options) {
			this.Item = item;
			this.Options = options;
			this.ReplacePattern = replacePattern;
		}
	}
}
