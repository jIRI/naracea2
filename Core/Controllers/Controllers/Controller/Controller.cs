﻿using Naracea.Common;
using Naracea.Controller.Actions;
using Naracea.Controller.ErrorReporters;
using Naracea.Controller.Plugins;
using Naracea.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Naracea.Controller {
	public class Controller : ControllerBase, IController {
		#region Attributes
		List<IDocumentController> _documents = new List<IDocumentController>();
		List<IDocumentController> _documentsToBeClosed = new List<IDocumentController>();
		ControllerAction _createNewDocument;
		ControllerAction _openDocumentOrDocuments;
		ControllerAction _openDocument;
		ControllerAction _openDocuments;
		ControllerAction _printDocument;
		ControllerAction _saveDocument;
		ControllerAction _saveAsDocument;
		ControllerAction _saveAllDocuments;
		ControllerAction _closeDocument;
		ControllerAction _exit;
		ControllerAction _start;
		ControllerAction _secondInstanceStartAttempt;
		ControllerAction _updateMRU;
		ControllerAction _findText;
		ControllerAction _export;
		ControllerAction _showHelp;
		ControllerAction _installLanguage;
		ControllerAction _checkUpdate;
		IErrorReporter _errorReporter;
		PluginHost _pluginHost;
		object _fileOperationsLock = new object();
		int _fileOperationsLockCount = 0;
		#endregion

		#region ctor
		public Controller(IControllerContext context)
			: base(context) {
			//create settings
			this.Settings = this.Context.Controller.CreateSettings(this.Context.Settings, this.Context.Settings.Data);

			// load plugins - this needs to be first thing to do, because we are expecting plugin host to be initialized later during actions intialization
			_pluginHost = new Plugins.PluginHost(this, this.Context);
			this.Context.LoadPlugins(_pluginHost);
			_pluginHost.ResolveAdditionalDependencies(this.Context);
			this.RegisterForDisposal(_pluginHost);

			//well, we do some new-ing here, but that's because actions are tightly coupled with the controller anyway...
			this.RegisterForDisposal(_createNewDocument = new Naracea.Controller.Actions.CreateDocument(this));
			this.RegisterForDisposal(_openDocumentOrDocuments = new Naracea.Controller.Actions.OpenDocumentOrDocuments(this));
			this.RegisterForDisposal(_openDocument = new Naracea.Controller.Actions.OpenDocument(this));
			this.RegisterForDisposal(_openDocuments = new Naracea.Controller.Actions.OpenDocuments(this));
			this.RegisterForDisposal(_printDocument = new Naracea.Controller.Actions.PrintDocument(this));
			this.RegisterForDisposal(_saveDocument = new Naracea.Controller.Actions.SaveDocument(this));
			this.RegisterForDisposal(_saveAsDocument = new Naracea.Controller.Actions.SaveAsDocument(this));
			this.RegisterForDisposal(_saveAllDocuments = new Naracea.Controller.Actions.SaveAllDocuments(this));
			this.RegisterForDisposal(_closeDocument = new Naracea.Controller.Actions.CloseDocument(this));
			this.RegisterForDisposal(_exit = new Naracea.Controller.Actions.Stop(this));
			this.RegisterForDisposal(_start = new Naracea.Controller.Actions.Start(this));
			this.RegisterForDisposal(_secondInstanceStartAttempt = new Naracea.Controller.Actions.HandleSecondInstanceStartAttempt(this));
			this.RegisterForDisposal(_updateMRU = new Naracea.Controller.Actions.UpdateMRUList(this));
			this.RegisterForDisposal(_findText = new Naracea.Controller.Actions.FindReplaceText(this));
			this.RegisterForDisposal(_export = new Naracea.Controller.Actions.ExportDocument(this));
			this.RegisterForDisposal(_showHelp = new Naracea.Controller.Actions.ShowHelp(this));
			this.RegisterForDisposal(_installLanguage = new Naracea.Controller.Actions.Spellcheck(this));
			this.RegisterForDisposal(_checkUpdate = new Naracea.Controller.Actions.CheckUpdates(this));

			//error repoter contains some subscriptions
			_errorReporter = this.Context.Controller.CreateSimpleReporter(
				this.Bus,
				this.Context.View.CreateView<IErrorOccuredDialog>()
			);
			this.RegisterForDisposal(_errorReporter);
			this.InstallWorkersHandlers();
			this.InstallApplicationEventHandlers();
		}
		#endregion

		#region IController Members
		bool _areFileOperationLocked;
		public bool AreFileOperationsLocked {
			get {
				return _areFileOperationLocked;
			}
			private set {
				_areFileOperationLocked = value;
				this.View.AreFileOperationsLocked = value;
			}
		}


		public IApplicationView View {
			get { return this.Context.View.ApplicationView; }
		}

		public IFindReplaceTextView FindTextView {
			get { return this.Context.View.FindReplaceTextView; }
		}

		public IHostPluginAdapter PluginHost {
			get { return _pluginHost; }
		}

		public ISettings Settings { get; private set; }

		public IEnumerable<IDocumentController> Documents {
			get {
				return _documents;
			}
		}

		IDocumentController _activeDocument;
		public IDocumentController ActiveDocument {
			get {
				return _activeDocument;
			}
			set {
				if (_activeDocument == value) {
					return;
				}
				var prevDocument = _activeDocument;
				_activeDocument = value;
				if (_activeDocument != null && _activeDocument.View != null) {
					this.View.ActivateDocument(_activeDocument.View);
					_activeDocument.View.Focus();
				}
				if (prevDocument != null) {
					this.Bus.Publish(new Messages.DocumentDeactivated(prevDocument));
				}
				if (_activeDocument != null) {
					this.Bus.Publish(new Messages.DocumentActivated(_activeDocument));
				}
			}
		}

		public void LockFileOperations() {
			lock (_fileOperationsLock) {
				_fileOperationsLockCount++;
				this.AreFileOperationsLocked = true;
			}
		}

		public void UnlockFileOperations() {
			lock (_fileOperationsLock) {
				_fileOperationsLockCount--;
				if (_fileOperationsLockCount == 0) {
					this.AreFileOperationsLocked = false;
				}
			}
		}

		public void Close() {
			this.View.Close();
			this.Context.SaveSettings();
			this.Dispose();
			this.Bus.Publish(new Messages.Stopped());
			try {
				this.Context.Application.Dispatcher.Invoke(new Action(() => {
					this.Context.Application.Shutdown();
				}));
			} catch (System.Threading.Tasks.TaskCanceledException) {
				// do nothing just die. this occurs because task wich runs shutdown gets cancelled.
			}
		}
		#endregion

		#region Handlers
		/// <summary>
		/// Installs background workers event handlers
		/// </summary>
		void InstallWorkersHandlers() {
		}

		/// <summary>
		/// Installs event handlers
		/// </summary>
		void InstallApplicationEventHandlers() {
			if (Thread.CurrentThread != this.Context.Application.Dispatcher.Thread) {
				this.Context.Application.Dispatcher.BeginInvoke(new Action(() => this.Context.Application.Startup += (s, e) => this.Bus.Publish(new Requests.Start(e.Args))));
			} else {
				this.Context.Application.Startup += (s, e) => this.Bus.Publish(new Requests.Start(e.Args));
			}
			//exit
			this.RegisterForDisposal(this.Bus.Subscribe<View.Requests.Exit>(m => {
				//cancel this event, because we will handle closing by ourselves...
				m.Cancel = true;
				if (this.AreFileOperationsLocked) {
					this.Bus.Publish(new Naracea.View.Requests.NotifyUserFileOpsAreLocked());
				} else {
					this.Bus.Publish(new Requests.Stop());
				}
			}));
			//activate document
			this.RegisterForDisposal(this.Bus.Subscribe<View.Requests.ActivateDocument>(
				m => this.ActivateDocument(m.DocumentController)
			));

			this.RegisterForDisposal(this.Bus.Subscribe<Requests.RegisterDocument>(
				m => this.RegisterDocument(m.DocumentController)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Requests.UnregisterDocument>(
				m => this.UnregisterDocument(m.DocumentController)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Requests.UpdateDocument>(
				m => this.UpdateDocument(m.DocumentController)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<View.Requests.NotifyUserFileOpsAreLocked>(m => {
				var dialog = this.Context.View.CreateView<IFileOperationsAreLockedDialog>();
				dialog.ShowDialog();
			}));

		}

		private void UpdateDocument(IDocumentController docController) {
			this.View.UpdateDocument(docController.View);
		}

		private void UnregisterDocument(IDocumentController docController) {
			this.View.RemoveDocument(docController.View);
			_documents.Remove(docController);
		}

		private void RegisterDocument(IDocumentController docController) {
			_documents.Add(docController);
			this.View.AddDocument(docController.View);
			this.View.ActivateDocument(docController.View);
		}

		void ActivateDocument(IDocumentController documentController) {
			Debug.Assert(documentController != null);
			if (this.ActiveDocument != documentController) {
				this.ActiveDocument = documentController;
			}
		}
		#endregion
	}
}
