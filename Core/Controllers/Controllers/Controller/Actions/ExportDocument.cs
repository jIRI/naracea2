﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;
using System.Diagnostics;
using System.IO;
using Naracea.Core.FileSystem;
using Naracea.Plugin.Export;
using System.Threading;

namespace Naracea.Controller.Actions {
	internal class ExportDocument : ControllerAction {
		IDocumentController _documentToExport;
		IStreamBuilder _streamBuilder;

		object _lock = new object();
		public ExportDocument(IController controller)
			: base(controller) {
			_streamBuilder = _controller.Context.Core.CreateStreamBuilder();
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaved>(
				m => {
					this.Start(() => {
						lock( _lock ) {
							_documentToExport = m.DocumentController;
							this.DoWork();
						}
					});
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ExportCurrentBranch>(
				m => {
					this.Start(() => {
						lock( _lock ) {
							this.ExportCurrentBranch(m.DocumentController, m.BranchController, _controller.Context.Exporters[m.Format.Id]);
						}
					});
				}
			));
			_subscriptions.Add(_bus.Subscribe<Requests.ExportBranchWithName>(
				m => {
					this.Start(() => {
						lock( _lock ) {
							this.ExportOneBranchWithName(m.DocumentController, m.BranchController, m.FileName, m.ExportPlugin.CreateExporter());
						}
					});
				}
			));
		}

		protected void DoWork() {
			Debug.Assert(_documentToExport != null);
			if( _documentToExport == null ) {
				return;
			}
			bool exportSuccessful = true;
			var exportedDoc = _documentToExport;
			switch( _documentToExport.ExportOnSaveMode ) {
				case Controllers.Document.ExportOnSaveMode.ActiveBranch:
					exportSuccessful = this.ExportOneBranch(exportedDoc, _documentToExport.ActiveBranch);
					break;
				case Controllers.Document.ExportOnSaveMode.AllTouchedBranches:
					var branches = _documentToExport.Branches.ToArray();
					foreach( var branch in branches ) {
						if( branch.IsActivated ) {
							exportSuccessful &= this.ExportOneBranch(exportedDoc, branch);
						}
					}
					break;
				case Controllers.Document.ExportOnSaveMode.Nothing:
					//do nothing here, but we still might have branches with forced export
					break;
				default:
					Debug.Fail("Unsupported export mode.");
					exportSuccessful = false;
					break;
			}

			// handle "always exporting" branches
			// activation might be needed
			var document = _controller.ActiveDocument;
			using( var branchActivatedEvent = new AutoResetEvent(false) ) {
				var activeBranch = document.ActiveBranch;
				foreach( var branch in _documentToExport.Branches.Where(b => b.IsAlwaysExporting).ToArray() ) {
					if( !branch.IsActivated ) {
						var subscription = _bus.Subscribe<Messages.BranchShiftingFinished>(m => {
							if( m.BranchController == branch ) { branchActivatedEvent.Set(); }
						});

						using( subscription ) {
							document.ActiveBranch = branch;
							branchActivatedEvent.WaitOne();
						}
					}
					exportSuccessful &= this.ExportOneBranch(exportedDoc, branch);
				}
				if( document.ActiveBranch != activeBranch ) {
					document.ActiveBranch = activeBranch;
				}
			}

			if( exportSuccessful ) {
				_bus.Publish(new Messages.DocumentExported(exportedDoc));
			}
			_documentToExport = null;
		}

		bool ExportOneBranch(IDocumentController document, IBranchController branch) {
			string branchFileName = branch.ExportFileName;
			string text = branch.Editor.Text;
			string exportedBranchFileName = this.CreateExportedFilePath(branchFileName);
			return ExportOneBranchWithName(document, branch, exportedBranchFileName, branch.Exporter.CreateExporter());
		}

		bool ExportOneBranchWithName(IDocumentController document, IBranchController branch, string exportedBranchFileName, IExporter exporter) {
			var progressIndicator = this.OpenProgressIndicator(exportedBranchFileName);
			_controller.LockFileOperations();
			bool exportSuccessful = false;
			try {
				//lock here on stream builder to prevent overwriting file which still/already in use
				lock( _streamBuilder ) {
					var fileChecker = _controller.Context.Core.CreateFileChecker();
					//check whether target file already exists
					if( fileChecker.Exists(exportedBranchFileName) ) {
						//it does, so remove it...
						var fileRemover = _controller.Context.Core.CreateFileRemover();
						fileRemover.Remove(exportedBranchFileName);
					}

					using( var stream = _streamBuilder.Create(exportedBranchFileName, FileMode.CreateNew) ) {
						exporter.Export(stream, branch.Editor.Text);
					}
					exportSuccessful = true;
				}
				_bus.Publish(new Messages.BranchExported(document, branch));
			} catch( NaraceaException ex ) {
                _bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileExporting, ex));
			}
			progressIndicator.Close();
			_controller.UnlockFileOperations();
			return exportSuccessful;
		}

		void ExportCurrentBranch(IDocumentController documentController, IBranchController branchControler, IExportPlugin exporter) {
			var dialog = exporter.CreateDialog();
			string fileName = null;
			dialog.SuggestedFilename = Path.GetFileNameWithoutExtension(branchControler.ExportFileName);
			dialog.FileSelected += (s, e) => fileName = e.Filename;
			dialog.ShowDialog();

			if( fileName != null ) {
				this.ExportOneBranchWithName(documentController, branchControler, fileName, exporter.CreateExporter());
			}
		}

		string CreateExportedFilePath(string branchFileName) {
			return Path.Combine(Path.GetDirectoryName(_documentToExport.RepositoryPath), branchFileName);
		}

		IProgressIndicator OpenProgressIndicator(string fileName) {
			var progressIndicator = _controller.Context.View.CreateView<IProgressIndicator>();
            progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.ExportingDocumentShort;
			var fileNameShortener = _controller.Context.Core.CreateFileNameShortener();
			progressIndicator.DetailedMessage = fileNameShortener.Shorten(fileName, progressIndicator.DetailedMessageCapacity);
			progressIndicator.Open(_controller.Context.View.ApplicationView.ProgressIndicators);
			return progressIndicator;
		}

	}
}
