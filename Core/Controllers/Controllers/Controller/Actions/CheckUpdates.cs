﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using Naracea.Core.Update;
using System.Timers;

namespace Naracea.Controller.Actions {
	internal class CheckUpdates : ControllerAction {
		Version _availableVersion = null;
		Version _ignoreVersion = null;
		IUpdateChecker _updater = null;
		View.Components.IUpdateAvailableIndicator _indicator = null;
		// check for updates once a day
		Timer _updateCheckTimer = new Timer(24 * 60 * 60 * 1000);

		public CheckUpdates(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Messages.Started>(m => this.DoWork()));
			_subscriptions.Add(_bus.Subscribe<Messages.Stopped>(m => {
				if( _updater != null ) {
					_updater.Abort();
				}
			}));
			_updateCheckTimer.Elapsed += (s, e) => this.DoWork();
		}

		protected async void DoWork() {
			_updateCheckTimer.Stop();
			if( _updater == null ) {
				_updater = _controller.Context.Core.CreateUpdater();
			}

			if( _controller.Context.CurrentVersion.IsVersionValid ) {
				bool hasUpdate = await this.DoCheckUpdates();
				if( !hasUpdate || _availableVersion.CompareTo(_ignoreVersion) == 0 ) {
					_updateCheckTimer.Start();
					return;
				}

				// we ignore all other checks resulting in this version. user either sees the indicator, or clicked for download, or ignored the version.
				// anyway we do not need to show it any more
				_ignoreVersion = _availableVersion;

				if( _indicator == null ) {
					_indicator = _controller.Context.View.CreateView<View.Components.IUpdateAvailableIndicator>();
					_indicator.IgnoreRequested += (s, e) => {
						_indicator.Close();
						_indicator = null;
					};
					_indicator.UpdateRequested += (s, e) => {
						Process.Start(new ProcessStartInfo("http://naracea.com/download?from=" + _controller.Context.CurrentVersion.Version.ToString()));
						_indicator.Close();
						_indicator = null;
					};
					_indicator.Open(_controller.Context.View.ApplicationView.ProgressIndicators);
				}
				// set the version, this handles situation when new version appears while indicator is being shown
				_indicator.VersionInfo = _availableVersion.ToString();
				_updateCheckTimer.Start();
			}
		}

		#region Private methods
		private System.Threading.Tasks.Task<bool> DoCheckUpdates() {
			return Task.Run(() => _updater.Check(_controller.Context.CurrentVersion.Version, out _availableVersion));
		}
		#endregion
	}
}
