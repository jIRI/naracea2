﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Naracea.Common.Extensions;
using Naracea.Common.Exceptions;
using Naracea.View;
using Naracea.View.Compounds;
using System.Diagnostics;
using MemBus;

namespace Naracea.Controller.Actions {
	internal abstract class ControllerAction : ActionBase {
		protected IController _controller;
		public ControllerAction(IController controller) 
			: base(controller.Bus) {
			_controller = controller;
		}
	}
}
