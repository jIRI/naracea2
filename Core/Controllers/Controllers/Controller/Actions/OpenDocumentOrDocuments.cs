﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;

namespace Naracea.Controller.Actions {
	internal class OpenDocumentOrDocuments : ControllerAction {
		public OpenDocumentOrDocuments(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.OpenDocumentOrDocuments>(m => this.DoWork()));
			_subscriptions.Add(_bus.Subscribe<View.Requests.OpenDocumentOrDocuments>(m => this.DoWork()));
		}

		protected void DoWork() {
			var dialog = _controller.Context.View.CreateView<IOpenDocumentDialog>();
			dialog.FileSelected += (s, e) => _controller.Bus.Publish(new Requests.OpenDocument(e.Filename));
			dialog.FilesSelected += (s, e) => _controller.Bus.Publish(new Requests.OpenDocuments(e.Filenames));
			dialog.ShowDialog();
		}
	}
}
