﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.View.Compounds;

namespace Naracea.Controller.Actions {
	internal class SaveAsDocument : ControllerAction {
		IDocumentController _documentToSave;

		public SaveAsDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.SaveAsDocument>(
				m => {
					_documentToSave = m.DocumentController;
					this.Start(this.DoWork);
				}
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.SaveAsDocument>(
				m => {
					_documentToSave = m.DocumentController;
					this.Start(this.DoWork);
				}
			));
		}

		private void DoWork() {
			var dialog = _controller.Context.View.CreateView<ISaveDocumentDialog>();
			string fileName = null;
			dialog.FileSelected += (s, e) => fileName = e.Filename;
			dialog.ShowDialog();

			if( fileName == null ) {
				// cancelled
				_controller.Bus.Publish(new Messages.DocumentSaveCancelled(_documentToSave));
				return;
			}

			_controller.LockFileOperations();
			try {
				//change doc's repository
				var repoContext = _controller.Context.Core.CreateRepositoryContext();
				repoContext.Path = fileName;
				_documentToSave.ChangeRepository(repoContext);
				
				var fileChecker = _controller.Context.Core.CreateFileChecker();
				//check whether target file already exists
				if( fileChecker.Exists(_documentToSave.RepositoryPath) ) {
					//it does, so remove it...
					var fileRemover = _controller.Context.Core.CreateFileRemover();
					fileRemover.Remove(_documentToSave.RepositoryPath);
				}
				// use normal save to save the document
				_bus.Publish(new Requests.SaveDocument(_documentToSave));
			} catch( NaraceaException ex ) {
                _bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileSaving, ex));
				_bus.Publish(new Messages.DocumentSaveFailed(_documentToSave));
			}
			_controller.UnlockFileOperations();
		}
	}
}
