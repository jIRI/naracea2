﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;
using System.Diagnostics;
using System.Threading.Tasks;
using MemBus.Subscribing;

namespace Naracea.Controller.Actions {
	internal class SaveAllDocuments : ControllerAction {
		IDocumentController _currentlySaving;
		IEnumerator<IDocumentController> _documentsToSave;

		public SaveAllDocuments(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.SaveAllDocuments>(
				m => {
					this.Start(this.DoWork);
				}
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.SaveAllDocuments>(
				m => {
					this.Start(this.DoWork);
				}
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveCancelled>(
				m => {
					this.ResetSaveAll();
				},
				new ShapeToFilter<Messages.DocumentSaveCancelled>(msg => msg.DocumentController == _currentlySaving)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveFailed>(
				m => {
					this.ResetSaveAll();
				},
				new ShapeToFilter<Messages.DocumentSaveFailed>(msg => msg.DocumentController == _currentlySaving)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveCompleted>(
				m => {
					this.Start(this.SaveNext);
				},
				new ShapeToFilter<Messages.DocumentSaveCompleted>(msg => this.IsDocumentPending())
			));
		}

		protected void DoWork() {
			_documentsToSave = this.GetDocumentsToSave().GetEnumerator();
			this.SaveNext();
		}

		IEnumerable<IDocumentController> GetDocumentsToSave() {
			foreach( var doc in _controller.Documents ) {
				yield return doc;
			}
		}

		void SaveNext() {
			if( _documentsToSave == null ) {
				return;
			}

			if( _documentsToSave.MoveNext() ) {
				_currentlySaving = _documentsToSave.Current;
				_bus.Publish(new Requests.SaveDocument(_currentlySaving));
			} else {
				this.ResetSaveAll();
			}
		}

		bool IsDocumentPending() {
			return _documentsToSave != null && _currentlySaving != null;
		}


		void ResetSaveAll() {
			_documentsToSave = null;
			_currentlySaving = null;
		}
	}
}
