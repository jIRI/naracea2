﻿using System.Collections.Generic;
using System.Linq;

namespace Naracea.Controller.Actions {
	internal class Start : ControllerAction {
		bool _starting = false;
		string[] _args;
		bool _hasAutodoc;

		public Start(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.Start>(m => {
				_args = m.Args;
				this.DoWork();
			}));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentOpened>(
				m => {
					if( _starting ) {
						this.OnDocumentOpened();
					} else if( _hasAutodoc ) {
						var doc = _controller.Documents.ElementAt(0);
						if( !doc.HasRepositoryContext && !doc.IsDirty && m.DocumentController.HasRepositoryContext ) {
							//close only nondirty unsaved document, and only if the next document was opened (not created)
							_bus.Publish(new Requests.CloseDocument(doc));
						}
						_hasAutodoc = false;
					}
				}
			));
		}

		protected void DoWork() {
			_starting = true;
			_controller.Context.LoadSettings();
			_controller.View.Open(_controller.Context.Settings);
			//handle app start args
			var validFileNames = new List<string>();
			if( _args != null ) {
				var fileChecker = _controller.Context.Core.CreateFileChecker();
				foreach( var arg in _args ) {
					if( fileChecker.Exists(arg) ) {
						validFileNames.Add(arg);
					}
				}
			}

			if( validFileNames.Count > 0 ) {
				_bus.Publish(new Requests.OpenDocuments(validFileNames));
			} else {
				_bus.Publish(new Requests.CreateDocument());
			}
		}

		void OnDocumentOpened() {
			_starting = false;
			_hasAutodoc = true;
			_bus.Publish(new Messages.Started());
		}
	}
}
