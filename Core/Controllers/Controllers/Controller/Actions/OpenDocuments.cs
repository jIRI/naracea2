﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;
using System.Threading.Tasks;
using MemBus.Subscribing;

namespace Naracea.Controller.Actions {
	internal class OpenDocuments : ControllerAction {
		string[] _paths;
		int _currentPathIndex = -1;

		public OpenDocuments(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.OpenDocuments>(
				m => {
					_paths = m.Paths.ToArray();
					_currentPathIndex = -1;
					this.OpenNextDocument();
				}
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentOpened>(
				m => this.OpenNextDocument(),
				new ShapeToFilter<Messages.DocumentOpened>(msg => 
					_paths != null
					&& _currentPathIndex != -1 
					&& _currentPathIndex < _paths.Length 
					&& msg.DocumentController.RepositoryPath == _paths[_currentPathIndex])
				)
			);
		}

		void OpenNextDocument() {
			if( _paths == null ) {
				return;
			}

			_currentPathIndex++;
			if( _currentPathIndex < _paths.Length ) {
				_bus.Publish(new Requests.OpenDocument(_paths[_currentPathIndex]));
			} else {
				_currentPathIndex = -1;
				_paths = null;
			}
		}
	}
}
