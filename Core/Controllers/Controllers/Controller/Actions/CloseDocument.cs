﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using System.Diagnostics;
using MemBus.Subscribing;

namespace Naracea.Controller.Actions {
	internal class CloseDocument : ControllerAction {
		IDocumentController _documentToClose = null;
		public CloseDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.CloseDocument>(
				m => {
					_documentToClose = m.DocumentController;
					this.Start(() => this.DoWork());
				}
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.CloseDocument>(
				m => {
					_documentToClose = m.DocumentController;
					this.Start(() => this.DoWork());
				}
			));

			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveCompleted>(
				m => this.OnDocumentSaved(),
				new ShapeToFilter<Messages.DocumentSaveCompleted>(msg => msg.DocumentController == _documentToClose)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentExported>(
				m => this.OnDocumentExported(),
				new ShapeToFilter<Messages.DocumentExported>(msg => msg.DocumentController == _documentToClose)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveCancelled>(
				m => this.OnDocumentNotSaved(),
				new ShapeToFilter<Messages.DocumentSaveCancelled>(msg => msg.DocumentController == _documentToClose)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveFailed>(
				m => this.OnDocumentNotSaved(),
				new ShapeToFilter<Messages.DocumentSaveFailed>(msg => msg.DocumentController == _documentToClose)
			));
		}

		protected void DoWork() {
			_controller.View.ActivateDocument(_documentToClose.View);
			var busyIndicator = _controller.View;
			busyIndicator.BeginBusy();
			if( _documentToClose.IsDirty ) {
				bool cancelled = false;
				bool doSave = true;
				var dialog = _controller.Context.View.CreateView<IConfirmCloseDocumentDialog>();
				dialog.CancelSelected += (s, e) => cancelled = true;
				dialog.YesSelected += (s, e) => doSave = true;
				dialog.NoSelected += (s, e) => doSave = false;
				dialog.ShowDialog();

				if( cancelled ) {
					_documentToClose = null;
					this.OnDocumentNotSaved();
					busyIndicator.EndBusy();
					return;
				}
				
				if( doSave ) {
					_bus.Publish(new Requests.SaveDocument(_documentToClose));
				} else {
					this.DoCloseDocument();
				}
			} else {
				this.DoCloseDocument();
			}
			busyIndicator.EndBusy();
		}

		void OnDocumentSaved() {
			if( _documentToClose.ExportOnSaveMode == Controllers.Document.ExportOnSaveMode.Nothing ) {
				this.DoCloseDocument();
			}
		}

		void OnDocumentExported() {
			this.DoCloseDocument();
		}

		void OnDocumentNotSaved() {
			_bus.Publish(new Messages.DocumentCloseCancelled(_documentToClose));
		}

		private void DoCloseDocument() {
			var documents = _controller.Documents.ToList();
			int newActiveIndex = -1;
			if( documents.Count > 1 ) {
				newActiveIndex = documents.IndexOf(_documentToClose) - 1;
				newActiveIndex = newActiveIndex < 0 ? 0 : newActiveIndex;
			}

			documents.Remove(_documentToClose);
			//activate next document
			if( newActiveIndex != -1 ) {
				/// TODO: remove this, we can move it to controller. it can subscribe to doc close event and when it happens automatically update active document
				_controller.ActiveDocument = documents[newActiveIndex];
			} else {
				_controller.ActiveDocument = null;
			}

			//we need temp variable, since closing is basically async
			var doc = _documentToClose;
			_documentToClose = null;
			if( doc != null ) {
				doc.Close();
			}
		}
	}
}
