﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using System.Threading.Tasks;
using MemBus.Subscribing;

namespace Naracea.Controller.Actions {
	internal class Stop : ControllerAction {
		bool _stopping = false;
		ICanBeBusy _busyIndicator;

		public Stop(IController controller)
			: base(controller) {
				_subscriptions.Add(_bus.Subscribe<Requests.Stop>(m => this.Start(this.DoWork)));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentClosed>(
				m => this.Start(() => this.OnDocumentClose()), 
				new ShapeToFilter<Messages.DocumentClosed>(msg => _stopping)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentCloseCancelled>(m => this.Start(() => this.OnDocumentCloseCancelled())));
		}

		protected void DoWork() {
			_stopping = true;
			_busyIndicator = _controller.View;
			_busyIndicator.BeginBusy();
			this.CloseNextDocument();
		}

		void OnDocumentClose() {
			this.CloseNextDocument();
		}

		void OnDocumentCloseCancelled() {
			_stopping = false;
			_bus.Publish(new Messages.StopCancelled());
			if( _busyIndicator != null ) {
				_busyIndicator.EndBusy();
			}
		}

		void CloseNextDocument() {
			if( _controller.Documents.Any() ) {
					_bus.Publish(new Requests.CloseDocument(_controller.Documents.First()));
			} else {
				if( _busyIndicator != null ) {
					_busyIndicator.EndBusy();
				}
				_controller.Close();
			}
		}
	}
}
