﻿using Naracea.View;

namespace Naracea.Controller.Actions {
	internal class ShowHelp : ControllerAction {
		IHelpViewerView _helpView;

		public ShowHelp(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<View.Requests.ShowHelp>(
				m => this.DoWork()
			));
		}


		protected void DoWork() {
			if( _helpView == null ) {
				//view kills itself on app view closing message
				_helpView = _controller.Context.View.CreateView<IHelpViewerView>();
				if( _controller.Context.CurrentVersion.IsVersionValid ) {
					_helpView.SetCurrentVersion(_controller.Context.CurrentVersion.Version);
				}
				var path = "Naracea.Core.Ui.Wpf.Help.en.xaml";
				_helpView.SetContent(path);
			}
			_helpView.Open(_controller.Settings);
		}
	}
}
