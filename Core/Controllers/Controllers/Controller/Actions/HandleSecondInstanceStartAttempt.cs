﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Controller.Actions {
	internal class HandleSecondInstanceStartAttempt : ControllerAction {
		IList<string> _args;

		public HandleSecondInstanceStartAttempt(IController controller) 
		: base(controller)
		{
			_subscriptions.Add(_bus.Subscribe<Requests.SecondInstanceStartAttempt>(m => {
				_args = m.Args;
				this.Start(this.DoWork);
			}));
		}

		protected void DoWork() {
			if( _args.Count > 0 ) {
				// if there is something in the list, remove first item,
				// since it is path to the application's exe.
				_args.RemoveAt(0);
			}
			//handle app start args
			var validFileNames = new List<string>();
			
			var fileChecker = _controller.Context.Core.CreateFileChecker();
			foreach( var arg in _args ) {
				if( fileChecker.Exists(arg) ) {
					validFileNames.Add(arg);
				}
			}
			if( validFileNames.Count > 0 ) {
				_bus.Publish(new Requests.OpenDocuments(validFileNames));
			}
			_controller.View.Show();
		}
	}
}
