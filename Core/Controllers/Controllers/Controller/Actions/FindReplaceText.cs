﻿using MemBus.Subscribing;
using Naracea.Common.Extensions;
using Naracea.Core.Finders.Text;
using Naracea.View;
using Naracea.View.Components;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Naracea.Controller.Actions
{
	internal class FindReplaceText : ControllerAction
	{
		enum FindStates
		{
			TextFind,
			HistorySearchOngoing,
			HistorySearchFinished
		}

		IEnumerator<TextSearchResult> _findNextResults;
		IEnumerator<TextSearchResult> _findPrevResults;
		ITextSurroundingsFinder _surroundingsFinder;
		IDocumentView _lastDocumentView;
		IBranchView _lastBranchView;
		ObservableCollection<ISearchMatchItem> _matches = new ObservableCollection<ISearchMatchItem>();
		Action _actionAfterShiftingFinished;
		FindStates _findState = FindStates.TextFind;
		IDocumentController _currentlySearchedDocumentController;
		IBranchController _currentlySearchedBranchController;
		bool _currentSearchNeedsToCancel = false;
		Action _actionAfterBranchActivationFinished = null;
		NamedSearchType[] _searchTypes = new NamedSearchType[] {
			new NamedSearchType {
				Type = SearchType.Text,
				Text = Naracea.Core.Properties.Resources.TypeText,
				CanFindAll = true,
				CanFind = true,
				CanReplace = true,
			},
			new NamedSearchType {
				Type = SearchType.History,
				Text = Naracea.Core.Properties.Resources.TypeHistory,
				CanFindAll = true,
				CanFind = false,
				CanReplace = false,
			},
		};
		NamedSearchScope[] _searchScopes = new NamedSearchScope[] {
			new NamedSearchScope {
				Scope = SearchScope.CurrentBranch,
				Text = Naracea.Core.Properties.Resources.ScopeCurrentBranch,
			},
			new NamedSearchScope {
				Scope = SearchScope.AllActiveBranches,
				Text = Naracea.Core.Properties.Resources.ScopeAllActiveBranches,
			},
			new NamedSearchScope {
				Scope = SearchScope.AllBranches,
				Text = Naracea.Core.Properties.Resources.ScopeAllBranches,
			},
		};
		object _showHistoryFindResultPadlock = new object();
		const int SurroundingSize = 7;

		public FindReplaceText(IController controller)
			: base(controller)
		{
			_controller.FindTextView.AvailableSearchTypes = _searchTypes;
			_controller.FindTextView.AvailableSearchScopes = _searchScopes;
			_surroundingsFinder = _controller.Context.Core.CreateTextSurroundingFinder();
			_controller.FindTextView.Matches = _matches;
			_controller.PluginHost.ApplicationSearchResults = _matches;
			_subscriptions.Add(_bus.Subscribe<Requests.FindAll>(
				m => this.Start(() =>
				{
					//search from API calls (e.g. scripts), clear all results
					this.ClearAllResults();
					this.DoFindAll(m.Options, m.Pattern);
				})
			));
			_subscriptions.Add(_bus.Subscribe<Requests.FindAllHistory>(
				m => this.Start(() =>
				{
					//search from API calls (e.g. scripts), clear all results
					this.ClearAllResults();
					this.DoFindAllInHistory(m.Options, m.Pattern);
				})
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.FindAll>(
				m => this.StartSearchAll(m)
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.FindNext>(
				m =>
				{
					var coreOptions = this.GetCoreOptionsFromViewOptions(m.Options);
					this.Start(() => this.DoFindNext(coreOptions, m.Options.FindPattern));
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.FindPrevious>(
				m =>
				{
					var coreOptions = this.GetCoreOptionsFromViewOptions(m.Options);
					this.Start(() => this.DoFindPrevious(coreOptions, m.Options.FindPattern));
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ReplaceAll>(
				m =>
				{
					var coreOptions = this.GetCoreOptionsFromViewOptions(m.Options);
					this.Start(() => this.DoReplaceAll(coreOptions, m.Options.FindPattern, m.Options.ReplacePattern));
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ReplaceNext>(
				m =>
				{
					var coreOptions = this.GetCoreOptionsFromViewOptions(m.Options);
					this.Start(() => this.DoReplaceNext(coreOptions, m.Options.FindPattern, m.Options.ReplacePattern));
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.FindResultSelected>(
				m => this.ShowFindResult(m.SelectedItem)
			));
			_subscriptions.Add(_bus.Subscribe<Requests.ReplaceSearchResult>(
				m =>
				{
					this.Replace(
						m.Item,
						m.ReplacePattern,
						m.Options
					);
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ReplaceItem>(
				m =>
				{
					this.Replace(
						m.Item,
						m.Options.ReplacePattern,
						this.GetCoreOptionsFromViewOptions(m.Options)
					);
				}
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Messages.TextEditorTextChanged>(
				m => this.UpdateFindResults(m.Position, m.AddedLength, m.RemovedLength),
				new ShapeToFilter<Naracea.View.Messages.TextEditorTextChanged>(
					msg => msg.AddedLength != 0 || msg.RemovedLength != 0
				)
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ActivateDocument>(
				m =>
				{
					this.ResetFind();
					_lastDocumentView = m.DocumentView;
				},
				new ShapeToFilter<Naracea.View.Requests.ActivateDocument>(msg => msg.DocumentView != _lastDocumentView)
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ActivateBranch>(
				m =>
				{
					this.ResetFind();
					_lastBranchView = m.BranchView;
				},
				new ShapeToFilter<Naracea.View.Requests.ActivateBranch>(msg => msg.BranchView != _lastBranchView)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.BranchActivated>(m =>
			{
				lock (_showHistoryFindResultPadlock)
				{
					if (_actionAfterBranchActivationFinished != null)
					{
						_actionAfterBranchActivationFinished();
						_actionAfterBranchActivationFinished = null;
					}
				}
			}));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.CloseDocument>(
				m => this.HandleDocumentClosing(m.DocumentController)
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ClearAllFindResults>(
				m => this.Start(() => this.ClearAllResults())
			));
			_subscriptions.Add(_bus.Subscribe<Requests.ClearAllFindResults>(
				m => this.Start(() => this.ClearAllResults())
			));
			_subscriptions.Add(_bus.Subscribe<Naracea.View.Requests.ClearCurrentBranchFindResults>(
				m => this.Start(() => this.ClearCurrentBranchResults())
			));
			_subscriptions.Add(_bus.Subscribe<Messages.BranchClosed>(
				m => this.Start(() => this.ClearAllResults())
			));
			_subscriptions.Add(_bus.Subscribe<Messages.BranchShiftingFinished>(
				m =>
				{
					Action action;
					lock (_showHistoryFindResultPadlock)
					{
						action = _actionAfterShiftingFinished;
						_actionAfterShiftingFinished = null;
					}
					action();
				},
				new ShapeToFilter<Messages.BranchShiftingFinished>(msg => _actionAfterShiftingFinished != null)
			));
		}

		#region Properties
		string _currentText;
		object _currentTextPadlock = new Object();
		string CurrentText
		{
			get
			{
				lock (_currentTextPadlock)
				{
					if (_currentText == null
							&& _controller.ActiveDocument != null
							&& _controller.ActiveDocument.ActiveBranch != null
							&& _controller.ActiveDocument.ActiveBranch.Editor != null
					)
					{
						_currentText = _controller.ActiveDocument.ActiveBranch.Editor.Text;
					}
				}
				return _currentText;
			}
		}
		#endregion

		#region Find and replace
		private void StartSearchAll(View.Requests.FindAll message)
		{
			_currentSearchNeedsToCancel = false;
			var coreOptions = this.GetCoreOptionsFromViewOptions(message.Options);
			Action searchAction = null;
			switch (message.Options.Type)
			{
				case SearchType.Text:
					searchAction = () => this.DoFindAll(coreOptions, message.Options.FindPattern);
					break;
				case SearchType.History:
					searchAction = () => this.DoFindAllInHistory(coreOptions, message.Options.FindPattern);
					break;
				default:
					Debug.Fail("Unhandled search type");
					break;
			}

			if (searchAction == null)
			{
				return;
			}

			if (message.Options.ClearResultsOnFindAll)
			{
				this.ClearAllResults();
			}

			switch (message.Options.Scope)
			{
				case SearchScope.CurrentBranch:
					Task.Factory.StartNew(() => this.PerformOnCurrentBranch(searchAction));
					break;
				case SearchScope.AllActiveBranches:
					Task.Factory.StartNew(() => this.PerformOnAllActiveBranches(searchAction));
					break;
				case SearchScope.AllBranches:
					Task.Factory.StartNew(() => this.PerformOnAllBranches(searchAction));
					break;
				default:
					Debug.Fail("Unhandled search scope");
					break;
			}
		}

		private void PerformOnAllBranches(Action searchAction)
		{
			_controller.FindTextView.BeginBusy();

			var activeBranch = _controller.ActiveDocument.ActiveBranch;
			using (var branchActivatedEvent = new AutoResetEvent(false))
			{
				foreach (var branch in _controller.ActiveDocument.Branches)
				{
					var needsWaitForBranchRewind = !branch.IsActivated;
					if (needsWaitForBranchRewind)
					{
						var subscription = _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m =>
						{
							if (m.BranchController == branch) { branchActivatedEvent.Set(); }
						});

						using (subscription)
						{
							_controller.ActiveDocument.ActiveBranch = branch;
							branchActivatedEvent.WaitOne();
						}
					}
					else
					{
						_controller.ActiveDocument.ActiveBranch = branch;
					}

					searchAction();

					if (_currentSearchNeedsToCancel)
					{
						break;
					}
				}
			}
			_controller.ActiveDocument.ActiveBranch = activeBranch;

			_controller.FindTextView.EndBusy();
		}

		private void PerformOnAllActiveBranches(Action searchAction)
		{
			_controller.FindTextView.BeginBusy();

			var activeBranch = _controller.ActiveDocument.ActiveBranch;
			foreach (var branch in _controller.ActiveDocument.Branches.Where(b => b.IsActivated))
			{
				_controller.ActiveDocument.ActiveBranch = branch;
				searchAction();
				if (_currentSearchNeedsToCancel)
				{
					break;
				}
			}
			_controller.ActiveDocument.ActiveBranch = activeBranch;

			_controller.FindTextView.EndBusy();
		}

		private void PerformOnCurrentBranch(Action searchAction)
		{
			_controller.FindTextView.BeginBusy();
			searchAction();
			_controller.FindTextView.EndBusy();
		}

		protected void DoFindAll(TextSearchOptions options, string pattern)
		{
			_findState = FindStates.TextFind;
			this.ResetFind();

			var finder = this.CreateFinder(options);
			var matches = finder.Find(this.CurrentText, pattern, options);
			foreach (var match in matches)
			{
				var item = this.CreateSearchMatchItem(this.CurrentText, match.Position, pattern, match.MatchedText);
				this.AddMatch(item);
			}
		}

		protected void DoFindAllInHistory(TextSearchOptions options, string pattern)
		{
			if (_controller.ActiveDocument == null
				|| _controller.ActiveDocument.ActiveBranch == null
				|| !_controller.ActiveDocument.ActiveBranch.IsActivated
				)
			{
				return;
			}

			_findState = FindStates.HistorySearchOngoing;
			this.ResetFind();

			//lists
			Dictionary<int, HistorySearchMatchItem> historyMatches = new Dictionary<int, HistorySearchMatchItem>();
			List<HistorySearchMatchItem> confirmedHistoryMatches = new List<HistorySearchMatchItem>();

			//intialize data
			_currentlySearchedDocumentController = _controller.ActiveDocument;
			_currentlySearchedBranchController = _currentlySearchedDocumentController.ActiveBranch;
			var currentBranch = _currentlySearchedBranchController.Branch;
			int startingChangeIndex = currentBranch.CurrentChangeIndex;
			int currentChangeIndex = startingChangeIndex;
			//setup text editor proxy so we do not mess with a ctual text editor
			//passing null to the factory cause that no changes are propagated further
			var editorProxy = _controller.Context.Core.CreateTextEditorProxy(null);
			//initialize text
			editorProxy.Text = _currentlySearchedBranchController.Editor.Text;
			//set text changes handling
			editorProxy.ProxiedTextChanged += (s, e) => this.ConfirmHistoryMatches(historyMatches, confirmedHistoryMatches, e.Position, e.AddedLength, e.RemovedLength);

			var finder = this.CreateFinder(options);

			//get progress indicator
			var progressIndicator = this.OpenProgressIndicator(startingChangeIndex);
			int historySearchProgressReportStep = 1 + currentChangeIndex / 100;
			bool cancelled = false;
			progressIndicator.CancelRequested += (s, e) => cancelled = true;

			int matchesCount = 0;
			while (currentChangeIndex >= 0 && !cancelled && !_currentSearchNeedsToCancel)
			{
				var text = editorProxy.Text;
				//find matches
				var matches = finder.Find(text, pattern, options);

				//go though matches and add them in necessary
				foreach (var match in matches)
				{
					if (_currentSearchNeedsToCancel)
					{
						break;
					}

					//now we expect we have items in the list already updated to their new positions,
					//so we can recognize which matches should be added
					if (!historyMatches.ContainsKey(match.Position))
					{
						//create match item
						//this one will get updated when text changes so we can track the text changes
						//this way we will be able to ignore matches which appeared in text at some point in history and survived to the most recent version of the text
						var item = new HistorySearchMatchItem
						{
							Position = match.Position,
							MatchedText = match.MatchedText,
							CurrentChangeIndex = currentChangeIndex,
						};
						//create search match item so we can display it later
						item.SearchMatchItem = this.CreateSearchMatchItem(
							currentBranch.Changes.At(currentChangeIndex).DateTime,
							text,
							item.Position,
							pattern,
							item.MatchedText,
							currentChangeIndex
						);
						//there is no item with the same position on the same change which wasn't overwritten
						//that means we have some new occurence of the pattern, and we need to add it to the list
						historyMatches.Add(item.Position, item);
					}
				}

				if (_currentSearchNeedsToCancel)
				{
					break;
				}

				//rewind current change
				var change = currentBranch.Changes.At(currentChangeIndex);
				change.ExecuteUndo(editorProxy);
				//next change
				currentChangeIndex -= change.ShiftCount;
				if (change.ShouldShifterExecuteFollowingChange)
				{
					currentBranch.Changes.At(currentChangeIndex).ExecuteUndo(editorProxy);
					currentChangeIndex--;
				}

				//report progress
				int progressVal = startingChangeIndex - currentChangeIndex;
				if (progressVal % historySearchProgressReportStep == 0)
				{
					progressIndicator.Value = progressVal;
				}

				int currentMatchCount = confirmedHistoryMatches.Count + historyMatches.Count;
				if (matchesCount != currentMatchCount)
				{
					progressIndicator.DetailedMessage = Naracea.Core.Properties.Resources.MatchesFoundSoFar + currentMatchCount;
					matchesCount = currentMatchCount;
				}
			}

			if (!_currentSearchNeedsToCancel)
			{
				//sort matches
				var orderedMatches = from match in confirmedHistoryMatches.Concat(historyMatches.Values)
														 orderby match.CurrentChangeIndex descending, match.Position ascending
														 select match;
				//add matches to the view
				_controller.FindTextView.Exec(() =>
				{
					foreach (var match in orderedMatches)
					{
						this.AddMatch(match.SearchMatchItem);
					}
				});
			}
			_findState = FindStates.HistorySearchFinished;
			progressIndicator.Close();
		}

		void ConfirmHistoryMatches(Dictionary<int, HistorySearchMatchItem> historyMatches, List<HistorySearchMatchItem> confirmedHistoryMatches, int changePosition, int addedLength, int removedLength)
		{
			//update items
			var currentMatches = historyMatches.Values.OrderBy(it => it.Position).ToList();
			var updates = new List<HistorySearchMatchItem>();
			for (int i = 0; i < currentMatches.Count; )
			{
				var item = currentMatches[i];
				var originalPosition = item.Position;
				if (item.WasOverwritten)
				{
					//ovewritten items should be already removed at this point
					Debug.Fail("Unexpected match in history items.");
					i++;
					continue;
				}

				bool doUpdate = false;
				if (changePosition < item.Position && removedLength <= item.Position - changePosition)
				{
					//this item needs to be updated, change in text happend before it
					//but not a remove affecting the item
					doUpdate = true;
				}
				else if (changePosition < item.Position)
				{
					//remove happend, and it spans in to the match
					item.WasOverwritten = true;
				}
				else if (changePosition == item.Position)
				{
					if (addedLength > 0 && removedLength == 0)
					{
						//there was some text addedLength before the match, but none removed
						//update the match so it moves in the text
						doUpdate = true;
					}
					else
					{
						//text was either removed at match position, or it was replaced
						//it was overwritten anyway
						item.WasOverwritten = true;
					}
				}
				else if (changePosition > item.Position && changePosition < item.Position + item.MatchedText.Length)
				{
					//something was changed inside of the match.
					//overwritten
					item.WasOverwritten = true;
				}

				if (doUpdate)
				{
					//update the position
					item.Position += addedLength - removedLength;
					historyMatches.Remove(originalPosition);
					updates.Add(item);
				}

				if (item.WasOverwritten)
				{
					confirmedHistoryMatches.Add(item);
					historyMatches.Remove(originalPosition);
					currentMatches.Remove(item);
				}
				else
				{
					i++;
				}
			}

			// and add them back updated
			foreach (var item in updates)
			{
				historyMatches.Add(item.Position, item);
			}
		}


		protected ISearchMatchItem DoFindNext(TextSearchOptions options, string pattern)
		{
			_findPrevResults = null;

			if (_findNextResults == null)
			{
				options.Direction = SearchDirection.DownFromPosition;
				options.StartPosition = _controller.ActiveDocument.ActiveBranch.Editor.CaretPosition;
				var currentText = _controller.ActiveDocument.ActiveBranch.Editor.Text;
				var finder = this.CreateFinder(options);
				var matches = finder.Find(currentText, pattern, options);
				_findNextResults = matches.GetEnumerator();
			}

			ISearchMatchItem item = null;
			if (_findNextResults.MoveNext())
			{
				var match = _findNextResults.Current;
				item = this.CreateSearchMatchItem(this.CurrentText, match.Position, pattern, match.MatchedText);
				this.AddMatch(item);
				this.ShowFindResult(item);
			}

			return item;
		}

		protected ISearchMatchItem DoFindPrevious(TextSearchOptions options, string pattern)
		{
			_findNextResults = null;

			if (_findPrevResults == null)
			{
				options.Direction = SearchDirection.UpFromPosition;
				options.StartPosition = _controller.ActiveDocument.ActiveBranch.Editor.CaretPosition;
				var currentText = _controller.ActiveDocument.ActiveBranch.Editor.Text;
				var finder = this.CreateFinder(options);
				var matches = finder.Find(currentText, pattern, options);
				_findPrevResults = matches.GetEnumerator();
			}

			ISearchMatchItem item = null;
			if (_findPrevResults.MoveNext())
			{
				var match = _findPrevResults.Current;
				item = this.CreateSearchMatchItem(this.CurrentText, match.Position, pattern, match.MatchedText);
				this.AddMatch(item);
				this.ShowFindResult(item);
			}

			return item;
		}

		protected void DoReplaceAll(TextSearchOptions options, string pattern, string replaceWith)
		{
			var appBusyIndicator = _controller.View;
			_controller.FindTextView.BeginBusy();
			appBusyIndicator.BeginBusy();

			//clear find next/prev enumerations
			_findNextResults = null;
			_findPrevResults = null;
			this.ClearAllResults();

			if (_currentText != null)
			{
				//reset current text, it might be changed
				lock (_currentTextPadlock)
				{
					_currentText = null;
				}
			}

			var finder = this.CreateFinder(options);
			var matches = from m in finder.Find(this.CurrentText, pattern, options)
										select new
										{
											Position = m.Position,
											Text = m.MatchedText
										};


			if (matches.Any())
			{
				var document = _controller.ActiveDocument;
				var branch = document.ActiveBranch;
				// iterate in reverse order, this way indices are valid
				var editor = branch.Editor;

				// we group all replace changes, so store group begin
				branch.StoreGroupBegin();
				// do replacement
				int replaceWithLen = replaceWith.Length;
				foreach (var match in matches.ToArray().Reverse())
				{
					editor.Select(match.Position, match.Text.Length);
					var replacedText = finder.Replace(branch.Editor.SelectedText, pattern, replaceWith, options);
					editor.ReplaceSelection(replacedText);
				}
				// store end of the group
				branch.StoreGroupEnd();
			}

			appBusyIndicator.EndBusy();
			_controller.FindTextView.EndBusy();
		}

		protected void DoReplaceNext(TextSearchOptions options, string pattern, string replaceWith)
		{
			this.ResetFind();
			this.Replace(this.DoFindNext(options, pattern), replaceWith, options);
		}

		void Replace(ISearchMatchItem item, string replaceWith, TextSearchOptions options)
		{
			if (item == null)
			{
				return;
			}

			this.ShowFindResult(item);
			var branch = _controller.ActiveDocument.ActiveBranch;
			var finder = this.CreateFinder(options);
			string replacedText = null;

			try
			{
				replacedText = finder.Replace(branch.Editor.SelectedText, item.SearchPattern, replaceWith, options);
			}
			catch (Exception)
			{
				// errr, ignore these exceptions here. when processing regexp it is sure thing there will be some...
			}

			if (replacedText != null)
			{
				branch.Editor.ReplaceSelection(replacedText);
				_controller.FindTextView.Exec(() =>
				{
					lock (_matches)
					{
						_matches.Remove(item);
					}
				});
			}
		}
		#endregion

		#region Match items management
		ISearchMatchItem CreateSearchMatchItem(string currentText, int position, string searchPattern, string matchedText)
		{
			return this.CreateSearchMatchItem(
				DateTimeOffset.Now,
				currentText,
				position,
				searchPattern,
				matchedText,
				_controller.ActiveDocument.ActiveBranch.Branch.CurrentChangeIndex
			);
		}

		ISearchMatchItem CreateSearchMatchItem(DateTimeOffset dateTime, string currentText, int position, string searchPattern, string matchedText, int changeIndex)
		{
			ISearchMatchItem item = null;
			_controller.FindTextView.Exec(() =>
			{
				item = _controller.Context.View.CreateSearchMatchItem();
				var foundSurroundings = _surroundingsFinder.Find(currentText, matchedText, position, FindReplaceText.SurroundingSize);
				var formattedText = foundSurroundings.Text;
				item.SearchPattern = searchPattern;
				item.DocumentView = _controller.ActiveDocument.View;
				item.BranchView = _controller.ActiveDocument.ActiveBranch.View;
				item.PositionInText = position;
				item.MatchedText = matchedText;
				item.MatchOffsetInText = foundSurroundings.SubstringBegin;
				item.PositionInPercents = this.PercentOfText(position, currentText.Length);
				item.SurroundedMatchText = formattedText;
				item.CurrentChangeIndex = changeIndex;
				item.IsHistorySearch = _findState == FindStates.HistorySearchOngoing;
				item.DateTime = new DateTime(dateTime.Ticks);
			});
			return item;
		}

		void ShowFindResult(ISearchMatchItem item)
		{
			//activate branch
			_controller.Bus.Publish(new Naracea.View.Requests.ActivateDocument(item.DocumentView));

			bool isFindWindowActivationPending = _controller.ActiveDocument.ActiveBranch.View != item.BranchView;
			lock (_showHistoryFindResultPadlock)
			{
				_actionAfterBranchActivationFinished = () =>
				{
					_controller.FindTextView.Exec(() =>
						_controller.ActiveDocument.ActiveBranch.Editor.Select(item.PositionInText, item.MatchedText.Length)
					);
					_controller.View.ExecAsync(() => _controller.FindTextView.FocusResultList());
				};
			}

			_controller.Bus.Publish(new Naracea.View.Requests.ActivateBranch(item.DocumentView, item.BranchView));

			if (!item.IsHistorySearch || item.CurrentChangeIndex == _controller.ActiveDocument.ActiveBranch.Branch.CurrentChangeIndex)
			{
				if (!isFindWindowActivationPending)
				{
					lock (_showHistoryFindResultPadlock)
					{
						_actionAfterBranchActivationFinished();
						_actionAfterBranchActivationFinished = null;
					}
				}
			}
			else
			{
				lock (_showHistoryFindResultPadlock)
				{
					if (_actionAfterShiftingFinished == null)
					{
						_actionAfterShiftingFinished = () => _controller.FindTextView.Exec(() => _controller.ActiveDocument.ActiveBranch.Editor.Select(item.PositionInText, item.MatchedText.Length));
						_controller.FindTextView.Exec(() =>
							_controller.Bus.Publish(new Requests.ShiftToChange(_controller.ActiveDocument, _controller.ActiveDocument.ActiveBranch, item.CurrentChangeIndex))
						);
					}
				}
			}
		}

		void UpdateFindResults(int position, int addedLength, int removedLength)
		{
			//because of fitering, we know we have matches, and there is something either added or removed.
			this.ResetFind();

			lock (_matches)
			{
				int matchesForActiveBranchCount = 0;
				for (int i = 0; i < _matches.Count; i++)
				{
					var item = _matches[i];
					if (item.IsHistorySearch
						|| item.DocumentView != _controller.ActiveDocument.View
						|| item.BranchView != _controller.ActiveDocument.ActiveBranch.View
					)
					{
						continue;
					}

					matchesForActiveBranchCount++;
					_controller.FindTextView.Exec(() =>
					{
						bool doUpdate = false;
						bool doRemove = false;
						var positionInText = item.PositionInText;
						var matchedText = item.MatchedText;
						var matchOffsetInText = item.MatchOffsetInText;
						var text = item.SurroundedMatchText;
						if (position < positionInText && removedLength <= positionInText - position)
						{
							//this item needs to be updated, change in text happend before it
							//and it wasn't remove which would span into actual match
							doUpdate = true;
						}
						else if (position < positionInText)
						{
							//some text was removed and it affects also the match
							//remove it
							doRemove = true;
						}
						else if (position == positionInText)
						{
							if (addedLength > 0 && removedLength == 0)
							{
								//there was some text addedLength before the match, but none removed
								//it is safe to update the match
								doUpdate = true;
							}
							else if (addedLength == 0 && removedLength > 0)
							{
								//text removed at match position
								//remove the match
								doRemove = true;
							}
							else if (addedLength == removedLength
							&& !this.CurrentText.Substring(position, addedLength).Equals(matchedText)
						)
							{
								//text at the position of the item was replaced with text of the same length
								//but the text changed, remove the match
								doRemove = true;
							}
						}
						else if (position > positionInText && position < positionInText + matchedText.Length)
						{
							//something was changed inside of the match.
							//remove it
							doRemove = true;
						}
						else if (position > positionInText + matchedText.Length && position < positionInText + text.Length)
						{
							//now, there is change behind the text, but it still falls into surrounding
							//do update, 'cause it looks kewl :-)
							doUpdate = true;
						}

						if (doUpdate)
						{
							if (position >= item.PositionInText - item.MatchOffsetInText
								&& position <= item.PositionInText - item.MatchOffsetInText + item.SurroundedMatchText.Length
							)
							{
								var foundSurroundings = _surroundingsFinder.Find(this.CurrentText, item.MatchedText, item.PositionInText, FindReplaceText.SurroundingSize);
								item.SurroundedMatchText = foundSurroundings.Text;
								item.MatchOffsetInText = foundSurroundings.SubstringBegin;
							}
							if (position <= item.PositionInText)
							{
								item.PositionInText += addedLength - removedLength;
								item.PositionInPercents = this.PercentOfText(item.PositionInText, this.CurrentText.Length);
							}
						}
						if (doRemove)
						{
							_matches.RemoveAt(i);
							matchesForActiveBranchCount--;
							//we need to go one item back, since we did remove
							i--;
						}
					});
				}
				_controller.ActiveDocument.ActiveBranch.IsTextChangeReportingEnabled = matchesForActiveBranchCount > 0;
			}
		}

		int PercentOfText(int current, int max)
		{
			return (int)((double)100 * (double)current / (double)max);
		}

		TextSearchOptions GetCoreOptionsFromViewOptions(Naracea.View.ViewFindOptions viewOptions)
		{
			var options = new TextSearchOptions
			{
				MatchCase = viewOptions.MatchCase,
				MatchWholeWords = viewOptions.MatchWholeWords,
				UseRegEx = viewOptions.UseRegex
			};
			return options;
		}

		void ResetFind()
		{
			//clear find next/prev enumerations - once something is typed, we need to re-generate them
			_findNextResults = null;
			_findPrevResults = null;
			if (_currentText != null)
			{
				//reset current text, since it changed
				lock (_currentTextPadlock)
				{
					_currentText = null;
				}
			}
		}

		void AddMatch(ISearchMatchItem item)
		{
			item.BranchView.Controller.As<IBranchController>().IsTextChangeReportingEnabled = true;
			_controller.FindTextView.Exec(() =>
			{
				lock (_matches)
				{
					if (!_matches.Any(i =>
							i.DocumentView == item.DocumentView
							&& i.BranchView == item.BranchView
							&& i.PositionInText == item.PositionInText
							&& i.MatchedText == item.MatchedText
							&& i.CurrentChangeIndex == item.CurrentChangeIndex
						)
					)
					{
						_matches.Add(item);
					}
				}
			});
		}

		void ClearAllResults()
		{
			_controller.FindTextView.Exec(() => _matches.Clear());
		}

		void ClearCurrentBranchResults()
		{
			this.ClearBranchResults(_controller.ActiveDocument.View, _controller.ActiveDocument.ActiveBranch.View);
		}

		void ClearDocumentResult(IDocumentController docController)
		{
			if (_lastDocumentView == docController.View)
			{
				_lastDocumentView = null;
			}
			foreach (var branch in docController.Branches)
			{
				this.ClearBranchResults(docController.View, branch.View);
			}
		}

		void ClearBranchResults(IDocumentView docView, IBranchView branchView)
		{
			if (_lastBranchView == branchView)
			{
				_lastBranchView = null;
			}
			lock (_matches)
			{
				_controller.FindTextView.Exec(() =>
				{
					var currentBranchItems = (from match in _matches
																		where match.DocumentView == docView
																		 && match.BranchView == branchView
																		select match
																	 ).ToArray();
					foreach (var match in currentBranchItems)
					{
						_controller.FindTextView.Exec(() => _matches.Remove(match));
					}
				});
			}
		}

		IProgressIndicator OpenProgressIndicator(int steps)
		{
			var progressIndicator = _controller.Context.View.CreateView<IProgressIndicator>();
			progressIndicator.IsIndeterminate = false;
			progressIndicator.IsCancellable = true;
			progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.SearchingBranch;
			progressIndicator.To = steps;
			progressIndicator.Open(_controller.Context.View.ApplicationView.ProgressIndicators);
			return progressIndicator;
		}

		void HandleDocumentClosing(IDocumentController doc)
		{
			_currentSearchNeedsToCancel = (doc == _currentlySearchedDocumentController);
			this.ClearDocumentResult(doc);
		}

		ITextFinder CreateFinder(TextSearchOptions options)
		{
			if (options.UseRegEx)
			{
				return _controller.Context.Core.CreateRegExTextFinder();
			}
			else
			{
				return _controller.Context.Core.CreatePlainTextFinder();
			}
		}
		#endregion
	}
}
