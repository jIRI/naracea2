﻿using MemBus.Subscribing;
using Naracea.View;
using System;
using System.Collections.Generic;

namespace Naracea.Controller.Actions
{
	internal class UpdateMRUList : ControllerAction
	{
		List<MruItem> _mruList = new List<MruItem>();
		const int MaxNumberOfMruItems = 14;

		public UpdateMRUList(IController controller)
			: base(controller)
		{
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentOpened>(
				m => this.DoWork(m.DocumentController),
				new ShapeToFilter<Messages.DocumentOpened>(msg => msg.DocumentController.RepositoryPath != null)
			));
			_subscriptions.Add(_bus.Subscribe<Messages.DocumentSaveCompleted>(m =>
			{
				this.DoWork(m.DocumentController);
			}));
			_subscriptions.Add(_bus.Subscribe<Messages.Started>(m =>
			{
				this.RestoreMruList();
			}));
		}

		protected void DoWork(IDocumentController documentController)
		{
			string fileName = documentController.RepositoryPath;
			var item = _mruList.Find(i => i.FileName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase));
			if (item != null)
			{
				_mruList.Remove(item);
			}
			else
			{
				item = new MruItem(fileName);
			}
			if (_mruList.Count >= MaxNumberOfMruItems)
			{
				//list is full, remove last item
				_mruList.RemoveAt(_mruList.Count - 1);
			}
			//if there is still space in the list, add the item
			if (_mruList.Count < MaxNumberOfMruItems)
			{
				_mruList.Insert(0, item);
			}
			_controller.View.MruList = _mruList;
			this.StoreMruList();
		}

		private void RestoreMruList()
		{
			if (_controller.Settings.HasValue(Naracea.Common.Settings.MruList))
			{
				try
				{
					_mruList = _controller.Settings.Get<List<MruItem>>(Naracea.Common.Settings.MruList);
				}
				catch (Exception)
				{
					//well, invalid string
					//do nothing about it, just do not change the list
				}
			}
			_controller.View.MruList = _mruList;
		}

		private void StoreMruList()
		{
			_controller.Settings.Set(Naracea.Common.Settings.MruList, _mruList);
		}
	}
}
