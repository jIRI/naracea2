﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;

namespace Naracea.Controller.Actions {
	internal class PrintDocument : ControllerAction {
		public PrintDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<View.Requests.PrintDocument>(
				m => this.DoWork(m.DocumentController)
			));
			_subscriptions.Add(_bus.Subscribe<Requests.PrintDocument>(
				m => this.DoWork(m.DocumentController)
			));
		}

		protected void DoWork(IDocumentController documentController) {
			var dialog = _controller.Context.View.CreateView<IPrintDialog>();
			dialog.DocumentName = string.Format("{0} - {1}", documentController.Name, documentController.ActiveBranch.Name);
			var converter = _controller.Context.Core.CreateMarkupConverter();
			dialog.Xaml = converter.ToXamlDocument(documentController.ActiveBranch.Editor.Text);
			dialog.ShowDialog();
		}
	}
}
