﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO;

namespace Naracea.Controller.Actions {
	internal class SaveDocument : ControllerAction {
		IDocumentController _documentToSave;
		protected Task _task;

		public SaveDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.SaveDocument>(
				m => {
					_documentToSave = m.DocumentController;
					this.Start(this.DoWork);
				}
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.SaveDocument>(
				m => {
					_documentToSave = m.DocumentController;
					this.Start(this.DoWork);
				}
			));
		}

		object _lock = new object();
		protected override void Start(Action a) {
			if( _task != null ) {
				_task.Wait();
			}
			_task = Task.Factory.StartNew(() => { lock( _lock ) a(); });
		}

		protected void DoWork() {
			if( _documentToSave == null ) {
				//well, this can happen when you hit save, and close document immediatelly after it.
				return;
			}

			if( !_documentToSave.HasRepositoryContext ) {
				_bus.Publish(new Requests.SaveAsDocument(_documentToSave));
				return;
			}

			var progressIndicator = this.OpenProgressIndicator(_documentToSave.RepositoryPath);
			_controller.LockFileOperations();
			_documentToSave.Lock();
			try {
				// null = no temp file used
				string tempFilePath = null;

				// check whether file is already saved
				var fileChecker = _controller.Context.Core.CreateFileChecker();
				var fileRemover = _controller.Context.Core.CreateFileRemover();
				//check whether target file already exists
				if( fileChecker.Exists(_documentToSave.RepositoryPath) ) {
					//it does, so get temp file name
					tempFilePath = Path.Combine(
						Path.GetDirectoryName(_documentToSave.RepositoryPath),
						"~$" + Path.GetFileName(_documentToSave.RepositoryPath)
					);
					// catch all exceptions here. if temp file operation fails, we still want to save the file
					try {
						// if temp file already exists, delete it
						if( fileChecker.Exists(tempFilePath) ) {
							fileRemover.Remove(tempFilePath);
						}
						// now rename original document file to tmp file
						var fileRenamer = _controller.Context.Core.CreateFileRenamer();
						fileRenamer.Rename(_documentToSave.RepositoryPath, tempFilePath);
						// set it as hidden
						var fileAttrib = _controller.Context.Core.CreateFileAttributes();
						fileAttrib.Set(tempFilePath, FileAttributes.Hidden);
					} catch( Exception ) {
						tempFilePath = null;
					}
				}

				// do save
				_documentToSave.Save();

				if( tempFilePath != null ) {
					// seems we saved successfully, remove the temp file
					if( fileChecker.Exists(tempFilePath) ) {
						fileRemover.Remove(tempFilePath);
					}
				}
			} catch( NaraceaException ex ) {
				_bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileSaving, ex));
				_bus.Publish(new Messages.DocumentSaveFailed(_documentToSave));
			} finally {
				_documentToSave.Unlock();
			}

			//we need this special message so we can detect when whole save operation is complete
			_bus.Publish(new Messages.DocumentSaveCompleted(_documentToSave));

			progressIndicator.Close();
			_controller.UnlockFileOperations();
		}

		IProgressIndicator OpenProgressIndicator(string fileName) {
			var progressIndicator = _controller.Context.View.CreateView<IProgressIndicator>();
			progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.SavingDocumentShort;
			var fileNameShortener = _controller.Context.Core.CreateFileNameShortener();
			progressIndicator.DetailedMessage = fileNameShortener.Shorten(fileName, progressIndicator.DetailedMessageCapacity);
			progressIndicator.Open(_controller.Context.View.ApplicationView.ProgressIndicators);
			return progressIndicator;
		}
	}
}
