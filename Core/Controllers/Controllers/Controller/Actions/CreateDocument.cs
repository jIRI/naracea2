﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using System.Threading.Tasks;

namespace Naracea.Controller.Actions {
	internal class CreateDocument : ControllerAction {
		public CreateDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.CreateDocument>(
				m => this.DoWork()
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.CreateDocument>(
				m => this.DoWork()
			));
		}

		protected async void DoWork() {
			_controller.LockFileOperations();
			await Task.Run(() => this.DoCreateDocument());
			_controller.UnlockFileOperations();
		}

		private void DoCreateDocument() {
			try {
				var documentController = _controller.Context.Controller.CreateDocumentController(_controller);
				documentController.Initialize();
				_controller.View.UpdateDocument(documentController.View);
			} catch( NaraceaException ex ) {
				_bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileCreatingNewDocument, ex));
				_bus.Publish(new Messages.DocumentOpenFailed());
			}
		}
	}
}
