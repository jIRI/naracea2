﻿using Naracea.Common.Exceptions;
using Naracea.Core.Spellcheck;
using Naracea.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Naracea.Controller.Actions
{
    internal class Spellcheck : ControllerAction
    {
        IDictionaryStore _dictionaryStore;

        public Spellcheck(IController controller)
            : base(controller)
        {
            _dictionaryStore = _controller.Context.DictionaryStore;
            _subscriptions.Add(_bus.Subscribe<View.Requests.SpellcheckInstallLanguage>(m =>
            {
                this.Start(this.DoWork);
            }));
            _subscriptions.Add(_bus.Subscribe<Messages.Started>(m =>
            {
                this.RestoreInstalledLanguages();
            }));
        }

        protected void DoWork()
        {
            var dialog = _controller.Context.View.CreateView<IInstallDictionaryDialog>();
            bool? success = null;
            dialog.FileSelected += (s, e) => success = this.TryInstallOo(e.Filename);
            dialog.FilesSelected += (s, e) => success = this.TryInstallAffDic(e.Filenames.ToArray());
            dialog.ShowDialog();

            if (success.HasValue)
            {
                if (!success.Value)
                {
                    _bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileInstallingDictionary, Naracea.Core.Properties.Resources.ErrorWhileInstallingDictionaryDetail));
                }
                else
                {
                    this.StoreInstalledLanguages();
                    this.UpdateView();
                }
            }
        }

        bool TryInstallAffDic(string[] files)
        {
            bool success = false;
            if (files.Length != 2)
            {
                return false;
            }
            var affQuery = from f in files
                           where Path.GetExtension(f) == ".aff"
                           select f;
            string aff = affQuery.FirstOrDefault();
            var dicQuery = from f in files
                           where Path.GetExtension(f) == ".dic"
                           select f;
            string dic = dicQuery.FirstOrDefault();
            if (string.IsNullOrEmpty(aff) || string.IsNullOrEmpty(dic))
            {
                return false;
            }
            string name = Path.GetFileNameWithoutExtension(dic);
            try
            {
                _dictionaryStore.Install(name, aff, dic);
                success = true;
            }
            catch (NaraceaException ex)
            {
                _bus.Publish(new Messages.Error(ex.Message, ex.InnerException.Message));
            }
            return success;
        }

        bool TryInstallOo(string oxtFile)
        {
            bool success = false;
            try
            {
                _dictionaryStore.InstallOoDictionary(oxtFile);
                success = true;
            }
            catch (NaraceaException ex)
            {
                _bus.Publish(new Messages.Error(ex.Message, ex.InnerException.Message));
            }
            return success;
        }

        void RestoreInstalledLanguages()
        {
            if (_controller.Settings.HasValue(Naracea.Common.Settings.SpellcheckInstalledLanguages))
            {
                try
                {
                    var fileChecker = _controller.Context.Core.CreateFileChecker();
                    var dictionaries = _controller.Settings.Get<List<DictionaryDescriptor>>(Naracea.Common.Settings.SpellcheckInstalledLanguages);
                    foreach (var dict in dictionaries)
                    {
                        if (fileChecker.Exists(dict.DicFileName) && fileChecker.Exists(dict.AffFileName))
                        {
                            _dictionaryStore.InstalledLanguages.Add(dict);
                        }
                    }
                    //there might be some dictionaries deleted, remove them from the list
                    this.StoreInstalledLanguages();
                    this.UpdateView();
                }
                catch (Exception)
                {
                    //well, invalid string
                    //do nothing about it, just do not change the list
                }
            }
        }

        void StoreInstalledLanguages()
        {
            _controller.Settings.Set(Naracea.Common.Settings.SpellcheckInstalledLanguages, _dictionaryStore.InstalledLanguages.ToList());
        }

        void UpdateView()
        {
            var langsQuery = from lang in _dictionaryStore.InstalledLanguages
                             select new SpellcheckDictionaryItem(lang.DictionaryName, lang.DisplayName, lang);
            _controller.View.SpellcheckInstalledLanguages = langsQuery.ToList();
        }
    }
}
