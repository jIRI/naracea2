﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Events;
using Naracea.Common.Exceptions;
using Naracea.View.Components;

namespace Naracea.Controller.Actions {
	internal class OpenDocument : ControllerAction {
		string _fileName;

		public OpenDocument(IController controller)
			: base(controller) {
			_subscriptions.Add(_bus.Subscribe<Requests.OpenDocument>(
				m => {
					_fileName = m.FileName;
					this.Start(this.DoWork);
				}
			));
			_subscriptions.Add(_bus.Subscribe<View.Requests.OpenDocument>(
				m => {
					_fileName = m.FileName;
					this.Start(this.DoWork);
				}
			));
		}

		protected void DoWork() {
			if( this.DocumentAlreadyOpen(_fileName) ) {
				return;
			}

			var progressIndicator = OpenProgressIndicator(_fileName);
			_controller.LockFileOperations();
			if( _controller.Context.FileNameValidator.IsValid(_fileName) ) {
				IDocumentController documentController = null;
				try {
					documentController = _controller.Context.Controller.CreateDocumentController(_controller);
					documentController.Lock();
					documentController.View.Name = System.IO.Path.GetFileName(_fileName);
					_controller.View.UpdateDocument(documentController.View);
					var busyIndicator = documentController.View;
					busyIndicator.BeginBusy();
					var repoContext = _controller.Context.Core.CreateRepositoryContext();
					repoContext.Path = _fileName;
					documentController.Open(repoContext);
					_controller.View.UpdateDocument(documentController.View);
					documentController.Unlock();
					busyIndicator.EndBusy();
				} catch( NaraceaException ex ) {
                    _bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileCreatingNewDocument, ex));
					_bus.Publish(new Messages.DocumentOpenFailed());

					if( documentController != null ) {
						documentController.Close();
					}
				}
			} else {
				//invalid file name
                _bus.Publish(new Messages.Error(Naracea.Core.Properties.Resources.ErrorWhileOpening, _fileName));
				_bus.Publish(new Messages.DocumentOpenFailed());
			}
			_controller.UnlockFileOperations();
			progressIndicator.Close();
		}

		bool DocumentAlreadyOpen(string _fileName) {
			foreach( var doc in _controller.Documents ) {
				if( doc.RepositoryPath == _fileName ) {
					_controller.Bus.Publish(new Naracea.View.Requests.ActivateDocument(doc.View));
					return true;
				}
			}

			return false;
		}

		IProgressIndicator OpenProgressIndicator(string fileName) {
			var progressIndicator = _controller.Context.View.CreateView<IProgressIndicator>();
            progressIndicator.ShortMessage = Naracea.Core.Properties.Resources.OpeningDocumentShort;
			var fileNameShortener = _controller.Context.Core.CreateFileNameShortener();
			progressIndicator.DetailedMessage = fileNameShortener.Shorten(fileName, progressIndicator.DetailedMessageCapacity);
			progressIndicator.Open(_controller.Context.View.ApplicationView.ProgressIndicators);
			return progressIndicator;
		}
	}
}
