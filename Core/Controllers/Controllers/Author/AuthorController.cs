﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.Diagnostics;

namespace Naracea.Controller {
	public class AuthorController : ControllerBase, IAuthorController {
		IEnumerable<IAuthor> _authors;
		public AuthorController(IEnumerable<IAuthor> authors, IControllerContext context)
			: base(context) {
			Debug.Assert(authors != null);
			Debug.Assert(authors.Count() == 1);
			_authors = authors;
			this.CurrentAuthor = _authors.First();
		}

		#region IAuthorController Members
		public IAuthor CurrentAuthor { get; private set; }

		public void Close() {
			this.Dispose();
		}
		#endregion
	}
}
