﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Context;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;

namespace Naracea.Controller {
	public interface IAuthorController {
		IAuthor CurrentAuthor {get;}
		void Close();
	}
}
