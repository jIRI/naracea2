﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Naracea.Common.Extensions;
using Naracea.Common.Exceptions;
using Naracea.View;
using Naracea.View.Compounds;
using System.Diagnostics;
using MemBus;
using System.Threading.Tasks;

namespace Naracea.Controller {
	internal abstract class ActionBase : IDisposable {
		protected IBus _bus;
		protected List<IDisposable> _subscriptions = new List<IDisposable>();
		
		public ActionBase(IBus bus) {
			_bus = bus;
		}

		protected virtual void Start(Action a) {
			Task.Factory.StartNew(a);
		}

		#region IDisposable Members
		~ActionBase() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
			Debug.Assert(false, this.GetType().FullName);
		}

		private bool _disposed = false;
		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
					foreach( var subscription in _subscriptions ) {
						subscription.Dispose();
					}
					_subscriptions.Clear();
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
