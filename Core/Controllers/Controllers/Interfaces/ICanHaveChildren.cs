﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Controller {
	public interface ICanHaveChildren<T> where T : class {
		void RegisterChild(T child);
		void UnregisterChild(T child);
	}
}
