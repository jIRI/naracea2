﻿using MemBus;
using Naracea.Common;
using Naracea.Controller.Plugins;
using Naracea.Core;
using Naracea.Core.FileSystem;
using Naracea.Core.Spellcheck;
using Naracea.Plugin;
using Naracea.Plugin.Export;
using Naracea.View;
using Naracea.View.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Windows;

namespace Naracea.Controller {
	public class ControllerContext : IControllerContext {
		#region ctors
		public ControllerContext(
		Application application,
		IControllerFactories controllerFactory,
		ICoreFactories coreFactory,
		IViewContext viewContext,
		IDateTimeProvider dateTimeProvider,
		CurrentVersion currentVersion,
		IDictionaryStore dictionaryStore,
		ISettings settings,
		IBus bus
		) {
			this.Application = application;
			this.Controller = controllerFactory;
			this.Core = coreFactory;
			this.View = viewContext;
			this.DateTimeProvider = dateTimeProvider;
			this.FileNameValidator = this.Core.CreateFileNameValidator();
			this.CurrentVersion = currentVersion;
			this.DictionaryStore = dictionaryStore;
			this.Settings = settings;
			this.Bus = bus;
			this.Exporters = new Dictionary<string, IExportPlugin>();
			this.View.ApplicationView.ExportFormats = new ExportFormatItem[0];
		}
		#endregion

		#region IControllerContext Members
		public Application Application { get; private set; }
		public IControllerFactories Controller { get; private set; }
		public ICoreFactories Core { get; private set; }
		public IViewContext View { get; private set; }
		public IDateTimeProvider DateTimeProvider { get; private set; }
		public IFileNameValidator FileNameValidator { get; private set; }
		public IDictionaryStore DictionaryStore { get; private set; }
		public ISettings Settings { get; private set; }
		public IBus Bus { get; private set; }
		public CurrentVersion CurrentVersion { get; private set; }
		public IDictionary<string, IExportPlugin> Exporters { get; private set; }
		public IPluginHost PluginHost { get; private set; }
		public IPluginContainer Plugins { get; private set; }
		public string PluginDataFolder { get { return ControllerContext.GetPluginDataFolder(); } }

		public void SaveSettings() {
			try {
				var settingsFile = ControllerContext.GetSettingsFileName();
				var fileChecker = this.Core.CreateFileChecker();
				if (fileChecker.Exists(settingsFile)) {
					var fileRemover = this.Core.CreateFileRemover();
					fileRemover.Remove(settingsFile);
				}
				var serializer = this.Core.CreateFileXmlSerializer();
				serializer.Serialize(settingsFile, new Naracea.Common.Collections.SerializableDictionary<string, string>(this.Settings.Data));
			} catch {
				//cannot save settings. well, that's sad, but we can hardly do anything about it
			}
		}

		public void LoadSettings() {
			var settingsFile = ControllerContext.GetSettingsFileName();
			var fileChecker = this.Core.CreateFileChecker();
			if (!fileChecker.Exists(settingsFile)) {
				return;
			}
			var serializer = this.Core.CreateFileXmlSerializer();
			try {
				var dict = serializer.Deserialize<Naracea.Common.Collections.SerializableDictionary<string, string>>(settingsFile);
				if (dict != null) {
					foreach (var pair in dict) {
						this.Settings.Set(pair.Key, pair.Value);
					}
				}
			} catch {
				//seems that settings are corrupted. we must live with it.
			}
		}

		private static string GetSettingsFileName() {
			var savePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			savePath = System.IO.Path.Combine(savePath, Naracea.Common.Constants.AppDirectory);
			if (!Directory.Exists(savePath)) {
				Directory.CreateDirectory(savePath);
			}
			var settingsFile = System.IO.Path.Combine(savePath, Naracea.Common.Settings.SettingsFile);
			return settingsFile;
		}

		private static string GetPluginDataFolder() {
			var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			path = System.IO.Path.Combine(path, Naracea.Common.Constants.AppDirectory, Naracea.Common.Constants.PluginDataDirectory);
			if (!Directory.Exists(path)) {
				Directory.CreateDirectory(path);
			}
			return path;
		}

		public void LoadPlugins(IPluginHost pluginHost) {
			this.PluginHost = pluginHost;

			// prepare mef - it will compose whatever is in app directory
			var catalog = new AggregateCatalog();
			var path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			catalog.Catalogs.Add(new DirectoryCatalog(path));
			CompositionContainer container = new CompositionContainer(catalog);

			// we need to pass plugin host 
			container.ComposeExportedValue<IPluginHost>(this.PluginHost);

			// we will load plugins into this object
			var pluginContainer = new Plugins.PluginContainer();
			this.Plugins = pluginContainer;

			// we do not catch any exception here, if plugins are corrupted, fail early
			container.ComposeParts(pluginContainer);

			//now we need to do some additional initialization...
			this.InitializeExporters();
		}

		#endregion

		#region Private methods
		private void InitializeExporters() {
			var viewItems = new List<ExportFormatItem>();
			foreach (var exporter in this.Plugins.Exporters) {
				this.Exporters.Add(exporter.ExporterId.ToUpperInvariant(), exporter);
				viewItems.Add(new ExportFormatItem {
					Id = exporter.ExporterId,
					DisplayName = exporter.ShortDescription,
					Description = exporter.LongDescription,
					ImageSmall = exporter.SmallImage,
					ImageLarge = exporter.LargeImage,
				});
			}
			this.View.ApplicationView.ExportFormats = viewItems;
		}
		#endregion
	}
}
