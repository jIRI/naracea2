﻿using System.Collections.Generic;
using System.Windows;
using MemBus;
using Naracea.Common;
using Naracea.Core;
using Naracea.Core.FileSystem;
using Naracea.Core.Spellcheck;
using Naracea.Plugin.Export;
using Naracea.View.Context;
using Naracea.Controller.Plugins;
using Naracea.Plugin;

namespace Naracea.Controller {
	public interface IControllerContext {
		Application Application { get; }
		IControllerFactories Controller { get; }
		ICoreFactories Core { get; }
		IViewContext View { get; }
		IDateTimeProvider DateTimeProvider { get; }
		IFileNameValidator FileNameValidator { get; }
		ISettings Settings { get; }
		CurrentVersion CurrentVersion { get; }
		IBus Bus { get; }
		IDictionaryStore DictionaryStore { get; }
		IDictionary<string, IExportPlugin> Exporters { get; }
		string PluginDataFolder { get; }

		IPluginHost PluginHost { get; }
		IPluginContainer Plugins { get; }

		void LoadPlugins(IPluginHost pluginHost);
		void SaveSettings();
		void LoadSettings();
	}
}
