﻿using System.Collections.Generic;
using MemBus;
using Naracea.Common;
using Naracea.Controller.ErrorReporters;
using Naracea.Core;
using Naracea.Core.Model;
using Naracea.View;
using Naracea.Plugin;
using Naracea.Controller.Plugins;

namespace Naracea.Controller {
	public interface IControllerFactories {
		IDocumentController CreateDocumentController(
				IController controller
		);
		IBranchController CreateBranchController(
				IDocumentController documentController,
				IBranch branch,
				IControllerContext context
		);
		ITextEditorController CreateTextEditorController(
				IDocumentController documentController,
				IBranchController branchController,
				IControllerContext context
		);
		ISpellcheckController CreateSpellcheckController(
				IDocumentController documentController,
				IBranchController branchController,
				ITextEditorController textEditorController,
				IControllerContext context
		);
		ITimelineController CreateTimelineController(
				IBranchController branchController,
				IControllerContext context
		);
		IChangeStreamController CreateChangeStreamController(
				IBranchController branchController,
				IControllerContext context
		);
		ITextEditor CreateTextEditorBridge(
				ITextEditorView editorView
		);
		IAuthorController CreateAuthorController(
				IEnumerable<IAuthor> authors,
				IControllerContext context
		);

		IErrorReporter CreateSimpleReporter(
				IBus bus,
				IErrorOccuredDialog dialog
		);
		ISettings CreateSettings(
				ISettings parent,
				IDictionary<string, string> container
		);

		IApplicationPluginAdapter CreateApplicationPluginAdapter(
			IController controller,
			IControllerContext context
		);
		IDocumentPluginAdapter CreateDocumentPluginAdapter(
				IDocumentController documentController,
				IControllerContext context
		);
		IBranchPluginAdapter CreateBranchPluginAdapter(
			IBranchController branchController,
			IControllerContext context
		);
		IEditorPluginAdapter CreateEditorPluginAdapter(
			ITextEditorController editorController,
				IControllerContext context
		);

		ISearchResultPluginAdapter CreateSearchResultPluginAdapter(
			IDocumentPluginAdapter document, 
			IBranchPluginAdapter branch, 
			ISearchMatchItem item
		);
	}
}
