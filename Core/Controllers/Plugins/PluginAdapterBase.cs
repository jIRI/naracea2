﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins {
	internal class PluginAdapterBase : IDisposable {
		List<IDisposable> _disposables = new List<IDisposable>();

		public IControllerContext Context { get; private set; }

		public PluginAdapterBase(IControllerContext context) {
			this.Context = context;
		}

		protected void RegisterForDisposal(IDisposable subscription) {
			_disposables.Add(subscription);
		}

		protected void DisposeDisposables() {
			foreach( var subscription in _disposables ) {
				subscription.Dispose();
			}
			_disposables.Clear();
		}

		protected void CheckDisposeState() {
			if( _disposed ) {
				throw new InvalidOperationException("Using disposed object.");
			}
		}
		
		#region IDisposable Members
		~PluginAdapterBase() {
			Dispose(false);
		}

		protected bool _disposed = false;
		public bool IsDisposed {
			get {
				return _disposed;
			}
		}

		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
					this.DisposeDisposables();
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
