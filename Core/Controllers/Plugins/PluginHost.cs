﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using MemBus;
using Naracea.Plugin;
using Naracea.View;

namespace Naracea.Controller.Plugins {
	internal class PluginHost : PluginAdapterBase, IHostPluginAdapter {
		IController _controller;
		static List<Type> _supportedViewTypes = new List<Type> {
			typeof(Naracea.View.IOpenDocumentDialog),
			typeof(Naracea.View.ISaveDocumentDialog),
			typeof(Naracea.View.IExportBranchDialog),
			typeof(Naracea.View.IErrorOccuredDialog),
			typeof(Naracea.View.Components.IProgressIndicator),
			typeof(Naracea.View.Components.ICompositeIndicator),
			typeof(Naracea.View.Components.IIntIndicator),
			typeof(Naracea.View.Components.IStringIndicator),
			typeof(Naracea.View.IDocumentPropertiesView),
			typeof(Naracea.View.IBranchPropertiesView),
			typeof(Naracea.View.IConfirmCloseDocumentDialog),
			typeof(Naracea.View.IConfirmBranchDeletionDialog),
			typeof(Naracea.View.ITextEditorView)
		};

		SortedList<string, Action> _uiInitializationQueue = new SortedList<string, Action>();

		public PluginHost(IController controller, IControllerContext context)
			: base(context) {
			_controller = controller;
			//configure plugin bus
			this.Bus = BusSetup.StartWith<
					MemBus.Configurators.Conservative,
					Naracea.Controller.Configuration.Bus.PublishingConventions
				>().Construct();
			this.ConfigureMessageForwarding();
			//create application adaptor
			this.ApplicationAdapter = this.Context.Controller.CreateApplicationPluginAdapter(_controller, this.Context);
		}

		#region IPluginHost Members
		public Version ApplicationVersion {
			get {
				return this.Context.CurrentVersion.Version;
			}
		}

		public IBus Bus {
			get;
			private set;
		}

		public IEnumerable<IPlugin> Plugins {
			get;
			private set;
		}

		public IEnumerable<Plugin.Export.IExportPlugin> Exporters {
			get;
			private set;
		}

		public IPluginHostApplication Application {
			get { return this.ApplicationAdapter; }
		}

		public Core.ICoreFactories CoreFactories {
			get { return _controller.Context.Core; }
		}

		public string PluginDataFolder {
			get {
				return this.Context.PluginDataFolder;
			}
		}
		
		public View.Compounds.IHasIndicators DocumentIndicators {
			get {
				return this.Context.View.ApplicationView.DocumentStatusBar;
			}
		}

		public View.Compounds.IHasIndicators ProgressIndicators {
			get {
				return this.Context.View.ApplicationView.ProgressIndicators;
			}
		}

		public T CreateView<T>() where T : class {
			if( !_supportedViewTypes.Contains(typeof(T)) ) {
				throw new NotSupportedException(typeof(T).Name + ": type not supported");
			}
			return this.Context.View.CreateView<T>();
		}

		public void EnqueuePluginUiInitialization(IPlugin plugin, Action initAction) {
			_uiInitializationQueue.Add(plugin.PluginId.ToString(), initAction);
		}

		public void ExecuteOnUiThread(Action action) {
			_controller.View.Exec(action);
		}

		public void ExecuteOnUiThreadAsync(Action action) {
			_controller.View.ExecAsync(action);
		}

		public void ReportError(string shortMessage, string detail) {
			this.Context.Bus.Publish(new Messages.Error(shortMessage, detail));
		}

		public void AddGroupToRibbonTab(ApplicationRibbonTabIds tab, System.Windows.Controls.Ribbon.RibbonGroup group) {
			_controller.View.AddRibbonGroup(tab, group);
		}

		public void AddRibbonTab(System.Windows.Controls.Ribbon.RibbonTab tab) {
			_controller.View.AddRibbonTab(tab);
		}

		public void NotifyUiUpdated() {
			_controller.View.FinalizePluginUiUpdate();
		}

		public void RegisterControlName(string name, System.Windows.Controls.Control control) {
			_controller.View.RegisterControl(name, control);
		}

		public void UnregisterControlName(string name) {
			_controller.View.UnregisterControl(name);
		}

		public void RegisterPluginWindow(System.Windows.Window window) {
			_controller.View.RegisterChildWindow(window);
		}
		#endregion

		#region IHostPluginAdapter members
		public IApplicationPluginAdapter ApplicationAdapter { get; private set; }

		public System.Collections.ObjectModel.ObservableCollection<ISearchMatchItem> ApplicationSearchResults {
			get {
				return this.ApplicationAdapter.SearchMatches;
			}
			set {
				this.ApplicationAdapter.SearchMatches = value;
			}
		}
		#endregion

		#region Internal methods
		internal void ResolveAdditionalDependencies(IControllerContext context) {
			this.Exporters = context.Plugins.Exporters;
			this.Plugins = context.Plugins.Plugins;
		}
		#endregion

		#region Private methods
		private void ConfigureMessageForwarding() {
			//application messages
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.Error>(
				m => this.Bus.Publish(new Naracea.Plugin.Messages.Error(this))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.Started>(
				m => {
					this.Bus.Publish(new Naracea.Plugin.Messages.Started(this));
					// at this point all plugins should have their UI initialization requests enqueued
					this.InitializePluginsUi();
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.StartFailed>(
				m => this.Bus.Publish(new Naracea.Plugin.Messages.StartFailed(this))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.Stopped>(
				m => this.Bus.Publish(new Naracea.Plugin.Messages.Stopped(this))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.StopCancelled>(
				m => this.Bus.Publish(new Naracea.Plugin.Messages.StopCancelled(this))
			));
			// document messages
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentOpenFailed>(
				m => this.Bus.Publish(new Naracea.Plugin.Messages.DocumentOpenFailed(this))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentCloseCancelled>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentCloseCancelled(
						this, 
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaveCancelled>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentSaveCancelled(
						this, 
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaveFailed>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentSaveFailed(
						this, 
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaveCompleted>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentSaveCompleted(
						this, 
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentActivated>(
				// here we need to use task, because we would use autofac to create instance while ctor of docController runs which would end in deadlock
				m => Task.Factory.StartNew(() =>
					this.Bus.Publish(
						new Naracea.Plugin.Messages.DocumentActivated(
							this,
							this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
						)
					))
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentDeactivated>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentDeactivated(
						this,
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentClosed>(
				m => {
					// cache adapter
					var docAdapter = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					// remove adapter
					this.ApplicationAdapter.RemoveDocumentAdapter(m.DocumentController);
					// publish with cached adapter
					this.Bus.Publish(new Naracea.Plugin.Messages.DocumentClosed(this, docAdapter));
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentSaved>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentSaved(
						this,
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
						)
					)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentOpened>(
				m => this.Bus.Publish(
						new Naracea.Plugin.Messages.DocumentOpened(
							this,
							this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
						)
					)
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.DocumentExported>(
				m => this.Bus.Publish(
					new Naracea.Plugin.Messages.DocumentExported(
						this,
						this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController)
					)
				)
			));
			// branch messages
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchClosed>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(
						new Naracea.Plugin.Messages.BranchClosed(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDirty>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchDirty(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.IndicatorsUpdateNeeded>(
				m => {
					try {
						var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
						if( document != null ) {
							this.Bus.Publish(new Naracea.Plugin.Messages.IndicatorsUpdateNeeded(
									this,
									document,
									document.GetOrCreateBranchAdapter(m.BranchController)
								)
							);
						}
					} catch( NullReferenceException ex ) {
						Debug.WriteLine("Gotcha! " + ex.Message);
					}
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.TextEditorClosed>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					var branch = document.GetOrCreateBranchAdapter(m.BranchController);
					this.Bus.Publish(new Naracea.Plugin.Messages.TextEditorClosed(
							this,
							document,
							branch,
							branch.Editor
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchShiftingFinished>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchShiftingFinished(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchOpened>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchOpened(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDeleted>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					var branch = document.GetOrCreateBranchAdapter(m.BranchController);
					document.RemoveBranchAdapter(branch.Controller);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchDeleted(
							this,
							document,
							branch
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDeleteCancelled>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchDeleteCancelled(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchActivated>(
				// here we need to use task, because we would use autofac to create instance while ctor of branchController runs which would end in deadlock
				m => Task.Factory.StartNew(() => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchActivated(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				})
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchDeactivated>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchDeactivated(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
			this.RegisterForDisposal(this.Context.Bus.Subscribe<Messages.BranchExported>(
				m => {
					var document = this.ApplicationAdapter.GetOrCreateDocumentAdapter(m.DocumentController);
					this.Bus.Publish(new Naracea.Plugin.Messages.BranchExported(
							this,
							document,
							document.GetOrCreateBranchAdapter(m.BranchController)
						)
					);
				}
			));
		}

		private void InitializePluginsUi() {
			foreach( var action in _uiInitializationQueue.Values ) {
				this.ExecuteOnUiThread(action);
			}
		}
		#endregion
	}
}
