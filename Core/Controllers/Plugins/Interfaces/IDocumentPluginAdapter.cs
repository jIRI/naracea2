﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;

namespace Naracea.Controller.Plugins {
	public interface IDocumentPluginAdapter : IPluginHostDocument {
		Dictionary<IBranchController, IBranchPluginAdapter> BranchAdapters { get; }
		IDocumentController Controller { get;}

		void RemoveBranchAdapter(IBranchController branchController);
		IBranchPluginAdapter GetOrCreateBranchAdapter(IBranchController branchController);
	}
}
