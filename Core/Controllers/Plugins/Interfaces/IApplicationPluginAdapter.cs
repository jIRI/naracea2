﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;
using Naracea.View;

namespace Naracea.Controller.Plugins {
	public interface IApplicationPluginAdapter : IPluginHostApplication {
		Dictionary<IDocumentController, IDocumentPluginAdapter> DocumentAdapters { get; }
		ObservableCollection<ISearchMatchItem> SearchMatches { get; set; }

		IController Controller { get; }

		void RemoveDocumentAdapter(IDocumentController documentController);
		IDocumentPluginAdapter GetOrCreateDocumentAdapter(IDocumentController documentController);
	}
}
