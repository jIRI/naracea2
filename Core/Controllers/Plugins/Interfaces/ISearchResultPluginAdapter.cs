﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;
using Naracea.View;

namespace Naracea.Controller.Plugins {
	public interface ISearchResultPluginAdapter : IPluginHostSearchResult {
		ISearchMatchItem MatchItem { get; }
	}
}
