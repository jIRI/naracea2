﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;

namespace Naracea.Controller.Plugins {
	public interface IEditorPluginAdapter : IPluginHostEditor {
		ITextEditorController Controller { get; }	
	}
}
