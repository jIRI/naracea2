﻿using Naracea.Plugin;
using Naracea.Plugin.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins {
	public interface IPluginContainer {
		IEnumerable<IExportPlugin> Exporters { get; set; }
		
		IEnumerable<IPlugin> Plugins { get; set; }
	}
}
