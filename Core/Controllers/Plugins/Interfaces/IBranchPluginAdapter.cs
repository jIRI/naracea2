﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;

namespace Naracea.Controller.Plugins {
	public interface IBranchPluginAdapter : IPluginHostBranch {
		IEditorPluginAdapter EditorAdapter { get; }
		IBranchController Controller { get; }
	}
}
