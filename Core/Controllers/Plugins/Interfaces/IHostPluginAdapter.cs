﻿using System.Collections.ObjectModel;
using Naracea.Plugin;
using Naracea.View;

namespace Naracea.Controller.Plugins {
	public interface IHostPluginAdapter : IPluginHost {
		IApplicationPluginAdapter ApplicationAdapter { get; }
		ObservableCollection<ISearchMatchItem> ApplicationSearchResults { get; set; }
	}
}
