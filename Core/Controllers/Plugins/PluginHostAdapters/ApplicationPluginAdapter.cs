﻿using Naracea.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Naracea.View;

namespace Naracea.Controller.Plugins.PluginHostAdapters {
	internal class ApplicationPluginAdapter : PluginAdapterBase, IApplicationPluginAdapter {
		public IController Controller { get; private set; }

		public ApplicationPluginAdapter(IController controller, IControllerContext context)
			: base(context) {
			this.Controller = controller;
			this.InitializeDocumentAdapters();
			this.InitializeHandlers();
		}

		#region IPluginHostApplication Members
		public Common.ISettings Settings {
			get { return this.Controller.Settings; }
		}

		public IEnumerable<IPluginHostDocument> Documents {
			get {
				return this.DocumentAdapters.Values;
			}
		}

		public IEnumerable<IPluginHostSearchResult> SearchResults {
			get {
				var query = from match in this.SearchMatches
										let document = this.GetOrCreateDocumentAdapter(match.DocumentView.Controller as IDocumentController)
										let branch = document.GetOrCreateBranchAdapter(match.BranchView.Controller as IBranchController)
										select this.Context.Controller.CreateSearchResultPluginAdapter(document, branch, match);
				return query;
			}
		}

		public IPluginHostDocument ActiveDocument {
			get {
				return this.GetOrCreateDocumentAdapter(this.Controller.ActiveDocument);
			}
			set {
				var docAdapter = value as IDocumentPluginAdapter;
				if( docAdapter != null ) {
					this.Controller.ActiveDocument = docAdapter.Controller;
				}
			}
		}

		public void CreateNewDocument() {
			this.Controller.Bus.Publish(new Requests.CreateDocument());
		}

		public void OpenDocument(string path) {
			this.Controller.Bus.Publish(new Requests.OpenDocument(path));
		}

		public void SaveDocument(IPluginHostDocument document) {
			var docAdapter = document as IDocumentPluginAdapter;
			if( docAdapter != null ) {
				this.Controller.Bus.Publish(new Requests.SaveDocument(docAdapter.Controller));
			}
		}

		public void SaveDocumentAs(IPluginHostDocument document) {
			var docAdapter = document as IDocumentPluginAdapter;
			if( docAdapter != null ) {
				this.Controller.Bus.Publish(new Requests.SaveAsDocument(docAdapter.Controller));
			}
		}

		public void SaveDocumentAs(IPluginHostDocument document, string path) {
			var docAdapter = document as IDocumentPluginAdapter;
			if( docAdapter != null ) {
				var repoContext = this.Context.Core.CreateRepositoryContext();
				repoContext.Path = path;
				docAdapter.Controller.ChangeRepository(repoContext);
				this.Controller.Bus.Publish(new Requests.SaveDocument(docAdapter.Controller));
			}
		}

		public void SaveAllDocuments() {
			this.Controller.Bus.Publish(new Requests.SaveAllDocuments());
		}

		public void CloseDocument(IPluginHostDocument document) {
			var docAdapter = document as IDocumentPluginAdapter;
			if( docAdapter != null ) {
				this.Controller.Bus.Publish(new Requests.CloseDocument(docAdapter.Controller));
			}
		}

		public void PrintActiveBranch(IPluginHostDocument document) {
			var docAdapter = document as IDocumentPluginAdapter;
			if( docAdapter != null ) {
				this.Controller.Bus.Publish(new Requests.CloseDocument(docAdapter.Controller));
			}
		}

		public void ExportBranch(IPluginHostDocument document, IPluginHostBranch branch, Plugin.Export.IExportPlugin exportPlugin, string fileName) {
			var docAdapter = document as IDocumentPluginAdapter;
			var branchAdapter = branch as IBranchPluginAdapter;
			if( docAdapter != null && branchAdapter != null ) {
				this.Controller.Bus.Publish(new Requests.ExportBranchWithName(docAdapter.Controller, branchAdapter.Controller, exportPlugin, fileName));
			}
		}

		public void ClearSearchResults() {
			this.Controller.Bus.Publish(new Requests.ClearAllFindResults());
		}

		public void SearchActiveBranch(Core.Finders.Text.TextSearchOptions options, string pattern) {
			this.Controller.Bus.Publish(new Requests.FindAll(options, pattern));
		}

		public void SearchActiveBranchHistory(Core.Finders.Text.TextSearchOptions options, string pattern) {
			this.Controller.Bus.Publish(new Requests.FindAllHistory(options, pattern));
		}

		public void ReplaceSearchResult(IPluginHostSearchResult item, string replacePattern, Naracea.Core.Finders.Text.TextSearchOptions options) {
			var matchItem = item as ISearchResultPluginAdapter;
			if( item != null ) {
				this.Context.Bus.Publish(new Requests.ReplaceSearchResult(matchItem.MatchItem, replacePattern, options));
			}
		}
		#endregion

		#region Internal methods
		#endregion

		#region IApplicationPluginAdapter Members
		public Dictionary<IDocumentController, IDocumentPluginAdapter> DocumentAdapters { get; private set; }
		public ObservableCollection<ISearchMatchItem> SearchMatches { get; set; }

		public void RemoveDocumentAdapter(IDocumentController documentController) {
			if( this.DocumentAdapters.ContainsKey(documentController) ) {
				this.DocumentAdapters.Remove(documentController);
			}
		}

		public IDocumentPluginAdapter GetOrCreateDocumentAdapter(IDocumentController documentController) {
			if( documentController == null ) {
				return null;
			}
			
			IDocumentPluginAdapter document = null;
			if( !this.DocumentAdapters.ContainsKey(documentController) ) {
				document = this.Context.Controller.CreateDocumentPluginAdapter(documentController, this.Context);
				this.DocumentAdapters.Add(documentController, document);
			} else {
				document = this.DocumentAdapters[documentController];
			}
			return document;
		}
		#endregion

		#region Private methods
		private void InitializeHandlers() {
		}

		private void InitializeDocumentAdapters() {
			this.DocumentAdapters = new Dictionary<IDocumentController, IDocumentPluginAdapter>();
			foreach( var docController in this.Controller.Documents ) {
				this.DocumentAdapters.Add(
					docController,
					this.Context.Controller.CreateDocumentPluginAdapter(docController, this.Context)
				);
			}
		}
		#endregion
	}
}
