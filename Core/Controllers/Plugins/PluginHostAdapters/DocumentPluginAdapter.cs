﻿using Naracea.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins.PluginHostAdapters {
	internal class DocumentPluginAdapter : PluginAdapterBase, IDocumentPluginAdapter {
		public IDocumentController Controller { get; private set; }

		public DocumentPluginAdapter(IDocumentController controller, IControllerContext context)
			: base(context) {
			this.Controller = controller;
			this.InitializeBranchAdapters();
		}

		#region IPluginHostDocument Members
		public bool IsDirty {
			get { return this.Controller.IsDirty; }
		}

		public string Name {
			get { return this.Controller.Name; }
			set {
				if( value == null ) {
					throw new ArgumentNullException("value");
				}
				this.Controller.Name = value;
			}
		}

		public string Comment {
			get { return this.Controller.Comment; }
			set {
				this.Controller.Comment = value;
			}
		}

		public string Filename {
			get { return this.Controller.RepositoryPath; }
		}

		public bool IsActive {
			get { return this.Controller.IsActive; }
		}

		public bool IsLocked {
			get { return this.Controller.IsLocked; }
		}

		public Common.ISettings Settings {
			get { return this.Controller.Settings; }
		}

		public IEnumerable<IPluginHostBranch> Branches {
			get { return this.BranchAdapters.Values; }
		}

		public IPluginHostBranch ActiveBranch {
			get { return this.GetOrCreateBranchAdapter(this.Controller.ActiveBranch); }
			set {
				var branchAdapter = value as IBranchPluginAdapter;
				if( branchAdapter == null || !this.BranchAdapters.ContainsValue(branchAdapter) ) {
					throw new ArgumentException("Invalid branch.");
				}
				this.Controller.ActiveBranch = branchAdapter.Controller;
			}
		}

		public void CreateBranch() {
			this.Context.Bus.Publish(new Requests.CreateBranch(this.Controller));
		}

		public void CreateEmptyBranch() {
			this.Context.Bus.Publish(new Requests.CreateEmptyBranch(this.Controller));
		}

		public void DeleteBranch(IPluginHostBranch branch) {
			var branchAdapter = branch as IBranchPluginAdapter;
			if( branchAdapter == null || !this.BranchAdapters.ContainsValue(branchAdapter) ) {
				throw new ArgumentException("Invalid branch.");
			}
			this.Context.Bus.Publish(new Requests.DeleteBranch(this.Controller, branchAdapter.Controller));
		}

		public void Lock() {
			this.Controller.Lock();
		}

		public void Unlock() {
			this.Controller.Unlock();
		}
		#endregion

		#region IDocumentPluginAdapter Members
		public Dictionary<IBranchController, IBranchPluginAdapter> BranchAdapters {
			get;
			private set;
		}

		public void RemoveBranchAdapter(IBranchController branchController) {
			if( this.BranchAdapters.ContainsKey(branchController) ) {
				this.BranchAdapters.Remove(branchController);
			}
		}

		public IBranchPluginAdapter GetOrCreateBranchAdapter(IBranchController branchController) {
			if( branchController == null ) {
				return null;
			}

			IBranchPluginAdapter branch = null;
			if( !this.BranchAdapters.ContainsKey(branchController) ) {
				branch = this.Context.Controller.CreateBranchPluginAdapter(branchController, this.Context);
				this.BranchAdapters.Add(branchController, branch);
			} else {
				branch = this.BranchAdapters[branchController];
			}

			return branch;
		}
		#endregion

		#region Private methods
		private void InitializeBranchAdapters() {
			this.BranchAdapters = new Dictionary<IBranchController, IBranchPluginAdapter>();
			foreach( var branchController in this.Controller.Branches ) {
				this.BranchAdapters.Add(
					branchController,
					this.Context.Controller.CreateBranchPluginAdapter(branchController, this.Context)
				);
			}
		}
		#endregion
	}
}
