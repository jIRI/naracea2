﻿using Naracea.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins.PluginHostAdapters {
	internal class SearchResultPluginAdapter : ISearchResultPluginAdapter {
		public SearchResultPluginAdapter(IDocumentPluginAdapter document, IBranchPluginAdapter branch, View.ISearchMatchItem item) {
			this.MatchItem = item;
			this.Document = document;
			this.Branch = branch;
		}

		#region ISearchResultPluginAdapter Members
		public View.ISearchMatchItem MatchItem { get; private set; }
		#endregion

		#region IPluginHostSearchResult Members
		public string SearchPattern { get { return this.MatchItem.SearchPattern; } }

		public int PositionInText { get { return this.MatchItem.PositionInText; } }

		public int PositionInPercents { get { return this.MatchItem.PositionInPercents; } }

		public string SurroundedMatchText { get { return this.MatchItem.SurroundedMatchText; } }

		public string MatchedText { get { return this.MatchItem.MatchedText; } }

		public int MatchOffsetInSurroundedText { get { return this.MatchItem.MatchOffsetInText; } }

		public IPluginHostDocument Document { get; private set; }

		public IPluginHostBranch Branch { get; private set; }

		public bool IsHistorySearch { get { return this.MatchItem.IsHistorySearch; } }

		public int ChangeIndex { get { return this.MatchItem.CurrentChangeIndex; } }

		public DateTime DateTime { get { return this.MatchItem.DateTime; } }
		#endregion
	}
}
