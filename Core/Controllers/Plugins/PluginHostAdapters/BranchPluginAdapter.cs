﻿using Naracea.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core.Shifter;

namespace Naracea.Controller.Plugins.PluginHostAdapters {
	internal class BranchPluginAdapter : PluginAdapterBase, IBranchPluginAdapter {
		public IBranchController Controller { get; private set; }

		public BranchPluginAdapter(IBranchController controller, IControllerContext context)
			: base(context) {
			this.Controller = controller;
			this.EditorAdapter = this.Context.Controller.CreateEditorPluginAdapter(this.Controller.Editor, this.Context);
		}

		#region IPluginHostBranch Members

		public string Name {
			get {
				return this.Controller.Name;
			}
			set {
				if( value == null ) {
					throw new ArgumentNullException("value");
				}
				this.Controller.Name = value;
			}
		}

		public string Comment {
			get {
				return this.Controller.Comment;
			}
			set {
				this.Controller.Comment = value;
			}
		}

		public string ExportFileName {
			get { return this.Controller.ExportFileName; }
		}

		public bool IsDirty {
			get { return this.Controller.IsDirty; }
		}

		public bool IsActivated {
			get { return this.Controller.IsActivated; }
		}

		public bool IsRewound {
			get { return this.Controller.IsRewound; }
		}

		public bool IsDocumentActive {
			get { return this.Controller.IsDocumentActive; }
		}

		public bool IsEditing {
			get { return this.Controller.IsEditing; }
		}

		public bool CanBeDeleted {
			get { return this.Controller.CanBeDeleted; }
		}

		public IPluginHostEditor Editor {
			get { return this.EditorAdapter; }
		}

		public Core.Model.IBranch Branch {
			get { return this.Controller.Branch; }
		}

		public Plugin.Export.IExportPlugin Exporter {
			get { return this.Controller.Exporter; }
		}

		public Common.ISettings Settings {
			get { return this.Controller.Settings; }
		}

		public void BeginEdit() {
			this.Controller.BeginEdit();
		}

		public void EndEdit() {
			this.Controller.EndEdit();
		}

		public void StoreGroupBegin() {
			this.Controller.StoreGroupBegin();
		}

		public void StoreGroupEnd() {
			this.Controller.StoreGroupEnd();
		}

		public void Store(Core.Model.Changes.IChange change) {
			if( change == null ) {
				throw new ArgumentNullException("change");
			}
			//we need to execute changes on store, because otherwise user of API can add changes which would never take effect
			//since there is no access to text editor itself (and we are invoking changes programmatically)
			change.ExecuteDo(this.Controller.Editor.CoreBridge);
			this.Controller.Store(change);
		}

		public Core.Model.Changes.IChange FindUndoableChange() {
			return this.Controller.FindUndoableChange();
		}

		public Core.Model.Changes.IChange FindRedoableChange() {
			return this.Controller.FindRedoableChange();
		}

		public void ExecuteShifter(IShifter shifter) {
			if( shifter == null ) {
				throw new ArgumentNullException("shifter");
			}
			this.Controller.ExecuteShifter(shifter);
		}

		#endregion

		#region IBranchPluginAdapter Members
		public IEditorPluginAdapter EditorAdapter {
			get;
			private set;
		}
		#endregion

		#region Private methods
		#endregion
	}
}
