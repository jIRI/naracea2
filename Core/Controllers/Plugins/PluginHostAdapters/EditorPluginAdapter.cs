﻿using Naracea.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins.PluginHostAdapters {
	internal class EditorPluginAdapter : PluginAdapterBase, IEditorPluginAdapter {
		public ITextEditorController Controller { get; private set; }

		public EditorPluginAdapter(ITextEditorController controller, IControllerContext context)
			: base(context) {
			this.Controller = controller;
			this.Controller.TextChanged += (s, e) => {
				var handlers = this.TextChanged;
				if( handlers != null ) {
					handlers(this, e);
				}
			};
		}

		#region IPluginHostEditor Members

		public int CurrentLine {
			get {return this.Controller.CurrentLine;}
			set { this.Controller.CurrentLine = value; }
		}

		public int CurrentColumn {
			get {return this.Controller.CurrentColumn;}
		}

		public int CaretPosition {
			get {return this.Controller.CaretPosition;}
			set { this.Controller.CaretPosition = value; }
		}

		public int TextLength {
			get {return this.Controller.TextLength;}
		}

		public bool IsOverwriteMode {
			get {return this.Controller.IsOverwriteMode;}
		}

		public string Text {
			get {return this.Controller.Text;}
			set {
				if( value == null ) {
					throw new ArgumentNullException("value");
				}
				this.Controller.Text = value;
			}
		}

		public string SelectedText {
			get {return this.Controller.SelectedText;}
		}

		public event EventHandler<Core.TextChangedEventArgs> TextChanged;

		public Common.ISettings Settings {
			get {return this.Controller.Settings;}
		}

		public void Select(int position, int length) {
			this.Controller.Select(position, length);
		}

		public void ReplaceSelection(string replaceWith) {
			this.Controller.ReplaceSelection(replaceWith);
		}

		public void Insert(int position, string text) {
			this.Controller.Insert(position, text);
		}

		public void Insert(string text) {
			this.Controller.Insert(text);
		}

		public void Delete(int position, int length) {
			this.Controller.Delete(position, length);
		}

		public void Delete(int length) {
			this.Controller.Delete(length);
		}

		#endregion
	}
}
