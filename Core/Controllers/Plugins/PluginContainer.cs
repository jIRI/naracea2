﻿using Naracea.Plugin;
using Naracea.Plugin.Export;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Controller.Plugins {
	internal class PluginContainer : IPluginContainer {
		[ImportMany(typeof(IPlugin))]
		public IEnumerable<IPlugin> Plugins { get; set; }

		[ImportMany(typeof(IExportPlugin))]
		public IEnumerable<IExportPlugin> Exporters { get; set; }
	}
}
