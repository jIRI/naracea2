﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MemBus;
using Naracea.Controller.Plugins;
using Naracea.Plugin;
using Naracea.Controller.Plugins.PluginHostAdapters;

namespace Naracea.Controller.Configuration {
	internal class PluginModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<PluginHost>().As<IPluginHost>();
			builder.RegisterType<ApplicationPluginAdapter>().As<IApplicationPluginAdapter>();
			builder.RegisterType<DocumentPluginAdapter>().As<IDocumentPluginAdapter>();
			builder.RegisterType<BranchPluginAdapter>().As<IBranchPluginAdapter>();
			builder.RegisterType<EditorPluginAdapter>().As<IEditorPluginAdapter>();
			builder.RegisterType<SearchResultPluginAdapter>().As<ISearchResultPluginAdapter>();
		}
		#endregion
	}
}
