﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MemBus;

namespace Naracea.Controller.Configuration {
	public class Configurator : IDisposable {
		ContainerBuilder _builder;

		#region IConfigurator Members
		public Configurator() {
			_builder = new ContainerBuilder();
			new FactoriesModule().Configure(_builder);
			new ControllerModule().Configure(_builder);
            new ErrorReportingModule().Configure(_builder);
            new PluginModule().Configure(_builder);
			this.Container = _builder.Build();
			this.Bus = this.Container.Resolve<IBus>();
		}

		public IContainer Container { get; private set; }
		public IBus Bus { get; private set; }
		#endregion

		#region IDisposable Members
		private bool disposed = false;
		public void Dispose() {
			Dispose(true);
		}

		private void Dispose(bool disposing) {
			if( !this.disposed ) {
				if( disposing ) {
					if( this.Container != null ) {
						this.Container.Dispose();
					}
				}
				disposed = true;
			}
		}
		#endregion
	}
}
