using System;
using System.Collections.Generic;
using System.Linq;
using MemBus.Publishing;
using System.Diagnostics;

namespace Naracea.Controller.Configuration.Bus {
	/// <summary>
	/// This is the most simple publisher that works like event handlers: All subscriptions are called in sequence.
	/// If any subscription throws an exception the chain is broken.
	/// </summary>
	public class CheckingSequentialPublisher : IPublishPipelineMember {
		public void LookAt(PublishToken token) {
			//Debug.WriteLine(" #" + token.Message.GetType().FullName);
			var subscriptions = token.Subscriptions.ToArray();
			foreach( var s in subscriptions ) {
				//deliver only to subscribers which still exists
				if( token.Subscriptions.Contains(s) ) {
					s.Push(token.Message);
				}
			}
		}
	}
}