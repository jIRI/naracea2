﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemBus.Publishing;
using MemBus.Setup;
using MemBus.Subscribing;


namespace Naracea.Controller.Configuration.Bus {
	public class PublishingConventions : ISetup<IConfigurableBus> {
		public void Accept(IConfigurableBus setup) {
			setup.ConfigurePublishing(p => p.DefaultPublishPipeline(new CheckingSequentialPublisher()));
		}
	}
}
