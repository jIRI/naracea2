﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MemBus;

namespace Naracea.Controller.Configuration {
	internal class ControllerModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterInstance<IBus>(BusSetup.StartWith<MemBus.Configurators.Conservative, Bus.PublishingConventions>().Construct());
			builder.RegisterType<Controller>().As<IController>().As<Controller>().SingleInstance();
			builder.RegisterType<ControllerContext>().As<IControllerContext>().As<ControllerContext>().SingleInstance();
		}
		#endregion
	}
}
