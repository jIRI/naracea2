﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core;

namespace Naracea.Controller.Configuration {
	internal class FactoriesModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			//Controllers
			builder.RegisterType<DocumentController>().As<IDocumentController>().As<DocumentController>().ExternallyOwned();
			builder.RegisterType<BranchController>().As<IBranchController>().As<BranchController>().ExternallyOwned();
			builder.RegisterType<TextEditorController>().As<ITextEditorController>().As<TextEditorController>().ExternallyOwned();
			builder.RegisterType<TimelineController>().As<ITimelineController>().As<TimelineController>().ExternallyOwned();
			builder.RegisterType<SpellcheckController>().As<ISpellcheckController>().As<SpellcheckController>().ExternallyOwned();
			builder.RegisterType<ChangeStreamController>().As<IChangeStreamController>().As<ChangeStreamController>().ExternallyOwned();
			builder.RegisterType<AuthorController>().As<IAuthorController>().As<AuthorController>().ExternallyOwned();

			//settings
			builder.RegisterType<Naracea.Common.Settings>().As<Naracea.Common.ISettings>().As<Naracea.Common.Settings>();

			//misc types
			builder.RegisterType<CurrentVersion>().As<ICurrentVersion>().As<CurrentVersion>();

			//bridges
			builder.RegisterType<TextEditorBridge>().As<ITextEditor>().As<TextEditorBridge>();

			//Factory agregator
			builder.RegisterType<ControllerFactories>().As<IControllerFactories>().SingleInstance();
		}
		#endregion
	}
}
