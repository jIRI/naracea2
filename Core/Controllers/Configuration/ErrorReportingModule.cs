﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Controller;
using Autofac;
using Naracea.Controller.ErrorReporters;

namespace Naracea.Controller.Configuration {
	internal class ErrorReportingModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<SimpleErrorReporter>().SingleInstance();
		}
		#endregion
	}
}
