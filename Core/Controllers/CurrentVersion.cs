﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;
using System.IO;

namespace Naracea.Controller {
	public class CurrentVersion : ICurrentVersion {
		public Version Version { get; private set; }
		public bool IsVersionValid { get; private set; }
		public CurrentVersion() {
			//set default value
			this.Version = new Version(1, 0, 0);
			this.IsVersionValid = false;
			//now parse the VERSION file
			try {
				Version version = null;
				var streamInfo = Application.GetResourceStream(new Uri("Controller;component/VERSION", UriKind.Relative));
				if( streamInfo.Stream != null ) {
					using( StreamReader reader = new StreamReader(streamInfo.Stream) ) {
						string versionString = reader.ReadLine();
						if( Version.TryParse(versionString, out version) ) {
							this.Version = version;
							this.IsVersionValid = true;
						}
					}
				}
			} catch (Exception) {
				//well, something bad happend. but we have info about that in IsVersionValid. so just ignore the issue
			}
		}
	}
}
