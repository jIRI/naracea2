﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.Interfaces {
	public interface ICanBeBusy {
		void SetBusy();
		void SetNotBusy();
	}
}
