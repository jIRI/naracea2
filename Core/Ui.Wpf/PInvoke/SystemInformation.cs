﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Naracea.Core.Ui.Wpf.PInvoke {
	internal static class SystemInformation {
		[DllImport("user32.dll")]
		static extern uint GetDoubleClickTime();

		public static int DoubleClickTime {
			get {
				return (int)GetDoubleClickTime();
			}
		}
	}
}
