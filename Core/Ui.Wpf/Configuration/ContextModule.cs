﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.Core.Ui.Wpf.Context;
using Naracea.View.Context;
using Naracea.Core.Ui.Wpf.View;
using Naracea.Core.Ui.Wpf.View.Dialogs;
using Autofac;
using MemBus;

namespace Naracea.Core.Ui.Wpf.Configuration {
	internal class ContextModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<WpfViewContext>().As<IViewContext>().SingleInstance();
		}
		#endregion
	}
}
