﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using MemBus;

namespace Naracea.Core.Ui.Wpf.Configuration {
	public class Configurator : IDisposable {
		ContainerBuilder _builder;

		#region IConfigurator Members
		public Configurator(IBus bus) {
			_builder = new ContainerBuilder();
			_builder.RegisterInstance<IBus>(bus);
			new UiModule().Configure(_builder);
			new ContextModule().Configure(_builder);
			this.Container = _builder.Build();
		}

		public IContainer Container { get; private set; }
		#endregion

		#region IDisposable Members
		private bool disposed = false;
		public void Dispose() {
			Dispose(true);
		}

		private void Dispose(bool disposing) {
			if( !this.disposed ) {
				if( disposing ) {
					if( this.Container != null ) {
						this.Container.Dispose();
					}
				}
				disposed = true;
			}
		}
		#endregion
	}
}
