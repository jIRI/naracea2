﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View;
using Autofac;
using Naracea.View.Components;
using MemBus;

namespace Naracea.Core.Ui.Wpf.Configuration {
	internal class UiModule : Naracea.Common.IModule {
		#region IModule Members
		public void Configure(Autofac.ContainerBuilder builder) {
			//appview
			builder.RegisterType<View.ApplicationView>().As<IApplicationView>().As<View.ApplicationView>().SingleInstance();
			
			//dialogs
			builder.RegisterType<View.Dialogs.OpenDocumentDialog>().As<IOpenDocumentDialog>().As<View.Dialogs.OpenDocumentDialog>();
			builder.RegisterType<View.Dialogs.SaveDocumentDialog>().As<ISaveDocumentDialog>().As<View.Dialogs.SaveDocumentDialog>();
			builder.RegisterType<View.Dialogs.ConfirmCloseDocumentDialog>().As<IConfirmCloseDocumentDialog>().As<View.Dialogs.ConfirmCloseDocumentDialog>();
			builder.RegisterType<View.Dialogs.ConfirmBranchDeletionDialog>().As<IConfirmBranchDeletionDialog>().As<View.Dialogs.ConfirmBranchDeletionDialog>();
			builder.RegisterType<View.Dialogs.ErrorOccuredDialog>().As<IErrorOccuredDialog>().As<View.Dialogs.ErrorOccuredDialog>();
			builder.RegisterType<View.Dialogs.PrintDialog>().As<IPrintDialog>().As<View.Dialogs.PrintDialog>();
			builder.RegisterType<View.Dialogs.FileOperationsAreLockedDialog>().As<IFileOperationsAreLockedDialog>().As<View.Dialogs.FileOperationsAreLockedDialog>();
			builder.RegisterType<View.Dialogs.InstallDictionaryDialog>().As<IInstallDictionaryDialog>().As<View.Dialogs.InstallDictionaryDialog>();
			builder.RegisterType<View.Dialogs.ExportBranchDialog>().As<IExportBranchDialog>().As<View.Dialogs.ExportBranchDialog>();

			//views
			builder.RegisterType<View.DocumentView>().As<IDocumentView>().As<View.DocumentView>().ExternallyOwned();
			builder.RegisterType<View.BranchView>().As<IBranchView>().As<View.BranchView>().ExternallyOwned();
			builder.RegisterType<View.TextEditorView>().As<ITextEditorView>().As<View.TextEditorView>().ExternallyOwned();
			builder.RegisterType<View.TimelineView>().As<ITimelineView>().As<View.TimelineView>().ExternallyOwned();
			builder.RegisterType<View.ChangeStreamView>().As<IChangeStreamView>().As<View.ChangeStreamView>().ExternallyOwned();
			builder.RegisterType<View.FindReplaceTextView>().As<IFindReplaceTextView>().As<View.FindReplaceTextView>().ExternallyOwned();
			builder.RegisterType<View.DocumentPropertiesView>().As<IDocumentPropertiesView>().As<View.DocumentPropertiesView>().ExternallyOwned();
			builder.RegisterType<View.BranchPropertiesView>().As<IBranchPropertiesView>().As<View.BranchPropertiesView>().ExternallyOwned();
			builder.RegisterType<View.BranchTextViewerView>().As<IBranchTextViewerView>().As<View.BranchTextViewerView>().ExternallyOwned();
			builder.RegisterType<View.HelpViewerView>().As<IHelpViewerView>().As<View.HelpViewerView>().SingleInstance().ExternallyOwned();

			//view components
			builder.RegisterType<View.Components.ProgressIndicatorComponent>().As<IProgressIndicator>().As<View.Components.ProgressIndicatorComponent>();
			builder.RegisterType<View.Components.IntIndicatorComponent>().As<IIntIndicator>().As<View.Components.IntIndicatorComponent>();
			builder.RegisterType<View.Components.StringIndicatorComponent>().As<IStringIndicator>().As<View.Components.StringIndicatorComponent>();
			builder.RegisterType<View.Components.CurrentColumnIndicatorComponent>().As<ICurrentColumnIndicator>().As<View.Components.CurrentColumnIndicatorComponent>();
			builder.RegisterType<View.Components.CurrentLineIndicatorComponent>().As<ICurrentLineIndicator>().As<View.Components.CurrentLineIndicatorComponent>();
			builder.RegisterType<View.Components.CurrentParagraphIndicatorComponent>().As<ICurrentParagraphIndicator>().As<View.Components.CurrentParagraphIndicatorComponent>();
			builder.RegisterType<View.Components.TextLengthIndicatorComponent>().As<ITextLengthIndicator>().As<View.Components.TextLengthIndicatorComponent>();
			builder.RegisterType<View.Components.CurrentCaretPositionIndicatorComponent>().As<ICurrentCaretPositionIndicator>().As<View.Components.CurrentCaretPositionIndicatorComponent>();
			builder.RegisterType<View.Components.WritingModeIndicatorComponent>().As<IWritingModeIndicator>().As<View.Components.WritingModeIndicatorComponent>();
			builder.RegisterType<View.Components.UpdateAvailablendicatorComponent>().As<IUpdateAvailableIndicator>().As<View.Components.UpdateAvailablendicatorComponent>();
			builder.RegisterType<View.Components.CompositeIndicator>().As<ICompositeIndicator>().As<View.Components.CompositeIndicator>();
			builder.RegisterType<View.Components.EditorIndicators>().As<IEditorIndicators>().As<View.Components.EditorIndicators>();

			//data model
			builder.RegisterType<View.DataModel.SearchMatchItem>().As<ISearchMatchItem>().As<View.DataModel.SearchMatchItem>();
		}
		#endregion
	}
}
