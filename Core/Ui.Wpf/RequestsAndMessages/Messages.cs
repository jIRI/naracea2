﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.Messages {
	#region Preview/Change Font
	public abstract class FontMessageBase {
		public FontFamily FontFamily { get; private set; }
		public double FontSize { get; private set; }
		public FontMessageBase(FontFamily fontFamily, double fontSize) {
			this.FontFamily = fontFamily;
			this.FontSize = fontSize;
		}
	}

	public class PreviewChangeFont : FontMessageBase {
		public PreviewChangeFont(FontFamily fontFamily, double fontSize)
			: base(fontFamily, fontSize) {
		}
	}

	public class ChangeFont : FontMessageBase {
		public ChangeFont(FontFamily fontFamily, double fontSize)
			: base(fontFamily, fontSize) {
		}
	}

	public class CancelChangeFont {
	} 
	#endregion

	#region Preview/Change Colors
	public abstract class ColorMessageBase {
		public SolidColorBrush Foreground { get; private set; }
		public SolidColorBrush Background { get; private set; }
		public ColorMessageBase(SolidColorBrush foreground, SolidColorBrush background) {
			this.Foreground = foreground;
			this.Background = background;
		}
	}

	public class PreviewChangeColor : ColorMessageBase {
		public PreviewChangeColor(SolidColorBrush foreground, SolidColorBrush background)
			: base(foreground, background) {
		}
	}

	public class ChangeColor : ColorMessageBase {
		public ChangeColor(SolidColorBrush foreground, SolidColorBrush background)
			: base(foreground, background) {
		}
	}

	public class CancelChangeColor {
	}
	#endregion

	#region Preview/Change syntax highlighting
	public abstract class SyntaxHighlightingMessageBase {
		public string SyntaxHighlighting { get; private set; }
		public ITextEditorView TextEditorView { get; private set; }
		public SyntaxHighlightingMessageBase(ITextEditorView textEditorView, string highlighting) {
			this.TextEditorView = textEditorView;
			this.SyntaxHighlighting = highlighting;
		}
	}

	public class PreviewSyntaxHighlighting : SyntaxHighlightingMessageBase {
		public PreviewSyntaxHighlighting(ITextEditorView textEditorView, string highlighting)
			: base(textEditorView, highlighting) {
		}
	}

	public class ChangeSyntaxHighlighting : SyntaxHighlightingMessageBase {
		public ChangeSyntaxHighlighting(ITextEditorView textEditorView, string highlighting)
			: base(textEditorView, highlighting) {
		}
	}

	public class CancelSyntaxHighlighting {
	}
	#endregion

	#region Texteditor width
	#endregion

	#region Window messages
	public class ApplicationWindowModeChanged {
		public bool IsMaximized { get; private set; }
		public bool IsMinimized { get; private set; }
		public bool IsNormal { get; private set; }
		public ApplicationWindowModeChanged(bool isMaximized, bool isMinimized, bool isNormal) {
			this.IsMaximized = isMaximized;
			this.IsMinimized = isMinimized;
			this.IsNormal = isNormal;
		}
	}
	#endregion
}
