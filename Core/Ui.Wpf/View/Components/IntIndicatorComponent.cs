﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class IntIndicatorComponent : IndicatorWithDescriptionComponent, IIntIndicator {
		#region ctor
		public IntIndicatorComponent() {
			_control = new Naracea.Core.Ui.Wpf.Controls.ValueIndicator();
			//default aligment is to left
			_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
		}
		#endregion

		#region IIntIndicator Members
		public int Value {
			get {
				int result = 0;
				_control.ExecuteOnUiThread(() => result = int.Parse(_control.value.Text));
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.value.Text = value.ToString());
			}
		}

		public override void Exec(Action action) {
			_control.ExecuteOnUiThread(action);
		}
		#endregion
	}
}
