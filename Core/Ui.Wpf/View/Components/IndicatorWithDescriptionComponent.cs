﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public abstract class IndicatorWithDescriptionComponent : IndicatorComponent, IWithDescription {
		#region ctor
		public IndicatorWithDescriptionComponent() {
		}
		#endregion

		#region IIndicator Members
		protected Naracea.Core.Ui.Wpf.Controls.ValueIndicator _control;
		public override object Control {
			get { return _control; }
		}

		public override void Show() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Collapsed);
		}

		public override void Hide() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Visible);
		}

		public override bool IsVisible {
			get { return _control.Visibility == System.Windows.Visibility.Visible; }
		}
		#endregion

		#region IWithDescription Members
		public string Description {
			get {
				string result = null;
				_control.ExecuteOnUiThread(() => result = _control.description.Text);
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.description.Text = value);
			}
		}
		#endregion
	}
}
