﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class UpdateAvailablendicatorComponent : IndicatorComponent, IUpdateAvailableIndicator {
		#region ctor
		public UpdateAvailablendicatorComponent() {
			_control = new Naracea.Core.Ui.Wpf.Controls.UpdateAvailableIndicator();
			//default alignemnt is to left
			_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			//by default, hide all descriptions
			_control.ignore.Click += (s, e) => this.IgnoreRequested.Raise(this, new Naracea.View.Events.IgnoreUpdateEventArgs());
			_control.downloadLink.Click += (s, e) => this.UpdateRequested.Raise(this, new Naracea.View.Events.DownloadUpdateEventArgs());
		}
		#endregion

		#region IProgressIndicator Members
		public event EventHandler<Naracea.View.Events.IgnoreUpdateEventArgs> IgnoreRequested;
		public event EventHandler<Naracea.View.Events.DownloadUpdateEventArgs> UpdateRequested;

		public string VersionInfo {
			get {
				string result = "";
				_control.ExecuteOnUiThread(() => result = _control.versionInfo.Text);
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.versionInfo.Text = value);
			}
		}

		Naracea.Core.Ui.Wpf.Controls.UpdateAvailableIndicator _control;

		public override object Control {
			get { return _control; }
		}

		public override void Exec(Action action) {
			_control.ExecuteOnUiThread(action);
		}

		public override void Show() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Visible);
		}

		public override void Hide() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Collapsed);
		}

		public override bool IsVisible {
			get { return _control.Visibility == System.Windows.Visibility.Visible; }
		}

		#endregion
	}
}
