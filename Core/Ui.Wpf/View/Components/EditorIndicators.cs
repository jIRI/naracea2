﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using Naracea.Core.Ui.Wpf.Controls;
using System.Windows.Controls;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class EditorIndicators : CompositeIndicator, IEditorIndicators {
		#region Attributes
		ICurrentColumnIndicator _currentColumn;
		ICurrentLineIndicator _currentLine;
		ICurrentParagraphIndicator _currentParagraph;
		ICurrentCaretPositionIndicator _currentCaretPosition;
		ITextLengthIndicator _textLength;
		IWritingModeIndicator _writingMode;
		#endregion


		public EditorIndicators(
			ICurrentColumnIndicator column,
			ICurrentLineIndicator line,
			ICurrentParagraphIndicator paragraph,
			ICurrentCaretPositionIndicator caret,
			ITextLengthIndicator len,
			IWritingModeIndicator writingMode
		)
			: base() {
			_currentColumn = column;
			_currentColumn.Open(this);
			_currentLine = line;
			_currentLine.Open(this);
			_currentParagraph = paragraph;
			_currentParagraph.Open(this);
			_currentCaretPosition = caret;
			_currentCaretPosition.Open(this);
			_textLength = len;
			_textLength.Open(this);
			_writingMode = writingMode;
			_writingMode.Open(this);
			//base ctor was called already, we have the control...
			this.Hide();
		}


		#region IEditorIndicators Members

		public int CurrentColumn {
			set {
				_currentColumn.Value = value;
			}
		}

		public int CurrentLine {
			set {
				_currentLine.Value = value;
			}
		}

		public int CurrentParagraph {
			set {
				_currentParagraph.Value = value;
			}
		}

		public int CurrentCaretPosition {
			set {
				_currentCaretPosition.Value = value;
			}
		}

		public int TextLength {
			set {
				_textLength.Value = value;
			}
		}

		public bool IsOverWriteMode {
			set {
                _writingMode.Value = value ? Naracea.Core.Properties.Resources.WritingModeOverwrite : Naracea.Core.Properties.Resources.WritingModeInsert;
			}
		}

		#endregion
	}
}
