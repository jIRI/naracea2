﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class StringIndicatorComponent : IndicatorWithDescriptionComponent, IStringIndicator {
		#region ctor
		public StringIndicatorComponent() {
			_control = new Naracea.Core.Ui.Wpf.Controls.ValueIndicator();
			//default alignemnt is to left
			_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
		}
		#endregion

		#region IStringIndicator Members
		public string Value {
			get {
				string result = null;
				_control.ExecuteOnUiThread(() => result = _control.value.Text);
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.value.Text = value);
			}
		}

		public override void Exec(Action action) {
			_control.ExecuteOnUiThread(action);
		}
		#endregion
	}
}
