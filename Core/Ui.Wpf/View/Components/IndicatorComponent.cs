﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.Windows.Controls;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public abstract class IndicatorComponent : IIndicator {
		#region ctor
		public IndicatorComponent() {
			this.IsOpen = false;
		}
		#endregion

		#region IIndicator Members
		public abstract void Show();
		public abstract void Hide();

		protected Naracea.View.Compounds.IHasIndicators _view;
		public virtual void Open(Naracea.View.Compounds.IHasIndicators view) {
			Debug.Assert(view != null);
			_view = view;
			_view.RegisterIndicator(this);
			this.IsOpen = true;
		}

		public abstract object Control { get; }
		public abstract void Exec(Action action);

		public void Close() {
			var ctrl = this.Control.As<Control>();
			Debug.Assert(ctrl != null);
			ctrl.ExecuteOnUiThread(() => ctrl.Visibility = System.Windows.Visibility.Collapsed);
			_view.UnregisterIndicator(this);
			this.IsOpen = false;
		}

		public bool IsOpen { get; private set; }
		public abstract bool IsVisible { get; }
		#endregion
	}
}
