﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using Naracea.Core.Ui.Wpf.Controls;
using System.Windows.Controls;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class CompositeIndicator : ICompositeIndicator {

		List<IIndicator> _indicators = new List<IIndicator>();
		public CompositeIndicator() {
			_control = new CompositeIndicatorPanel();
			this.IsOpen = false;
		}

		#region IIndicatorContainer Members
		public IEnumerable<IIndicator> Indicators {
			get { return _indicators; }
		}

		#endregion

		#region IHasControl Members
		protected CompositeIndicatorPanel _control;
		public object Control {
			get { return _control; }
		}

		public void Exec(Action action) {
			_control.ExecuteOnUiThread(action);
		}
		#endregion

		#region IIndicator Members
		Naracea.View.Compounds.IHasIndicators _view;
		public void Open(Naracea.View.Compounds.IHasIndicators view) {
			Debug.Assert(view != null);

			_view = view;
			_view.RegisterIndicator(this);
			this.IsOpen = true;
		}

		public virtual void Close() {
			while(_indicators.Count > 0) {
				_indicators[0].Close();
			}
			_view.UnregisterIndicator(this);
			this.IsOpen = false;
		}

		public bool IsOpen { get; private set; }
		public bool IsVisible {
			get {
				return _control.Visibility == System.Windows.Visibility.Visible;
			}
		}

		public void Show() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Visible);
		}

		public void Hide() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Collapsed);
		}
		#endregion

		#region IHasIndicators Members
		public void RegisterIndicator(IIndicator indicator) {
			_indicators.Add(indicator);
			_control.ExecuteOnUiThread(() => _control.Add(indicator.Control.As<Control>()));
		}

		public void UnregisterIndicator(IIndicator indicator) {
			_indicators.Remove(indicator);
			_control.ExecuteOnUiThread(() => _control.Remove(indicator.Control.As<Control>()));
		}
		#endregion
	}
}
