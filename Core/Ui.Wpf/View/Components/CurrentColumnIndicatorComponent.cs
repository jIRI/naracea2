﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class CurrentColumnIndicatorComponent : IntIndicatorComponent, ICurrentColumnIndicator {
		#region ctor
		public CurrentColumnIndicatorComponent() {
            this.Description = Naracea.Core.Properties.Resources.ColumnIndicatorDescription;
		}
		#endregion
	}
}
