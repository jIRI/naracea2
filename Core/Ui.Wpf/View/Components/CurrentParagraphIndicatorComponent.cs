﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class CurrentParagraphIndicatorComponent : IntIndicatorComponent, ICurrentParagraphIndicator {
		#region ctor
		public CurrentParagraphIndicatorComponent() {
            this.Description = Naracea.Core.Properties.Resources.ParagraphIndicatorDescription;
		}
		#endregion
	}
}
