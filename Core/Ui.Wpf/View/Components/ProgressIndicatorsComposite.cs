﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using Naracea.Core.Ui.Wpf.Controls;
using System.Windows.Controls;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class ProgressIndicatorComposite : CompositeIndicator {
		public ProgressIndicatorComposite() {
			_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
		}
	}
}
