﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class ProgressIndicatorComponent : IndicatorComponent, IProgressIndicator {
		#region ctor
		public ProgressIndicatorComponent() {
			_control = new Naracea.Core.Ui.Wpf.Controls.ProgressIndicator();
			//default alignemnt is to left
			_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
			//by default, hide all descriptions
			this.ShortMessage = null;
			this.DetailedMessage = null;
			//default is indeterminate
			this.IsIndeterminate = true;
			//default is not cancellable
			this.IsCancellable = false;
			//default range is 0 to 100
			this.From = 0;
			this.To = 100;
			this.Value = 0;
			
			_control.cancel.Click += (s, e) => this.CancelRequested.Raise(this, new Naracea.View.Events.CancelOperationEventArgs());
		}
		#endregion

		#region IProgressIndicator Members
		public event EventHandler<Naracea.View.Events.CancelOperationEventArgs> CancelRequested;

		Naracea.Core.Ui.Wpf.Controls.ProgressIndicator _control;

		public override object Control {
			get { return _control; }
		}

		public override void Exec(Action action) {
			_control.ExecuteOnUiThread(action);
		}

		public override void Show() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Visible);
		}

		public override void Hide() {
			_control.ExecuteOnUiThread(() => _control.Visibility = System.Windows.Visibility.Collapsed);
		}

		public override bool IsVisible {
			get { return _control.Visibility == System.Windows.Visibility.Visible; }
		}

		public string ShortMessage {
			get {
				string message = null;
				_control.ExecuteOnUiThread(() => message = _control.shortMessage.Text);
				return message;
			}
			set {
				_control.ExecuteOnUiThread(() => {
					_control.shortMessage.Visibility = string.IsNullOrEmpty(value) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
					_control.shortMessage.Text = value;
				});
			}
		}

		public string DetailedMessage {
			get {
				string message = null;
				_control.ExecuteOnUiThread(() => message = _control.detailedMessage.Text);
				return message;
			}
			set {
				_control.ExecuteOnUiThread(() => {
					_control.detailedMessage.Visibility = string.IsNullOrEmpty(value) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
					_control.detailedMessage.Text = value;
				});
			}
		}

		public int DetailedMessageCapacity { 
			get {
				int result = 40;
				return result;
			} 
		}

		public bool IsIndeterminate {
			get {
				bool result = false;
				_control.ExecuteOnUiThread(() => result = _control.progressBar.IsIndeterminate);
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.progressBar.IsIndeterminate = value);
			}
		}

		public bool IsCancellable {
			get {
				bool result = false;
				_control.ExecuteOnUiThread(() => result = _control.cancel.Visibility == System.Windows.Visibility.Visible);
				return result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.cancel.Visibility = value ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed);
			}
		}

		public int From {
			get {
				double result = 0.0;
				_control.ExecuteOnUiThread(() => result = _control.progressBar.Minimum);
				return (int)result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.progressBar.Minimum = value);
			}
		}

		public int To {
			get {
				double result = 0.0;
				_control.ExecuteOnUiThread(() => result = _control.progressBar.Maximum);
				return (int)result;
			}
			set {
				_control.ExecuteOnUiThread(() => _control.progressBar.Maximum = value);
			}
		}

		public int Value {
			get {
				double result = 0.0;
				_control.ExecuteOnUiThread(() => result = _control.progressBar.Value);
				return (int)result;
			}
			set {
				_control.ExecuteOnUiThreadAsync(() => _control.progressBar.Value = value);
			}
		}
		#endregion
	}
}
