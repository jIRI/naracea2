﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;
using System.Diagnostics;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Components {
	public class WritingModeIndicatorComponent : StringIndicatorComponent, IWritingModeIndicator {
		#region ctor
		public WritingModeIndicatorComponent() {
            this.Description = Naracea.Core.Properties.Resources.WritingIndicatorDescription;
		}
		#endregion
	}
}
