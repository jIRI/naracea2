﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Ui.Wpf.Forms;
using Naracea.View.Events;
using Naracea.View;
using System.Windows;
using System.Threading;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Core.Ui.Wpf.View.Components;
using MemBus;
using System.Windows.Controls.Ribbon;
using System.Windows.Media;
using System.Threading.Tasks;
using Naracea.Common;
using Naracea.View.Messages;
using Naracea.View.Requests;
using System.Collections.ObjectModel;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	public class FontSetter {
		#region Private attributes
		Control _control;
		FontFamily _family;
		FontSize _size;
		#endregion

		#region ctor
		public FontSetter(Control control) {
			_control = control;
			_family = _control.FontFamily;
			_size = new FontSize(_control.FontSize);
		}
		#endregion

		#region Color settings
		public void Preview(FontFamily family, double size) {
			this.Preview(family, new FontSize(size));
		}

		public void Preview(FontFamily family, FontSize size) {
			if( family != null ) {
				_control.FontFamily = family;
			}
			if( size != null ) {
				_control.FontSize = size.Size;
			}
		}

		public void CancelPreview() {
			this.Apply();
		}

		public void SetFamily(FontFamily family) {
			_family = family;
		}

		public void SetSize(FontSize size) {
			_size = size;
		}

		public void SetSize(double size) {
			_size = new FontSize(size);
		}

		public void Apply() {
			if( _family != null ) {
				_control.FontFamily = _family;
			}
			if( _size != null ) {
				_control.FontSize = _size.Size;
			}
		}
		#endregion
	}
}
