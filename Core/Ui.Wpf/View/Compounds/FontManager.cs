﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Ui.Wpf.Forms;
using Naracea.View.Events;
using Naracea.View;
using System.Windows;
using System.Threading;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Core.Ui.Wpf.View.Components;
using MemBus;
using System.Windows.Controls.Ribbon;
using System.Windows.Media;
using System.Threading.Tasks;
using Naracea.Common;
using Naracea.View.Messages;
using Naracea.View.Requests;
using System.Collections.ObjectModel;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	public class FontManager {
		#region Private attributes
		FontSize[] _defaultFontSizes = new FontSize[] {
			new FontSize(8), new FontSize(9), new FontSize(10), new FontSize(11), 
			new FontSize(12), new FontSize(14), new FontSize(16), new FontSize(18), 
			new FontSize(20), new FontSize(22), new FontSize(24), new FontSize(28), 
			new FontSize(36), new FontSize(48), new FontSize(72),
		};
		List<FontFamily> _availableFontFamilies;
		ISettings _settings;
		Control _control;
		#endregion

		#region ctors
		public FontManager(Control control, ISettings settings) {
			_control = control;
			_availableFontFamilies = Fonts.SystemFontFamilies.OrderBy(i => i.Source).ToList();
			_settings = settings;
			this.DefaultFontFamily = _control.FontFamily;
			this.IsAutoFontFamilySelected = true;
			this.DefaultFontSize = new FontSize(_control.FontSize);
			this.IsAutoFontSizeSelected = true;
		}
		#endregion

		#region Font settings
		public bool IsAutoFontFamilySelected { get; set; }
		public bool IsAutoFontSizeSelected { get; set; }
		public ICollection<FontFamily> AvailableFontFamilies {
			get {
				return _availableFontFamilies;
			}
		}
		public ICollection<FontSize> DefaultFontSizes {
			get {
				return _defaultFontSizes;
			}
		}
		public FontFamily DefaultFontFamily { get; private set; }
		public FontSize DefaultFontSize { get; private set; }
		public FontFamily PreferredFontFamily { get; set; }
		public FontSize PreferredFontSize { get; set; }
		public FontFamily CurrentFontFamily {
			get {
				if( this.IsAutoFontFamilySelected ) {
					return this.DefaultFontFamily;
				} else {
					return this.PreferredFontFamily;
				}
			}
		}
		public FontSize CurrentFontSize {
			get {
				if( this.IsAutoFontSizeSelected ) {
					return this.DefaultFontSize;
				} else {
					return this.PreferredFontSize;
				}
			}
		}

		public void StoreFontSettings() {
			if( !this.IsAutoFontFamilySelected) {
				_settings.Set(Naracea.Common.Settings.ViewTextEditorFontFamilyName, this.PreferredFontFamily.Source);
			} else {
				_settings.Clear(Naracea.Common.Settings.ViewTextEditorFontFamilyName);
			}
			if( !this.IsAutoFontSizeSelected ) {
				_settings.Set(Naracea.Common.Settings.ViewTextEditorFontSize, this.PreferredFontSize.Size);
			} else {
				_settings.Clear(Naracea.Common.Settings.ViewTextEditorFontSize);
			}
		}

		public void RestoreFontSettings() {
			if( _settings.HasValue(Naracea.Common.Settings.ViewTextEditorFontFamilyName) ) {
				var fontSource = _settings.Get<string>(Naracea.Common.Settings.ViewTextEditorFontFamilyName);
				var fontQuery = from f in _availableFontFamilies
												where f.Source == fontSource
												select f;
				this.IsAutoFontFamilySelected = !fontQuery.Any();
				if( !this.IsAutoFontFamilySelected ) {
					this.PreferredFontFamily = fontQuery.First();
				}
			} 

			if( _settings.HasValue(Naracea.Common.Settings.ViewTextEditorFontSize) ) {
				var fontSize = _settings.Get<double>(Naracea.Common.Settings.ViewTextEditorFontSize);
				var fontSizeQuery = from f in _defaultFontSizes
														where f.Size == fontSize
														select f;
				this.IsAutoFontSizeSelected = !fontSizeQuery.Any();
				if( !this.IsAutoFontSizeSelected ) {
					this.PreferredFontSize = fontSizeQuery.First();
				}
			}
		}
		#endregion
	}
}
