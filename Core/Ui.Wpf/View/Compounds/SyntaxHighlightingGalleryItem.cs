﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	public class SyntaxHighlightingGalleryItem {
		public static readonly SyntaxHighlightingGalleryItem None = new SyntaxHighlightingGalleryItem();

		public string Name { get; set; }

		public override string ToString() {
			return this.Name;
		}
	}
}
