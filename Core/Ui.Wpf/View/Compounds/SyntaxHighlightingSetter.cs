﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class SyntaxHighlightingSetter {
		#region Private attributes
		TextEditorView _editor;
		string _currentHighlighting;
		#endregion

		#region ctor
		public SyntaxHighlightingSetter(TextEditorView editor) {
			_editor = editor;
			_currentHighlighting = _editor.SyntaxHighlighting;
		}
		#endregion

		#region Highlighting settings
		public void Preview(string highlighting) {
			_editor.SyntaxHighlighting = highlighting;
		}

		public void CancelPreview() {
			this.Apply();
		}

		public void SetSyntaxHighlighting(string highlighting) {
			_currentHighlighting = highlighting;
		}
		
		public void Apply() {
			_editor.SyntaxHighlighting = _currentHighlighting;
		}
		#endregion
	}
}
