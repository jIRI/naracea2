﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.Common;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class WindowSizeRestorer {
		#region Private attributes
		Rect _lastValidRect;
		Window _window;
		ISettings _settings;
		string _windowStateSettingKey;
		string _windowRectSettingKey;
		#endregion

		#region ctors
		public WindowSizeRestorer(Window window, ISettings settings, string settingKey) {
			_window = window;
			_settings = settings;
			_windowRectSettingKey = Naracea.Common.Settings.ViewWindowRect + "." + settingKey;
			_windowStateSettingKey = Naracea.Common.Settings.ViewWindowState + "." + settingKey;
		}
		#endregion 

		#region Window bounds
		public bool WasOpen { get; set; }

		public void StoreWindowBounds() {
			//store window state and position
			_settings.Set(_windowStateSettingKey, _window.WindowState);
			_settings.Set(_windowRectSettingKey, _lastValidRect);
		}

		public void RestoreWindowBounds() {
			WindowState windowState = WindowState.Normal;
			if( _window.WindowStyle != WindowStyle.ToolWindow ) {
				windowState = _settings.Get<WindowState>(_windowStateSettingKey);
			}
			_lastValidRect = _settings.Get<Rect>(_windowRectSettingKey);

			_window.WindowState = WindowState.Normal;
			_window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

			var virtualScreenRect = new Rect(
				SystemParameters.VirtualScreenLeft,
				SystemParameters.VirtualScreenTop,
				SystemParameters.VirtualScreenWidth,
				SystemParameters.VirtualScreenHeight
			);

			// check if the saved bounds are nonzero and visible on any screen
			if( _lastValidRect.Height != 0 && _lastValidRect.Width != 0 && virtualScreenRect.IntersectsWith(_lastValidRect) ) {
				_window.WindowStartupLocation = WindowStartupLocation.Manual;
				_window.Top = _lastValidRect.Top;
				_window.Left = _lastValidRect.Left;
				_window.Width = _lastValidRect.Width;
				_window.Height = _lastValidRect.Height;
				_window.WindowState = windowState;
			} else {
				if( _lastValidRect.Height != 0 && _lastValidRect.Width != 0 ) {
					_window.Width = _lastValidRect.Width;
					_window.Height = _lastValidRect.Height;
				}
			}
		}

		public void HandleSizeChanged() {
			if( this.WasOpen && _window.WindowState == WindowState.Normal ) {
				_lastValidRect = new Rect(
					_window.Left, 
					_window.Top,
					_window.Width,
					_window.Height
				);
			}
		}
		#endregion	
	}
}
