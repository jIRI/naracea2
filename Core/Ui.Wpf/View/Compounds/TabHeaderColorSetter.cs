﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Diagnostics;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class TabHeaderColorSetter {
		#region Private attributes
		WpfExtension.CustomControls.TabControl _control;
		SolidColorBrush _foreground;
		SolidColorBrush _background;
		#endregion

		#region ctor
		public TabHeaderColorSetter(WpfExtension.CustomControls.TabControl control) {
			_control = control;
			_foreground = _control.Foreground as SolidColorBrush;
			_background = _control.TabItemNormalBackground as SolidColorBrush;
		}
		#endregion

		#region Color settings
		public void Preview(SolidColorBrush foreground, SolidColorBrush background) {
			if( foreground != null ) {
				this.UpdateTabForegroundColor(foreground);
			}
			if( background != null ) {
				this.UpdateTabBackgroundColors(background);
			}
		}

		public void CancelPreview() {
			this.Apply();
		}

		public void SetForeground(SolidColorBrush foreground) {
			_foreground = foreground;
		}

		public void SetBackground(SolidColorBrush background) {
			_background = background;
		}

		public void Apply() {
			if( _foreground != null ) {
				this.UpdateTabForegroundColor(_foreground);
			}
			if( _background != null ) {
				this.UpdateTabBackgroundColors(_background);
			}
		}
		#endregion

		#region Private methods
		private void UpdateTabForegroundColor(SolidColorBrush foreground) {
			const int colorShift = 20;
			var normalColor = foreground.Color;
			if( normalColor.R > colorShift ) {
				normalColor.R -= colorShift;
			} else {
				normalColor.R += colorShift;
			}
			if( normalColor.G > colorShift ) {
				normalColor.G -= colorShift;
			} else {
				normalColor.G += colorShift;
			}
			if( normalColor.B > colorShift ) {
				normalColor.B -= colorShift;
			} else {
				normalColor.B += colorShift;
			}
			_control.Foreground = new SolidColorBrush(normalColor);
			_control.TabItemNormalForeground = _control.Foreground;

			_control.TabItemSelectedForeground = foreground;
		}

		private void UpdateTabBackgroundColors(SolidColorBrush background) {
			_control.TabItemSelectedBackground = background;

			const int colorShift = 15;

			var mouseOverColor = background.Color;
			if( mouseOverColor.R > colorShift ) {
				mouseOverColor.R -= colorShift / 2;
			} else {
				mouseOverColor.R += colorShift / 2;
			}
			if( mouseOverColor.G > colorShift ) {
				mouseOverColor.G -= colorShift / 2;
			} else {
				mouseOverColor.G += colorShift / 2;
			}
			if( mouseOverColor.B > colorShift ) {
				mouseOverColor.B -= colorShift / 2;
			} else {
				mouseOverColor.B += colorShift / 2;
			}
			_control.TabItemMouseOverBackground = new SolidColorBrush(mouseOverColor);

			var normalColor = background.Color;
			if( normalColor.R > colorShift ) {
				normalColor.R -= colorShift;
			} else {
				normalColor.R += colorShift;
			}
			if( normalColor.G > colorShift ) {
				normalColor.G -= colorShift;
			} else {
				normalColor.G += colorShift;
			}
			if( normalColor.B > colorShift ) {
				normalColor.B -= colorShift;
			} else {
				normalColor.B += colorShift;
			}
			_control.TabItemNormalBackground = new SolidColorBrush(normalColor);
		}
		#endregion
	}
}
