﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	public class NamedBrush {
		public SolidColorBrush Brush { get; set; }
		public string Name { get; set; }
	}
}
