﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	public class FontSize {
		public double Size { get; set; }
		public FontSize(double size) {
			this.Size = size;
		}
	}
}
