﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Naracea.Common;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class ColorManager {
		#region Private attributes
		Control _control;
		ISettings _settings;
		#endregion

		#region ctor
		public ColorManager(Control control, ISettings settings) {
			_control = control;
			_settings = settings;
			this.DefaultBackground = new NamedBrush { Brush = new SolidColorBrush(SystemColors.WindowColor) };
			this.DefaultForeground = new NamedBrush { Brush = new SolidColorBrush(SystemColors.WindowTextColor) };
			this.IsAutoForegroundColorSelected = true;
			this.IsAutoBackgroundColorSelected = true;
		}
		#endregion

		#region Color settings
		public bool IsAutoForegroundColorSelected { get; set; }
		public bool IsAutoBackgroundColorSelected { get; set; }
		public NamedBrush DefaultForeground { get; private set; }
		public NamedBrush DefaultBackground { get; private set; }
		public NamedBrush CurrentForeground {
			get {
				if( this.IsAutoForegroundColorSelected ) {
					return this.DefaultForeground;
				} else {
					return this.PreferredForeground;
				}
			}
		}
		public NamedBrush CurrentBackground {
			get {
				if( this.IsAutoBackgroundColorSelected ) {
					return this.DefaultBackground;
				} else {
					return this.PreferredBackground;
				}
			}
		}

		public NamedBrush PreferredForeground { get; set; }
		public NamedBrush PreferredBackground { get; set; }

		List<NamedBrush> _availableColors = new List<NamedBrush>();
		public ICollection<NamedBrush> AvailableColors {
			get {
				return _availableColors;
			}
		}

		public void RestoreColors() {
			var colorsProperties = typeof(Brushes).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
			foreach( var property in colorsProperties ) {
				var brush = (SolidColorBrush)property.GetValue(null, null);
				_availableColors.Add(new NamedBrush {
					Brush = brush
#if DEBUG
					,Name = property.Name
#endif
				});
			}

			var brushConverter = new BrushConverter();
			if( _settings.HasValue(Naracea.Common.Settings.ViewForegroundColor) ) {
				var textBrushString = _settings.Get<string>(Naracea.Common.Settings.ViewForegroundColor);
				var foregroundBrushQuery = from b in _availableColors
																	 where brushConverter.ConvertToString(b.Brush) == textBrushString
																	 select b;
				this.IsAutoForegroundColorSelected = !foregroundBrushQuery.Any();
				if( !this.IsAutoForegroundColorSelected ) {
					this.PreferredForeground = foregroundBrushQuery.First();
				}
			}

			if( _settings.HasValue(Naracea.Common.Settings.ViewBackgroundColor) ) {
				var backgroundBrushString = _settings.Get<string>(Naracea.Common.Settings.ViewBackgroundColor);
				var backgroundBrushQuery = from b in _availableColors
																	 where brushConverter.ConvertToString(b.Brush) == backgroundBrushString
																	 select b;
				this.IsAutoBackgroundColorSelected = !backgroundBrushQuery.Any();
				if( !this.IsAutoBackgroundColorSelected ) {
					this.PreferredBackground = backgroundBrushQuery.First();
				}
			}
		}

		public void StoreColorSettings() {
			var brushConverter = new BrushConverter();
			if( !this.IsAutoForegroundColorSelected ) {
				_settings.Set(Naracea.Common.Settings.ViewForegroundColor, brushConverter.ConvertToString(this.PreferredForeground.Brush));
			} else {
				_settings.Clear(Naracea.Common.Settings.ViewForegroundColor);
			}
			if( !this.IsAutoBackgroundColorSelected ) {
				_settings.Set(Naracea.Common.Settings.ViewBackgroundColor, brushConverter.ConvertToString(this.PreferredBackground.Brush));
			} else {
				_settings.Clear(Naracea.Common.Settings.ViewBackgroundColor);
			}
		}
		#endregion
	}
}
