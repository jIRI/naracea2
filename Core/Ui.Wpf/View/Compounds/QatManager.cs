﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Ui.Wpf.Forms;
using Naracea.View.Events;
using Naracea.View;
using System.Windows;
using System.Threading;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Core.Ui.Wpf.View.Components;
using MemBus;
using System.Windows.Media;
using System.Threading.Tasks;
using Naracea.Common;
using Naracea.View.Messages;
using Naracea.View.Requests;
using System.Collections.ObjectModel;
using System.Windows.Controls.Ribbon;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class QatManager {
		#region Private attributes
		Ribbon _ribbon;
		ISettings _settings;
		#endregion

		#region ctors
		public QatManager(Ribbon ribbon, ISettings settings) {
			_ribbon = ribbon;
			_settings = settings;
		}
		#endregion

		#region QAT settings
		public void RestoreQatItems() {
			//restore ribbon's quick access toolbar items
			if( _settings.HasValue(Naracea.Common.Settings.ViewMainQatItems) ) {
				var qatItemsNames = _settings.Get<List<string>>(Naracea.Common.Settings.ViewMainQatItems);
				if( qatItemsNames == null ) {
					return;
				}

				var currentQatItemsIds = this.GetQatItemsIds();
				foreach( var name in qatItemsNames ) {
					// check whether we didn't add this item already
					if( !currentQatItemsIds.Contains(name)) {
						//QuickAccessToolBarId needs to be same as the Name of the control
						var control = _ribbon.FindName(name);
						if( control != null ) {
							object qatControl = this.CloneRibbonControl(control);
							if( qatControl != null ) {
								_ribbon.QuickAccessToolBar.Items.Add(qatControl);
							}
						}
					}
				}
			}
		}

		public void StoreQatItems() {
			//store ribbon's quick access toolbar
			var qatItemsNames = this.GetQatItemsIds();
			_settings.Set(Naracea.Common.Settings.ViewMainQatItems, qatItemsNames);
		}
		#endregion

		#region Private methods
		private List<string> GetQatItemsIds() {
			var ids = new List<string>();
			foreach( dynamic item in _ribbon.QuickAccessToolBar.Items ) {
				Debug.Assert(item.QuickAccessToolBarId is string);
				ids.Add( item.QuickAccessToolBarId as string );
			}
			return ids;
		}

		private object CloneRibbonControl(object control) {
			object qatControl = null;
			if( control.GetType() == typeof(RibbonButton) ) {
				var typedControl = control as RibbonButton;
				var qatTypedControl = new RibbonButton();
				qatTypedControl.Command = typedControl.Command;
				qatTypedControl.CommandParameter = typedControl.CommandParameter;
				qatTypedControl.SmallImageSource = typedControl.SmallImageSource;
				qatTypedControl.ToolTip = typedControl.ToolTip;
				qatTypedControl.QuickAccessToolBarId = typedControl.QuickAccessToolBarId;
				qatControl = qatTypedControl;
			} else if( control.GetType() == typeof(RibbonSplitButton) ) {
				var typedControl = control as RibbonSplitButton;
				var qatTypedControl = new RibbonSplitButton();
				qatTypedControl.Command = typedControl.Command;
				qatTypedControl.CommandParameter = typedControl.CommandParameter;
				qatTypedControl.SmallImageSource = typedControl.SmallImageSource;
				qatTypedControl.ToolTip = typedControl.ToolTip;
				qatTypedControl.QuickAccessToolBarId = typedControl.QuickAccessToolBarId;
				foreach( var menuItem in typedControl.Items ) {
					qatTypedControl.Items.Add(this.CloneRibbonControl(menuItem));
				}
				qatControl = qatTypedControl;
			} else if( control.GetType() == typeof(RibbonMenuItem) ) {
				var typedControl = control as RibbonMenuItem;
				var qatTypedControl = new RibbonMenuItem();
				qatTypedControl.Command = typedControl.Command;
				qatTypedControl.CommandParameter = typedControl.CommandParameter;
				qatTypedControl.ImageSource = typedControl.ImageSource;
				qatTypedControl.QuickAccessToolBarImageSource = typedControl.QuickAccessToolBarImageSource;
				qatTypedControl.Header = typedControl.Header;
				qatTypedControl.ToolTip = typedControl.ToolTip;
				qatTypedControl.QuickAccessToolBarId = typedControl.QuickAccessToolBarId;
				foreach( var menuItem in typedControl.Items ) {
					qatTypedControl.Items.Add(this.CloneRibbonControl(menuItem));
				}
				qatControl = qatTypedControl;
			} else if( control.GetType() == typeof(RibbonApplicationMenuItem) ) {
				var typedControl = control as RibbonApplicationMenuItem;
				var qatTypedControl = new RibbonButton();
				qatTypedControl.Command = typedControl.Command;
				qatTypedControl.CommandParameter = typedControl.CommandParameter;
				qatTypedControl.SmallImageSource = typedControl.QuickAccessToolBarImageSource;
				qatTypedControl.ToolTip = typedControl.ToolTip;
				qatTypedControl.QuickAccessToolBarId = typedControl.QuickAccessToolBarId;
				qatControl = qatTypedControl;
			}
			return qatControl;
		}
		#endregion
	}
}
