﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Compounds {
	internal class ColorSetter {
		#region Private attributes
		Control _control;
		SolidColorBrush _foreground;
		SolidColorBrush _background;
		#endregion

		#region ctor
		public ColorSetter(Control control) {
			_control = control;
			_foreground = _control.Foreground as SolidColorBrush;
			_background = _control.Background as SolidColorBrush;
		}
		#endregion

		#region Color settings
		public void Preview(SolidColorBrush foreground, SolidColorBrush background) {
			if( foreground != null ) {
				_control.Foreground = foreground;
			}
			if( background != null ) {
				_control.Background = background;
			}
		}

		public void CancelPreview() {
			this.Apply();
		}

		public void SetForeground(SolidColorBrush foreground) {
			_foreground = foreground;
		}

		public void SetBackground(SolidColorBrush background) {
			_background = background;
		}
		
		public void Apply() {
			if( _foreground != null ) {
				_control.Foreground = _foreground;
			}
			if( _background != null ) {
				_control.Background = _background;
			}
		}
		#endregion
	}
}
