﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Threading;

namespace Naracea.Core.Ui.Wpf.View.Extensions {
	public static class UiThreadExec {
		public static void ExecuteOnUiThread(this Control control, Action action) {
			Debug.Assert(control != null);
			control.Dispatcher.ExecuteOnUiThread(action);
		}

		public static void ExecuteOnUiThreadAsync(this Control control, Action action) {
			Debug.Assert(control != null);
			control.Dispatcher.ExecuteOnUiThreadAsync(action);
		}

		public static void ExecuteOnUiThread(this Dispatcher dispatcher, Action action) {
			if( Thread.CurrentThread != dispatcher.Thread ) {
				dispatcher.Invoke(action, null);
			} else {
				action.Invoke();
			}
		}

		public static void ExecuteOnUiThreadAsync(this Dispatcher dispatcher, Action action) {
			dispatcher.BeginInvoke(action, null);
		}
	}
}
