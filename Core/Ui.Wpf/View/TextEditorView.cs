﻿using MemBus;
using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Compounds;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using Naracea.View.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View {
	public class TextEditorView : ViewBase, ITextEditorView {
		#region Private attributes
		bool _pendingFocus;
		FontFamily _currentFontFamily;
		double _currentFontSize;
		ColorSetter _colorSetter;
		FontSetter _fontSetter;
		SyntaxHighlightingSetter _highlightingSetter;
		#endregion

		public TextEditorView(IBus bus)
			: base(bus) {
			this.ViewControl = new Controls.TextEditor();
			this.ViewControl.Width = double.NaN;
			this.ViewControl.Height = double.NaN;
			_currentFontFamily = this.ViewControl.FontFamily;
			_currentFontSize = this.ViewControl.FontSize;
			_pendingFocus = false;
			this.CanEdit = true;
			this.InstallHandlers();
		}

		#region ITextEditorView Members
		public bool CanEdit {
			get {
				return this.ViewControl.CanEdit;
			}
			set {
				this.ViewControl.CanEdit = value;
			}
		}

		public bool CanUndo {
			get {
				return this.ViewControl.CanUndo;
			}
			set {
				this.ViewControl.CanUndo = value;
			}
		}

		public bool CanRedo {
			get {
				return this.ViewControl.CanRedo;
			}
			set {
				this.ViewControl.CanRedo = value;
			}
		}

		public string Text {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.Text = value);
			}
		}

		public string SelectedText {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.SelectedText);
				return result;
			}
		}

		public int CaretPosition {
			get {
				int pos = 0;
				this.Exec(() => pos = this.ViewControl.CaretOffset);
				return pos;
			}
			set {
				this.Exec(() => {
					this.ViewControl.CaretOffset = value;
					this.ViewControl.TextArea.Caret.BringCaretToView();
				});
			}
		}

		public int CurrentLine {
			get {
				int pos = 0;
				this.Exec(() => pos = this.ViewControl.TextArea.Caret.TextLine);
				return pos;
			}
			set {
				this.Exec(() => {
					this.ViewControl.TextArea.Caret.TextLine = value;
					this.ViewControl.TextArea.Caret.BringCaretToView();
				});
			}
		}

		public int CurrentColumn {
			get {
				int pos = 0;
				this.Exec(() => pos = this.ViewControl.TextArea.Caret.TextLineColumn);
				return pos;
			}
		}

		public int CurrentParagraph {
			get {
				int pos = 0;
				this.Exec(() => pos = this.ViewControl.TextArea.Caret.Line);
				return pos;
			}
			set {
				this.Exec(() => {
					this.ViewControl.TextArea.Caret.Line = value;
					this.ViewControl.TextArea.Caret.BringCaretToView();
				});
			}
		}


		public int TextLength {
			get {
				int pos = 0;
				this.Exec(() => pos = this.ViewControl.Document.TextLength);
				return pos;
			}
		}

		public bool IsOverwriteMode {
			get {
				bool res = false;
				this.Exec(() => res = this.ViewControl.TextArea.OverstrikeMode);
				return res;
			}
		}

		public bool WrapsLines {
			get {
				bool wraps = false;
				this.Exec(() => wraps = this.ViewControl.WordWrap);
				return wraps;
			}
			set {
				this.Exec(() => {
					this.ViewControl.WordWrap = value;
					this.ViewControl.Options.EnableRectangularSelection = !value;
				});
			}
		}

		public bool ShowWhitespaces {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.Options.ShowEndOfLine);
				return result;
			}
			set {
				this.Exec(() => {
					this.ViewControl.Options.ShowEndOfLine = value;
					this.ViewControl.Options.ShowTabs = value;
					this.ViewControl.Options.ShowSpaces = value;
				});
			}
		}

		public int TabSize {
			get {
				int res = 0;
				this.Exec(() => res = this.ViewControl.Options.IndentationSize);
				return res;
			}
			set {
				this.Exec(() => {
					if (this.ViewControl.Options.IndentationSize != value) {
						this.ViewControl.Options.IndentationSize = value;
					}
				});
			}
		}

		public bool KeepTabs {
			get {
				bool res = false;
				this.Exec(() => res = !this.ViewControl.Options.ConvertTabsToSpaces);
				return res;
			}
			set {
				this.Exec(() => this.ViewControl.Options.ConvertTabsToSpaces = !value);
			}
		}

		public string SyntaxHighlighting {
			get {
				string res = null;
				this.Exec(() => res = this.ViewControl.SyntaxHighlighting != null ? this.ViewControl.SyntaxHighlighting.Name : null);
				return res;
			}
			set {
				this.Exec(() =>
					this.ViewControl.SyntaxHighlighting = (value != null) ? ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition(value) : null
				);
			}
		}

		public string SpellcheckLanguageName { get; set; }

		public void InsertTextBeforePosition(int pos, string text) {
			this.Exec(() => this.ViewControl.InsertTextBeforePosition(pos, text));
		}

		public void InsertTextBehindPosition(int pos, string text) {
			this.Exec(() => this.ViewControl.InsertTextBehindPosition(pos, text));
		}

		public void DeleteText(int pos, string text) {
			this.Exec(() => this.ViewControl.DeleteText(pos, text));
		}

		public void BackspaceText(int pos, string text) {
			this.Exec(() => this.ViewControl.BackspaceText(pos, text));
		}

		public void SetTextSilently(string text) {
			this.Exec(() => this.ViewControl.SetTextSilently(text));
		}

		public void BeginUpdate() {
			this.Exec(() => this.ViewControl.Document.BeginUpdate());
		}

		public void EndUpdate() {
			this.Exec(() => {
				this.ViewControl.Document.EndUpdate();
				this.ViewControl.TextArea.Caret.BringCaretToView();
			});
		}

		public void Insert(int position, string text) {
			this.Exec(() => this.ViewControl.InsertTextWithChange(position, text));
		}

		public void Insert(string text) {
			this.Exec(() => this.ViewControl.InsertTextWithChange(text));
		}

		public void Delete(int position, int length) {
			this.Exec(() => this.ViewControl.DeleteTextWithChange(position, length));
		}

		public void Delete(int length) {
			this.Exec(() => this.ViewControl.DeleteTextWithChange(length));
		}

		public void Select(int position, int length) {
			this.Exec(() => {
				var textLen = this.ViewControl.Document.TextLength;
				if (position < textLen && (position + length) < textLen) {
					this.ViewControl.TextArea.Caret.Offset = position;
					this.ViewControl.TextArea.Caret.BringCaretToView();
					this.ViewControl.Select(position, length);
				}
			});
		}

		public void ReplaceSelection(string replaceWith) {
			this.Exec(() => this.ViewControl.SelectedText = replaceWith);
		}

		public void SetSpellingErrors(IEnumerable<Tuple<int, int>> errors) {
			var viewItems = from err in errors
											select new ICSharpCode.AvalonEdit.Document.SpellingErrorRange(err.Item1, err.Item2);
			this.Exec(() => {
				this.ViewControl.SetSpellingErrors(viewItems);
			});
		}

		public void ClearSpellingErrors() {
			this.Exec(() => {
				this.ViewControl.ClearSpellingErrors();
			});
		}

		public void SetSpellingSuggestions(IEnumerable<string> suggestions) {
			this.Exec(() => this.ViewControl.SetSpellingSuggestions(suggestions));
		}

		public event EventHandler<Naracea.View.Events.RawTextChangedArgs> RawTextChanged;
		public event EventHandler<StoreTextInsertedArgs> StoreTextInserted;
		public event EventHandler<StoreTextDeletedArgs> StoreTextDeleted;
		public event EventHandler<StoreTextBackspacedArgs> StoreTextBackspaced;
		public event EventHandler<StoreGroupBeginArgs> StoreGroupBegin;
		public event EventHandler<StoreGroupEndArgs> StoreGroupEnd;
		public event EventHandler<StoreUndoArgs> StoreUndo;
		public event EventHandler<StoreUndoWordArgs> StoreUndoWord;
		public event EventHandler<StoreUndoSentenceArgs> StoreUndoSentence;
		public event EventHandler<StoreRedoArgs> StoreRedo;
		public event EventHandler<PasteCharsAsChangesArgs> PasteCharsAsChanges;
		public event EventHandler<UpdateIndicatorsArgs> UpdateIndicators;
		#endregion

		#region IHasContext Members
		public object Controller { get; set; }
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Controls.TextEditor ViewControl { get; private set; }
		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		void InstallHandlers() {
			this.ViewControl.RawTextChanged += (s, e) => {
				this.RawTextChanged.Raise(this, new RawTextChangedArgs(this, e.Position, e.AddedLength, e.RemovedLength));
			};
			this.ViewControl.TextAdded += (s, e) => {
				if (!string.IsNullOrEmpty(e.Text)) {
					this.StoreTextInserted.Raise(this, new StoreTextInsertedArgs(this, e.Position, e.Text));
				}
			};
			this.ViewControl.TextDeleted += (s, e) => {
				if (!string.IsNullOrEmpty(e.Text)) {
					this.StoreTextDeleted.Raise(this, new StoreTextDeletedArgs(this, e.Position, e.Text));
				}
			};
			this.ViewControl.TextBackspaced += (s, e) => {
				if (!string.IsNullOrEmpty(e.Text)) {
					this.StoreTextBackspaced.Raise(this, new StoreTextBackspacedArgs(this, e.Position, e.Text));
				}
			};
			this.ViewControl.GroupOperationBegin += (s, e) => {
				this.StoreGroupBegin.Raise(this, new StoreGroupBeginArgs(this));
			};
			this.ViewControl.GroupOperationEnd += (s, e) => {
				this.StoreGroupEnd.Raise(this, new StoreGroupEndArgs(this));
			};
			this.ViewControl.ChangeUndone += (s, e) => {
				this.StoreUndo(this, new StoreUndoArgs(this));
			};
			this.ViewControl.UndoWordRequest += (s, e) => {
				this.StoreUndoWord.Raise(this, new StoreUndoWordArgs(this));
			};
			this.ViewControl.UndoSentenceRequest += (s, e) => {
				this.StoreUndoSentence.Raise(this, new StoreUndoSentenceArgs(this));
			};
			this.ViewControl.ChangeRedone += (s, e) => {
				this.StoreRedo.Raise(this, new StoreRedoArgs(this));
			};
			this.ViewControl.PasteCharsAsChangesRequest += (s, e) => {
				try {
					if (Clipboard.ContainsText()) {
						var pos = this.CaretPosition;
						var text = Clipboard.GetText().NormalizeEols();
						this.PasteCharsAsChanges.Raise(this, new PasteCharsAsChangesArgs(this, pos, text));
					}
				} catch (System.Runtime.InteropServices.COMException) {
					//prevent failing on that stupid clipboard exception
				}
			};
			this.ViewControl.IsVisibleChanged += (s, e) => this.ApplyPendingFocus();
			this.ViewControl.IsEnabledChanged += (s, e) => this.ApplyPendingFocus();
			this.ViewControl.PropertyChanged += (s, e) => {
				this.UpdateIndicators.Raise(this, new UpdateIndicatorsArgs(this));
			};
			// font changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeFont>(
				m => {
					_fontSetter.SetFamily(m.FontFamily);
					_fontSetter.SetSize(m.FontSize);
					_fontSetter.Apply();
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeFont>(
				m => {
					_fontSetter.Preview(m.FontFamily, m.FontSize);
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeFont>(
				m => _fontSetter.CancelPreview()
			));
			// color changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeColor>(
				m => {
					_colorSetter.SetForeground(m.Foreground);
					_colorSetter.SetBackground(m.Background);
					_colorSetter.Apply();
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeColor>(
				m => _colorSetter.Preview(m.Foreground, m.Background)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeColor>(
				m => _colorSetter.CancelPreview()
			));
			// syntax highlighting changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeSyntaxHighlighting>(
				m => {
					_highlightingSetter.SetSyntaxHighlighting(m.SyntaxHighlighting);
					_highlightingSetter.Apply();
				},
				new ShapeToFilter<Messages.ChangeSyntaxHighlighting>(msg => msg.TextEditorView == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewSyntaxHighlighting>(
				m => _highlightingSetter.Preview(m.SyntaxHighlighting),
				new ShapeToFilter<Messages.PreviewSyntaxHighlighting>(msg => msg.TextEditorView == this)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelSyntaxHighlighting>(
				m => _highlightingSetter.CancelPreview()
			));
			// spell checking
			this.ViewControl.AddToSpellingDictionaryRequest += (s, e) =>
				this.Bus.Publish(new Naracea.View.Requests.AddToSpellingDictionary(this, e.WordPosition, e.WordLength));
			this.ViewControl.SuggestSpelling += (s, e) =>
				this.Bus.Publish(new Naracea.View.Requests.SuggestSpelling(this, e.WordPosition, e.WordLength));
		}

		void ApplyPendingFocus() {
			if (_pendingFocus && this.ViewControl.IsEnabled && this.ViewControl.IsVisible) {
				this.Focus();
				_pendingFocus = false;
			}
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
		}

		public void EndBusy() {
			_busyCount--;
		}
		#endregion

		#region ICanBeFocused Members
		public void Focus() {
			//must be async, otherwise tab or toolbar stoles focus from the editor...
			this.ViewControl.ExecuteOnUiThreadAsync(() => {
				//check for null here, because view can be closed already when pending focus is getting handled
				if (this.ViewControl != null) {
					_pendingFocus = !this.ViewControl.Focus();
				}
			});
		}
		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			this.Exec(() => {
				// set fonts properly
				_fontSetter = new FontSetter(this.ViewControl);
				if (settings.HasValue(Settings.ViewTextEditorFontFamilyName)) {
					_fontSetter.SetFamily(new FontFamily(settings.Get<string>(Settings.ViewTextEditorFontFamilyName)));
				}
				if (settings.HasValue(Settings.ViewTextEditorFontSize)) {
					_fontSetter.SetSize(settings.Get<double>(Settings.ViewTextEditorFontSize));
				}
				_fontSetter.Apply();

				//set colors properly
				_colorSetter = new ColorSetter(this.ViewControl);
				if (settings.HasValue(Settings.ViewForegroundColor)) {
					_colorSetter.SetForeground(new SolidColorBrush((Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewForegroundColor))));
				}

				if (settings.HasValue(Settings.ViewBackgroundColor)) {
					_colorSetter.SetBackground(new SolidColorBrush((Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewBackgroundColor))));
				}
				_colorSetter.Apply();

				_highlightingSetter = new SyntaxHighlightingSetter(this);
				if (settings.HasValue(Settings.ViewTextEditorSyntaxHighlighting)) {
					_highlightingSetter.SetSyntaxHighlighting(settings.Get<string>(Settings.ViewTextEditorSyntaxHighlighting));
				}
			});
		}

		public void Close() {
			this.Exec(() => {
				_fontSetter = null;
				_colorSetter = null;
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion

	}
}
