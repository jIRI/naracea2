﻿using MemBus;
using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.Forms;
using Naracea.Core.Ui.Wpf.View.Components;
using Naracea.Core.Ui.Wpf.View.Compounds;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using Naracea.View.Messages;
using Naracea.View.Requests;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Input;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View
{
	public partial class ApplicationView : Naracea.View.ViewBase, IApplicationView
	{
		#region Private attributes
		Compounds.WindowSizeRestorer _windowRestorer;
		Compounds.ColorManager _mainColorManager;
		Compounds.ColorSetter _mainColorSetter;
		Compounds.ColorSetter _documentContainerColorSetter;
		Compounds.TabHeaderColorSetter _documentTabsColorSetter;
		Compounds.QatManager _qatManager;
		Compounds.FontManager _fontManager;
		int _documentCounter = 1;
		#endregion

		#region ctors
		public ApplicationView(IBus bus)
			: base(bus)
		{
			this.MruList = new List<MruItem>();
			this.ViewControl = new Main();
			this.ViewControl.mruList.ItemsSource = _mruList;
			this.ViewControl.homeSpellcheckLang.ItemsSource = _spellcheckInstalledLanguages;

			//right side indicator for text editor
			var documentStatusBar = new CompositeIndicator();
			documentStatusBar.Open(this);
			this.DocumentStatusBar = documentStatusBar;
			//left side indicators for progress
			var progressIndicators = new ProgressIndicatorComposite();
			progressIndicators.Open(this);
			this.ProgressIndicators = progressIndicators;
			this.InstallEventHandlers();
		}
		#endregion

		#region Internal methods and properties
		internal IList<IDocumentView> DocumentViews { get; private set; }
		internal void SetAsOwnerOf(Window window)
		{
			window.Owner = this.ViewControl;
		}
		#endregion

		#region IApplication Members
		public ISettings Settings { get; private set; }
		public object Controller { get; set; }
		public Naracea.View.Compounds.IHasIndicators DocumentStatusBar { get; private set; }
		public Naracea.View.Compounds.IHasIndicators ProgressIndicators { get; private set; }
		public IFindReplaceTextView FindReplaceView { get; set; }
		bool _areFileOperationsLocked;
		public bool AreFileOperationsLocked
		{
			get
			{
				return _areFileOperationsLocked;
			}
			set
			{
				if (_areFileOperationsLocked != value)
				{
					_areFileOperationsLocked = value;
					this.ViewControl.ExecuteOnUiThreadAsync(() =>
					{
						CommandManager.InvalidateRequerySuggested();
						this.UpdateCommands();
					});
				}
			}
		}

		ObservableCollection<MruItem> _mruList = new ObservableCollection<Naracea.View.MruItem>();
		public IList<MruItem> MruList
		{
			get
			{
				return _mruList;
			}
			set
			{
				if (this.ViewControl != null)
				{
					this.Exec(() =>
					{
						_mruList.SetFrom(value);
					});
				}
			}
		}

		ObservableCollection<SpellcheckDictionaryItem> _spellcheckInstalledLanguages = new ObservableCollection<Naracea.View.SpellcheckDictionaryItem>();
		public IList<SpellcheckDictionaryItem> SpellcheckInstalledLanguages
		{
			get
			{
				return _spellcheckInstalledLanguages;
			}
			set
			{
				if (this.ViewControl != null)
				{
					this.Exec(() =>
					{
						_spellcheckInstalledLanguages.SetFrom(value);
					});
				}
			}
		}

		Dictionary<IDocumentView, TabItem> _documents = new Dictionary<IDocumentView, TabItem>();
		public IEnumerable<IDocumentView> Documents { get { return _documents.Keys; } }

		IEnumerable<ExportFormatItem> _exportFormats = new ExportFormatItem[] { };
		public IEnumerable<ExportFormatItem> ExportFormats
		{
			get
			{
				return _exportFormats;
			}
			set
			{
				_exportFormats = value.ToArray();
				if (this.ViewControl != null)
				{
					this.Exec(() => this.ViewControl.menuFileExport.ItemsSource = _exportFormats);
				}
			}
		}

		public void AddDocument(IDocumentView document)
		{
			this.Exec(new Action(() => this.AddDo(document)));
		}

		public void RemoveDocument(IDocumentView document)
		{
			this.Exec(new Action(() => this.RemoveDo(document)));
		}

		public void ActivateDocument(IDocumentView documentView)
		{
			this.Exec(new Action(() => this.ActivateDo(documentView)));
		}

		public void UpdateDocument(IDocumentView documentView)
		{
			this.Exec(new Action(() => this.UpdateDo(documentView)));
		}

		public IDocumentView ActiveDocument { get; private set; }

		public void Open(ISettings settings)
		{
			this.Settings = settings;

			this.Exec(() =>
			{
				this.ViewControl.ApplicationView = this;

				_windowRestorer = new WindowSizeRestorer(this.ViewControl, this.Settings, "Main");
				_mainColorManager = new ColorManager(this.ViewControl, this.Settings);
				_mainColorSetter = new ColorSetter(this.ViewControl);
				_documentContainerColorSetter = new ColorSetter(this.ViewControl.documentContainer);
				_documentTabsColorSetter = new TabHeaderColorSetter(this.ViewControl.documentContainer);
				_qatManager = new QatManager(this.ViewControl.ribbon, this.Settings);
				_fontManager = new FontManager(this.ViewControl, this.Settings);

				// keep the order, it doesnt matter :-)
				this.RestoreDocumentContainerWidthSlider();
				_windowRestorer.RestoreWindowBounds();
				_qatManager.RestoreQatItems();

				_fontManager.RestoreFontSettings();
				this.ViewControl.homeFontSizes.ItemsSource = _fontManager.DefaultFontSizes;
				this.ViewControl.homeFontSizeAuto.ItemsSource = new FontSize[] { _fontManager.DefaultFontSize };
				this.ViewControl.homeFontSizes.ItemsSource = _fontManager.DefaultFontSizes;
				this.ViewControl.homeFontSizeGallery.SelectedValue = _fontManager.CurrentFontSize;
				this.ViewControl.homeFontFamilies.ItemsSource = _fontManager.AvailableFontFamilies;
				this.ViewControl.homeFontFamilyAuto.ItemsSource = new FontFamily[] { _fontManager.DefaultFontFamily };
				this.ViewControl.homeFontFamilyGallery.SelectedValue = _fontManager.CurrentFontFamily;

				_mainColorManager.RestoreColors();
				this.ViewControl.homeFontBackgroundColorAuto.ItemsSource = new NamedBrush[] { _mainColorManager.DefaultBackground };
				this.ViewControl.homeFontColorAuto.ItemsSource = new NamedBrush[] { _mainColorManager.DefaultForeground };
				this.ViewControl.homeFontColors.ItemsSource = _mainColorManager.AvailableColors;
				this.ViewControl.homeFontColorGallery.SelectedValue = _mainColorManager.CurrentForeground;
				this.ViewControl.homeFontBackgroundColors.ItemsSource = _mainColorManager.AvailableColors;
				this.ViewControl.homeFontBackgroundColorGallery.SelectedValue = _mainColorManager.CurrentBackground;

				this.ViewControl.viewEditorHighlightingNone.ItemsSource = new SyntaxHighlightingGalleryItem[] { SyntaxHighlightingGalleryItem.None };
				this.ViewControl.viewEditorHighlighting.ItemsSource = (from h in ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.HighlightingDefinitions
																															 select new SyntaxHighlightingGalleryItem
																															 {
																																 Name = h.Name
																															 }).ToArray();

				this.ViewControl.homeSpellcheckLangNone.ItemsSource = new SpellcheckDictionaryItem[] { SpellcheckDictionaryItem.None };

				_documentTabsColorSetter.SetForeground(_mainColorManager.DefaultForeground.Brush);
				_documentTabsColorSetter.SetBackground(_mainColorManager.DefaultBackground.Brush);
				_documentTabsColorSetter.Apply();

				this.ViewControl.Show();
				_windowRestorer.WasOpen = true;
			});
		}

		public void Close()
		{
			this.Bus.Publish(new Naracea.View.Messages.ClosingAppView());

			this.Exec(() =>
			{
				_qatManager.StoreQatItems();
				_windowRestorer.StoreWindowBounds();
				_fontManager.StoreFontSettings();
				_mainColorManager.StoreColorSettings();
			});
			this.ViewControl.ApplicationView = null;
			this.Dispose();
		}

		public void Hide()
		{
			this.Exec(() => this.ViewControl.Visibility = System.Windows.Visibility.Hidden);
		}

		public void Show()
		{
			this.Exec(() =>
			{
				this.ViewControl.Visibility = System.Windows.Visibility.Visible;
				this.ViewControl.Activate();
			});
		}

		public void AddRibbonTab(RibbonTab tab)
		{
			if (tab != null)
			{
				this.Exec(() => this.ViewControl.ribbon.Items.Add(tab));
			}
		}

		public void AddRibbonGroup(ApplicationRibbonTabIds tab, RibbonGroup group)
		{
			if (group == null)
			{
				return;
			}

			this.Exec(() =>
			{
				switch (tab)
				{
					case ApplicationRibbonTabIds.View:
						this.ViewControl.tabView.Items.Add(group);
						break;
					case ApplicationRibbonTabIds.Document:
						this.ViewControl.tabDocument.Items.Add(group);
						break;
					default:
						throw new NotSupportedException("Adding group to this tab is not supported.");
				}
			});
		}

		public void FinalizePluginUiUpdate()
		{
			this.Exec(() =>
			{
				_qatManager.RestoreQatItems();
				CommandManager.InvalidateRequerySuggested();
				this.UpdateCommands();
			});
		}

		public void RegisterControl(string name, System.Windows.Controls.Control control)
		{
			this.ViewControl.ribbon.RegisterName(name, control);
		}

		public void UnregisterControl(string name)
		{
			this.ViewControl.ribbon.UnregisterName(name);
		}

		public void ExecAsync(Action action)
		{
			this.ViewControl.ExecuteOnUiThreadAsync(action);
		}

		public void RegisterChildWindow(Window window)
		{
			window.Owner = this.ViewControl;
		}
		#endregion

		#region Private methods
		private void InstallEventHandlers()
		{
			this.InstallGeneralAppViewHandlers();
			this.InstallFindAndReplaceHandlers();
			this.InstallMruHandlers();
			this.InstallSpellcheckHandlers();
			this.InstallTimelineHandlers();
			this.InstallFontChangeEventHandlers();
			this.InstallColorChangesHandlers();
			this.InstallSyntaxHighlightingHandlers();
		}

		void InstallSyntaxHighlightingHandlers()
		{
			// syntax highlighting changes
			this.ViewControl.viewEditorHighlightingGallery.Command = new PreviewableCommand<SyntaxHighlightingGalleryItem>(
				p =>
				{
					var highlighting = p.Name;
					this.Bus.Publish(new Messages.ChangeSyntaxHighlighting(this.ActiveDocument.ActiveBranch.Editor, highlighting));
					this.Bus.Publish(new SetSyntaxHighlighting(this.ActiveDocument.ActiveBranch.Editor, highlighting));
				},
				p => this.HasActiveBranch(),
				p =>
				{
					var highlighting = p.Name;
					this.Bus.Publish(new Messages.PreviewSyntaxHighlighting(this.ActiveDocument.ActiveBranch.Editor, highlighting));
				},
				() => this.Bus.Publish(new Messages.CancelSyntaxHighlighting())
			);
		}

		void InstallColorChangesHandlers()
		{
			// color changes
			this.ViewControl.homeFontColorGallery.Command = new PreviewableCommand<NamedBrush>(
				p =>
				{
					this.UpdateColors();
					this.Bus.Publish(new Messages.ChangeColor(p.Brush, _mainColorManager.CurrentBackground.Brush));
					_mainColorManager.StoreColorSettings();
				},
				p => true,
				p => this.Bus.Publish(new Messages.PreviewChangeColor(p.Brush, _mainColorManager.CurrentBackground.Brush)),
				() => this.Bus.Publish(new Messages.CancelChangeColor())
			);
			this.ViewControl.homeFontBackgroundColorGallery.Command = new PreviewableCommand<NamedBrush>(
				p =>
				{
					this.UpdateColors();
					this.Bus.Publish(new Messages.ChangeColor(_mainColorManager.CurrentForeground.Brush, p.Brush));
					_mainColorManager.StoreColorSettings();
				},
				p => true,
				p => this.Bus.Publish(new Messages.PreviewChangeColor(_mainColorManager.CurrentForeground.Brush, p.Brush)),
				() => this.Bus.Publish(new Messages.CancelChangeColor())
			);
			// color changes handling
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeColor>(
				m =>
				{
					_mainColorSetter.SetForeground(m.Foreground);
					_mainColorSetter.SetBackground(m.Background);
					_mainColorSetter.Apply();
					_documentContainerColorSetter.SetForeground(m.Foreground);
					_documentContainerColorSetter.SetBackground(m.Background);
					_documentContainerColorSetter.Apply();
					_documentTabsColorSetter.SetForeground(m.Foreground);
					_documentTabsColorSetter.SetBackground(m.Background);
					_documentTabsColorSetter.Apply();
				},
				new ShapeToFilter<Messages.ChangeColor>(msg => this.ViewControl.WindowState == WindowState.Maximized)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeColor>(
				m =>
				{
					_mainColorSetter.Preview(m.Foreground, m.Background);
					_documentContainerColorSetter.Preview(m.Foreground, m.Background);
					_documentTabsColorSetter.Preview(m.Foreground, m.Background);
				},
				new ShapeToFilter<Messages.PreviewChangeColor>(msg => this.ViewControl.WindowState == WindowState.Maximized)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeColor>(
				m =>
				{
					_mainColorSetter.CancelPreview();
					_documentContainerColorSetter.CancelPreview();
					_documentTabsColorSetter.CancelPreview();
				},
				new ShapeToFilter<Messages.CancelChangeColor>(msg => this.ViewControl.WindowState == WindowState.Maximized)
			));
		}

		void InstallFontChangeEventHandlers()
		{
			// font changes
			this.ViewControl.homeFontFamilyGallery.Command = new PreviewableCommand<FontFamily>(
				p =>
				{
					this.UpdateFont();
					this.Bus.Publish(new Messages.ChangeFont(p, _fontManager.CurrentFontSize.Size));
					_fontManager.StoreFontSettings();
				},
				p => true,
				p => this.Bus.Publish(new Messages.PreviewChangeFont(p, _fontManager.CurrentFontSize.Size)),
				() => this.Bus.Publish(new Messages.CancelChangeFont())
			);
			this.ViewControl.homeFontSizeGallery.Command = new PreviewableCommand<FontSize>(
				p =>
				{
					this.UpdateFont();
					this.Bus.Publish(new Messages.ChangeFont(_fontManager.CurrentFontFamily, p.Size));
					_fontManager.StoreFontSettings();
				},
				p => true,
				p => this.Bus.Publish(new Messages.PreviewChangeFont(_fontManager.CurrentFontFamily, p.Size)),
				() => this.Bus.Publish(new Messages.CancelChangeFont())
			);
		}

		void InstallTimelineHandlers()
		{
			//timeline settings
			this.ViewControl.viewTimelineGranularityGallery.Command = new Command(
				() =>
				{
					if (this.HasActiveBranch())
					{
						this.Bus.Publish(new TimelineSettingsChanged(
							this.ActiveDocument.ActiveBranch.Timeline,
							this.ViewControl.viewTimelineGranularityGallery.SelectedValue as IUnitOfTime,
							this.ActiveDocument.ActiveBranch.Timeline.ShowEmptyBars)
						);
					}
				},
				() => this.ActiveDocument != null && this.ActiveDocument.ActiveBranch != null
			);
			this.ViewControl.viewTimelineShowEmptyBars.Command = new Command(
				() =>
				{
					if (this.HasActiveBranch())
					{
						this.Bus.Publish(new TimelineSettingsChanged(
							this.ActiveDocument.ActiveBranch.Timeline,
							this.ActiveDocument.ActiveBranch.Timeline.Granularity,
							this.ViewControl.viewTimelineShowEmptyBars.IsChecked.Value)
							);
					}
				},
				() => this.ActiveDocument != null && this.ActiveDocument.ActiveBranch != null
			);
		}

		void InstallMruHandlers()
		{
			//mru list handling
			this.ViewControl.mruListGallery.Command = new Command(
				() =>
				{
					var item = this.ViewControl.mruListGallery.SelectedValue as MruItem;
					if (item != null)
					{
						this.Bus.Publish(new OpenDocument(item.FileName));
					}
					this.ViewControl.mruListGallery.SelectedValue = null;
				},
				() => true
			);
		}

		void InstallSpellcheckHandlers()
		{
			//mru list handling
			this.ViewControl.homeSpellcheckLangGallery.Command = new Command(
				() =>
				{
					var item = this.ViewControl.homeSpellcheckLangGallery.SelectedValue as SpellcheckDictionaryItem;
					if (item != null)
					{
						this.Bus.Publish(new SpellcheckSetLanguage(this.ActiveDocument, this.ActiveDocument.ActiveBranch, item));
					}
				},
				() => this.HasActiveBranch()
			);
		}

		void InstallFindAndReplaceHandlers()
		{
			//attach to find next/prev events
			this.ViewControl.FindNextRequested += (s, e) =>
			{
				if (this.FindReplaceView.FindPattern != null && this.FindReplaceView.FindPattern.Length > 0)
				{
					this.Bus.Publish(new Naracea.View.Requests.FindNext(this.FindReplaceView.GetFindOptions()));
				}
			};
			this.ViewControl.FindPreviousRequested += (s, e) =>
			{
				if (this.FindReplaceView.FindPattern != null && this.FindReplaceView.FindPattern.Length > 0)
				{
					this.Bus.Publish(new Naracea.View.Requests.FindPrevious(this.FindReplaceView.GetFindOptions()));
				}
			};
		}

		void InstallGeneralAppViewHandlers()
		{
			this.ViewControl.Closing += this.UiClosing;

			this.ViewControl.ExitRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.Exit());
			this.ViewControl.CloseFileRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.CloseDocument(this.ActiveDocument));
			this.ViewControl.NewFileRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.CreateDocument());
			this.ViewControl.OpenFileRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.OpenDocumentOrDocuments());
			this.ViewControl.PrintRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.PrintDocument(this.ActiveDocument));
			this.ViewControl.HelpRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowHelp());
			this.ViewControl.SaveFileRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SaveDocument(this.ActiveDocument));
			this.ViewControl.SaveFileAsRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SaveAsDocument(this.ActiveDocument));
			this.ViewControl.SaveAllFilesRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SaveAllDocuments());
			this.ViewControl.ExportCurrentBranchRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ExportCurrentBranch(this.ActiveDocument, this.ActiveDocument.ActiveBranch, e.Item));
			this.ViewControl.ActiveDocumentChanged += this.HandleActiveDocumentChanged;
			this.ViewControl.RefocusNeeded += this.ViewControl_RefocusNeeded;
			this.ViewControl.SizeChanged += (s, e) => this.HandleSizeChanged();
			this.ViewControl.LocationChanged += (s, e) => this.HandleSizeChanged();
			this.ViewControl.StateChanged += (s, e) => this.HandleWindowStateChanged();
			this.ViewControl.documentContainerWidthSlider.ValueChanged += this.EditorWidthSlider_ValueChanged;
			this.ViewControl.DocumentContainer.TabItemClosing += this.DocumentContainer_TabItemClosing;
			this.ViewControl.ShowFindTextWindowRequested += this.HandleShowFindTextWindowRequested;
			this.ViewControl.ShowDocumentPropertiesRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowDocumentProperties(this.ActiveDocument));
			this.ViewControl.ShowBranchPropertiesRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowBranchProperties(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.DeleteBranchRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.DeleteBranch(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.ShowTextViewerRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowBranchTextViewer(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.OpenBrowserRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.OpenBrowser(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.ToggleWrapLinesRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ToggleWrapLines(this.ActiveDocument.ActiveBranch.Editor));
			this.ViewControl.ToggleShowWhitespacesRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ToggleShowWhitespaces(this.ActiveDocument.ActiveBranch.Editor));
			this.ViewControl.ToggleKeepTabsRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ToggleKeepTabs(this.ActiveDocument.ActiveBranch.Editor));
			this.ViewControl.ShowTimelineOnlyRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowTimelineOnly(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.ShowTimelineAndChangeStreamRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowTimelineAndChangeStream(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.ShowChangeStreamOnlyRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ShowChangeStreamOnly(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
			this.ViewControl.TabSizeChangeRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SetTabSize(this.ActiveDocument.ActiveBranch.Editor, e.Size));
			this.ViewControl.InstallSpellcheckDictionaryRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SpellcheckInstallLanguage());
			this.ViewControl.SpellcheckRecheckAllRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.SpellcheckRecheckAll(this.ActiveDocument.ActiveBranch.Editor));
			this.ViewControl.CopyToClipboardAsHtmlRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.CopyToClipboardAsHtml(this.ActiveDocument, this.ActiveDocument.ActiveBranch));

			this.ViewControl.RestoreEditorFocusRequest += (s, e) =>
			{
				if (HasActiveBranch())
				{
					this.ActiveDocument.ActiveBranch.Editor.Focus();
				}
			};

			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ActivateDocument>(
				m => this.UpdateBranchSpecificControls(),
				new ShapeToFilter<Naracea.View.Requests.ActivateDocument>(msg => this.ActiveDocument != null && this.ActiveDocument.ActiveBranch != null)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ActivateBranch>(
				m => this.UpdateBranchSpecificControls(),
				new ShapeToFilter<Naracea.View.Requests.ActivateBranch>(msg => this.ActiveDocument != null && this.ActiveDocument.ActiveBranch != null)
			));
		}

		void ViewControl_RefocusNeeded(object sender, EventArgs e)
		{
			this.ActiveDocument.Focus();
		}

		void ViewControl_ShowBranchPropertiesRequested(object sender, EventArgs e)
		{
			this.Bus.Publish(new Naracea.View.Requests.ShowBranchProperties(this.ActiveDocument, this.ActiveDocument.ActiveBranch));
		}

		void HandleShowFindTextWindowRequested(object sender, EventArgs e)
		{
			var selectedText = "";
			if (this.ActiveDocument != null && this.ActiveDocument.ActiveBranch != null)
			{
				selectedText = this.ActiveDocument.ActiveBranch.Editor.SelectedText;
			}
			this.Bus.Publish(new Naracea.View.Requests.ShowFindTextWindow(selectedText));
		}

		void DocumentContainer_TabItemClosing(object sender, WpfExtension.CustomControls.TabItemCancelEventArgs e)
		{
			this.ViewControl.DocumentContainer.SelectedItem = sender;
			if (!this.ActiveDocument.IsBusy)
			{
				ApplicationCommands.Close.Execute(null, null);
			}
			e.Cancel = true;
		}

		void AddDo(IDocumentView documentView)
		{
			var control = documentView.Control as FrameworkElement;
			Debug.Assert(control != null);
			control.Width = double.NaN;
			control.Height = double.NaN;

			var tabItem = new WpfExtension.CustomControls.TabItem();
			var header = new Controls.DocumentTabHeader();
			tabItem.Name = string.Format("document{0}", _documentCounter++);
			tabItem.Header = header;
			tabItem.Content = control;
			tabItem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
			tabItem.VerticalContentAlignment = VerticalAlignment.Stretch;
			tabItem.AllowDelete = true;
			tabItem.MouseDoubleClick += HandleDocumentTabItemMouseDoubleClick;
			this.ViewControl.DocumentContainer.Items.Add(tabItem);
			_documents.Add(documentView, tabItem);
			this.UpdateDo(documentView);
		}

		void RemoveDo(IDocumentView documentView)
		{
			//here we do not assert. problem is in closing half-baked documents which appears ok, but don't have UI constructed.
			//It is lame, but easier to fix it here than tracking it all the way up
			if (_documents.ContainsKey(documentView))
			{
				this.ViewControl.DocumentContainer.Items.Remove(_documents[documentView]);
				_documents[documentView].MouseDoubleClick -= HandleDocumentTabItemMouseDoubleClick;
				_documents.Remove(documentView);
				if (_documents.Count == 0)
				{
					this.ActiveDocument = null;
					this.UpdateCommands();
				}
			}
		}

		void ActivateDo(IDocumentView documentView)
		{
			Debug.Assert(_documents.ContainsKey(documentView));
			this.ViewControl.DocumentContainer.SelectedItem = _documents[documentView];
		}

		void UpdateDo(IDocumentView documentView)
		{
			Debug.Assert(_documents.ContainsKey(documentView));
			var header = _documents[documentView].Header as Controls.DocumentTabHeader;
			header.Text = documentView.Name;
			header.ToolTip = documentView.FullPath;
		}

		void UiClosing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!this.AreFileOperationsLocked)
			{
				var msg = new Naracea.View.Requests.Exit();
				this.Bus.Publish(msg);
				e.Cancel = msg.Cancel;
			}
			else
			{
				this.Bus.Publish(new Naracea.View.Requests.NotifyUserFileOpsAreLocked());
				e.Cancel = true;
			}
		}

		void HandleDocumentTabItemMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var tabItem = sender as TabItem;
			var position = e.GetPosition(tabItem);
			if (tabItem != null && position.X >= 0 && position.Y >= 0 && position.X < tabItem.ActualWidth && position.Y < tabItem.ActualHeight)
			{
				this.Bus.Publish(new Naracea.View.Requests.ShowDocumentProperties(this.ActiveDocument));
			}
		}

		void HandleActiveDocumentChanged(object sender, EventArgs e)
		{
			var docView = from view in _documents
										where view.Value == this.ViewControl.SelectedDocumentTab
										select view.Key;

			if (!docView.Any())
			{
				//this happens during document removal, so it is not in view anymore
				//therefore we do nothing
				return;
			}

			Debug.Assert(docView.Count() == 1);
			this.ActiveDocument = docView.First();
			this.ActiveDocument.Focus();
			this.Bus.Publish(new Naracea.View.Requests.ActivateDocument(this.ActiveDocument));
		}

		private void HandleWindowStateChanged()
		{
			if (this.ViewControl.WindowState == WindowState.Maximized)
			{
				_mainColorSetter.SetForeground(_mainColorManager.CurrentForeground.Brush);
				_mainColorSetter.SetBackground(_mainColorManager.CurrentBackground.Brush);
				_mainColorSetter.Apply();
				_documentContainerColorSetter.SetForeground(_mainColorManager.CurrentForeground.Brush);
				_documentContainerColorSetter.SetBackground(_mainColorManager.CurrentBackground.Brush);
				_documentContainerColorSetter.Apply();
				_documentTabsColorSetter.SetForeground(_mainColorManager.CurrentForeground.Brush);
				_documentTabsColorSetter.SetBackground(_mainColorManager.CurrentBackground.Brush);
				_documentTabsColorSetter.Apply();

			}
			else
			{
				_mainColorSetter.SetForeground(_mainColorManager.DefaultForeground.Brush);
				_mainColorSetter.SetBackground(_mainColorManager.DefaultBackground.Brush);
				_mainColorSetter.Apply();
				_documentContainerColorSetter.SetForeground(_mainColorManager.DefaultForeground.Brush);
				_documentContainerColorSetter.SetBackground(_mainColorManager.DefaultBackground.Brush);
				_documentContainerColorSetter.Apply();
				_documentTabsColorSetter.SetForeground(_mainColorManager.DefaultForeground.Brush);
				_documentTabsColorSetter.SetBackground(_mainColorManager.DefaultBackground.Brush);
				_documentTabsColorSetter.Apply();
			}
			this.UpdateDocumentContainerWidth();
		}

		void EditorWidthSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			this.UpdateDocumentContainerWidth();
		}

		void UpdateBranchSpecificControls()
		{
			this.Exec(() =>
			{
				this.ViewControl.homeTextToggleWrap.IsChecked = this.ActiveDocument.ActiveBranch.Editor.WrapsLines;
				this.ViewControl.homeEditorToggleShowWhitespaces.IsChecked = this.ActiveDocument.ActiveBranch.Editor.ShowWhitespaces;
				this.ViewControl.viewTimelineGranularities.ItemsSource = this.ActiveDocument.ActiveBranch.Timeline.AvailableGranularities;
				this.ViewControl.viewTimelineGranularityGallery.SelectedValue = this.ActiveDocument.ActiveBranch.Timeline.Granularity;
				this.ViewControl.viewTimelineShowEmptyBars.IsChecked = this.ActiveDocument.ActiveBranch.Timeline.ShowEmptyBars;
				this.ViewControl.viewTimelineShowTimeline.IsChecked = this.ActiveDocument.ActiveBranch.IsTimelineVisibleOnly;
				this.ViewControl.viewTimelineShowTimelineAndChangeStream.IsChecked = this.ActiveDocument.ActiveBranch.IsTimelineAndChangeStreamVisible;
				this.ViewControl.viewTimelineShowChangeStream.IsChecked = this.ActiveDocument.ActiveBranch.IsChangeStreamVisibleOnly;
				this.ViewControl.homeTextToggleKeepTabs.IsChecked = this.ActiveDocument.ActiveBranch.Editor.KeepTabs;
				this.ViewControl.viewEditorTabSize.Value = this.ActiveDocument.ActiveBranch.Editor.TabSize;
				if (this.ViewControl.viewEditorHighlighting.ItemsSource != null)
				{
					var highlighting = this.ActiveDocument.ActiveBranch.Editor.SyntaxHighlighting;
					if (highlighting != null)
					{
						foreach (var value in this.ViewControl.viewEditorHighlighting.ItemsSource)
						{
							if ((value as SyntaxHighlightingGalleryItem).Name.Equals(highlighting))
							{
								this.ViewControl.viewEditorHighlightingGallery.SelectedValue = value;
								break;
							}
						}
					}
					else
					{
						this.ViewControl.viewEditorHighlightingGallery.SelectedValue = SyntaxHighlightingGalleryItem.None;
					}
				}
				if (this.ViewControl.homeSpellcheckLang.ItemsSource != null)
				{
					var dictionaryName = this.ActiveDocument.ActiveBranch.Editor.SpellcheckLanguageName;
					if (dictionaryName != null)
					{
						foreach (var value in this.ViewControl.homeSpellcheckLang.ItemsSource)
						{
							if ((value as SpellcheckDictionaryItem).Name.Equals(dictionaryName))
							{
								this.ViewControl.homeSpellcheckLangGallery.SelectedValue = value;
								break;
							}
						}
					}
					else
					{
						this.ViewControl.homeSpellcheckLangGallery.SelectedValue = SpellcheckDictionaryItem.None;
					}
				}
				this.UpdateCommands();
			});
		}

		void UpdateCommands()
		{
			//this seems silly, but we need to raise CanExecuteChanged event for commands to get the buttons enabled
			// which hapens here when document/branch is activated (or removed when we use this to disable commands/controls)
			this.UpdateAllCommandsInItemsControl(this.ViewControl.ribbon);
			this.UpdateAllCommandsInItemsControl(this.ViewControl.ribbon.QuickAccessToolBar);
			this.UpdateAllCommandsInItemsControl(this.ViewControl.ribbon.ApplicationMenu);
		}

		void UpdateAllCommandsInItemsControl(ItemsControl itemsControl)
		{
			foreach (var item in itemsControl.Items)
			{
				var prop = item.GetType().GetProperty("Command");
				if (prop != null)
				{
					var command = prop.GetValue(item) as ICommand;
					if (command != null)
					{
						command.CanExecute(null);
					}
				}
				if (item is ItemsControl)
				{
					//recurse
					this.UpdateAllCommandsInItemsControl(item as ItemsControl);
				}
			}
		}

		bool HasActiveBranch()
		{
			return this.HasActiveDocument() && this.ActiveDocument.ActiveBranch != null;
		}

		bool HasActiveDocument()
		{
			return this.ActiveDocument != null;
		}
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Main ViewControl { get; private set; }

		public void Exec(Action action)
		{
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy
		{
			get
			{
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy()
		{
			_busyCount++;
			this.Exec(() =>
			{
				this.ViewControl.SetBusy();
				this.FindReplaceView.BeginBusy();
			});
		}

		public void EndBusy()
		{
			if (_busyCount > 0)
			{
				_busyCount--;
			}
			if (!this.IsBusy)
			{
				this.Exec(() =>
				{
					this.ViewControl.SetNotBusy();
					this.FindReplaceView.EndBusy();
				});
				if (this.ActiveDocument != null)
				{
					this.ActiveDocument.Focus();
				}
			}
		}
		#endregion

		#region IHasIndicators Members
		public void RegisterIndicator(Naracea.View.Components.IIndicator indicator)
		{
			this.Exec(() => this.ViewControl.statusBar.Items.Add(indicator.Control));
		}

		public void UnregisterIndicator(Naracea.View.Components.IIndicator indicator)
		{
			this.Exec(() => this.ViewControl.statusBar.Items.Remove(indicator.Control));
		}
		#endregion

		#region ICanBeFocused Members
		public void Focus()
		{
			this.Exec(() => this.ViewControl.Activate());
		}
		#endregion

		#region Fonts
		private void UpdateFont()
		{
			_fontManager.IsAutoFontSizeSelected = this.ViewControl.homeFontSizeGallery.SelectedValue == null || this.ViewControl.homeFontSizeGallery.SelectedValue == _fontManager.DefaultFontSize;
			if (!_fontManager.IsAutoFontSizeSelected)
			{
				_fontManager.PreferredFontSize = (FontSize)this.ViewControl.homeFontSizeGallery.SelectedValue;
			}
			_fontManager.IsAutoFontFamilySelected = this.ViewControl.homeFontFamilyGallery.SelectedValue == null || this.ViewControl.homeFontFamilyGallery.SelectedValue == _fontManager.DefaultFontFamily;
			if (!_fontManager.IsAutoFontFamilySelected)
			{
				_fontManager.PreferredFontFamily = (FontFamily)this.ViewControl.homeFontFamilyGallery.SelectedValue;
			}
		}
		#endregion

		#region Colors
		private void UpdateColors()
		{
			_mainColorManager.IsAutoForegroundColorSelected = this.ViewControl.homeFontColorGallery.SelectedValue == null || this.ViewControl.homeFontColorGallery.SelectedValue == _mainColorManager.DefaultForeground;
			if (!_mainColorManager.IsAutoForegroundColorSelected)
			{
				_mainColorManager.PreferredForeground = (NamedBrush)this.ViewControl.homeFontColorGallery.SelectedValue;
			}
			_mainColorManager.IsAutoBackgroundColorSelected = this.ViewControl.homeFontBackgroundColorGallery.SelectedValue == null || this.ViewControl.homeFontBackgroundColorGallery.SelectedValue == _mainColorManager.DefaultBackground;
			if (!_mainColorManager.IsAutoBackgroundColorSelected)
			{
				_mainColorManager.PreferredBackground = (NamedBrush)this.ViewControl.homeFontBackgroundColorGallery.SelectedValue;
			}
		}
		#endregion

		#region Window bounds
		void HandleSizeChanged()
		{
			_windowRestorer.HandleSizeChanged();
			this.UpdateDocumentContainerWidth();
		}

		void UpdateDocumentContainerWidth()
		{
			bool maximized = this.ViewControl.WindowState == WindowState.Maximized;
			if (this.ViewControl.documentContainerWidthSlider.IsEnabled != maximized)
			{
				this.ViewControl.documentContainerWidthSlider.IsEnabled = maximized;
			}
			double width = this.ViewControl.documentContainerWidthSlider.Value * (this.ViewControl.ActualWidth / 100.00);
			if (maximized && this.ViewControl.documentContainerWidthSlider.Value < 98.5)
			{
				this.ViewControl.documentContainer.Width = width;
			}
			else
			{
				this.ViewControl.documentContainer.Width = double.NaN;
			}
			this.Settings.Set<double>(Naracea.Common.Settings.ViewDocumentContainerWidth, this.ViewControl.documentContainerWidthSlider.Value);
		}

		void RestoreDocumentContainerWidthSlider()
		{
			if (this.Settings.HasValue(Naracea.Common.Settings.ViewDocumentContainerWidth))
			{
				this.ViewControl.documentContainerWidthSlider.Value = this.Settings.Get<double>(Naracea.Common.Settings.ViewDocumentContainerWidth);
			}
		}
		#endregion
	}
}
