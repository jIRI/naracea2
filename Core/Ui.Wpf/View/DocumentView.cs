﻿using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Compounds;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View {
	public class DocumentView : Naracea.View.ViewBase, IDocumentView {
		#region Attributes
		bool _suppressBranchActivationEvents = false;
		int _branchCounter = 1;
		TabHeaderColorSetter _colorSetter;
		#endregion

		public DocumentView(IBus bus)
			: base(bus) {
			this.ViewControl = new Naracea.Core.Ui.Wpf.Controls.Document();
			this.ViewControl.NewBranchRequest += this.Handle_NewBranchRequest;
			this.ViewControl.EmptyBranchRequest += this.Handle_EmptyBranchRequest;
			this.ViewControl.ActiveBranchChanged += this.Handle_ActiveBranchChanged;
			this.ViewControl.branchContainer.TabReorderingEnded += this.Handle_branchContainer_TabReorderingEnded;
			this.ViewControl.RefocusNeeded += this.ViewControl_RefocusNeeded;
			_colorSetter = new TabHeaderColorSetter(this.ViewControl.branchContainer);
			this.InstallHandlers();
		}

		#region IDocumentView Members
		Dictionary<IBranchView, TabItem> _branches = new Dictionary<IBranchView, TabItem>();
		public IEnumerable<IBranchView> Branches { get { return _branches.Keys; } }

		public void AddBranch(IBranchView branchView) {
			this.Exec(new Action(() => this.AddDo(branchView)));
		}

		public void RemoveBranch(IBranchView branchView) {
			this.Exec(new Action(() => this.RemoveDo(branchView)));
		}

		public void ActivateBranch(IBranchView branchView) {
			this.Exec(new Action(() => this.ActivateDo(branchView)));
		}

		public void UpdateBranch(IBranchView branchView) {
			this.Exec(new Action(() => this.UpdateDo(branchView)));
		}

		bool _locked = false;
		public bool IsLocked {
			get {
				return _locked;
			}
			set {
				if (_locked != value) {
					_locked = value;
					CommandManager.InvalidateRequerySuggested();
				}
			}
		}

		public IBranchView ActiveBranch { get; set; }

		public string FullPath { get; set; }

		public object Control { get { return this.ViewControl; } }
		internal Controls.Document ViewControl { get; private set; }

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}

		public int GetBranchVisualOrder(IBranchView branchView) {
			int result = -1;
			this.Exec(new Action(() => result = this.GetBranchVisualOrderDo(branchView)));
			return result;
		}
		#endregion

		#region IHasContext Members
		public object Controller { get; set; }
		#endregion

		#region IHasName Members
		public string Name { get; set; }
		#endregion

		#region Private methods
		void AddDo(IBranchView branchView) {
			var control = branchView.Control as FrameworkElement;
			Debug.Assert(control != null);
			control.Width = double.NaN;
			control.Height = double.NaN;

			_suppressBranchActivationEvents = true;
			var tabItem = new WpfExtension.CustomControls.TabItem();
			var header = new Controls.BranchTabHeader();
			tabItem.Name = string.Format("branch{0}", _branchCounter++);
			tabItem.Header = header;
			tabItem.Content = control;
			tabItem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
			tabItem.VerticalContentAlignment = VerticalAlignment.Stretch;
			tabItem.AllowDelete = false;
			tabItem.MouseDoubleClick += Handle_BranchTabItemMouseDoubleClick;
			this.ViewControl.branchContainer.Items.Add(tabItem);
			_branches.Add(branchView, tabItem);
			_suppressBranchActivationEvents = false;
			this.UpdateDo(branchView);
		}

		public void RemoveDo(IBranchView branchView) {
			if (branchView == null) {
				return;
			}

			Debug.Assert(_branches.ContainsKey(branchView));
			this.ViewControl.branchContainer.SelectedItem = null;
			this.ViewControl.branchContainer.Items.Remove(_branches[branchView]);
			_branches[branchView].MouseDoubleClick -= Handle_BranchTabItemMouseDoubleClick;
			_branches.Remove(branchView);
		}

		void ActivateDo(IBranchView branchView) {
			Debug.Assert(_branches.ContainsKey(branchView));
			this.ViewControl.branchContainer.SelectedItem = _branches[branchView];
			this.ActiveBranch = branchView;
		}

		void UpdateDo(IBranchView branchView) {
			Debug.Assert(_branches.ContainsKey(branchView));
			var header = _branches[branchView].Header as Controls.BranchTabHeader;
			header.Text = branchView.Name;
		}

		int GetBranchVisualOrderDo(IBranchView branchView) {
			return this.ViewControl.branchContainer.Items.IndexOf(_branches[branchView]);
		}

		void Handle_BranchTabItemMouseDoubleClick(object sender, MouseButtonEventArgs e) {
			var tabItem = sender as TabItem;
			var position = e.GetPosition(tabItem);
			if (tabItem != null && position.X >= 0 && position.Y >= 0 && position.X < tabItem.ActualWidth && position.Y < tabItem.ActualHeight) {
				this.Bus.Publish(new Naracea.View.Requests.ShowBranchProperties(this, this.ActiveBranch));
			}
		}

		void Handle_EmptyBranchRequest(object sender, EventArgs e) {
			this.Bus.Publish(new Naracea.View.Requests.CreateEmptyBranch(this));
		}

		void Handle_NewBranchRequest(object sender, EventArgs e) {
			this.Bus.Publish(new Naracea.View.Requests.CreateBranch(this));
		}

		void Handle_ActiveBranchChanged(object sender, EventArgs e) {
			if (_suppressBranchActivationEvents || this.ViewControl.branchContainer.IsTabReorderingInProgress) {
				return;
			}

			var branchView = from view in _branches
											 where view.Value == this.ViewControl.branchContainer.SelectedItem
											 select view.Key;

			if (!branchView.Any()) {
				//this happens during branch removal, so it is not in view anymore
				//therefore we do nothing
				return;
			}

			Debug.Assert(branchView.Count() == 1);
			this.ActiveBranch = branchView.First();
			this.ActiveBranch.Focus();
			this.Bus.Publish(new Naracea.View.Requests.ActivateBranch(this, this.ActiveBranch));
		}

		private void Handle_branchContainer_TabReorderingEnded(object sender, EventArgs e) {
			//we ignore branch activation during drag, so ensure we handle that here
			this.Handle_ActiveBranchChanged(sender, e);
			//we ignore branch in handler of this message, but since drag&drop always activates branch, it is safe to use active branch here
			this.Bus.Publish(new Naracea.View.Requests.RefreshBranchVisualOrder(this, this.ActiveBranch));
		}

		void ViewControl_RefocusNeeded(object sender, EventArgs e) {
			this.Focus();
		}
		#endregion

		#region IHasDirtyNotification Members
		public void DirtyStatusChanged(bool isDirty) {
			this.Exec(() => {
				var tab = this.ViewControl.Parent as WpfExtension.CustomControls.TabItem;
				if (tab != null) {
					var header = tab.Header as Controls.DocumentTabHeader;
					header.IsDirty = isDirty;
				}
			});
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
			this.Exec(() => this.ViewControl.SetBusy());
		}

		public void EndBusy() {
			if (_busyCount > 0) {
				_busyCount--;
			}
			if (!this.IsBusy) {
				this.Exec(() => this.ViewControl.SetNotBusy());
				this.Focus();
			}
		}
		#endregion

		#region ICanBeFocused
		public void Focus() {
			if (this.ActiveBranch != null) {
				this.ActiveBranch.Focus();
			}
		}
		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			this.Exec(() => {
				//set colors properly
				Color foreground = SystemColors.WindowTextColor;
				if (settings.HasValue(Settings.ViewForegroundColor)) {
					foreground = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewForegroundColor));
				}
				_colorSetter.SetForeground(new SolidColorBrush(foreground));
				Color background = SystemColors.WindowColor;
				if (settings.HasValue(Settings.ViewBackgroundColor)) {
					background = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewBackgroundColor));
				}
				_colorSetter.SetBackground(new SolidColorBrush(background));
				_colorSetter.Apply();
			});
		}

		private void InstallHandlers() {
			// color changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeColor>(
				m => {
					_colorSetter.SetForeground(m.Foreground);
					_colorSetter.SetBackground(m.Background);
					_colorSetter.Apply();
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeColor>(
				m => _colorSetter.Preview(m.Foreground, m.Background)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeColor>(
				m => _colorSetter.CancelPreview()
			));
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl.NewBranchRequest -= this.Handle_NewBranchRequest;
				this.ViewControl.EmptyBranchRequest -= this.Handle_EmptyBranchRequest;
				this.ViewControl.ActiveBranchChanged -= this.Handle_ActiveBranchChanged;
				this.ViewControl.branchContainer.TabReorderingEnded -= this.Handle_branchContainer_TabReorderingEnded;
				this.ViewControl = null;
				this.ActiveBranch = null;
				this.Dispose();
			});
		}
		#endregion
	}
}
