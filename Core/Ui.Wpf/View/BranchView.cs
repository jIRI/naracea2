﻿using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace Naracea.Core.Ui.Wpf.View {
	public class BranchView : Naracea.View.ViewBase, IBranchView {
		#region Private attributes
		ISettings _settings;
		#endregion

		#region ctor
		public BranchView(IBus bus)
			: base(bus) {
			this.ViewControl = new Controls.Branch();
			this.ViewControl.Width = double.NaN;
			this.ViewControl.Height = double.NaN;
			this.InstallHandlers();
		}
		#endregion

		#region IBranchView Members
		public bool CanRewind {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.CanRewind);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.CanRewind = value);
			}
		}

		public bool CanReplay {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.CanReplay);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.CanReplay = value);
			}
		}

		public bool CanBeDeleted { get; set; }

		ITextEditorView _textEditor;
		public ITextEditorView Editor {
			get {
				return _textEditor;
			}
			set {
				Debug.Assert(value.Control is System.Windows.UIElement);
				if (_textEditor != null) {
					this.Exec(() => this.ViewControl.textEditorHolder.Children.Remove(_textEditor.Control as System.Windows.UIElement));
				}
				_textEditor = value;
				this.Exec(() => this.ViewControl.textEditorHolder.Children.Add(_textEditor.Control as System.Windows.UIElement));
			}
		}

		ITimelineView _timeline;
		public ITimelineView Timeline {
			get {
				return _timeline;
			}
			set {
				Debug.Assert(value.Control is System.Windows.UIElement);
				if (_timeline != null) {
					this.Exec(() => this.ViewControl.timelineHolder.Children.Remove(_timeline.Control as System.Windows.UIElement));
				}
				_timeline = value;
				this.Exec(() => this.ViewControl.timelineHolder.Children.Add(_timeline.Control as System.Windows.UIElement));
			}
		}

		IChangeStreamView _changeStream;
		public IChangeStreamView ChangeStream {
			get {
				return _changeStream;
			}
			set {
				Debug.Assert(value.Control is System.Windows.UIElement);
				if (_changeStream != null) {
					this.Exec(() => this.ViewControl.changeStreamHolder.Children.Remove(_changeStream.Control as System.Windows.UIElement));
				}
				_changeStream = value;
				this.Exec(() => this.ViewControl.changeStreamHolder.Children.Add(_changeStream.Control as System.Windows.UIElement));
			}
		}

		public Naracea.View.Components.IEditorIndicators Indicators { get; set; }

		public void SetClipboard(string text) {
			try {
				Clipboard.SetText(text);
			} catch (Exception) {
				/// this is preemptive strike to aviod those pesky clipboard exceptions
			}
		}

		public void SetTimelineHeight(double height) {
			this.Exec(() => this.ViewControl.timelineRow.Height = new GridLength(height));
		}

		bool _isTimelineVisibleOnly = false;
		public bool IsTimelineVisibleOnly {
			get {
				return _isTimelineVisibleOnly;
			}
			set {
				if (value) {
					_isTimelineVisibleOnly = true;
					_isTimelineAndChangeStreamVisible = false;
					_isChangeStreamVisibleOnly = false;
					this.UpdateTimelineAndChangeStreamVisibility();
				}
			}
		}

		bool _isTimelineAndChangeStreamVisible = false;
		public bool IsTimelineAndChangeStreamVisible {
			get {
				return _isTimelineAndChangeStreamVisible;
			}
			set {
				if (value) {
					_isTimelineVisibleOnly = false;
					_isTimelineAndChangeStreamVisible = true;
					_isChangeStreamVisibleOnly = false;
					this.UpdateTimelineAndChangeStreamVisibility();
				}
			}
		}

		bool _isChangeStreamVisibleOnly = false;
		public bool IsChangeStreamVisibleOnly {
			get {
				return _isChangeStreamVisibleOnly;
			}
			set {
				if (value) {
					_isTimelineVisibleOnly = false;
					_isTimelineAndChangeStreamVisible = false;
					_isChangeStreamVisibleOnly = true;
					this.UpdateTimelineAndChangeStreamVisibility();
				}
			}
		}
		#endregion

		#region IHasContext Members
		public object Controller { get; set; }
		#endregion

		#region IHasName Members
		public string Name { get; set; }
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Controls.Branch ViewControl { get; private set; }

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region IHasDirtyNotification Members
		public void DirtyStatusChanged(bool isDirty) {
			this.Exec(() => {
				var tab = this.ViewControl.Parent as WpfExtension.CustomControls.TabItem;
				if (tab != null) {
					var header = tab.Header as Controls.BranchTabHeader;
					header.IsDirty = isDirty;
				}
			});
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
		}

		public void EndBusy() {
			_busyCount--;
		}
		#endregion

		#region IHasIndicators Members
		public void RegisterIndicator(Naracea.View.Components.IIndicator indicator) {
			this.Exec(() => this.ViewControl.statusBar.Items.Add(indicator.Control));
		}

		public void UnregisterIndicator(Naracea.View.Components.IIndicator indicator) {
			this.Exec(() => this.ViewControl.statusBar.Items.Remove(indicator.Control));
		}
		#endregion

		#region ICanBeFocused Members
		public void Focus() {
			this.Editor.Focus();
		}
		#endregion

		#region Private members
		private void InstallHandlers() {
			this.ViewControl.RewindChangeRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.RewindChange(this));
			this.ViewControl.RewindWordRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.RewindWord(this));
			this.ViewControl.RewindSentenceRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.RewindSentence(this));
			this.ViewControl.RewindParagraphRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.RewindParagraph(this));
			this.ViewControl.RewindAllRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.RewindAll(this));

			this.ViewControl.ReplayChangeRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplayChange(this));
			this.ViewControl.ReplayWordRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplayWord(this));
			this.ViewControl.ReplaySentenceRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplaySentence(this));
			this.ViewControl.ReplayParagraphRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplayParagraph(this));
			this.ViewControl.ReplayAllRequest += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplayAll(this));

			this.ViewControl.SizeChanged += (s, e) => this.UpdateTimelineAndChangeStreamVisibility();
		}

		void UpdateTimelineAndChangeStreamVisibility() {
			this.Exec(() => {
				if (this.IsTimelineVisibleOnly) {
					(this.ViewControl.timelineHolder.Children[0] as Control).Width = this.ViewControl.ActualWidth;
					(this.ViewControl.changeStreamHolder.Children[0] as Control).Width = 0.0;
				}
				if (this.IsTimelineAndChangeStreamVisible) {
					(this.ViewControl.timelineHolder.Children[0] as Control).Width = this.ViewControl.ActualWidth / 2.0;
					(this.ViewControl.changeStreamHolder.Children[0] as Control).Width = this.ViewControl.ActualWidth / 2.0;
				}
				if (this.IsChangeStreamVisibleOnly) {
					(this.ViewControl.timelineHolder.Children[0] as Control).Width = 0.0;
					(this.ViewControl.changeStreamHolder.Children[0] as Control).Width = this.ViewControl.ActualWidth;
				}
			});
		}
		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			_settings = settings;
			this.IsTimelineVisibleOnly = _settings.Get<bool>(Settings.TimelineShowTimelineOnly);
			this.IsTimelineAndChangeStreamVisible = _settings.Get<bool>(Settings.TimelineShowTimelineAndChangeStream);
			this.IsChangeStreamVisibleOnly = _settings.Get<bool>(Settings.TimelineShowChangeStreamOnly);
		}

		public void Close() {
			_settings.Set(Settings.TimelineShowTimelineOnly, this.IsTimelineVisibleOnly);
			_settings.Set(Settings.TimelineShowTimelineAndChangeStream, this.IsTimelineAndChangeStreamVisible);
			_settings.Set(Settings.TimelineShowChangeStreamOnly, this.IsChangeStreamVisibleOnly);

			this.Exec(() => {
				this.ViewControl.timelineHolder.Children.Clear();
				this.ViewControl.textEditorHolder.Children.Clear();
				_textEditor = null;
				_timeline = null;
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion
	}
}
