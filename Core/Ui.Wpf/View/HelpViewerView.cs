﻿using System;
using System.Windows.Documents;
using System.Windows.Markup;
using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.View {
	public class HelpViewerView : ViewBase, IHelpViewerView {
		#region Private attributes
		ApplicationView _appView;
		#endregion

		#region ctor
		public HelpViewerView(IBus bus, ApplicationView appView)
			: base(bus) {
			_appView = appView;
			this.ViewControl = new Forms.HelpViewer();
			_appView.SetAsOwnerOf(this.ViewControl);
			this.InstallHandlers();
		}
		#endregion

		#region ICanOpenClose Members
		public void Open(ISettings settings) {
			this.Exec(() => {
				this.ViewControl.Show();
			});
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl.Close();
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion

		#region IHasControl Members
		public Forms.HelpViewer ViewControl { get; private set; }

		public object Control {
			get { return this.ViewControl; }
		}

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		private void InstallHandlers() {
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Messages.ClosingAppView>(m => this.Close()));
			this.ViewControl.Closed += (s, e) => {
				this.Dispose();
			};
		}
		#endregion

		#region IHelpView Members
		public void SetCurrentVersion(Version version) {
			this.Exec(() => this.ViewControl.version.Text = version.ToString());
		}

		public void SetContent(string path) {
			this.Exec(() => {
				try {
					using( var stream = this.GetType().Assembly.GetManifestResourceStream(path) ) {
						FlowDocument document = (FlowDocument)XamlReader.Load(stream);
						this.ViewControl.flowDocumentReader.Document = document;
					}
				} catch {
					//intentionally do nothing here. we have message in the viewer if loading fails.
				}
			});
		}
		#endregion
	}
}
