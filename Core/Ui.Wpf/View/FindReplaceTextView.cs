﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MemBus;
using Naracea.Common;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.View {
	public class StoredItem {
		public string Text { get; private set; }
		public void Forget() {
			_owner.Remove(this);
		}

		ObservableCollection<StoredItem> _owner;
		public StoredItem(string text, ObservableCollection<StoredItem> owner) {
			this.Text = text;
			_owner = owner;
		}

		public override string ToString() {
			return this.Text;
		}
	}

	public class FindReplaceTextView : Naracea.View.ViewBase, IFindReplaceTextView {
		#region Private attributes
		ApplicationView _appView;
		Compounds.WindowSizeRestorer _windowRestorer;
		ObservableCollection<StoredItem> _searchList = new ObservableCollection<StoredItem>();
		ObservableCollection<StoredItem> _replaceList = new ObservableCollection<StoredItem>();
		int _historyLength = Settings.MaximumLengthOfFindReplaceHistory;
		#endregion

		#region ctor
		public FindReplaceTextView(IBus bus, ApplicationView appView)
			: base(bus) {
			_appView = appView;
			this.ViewControl = new Forms.FindTextWindow();
			this.ViewControl.ApplicationView = _appView;
			this.ViewControl.searches.ItemsSource = _searchList;
			this.ViewControl.replaces.ItemsSource = _replaceList;
			this.InstallHandlers();
		}
		#endregion

		#region IFindTextView Members
		public SearchType Type {
			get {
				SearchType result = SearchType.Text;
				this.Exec(() => result = this.ViewControl.searchType.SelectedValue.As<NamedSearchType>().Type);
				return result;
			}
			private set {
				var type = from s in this.AvailableSearchTypes
									 where s.Type == value
									 select s;
				this.Exec(() => this.ViewControl.searchType.SelectedValue = type.First());
			}
		}

		public SearchScope Scope {
			get {
				SearchScope result = SearchScope.CurrentBranch;
				this.Exec(() => result = this.ViewControl.searchScope.SelectedValue.As<NamedSearchScope>().Scope);
				return result;
			}
			private set {
				var type = from s in this.AvailableSearchScopes
									 where s.Scope == value
									 select s;
				this.Exec(() => this.ViewControl.searchScope.SelectedValue = type.First());
			}
		}

		public bool MatchCase {
			get {
				var result = false;
				this.Exec(() => result = this.ViewControl.matchCase.IsChecked.Value);
				return result;
			}
			private set {
				this.Exec(() => this.ViewControl.matchCase.IsChecked = value);
			}
		}

		public bool MatchWholeWords {
			get {
				var result = false;
				this.Exec(() => result = this.ViewControl.matchWholeWords.IsChecked.Value);
				return result;
			}
			private set {
				this.Exec(() => this.ViewControl.matchWholeWords.IsChecked = value);
			}
		}

		public bool UseRegularExpressions {
			get {
				var result = false;
				this.Exec(() => result = this.ViewControl.regularExpressions.IsChecked.Value);
				return result;
			}
			private set {
				this.Exec(() => this.ViewControl.regularExpressions.IsChecked = value);
			}
		}

		public bool ClearResultsBeforeFindAll {
			get {
				var result = false;
				this.Exec(() => result = this.ViewControl.clearResultsBeforeFindAll.IsChecked.Value);
				return result;
			}
			private set {
				this.Exec(() => this.ViewControl.clearResultsBeforeFindAll.IsChecked = value);
			}
		}

		ObservableCollection<ISearchMatchItem> _matches;
		public ObservableCollection<ISearchMatchItem> Matches {
			get {
				return _matches;
			}
			set {
				_matches = value;
				this.Exec(() => this.ViewControl.matches.ItemsSource = _matches);
			}
		}

		public string FindPattern {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.searches.Text);
				return result;
			}
		}

		List<NamedSearchType> _availableSearchType;
		public IEnumerable<NamedSearchType> AvailableSearchTypes {
			get {
				return _availableSearchType;
			}
			set {
				_availableSearchType = new List<NamedSearchType>(value);
				this.Exec(() => this.ViewControl.searchType.ItemsSource = _availableSearchType);
			}
		}

		List<NamedSearchScope> _availableSearchScope;
		public IEnumerable<NamedSearchScope> AvailableSearchScopes {
			get {
				return _availableSearchScope;
			}
			set {
				_availableSearchScope = new List<NamedSearchScope>(value);
				this.Exec(() => this.ViewControl.searchScope.ItemsSource = _availableSearchScope);
			}
		}

		public Naracea.View.ViewFindOptions GetFindOptions() {
			return new Naracea.View.ViewFindOptions {
				ClearResultsOnFindAll = this.ViewControl.clearResultsBeforeFindAll.IsChecked.Value,
				MatchCase = this.ViewControl.matchCase.IsChecked.Value,
				MatchWholeWords = this.ViewControl.matchWholeWords.IsChecked.Value,
				FindPattern = this.ViewControl.searches.Text,
				Type = ( this.ViewControl.searchType.SelectedValue as NamedSearchType ).Type,
				Scope = ( this.ViewControl.searchScope.SelectedValue as NamedSearchScope ).Scope,
				UseRegex = this.UseRegularExpressions
			};
		}

		public void FocusResultList() {
			this.Open(_appView.Settings);
			if( this.ViewControl.matches.SelectedIndex > -1 ) {
				(this.ViewControl.matches.ItemContainerGenerator.ContainerFromIndex(this.ViewControl.matches.SelectedIndex) as ListBoxItem).Focus();
			} else {
				this.ViewControl.matches.Focus();
			}
		}
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Forms.FindTextWindow ViewControl { get; private set; }
		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
			this.Exec(() => this.ViewControl.SetBusy());
		}

		public void EndBusy() {
			if( _busyCount > 0 ) {
				_busyCount--;
			}
			if( !this.IsBusy ) {
				this.Exec(() => this.ViewControl.SetNotBusy());
			}
		}
		#endregion

		#region Private members
		void RestoreStringList(ObservableCollection<StoredItem> target, string id) {
			if( _appView.Settings.HasValue(id) ) {
				try {
					target.SetFrom(
						from item in _appView.Settings.Get<List<string>>(id).Take(_historyLength)
						select new StoredItem(item, target)
					);
				} catch( Exception ) {
					//well, invalid string
					//do nothing about it, just do not change the list
				}
			}
		}

		private void InstallHandlers() {
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Messages.ClosingAppView>(m => this.Close()));
			this.RegisterForDisposal(this.Bus.Subscribe<Naracea.View.Requests.ShowFindTextWindow>(m => {
				this.Open(_appView.Settings);
				if( !string.IsNullOrEmpty(m.SelectedText)) {
					this.ViewControl.searches.Text = m.SelectedText;
				}
			}));

			this.ViewControl.SizeChanged += (s, e) => this.HandleSizeChanged();
			this.ViewControl.LocationChanged += (s, e) => this.HandleSizeChanged();

			this.ViewControl.FindNextRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.FindNext(this.GetFindOptions()));
			this.ViewControl.FindNextAndReplaceRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplaceNext(this.GetReplaceOptions()));
			this.ViewControl.FindPreviousRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.FindPrevious(this.GetFindOptions()));

			this.ViewControl.FindAllRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.FindAll(this.GetFindOptions()));
			this.ViewControl.ReplaceAllRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplaceAll(this.GetReplaceOptions()));
			this.ViewControl.ForgetItem += (s, e) => ( e.Item as StoredItem ).Forget();
			this.ViewControl.ReplaceRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ReplaceItem(e.Item, this.GetReplaceOptions()));

			this.ViewControl.ClearAllResultsRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ClearAllFindResults());
			this.ViewControl.ClearCurrentBranchResultsRequested += (s, e) => this.Bus.Publish(new Naracea.View.Requests.ClearCurrentBranchFindResults());

			this.ViewControl.searches.PreviewKeyDown += this.Searches_PreviewKeyDown;
			this.ViewControl.replaces.PreviewKeyDown += this.Replaces_PreviewKeyDown;
			this.ViewControl.findAll.Click += this.Find_Click;

			this.ViewControl.matches.SelectionChanged += this.Matches_SelectionChanged;
		}

		void HandleSizeChanged() {
			_windowRestorer.HandleSizeChanged();
		}

		void Matches_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			if( this.ViewControl.matches.SelectedValue != null ) {
				var selectedMatch = this.ViewControl.matches.SelectedValue as ISearchMatchItem;
				this.Bus.Publish(new Naracea.View.Requests.FindResultSelected(selectedMatch));
			}
		}

		void Find_Click(object sender, RoutedEventArgs e) {
			this.AddToList(this.ViewControl.searches.Text, _searchList);
			this.AddToList(this.ViewControl.replaces.Text, _replaceList);
		}

		void Searches_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			switch( e.Key ) {
				case System.Windows.Input.Key.Enter:
					this.AddToList(this.ViewControl.searches.Text, _searchList);
					this.Bus.Publish(new Naracea.View.Requests.FindAll(this.GetFindOptions()));
					break;
			}
		}

		void Replaces_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			switch( e.Key ) {
				case System.Windows.Input.Key.Enter:
					this.AddToList(this.ViewControl.replaces.Text, _replaceList);
					break;
			}
		}

		void AddToList(string text, ObservableCollection<StoredItem> list) {
			if( !string.IsNullOrEmpty(text) && !list.Any(i => i.Text.Equals(text)) ) {
				list.Insert(0, new StoredItem(text, list));
			}
		}

		Naracea.View.Requests.ViewReplaceOptions GetReplaceOptions() {
			return new Naracea.View.Requests.ViewReplaceOptions {
				MatchCase = this.ViewControl.matchCase.IsChecked.Value,
				MatchWholeWords = this.ViewControl.matchWholeWords.IsChecked.Value,
				UseRegex = this.UseRegularExpressions,
				FindPattern = this.ViewControl.searches.Text,
				ReplacePattern = this.ViewControl.replaces.Text
			};
		}
		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			_windowRestorer = new Compounds.WindowSizeRestorer(this.ViewControl, _appView.Settings, "FindAndReplace");
			if( this.ViewControl.Owner == null ) {
				// if window doesn't have parent, give it the app view
				// it will make window app modal
				_appView.SetAsOwnerOf(this.ViewControl);
				_windowRestorer.RestoreWindowBounds();
				if( _appView.Settings.HasValue(Settings.SearchType) ) {
					this.Type = _appView.Settings.Get<SearchType>(Settings.SearchType);
				} else {
					this.Type = SearchType.Text;
				}
				if( _appView.Settings.HasValue(Settings.SearchScope) ) {
					this.Scope = _appView.Settings.Get<SearchScope>(Settings.SearchScope);
				} else {
					this.Scope  = SearchScope.CurrentBranch;
				}
				if( _appView.Settings.HasValue(Settings.SearchMatchCase) ) {
					this.MatchCase = _appView.Settings.Get<bool>(Settings.SearchMatchCase);
				}
				if( _appView.Settings.HasValue(Settings.SearchMatchWholeWords) ) {
					this.MatchWholeWords = _appView.Settings.Get<bool>(Settings.SearchMatchWholeWords);
				}
				if( _appView.Settings.HasValue(Settings.SearchUseRegularExpressions) ) {
					this.UseRegularExpressions = _appView.Settings.Get<bool>(Settings.SearchUseRegularExpressions);
				}
				if( _appView.Settings.HasValue(Settings.SearchClearResultsBeforeFindAll) ) {
					this.ClearResultsBeforeFindAll = _appView.Settings.Get<bool>(Settings.SearchClearResultsBeforeFindAll);
				}
				if( _appView.Settings.HasValue(Settings.SearchHistoryLength) ) {
					_historyLength = _appView.Settings.Get<int>(Settings.SearchHistoryLength);
				}
				this.RestoreStringList(_searchList, Settings.SearchSearchItems);
				this.RestoreStringList(_replaceList, Settings.SearchReplaceItems);
			}

			this.ViewControl.Show();
			this.ViewControl.Activate();
			_windowRestorer.WasOpen = true;
		}

		public void Close() {
			this.Exec(() => {
				// close can be called without ever opening the view
				// in such a case we don't have any settings
				if( this.ViewControl.Owner != null && _appView.Settings != null ) {
					_appView.Settings.Set(Settings.SearchType, this.Type);
					_appView.Settings.Set(Settings.SearchScope, this.Scope);
					_appView.Settings.Set(Settings.SearchMatchCase, this.MatchCase);
					_appView.Settings.Set(Settings.SearchMatchWholeWords, this.MatchWholeWords);
					_appView.Settings.Set(Settings.SearchUseRegularExpressions, this.UseRegularExpressions);
					_appView.Settings.Set(Settings.SearchClearResultsBeforeFindAll, this.ClearResultsBeforeFindAll);
					_appView.Settings.Set(
						Settings.SearchSearchItems,
						new List<string>(from item in _searchList select item.Text)
					);
					_appView.Settings.Set(
						Settings.SearchReplaceItems,
						new List<string>(from item in _replaceList.Take(_historyLength) select item.Text)
					);
					if( _windowRestorer != null ) {
						this.Exec(() => {
							_windowRestorer.StoreWindowBounds();
						});
					}
				}
				this.ViewControl.Close();
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion

		#region ICanHideShow Members
		public void Hide() {
			this.ViewControl.Hide();
		}

		public void Show() {
			this.ViewControl.Show();
		}
		#endregion
	}
}
