﻿using System;
using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.View {
	public class DocumentPropertiesView : ViewBase, IDocumentPropertiesView {
		#region Private attributes
		ApplicationView _appView;
		#endregion
		
		#region ctor
		public DocumentPropertiesView(IBus bus, ApplicationView appView)
			: base(bus) {
			_appView = appView;
			this.ViewControl = new Forms.DocumentProperties();
			_appView.SetAsOwnerOf(this.ViewControl);
			this.InstallHandlers();
		}
		#endregion

		#region IDocumentPropertiesView Members
		public string Name {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.name.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.name.Text = value);
			}
		}

		public string Comment {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.comment.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.comment.Text = value);
			}
		}

		public bool IsAutosaveEnabled {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.autosaveEnabled.IsChecked.Value);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.autosaveEnabled.IsChecked = value);
			}
		}

		public bool ExportOnSaveNothing {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.exportNothing.IsChecked.Value);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.exportNothing.IsChecked = value);
			}
		}

		public bool ExportOnSaveActiveBranch {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.exportActiveBranch.IsChecked.Value);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.exportActiveBranch.IsChecked = value);
			}
		}

		public bool ExportOnSaveAllBranches {
			get {
				bool result = false;
				this.Exec(() => result = this.ViewControl.exportAllBranches.IsChecked.Value);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.exportAllBranches.IsChecked = value);
			}
		}

		#endregion

		#region ICanOpenClose Members
		public void Open(ISettings settings) {
			this.ViewControl.name.Focus();
			this.ViewControl.ShowDialog();
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl.Close();
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion

		#region IHasControl Members
		public object Control {
			get { return this.ViewControl; }
		}

		internal Forms.DocumentProperties ViewControl { get; private set; }

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		private void InstallHandlers() {
			this.ViewControl.buttonOk.Click += this.buttonOk_Click;
			this.ViewControl.buttonCancel.Click += this.buttonCancel_Click;
			this.ViewControl.Closed += (s, e) => this.Close();
		}

		void buttonCancel_Click(object sender, System.Windows.RoutedEventArgs e) {
			this.Close();
		}

		void buttonOk_Click(object sender, System.Windows.RoutedEventArgs e) {
			this.Bus.Publish(new Naracea.View.Requests.CommitDocumentProperties(_appView.ActiveDocument, this));
		}
		#endregion
	}
}
