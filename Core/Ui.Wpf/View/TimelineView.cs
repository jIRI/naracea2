﻿using MemBus;
using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.Controls;
using Naracea.Core.Ui.Wpf.View.Compounds;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using Naracea.View.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View {
	public class TimelineView : Naracea.View.ViewBase, ITimelineView {
		#region Attributes
		List<Controls.TimelineBarControl> _bars = new List<Controls.TimelineBarControl>();
		ColorSetter _colorSetter;
		#endregion

		public TimelineView(IBus bus)
			: base(bus) {
			this.ViewControl = new Controls.Timeline(_bars);
			this.ViewControl.Width = double.NaN;
			_colorSetter = new ColorSetter(this.ViewControl.scrollViewer);
			this.InstallHandlers();
		}

		#region ITimelineView Members
		int _currentChangeIndex;
		public int CurrentChangeIndex {
			get {
				return _currentChangeIndex;
			}
			set {
				if (value != -1 && _currentChangeIndex != value) {
					_currentChangeIndex = value;
					//first find the bar
					var barIndex = -1;
					for (int i = 0; i < _bars.Count; i++) {
						var bar = _bars[i].Bar;
						if (bar.Ids.Count == 0) {
							continue;
						}
						if (bar.Ids[0] <= _currentChangeIndex && _currentChangeIndex <= bar.Ids[bar.Ids.Count - 1]) {
							barIndex = i;
							break;
						}
					}

					Debug.Assert(barIndex >= 0);
					//now find the index inside of bar
					//since bars are linear, there is good chance of finding the right index by calculation:
					var ids = _bars[barIndex].Bar.Ids;
					var index = _currentChangeIndex - ids[0];
					//check whether the calculation is right
					if (_currentChangeIndex != ids[index]) {
						//ok, we failed, let's find the right spot by dividing the interval
						int lower = 0;
						int upper = ids.Count - 1;
						do {
							index = (lower - upper) / 2;
							if (_currentChangeIndex > ids[index]) {
								lower = index;
							} else {
								upper = index;
							}
						} while (ids[index] != _currentChangeIndex);
					}

					Debug.Assert(index >= 0);
					this.Exec(() => {
						this.ViewControl.SetMarker(barIndex, index);
						this.ViewControl.EnsureMarkerVisible();
					});
				} else {
					_currentChangeIndex = value;
					if (_currentChangeIndex == -1) {
						this.Exec(() => {
							this.ViewControl.SetMarker(0, 0);
							this.ViewControl.EnsureMarkerVisible();
						});
					}
				}
			}
		}

		public IList<IUnitOfTime> AvailableGranularities { get; set; }
		public bool ShowEmptyBars { get; private set; }
		public IUnitOfTime Granularity { get; private set; }

		public void ClearBars() {
			_bars.Clear();
			this.CurrentChangeIndex = -1;
			this.Exec(() => {
				this.ViewControl.SetMarker(0, 0);
				this.ViewControl.Refresh();
			});
		}

		public void SetBars(IList<TimelineBar> bars) {
			this.Exec(() => {
				_bars.Clear();
				foreach (var bar in bars) {
					_bars.Add(new Controls.TimelineBarControl(bar));
				}
				this.Exec(() => {
					this.ViewControl.Refresh();
				});
			});
		}

		public void AddBar(TimelineBar bar) {
			this.Exec(() => {
				var newBar = new Controls.TimelineBarControl(bar);
				_bars.Add(newBar);
				this.ViewControl.BarAdded();
			});
		}

		public void UpdateLastBar(TimelineBar bar) {
			this.Exec(() => {
				Debug.Assert(_bars.Count > 0);
				_bars[_bars.Count - 1] = new Controls.TimelineBarControl(bar);
				this.ViewControl.LastBarUpdated();
			});
		}
		#endregion

		#region IHasContext Members
		public object Controller { get; set; }
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Controls.Timeline ViewControl { get; private set; }
		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		void InstallHandlers() {
			this.RegisterForDisposal(this.Bus.Subscribe<TimelineSettingsChanged>(
				m => {
					this.Granularity = m.Granularity;
					this.ShowEmptyBars = m.ShowEmptyBars;
				},
				new ShapeToFilter<TimelineSettingsChanged>(
					msg => msg.TimelineView == this && (this.Granularity != msg.Granularity || msg.ShowEmptyBars != this.ShowEmptyBars)
				)
			));
			// color changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeColor>(
				m => {
					_colorSetter.SetForeground(m.Foreground);
					_colorSetter.SetBackground(m.Background);
					_colorSetter.Apply();
					TimelineBarControl.UpdateColors(m.Foreground.Color, m.Background.Color);
					this.ViewControl.Refresh();
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeColor>(
				m => _colorSetter.Preview(m.Foreground, m.Background)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeColor>(
				m => _colorSetter.CancelPreview()
			));

			this.ViewControl.TimelineBarSelected += this.ViewControl_TimelineBarSelected;
			this.ViewControl.SizeChanged += (s, e) => this.Bus.Publish(new TimelineSizeChanged(this, this.ViewControl.RenderSize));
		}

		void ViewControl_TimelineBarSelected(object sender, Controls.Timeline.BarSelectedEventArgs e) {
			int id = -1;
			//small trick here - to make it look naturaly, we set ID to -1 so everything is rewinded when 
			//both bar and index are 0
			if (e.BarIndex != 0 || e.InBarPosition != 0) {
				Controls.TimelineBarControl bar = null;
				var originalBar = _bars[e.BarIndex];
				//find first non empty bar
				for (int i = e.BarIndex; i >= 0; i--) {
					if (_bars[i].Bar.Ids.Count > 0) {
						bar = _bars[i];
						break;
					}
				}

				//was the original bar non-empty?
				if (originalBar == bar) {
					//use id on the position
					id = bar.Bar.Ids[e.InBarPosition];
				} else if (bar != null && bar.Bar.Ids.Any()) {
					//use last id in bar
					id = bar.Bar.Ids.Last();
				}
			}

			this.Bus.Publish(new Naracea.View.Requests.TimelineChangeSelected(this, id));
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
			this.Exec(() => this.ViewControl.SetBusy());
		}

		public void EndBusy() {
			if (_busyCount > 0) {
				_busyCount--;
			}
			if (!this.IsBusy) {
				this.Exec(() => this.ViewControl.SetNotBusy());
			}
		}
		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			this.Exec(() => {
				//set colors properly
				Color foreground = SystemColors.WindowTextColor;
				if (settings.HasValue(Settings.ViewForegroundColor)) {
					foreground = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewForegroundColor));
					_colorSetter.SetForeground(new SolidColorBrush(foreground));
				}
				Color background = SystemColors.WindowColor;
				if (settings.HasValue(Settings.ViewBackgroundColor)) {
					background = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewBackgroundColor));
					_colorSetter.SetBackground(new SolidColorBrush(background));
				}
				_colorSetter.Apply();
				TimelineBarControl.UpdateColors(foreground, background);
			});
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion
	}
}
