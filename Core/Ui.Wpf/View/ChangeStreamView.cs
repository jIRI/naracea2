﻿using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Compounds;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;

namespace Naracea.Core.Ui.Wpf.View {
	public class ChangeStreamView : Naracea.View.ViewBase, IChangeStreamView {
		#region Attributes
		ColorSetter _colorSetter;
		#endregion

		public ChangeStreamView(IBus bus)
			: base(bus) {
			this.ViewControl = new Controls.ChangeStream();
			this.ViewControl.Width = double.NaN;
			this.InstallHandlers();
		}

		#region IChangeStreamView Members
		int _currentChangeIndex = -1;
		public int CurrentChangeIndex {
			get {
				return _currentChangeIndex;
			}
			set {
				_currentChangeIndex = value;
				this.Exec(() => this.ViewControl.SelectedItemId = _currentChangeIndex);
			}
		}

		public void Refresh() {
			this.Exec(() => {
				this.ViewControl.Refresh();
			});
		}

		public void AddChange(ChangeStreamItem item) {
			this.Exec(() => {
				this.ViewControl.AddItem(item);
			});
		}

		public void AddChanges(IEnumerable<ChangeStreamItem> items) {
			this.Exec(() => {
				foreach (var item in items) {
					this.ViewControl.AddItem(item);
				}
			});
		}

		#endregion

		#region IHasContext Members
		public object Controller { get; set; }
		#endregion

		#region IHasControl Members
		public object Control { get { return this.ViewControl; } }
		internal Controls.ChangeStream ViewControl { get; private set; }
		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		void InstallHandlers() {
			// color changes
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.ChangeColor>(
				m => {
					_colorSetter.SetForeground(m.Foreground);
					_colorSetter.SetBackground(m.Background);
					_colorSetter.Apply();
					this.ViewControl.UpdateColors(m.Foreground.Color, m.Background.Color);
				}
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.PreviewChangeColor>(
				m => _colorSetter.Preview(m.Foreground, m.Background)
			));
			this.RegisterForDisposal(this.Bus.Subscribe<Messages.CancelChangeColor>(
				m => _colorSetter.CancelPreview()
			));
			//change selected
			this.ViewControl.painter.SelectedItemChanged += (s, e) => {
				if (!this.IsBusy) {
					this.Bus.Publish(new Naracea.View.Requests.ChangeStreamChangeSelected(this, this.ViewControl.painter.SelectedItemId));
				}
			};
		}
		#endregion

		#region ICanBeBusy Members
		public bool IsBusy {
			get {
				return _busyCount > 0;
			}
		}

		int _busyCount = 0;
		public void BeginBusy() {
			_busyCount++;
			this.Exec(() => this.ViewControl.SetBusy());
		}

		public void EndBusy() {
			if (_busyCount > 0) {
				_busyCount--;
			}
			if (!this.IsBusy) {
				this.Exec(() => this.ViewControl.SetNotBusy());
			}
		}
		#endregion


		#region ICanUpdate Members
		int _updateCount = 0;
		public bool IsUpdating {
			get { return _updateCount > 0; }
		}

		public void BeginUpdate() {
			_updateCount++;
			this.Exec(() => {
				this.ViewControl.IsUpdating = this.IsUpdating;
			});
		}

		public void EndUpdate() {
			_updateCount--;
			this.Exec(() => {
				this.ViewControl.IsUpdating = this.IsUpdating;
			});
			Debug.Assert(_updateCount >= 0);
		}

		#endregion

		#region ICanOpenClose
		public void Open(ISettings settings) {
			this.Exec(() => {
				//set colors properly
				_colorSetter = new ColorSetter(this.ViewControl.scrollViewer);
				Color foreground = SystemColors.WindowTextColor;
				if (settings.HasValue(Settings.ViewForegroundColor)) {
					foreground = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewForegroundColor));
					_colorSetter.SetForeground(new SolidColorBrush(foreground));
				}
				Color background = SystemColors.WindowColor;
				if (settings.HasValue(Settings.ViewBackgroundColor)) {
					background = (Color)ColorConverter.ConvertFromString(settings.Get<string>(Settings.ViewBackgroundColor));
					_colorSetter.SetBackground(new SolidColorBrush(background));
				}
				_colorSetter.Apply();
				this.ViewControl.UpdateColors(foreground, background);
			});
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion
	}
}
