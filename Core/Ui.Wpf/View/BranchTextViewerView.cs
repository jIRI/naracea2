﻿using System;
using System.Windows.Documents;
using System.Windows.Markup;
using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.View {
	public class BranchTextViewerView : ViewBase, IBranchTextViewerView {
		#region Private attributes
		ApplicationView _appView;
		#endregion

		#region ctor
		public BranchTextViewerView(IBus bus, ApplicationView appView)
			: base(bus) {
			_appView = appView;
			this.ViewControl = new Forms.TextViewer();
			this.InstallHandlers();
		}
		#endregion

		#region ICanOpenClose Members
		public void Open(ISettings settings) {
			this.Exec(() => this.ViewControl.Show());
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl.Close();
				this.Dispose();
				this.ViewControl = null;
			});
		}
		#endregion

		#region IHasControl Members
		public Forms.TextViewer ViewControl { get; private set; }

		public object Control {
			get { return this.ViewControl; }
		}

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		private void InstallHandlers() {
			this.ViewControl.Closed += (s, e) => {
				this.Bus.Publish(new Naracea.View.Messages.BranchTextViewerClosed(this));
				this.Dispose();
			};
		}
		#endregion

		#region ICanHideShow Members
		public void Hide() {
			this.Exec(() => this.ViewControl.Hide());
		}

		public void Show() {
			this.Exec(() => this.ViewControl.Show());
		}
		#endregion

		#region IBranchTextViewerView Members
		public string Title {
			set {
				this.ViewControl.Title = value;
			}
		}

		public void UpdateText(string text, int paragraphNumber) {
			this.Exec(() => {
				//Block pendingVisibleBlock = null;
				FlowDocument flowDocument = (FlowDocument)XamlReader.Parse(text);				
				this.ViewControl.flowDocumentReader.Document = flowDocument;
				//pendingVisibleBlock.BringIntoView();
			});
		}
		#endregion
	}
}
