﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Windows;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.DataModel {
	public class SearchMatchItem : DependencyObject, ISearchMatchItem {
		public string SearchPattern { get; set; }

		public int PositionInText {
			get { return (int)GetValue(PositionInTextProperty); }
			set { SetValue(PositionInTextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PositionInTextProperty =
				DependencyProperty.Register("PositionInText", typeof(int), typeof(SearchMatchItem), new UIPropertyMetadata(0));

		public int PositionInPercents {
			get { return (int)GetValue(PositionInPercentsProperty); }
			set { SetValue(PositionInPercentsProperty, value); }
		}

		// Using a DependencyProperty as the backing store for PositionInPercents.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PositionInPercentsProperty =
				DependencyProperty.Register("PositionInPercents", typeof(int), typeof(SearchMatchItem), new UIPropertyMetadata(0));

		public string SurroundedMatchText {
			get { return (string)GetValue(SurroundedMatchTextProperty); }
			set { SetValue(SurroundedMatchTextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SurroundedMatchTextProperty =
				DependencyProperty.Register("SurroundedMatchText", typeof(string), typeof(SearchMatchItem), new UIPropertyMetadata(null));

		public string MatchedText {
			get { return (string)GetValue(MatchedTextProperty); }
			set { SetValue(MatchedTextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MatchedText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MatchedTextProperty =
				DependencyProperty.Register("MatchedText", typeof(string), typeof(SearchMatchItem), new UIPropertyMetadata(null));

		public int MatchOffsetInText {
			get { return (int)GetValue(MatchOffsetInTextProperty); }
			set { SetValue(MatchOffsetInTextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MatchOffsetInText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MatchOffsetInTextProperty =
				DependencyProperty.Register("MatchOffsetInText", typeof(int), typeof(SearchMatchItem), new UIPropertyMetadata(0));

		public IDocumentView DocumentView { get; set; }
		public IBranchView BranchView { get; set; }
		public string DocumentName {
			get {
				return this.DocumentView.Name;
			}
		}

		public string BranchName {
			get {
				return this.BranchView.Name;
			}
		}

		public int CurrentChangeIndex { get; set; }
		public DateTime DateTime { get; set; }
		public bool IsHistorySearch { get; set; }
		public bool IsReplaceable {
			get {
				return !this.IsHistorySearch;
			}
		}
	}
}
