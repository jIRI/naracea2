﻿using System;
using System.Collections.Generic;
using System.Linq;
using MemBus;
using Naracea.Common;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.View {
	public class BranchPropertiesView : ViewBase, IBranchPropertiesView {
		#region Private attributes
		ApplicationView _appView;
		#endregion

		#region ctor
		public BranchPropertiesView(IBus bus, ApplicationView appView)
			: base(bus) {
			_appView = appView;
			this.ViewControl = new Forms.BranchProperties();
			_appView.SetAsOwnerOf(this.ViewControl);
			this.InstallHandlers();
		}
		#endregion

		#region IBranchPropertiesView Members
		public string Name {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.name.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.name.Text = value);
			}
		}

		public string ExportFileName {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.exportFilename.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.exportFilename.Text = value);
			}
		}

		public string Comment {
			get {
				string result = null;
				this.Exec(() => result = this.ViewControl.comment.Text);
				return result;
			}
			set {
				this.Exec(() => this.ViewControl.comment.Text = value);
			}
		}

		public bool IsAlwaysExporting {
			get {
				bool? result = false;
				this.Exec(() => result = this.ViewControl.alwaysExport.IsChecked);
				return result ?? false;
			}
			set {
				this.Exec(() => this.ViewControl.alwaysExport.IsChecked = value);
			}
		}

		public string Author {
			set {
				this.Exec(() => this.ViewControl.author.Text = value);
			}
		}

		public string Parent {
			set {
				this.Exec(() => this.ViewControl.parent.Text = value);
			}
		}

		public DateTimeOffset BranchingDateTime {
			set {
				this.Exec(() => this.ViewControl.branchDate.Text = value.LocalDateTime.ToString());
			}
		}

		public IEnumerable<ExportFormatItem> ExportFormats {
			set {
				this.Exec(() => this.ViewControl.exportFormat.ItemsSource = value.ToArray());
			}
		}

		public ExportFormatItem ExportFormat {
			set {
				this.Exec(() => this.ViewControl.exportFormat.SelectedValue = value);
			}
			get {
				ExportFormatItem currentItem = null;
				this.Exec(() => currentItem = (ExportFormatItem)this.ViewControl.exportFormat.SelectedValue);
				return currentItem;
			}
		}
		#endregion

		#region ICanOpenClose Members
		public void Open(ISettings settings) {
			this.ViewControl.name.Focus();
			this.ViewControl.ShowDialog();
		}

		public void Close() {
			this.Exec(() => {
				this.ViewControl.Close();
				this.ViewControl = null;
				this.Dispose();
			});
		}
		#endregion


		#region IHasControl Members
		public object Control {
			get { return this.ViewControl; }
		}

		internal Forms.BranchProperties ViewControl { get; private set; }

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}
		#endregion

		#region Private methods
		private void InstallHandlers() {
			this.ViewControl.buttonOk.Click += this.buttonOk_Click;
			this.ViewControl.buttonCancel.Click += this.buttonCancel_Click;
			this.ViewControl.Closed += (s, e) => this.Close();
		}

		void buttonCancel_Click(object sender, System.Windows.RoutedEventArgs e) {
			this.Close();
		}

		void buttonOk_Click(object sender, System.Windows.RoutedEventArgs e) {
			this.Bus.Publish(new Naracea.View.Requests.CommitBranchProperties(_appView.ActiveDocument, _appView.ActiveDocument.ActiveBranch, this));
		}
		#endregion
	}
}
