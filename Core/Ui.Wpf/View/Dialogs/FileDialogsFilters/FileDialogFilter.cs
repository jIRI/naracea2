﻿using Naracea.Core.Ui.Wpf.View.Dialogs.FileDialogsFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.View.Dialogs.FileDialogsFilters {
	internal static class FileDialogFilter {
		struct NameExtensionPair {
			public string Name {get; set;}
			public string Extension {get; set;}
		}

		static readonly NameExtensionPair[] _documentFilterParts = new NameExtensionPair[] {
			new NameExtensionPair{ 
				Name = FilterRes.NaraceaDocumentsName, 
				Extension = FilterRes.NaraceaDocumentsExt 
			},
			new NameExtensionPair{ 
				Name = FilterRes.AllFilesName, 
				Extension = FilterRes.AllFilesExt
			},
		};

		static readonly NameExtensionPair[] _dictionaryFilterParts = new NameExtensionPair[] {
			new NameExtensionPair{ 
				Name = FilterRes.SpellcheckerDictionaryName, 
				Extension = FilterRes.SpellcheckerDictionaryExt 
			},
		};

		static readonly NameExtensionPair[] _textFileFilterParts = new NameExtensionPair[] {
			new NameExtensionPair{ 
				Name = FilterRes.TextFileName, 
				Extension = FilterRes.TextFileExt
			},
		};

		static readonly NameExtensionPair[] _htmlFilterParts = new NameExtensionPair[] {
			new NameExtensionPair{ 
				Name = FilterRes.HtmlFileName, 
				Extension = FilterRes.HtmlFileExt
			},
		};

		static readonly NameExtensionPair[] _rtfFilterParts = new NameExtensionPair[] {
			new NameExtensionPair{ 
				Name = FilterRes.RtfFileName, 
				Extension = FilterRes.RtfFileExt 
			},
		};

		static FileDialogFilter() {
			FileDialogFilter.DocumentFilter = FileDialogFilter.BuildFilterString(_documentFilterParts);
			FileDialogFilter.DictionaryFilter = FileDialogFilter.BuildFilterString(_dictionaryFilterParts);
			FileDialogFilter.TextFilter =  FileDialogFilter.BuildFilterString(_textFileFilterParts);
			FileDialogFilter.HtmlFilter = FileDialogFilter.BuildFilterString(_htmlFilterParts);
			FileDialogFilter.RtfFilter = FileDialogFilter.BuildFilterString(_rtfFilterParts );
		}

		private static string BuildFilterString(IEnumerable<NameExtensionPair> list) {
			var filterPairs = new List<string>();
			foreach( var pair in list ) {
				filterPairs.Add(string.Format("{0} ({1})|{1}", pair.Name, pair.Extension));
			}
			return string.Join("|", filterPairs.ToArray());
		}

		public static string DocumentFilter { get; private set; }
		public static string DictionaryFilter { get; private set; }
		public static string TextFilter { get; private set; }
		public static string HtmlFilter { get; private set; }
		public static string RtfFilter { get; private set; }
	}
}
