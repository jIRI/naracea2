﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Threading;
using System.Windows;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class ConfirmBranchDeletionDialog : OkCancelDialog, IConfirmBranchDeletionDialog {
		public ConfirmBranchDeletionDialog(ApplicationView appView)
			: base(appView) {
		}

		public override System.Windows.Window GetDialogWindow() {
			return new Forms.ConfirmBranchDeletion();
		}
	}
}
