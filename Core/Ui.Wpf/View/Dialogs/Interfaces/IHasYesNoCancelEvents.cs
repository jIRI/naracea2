﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	internal interface IHasYesNoCancelEvents {
		event EventHandler YesClicked;
		event EventHandler NoClicked;
		event EventHandler CancelClicked;
	}
}
