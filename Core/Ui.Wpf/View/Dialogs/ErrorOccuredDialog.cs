﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View;
using System.Windows;
using System.Diagnostics;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class ErrorOccuredDialog : IErrorOccuredDialog {
		#region ctor
		ApplicationView _appView;

		public ErrorOccuredDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}
		#endregion
	
		#region IErrorOccuredDialog Members
		public string ShortMessage { get; set; }
		public string DetailedMessage { get; set; }

		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = new Forms.ErrorOccured();
				_appView.SetAsOwnerOf(dialog);
				dialog.ShortMessage = this.ShortMessage;
				dialog.DetailedMessage = this.DetailedMessage;
				dialog.ShowDialog();
				dialog.Owner = null;
			}));
		}
		#endregion
	}
}
