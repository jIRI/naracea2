﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Diagnostics;
using Microsoft.Win32;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class SaveDocumentDialog : ISaveDocumentDialog {
		#region ctor
		ApplicationView _appView;

		public SaveDocumentDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}
		#endregion

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = GetSaveFileDialog();
				var result = dialog.ShowDialog();
				if( !result.HasValue || !result.Value ) {
					return;
				}

				if( dialog.FileName != null ) {
					if( this.FileSelected != null ) {
						this.FileSelected(this, new Naracea.View.Events.FileSelectedEventArgs(dialog.FileName));
					}
				}
			}));
		}
		#endregion

		#region IFileSelectionDialogResult Members
		public event EventHandler<FileSelectedEventArgs> FileSelected;
		#endregion

		#region Private methods
		SaveFileDialog GetSaveFileDialog() {
			var dialog = new Microsoft.Win32.SaveFileDialog() {
				AddExtension = true,
				OverwritePrompt = true,
				CheckFileExists = false,
				CheckPathExists = true,
				Filter = FileDialogsFilters.FileDialogFilter.DocumentFilter,
				FilterIndex = 0
			};
			return dialog;
		}
		#endregion
	}
}
