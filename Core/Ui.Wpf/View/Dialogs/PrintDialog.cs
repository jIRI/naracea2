﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Windows.Documents;
using MemBus;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.IO;
using System.IO.Packaging;
using System.Windows.Xps.Packaging;
using System.Windows.Markup;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class PrintDialog : IPrintDialog {
		#region Private attributes
		ApplicationView _appView;
		#endregion

		#region ctor
		public PrintDialog(ApplicationView appView) {
			_appView = appView;
			this.ViewControl = new Forms.PrintPreviewDialog();
			_appView.SetAsOwnerOf(this.ViewControl);
		}
		#endregion

		#region IPrintDialog Members
		public string DocumentName { get; set; }
		public string Xaml { get; set; }
		#endregion

		#region IDialog Members
		public void ShowDialog() {
			this.Exec(() => {
				using( var stream = new MemoryStream() ) {
					var printDialog = new System.Windows.Controls.PrintDialog();
					FlowDocument flowDocument = (FlowDocument)XamlReader.Parse(this.Xaml);				
					flowDocument.ColumnWidth = printDialog.PrintableAreaWidth;
					flowDocument.PagePadding = new System.Windows.Thickness(25);

					Package package = Package.Open(stream, FileMode.Create, FileAccess.ReadWrite);
					var uri = new Uri(@"memorystream://naraceaPrintPreview.xps");
					PackageStore.AddPackage(uri, package);
					var xpsDoc = new XpsDocument(package);

					xpsDoc.Uri = uri;
					IDocumentPaginatorSource dps = flowDocument;
					XpsDocument.CreateXpsDocumentWriter(xpsDoc).Write(dps.DocumentPaginator);
					
					this.ViewControl.documentViewer.Document = xpsDoc.GetFixedDocumentSequence();
					this.ViewControl.ShowDialog();
					PackageStore.RemovePackage(uri);
				}
			});
		}
		#endregion

		#region IHasControl Members
		public Forms.PrintPreviewDialog ViewControl { get; private set; }

		public object Control {
			get { return this.ViewControl; }
		}

		public void Exec(Action action) {
			this.ViewControl.ExecuteOnUiThread(action);
		}

		#endregion
	}
}
