﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View;
using System.Windows;
using System.Diagnostics;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public abstract class YesNoCancelDialog : 
		IDialog,
		IHasYesDialogResult,
		IHasNoDialogResult,
		IHasCancelDialogResult 
	{
		ApplicationView _appView;

		public YesNoCancelDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}

		public abstract Window GetDialogWindow();

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(() => {
				var dialog = this.GetDialogWindow();
				var dialogEvents = dialog as IHasYesNoCancelEvents;
				dialogEvents.CancelClicked += (s, e) => this.FireCancelEvent();
				dialogEvents.YesClicked += (s, e) => this.FireYesEvent();
				dialogEvents.NoClicked += (s, e) => this.FireNoEvent();
				_appView.SetAsOwnerOf(dialog);
				dialog.ShowDialog();
				dialog.Owner = null;
			});
		}
		#endregion

		#region IOkCancelDialogResult Members
		public event EventHandler<DialogResultEventArgs> YesSelected;
		public event EventHandler<DialogResultEventArgs> NoSelected;
		public event EventHandler<DialogResultEventArgs> CancelSelected;
		#endregion

		#region Private methods
		private void FireCancelEvent() {
			if( this.CancelSelected != null ) {
				this.CancelSelected(this, new Naracea.View.Events.DialogResultEventArgs());
			}
		}

		private void FireYesEvent() {
			if( this.YesSelected != null ) {
				this.YesSelected(this, new Naracea.View.Events.DialogResultEventArgs());
			}
		}

		private void FireNoEvent() {
			if( this.NoSelected != null ) {
				this.NoSelected(this, new Naracea.View.Events.DialogResultEventArgs());
			}
		}
		#endregion
	}
}
