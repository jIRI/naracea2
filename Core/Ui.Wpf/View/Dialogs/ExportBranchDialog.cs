﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Diagnostics;
using Microsoft.Win32;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class ExportBranchDialog : IExportBranchDialog {
		#region ctor
		ApplicationView _appView;

		public ExportBranchDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
			this.SuggestedFilename = "";
		}
		#endregion

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = GetSaveFileDialog();
				var result = dialog.ShowDialog();
				if( !result.HasValue || !result.Value ) {
					return;
				}

				if( dialog.FileName != null ) {
					if( this.FileSelected != null ) {
						this.FileSelected(this, new Naracea.View.Events.FileSelectedEventArgs(dialog.FileName));
					}
				}
			}));
		}
		#endregion

		#region IFileSelectionDialogResult Members
		public event EventHandler<FileSelectedEventArgs> FileSelected;
		#endregion

		#region Private methods
		SaveFileDialog GetSaveFileDialog() {
			var dialog = new Microsoft.Win32.SaveFileDialog() {
				Title = this.Title,
				AddExtension = true,
				OverwritePrompt = true,
				CheckFileExists = false,
				CheckPathExists = true,
				FileName = this.SuggestedFilename,
				Filter = this.Filter,
				FilterIndex = 0,		
				DefaultExt = this.DefaultExtension,
			};
			return dialog;
		}
		#endregion

		#region IExportBranchDialog Members
		public string SuggestedFilename { get; set; }
		public string DefaultExtension { get; set; }
		public string Filter { get; set; }
		public string Title { get; set; }
		#endregion
	}
}
