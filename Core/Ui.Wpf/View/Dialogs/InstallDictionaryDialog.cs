﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Diagnostics;
using Microsoft.Win32;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class InstallDictionaryDialog : IInstallDictionaryDialog {
		#region ctor
		ApplicationView _appView;

		public InstallDictionaryDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}
		#endregion

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = GetOpenFileDialog();
				var result = dialog.ShowDialog();
				if( !result.HasValue || !result.Value ) {
					return;
				}
				if( dialog.FileNames.Length > 1 ) {
					if( this.FilesSelected != null ) {
						this.FilesSelected(this, new FilesSelectedEventArgs(dialog.FileNames));
					}
				} else if( dialog.FileName != null ) {
					if( this.FileSelected != null ) {
						this.FileSelected(this, new FileSelectedEventArgs(dialog.FileName));
					}
				}
			}));
		}
		#endregion

		#region IFileSelectionDialogResult Members
		public event EventHandler<FileSelectedEventArgs> FileSelected;
		#endregion

		#region IFilesSelectionDialogResult Members
		public event EventHandler<FilesSelectedEventArgs> FilesSelected;
		#endregion

		#region Private methods
		OpenFileDialog GetOpenFileDialog() {
			var dialog = new Microsoft.Win32.OpenFileDialog() {
				Multiselect = true,
				CheckFileExists = true,
				CheckPathExists = true,
				Filter = FileDialogsFilters.FileDialogFilter.DictionaryFilter,
				FilterIndex = 0
			};
			return dialog;
		}
		#endregion
	}
}
