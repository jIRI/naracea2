﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using System.Threading;
using System.Windows;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public class FileOperationsAreLockedDialog : IFileOperationsAreLockedDialog {
		ApplicationView _appView;

		public FileOperationsAreLockedDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = new Forms.FileOperationsAreLockedDialog();
				_appView.SetAsOwnerOf(dialog);
				dialog.ShowDialog();
				dialog.Owner = null;
			}));
		}
		#endregion
	}
}
