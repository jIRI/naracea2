﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View;
using System.Windows;
using System.Diagnostics;
using Naracea.View.Events;
using Naracea.Core.Ui.Wpf.View.Extensions;

namespace Naracea.Core.Ui.Wpf.View.Dialogs {
	public abstract class OkCancelDialog :
		IDialog,
		IHasOkDialogResult,
		IHasCancelDialogResult 
	{
		#region ctor
		ApplicationView _appView;

		public OkCancelDialog(ApplicationView appView) {
			Debug.Assert(appView != null);
			_appView = appView;
		}
		#endregion

		#region Abstract methods
		public abstract Window GetDialogWindow();
		#endregion

		#region IDialog Members
		public void ShowDialog() {
			_appView.Exec(new Action(() => {
				var dialog = this.GetDialogWindow();
				_appView.SetAsOwnerOf(dialog);
				var result = dialog.ShowDialog();
				if( !result.HasValue ) {
					this.FireCancelEvent();
				} else {
					if( result.Value ) {
						this.FireOkEvent();
					} else {
						this.FireCancelEvent();
					}
				}
				dialog.Owner = null;
			}));
		}
		#endregion

		#region IOkCancelDialogResult Members
		public event EventHandler<DialogResultEventArgs> OkSelected;
		public event EventHandler<DialogResultEventArgs> CancelSelected;
		#endregion

		#region Private methods
		private void FireCancelEvent() {
			if( this.CancelSelected != null ) {
				this.CancelSelected(this, new Naracea.View.Events.DialogResultEventArgs());
			}
		}

		private void FireOkEvent() {
			if( this.OkSelected != null ) {
				this.OkSelected(this, new Naracea.View.Events.DialogResultEventArgs());
			}
		}
		#endregion
	}
}
