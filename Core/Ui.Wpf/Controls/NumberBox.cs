﻿using Naracea.Common.Extensions;
using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Naracea.Core.Ui.Wpf.Controls
{
	public class NumberBox : TextBox
	{

		#region Dependency property
		public int MaximumNumberLength
		{
			get { return (int)GetValue(MaximumNumberLengthProperty); }
			set { SetValue(MaximumNumberLengthProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MaximumNumberLength.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MaximumNumberLengthProperty =
				DependencyProperty.Register("MaximumNumberLength", typeof(int), typeof(NumberBox), new UIPropertyMetadata(-1));

		public int Value
		{
			get { return (int)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValueProperty =
				DependencyProperty.Register("Value", typeof(int), typeof(NumberBox), new UIPropertyMetadata(0, OnValueChanged));

		public int MinimumValue
		{
			get { return (int)GetValue(MinimumValueProperty); }
			set { SetValue(MinimumValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MinimumValue.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MinimumValueProperty =
				DependencyProperty.Register("MinimumValue", typeof(int), typeof(NumberBox), new UIPropertyMetadata(0));

		public int MaximumValue
		{
			get { return (int)GetValue(MaximumValueProperty); }
			set { SetValue(MaximumValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MaximumValue.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MaximumValueProperty =
				DependencyProperty.Register("MaximumValue", typeof(int), typeof(NumberBox), new UIPropertyMetadata(int.MaxValue));
		#endregion

		#region Attributes
		int _lastValidNumber = 0;
		#endregion

		#region Events
		public event EventHandler ValueChanged;
		#endregion

		#region ctors
		public NumberBox()
			: base()
		{
			this.PreviewTextInput += this.NumbericBox_PreviewTextInput;
		}

		static NumberBox()
		{
			var commandMappings = new ControlMapping[] {
				new ControlMapping {
					Type = typeof(NumberBox),
					Binding = new CommandBinding(System.Windows.Input.ApplicationCommands.Paste, ExecutePasteCommand, CanExecutePasteCommand),
				},
			};
			//update system commands
			ControlMapping.Register(commandMappings);
		}
		#endregion

		#region Paste command
		public static void CanExecutePasteCommand(object sender, CanExecuteRoutedEventArgs e)
		{
			var editor = sender as NumberBox;
			try
			{
				bool canExecute = Clipboard.ContainsText();
				if (canExecute)
				{
					var text = Clipboard.GetText();
					canExecute = editor.IsNumber(text);
					if (canExecute)
					{
						canExecute = (editor.Text.Length - editor.SelectionLength + text.Length) <= editor.MaximumNumberLength;
					}
				}
				e.CanExecute = canExecute;
			}
			catch (COMException)
			{
				//well, here we get com exception from opening clipboard from time to time
				//there is hardly anything we can do about that...
				e.CanExecute = false;
			}
			e.Handled = true;
		}

		public static void ExecutePasteCommand(object sender, ExecutedRoutedEventArgs e)
		{
			var editor = sender as NumberBox;
			try
			{
				bool hasText = Clipboard.ContainsText();
				if (hasText)
				{
					var text = Clipboard.GetText();
					editor.SelectedText = text;
					editor.SelectionLength = 0;
					editor.SelectionStart = editor.SelectionStart + text.Length;
				}
			}
			catch (COMException)
			{
				//well, here we get com exception from opening clipboard from time to time
				//there is hardly anything we can do about that...
			}
		}
		#endregion

		protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
		{
			base.OnLostKeyboardFocus(e);
			this.NotifyValueChangeIfNeccessary();
		}

		protected override void OnTextChanged(System.Windows.Controls.TextChangedEventArgs e)
		{
			base.OnTextChanged(e);
			if (this.IsNumber(this.Text))
			{
				_lastValidNumber = int.Parse(this.Text);
			}
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			switch (e.Key)
			{
				case Key.Enter:
					this.NotifyValueChangeIfNeccessary();
					break;
				default:
					//do nothing
					break;
			}
		}

		void NumbericBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
		{
			bool isValidNumber = true;
			int maxNumLen = this.MaximumNumberLength;
			if (maxNumLen == -1 || (this.Text.Length - this.SelectionLength + e.Text.Length) <= maxNumLen)
			{
				isValidNumber = this.IsNumber(e.Text);
			}
			else
			{
				isValidNumber = false;
			}
			e.Handled = !isValidNumber;
		}

		static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var editor = d as NumberBox;
			editor._lastValidNumber = (int)e.NewValue;
			editor.Text = editor._lastValidNumber.ToString();
		}

		bool IsNumber(string text)
		{
			bool isValidNumber = text.Length > 0;
			foreach (var c in text)
			{
				if (!Char.IsNumber(c))
				{
					isValidNumber = false;
					break;
				}
			}
			return isValidNumber;
		}

		void NotifyValueChangeIfNeccessary()
		{
			if (!this.IsNumber(this.Text))
			{
				this.Text = _lastValidNumber.ToString();
			}
			else
			{
				_lastValidNumber = int.Parse(this.Text);
				if (_lastValidNumber > this.MaximumValue)
				{
					_lastValidNumber = this.MaximumValue;
					this.Text = _lastValidNumber.ToString();
				}
				if (_lastValidNumber < this.MinimumValue)
				{
					_lastValidNumber = this.MinimumValue;
					this.Text = _lastValidNumber.ToString();
				}
			}
			if (this.Value != _lastValidNumber)
			{
				this.Value = _lastValidNumber;
				this.ValueChanged.Raise(this, new EventArgs());
			}
		}
	}
}
