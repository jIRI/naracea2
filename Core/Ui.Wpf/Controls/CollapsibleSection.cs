﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Controls.Primitives;
using System.Windows;
using System.Windows.Input;

namespace Naracea.Core.Ui.Wpf.Controls {
	public class CollapsibleSection : Section {
		private ToggleButton _expandCollapseToggleButton;
		public CollapsibleSection() {
			this.CollapsibleBlocks = new List<Block>();
		}

		public Paragraph Header { get; set; }
		public List<Block> CollapsibleBlocks { get; private set; }

		public bool IsCollapsed {
			get {
				return !( _expandCollapseToggleButton.IsChecked ?? false );
			}
			set {
				_expandCollapseToggleButton.IsChecked = !value;
			}
		}

		protected override void OnInitialized(EventArgs e) {
			var header = from block in this.Blocks
									 let tag = block.Tag as string
									 where tag != null && tag.ToLowerInvariant().Equals("header")
									 select block;
			var headerParagraph = header.FirstOrDefault() as Paragraph;
			if( headerParagraph == null ) {
				return;
			}

			this.Blocks.Remove(headerParagraph);
			this.Header = new Paragraph();
			_expandCollapseToggleButton = new ToggleButton();
			_expandCollapseToggleButton.Content = "+";
			_expandCollapseToggleButton.Width = 20;
			_expandCollapseToggleButton.Margin = new Thickness(0, 0, 10, 0);
			_expandCollapseToggleButton.Click += this.ExpandCollapseToggleButton_Click;
			var inlineUIContainer = new InlineUIContainer(_expandCollapseToggleButton);
			inlineUIContainer.BaselineAlignment = BaselineAlignment.Center;
			inlineUIContainer.Cursor = Cursors.Arrow;
			this.Header.Inlines.Add(inlineUIContainer);
			while( headerParagraph.Inlines.Count > 0 ) {
				var inline = headerParagraph.Inlines.FirstInline;
				headerParagraph.Inlines.Remove(inline);
				this.Header.Inlines.Add(inline);
			}
			this.CollapsibleBlocks.AddRange(this.Blocks);
			this.Blocks.Clear();
			this.Invalidate();
			base.OnInitialized(e);
		}

		public void Invalidate() {
			this.Blocks.Clear();

			if( this.CollapsibleBlocks.Count == 0 ) {
				_expandCollapseToggleButton.IsChecked = null;
			}

			this.Blocks.Add(Header);

			if( !this.IsCollapsed ) {
				_expandCollapseToggleButton.Content = "-";
				this.Blocks.AddRange(this.CollapsibleBlocks);
			} else {
				_expandCollapseToggleButton.Content = "+";
			}
		}

		void ExpandCollapseToggleButton_Click(object sender, RoutedEventArgs e) {
			Invalidate();
		}
	}
}
