﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Naracea.View;
using Naracea.Core.Ui.Wpf.View.Extensions;
using Naracea.Common.Extensions;
using System.Diagnostics;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Threading;

namespace Naracea.Core.Ui.Wpf.Controls {
	public enum ColorType {
		Normal,
		Delete,
		NonContentChange
	}

	public class VisualItem {
		public static readonly VisualItem None = new VisualItem() {
			Rectangle = new Rect(),
			Text = null,
			Id = -1,
			ContentChange = false,
			Color = ColorType.Normal
		};

		public ColorType Color { get; set; }
		public Rect Rectangle { get; set; }
		public FormattedText Text { get; set; }
		public int Id { get; set; }
		public bool ContentChange { get; set; }
	}

	public class HighlightAdorner : Adorner {
		ChangeStreamPainter _painter;
		public HighlightAdorner(ChangeStreamPainter painter)
			: base(painter) {
			_painter = painter;
			_painter.HighlightAdorner = this;
			this.HighlightRect = Rect.Empty;
			this.SelectRect = Rect.Empty;
		}

		public Rect HighlightRect { get; set; }
		public Rect SelectRect { get; set; }

		protected override void OnRender(DrawingContext drawingContext) {
			if( !this.HighlightRect.IsEmpty ) {
				var rectPen = new Pen(_painter.Foreground, 2);
				rectPen.Freeze();
				drawingContext.DrawRectangle(null, rectPen, this.HighlightRect);
			}

			if( !this.SelectRect.IsEmpty ) {
				var rectPen = new Pen(_painter.Foreground, 2);
				rectPen.Freeze();
				drawingContext.PushClip(new RectangleGeometry(this.SelectRect));
				drawingContext.PushOpacity(0.5);
				drawingContext.DrawRectangle(SystemColors.HighlightBrush, null, this.SelectRect);
				drawingContext.Pop();
				drawingContext.Pop();
			}
		}

		protected override void OnMouseMove(MouseEventArgs e) {
			base.OnMouseMove(e);
			_painter.HandleOnMouseMove(e);
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e) {
			base.OnMouseLeftButtonDown(e);
			_painter.HandleOnMouseLeftButtonDown(e);
		}
	}

	/// <summary>
	/// Interaction logic for Painter.xaml
	/// </summary>
	public partial class ChangeStreamPainter : UserControl {
		#region Attributes
		const double DefaultMargin = 1.0;
		const double MaxTextWidth = 160.0;
		const double MinTextWidth = 11.0;

		double _lastItemX = ChangeStreamPainter.DefaultMargin;
		double _lastItemY = ChangeStreamPainter.DefaultMargin;
		int _firstVisibleItemIndex = -1;
		int _lastVisibleItemIndex = -1;
		int _firstFullyVisibleItemIndex = -1;
		int _lastFullyVisibleItemIndex = -1;
		List<VisualItem> _items = new List<VisualItem>();
		List<int> _lines = new List<int>();
		Brush _foregroundNormal;
		Brush _foregroundDelete;
		Brush _foregroundNonContent;
		Brush _backgroundNormal;
		Brush _backgroundDelete;
		Brush _backgroundNonContent;
		Task _updateTask;
		object _updateLock = new object();
		CancellationTokenSource _cancellationTokenSource;
		bool _refreshingLines = false;
		object _lineRefreshingLock = new object();
		#endregion

		#region ctor
		public ChangeStreamPainter() {
			InitializeComponent();
			this.UpdateColors(Colors.Black, Colors.White);
		}
		#endregion

		#region Events
		public event EventHandler SelectedItemChanged;
		#endregion

		#region Properties
		public HighlightAdorner HighlightAdorner { get; set; }
		Rect _visibleRect;
		public Rect VisibleRect {
			get {
				return _visibleRect;
			}
			set {
				if( value != _visibleRect ) {
					bool refreshRectangles = _visibleRect.Width != value.Width;
					_visibleRect = value;
					if( !this.IsUpdating ) {
						// update items layout
						if( refreshRectangles ) {
							this.Refresh();
						} else {
							this.UpdateVisibleItems();
							this.UpdateFullyVisibleItems();
							this.InvalidateVisual();
							this.RefreshSelection();
						}
					}
				}
			}
		}

		bool _isUpdating = false;
		public bool IsUpdating {
			get {
				return _isUpdating;
			}
			set {
				if( _isUpdating != value ) {
					_isUpdating = value;
					if( !value ) {
						this.Refresh();
					}
				}
			}
		}

		int _selectedItemIndex = -1;
		public int SelectedItemId {
			get {
				if( _selectedItemIndex >= 0 && _selectedItemIndex < _items.Count ) {
					return _items[_selectedItemIndex].Id;
				} else {
					return -1;
				}
			}
			set {
				_selectedItemIndex = value;
				this.RefreshSelection();
			}
		}

		public int FirstVisibleId {
			get {
				if( _firstFullyVisibleItemIndex != -1 ) {
					return _items[_firstFullyVisibleItemIndex].Id;
				} else {
					return -1;
				}
			}
		}

		public int LastVisibleId {
			get {
				if( _lastFullyVisibleItemIndex != -1 ) {
					return _items[_lastFullyVisibleItemIndex].Id;
				} else {
					return -1;
				}
			}
		}

		public double LineHeight { get; set; }

		#endregion

		#region Public methods
		public void Refresh() {
			this.StartUpdate();
		}

		public void AddItem(ChangeStreamItem item) {
			var text = item.Text;
			if( text.Trim().Length == 0 ) {
				text = text.Replace(' ', '\u00B7');
			}
			text = text.Replace('\n', '\u00b6').Replace('\t', '\u00BB');
			var formattedText = new FormattedText(text, this.Dispatcher.Thread.CurrentUICulture, this.FlowDirection, new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch), this.FontSize, this.Foreground);
			formattedText.MaxTextWidth = ChangeStreamPainter.MaxTextWidth;
			formattedText.MaxTextHeight = this.FontSize * ( 96.0 / 72.0 );
			formattedText.Trimming = TextTrimming.CharacterEllipsis;

			lock( _updateLock ) {
				var rect = this.CalculateItemRectangle(_items.Count, formattedText);
				bool contentChange = false;
				ColorType colorType = ColorType.Normal;
				switch( item.Type ) {
					case ChangeStreamItemType.Delete:
						contentChange = true;
						colorType = ColorType.Delete;
						break;
					case ChangeStreamItemType.Insert:
						contentChange = true;
						colorType = ColorType.Normal;
						break;
					default:
						//by default, do not draw rectangle
						contentChange = false;
						colorType = ColorType.NonContentChange;
						break;
				}
				var visualItem = new VisualItem {
					Rectangle = rect,
					Text = formattedText,
					Id = item.Id,
					ContentChange = contentChange,
					Color = colorType,
				};
				this.ApplyColor(visualItem);
				_items.Add(visualItem);
				if( !this.IsUpdating ) {
					this.UpdateVisibleItems();
					this.InvalidateMeasure();
					this.InvalidateVisual();
					this.SelectedItemId = item.Id;
				}
			}
		}

		public void HandleOnMouseMove(MouseEventArgs e) {
			Point position = e.GetPosition(this);
			if( this.HighlightAdorner.HighlightRect.Contains(position) ) {
				//quit quickly
				return;
			}

			var index = this.FindItemIndexUnderMouse(position);
			if( index != -1 ) {
				lock( _updateLock ) {
					var rect = _items[index].Rectangle;
					this.HighlightAdorner.HighlightRect = rect;
					this.HighlightAdorner.InvalidateVisual();
				}
			} else {
				if( !HighlightAdorner.HighlightRect.IsEmpty ) {
					this.HighlightAdorner.HighlightRect = Rect.Empty;
					this.HighlightAdorner.InvalidateVisual();
				}
			}
		}

		public void HandleOnMouseLeftButtonDown(MouseButtonEventArgs e) {
			var index = this.FindItemIndexUnderMouse(e.GetPosition(this));
			if( index != -1 ) {
				lock( _updateLock ) {
					if( index != _selectedItemIndex ) {
						this.SelectedItemId = _items[index].Id;
						this.SelectedItemChanged.Raise(this, new EventArgs());
					}
				}
			}
		}

		public void UpdateColors(Color foreground, Color background) {
			_foregroundNormal = new SolidColorBrush(foreground);
			Color foregroundDelete = foreground;
			foregroundDelete.R >>= 1;
			foregroundDelete.R += 20;
			foregroundDelete.G >>= 1;
			foregroundDelete.G += 1;
			foregroundDelete.B >>= 1;
			foregroundDelete.B += 1;
			_foregroundDelete = new SolidColorBrush(foregroundDelete);
			Color foregroundNonContent = foreground;
			foregroundNonContent.R >>= 2;
			foregroundNonContent.R += 30;
			foregroundNonContent.G >>= 2;
			foregroundNonContent.G += 30;
			foregroundNonContent.B >>= 2;
			foregroundNonContent.B += 30;
			_foregroundNonContent = new SolidColorBrush(foregroundNonContent);

			_backgroundNormal = new SolidColorBrush(background);
			_backgroundNonContent = new SolidColorBrush(background);
			Color backgroundDelete = background;
			backgroundDelete.R >>= 1;
			backgroundDelete.R += 20;
			backgroundDelete.G >>= 1;
			backgroundDelete.G += 1;
			backgroundDelete.B >>= 1;
			backgroundDelete.B += 1;
			_backgroundDelete = new SolidColorBrush(backgroundDelete);


			foreach( var item in _items ) {
				this.ApplyColor(item);
			}

			this.InvalidateVisual();
		}
		#endregion

		#region Overrides
		protected override void OnRender(DrawingContext drawingContext) {
			base.OnRender(drawingContext);

			if( !this.HasVisibleItems() ) {
				return;
			}

			//draw items
			var linePen = new Pen(_foregroundNonContent, 1);
			linePen.Freeze();
			//Debug.WriteLine("first line: {0} last line: {1}", _firstVisibleItemIndex, _lastVisibleItemIndex);
			for( int i = _firstVisibleItemIndex; i <= _lastVisibleItemIndex; i++ ) {
				var item = _items[i];
				drawingContext.DrawRectangle(this.GetBackgroundBrush(item), null, item.Rectangle);
				drawingContext.DrawLine(linePen, item.Rectangle.TopRight, item.Rectangle.BottomRight);
				drawingContext.DrawLine(linePen, item.Rectangle.BottomLeft, item.Rectangle.BottomRight);
				var textPos = item.Rectangle.TopLeft;
				textPos.Offset(( item.Rectangle.Width - item.Text.Width ) / 2, ( item.Rectangle.Height - item.Text.Height ) / 2);
				drawingContext.DrawText(item.Text, textPos);
			}
		}

		protected override void OnMouseLeave(MouseEventArgs e) {
			base.OnMouseLeave(e);
			if( !this.HighlightAdorner.HighlightRect.IsEmpty ) {
				this.HighlightAdorner.HighlightRect = Rect.Empty;
				this.HighlightAdorner.InvalidateVisual();
			}
		}

		protected override void OnMouseMove(MouseEventArgs e) {
			base.OnMouseMove(e);
			this.HandleOnMouseMove(e);
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e) {
			base.OnMouseLeftButtonDown(e);
			this.HandleOnMouseLeftButtonDown(e);
		}
		#endregion

		#region Private methods
		bool HasVisibleItems() {
			bool refreshing = false;
			lock( _lineRefreshingLock ) {
				refreshing = _refreshingLines;
			}
			return !refreshing && _firstVisibleItemIndex != -1 && _lastVisibleItemIndex != -1;
		}

		void UpdateRectangles(CancellationToken cancellationToken) {
			lock( _lineRefreshingLock ) {
				_refreshingLines = true;
			}
			_lines.Clear();
			_lastItemX = ChangeStreamPainter.DefaultMargin;
			_lastItemY = ChangeStreamPainter.DefaultMargin;
			for( int i = 0; i < _items.Count; i++ ) {
				if( cancellationToken.IsCancellationRequested ) {
					break;
				}
				var item = _items[i];
				item.Rectangle = this.CalculateItemRectangle(i, item.Text);
			}
			lock( _lineRefreshingLock ) {
				_refreshingLines = false;
			}
		}

		void UpdateVisibleItems() {
			_firstVisibleItemIndex = -1;
			_lastVisibleItemIndex = -1;
			if( _lines.Count == 0 ) {
				return;
			}

			double lineHeight = this.LineHeight;
			//get visible lines 
			int firstVisibleLine = (int)( this.VisibleRect.Top / lineHeight );
			int lastVisibleLine = (int)( this.VisibleRect.Bottom / lineHeight );
			//get first item
			if( firstVisibleLine < _lines.Count ) {
				_firstVisibleItemIndex = _lines[Math.Max(0, firstVisibleLine)];
			} else {
				_firstVisibleItemIndex = _lines[_lines.Count - 1];
			}
			//get last item
			if( lastVisibleLine < _lines.Count ) {
				_lastVisibleItemIndex = _lines[Math.Min(_lines.Count - 1, lastVisibleLine + 1)] - 1;
			} else {
				_lastVisibleItemIndex = _items.Count - 1;
			}
			//Debug.WriteLine("Visible lines: {0}-{1}", _firstVisibleItemIndex, _lastVisibleItemIndex);
		}

		void UpdateFullyVisibleItems() {
			_firstFullyVisibleItemIndex = -1;
			_lastFullyVisibleItemIndex = -1;
			if( _lines.Count == 0 ) {
				return;
			}

			double lineHeight = this.LineHeight;
			//get visible lines 
			int firstVisibleLine = (int)( this.VisibleRect.Top / lineHeight );
			int lastVisibleLine = (int)( this.VisibleRect.Bottom / lineHeight );
			// get first item
			if( firstVisibleLine < _lines.Count ) {
				_firstFullyVisibleItemIndex = _lines[(int)firstVisibleLine];
				var rect = _items[_firstFullyVisibleItemIndex].Rectangle;
				rect.Intersect(this.VisibleRect);
				if( Math.Abs(rect.Height - _items[_firstFullyVisibleItemIndex].Rectangle.Height) > 0.001 ) {
					firstVisibleLine = Math.Min(firstVisibleLine + 1, _lines.Count - 1);
					_firstFullyVisibleItemIndex = _lines[firstVisibleLine];
				}
			} else {
				_firstFullyVisibleItemIndex = _items.Count - 1;
			}
			// get last item
			if( lastVisibleLine < _lines.Count ) {
				_lastFullyVisibleItemIndex = _lines[(int)lastVisibleLine];
				var rect = _items[_lastFullyVisibleItemIndex].Rectangle;
				rect.Intersect(this.VisibleRect);
				if( Math.Abs(rect.Height - _items[_lastFullyVisibleItemIndex].Rectangle.Height) > 0.001 ) {
					lastVisibleLine = Math.Max(lastVisibleLine - 1, 0);
					_lastFullyVisibleItemIndex = _lines[lastVisibleLine] - 1;
				}
			} else {
				_lastFullyVisibleItemIndex = _items.Count - 1;
			}
			//ensure it is set to something
			if( _firstFullyVisibleItemIndex == -1 || _lastFullyVisibleItemIndex == -1 ) {
				_firstFullyVisibleItemIndex = _firstVisibleItemIndex;
				_lastFullyVisibleItemIndex = _lastVisibleItemIndex;
			}
			//Debug.WriteLine("Fully visible lines: {0}-{1}", _firstFullyVisibleItemIndex, _lastFullyVisibleItemIndex);
		}

		Rect CalculateItemRectangle(int itemIndex, FormattedText formattedText) {
			if( _lines.Count == 0 ) {
				_lines.Add(0);
			}
			var width = formattedText.Width + 1;
			var height = formattedText.Height + 2;
			this.LineHeight = height;
			width = width < ChangeStreamPainter.MinTextWidth ? ChangeStreamPainter.MinTextWidth : width;
			if( ( _lastItemX + width ) > this.VisibleRect.Width ) {
				_lastItemX = ChangeStreamPainter.DefaultMargin;
				_lastItemY += height;
				_lines.Add(itemIndex);
			}
			var rect = new Rect(_lastItemX, _lastItemY, width, height);
			_lastItemX += rect.Width;
			return rect;
		}

		int FindItemIndexUnderMouse(Point mousePosition) {
			if( !this.HasVisibleItems() ) {
				return -1;
			}

			double lineHeight = this.LineHeight;
			//get visible lines 
			int line = (int)( mousePosition.Y / lineHeight );
			if( line >= 0 && line < _lines.Count ) {
				int first = 0;
				int last = 0;
				if( line < _lines.Count - 1 ) {
					first = _lines[line];
					last = _lines[line + 1];
				} else {
					first = _lines[line];
					last = _items.Count - 1;
				}
				for( int i = first; i <= last; i++ ) {
					var item = _items[i];
					var rect = item.Rectangle;
					if( rect.Contains(mousePosition) ) {
						return i;
					}
				}
			}
			return -1;
		}

		void ApplyColor(VisualItem item) {
			switch( item.Color ) {
				case ColorType.Normal:
					item.Text.SetForegroundBrush(_foregroundNormal);
					break;
				case ColorType.Delete:
					item.Text.SetForegroundBrush(_foregroundDelete);
					break;
				case ColorType.NonContentChange:
					item.Text.SetForegroundBrush(_foregroundNonContent);
					break;
			}
		}

		Brush GetBackgroundBrush(VisualItem item) {
			Brush brush = _backgroundNormal;
			switch( item.Color ) {
				case ColorType.Normal:
					brush = _backgroundNormal;
					break;
				case ColorType.Delete:
					brush = _backgroundDelete;
					break;
				case ColorType.NonContentChange:
					brush = _backgroundNonContent;
					break;
			}
			return brush;
		}

		void RefreshSelection() {
			if( _selectedItemIndex >= 0 && _selectedItemIndex < _items.Count ) {
				//this assumes Id == index, which is true in all cases at this point
				this.HighlightAdorner.SelectRect = _items[_selectedItemIndex].Rectangle;
				this.HighlightAdorner.InvalidateVisual();
			} else {
				this.HighlightAdorner.SelectRect = Rect.Empty;
			}
		}

		void StartUpdate() {
			lock( _updateLock ) {
				if( _updateTask != null ) {
					if( _cancellationTokenSource != null ) {
						_cancellationTokenSource.Cancel();
						if( !_updateTask.IsCanceled ) {
							try {
								_updateTask.Wait();
							} catch( Exception ) {
								//ignore the exception here, we are probraly waiting on already cancelled task
							}
						}
					}
				}

				_cancellationTokenSource = new CancellationTokenSource();
				var cancellationToken = _cancellationTokenSource.Token;
				_updateTask = Task.Factory.StartNew(() => {
					this.UpdateRectangles(cancellationToken);
					if( cancellationToken.IsCancellationRequested ) {
						Debug.WriteLine("canceled");
						return;
					}

					// do not enable this, is causes freezes for some unknown reason...
					this.UpdateVisibleItems();
					this.UpdateFullyVisibleItems();
					this.ExecuteOnUiThreadAsync(() => {
						this.InvalidateMeasure();
						this.InvalidateVisual();
						this.RefreshSelection();
					});
				}, cancellationToken);
			}
		}

		protected override Size MeasureOverride(Size constraint) {
			var size = constraint;
			lock( _updateLock ) {
				if( _items.Count > 0 ) {
					size.Height = _items[_items.Count - 1].Rectangle.Bottom;
				} else {
					size.Height = 0;
				}
			}
			return size;
		}
		#endregion
	}
}
