﻿using System;
using System.Windows.Input;
using System.Windows.Controls;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class Document {
		#region Events
		public event EventHandler NewBranchRequest;
		public event EventHandler EmptyBranchRequest;
		public event EventHandler ActiveBranchChanged;
		public event EventHandler RefocusNeeded;
		#endregion

		#region NewBranch command
		static RoutedCommand _newBranch = new RoutedCommand("NewBranch", typeof(Document));
		public static RoutedCommand NewBranch { get { return _newBranch; } }

		public void CanExecuteNewBranchCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		public void ExecuteNewBranchCommand(object sender, ExecutedRoutedEventArgs e) {
			var document = sender as Document;
			if( document.NewBranchRequest != null ) {
				document.NewBranchRequest(document, new EventArgs());
			}
		}
		#endregion

		#region EmptyBranch command
		static RoutedCommand _emptyBranch = new RoutedCommand("EmptyBranch", typeof(Document));
		public static RoutedCommand EmptyBranch { get { return _emptyBranch; } }

		public void CanExecuteEmptyBranchCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		public void ExecuteEmptyBranchCommand(object sender, ExecutedRoutedEventArgs e) {
			var document = sender as Document;
			if( document.EmptyBranchRequest != null ) {
				document.EmptyBranchRequest(document, new EventArgs());
			}
		}
		#endregion

		#region ctors
		static Document() {
			NewBranch.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control | ModifierKeys.Shift));
			EmptyBranch.InputGestures.Add(new KeyGesture(Key.E, ModifierKeys.Control | ModifierKeys.Shift));
		}
		#endregion

		#region ICanBeBusy Members
		public void SetBusy() {
			this.SetComponentsEnabled(false);
		}

		public void SetNotBusy() {
			this.SetComponentsEnabled(true);
		}
		#endregion

		#region Private methods
		private void branchContainer_TabStripClicked(object sender, EventArgs e) {
			this.RefocusNeeded.Raise(this, new EventArgs());
		}

		void branchContainer_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.ActiveBranchChanged.Raise(this, new EventArgs());
			e.Handled = true;
		}

		void SetComponentsEnabled(bool enabled) {
			this.branchContainer.IsEnabled = enabled;
			this.IsEnabled = enabled;
		}
		#endregion
	}
}
