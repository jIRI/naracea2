﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.Controls {
	/// <summary>
	/// Interaction logic for ChangeStream.xaml
	/// </summary>
	public partial class ChangeStream : UserControl {
		public ChangeStream() {
			InitializeComponent();
			//add highlighting adorner to painter
			var layer = System.Windows.Documents.AdornerLayer.GetAdornerLayer(this.painter);
			layer.Add(new HighlightAdorner(this.painter));
			//register scrollviewr events
			this.scrollViewer.ScrollChanged += (s, e) => {
				this.painter.VisibleRect = new Rect(e.HorizontalOffset, e.VerticalOffset, e.ViewportWidth, e.ViewportHeight);
			};
			this.SizeChanged += (s, e) => {
				this.painter.VisibleRect = new Rect(this.scrollViewer.HorizontalOffset, this.scrollViewer.VerticalOffset, this.scrollViewer.ViewportWidth, this.scrollViewer.ViewportHeight);
			};
		}
	}
}
