﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Diagnostics;
using Naracea.Common.Extensions;
using Naracea.Core.Ui.Wpf.View.Extensions;
using System.Windows.Input;
using System.Windows;
using System.Runtime.InteropServices;
using Naracea.Common;
using System.Windows.Media;
using System.Windows.Documents;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Editing;
using System.Threading.Tasks;
using ICSharpCode.AvalonEdit.Document;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class TextEditor : ICSharpCode.AvalonEdit.TextEditor {
		#region Events
		public event EventHandler<RawTextChangedArgs> RawTextChanged;
		public event EventHandler<TextChangeArgs> TextAdded;
		public event EventHandler<TextChangeArgs> TextDeleted;
		public event EventHandler<TextChangeArgs> TextBackspaced;
		public event EventHandler<ChangeArgs> ChangeUndone;
		public event EventHandler<ChangeArgs> ChangeRedone;
		public event EventHandler UndoWordRequest;
		public event EventHandler UndoSentenceRequest;
		public event EventHandler PasteCharsAsChangesRequest;
		public event EventHandler PropertyChanged;
		public event EventHandler GroupOperationBegin;
		public event EventHandler GroupOperationEnd;
		public event EventHandler<SuggestSpellingArgs> SuggestSpelling;
		public event EventHandler<SuggestSpellingArgs> AddToSpellingDictionaryRequest;
		#endregion

		#region Properties
		bool IsSilent { get; set; }
		SimpleSelection PreChangeSelection { get; set; }
		#endregion

		#region Attrbitues
		List<MenuItem> _defaultMenuItems = new List<MenuItem>();
		List<Control> _spellcheckerMenuItems = new List<Control>();
		SpellingErrorRange _currentSpellingError = null;
		#endregion

		#region ctor
		public TextEditor() {
			this.Name = "textEditor2";
			this.SnapsToDevicePixels = true;
			this.Padding = new System.Windows.Thickness(4.0);
			this.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
			this.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
			this.WordWrap = true;
			this.Options.ShowEndOfLine = true;
			this.Options.ShowTabs = true;
			this.Options.ShowSpaces = true;
			this.Options.IndentationSize = 2;

			this.Document.UndoStack.SizeLimit = 0;
			this.Document.Changing += this.Document_Changing;
			this.Document.Changed += this.Document_Changed;
			this.Document.NewLine = Constants.NewLine;
			this.Document.GroupOperationBegin += this.Document_GroupOperationBegin;
			this.Document.GroupOperationEnd += this.Document_GroupOperationEnd;
			this.TextArea.Caret.PositionChanged += this.Caret_PositionChanged;
			this.TextArea.MouseRightButtonDown += this.TextArea_MouseRightButtonDown;
			this.Options.InheritWordWrapIndentation = false;

			var contextMenu = new ContextMenu();
			this.ContextMenu = contextMenu;
			this.ContextMenuOpening += new ContextMenuEventHandler(TextEditor_ContextMenuOpening);
			_defaultMenuItems.Add(new MenuItem {
				Command = ApplicationCommands.Copy,
			});
			_defaultMenuItems.Add(new MenuItem {
				Command = ApplicationCommands.Cut
			});
			_defaultMenuItems.Add(new MenuItem {
				Command = ApplicationCommands.Paste
			});
		}
		#endregion

		#region Public methods
		public void InsertTextWithChange(int pos, string text) {
			//insert text
			var normalizedText = text.NormalizeEols();
			this.Document.Insert(pos, normalizedText);
		}

		public void InsertTextWithChange(string text) {
			//insert text
			var normalizedText = text.NormalizeEols();
			this.Document.Insert(this.CaretOffset, normalizedText);
		}

		public void DeleteTextWithChange(int pos, int length) {
			this.Document.Remove(pos, length);
			//adjust cursor position
			this.CaretOffset = pos;
		}

		public void DeleteTextWithChange(int length) {
			this.Document.Remove(this.CaretOffset, length);
		}

		public void InsertTextBeforePosition(int pos, string text) {
			this.IsSilent = true;
			//here we add text
			this.Document.Insert(pos, text);
			//now set cursor position
			this.CaretOffset = pos + text.Length;
			this.IsSilent = false;
		}

		public void InsertTextBehindPosition(int pos, string text) {
			this.IsSilent = true;
			//insert text
			this.Document.Insert(pos, text);
			//adjust cursor position
			this.CaretOffset = pos;
			this.IsSilent = false;
		}

		public void DeleteText(int pos, string text) {
			this.IsSilent = true;
			this.Document.Remove(pos, text.Length);
			//adjust cursor position
			this.CaretOffset = pos;
			this.IsSilent = false;
		}

		public void BackspaceText(int pos, string text) {
			this.IsSilent = true;
			this.Document.Remove(pos, text.Length);
			//adjust cursor position
			this.CaretOffset = pos;
			this.IsSilent = false;
		}

		public void SetTextSilently(string text) {
			this.IsSilent = true;
			this.Text = text;
			this.IsSilent = false;
		}

		public void SetSpellingSuggestions(IEnumerable<string> suggestions) {
			this.ExecuteOnUiThreadAsync(() => {
				this.ContextMenu.BeginInit();
				if( suggestions.Any() ) {
					_spellcheckerMenuItems = suggestions.Select(i => new MenuItem {
						Header = i,
						Command = TextEditor.SelectSpellingSuggestion,
						CommandParameter = i
					}).ToList<Control>();
				} else {
					_spellcheckerMenuItems = new List<Control>();
					_spellcheckerMenuItems.Add(new MenuItem {
                        Header = Naracea.Core.Properties.Resources.NoSuggestions,
						IsEnabled = false
					});
				}
				_spellcheckerMenuItems.Add(new Separator());
				_spellcheckerMenuItems.Add(new MenuItem {
                    Header = Naracea.Core.Properties.Resources.AddToDictionary,
					Command = TextEditor.AddToDictionary
				});
				_spellcheckerMenuItems.Add(new Separator());

				int pos = 0;
				foreach( var item in _spellcheckerMenuItems ) {
					this.ContextMenu.Items.Insert(pos++, item);
				}
				this.ContextMenu.EndInit();
				CommandManager.InvalidateRequerySuggested();
			});
		}
		#endregion

		#region Overrides
		void TextEditor_ContextMenuOpening(object sender, ContextMenuEventArgs e) {
			this.ContextMenu.Items.Clear();
			foreach( var item in _defaultMenuItems ) {
				this.ContextMenu.Items.Add(item);
			}
			int caretPos = this.TextArea.Caret.Offset;
			var spellingErrorQuery = from err in this.Document.SpellingErrors
															 where caretPos >= err.Offset && caretPos <= ( err.Offset + err.Length )
															 select err;
			_currentSpellingError = spellingErrorQuery.FirstOrDefault();
			if( _currentSpellingError != null ) {
				_spellcheckerMenuItems = null;
				this.SuggestSpelling.Raise(this, new SuggestSpellingArgs(_currentSpellingError.Offset, _currentSpellingError.Length));
			}
		}

		void TextArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e) {
			var position = this.GetPositionFromPoint(e.GetPosition(this));
			if( position.HasValue ) {
				this.TextArea.Caret.Position = position.Value;
			}
		}

		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
			base.OnRenderSizeChanged(sizeInfo);
			this.PropertyChanged.Raise(this, new EventArgs());
		}

		void Caret_PositionChanged(object sender, EventArgs e) {
			this.PropertyChanged.Raise(this, new EventArgs());
		}

		void Document_GroupOperationBegin(object sender, EventArgs e) {
			if( !this.IsSilent ) {
				this.GroupOperationBegin.Raise(this, new EventArgs());
			}
		}

		void Document_GroupOperationEnd(object sender, EventArgs e) {
			if( !this.IsSilent ) {
				this.GroupOperationEnd.Raise(this, new EventArgs());
			}
		}

		void Document_Changing(object sender, ICSharpCode.AvalonEdit.Document.DocumentChangeEventArgs e) {
			if( this.IsSilent ) {
				return;
			}

			if( this.TextArea.Selection is SimpleSelection ) {
				this.PreChangeSelection = this.TextArea.Selection as SimpleSelection;
			} else {
				this.PreChangeSelection = null;
			}
		}

		void Document_Changed(object sender, ICSharpCode.AvalonEdit.Document.DocumentChangeEventArgs e) {
			if( !this.IsSilent ) {

				bool replacement = e.RemovalLength > 0 && e.InsertionLength > 0;
				if( replacement ) {
					this.GroupOperationBegin.Raise(this, new EventArgs());
				}
				if( e.RemovalLength > 0 ) {
					if( e.InsertionLength == 0 && this.PreChangeSelection != null ) {
						if( this.PreChangeSelection.StartPosition.CompareTo(this.PreChangeSelection.EndPosition) < 0 ) {
							this.TextDeleted.Raise(this, new TextChangeArgs(e.Offset, e.RemovedText.Text));
						} else {
							this.TextBackspaced.Raise(this, new TextChangeArgs(e.Offset, e.RemovedText.Text));
						}
					} else {
						this.TextDeleted.Raise(this, new TextChangeArgs(e.Offset, e.RemovedText.Text));
					}
				}
				if( e.InsertionLength > 0 ) {
					this.TextAdded.Raise(this, new TextChangeArgs(e.Offset, e.InsertedText.Text));
				}
				if( replacement ) {
					this.GroupOperationEnd.Raise(this, new EventArgs());
				}
				this.PropertyChanged.Raise(this, new EventArgs());
			}

			this.RawTextChanged.Raise(this, new RawTextChangedArgs(e.Offset, e.InsertionLength, e.RemovalLength));
		}
		#endregion

		#region Flags
		public bool CanEdit {
			get {
				var result = false;
				this.ExecuteOnUiThread(() => result = !this.IsReadOnly);
				return result;
			}
			set {
				this.ExecuteOnUiThread(() => this.IsReadOnly = !value);
			}
		}

		public new bool CanUndo { get; set; }
		public new bool CanRedo { get; set; }
		#endregion

		#region Undo command
		private static void CanExecuteUndoCommand(object sender, CanExecuteRoutedEventArgs e) {
			var editor = sender as TextEditor;
			e.CanExecute = editor.CanUndo;
			e.Handled = true;
		}

		private static void ExecuteUndoCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			if( editor.ChangeUndone != null ) {
				editor.IsSilent = true;
				editor.ChangeUndone(editor, new ChangeArgs());
				editor.IsSilent = false;
			}
		}

		static RoutedCommand _undoWord = new RoutedCommand("undoWord", typeof(TextEditor));
		public static RoutedCommand UndoWord { get { return _undoWord; } }
		public static void ExecuteUndoWordCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			editor.UndoWordRequest.Raise(editor, new EventArgs());
		}

		static RoutedCommand _undoSentence = new RoutedCommand("undoSentence", typeof(TextEditor));
		public static RoutedCommand UndoSentence { get { return _undoSentence; } }
		public static void ExecuteUndoSentenceCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			editor.UndoSentenceRequest.Raise(editor, new EventArgs());
		}
		#endregion

		#region Redo command
		private static void CanExecuteRedoCommand(object sender, CanExecuteRoutedEventArgs e) {
			var editor = sender as TextEditor;
			e.CanExecute = editor.CanRedo;
			e.Handled = true;
		}

		private static void ExecuteRedoCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			if( editor.ChangeRedone != null ) {
				editor.IsSilent = true;
				editor.ChangeRedone(editor, new ChangeArgs());
				editor.IsSilent = false;
			}
		}
		#endregion

		#region Paste special commands
		public static void CanExecutePasteCharsAsChangesCommand(object sender, CanExecuteRoutedEventArgs e) {
			var editor = sender as TextEditor;
			try {
				e.CanExecute = editor.CanEdit && Clipboard.ContainsText() && Clipboard.GetText().Length > 0;
			} catch( COMException ) {
				//well, here we get com exception from opening clipboard from time to time
				//there is hardly anything we can do about that...
			}
			e.Handled = true;
		}

		static RoutedCommand _pasteCharsAsChanges = new RoutedCommand("pasteCharsAsChanges", typeof(TextEditor));
		public static RoutedCommand PasteCharsAsChanges { get { return _pasteCharsAsChanges; } }
		public static void ExecutePasteCharsAsChangesCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			editor.PasteCharsAsChangesRequest.Raise(editor, new EventArgs());
		}
		#endregion

		#region Select spelling suggestion
		public static void CanExecuteSelectSpellingSuggestionCommand(object sender, CanExecuteRoutedEventArgs e) {
			var editor = sender as TextEditor;
			e.CanExecute = editor.CanEdit && editor._currentSpellingError != null;
			e.Handled = true;
		}

		static RoutedCommand _selectSpellingSuggestion = new RoutedCommand("selectSpellingSuggestion", typeof(TextEditor));
		public static RoutedCommand SelectSpellingSuggestion { get { return _selectSpellingSuggestion; } }
		public static void ExecuteSelectSpellingSuggestionCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			editor.Select(editor._currentSpellingError.Offset, editor._currentSpellingError.Length);
			var suggestedWord = e.Parameter.As<string>();
			editor.SelectedText = suggestedWord;
			editor._spellcheckerMenuItems = null;
			editor._currentSpellingError = null;
		}
		#endregion

		#region Add to spelling dictionary
		public static void CanExecuteAddToDictionaryCommand(object sender, CanExecuteRoutedEventArgs e) {
			var editor = sender as TextEditor;
			e.CanExecute = editor.CanEdit && editor._currentSpellingError != null;
			e.Handled = true;
		}

		static RoutedCommand _addToDictionary = new RoutedCommand("addToDictionary", typeof(TextEditor));
		public static RoutedCommand AddToDictionary { get { return _addToDictionary; } }
		public static void ExecuteAddToDictionaryCommand(object sender, ExecutedRoutedEventArgs e) {
			var editor = sender as TextEditor;
			editor.AddToSpellingDictionaryRequest.Raise(editor, new SuggestSpellingArgs(editor._currentSpellingError.Offset, editor._currentSpellingError.Length));
			editor._spellcheckerMenuItems = null;
			editor._currentSpellingError = null;
		}
		#endregion

		#region ctors
		static TextEditor() {
			//we do this because these are default framework commands and we need to change their behavior and availability
			//and since this control doesn't have XAML, wee ned to do it in code
			var commandMappings = new ControlMapping[] {
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(System.Windows.Input.ApplicationCommands.Undo, ExecuteUndoCommand, CanExecuteUndoCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(System.Windows.Input.ApplicationCommands.Redo, ExecuteRedoCommand, CanExecuteRedoCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(UndoWord, ExecuteUndoWordCommand, CanExecuteUndoCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(UndoSentence, ExecuteUndoSentenceCommand, CanExecuteUndoCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(PasteCharsAsChanges, ExecutePasteCharsAsChangesCommand, CanExecutePasteCharsAsChangesCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(SelectSpellingSuggestion, ExecuteSelectSpellingSuggestionCommand, CanExecuteSelectSpellingSuggestionCommand),
				},
				new ControlMapping {
					Type = typeof(TextEditor),
					Binding = new CommandBinding(AddToDictionary, ExecuteAddToDictionaryCommand, CanExecuteAddToDictionaryCommand),
				},
			};
			//update system commands
			ControlMapping.Register(commandMappings);

			//map special undo hotkeys
			UndoWord.InputGestures.Add(new KeyGesture(Key.Z, ModifierKeys.Alt));
			UndoSentence.InputGestures.Add(new KeyGesture(Key.Z, ModifierKeys.Alt | ModifierKeys.Shift));
			PasteCharsAsChanges.InputGestures.Add(new KeyGesture(Key.V, ModifierKeys.Control | ModifierKeys.Shift));
		}
		#endregion
	}
}
