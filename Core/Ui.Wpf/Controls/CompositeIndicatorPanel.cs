﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Naracea.Core.Ui.Wpf.Controls {
	public class CompositeIndicatorPanel : StatusBarItem {
		StackPanel _indicatorHolder;

		public CompositeIndicatorPanel() {
			//Grid.SetRow(this, 0);
			//Grid.SetColumn(this, 0);
			this.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
			_indicatorHolder = new StackPanel();
			_indicatorHolder.Orientation = Orientation.Horizontal;
			this.AddChild(_indicatorHolder);
		}

		public void Add(Control ctrl) {
			_indicatorHolder.Children.Add(ctrl);
		}

		public void Remove(Control ctrl) {
			_indicatorHolder.Children.Remove(ctrl);
		}
	}
}
