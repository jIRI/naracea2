﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Naracea.Core.Ui.Wpf.Interfaces;

namespace Naracea.Core.Ui.Wpf.Controls {
	/// <summary>
	/// Interaction logic for Document.xaml
	/// </summary>
	public partial class Document : UserControl, ICanBeBusy  {
		public Document() {
			InitializeComponent();
		}
	}
}
