﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class DocumentTabHeader {
		bool _isDirty = false;
		public bool IsDirty {
			get {
				return _isDirty;
			}
			set {
				_isDirty = value;
				this.dirtyImage.Visibility = _isDirty ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
			}
		}

		public string Text {
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TextProperty =
				DependencyProperty.Register("Text", typeof(string), typeof(DocumentTabHeader), new UIPropertyMetadata(null, OnTextChanged));

		static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var element = d as DocumentTabHeader;		
			element.header.Text = e.NewValue as string;
		}		

		#region ICloneable Members
		public object Clone() {
			var clone = new DocumentTabHeader();
			clone.IsDirty = this.IsDirty;
			clone.header.Text = this.header.Text;
			return clone;
		}
		#endregion

	}
}
