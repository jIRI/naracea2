﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Diagnostics;
using Naracea.Common.Extensions;
using System.Windows.Data;
using System.Globalization;
using Naracea.Common;
using System.Windows.Threading;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class Timeline {
		#region Attributes
		public IList<TimelineBarControl> Bars { get; private set; }
		int _currentFirstVisibleBar = 0;
		int _currentLastVisibleBar = 0;
		bool _ignoreNextScroll = false;
		Rectangle _marker;
		double _scale = 1.0;
		bool _autoScalingEnabled = true;
		int _markerBarIndex;
		int _markerIdIndex;
		bool _scrollPending = false;
		DispatcherTimer _repaintTimer = new DispatcherTimer();
		#endregion

		#region Events
		public class BarSelectedEventArgs : EventArgs {
			public int BarIndex { get; private set; }
			public int InBarPosition { get; private set; }
			public BarSelectedEventArgs(int barIndex, int inBarPos) {
				this.BarIndex = barIndex;
				this.InBarPosition = inBarPos;
			}
		}

		public event EventHandler<BarSelectedEventArgs> TimelineBarSelected;
		#endregion

		#region ctors
		public Timeline(IList<TimelineBarControl> bars)
			: this() {
			this.Bars = bars;
			this.scrollViewer.ScrollChanged += this.ScrollViewer_ScrollChanged;
			this.ConfigureMarker();
			this.barHolder.Children.Add(_marker);
			this.scrollViewer.AddHandler(UIElement.MouseLeftButtonDownEvent, (RoutedEventHandler)this.TimelineMouseLeftButtonDownEventHandler, true);
			_repaintTimer.Interval = new TimeSpan(50 * 10000); //50ms
			_repaintTimer.Tick += (s, e) => this.Refresh();
		}
		#endregion

		#region Public methods
		public void SetBusy() {
			this.IsEnabled = false;
		}

		public void SetNotBusy() {
			this.IsEnabled = true;
		}

		public void SetMarker(int bar, int index) {
			_markerBarIndex = bar;
			_markerIdIndex = index;
			Canvas.SetLeft(_marker, this.BarPositionX(_markerBarIndex));
			Canvas.SetBottom(_marker, this.ScaledHeight((int)( _markerIdIndex - _marker.Height / 2 )));
			_marker.Visibility = System.Windows.Visibility.Visible;
			Canvas.SetZIndex(_marker, int.MaxValue);
		}

		public void EnsureMarkerVisible() {
			var markerXPosition = this.BarPositionX(_markerBarIndex);
			var horizOffset = this.scrollViewer.HorizontalOffset;
			if( horizOffset > markerXPosition || horizOffset + this.scrollViewer.ViewportWidth < markerXPosition ) {
				_scrollPending = true;
				this.scrollViewer.ScrollToHorizontalOffset(markerXPosition);
			}
		}

		public void Refresh() {
			_repaintTimer.Stop();
			this.ConfigureMarker();
			this.barHolder.Children.Clear();
			this.barHolder.Children.Add(_marker);
			this.UpdateBarHolderWidth();
			if( _scrollPending ) {
				return;
			}
			if( this.Bars.Count > 0 ) {
				this.UpdateCurrentlyVisibleBars();
				this.UpdateScaling();
				for( int i = _currentFirstVisibleBar; i < _currentLastVisibleBar && i < this.Bars.Count; i++ ) {
					var bar = this.Bars[i];
					this.AddBarToTimeline(i, bar);
				}
				this.SetMarker(_markerBarIndex, _markerIdIndex);
			} else {
				this.barHolder.Width = 0;
				this.SetMarker(0, 0);
			}
		}

		private void UpdateBarHolderWidth() {
			var totalWidth = this.Bars.Sum(b => b.Bar.ColumnSpan * TimelineBarControl.DefaultWidth);
			this.barHolder.Width = totalWidth;
		}

		public void BarAdded() {
			if( this.UpdateScaling() || this.barHolder.Children.Count == 1 ) {
				this.barHolder.Width = 0;
				this.Refresh();
				return;
			}

			var newBarIndex = this.Bars.Count - 1;
			var start = this.GetFirstVisibleVisualBar();
			var end = this.GetLastVisibleVisualBar(start);
			if( end < newBarIndex ) {
				this.Refresh();
				this.EnsureMarkerVisible();
				return;
			}

			this.RemoveInvisibleBars();
			this.AddBarToTimeline(newBarIndex, this.Bars[newBarIndex]);
			_ignoreNextScroll = true;
			this.UpdateCurrentlyVisibleBars();
			this.SetMarker(this.Bars.Count - 1, this.Bars[newBarIndex].Bar.Ids.Count - 1);
			this.EnsureMarkerVisible();
		}

		public void LastBarUpdated() {
			if( this.barHolder.Children.Count == 1 || this.UpdateScaling() ) {
				this.Refresh();
				return;
			}

			var lastBar = this.GetBarAtIndex(this.barHolder.Children.Count - 1).barRect;
			var height = this.Bars[this.Bars.Count - 1].Bar.Ids.Count - 1;
			if( height >= 0 && (int)lastBar.Tag == ( this.Bars.Count - 1 ) ) {
				lastBar.Height = this.ScaledHeight(height);
			} else {
				this.Refresh();
			}
		}
		#endregion

		#region Event handlers
		void TimelineMouseLeftButtonDownEventHandler(object sender, RoutedEventArgs e) {
			var args = e as System.Windows.Input.MouseButtonEventArgs;
			if( args == null ) {
				return;
			}

			var position = args.GetPosition(this.barHolder);
			if( position.Y > this.scrollViewer.ViewportHeight ) {
				return;
			}

			int selectedBarIndex = -1;
			int inBarPosition = -1;
			var visualBarCount = this.barHolder.Children.Count;
			for( int i = 1; i < visualBarCount; i++ ) {
				var bar = this.GetBarAtIndex(i);
				var left = Canvas.GetLeft(bar);
				if( position.X >= left && position.X <= ( left + bar.Width ) ) {
					selectedBarIndex = (int)bar.Tag;
					var posY = this.scrollViewer.ViewportHeight - position.Y;
					if( posY < 10 && ( Canvas.GetBottom(_marker) > posY || _markerBarIndex != selectedBarIndex ) ) {
						//when user click very close to zero, pad it to zero...
						posY = 0;
					}
					inBarPosition = this.UnscaleHeight((int)Math.Min(posY, bar.barRect.Height - 1));
					inBarPosition = Math.Max(0, inBarPosition);
					break;
				}
			}

			if( selectedBarIndex == -1 ) {
				selectedBarIndex = this.Bars.Count - 1;
			}
			if( inBarPosition == -1 && selectedBarIndex >= 0 ) {
				inBarPosition = this.Bars[selectedBarIndex].Bar.Ids.Count - 1;
			}

			this.TimelineBarSelected.Raise(this, new BarSelectedEventArgs(selectedBarIndex, inBarPosition));
		}

		void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e) {
			if( _scrollPending || !_ignoreNextScroll ) {
				_scrollPending = false;
				_repaintTimer.Stop();
				_repaintTimer.Start();
			}
			_ignoreNextScroll = false;
			e.Handled = true;
		}
		#endregion

		#region Private methods
		private bool UpdateScaling() {
			if( !_autoScalingEnabled ) {
				return false;
			}

			var lastScale = _scale;
			_scale = 1.0;
			int max = 1;
			for( int i = _currentFirstVisibleBar; i < _currentLastVisibleBar && i < this.Bars.Count; i++ ) {
				var bar = this.Bars[i];
				if( bar.Bar.Ids.Count > max ) {
					max = bar.Bar.Ids.Count;
				}
			}
			if( this.scrollViewer.ViewportHeight != 0 ) {
				_scale = Math.Ceiling(max / ( 0.9 * this.scrollViewer.ViewportHeight ));
			}
			return lastScale != _scale;
		}

		int ScaledHeight(int height) {
			var scaledHeight = (int)( height / _scale );
			if( scaledHeight == 0 && height > 0 ) {
				scaledHeight = 1;
			}
			return scaledHeight;
		}

		int UnscaleHeight(int height) {
			return (int)( height * _scale );
		}

		private void RemoveInvisibleBars() {
			var barCapacity = this.scrollViewer.ViewportWidth / TimelineBarControl.DefaultWidth;
			for( int i = 1; i < ( this.barHolder.Children.Count - barCapacity ); i++ ) {
				this.barHolder.Children.RemoveAt(1);
			}
		}

		int GetBarIndexFromVisualIndex(int visualBarIndex) {
			return (int)this.GetBarAtIndex(visualBarIndex).Tag;
		}

		private TimelineBarControl GetBarAtIndex(int visualBarIndex) {
			return this.barHolder.Children[visualBarIndex] as TimelineBarControl;
		}

		int GetLastVisibleVisualBar(int start) {
			return start + (int)( this.scrollViewer.ViewportWidth / TimelineBarControl.DefaultWidth ) + 1;
		}

		int GetFirstVisibleVisualBar() {
			return (int)( this.scrollViewer.HorizontalOffset / TimelineBarControl.DefaultWidth );
		}

		int BarIndexFromVisulaBarIndex(int visualBarIndex) {
			int barIndex = 0;
			int currentVisualBarIndex = 0;
			while( barIndex < this.Bars.Count ) {
				currentVisualBarIndex += this.Bars[barIndex].Bar.ColumnSpan;
				if( currentVisualBarIndex >= visualBarIndex ) {
					break;
				}
				barIndex++;
			}
			return barIndex;
		}

		void AddBarToTimeline(int index, TimelineBarControl bar) {
			this.UpdateBarHolderWidth();
			var barControl = new TimelineBarControl();
			var currentWidth = bar.Bar.ColumnSpan * TimelineBarControl.DefaultWidth;
			barControl.Width = currentWidth;
			barControl.background.Width = currentWidth;
			barControl.background.Height = this.scrollViewer.ViewportHeight;
			barControl.background.Fill = TimelineBarControl.GridBrush;
			barControl.gridLine.Stroke = TimelineBarControl.GridStroke;
			barControl.gridLine.X1 = 0;
			barControl.gridLine.Y1 = 0;
			barControl.gridLine.X2 = 0;
			barControl.gridLine.Y2 = this.scrollViewer.ViewportHeight;
			barControl.barRect.Width = currentWidth;
			barControl.barRect.Height = this.ScaledHeight(bar.Bar.Ids.Count);
			barControl.barRect.Fill = TimelineBarControl.Brush;
			barControl.barRect.Stroke = TimelineBarControl.Stroke;
			barControl.barRect.Tag = index;
			barControl.Tag = index;
			var toolTip = new TextBlock();
			toolTip.Text = string.Format("{0} - {1} ({2})", bar.Bar.From.LocalDateTime, bar.Bar.To.LocalDateTime, bar.Bar.Ids.Count);
			barControl.ToolTip = toolTip;
			Canvas.SetLeft(barControl, this.BarPositionX(index));
			Canvas.SetBottom(barControl, 0);
			this.barHolder.Children.Add(barControl);
		}

		double BarPositionX(int index) {
			var barX = 0.0;
			for( int i = 0; i < index; i++ ) {
				barX += this.Bars[i].Bar.ColumnSpan * TimelineBarControl.DefaultWidth;
			}
			return barX;
		}

		void UpdateCurrentlyVisibleBars() {
			var firstVisualBar = this.GetFirstVisibleVisualBar();
			_currentFirstVisibleBar = this.BarIndexFromVisulaBarIndex(firstVisualBar);
			_currentLastVisibleBar = this.BarIndexFromVisulaBarIndex(this.GetLastVisibleVisualBar(firstVisualBar)) + 1;
		}

		void ConfigureMarker() {
			_marker = new Rectangle();
			_marker.Fill = TimelineBarControl.MarkerBrush;
			_marker.Stroke = TimelineBarControl.MarkerStrokeBrush;
			_marker.Width = TimelineBarControl.DefaultWidth;
			_marker.Height = TimelineBarControl.DefaultWidth / 2;
			_marker.Visibility = System.Windows.Visibility.Collapsed;
		}
		#endregion
	}
}
