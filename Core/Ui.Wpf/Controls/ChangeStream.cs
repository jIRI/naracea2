﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Controls;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class ChangeStream {
		#region Attributes
		#endregion

		#region Public methods
		public void AddItem(ChangeStreamItem item) {
			this.painter.AddItem(item);
			if( !this.IsUpdating ) {
				this.scrollViewer.ScrollToBottom();
			}
		}

		public int SelectedItemId {
			get {
				return this.painter.SelectedItemId;
			}
			set {
				bool toBottom = this.painter.SelectedItemId > value;
				this.painter.SelectedItemId = value;
				if( value == -1 ) {
					this.scrollViewer.ScrollToTop();
				} else if( value < this.painter.FirstVisibleId || value > this.painter.LastVisibleId ) {
					if( toBottom ) {
						this.scrollViewer.ScrollToVerticalOffset(Math.Max(0, this.painter.HighlightAdorner.SelectRect.Bottom - this.painter.VisibleRect.Height));
					} else {
						this.scrollViewer.ScrollToVerticalOffset(this.painter.HighlightAdorner.SelectRect.Top);
					}
				}
			}
		}

		bool _isUpdating = false;
		public bool IsUpdating {
			get {
				return _isUpdating;
			}
			set {
				_isUpdating = value;
				this.painter.IsUpdating = _isUpdating;
			}
		}

		public void UpdateColors(Color foreground, Color background) {
			this.painter.UpdateColors(foreground, background);
		}

		public void SetBusy() {
			this.IsEnabled = false;
		}

		public void SetNotBusy() {
			this.IsEnabled = true;
		}

		public void Refresh() {
			this.painter.Refresh();
			this.scrollViewer.ScrollToVerticalOffset(this.painter.HighlightAdorner.SelectRect.Top);
		}
		#endregion
	}
}
