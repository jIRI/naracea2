﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.Controls {
	public class GoToDateAndTimeEventArgs : EventArgs {
		public DateTime When { get; private set; }

		public GoToDateAndTimeEventArgs(DateTime dateTime) {
			this.When = dateTime;
		}
	}

	public class GoToChangeEventArgs : EventArgs {
		public int ChangeId { get; private set; }
		public GoToChangeEventArgs(int id) {
			this.ChangeId = id;
		}
	}
}
