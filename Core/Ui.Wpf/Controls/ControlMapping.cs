﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

namespace Naracea.Core.Ui.Wpf.Controls {
	internal class ControlMapping {
		public CommandBinding Binding { get; set; }
		public KeyGesture KeyGesture { get; set; }
		public Type Type { get; set; }

		public static void Register(IEnumerable<ControlMapping> mappings) {
			foreach( var mapping in mappings ) {
				CommandManager.RegisterClassCommandBinding(mapping.Type, mapping.Binding);
				if( mapping.KeyGesture != null ) {
					CommandManager.RegisterClassInputBinding(mapping.Type, new InputBinding(mapping.Binding.Command, mapping.KeyGesture));
				}
			}
		}
	}
}
