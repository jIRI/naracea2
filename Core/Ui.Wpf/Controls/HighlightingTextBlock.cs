﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows;
using Naracea.Core.Ui.Wpf.View.DataModel;

namespace Naracea.Core.Ui.Wpf.Controls {
	public class HighlightingTextBlock : TextBlock {
		public SearchMatchItem Match {
			get { return (SearchMatchItem)GetValue(MatchProperty); }
			set { SetValue(MatchProperty, value); }
		}

		// Using a DependencyProperty as the backing store for PlainText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MatchProperty =
				DependencyProperty.Register("Match", typeof(SearchMatchItem), typeof(HighlightingTextBlock), new UIPropertyMetadata(null, OnMatchPropertyChanged));

		private static void OnMatchPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e) {
			var control = source as HighlightingTextBlock;
			control.UpdateHighlight();
		}

		public HighlightingTextBlock()
			: base() {
		}

		private void UpdateHighlight() {
			if( string.IsNullOrEmpty(this.Match.SurroundedMatchText) ) {
				return;
			}

			this.Inlines.Clear();
			this.Inlines.Add(this.Match.SurroundedMatchText.Substring(0, this.Match.MatchOffsetInText));
			var bold = new System.Windows.Documents.Bold();
			bold.Inlines.Add(this.Match.SurroundedMatchText.Substring(this.Match.MatchOffsetInText, this.Match.MatchedText.Length));
			this.Inlines.Add(bold);
			this.Inlines.Add(this.Match.SurroundedMatchText.Substring(this.Match.MatchOffsetInText + this.Match.MatchedText.Length));
		}
	}
}
