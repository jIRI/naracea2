﻿using Naracea.Common.Extensions;
using System;
using System.Windows.Input;

namespace Naracea.Core.Ui.Wpf.Controls
{
	public partial class Branch
	{
		public event EventHandler RewindChangeRequest;
		public event EventHandler RewindWordRequest;
		public event EventHandler RewindSentenceRequest;
		public event EventHandler RewindParagraphRequest;
		public event EventHandler RewindAllRequest;
		public event EventHandler ReplayChangeRequest;
		public event EventHandler ReplayWordRequest;
		public event EventHandler ReplaySentenceRequest;
		public event EventHandler ReplayParagraphRequest;
		public event EventHandler ReplayAllRequest;

		#region Properties
		bool _canRewind = false;
		public bool CanRewind
		{
			get
			{
				return _canRewind;
			}
			set
			{
				if (_canRewind != value)
				{
					_canRewind = value;
					CommandManager.InvalidateRequerySuggested();
				}
			}
		}
		bool _canReplay = false;
		public bool CanReplay
		{
			get
			{
				return _canReplay;
			}
			set
			{
				if (_canReplay != value)
				{
					_canReplay = value;
					CommandManager.InvalidateRequerySuggested();
				}
			}
		}
		#endregion

		static Branch()
		{
			RewindChange.InputGestures.Add(new KeyGesture(Key.Left, ModifierKeys.Alt));
			RewindWord.InputGestures.Add(new KeyGesture(Key.Left, ModifierKeys.Alt | ModifierKeys.Control));
			RewindSentence.InputGestures.Add(new KeyGesture(Key.Left, ModifierKeys.Alt | ModifierKeys.Shift));
			RewindParagraph.InputGestures.Add(new KeyGesture(Key.Left, ModifierKeys.Alt | ModifierKeys.Control | ModifierKeys.Shift));
			RewindAll.InputGestures.Add(new KeyGesture(Key.Home, ModifierKeys.Alt));

			ReplayChange.InputGestures.Add(new KeyGesture(Key.Right, ModifierKeys.Alt));
			ReplayWord.InputGestures.Add(new KeyGesture(Key.Right, ModifierKeys.Alt | ModifierKeys.Control));
			ReplaySentence.InputGestures.Add(new KeyGesture(Key.Right, ModifierKeys.Alt | ModifierKeys.Shift));
			ReplayParagraph.InputGestures.Add(new KeyGesture(Key.Right, ModifierKeys.Alt | ModifierKeys.Control | ModifierKeys.Shift));
			ReplayAll.InputGestures.Add(new KeyGesture(Key.End, ModifierKeys.Alt));
		}

		#region Rewind commands
		public void CanExecuteRewindCommand(object sender, CanExecuteRoutedEventArgs e)
		{
			var branch = sender as Branch;
			e.CanExecute = branch.CanRewind;
			e.Handled = true;
		}

		static RoutedCommand _rewindChange = new RoutedCommand("rewindChange", typeof(Branch));
		public static RoutedCommand RewindChange { get { return _rewindChange; } }
		public void ExecuteRewindChangeCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.RewindChangeRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _rewindWord = new RoutedCommand("rewindWord", typeof(Branch));
		public static RoutedCommand RewindWord { get { return _rewindWord; } }
		public void ExecuteRewindWordCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.RewindWordRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _rewindSentence = new RoutedCommand("rewindSentence", typeof(Branch));
		public static RoutedCommand RewindSentence { get { return _rewindSentence; } }
		public void ExecuteRewindSentenceCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.RewindSentenceRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _rewindParagraph = new RoutedCommand("rewindParagraph", typeof(Branch));
		public static RoutedCommand RewindParagraph { get { return _rewindParagraph; } }
		public void ExecuteRewindParagraphCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.RewindParagraphRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _rewindAll = new RoutedCommand("rewindAll", typeof(Branch));
		public static RoutedCommand RewindAll { get { return _rewindAll; } }
		public void ExecuteRewindAllCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.RewindAllRequest.Raise(this, new EventArgs());
		}
		#endregion

		#region Replay commands
		public void CanExecuteReplayCommand(object sender, CanExecuteRoutedEventArgs e)
		{
			var branch = sender as Branch;
			e.CanExecute = branch.CanReplay;
			e.Handled = true;
		}

		static RoutedCommand _replayChange = new RoutedCommand("replayChange", typeof(Branch));
		public static RoutedCommand ReplayChange { get { return _replayChange; } }
		public void ExecuteReplayChangeCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.ReplayChangeRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _replayWord = new RoutedCommand("replayWord", typeof(Branch));
		public static RoutedCommand ReplayWord { get { return _replayWord; } }
		public void ExecuteReplayWordCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.ReplayWordRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _replaySentence = new RoutedCommand("replaySentence", typeof(Branch));
		public static RoutedCommand ReplaySentence { get { return _replaySentence; } }
		public void ExecuteReplaySentenceCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.ReplaySentenceRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _replayParagraph = new RoutedCommand("replayParagraph", typeof(Branch));
		public static RoutedCommand ReplayParagraph { get { return _replayParagraph; } }
		public void ExecuteReplayParagraphCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.ReplayParagraphRequest.Raise(this, new EventArgs());
		}

		static RoutedCommand _replayAll = new RoutedCommand("replayAll", typeof(Branch));
		public static RoutedCommand ReplayAll { get { return _replayAll; } }
		public void ExecuteReplayAllCommand(object sender, ExecutedRoutedEventArgs e)
		{
			this.ReplayAllRequest.Raise(this, new EventArgs());
		}
		#endregion
	}
}
