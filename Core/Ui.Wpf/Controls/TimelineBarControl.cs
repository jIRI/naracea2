﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.Controls {
	public partial class TimelineBarControl {
		public TimelineBar Bar { get; set; }
		public static int DefaultWidth { get; set; }
		public static Brush Brush { get; set; }
		public static Brush Stroke { get; set; }
		public static Brush GridBrush { get; set; }
		public static Brush GridStroke { get; set; }
		public static Brush MarkerBrush { get; set; }
		public static Brush MarkerStrokeBrush { get; set; }

		static TimelineBarControl() {
			TimelineBarControl.DefaultWidth = 15;
		}

		public TimelineBarControl(TimelineBar bar) {
			this.Bar = bar;
		}

		public static void UpdateColors(Color foreground, Color background) {
			var gradientStops = new GradientStopCollection();
			var barForeground = foreground;
			barForeground.R >>= 1;
			barForeground.R += 5;
			barForeground.G >>= 1;
			barForeground.G += 25;
			barForeground.B >>= 1;
			barForeground.B += 5;
			gradientStops.Add(new GradientStop(background, 0.0));
			gradientStops.Add(new GradientStop(barForeground, 0.20));
			gradientStops.Add(new GradientStop(barForeground, 0.35));
			gradientStops.Add(new GradientStop(background, 1.0));
			TimelineBarControl.Brush = new LinearGradientBrush(gradientStops, 0);
			
			var strokeColor = background;
			strokeColor.R >>= 1;
			strokeColor.R += 5;
			strokeColor.G >>= 1;
			strokeColor.G += 5;
			strokeColor.B >>= 1;
			strokeColor.B += 5;
			TimelineBarControl.Stroke = new SolidColorBrush(strokeColor);
			var gridStrokeColor = background;
			gridStrokeColor.R >>= 1;
			gridStrokeColor.R += 100;
			gridStrokeColor.G >>= 1;
			gridStrokeColor.G += 100;
			gridStrokeColor.B >>= 1;
			gridStrokeColor.B += 100;
			TimelineBarControl.GridStroke = new SolidColorBrush(gridStrokeColor);
			TimelineBarControl.GridBrush = new SolidColorBrush(background);

			var markerGradientStops = new GradientStopCollection();
			var markerForeground = foreground;
			markerForeground.R >>= 1;
			markerForeground.G >>= 1;
			markerForeground.B >>= 1;
			markerForeground.G += 100;
			markerForeground.B += 50;
			markerGradientStops.Add(new GradientStop(background, 0.0));
			markerGradientStops.Add(new GradientStop(markerForeground, 0.35));
			markerGradientStops.Add(new GradientStop(markerForeground, 0.55));
			markerGradientStops.Add(new GradientStop(background, 1.0));
			TimelineBarControl.MarkerBrush = new LinearGradientBrush(markerGradientStops, 90);
			TimelineBarControl.MarkerStrokeBrush = new SolidColorBrush(foreground);
		}
	}
}
