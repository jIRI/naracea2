﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Extensions;

namespace Naracea.Core.Ui.Wpf.Controls {
	public class TextChangeArgs : EventArgs {
		public int Position { get; private set; }
		public string Text { get; private set; }

		public TextChangeArgs(int pos, string text) {
			this.Position = pos;
			this.Text = text.NormalizeEols();
		}
	}

	public class RawTextChangedArgs : EventArgs {
		public int Position { get; private set; }
		public int AddedLength { get; private set; }
		public int RemovedLength { get; private set; }

		public RawTextChangedArgs(int pos, int added, int removed) {
			this.Position = pos;
			this.AddedLength = added;
			this.RemovedLength = removed;
		}
	}

	public class ChangeArgs : EventArgs {
	}

	public class SuggestSpellingArgs : EventArgs {
		public int WordPosition { get; private set; }
		public int WordLength { get; private set; }
		public SuggestSpellingArgs(int position, int length) {
			this.WordPosition = position;
			this.WordLength = length;
		}
	}
}
