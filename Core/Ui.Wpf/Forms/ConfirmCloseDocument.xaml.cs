﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Naracea.Core.Ui.Wpf.View.Dialogs;

namespace Naracea.Core.Ui.Wpf.Forms {
	/// <summary>
	/// Interaction logic for ConfirmExit.xaml
	/// </summary>
	public partial class ConfirmCloseDocument : Window, IHasYesNoCancelEvents {
		public event EventHandler YesClicked;
		public event EventHandler NoClicked;
		public event EventHandler CancelClicked;

		public ConfirmCloseDocument() {
			InitializeComponent();

			this.KeyDown += this.HandleKeyDown;
		}

		void HandleKeyDown(object sender, KeyEventArgs e) {
			UIElement keyboardFocus = Keyboard.FocusedElement as UIElement;
			switch( e.Key ) {
				case System.Windows.Input.Key.Escape:
					this.HandleCancel();
					break;
				case Key.Up:
					if( keyboardFocus != null ) {
						keyboardFocus.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
					}
					break;
				case Key.Down:
					if( keyboardFocus != null ) {
						keyboardFocus.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
					}
					break;
			}
		}

		private void buttonYes_Click(object sender, RoutedEventArgs e) {
			if( this.YesClicked != null ) {
				this.YesClicked(this, new EventArgs());
			}
			this.DialogResult = true;
			this.Close();
		}

		private void buttonNo_Click(object sender, RoutedEventArgs e) {
			if( this.NoClicked != null ) {
				this.NoClicked(this, new EventArgs());
			}
			this.Close();
		}

		private void buttonCancel_Click(object sender, RoutedEventArgs e) {
			this.HandleCancel();
		}

		private void HandleCancel() {
			if( this.CancelClicked != null ) {
				this.CancelClicked(this, new EventArgs());
			}
			this.Close();
		}
	}
}
