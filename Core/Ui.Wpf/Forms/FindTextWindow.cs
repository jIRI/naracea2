﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;
using System.Windows.Controls;
using Naracea.View;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class FindTextWindow {
		public class RemoveItemEventArgs : EventArgs {
			public object Item { get; private set; }

			public RemoveItemEventArgs(object item) {
				this.Item = item;
			}
		}

		public class SearchMatchItemEventArgs : EventArgs {
			public ISearchMatchItem Item {get;private set;}
			public SearchMatchItemEventArgs(ISearchMatchItem item) {
				this.Item = item;
			}
		}

		public event EventHandler FindAllRequested;
		public event EventHandler ReplaceAllRequested;
		public event EventHandler FindNextRequested;
		public event EventHandler FindNextAndReplaceRequested;
		public event EventHandler ClearAllResultsRequested;
		public event EventHandler ClearCurrentBranchResultsRequested;
		public event EventHandler FindPreviousRequested;
		public event EventHandler<SearchMatchItemEventArgs> ReplaceRequested;
		public event EventHandler<RemoveItemEventArgs> ForgetItem;
		
		#region ctors
		static FindTextWindow() {
			FindTextWindow.FindNext.InputGestures.Add(new KeyGesture(Key.F3));
			FindTextWindow.FindNext.InputGestures.Add(new KeyGesture(Key.F3, ModifierKeys.Shift));
		}
		#endregion

		#region Public property
		public IApplicationView ApplicationView { get; set; }
		#endregion

		#region Private method
		private bool IsAnyDocumentActive() {
			return this.ApplicationView != null
				&& this.ApplicationView.ActiveDocument != null
				&& !this.ApplicationView.ActiveDocument.IsBusy
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& !this.ApplicationView.ActiveDocument.ActiveBranch.IsBusy
			;
		}

		private void InstallEventHandlers() {
			this.Closing += this.FindTextWindow_Closing;

			this.KeyDown += (s, e) => {
				if( e.Key == System.Windows.Input.Key.Escape ) {
					this.Hide();
				}
			};

			this.regexHelpHyperlink.Click += (s, e) => {
				string navigateUri = this.regexHelpHyperlink.NavigateUri.ToString();
				Process.Start(new ProcessStartInfo(navigateUri));
				e.Handled = true;
			};
		}

		void FindTextWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			// do not close the window here.
			// this comes from user clicking on close button of the window, we need just hide the window
			// it gets closed when the app is closed
			e.Cancel = true;
			this.Hide();
		}
		#endregion

		#region Protected methods
		protected override void OnActivated(EventArgs e) {
			base.OnActivated(e);
			this.searches.Focus();
		}
		#endregion

		#region Find All command
		static RoutedCommand _findAll = new RoutedCommand("findAll", typeof(Main));
		public static RoutedCommand FindAll { get { return _findAll; } }

		public void CanExecuteFindAllCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive() && this.searches.Text.Length > 0;
			e.Handled = true;
		}

		public void ExecuteFindAllCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindAllRequested != null ) {
				this.FindAllRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Replace All command
		static RoutedCommand _replaceAll = new RoutedCommand("replaceAll", typeof(Main));
		public static RoutedCommand ReplaceAll { get { return _replaceAll; } }

		public void CanExecuteReplaceAllCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive() && this.searches.Text.Length > 0;
			e.Handled = true;
		}

		public void ExecuteReplaceAllCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ReplaceAllRequested != null ) {
				this.ReplaceAllRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Find next command
		static RoutedCommand _findNext = new RoutedCommand("findNext", typeof(Main));
		public static RoutedCommand FindNext { get { return _findNext; } }

		public void CanExecuteFindNextCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive() && this.searches.Text.Length > 0;
			e.Handled = true;
		}

		public void ExecuteFindNextCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindNextRequested != null ) {
				this.FindNextRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Find previous command
		static RoutedCommand _findPrevious = new RoutedCommand("findPrevious", typeof(Main));
		public static RoutedCommand FindPrevious { get { return _findPrevious; } }

		public void CanExecuteFindPreviousCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive() && this.searches.Text.Length > 0;
			e.Handled = true;
		}

		public void ExecuteFindPreviousCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindPreviousRequested != null ) {
				this.FindPreviousRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Replace & next command
		static RoutedCommand _findNextAndReplace = new RoutedCommand("findNextAndReplace", typeof(Main));
		public static RoutedCommand FindNextAndReplace { get { return _findNextAndReplace; } }

		public void CanExecuteFindNextAndReplaceCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive() && this.searches.Text.Length > 0;
			e.Handled = true;
		}

		public void ExecuteFindNextAndReplaceCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindNextAndReplaceRequested != null ) {
				this.FindNextAndReplaceRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Clear all results command
		static RoutedCommand _clearAllResults = new RoutedCommand("clearAllResults", typeof(Main));
		public static RoutedCommand ClearAllResults { get { return _clearAllResults; } }

		public void CanExecuteClearAllResultsCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive();
			e.Handled = true;
		}

		public void ExecuteClearAllResultsCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ClearAllResultsRequested != null ) {
				this.ClearAllResultsRequested(this, new EventArgs());
			}
		}
		#endregion
		
		#region Clear current branch results command
		static RoutedCommand _clearCurrentBranchResults = new RoutedCommand("clearCurrentBranchResults", typeof(Main));
		public static RoutedCommand ClearCurrentBranchResults { get { return _clearCurrentBranchResults; } }

		public void CanExecuteClearCurrentBranchResultsCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.IsAnyDocumentActive();
			e.Handled = true;
		}

		public void ExecuteClearCurrentBranchResultsCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ClearCurrentBranchResultsRequested != null ) {
				this.ClearCurrentBranchResultsRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Replace command
		private void ReplaceButton_Click(object sender, RoutedEventArgs e) {
			if( this.ReplaceRequested != null ) {
				ISearchMatchItem item = (ISearchMatchItem)( sender as Button ).Tag;
				this.ReplaceRequested(this, new SearchMatchItemEventArgs(item));
			}
		}
		#endregion

		#region Remove item command
		private void RemoveItemButton_Click(object sender, RoutedEventArgs e) {
			if( this.ForgetItem != null ) {
				this.ForgetItem(this, new RemoveItemEventArgs(( sender as Button ).Tag));
			}
		}
		#endregion

		#region Public methods
		public void SetBusy() {
			this.IsEnabled = false;
		}

		public void SetNotBusy() {
			this.IsEnabled = true;
		}
		#endregion
	}
}
