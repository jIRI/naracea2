﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class HelpViewer {
		public HelpViewer() {
			InitializeComponent();

			this.Closing += (s, e) => {
				e.Cancel = true;
				this.Hide();
			};

			this.homepageLink.Click += (s, e) => {
				string navigateUri = this.homepageLink.NavigateUri.ToString();
				Process.Start(new ProcessStartInfo(navigateUri));
				e.Handled = true;
			};

			this.KeyDown += (s, e) => {
				if( e.Key == System.Windows.Input.Key.Escape ) {
					this.Hide();
				}
			};
		}
	}
}
