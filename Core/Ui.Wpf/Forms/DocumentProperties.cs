﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class DocumentProperties {
		#region Private method
		private void InstallEventHandlers() {
			this.Closing += this.DocumentProperties_Closing;
		}

		void DocumentProperties_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			// do not close the window here.
			// this comes from user clicking on close button of the window, we need just hide the window
			// it gets closed when the app is closed
			e.Cancel = true;
			this.Hide();
		}
		#endregion
	}
}
