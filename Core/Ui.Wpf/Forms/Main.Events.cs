﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;

namespace Naracea.Core.Ui.Wpf.Forms {
	public class TabSizeChangeEventArgs : EventArgs {
		public int Size { get; private set; }
		public TabSizeChangeEventArgs(int size) {
			this.Size = size;
		}
	}

	public class ExportCurrentBranchEventArgs : EventArgs {
		public ExportFormatItem Item { get; private set; }
		public ExportCurrentBranchEventArgs(ExportFormatItem item) {
			this.Item = item;
		}
	}	
}
