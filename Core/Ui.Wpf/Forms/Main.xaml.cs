﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Naracea.View;
using Naracea.Core.Ui.Wpf.Interfaces;
using System.Windows.Controls.Ribbon;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class Main : RibbonWindow, ICanBeBusy {
		public Main() {
			InitializeComponent();
			InstallEventHandlers();
		}
	}
}
