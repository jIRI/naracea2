﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class TextViewer {
		public TextViewer() {
			InitializeComponent();
			this.KeyDown += (s, e) => {
				if( e.Key == System.Windows.Input.Key.Escape ) {
					this.Close();
				}
			};
		}
	}
}
