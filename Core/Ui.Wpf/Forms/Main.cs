﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using Naracea.View;
using Naracea.Common.Extensions;
using Microsoft.Windows.Input;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class Main {
		#region Events
		public event EventHandler CloseFileRequested;
		public event EventHandler NewFileRequested;
		public event EventHandler OpenFileRequested;
		public event EventHandler SaveFileRequested;
		public event EventHandler SaveFileAsRequested;
		public event EventHandler SaveAllFilesRequested;
		public event EventHandler ExitRequested;
		public event EventHandler ActiveDocumentChanged;
		public event EventHandler RefocusNeeded;
		public event EventHandler ShowFindTextWindowRequested;
		public event EventHandler FindNextRequested;
		public event EventHandler FindPreviousRequested;
		public event EventHandler ShowDocumentPropertiesRequested;
		public event EventHandler ShowBranchPropertiesRequested;
		public event EventHandler DeleteBranchRequested;
		public event EventHandler ShowTextViewerRequested;
		public event EventHandler OpenBrowserRequested;
		public event EventHandler ToggleWrapLinesRequested;
		public event EventHandler ToggleShowWhitespacesRequested;
		public event EventHandler ToggleKeepTabsRequested;
		public event EventHandler<TabSizeChangeEventArgs> TabSizeChangeRequested;
		public event EventHandler PrintRequested;
		public event EventHandler HelpRequested;
		public event EventHandler RestoreEditorFocusRequest;
		public event EventHandler ShowTimelineOnlyRequest;
		public event EventHandler ShowTimelineAndChangeStreamRequest;
		public event EventHandler ShowChangeStreamOnlyRequest;
		public event EventHandler InstallSpellcheckDictionaryRequested;
		public event EventHandler SpellcheckRecheckAllRequested;
		public event EventHandler CopyToClipboardAsHtmlRequested;
		public event EventHandler<ExportCurrentBranchEventArgs> ExportCurrentBranchRequested;
		#endregion

		#region Properties
		const int MaximumMruItemsWithHotKey = 9;
		public IApplicationView ApplicationView { get; set; }
		public WpfExtension.CustomControls.TabControl DocumentContainer { get { return this.documentContainer; } }
		public TabItem SelectedDocumentTab { get { return (TabItem)this.DocumentContainer.SelectedItem; } }

		public bool AreFileOperationsLocked {
			get {
				return this.ApplicationView == null || this.ApplicationView.AreFileOperationsLocked;
			}
		}
		private bool HasActiveDocument {
			get {
				return this.ApplicationView != null && this.ApplicationView.ActiveDocument != null;
			}
		}

		#endregion

		#region ctors
		static Main() {
			ApplicationCommands.Close.InputGestures.Add(new KeyGesture(Key.F4, ModifierKeys.Control));
			ApplicationCommands.New.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
			ApplicationCommands.Save.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
			ApplicationCommands.Open.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
			Main.Exit.InputGestures.Add(new KeyGesture(Key.Q, ModifierKeys.Control));
			Main.FindNext.InputGestures.Add(new KeyGesture(Key.F3));
			Main.FindPrevious.InputGestures.Add(new KeyGesture(Key.F3, ModifierKeys.Shift));
			Main.SaveAll.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift));
			Main.TextViewer.InputGestures.Add(new KeyGesture(Key.F11));
			Main.OpenBrowser.InputGestures.Add(new KeyGesture(Key.F12));
			Main.SpellcheckRecheckAll.InputGestures.Add(new KeyGesture(Key.F7));
		}
		#endregion

		#region Close command
		private void CanExecuteCloseCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.ApplicationView.ActiveDocument.IsBusy && !this.ApplicationView.ActiveDocument.IsLocked;
			e.Handled = true;
		}

		private void ExecuteCloseCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.CloseFileRequested != null ) {
				this.CloseFileRequested(this, new EventArgs());
			}
		}
		#endregion

		#region New command
		private void CanExecuteNewCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		private void ExecuteNewCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.NewFileRequested != null ) {
				this.NewFileRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Open command
		private void CanExecuteOpenCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		private void ExecuteOpenCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.OpenFileRequested != null ) {
				this.OpenFileRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Print command
		private void CanExecutePrintCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.AreFileOperationsLocked;
			e.Handled = true;
		}

		private void ExecutePrintCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.PrintRequested != null ) {
				this.PrintRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Help command
		private void CanExecuteHelpCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
		}

		private void ExecuteHelpCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.HelpRequested != null ) {
				this.HelpRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Save command
		private void CanExecuteSaveCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.ApplicationView.ActiveDocument.IsBusy && !this.ApplicationView.ActiveDocument.IsLocked;
			e.Handled = true;
		}

		private void ExecuteSaveCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.SaveFileRequested != null ) {
				this.SaveFileRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Save As command
		private void CanExecuteSaveAsCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.ApplicationView.ActiveDocument.IsBusy && !this.ApplicationView.ActiveDocument.IsLocked;
			e.Handled = true;
		}

		private void ExecuteSaveAsCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.SaveFileAsRequested != null ) {
				this.SaveFileAsRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Save All command
		static RoutedCommand _saveAll = new RoutedCommand("saveAll", typeof(Main));
		public static RoutedCommand SaveAll { get { return _saveAll; } }

		private void CanExecuteSaveAllCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.AreFileOperationsLocked;
			e.Handled = true;
		}

		private void ExecuteSaveAllCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.SaveAllFilesRequested != null ) {
				this.SaveAllFilesRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Exit command
		static RoutedCommand _exit = new RoutedCommand("exit", typeof(Main));
		public static RoutedCommand Exit { get { return _exit; } }

		private void CanExecuteExitCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = !this.AreFileOperationsLocked;
			e.Handled = true;
		}

		private void ExecuteExitCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ExitRequested != null ) {
				this.ExitRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Show Find Text Window command
		public void CanExecuteShowFindTextWindowCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument;
			e.Handled = true;
		}

		public void ExecuteShowFindTextWindowCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowFindTextWindowRequested != null ) {
				this.ShowFindTextWindowRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Find next command
		static RoutedCommand _findNext = new RoutedCommand("findNext", typeof(Main));
		public static RoutedCommand FindNext { get { return _findNext; } }

		public void CanExecuteFindNextCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		public void ExecuteFindNextCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindNextRequested != null ) {
				this.FindNextRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Find previous command
		static RoutedCommand _findPrevious = new RoutedCommand("findPrevious", typeof(Main));
		public static RoutedCommand FindPrevious { get { return _findPrevious; } }

		public void CanExecuteFindPreviousCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		public void ExecuteFindPreviousCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.FindPreviousRequested != null ) {
				this.FindPreviousRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Document properties command
		static RoutedCommand _documentProperties = new RoutedCommand("documentProperties", typeof(Main));
		public static RoutedCommand DocumentProperties { get { return _documentProperties; } }

		public void CanExecuteDocumentPropertiesCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.ApplicationView.ActiveDocument.IsBusy;
			e.Handled = true;
		}

		public void ExecuteDocumentPropertiesCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowDocumentPropertiesRequested != null ) {
				this.ShowDocumentPropertiesRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Branch properties command
		static RoutedCommand _branchProperties = new RoutedCommand("branchProperties", typeof(Main));
		public static RoutedCommand BranchProperties { get { return _branchProperties; } }

		public void CanExecuteBranchPropertiesCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument && !this.ApplicationView.ActiveDocument.IsBusy;
			e.Handled = true;
		}

		public void ExecuteBranchPropertiesCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowBranchPropertiesRequested != null ) {
				this.ShowBranchPropertiesRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Delete branch command
		static RoutedCommand _deleteBranch = new RoutedCommand("deleteBranch", typeof(Main));
		public static RoutedCommand DeleteBranch { get { return _deleteBranch; } }

		public void CanExecuteDeleteBranchCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.CanBeDeleted;
			e.Handled = true;
		}

		public void ExecuteDeleteBranchCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.DeleteBranchRequested != null ) {
				this.DeleteBranchRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Toggle wrap linescommand
		static RoutedCommand _toggleWrapLines = new RoutedCommand("toggleWrapLines", typeof(Main));
		public static RoutedCommand ToggleWrapLines { get { return _toggleWrapLines; } }

		public void CanExecuteToggleWrapLinesCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteToggleWrapLinesCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ToggleWrapLinesRequested != null ) {
				this.ToggleWrapLinesRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Toggle show whitespaces command
		static RoutedCommand _toggleWhitespaces = new RoutedCommand("toggleWhitespaces", typeof(Main));
		public static RoutedCommand ToggleWhitespaces { get { return _toggleWhitespaces; } }

		public void CanExecuteToggleWhitespacesCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteToggleWhitespacesCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ToggleShowWhitespacesRequested != null ) {
				this.ToggleShowWhitespacesRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Toggle keep tabs command
		static RoutedCommand _toggleKeepTabs = new RoutedCommand("toggleKeepTabs", typeof(Main));
		public static RoutedCommand ToggleKeepTabs { get { return _toggleKeepTabs; } }

		public void CanExecuteToggleKeepTabsCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteToggleKeepTabsCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ToggleKeepTabsRequested != null ) {
				this.ToggleKeepTabsRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Show timeline only command
		static RoutedCommand _showTimelineOnly = new RoutedCommand("_showTimelineOnly", typeof(Main));
		public static RoutedCommand ShowTimelineOnly { get { return _showTimelineOnly; } }

		public void CanExecuteShowTimelineOnlyCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null;
			e.Handled = true;
		}

		public void ExecuteShowTimelineOnlyCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowTimelineOnlyRequest != null ) {
				this.ShowTimelineOnlyRequest(this, new EventArgs());
			}
		}
		#endregion

		#region Show timeline and change stream command
		static RoutedCommand _showTimelineAndChangeStream = new RoutedCommand("_showTimelineAndChangeStream", typeof(Main));
		public static RoutedCommand ShowTimelineAndChangeStream { get { return _showTimelineAndChangeStream; } }

		public void CanExecuteShowTimelineAndChangeStreamCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null;
			e.Handled = true;
		}

		public void ExecuteShowTimelineAndChangeStreamCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowTimelineAndChangeStreamRequest != null ) {
				this.ShowTimelineAndChangeStreamRequest(this, new EventArgs());
			}
		}
		#endregion

		#region Show change stream command
		static RoutedCommand _showChangeStreamOnly = new RoutedCommand("_showChangeStreamOnly", typeof(Main));
		public static RoutedCommand ShowChangeStreamOnly { get { return _showChangeStreamOnly; } }

		public void CanExecuteShowChangeStreamOnlyCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null;
			e.Handled = true;
		}

		public void ExecuteShowChangeStreamOnlyCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowChangeStreamOnlyRequest != null ) {
				this.ShowChangeStreamOnlyRequest(this, new EventArgs());
			}
		}
		#endregion

		#region Branch text view command
		static RoutedCommand _textViewer = new RoutedCommand("textViewer", typeof(Main));
		public static RoutedCommand TextViewer { get { return _textViewer; } }

		public void CanExecuteTextViewerCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteTextViewerCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ShowTextViewerRequested != null ) {
				this.ShowTextViewerRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Branch open browser command
		static RoutedCommand _openBrowser = new RoutedCommand("openBrowser", typeof(Main));
		public static RoutedCommand OpenBrowser { get { return _openBrowser; } }

		public void CanExecuteOpenBrowserCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument 
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteOpenBrowserCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.OpenBrowserRequested != null ) {
				this.OpenBrowserRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Spellchecker commands
		static RoutedCommand _spellcheckInstallLanguage = new RoutedCommand("spellcheckInstallLanguage", typeof(Main));
		public static RoutedCommand SpellcheckInstallLanguage { get { return _spellcheckInstallLanguage; } }

		public void CanExecuteSpellcheckInstallLanguageCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = true;
			e.Handled = true;
		}

		public void ExecuteSpellcheckInstallLanguageCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.InstallSpellcheckDictionaryRequested != null ) {
				this.InstallSpellcheckDictionaryRequested(this, new EventArgs());
			}
		}

		static RoutedCommand _spellcheckRecheckAll = new RoutedCommand("spellcheckRecheckAll", typeof(Main));
		public static RoutedCommand SpellcheckRecheckAll  { get { return _spellcheckRecheckAll ; } }

		public void CanExecuteSpellcheckRecheckAllCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteSpellcheckRecheckAllCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.SpellcheckRecheckAllRequested!= null ) {
				this.SpellcheckRecheckAllRequested(this, new EventArgs());
			}
		}	
		#endregion

		#region Copy to clipboard as HTML command
		static RoutedCommand _copyToClipboardAsHtml = new RoutedCommand("copyToClipboardAsHtml", typeof(Main));
		public static RoutedCommand CopyToClipboardAsHtml { get { return _copyToClipboardAsHtml; } }

		public void CanExecuteCopyToClipboardAsHtmlCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteCopyToClipboardAsHtmlCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.CopyToClipboardAsHtmlRequested != null ) {
				this.CopyToClipboardAsHtmlRequested(this, new EventArgs());
			}
		}
		#endregion

		#region Export command
		static RoutedCommand _exportCurrentBranch = new RoutedCommand("exportCurrentBranch", typeof(Main));
		public static RoutedCommand ExportCurrentBranch { get { return _exportCurrentBranch; } }

		public void CanExecuteExportCurrentBranchCommand(object sender, CanExecuteRoutedEventArgs e) {
			e.CanExecute = this.HasActiveDocument
				&& this.ApplicationView.ActiveDocument.ActiveBranch != null
				&& this.ApplicationView.ActiveDocument.ActiveBranch.Editor != null;
			e.Handled = true;
		}

		public void ExecuteExportCurrentBranchCommand(object sender, ExecutedRoutedEventArgs e) {
			if( this.ExportCurrentBranchRequested != null ) {
				var type = e.Parameter as ExportFormatItem;
				if( type != null ) {
					this.ExportCurrentBranchRequested(this, new ExportCurrentBranchEventArgs(type));
				}
			}
		}
		#endregion

		#region ctors
		public Main(IApplicationView appView)
			: base() {
			this.ApplicationView = appView;
		}
		#endregion

		#region ICanBeBusy Members
		public void SetBusy() {
			this.SetComponentsEnabled(false);
		}

		public void SetNotBusy() {
			this.SetComponentsEnabled(true);
		}
		#endregion

		#region Private methods
		void documentContainer_RefocusNeeded(object sender, EventArgs e) {
			this.RefocusNeeded.Raise(this, new EventArgs());
		}

		void documentContainer_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.ActiveDocumentChanged.Raise(this, new EventArgs());
			e.Handled = true;
		}

		void SetComponentsEnabled(bool enabled) {
			this.documentContainer.IsEnabled = enabled;
			CommandManager.InvalidateRequerySuggested();
		}

		void InstallEventHandlers() {
			this.viewEditorTabSize.ValueChanged += (s, e) => this.TabSizeChangeRequested(this, new TabSizeChangeEventArgs(this.viewEditorTabSize.Value));
			;
		}
		#endregion

		#region Overrides
		bool WasColumnSelection { get; set; }

		protected override void OnPreviewMouseUp(MouseButtonEventArgs e) {
			base.OnPreviewMouseUp(e);
			this.WasColumnSelection = Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
			if( this.WasColumnSelection ) {
				KeyTipService.DismissKeyTips();
			}
		}

		protected override void OnPreviewKeyDown(KeyEventArgs e) {
			base.OnPreviewKeyDown(e);
			if( e.Key == Key.LeftAlt || e.Key == Key.RightAlt ) {
				this.WasColumnSelection = false;
			}
		}

		protected override void OnPreviewKeyUp(KeyEventArgs e) {
			base.OnPreviewKeyUp(e);
			if( this.WasColumnSelection ) {
				KeyTipService.DismissKeyTips();
				this.WasColumnSelection = false;
				this.RestoreEditorFocusRequest.Raise(this, new EventArgs());
			}
		}
		#endregion
	}
}
