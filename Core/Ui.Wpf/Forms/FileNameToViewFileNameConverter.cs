﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Naracea.Core.Ui.Wpf.Forms {
	public class FileNameToViewFileNameConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			var fileName = (string)value;
			int lenHint = 50;
			return Common.SmartFileNameShortener.Shorten(fileName, lenHint);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
