﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Ui.Wpf.Forms {
	public partial class ErrorOccured {
		public string ShortMessage {
			get {
				return this.shortMessage.Text;
			}
			set {
				this.shortMessage.Text = value;
			}
		}

		public string DetailedMessage {
			get {
				return this.detailedMessage.Text;
			}
			set {
				this.detailedMessage.Text = value;
			}
		}
	}
}
