﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Naracea.Core.Ui.Wpf.Forms {
	/// <summary>
	/// Interaction logic for ConfirmExit.xaml
	/// </summary>
	public partial class ErrorOccured : Window {
		public ErrorOccured() {
			InitializeComponent();
			this.KeyDown += this.HandleKeyDown;
		}

		void HandleKeyDown(object sender, KeyEventArgs e) {
			UIElement keyboardFocus = Keyboard.FocusedElement as UIElement;
			switch( e.Key ) {
				case System.Windows.Input.Key.Enter:
					goto case System.Windows.Input.Key.Escape;
				case System.Windows.Input.Key.Escape:
					this.Close();
					break;
			}
		}

		private void copyToClipboard_Click(object sender, RoutedEventArgs e) {
			Clipboard.SetText(this.DetailedMessage, TextDataFormat.Text);
		}	
	}
}
