﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Context;
using Naracea.Core.Ui.Wpf.View;
using Naracea.Core.Ui.Wpf.View.Dialogs;
using Naracea.View;
using System.Diagnostics;
using Naracea.View.Components;
using Naracea.Core.Ui.Wpf.View.Extensions;
using MemBus;

namespace Naracea.Core.Ui.Wpf.Context {
	public class WpfViewContext : ViewContext {
		#region Attributes
		Func<IOpenDocumentDialog> _openDocumentDialogFactory;
		Func<ISaveDocumentDialog> _saveDocumentDialogFactory;
		Func<IConfirmCloseDocumentDialog> _confirmCloseDocumentDialogFactory;
		Func<IConfirmBranchDeletionDialog> _confirmBranchDeletionDialogFactory;
		Func<IErrorOccuredDialog> _errorOccuredDialogFactory;
		Func<IDocumentView> _documentViewFactory;
		Func<IBranchView> _branchViewFactory;
		Func<ITextEditorView> _textEditorViewFactory;
		Func<ITimelineView> _timelineViewFactory;
		Func<IChangeStreamView> _changeStreamViewFactory;
		Func<IProgressIndicator> _progressIndicatorFactory;
		Func<ICurrentColumnIndicator> _columnIndicatorFactory;
		Func<ICurrentLineIndicator> _lineIndicatorFactory;
		Func<ITextLengthIndicator> _textLenIndicatorFactory;
		Func<ICurrentCaretPositionIndicator> _caretPositionIndicatorFactory;
		Func<IWritingModeIndicator> _writingModeIndicatorFactory;
		Func<ICompositeIndicator> _compositeIndicatorFactory;
		Func<IEditorIndicators> _branchIndicatorsFactory;
		Func<IFindReplaceTextView> _findTextViewFactory;
		Func<ISearchMatchItem> _searchMatchItemFactory;
		Func<IDocumentPropertiesView> _documentPropertiesFactory;
		Func<IBranchPropertiesView> _branchPropertiesFactory;
		Func<IPrintDialog> _printDialogFactory;
		Func<IBranchTextViewerView> _textViewerFactory;
		Func<IHelpViewerView> _helpViewerFactory;
		Func<IFileOperationsAreLockedDialog> _fileOperationsAreLockedDialogFactory;
		Func<IInstallDictionaryDialog> _installDictionaryDialogFactory;
		Func<IUpdateAvailableIndicator> _updateAvailableFactory;
		Func<IExportBranchDialog> _exportBranchDialogFactory;
		Func<IIntIndicator> _intIndicatorFactory;
		Func<IStringIndicator> _stringIndicatorFactory;
		#endregion

		#region ctor
		public WpfViewContext(
			ApplicationView application,
			IBus bus,
			Func<IOpenDocumentDialog> openDocumentDialogFactory,
			Func<ISaveDocumentDialog> saveDocumentDialogFactory,
			Func<IConfirmCloseDocumentDialog> confirmCloseDocumentDialogFactory,
			Func<IConfirmBranchDeletionDialog> confirmBranchDeletionDialogFactory,
			Func<IErrorOccuredDialog> errorOccuredDialogFactory,
			Func<IFileOperationsAreLockedDialog> fileOperationsAreLockedDialogFactory,
			Func<IDocumentView> documentViewFactory,
			Func<IBranchView> branchViewFactory,
			Func<ITextEditorView> textEditorViewFactory,
			Func<ITimelineView> timelineViewFactory,
			Func<IChangeStreamView> changeStreamViewFactory,
			Func<IProgressIndicator> progressIndicatorFactory,
			Func<IIntIndicator> intIndicatorFactory,
			Func<IStringIndicator> stringIndicatorFactory,
			Func<ICurrentColumnIndicator> columnIndicatorFactory,
			Func<ICurrentLineIndicator> lineIndicatorFactory,
			Func<ITextLengthIndicator> textLenIndicatorFactory,
			Func<ICurrentCaretPositionIndicator> caretPositionIndicatorFactory,
			Func<IWritingModeIndicator> writingModeIndicatorFactory,
			Func<ICompositeIndicator> compositeIndicatorFactory,
			Func<IEditorIndicators> branchIndicatorsFactory,
			Func<IFindReplaceTextView> findTextViewFactory,
			Func<ISearchMatchItem> searchMatchItemFactory,
			Func<IDocumentPropertiesView> documentPropertiesFactory,
			Func<IBranchPropertiesView> branchPropertiesFactory,
			Func<IPrintDialog> printDialogFactory,
			Func<IBranchTextViewerView> textViewerFactory,
			Func<IHelpViewerView> helpViewerFactory,
			Func<IInstallDictionaryDialog> installDictionaryDialogFactory,
			Func<IExportBranchDialog> exportBranchDialogFactory,
			Func<IUpdateAvailableIndicator> updateAvailableFactory
		) {
			this.ApplicationView = application;
			this.Bus = bus;

			_openDocumentDialogFactory = openDocumentDialogFactory;
			_saveDocumentDialogFactory = saveDocumentDialogFactory;
			_confirmCloseDocumentDialogFactory = confirmCloseDocumentDialogFactory;
			_confirmBranchDeletionDialogFactory = confirmBranchDeletionDialogFactory;
			_errorOccuredDialogFactory = errorOccuredDialogFactory;
			_printDialogFactory = printDialogFactory;
			_fileOperationsAreLockedDialogFactory = fileOperationsAreLockedDialogFactory;
			_installDictionaryDialogFactory = installDictionaryDialogFactory;
			_exportBranchDialogFactory = exportBranchDialogFactory;

			_documentViewFactory = documentViewFactory;
			_branchViewFactory = branchViewFactory;
			_textEditorViewFactory = textEditorViewFactory;
			_timelineViewFactory = timelineViewFactory;
			_changeStreamViewFactory = changeStreamViewFactory;

			_progressIndicatorFactory = progressIndicatorFactory;
			_intIndicatorFactory = intIndicatorFactory;
			_stringIndicatorFactory = stringIndicatorFactory;
			_columnIndicatorFactory = columnIndicatorFactory;
			_lineIndicatorFactory = lineIndicatorFactory;
			_textLenIndicatorFactory = textLenIndicatorFactory;
			_caretPositionIndicatorFactory = caretPositionIndicatorFactory;
			_writingModeIndicatorFactory = writingModeIndicatorFactory;
			_compositeIndicatorFactory = compositeIndicatorFactory;
			_branchIndicatorsFactory = branchIndicatorsFactory;

			_findTextViewFactory = findTextViewFactory;
			_documentPropertiesFactory = documentPropertiesFactory;
			_branchPropertiesFactory = branchPropertiesFactory;

			_textViewerFactory = textViewerFactory;

			_helpViewerFactory = helpViewerFactory;

			_searchMatchItemFactory = searchMatchItemFactory;

			_updateAvailableFactory = updateAvailableFactory;

			// create global ui components
			this.FindReplaceTextView = this.CreateView<IFindReplaceTextView>();
			this.ApplicationView.FindReplaceView = this.FindReplaceTextView;
		}
		#endregion

		public override T CreateView<T>() {
			var appView = this.ApplicationView as ApplicationView;
			T result = null;
			appView.Exec(new Action(() => result = this.CreateDo<T>()));
			return result;
		}

		T CreateDo<T>() {
			object view = null;
			if( typeof(T) == typeof(Naracea.View.IDocumentView) ) {
				view = _documentViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.IBranchView) ) {
				view = _branchViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.ITextEditorView) ) {
				view = _textEditorViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.ITimelineView) ) {
				view = _timelineViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.IChangeStreamView) ) {
				view = _changeStreamViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.IOpenDocumentDialog) ) {
				view = _openDocumentDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.ISaveDocumentDialog) ) {
				view = _saveDocumentDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IConfirmCloseDocumentDialog) ) {
				view = _confirmCloseDocumentDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IConfirmBranchDeletionDialog) ) {
				view = _confirmBranchDeletionDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IErrorOccuredDialog) ) {
				view = _errorOccuredDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IFileOperationsAreLockedDialog) ) {
				view = _fileOperationsAreLockedDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IProgressIndicator) ) {
				view = _progressIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IStringIndicator) ) {
				view = _stringIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IIntIndicator) ) {
				view = _intIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.ICurrentColumnIndicator) ) {
				view = _columnIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.ICurrentLineIndicator) ) {
				view = _lineIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.ITextLengthIndicator) ) {
				view = _textLenIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.ICurrentCaretPositionIndicator) ) {
				view = _caretPositionIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IWritingModeIndicator) ) {
				view = _writingModeIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.ICompositeIndicator) ) {
				view = _compositeIndicatorFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IEditorIndicators) ) {
				view = _branchIndicatorsFactory();
			} else if( typeof(T) == typeof(Naracea.View.Components.IUpdateAvailableIndicator) ) {
				view = _updateAvailableFactory();
			} else if( typeof(T) == typeof(Naracea.View.IFindReplaceTextView) ) {
				view = _findTextViewFactory();
			} else if( typeof(T) == typeof(Naracea.View.IDocumentPropertiesView) ) {
				view = _documentPropertiesFactory();
			} else if( typeof(T) == typeof(Naracea.View.IBranchPropertiesView) ) {
				view = _branchPropertiesFactory();
			} else if( typeof(T) == typeof(Naracea.View.IPrintDialog) ) {
				view = _printDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IBranchTextViewerView) ) {
				view = _textViewerFactory();
			} else if( typeof(T) == typeof(Naracea.View.IHelpViewerView) ) {
				view = _helpViewerFactory();
			} else if( typeof(T) == typeof(Naracea.View.IInstallDictionaryDialog) ) {
				view = _installDictionaryDialogFactory();
			} else if( typeof(T) == typeof(Naracea.View.IExportBranchDialog) ) {
				view = _exportBranchDialogFactory();
			} else {
				throw new NotImplementedException();
			}

			return (T)view;
		}

		public override ISearchMatchItem CreateSearchMatchItem() {
			ISearchMatchItem result = null;
			var appView = this.ApplicationView as ApplicationView;
			appView.Exec(() => result = _searchMatchItemFactory());
			return result;
		}
	}
}
