﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View {
	public class SpellcheckDictionaryItem {
		public readonly static SpellcheckDictionaryItem None = new SpellcheckDictionaryItem(null, null, null);

		public string DisplayName { get; private set; }
		public string Name { get; private set; }
		public object Data { get; private set; }

		public SpellcheckDictionaryItem(string name, string displayName, object data) {
			this.Name = name;
			this.DisplayName = displayName;
			this.Data = data;
		}
	}
}
