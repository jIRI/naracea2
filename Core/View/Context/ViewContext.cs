﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using MemBus;

namespace Naracea.View.Context {
	public abstract class ViewContext : IViewContext {
		#region IViewContext Members
		public IApplicationView ApplicationView { get; protected set; }
		public IFindReplaceTextView FindReplaceTextView { get; protected set; }
		public IBus Bus { get; protected set; }

		public abstract T CreateView<T>() where T : class;

		public abstract ISearchMatchItem CreateSearchMatchItem();

		#endregion
	}
}
