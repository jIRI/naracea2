﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;
using Naracea.View.Compounds;
using MemBus;

namespace Naracea.View.Context {
	public interface IViewContext {
		IApplicationView ApplicationView { get; }
		IFindReplaceTextView FindReplaceTextView { get; }

		IBus Bus { get; }
		T CreateView<T>() where T : class;

		ISearchMatchItem CreateSearchMatchItem();
	}
}
