﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Events;
using Naracea.View.Compounds;

namespace Naracea.View.Components {
	public interface IProgressIndicator : IIndicator {
		event EventHandler<CancelOperationEventArgs> CancelRequested;

		string ShortMessage { get; set; }
		string DetailedMessage { get; set; }
		int DetailedMessageCapacity { get; }
		bool IsIndeterminate { get; set; }
		bool IsCancellable { get; set; }
		int From { get; set; }
		int To { get; set; }
		int Value { get; set; }
	}
}
