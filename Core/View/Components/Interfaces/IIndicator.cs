﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Events;
using Naracea.View.Compounds;

namespace Naracea.View.Components {
	public interface IIndicator : IHasControl {
		void Open(IHasIndicators view);
		void Close();

		bool IsOpen { get; }
		bool IsVisible { get; }

		void Show();
		void Hide();
	}
}
