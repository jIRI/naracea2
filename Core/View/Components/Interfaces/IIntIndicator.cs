﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Events;
using Naracea.View.Compounds;

namespace Naracea.View.Components {
	public interface IIntIndicator : IIndicator, IWithDescription {
		int Value { get; set; }
	}
}
