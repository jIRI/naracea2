﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Events;
using Naracea.View.Compounds;

namespace Naracea.View.Components {
	public interface IEditorIndicators : ICompositeIndicator {
		int CurrentColumn { set; }
		int CurrentLine { set; }
		int CurrentParagraph { set; }
		int CurrentCaretPosition { set; }
		int TextLength { set; }
		bool IsOverWriteMode { set; }
	}
}
