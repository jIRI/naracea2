﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Input;
using System.Windows.Input;
using Naracea.Common.Extensions;

namespace Naracea.View {
		#region PreviewableCommand
		public class PreviewableCommand<T> : IPreviewCommand {
			Action<T> _executeMethod;
			Func<T, bool> _canExecuteMethod;
			Action<T> _previewMethod;
			Action _cancelPreviewMethod;
			bool _canExecute = false;

			public PreviewableCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod, Action<T> previewMethod, Action cancelPreviewMethod) {
				_executeMethod = executeMethod;
				_canExecuteMethod = canExecuteMethod;
				_previewMethod = previewMethod;
				_cancelPreviewMethod = cancelPreviewMethod;
			}

			public void CancelPreview() {
				_cancelPreviewMethod();
			}

			public void Preview(object parameter) {
				_previewMethod((T)parameter);
			}

			public bool CanExecute(object parameter) {
				if( parameter == null && typeof(T).IsValueType ) {
					if( !_canExecute ) {
						_canExecute = true;
						this.CanExecuteChanged.Raise(this, new EventArgs());
					}
					return true;
				}

				if( _canExecute != _canExecuteMethod((T)parameter) ) {
					_canExecute = _canExecuteMethod((T)parameter);
					this.CanExecuteChanged.Raise(this, new EventArgs());
				}

				return _canExecuteMethod((T)parameter);
			}

			public event EventHandler CanExecuteChanged;

			public void Execute(object parameter) {
				_executeMethod((T)parameter);
			}
		}
		#endregion

		#region PreviewableCommand
		public class Command : ICommand {
			Action _executeMethod;
			Action<object> _executeMethodWithParam;
			Func<bool> _canExecuteMethod;
			bool _canExecute = false;

			public Command(Action executeMethod, Func<bool> canExecuteMethod) {
				_executeMethod = executeMethod;
				_canExecuteMethod = canExecuteMethod;
				_canExecute = _canExecuteMethod();
			}

			public Command(Action<object> executeMethod, Func<bool> canExecuteMethod) {
				_executeMethodWithParam = executeMethod;
				_canExecuteMethod = canExecuteMethod;
				_canExecute = _canExecuteMethod();
			}

			public bool CanExecute(object parameter) {
				if( _canExecute != _canExecuteMethod() ) {
					_canExecute = _canExecuteMethod();
					this.CanExecuteChanged.Raise(this, new EventArgs());
				}
				return _canExecute;
			}

			public event EventHandler CanExecuteChanged;

			public void Execute(object parameter) {
				if( _executeMethodWithParam != null ) {
					_executeMethodWithParam(parameter);
				} else {
					_executeMethod();
				}
			}
		}
		#endregion
}
