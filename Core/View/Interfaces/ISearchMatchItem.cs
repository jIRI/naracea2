﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View {
	public interface ISearchMatchItem {
		string SearchPattern { get; set; }
		int PositionInText { get; set; }
		int PositionInPercents { get; set; }
		string SurroundedMatchText { get; set; }
		string MatchedText { get; set; }
		int MatchOffsetInText { get; set; }
		IDocumentView DocumentView { get; set; }
		IBranchView BranchView { get; set; }
		string DocumentName { get; }
		string BranchName { get; }
		bool IsHistorySearch { get; set; }
		int CurrentChangeIndex { get; set; }
		DateTime DateTime { get; set; }
	}
}
