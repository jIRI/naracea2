﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Collections.ObjectModel;

namespace Naracea.View {
	public interface IBranchTextViewerView
		: ICanOpenClose, 
			ICanHideShow,
			IHasControl
	{
		string Title { set; }
		void UpdateText(string text, int caretPos);
	}
}
