﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View.Events;

namespace Naracea.View {
	public interface IDocumentView
		: ICanOpenClose,
			ICanBeBusy,
			ICanBeFocused,
			IHasController,
			IHasControl,
			IHasName,
			IHasDirtyNotification
	{
		IEnumerable<IBranchView> Branches { get; }
		void AddBranch(IBranchView branchView);
		void RemoveBranch(IBranchView branchView);
		void ActivateBranch(IBranchView branchView);
		void UpdateBranch(IBranchView branchView);
		IBranchView ActiveBranch { get; set; }
		string FullPath {get; set;}
		bool IsLocked { get; set; }

		int GetBranchVisualOrder(IBranchView branchView);
	}
}
