﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Collections.ObjectModel;

namespace Naracea.View {
	public interface IHelpViewerView
		: ICanOpenClose, 
			IHasControl
	{
		void SetCurrentVersion(Version version);
		void SetContent(string path);	
	}
}
