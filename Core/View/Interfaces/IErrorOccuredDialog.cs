﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;

namespace Naracea.View {
	public interface IErrorOccuredDialog : IDialog {
		string ShortMessage { get; set; }
		string DetailedMessage { get; set; }
	}
}
