﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View.Events;

namespace Naracea.View {
	public interface ITextEditorView
		: ICanOpenClose,
			ICanBeBusy,
			ICanBeFocused,
			IHasController,
			IHasControl {
		string Text { get; set; }
		string SelectedText { get; }
		int CaretPosition { get; set; }
		int CurrentLine { get; set; }
		int CurrentColumn { get; }
		int CurrentParagraph { get; set; }
		int TextLength { get; }
		bool IsOverwriteMode { get; }
		bool WrapsLines { get; set; }
		bool ShowWhitespaces { get; set; }
		int TabSize { get; set; }
		bool KeepTabs { get; set; }
		string SyntaxHighlighting { get; set; }
		string SpellcheckLanguageName { get; set; }

		void SetSpellingErrors(IEnumerable<Tuple<int, int>> errors);
		void ClearSpellingErrors();
		void SetSpellingSuggestions(IEnumerable<string> suggestions);

		void InsertTextBeforePosition(int pos, string text);
		void InsertTextBehindPosition(int pos, string text);
		void DeleteText(int pos, string text);
		void BackspaceText(int pos, string text);
		void SetTextSilently(string text);

		void BeginUpdate();
		void EndUpdate();

		void Insert(int position, string text);
		void Insert(string text);
		void Delete(int position, int length);
		void Delete(int length);
		void Select(int position, int length);
		void ReplaceSelection(string replaceWith);

		bool CanEdit { get; set; }
		bool CanUndo { get; set; }
		bool CanRedo { get; set; }

		event EventHandler<RawTextChangedArgs> RawTextChanged;
		event EventHandler<StoreTextInsertedArgs> StoreTextInserted;
		event EventHandler<StoreTextDeletedArgs> StoreTextDeleted;
		event EventHandler<StoreTextBackspacedArgs> StoreTextBackspaced;
		event EventHandler<StoreGroupBeginArgs> StoreGroupBegin;
		event EventHandler<StoreGroupEndArgs> StoreGroupEnd;
		event EventHandler<StoreUndoArgs> StoreUndo;
		event EventHandler<StoreUndoWordArgs> StoreUndoWord;
		event EventHandler<StoreUndoSentenceArgs> StoreUndoSentence;
		event EventHandler<StoreRedoArgs> StoreRedo;
		event EventHandler<PasteCharsAsChangesArgs> PasteCharsAsChanges;
		event EventHandler<UpdateIndicatorsArgs> UpdateIndicators;
	}
}
