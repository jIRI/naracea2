﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View.Events;
using System.Collections.ObjectModel;
using Naracea.Common;

namespace Naracea.View {
	public interface ITimelineView
		: ICanOpenClose,
			ICanBeBusy,
			IHasController,
			IHasControl {
		int CurrentChangeIndex { get; set; }
		IList<IUnitOfTime> AvailableGranularities { get; set; }
		bool ShowEmptyBars { get; }
		IUnitOfTime Granularity { get; }

		void ClearBars();
		void SetBars(IList<TimelineBar> bars);
		void AddBar(TimelineBar bar);
		void UpdateLastBar(TimelineBar bar);
	}
}
