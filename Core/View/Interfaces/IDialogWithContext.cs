﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View {
	public interface IDialogWithContext {
		void ShowDialog(object context);
	}
}
