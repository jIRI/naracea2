﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Collections.ObjectModel;

namespace Naracea.View {
	public interface IDocumentPropertiesView
		: ICanOpenClose, 
			IHasControl
	{
		string Name { get; set; }
		string Comment { get; set; }
		bool IsAutosaveEnabled { get; set; }
		bool ExportOnSaveNothing { get; set; }
		bool ExportOnSaveActiveBranch { get; set; }
		bool ExportOnSaveAllBranches { get; set; }
	}
}
