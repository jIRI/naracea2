﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View.Events;
using System.Collections.ObjectModel;
using Naracea.Common;

namespace Naracea.View {
	public enum ChangeStreamItemType {
		Insert,
		Delete,
		WithChange,
		WithChangeList,
		Other
	}

	public class ChangeStreamItem {
		public ChangeStreamItem(ChangeStreamItemType type, int id, string text) {
			this.Type = type;
			this.Id = id;
			this.Text = text;
		}

		public ChangeStreamItemType Type { get; private set; }
		public int Id { get; private set; }
		public string Text { get; private set; }
	}

	public interface IChangeStreamView
		: ICanOpenClose,
			ICanBeBusy,
			ICanUpdate,
			IHasController,
			IHasControl {

		int CurrentChangeIndex { get; set; }

		void Refresh();

		void AddChanges(IEnumerable<ChangeStreamItem> items);
		void AddChange(ChangeStreamItem item);
	}
}
