﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Collections.ObjectModel;

namespace Naracea.View {
	public interface IBranchPropertiesView
		: ICanOpenClose, 
			IHasControl
	{
		string Name { get; set; }
		string ExportFileName { get; set; }
		string Comment { get; set; }
		bool IsAlwaysExporting { get; set; }
		string Author { set; }
		string Parent { set; }
		DateTimeOffset BranchingDateTime { set; }
		IEnumerable<ExportFormatItem> ExportFormats { set; }
		ExportFormatItem ExportFormat { get; set; }
	}
}
