﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;

namespace Naracea.View {
	public interface IExportBranchDialog : IDialog, IFileSelectionDialogResult {
		string SuggestedFilename { get; set; }
		string Filter { get; set; }
		string Title { get; set; }
		string DefaultExtension { get; set; }
	}
}
