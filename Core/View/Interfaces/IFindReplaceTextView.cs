﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Collections.ObjectModel;

namespace Naracea.View {
	public enum SearchType {
		Text,
		History,
	}

	public enum SearchScope {
		CurrentBranch,
		AllActiveBranches,
		AllBranches
	}

	public class NamedSearchType {
		public SearchType Type { get; set; }
		public string Text { get; set; }
		public bool CanFindAll { get; set; }
		public bool CanFind { get; set; }
		public bool CanReplace { get; set; }
	}
	
	public class NamedSearchScope {
		public SearchScope Scope { get; set; }
		public string Text { get; set; }
	}

	public class ViewFindOptions {
		public bool ClearResultsOnFindAll { get; set; }
		public bool MatchCase { get; set; }
		public bool MatchWholeWords { get; set; }
		public bool UseRegex { get; set; }
		public string FindPattern { get; set; }
		public SearchType Type { get; set; }
		public SearchScope Scope { get; set; }
	}

	public interface IFindReplaceTextView
		: ICanOpenClose, 
			ICanHideShow, 
			ICanBeBusy, 
			IHasControl 
	{
		bool MatchWholeWords { get; }
		bool MatchCase { get; }
		SearchType Type { get; }
		SearchScope Scope { get; }
		ObservableCollection<ISearchMatchItem> Matches { get; set; }
		string FindPattern { get; }
		IEnumerable<NamedSearchType> AvailableSearchTypes { get; set; }
		IEnumerable<NamedSearchScope> AvailableSearchScopes { get; set; }

		ViewFindOptions GetFindOptions();

		void FocusResultList();
	}
}
