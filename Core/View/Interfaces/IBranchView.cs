﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Compounds;
using Naracea.View.Events;
using Naracea.View.Components;

namespace Naracea.View {
	public interface IBranchView 
		: ICanOpenClose,
			ICanBeBusy,
			IHasIndicators,
			ICanBeFocused,
			IHasController, 
			IHasControl, 
			IHasName, 
			IHasDirtyNotification 
	{
		bool CanRewind { get; set; }
		bool CanReplay { get; set; }
		bool CanBeDeleted { get; set; }

		void SetClipboard(string text);
		void SetTimelineHeight(double height);

		ITextEditorView Editor { get; set; }
		ITimelineView Timeline { get; set; }
		IChangeStreamView ChangeStream { get; set; }

		bool IsTimelineVisibleOnly { get; set; }
		bool IsTimelineAndChangeStreamVisible { get; set; }
		bool IsChangeStreamVisibleOnly { get; set; }
	}
}
