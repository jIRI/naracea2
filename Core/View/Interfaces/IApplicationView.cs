﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Naracea.View.Events;
using Naracea.View.Compounds;
using Naracea.View;
using System.Windows.Controls.Ribbon;

namespace Naracea.View {
	public enum ApplicationRibbonTabIds {
		View,
		Document,
	}

	public interface IApplicationView
		: ICanOpenClose, 
			ICanHideShow, 
			ICanBeBusy, 
			IHasIndicators,
			IHasController, 
			IHasControl,
			ICanBeFocused
	{
		IEnumerable<IDocumentView> Documents { get; }
		IHasIndicators DocumentStatusBar { get; }
		IHasIndicators ProgressIndicators { get; }
		IList<MruItem> MruList { get; set; }
		IList<SpellcheckDictionaryItem> SpellcheckInstalledLanguages { get; set; }
		IEnumerable<ExportFormatItem> ExportFormats { get; set; }
		IFindReplaceTextView FindReplaceView { get; set; }
		bool AreFileOperationsLocked { get; set; }

		void AddRibbonTab(RibbonTab tab);
		void AddRibbonGroup(ApplicationRibbonTabIds tab, RibbonGroup group);
		void FinalizePluginUiUpdate();
		void RegisterControl(string name, System.Windows.Controls.Control control);
		void UnregisterControl(string name);

		void ExecAsync(Action action);

		void AddDocument(IDocumentView documentView);
		void RemoveDocument(IDocumentView documentView);
		void ActivateDocument(IDocumentView documentView);
		void UpdateDocument(IDocumentView documentView);
		IDocumentView ActiveDocument { get; }

		void RegisterChildWindow(Window window);
	}
}
