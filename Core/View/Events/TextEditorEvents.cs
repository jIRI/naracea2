﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.View.Events {
	public abstract class TextEditorArgsBase : EventArgs {
		public ITextEditorView TextEditorView { get; private set; }
		//public ITextEditorController TextEditorController { get; private set; }

		public TextEditorArgsBase(ITextEditorView textEditorView) {
			this.TextEditorView = textEditorView;
			//this.TextEditorController = textEditorView.Controller as ITextEditorController;
		}
	}

	public abstract class TextChangedArgsBase : TextEditorArgsBase {
		public int Position { get; private set; }
		public string Text { get; private set; }

		public TextChangedArgsBase(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView) {
			Debug.Assert(position >= 0);
			this.Position = position;
			Debug.Assert(text != null);
			this.Text = text;
		}
	}

	public class RawTextChangedArgs : TextEditorArgsBase {
		public int Position { get; private set; }
		public int AddedLength { get; private set; }
		public int RemovedLength { get; private set; }

		public RawTextChangedArgs(ITextEditorView textEditorView, int pos, int added, int removed)
			: base(textEditorView) {
			this.Position = pos;
			this.AddedLength = added;
			this.RemovedLength = removed;
		}
	}

	public class StoreGroupBeginArgs : TextEditorArgsBase {
		public StoreGroupBeginArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreGroupEndArgs : TextEditorArgsBase {
		public StoreGroupEndArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreTextInsertedArgs : TextChangedArgsBase {
		public StoreTextInsertedArgs(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreTextDeletedArgs : TextChangedArgsBase {
		public StoreTextDeletedArgs(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreTextBackspacedArgs : TextChangedArgsBase {
		public StoreTextBackspacedArgs(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class StoreUndoArgs : TextEditorArgsBase {
		public StoreUndoArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreUndoWordArgs : TextEditorArgsBase {
		public StoreUndoWordArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreUndoSentenceArgs : TextEditorArgsBase {
		public StoreUndoSentenceArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class StoreRedoArgs : TextEditorArgsBase {
		public StoreRedoArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}

	public class PasteCharsAsChangesArgs : TextChangedArgsBase {
		public PasteCharsAsChangesArgs(ITextEditorView textEditorView, int position, string text)
			: base(textEditorView, position, text) {
		}
	}

	public class UpdateIndicatorsArgs : TextEditorArgsBase {
		public UpdateIndicatorsArgs(ITextEditorView textEditorView)
			: base(textEditorView) {
		}
	}
}
