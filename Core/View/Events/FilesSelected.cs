﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View.Events {
	public class FilesSelectedEventArgs : EventArgs {
		public IEnumerable<string> Filenames { get; private set; }

		public FilesSelectedEventArgs(IEnumerable<string> fileNames) {
			this.Filenames = fileNames.ToArray();
		}
	}
}
