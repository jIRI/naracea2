﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View.Events {
	public class FileSelectedEventArgs : EventArgs {
		public string Filename { get; private set; }
		public object Context { get; private set; }
		public FileSelectedEventArgs(string fileName) {
			this.Filename = fileName;
		}

		public FileSelectedEventArgs(string fileName, object context)
			: this(fileName) {
			this.Context = context;
		}
	}
}
