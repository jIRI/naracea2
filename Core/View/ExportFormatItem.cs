﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Naracea.View {
	public class ExportFormatItem {
		public string Id { get; set; }
		public string DisplayName { get; set; }
		public string Description { get; set; }
		public System.Windows.Media.ImageSource ImageLarge { get; set; }
		public System.Windows.Media.ImageSource ImageSmall { get; set; }
	}
}
