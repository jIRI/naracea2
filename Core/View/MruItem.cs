﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View {
	public class MruItem {
		public string FileName { get; set; }

		public MruItem() {
		}

		public MruItem(string fileName)
			: this() {
			this.FileName = fileName;
		}
	}
}
