﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View {
	public class TimelineBar {
		public DateTimeOffset From { get; private set; }
		public DateTimeOffset To { get; private set; }
		public int ColumnSpan { get; private set; }
		public IList<int> Ids { get; private set; }
		public TimelineBar(DateTimeOffset from, DateTimeOffset to, IList<int> ids)
			: this(from, to, 1, ids) {
		}

		public TimelineBar(DateTimeOffset from, DateTimeOffset to, int span, IList<int> ids) {
			this.From = from;
			this.To = to;
			this.ColumnSpan = span;
			this.Ids = ids;
		}
	}
}
