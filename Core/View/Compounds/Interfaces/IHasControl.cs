﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View.Compounds {
	public interface IHasControl {
		object Control { get; }
		void Exec(Action action);
	}
}
