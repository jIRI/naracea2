﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Events;

namespace Naracea.View.Compounds {
	public interface IFilesSelectionDialogResult {
		event EventHandler<FilesSelectedEventArgs> FilesSelected;
	}
}
