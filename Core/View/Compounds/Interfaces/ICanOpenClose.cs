﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common;
using Naracea.View.Events;

namespace Naracea.View.Compounds {
	public interface ICanOpenClose {
		void Open(ISettings settings);
		void Close();
	}
}
