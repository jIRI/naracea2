﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.View.Compounds {
	public interface IHasController {
		object Controller { get; set; }
	}
}
