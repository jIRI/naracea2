﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View.Components;

namespace Naracea.View.Compounds {
	public interface IHasIndicators {
		void RegisterIndicator(IIndicator indicator);
		void UnregisterIndicator(IIndicator indicator);
	}
}
