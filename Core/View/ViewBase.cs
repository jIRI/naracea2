﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
using Naracea.View;
using MemBus;

namespace Naracea.View {
	public abstract class ViewBase : IDisposable {
		List<IDisposable> _disposables = new List<IDisposable>();
		public IBus Bus {get; private set;}
#if DEBUG
		StackTrace _stack;
		static int _ctorCount = 0;
		static int _disposeCount = 0;
		static int _finalizeCount = 0;
#endif
		
		public ViewBase(IBus bus) {
			Debug.Assert(bus != null);
			this.Bus = bus;
#if DEBUG
			_stack = new StackTrace(true);
			Debug.WriteLine("Creating view count={0}", _ctorCount++);
#endif
		}

		protected void RegisterForDisposal(IDisposable subscription) {
			_disposables.Add(subscription);
		}

		protected void DisposeDisposables() {
			foreach( var subscription in _disposables ) {
				subscription.Dispose();
			}
			_disposables.Clear();
		}

		#region IDisposable Members
		~ViewBase() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
#if DEBUG
			Debug.WriteLine("Finalizing view count={0}", _finalizeCount++);
			Debug.WriteLine(_stack.ToString());
#endif
			Debug.Assert(false, this.GetType().FullName);
		}

		protected bool _disposed = false;
		public void Dispose() {
#if DEBUG
			Debug.WriteLine("Disposing view count={0}", _disposeCount++);
#endif
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
					this.DisposeDisposables();
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
