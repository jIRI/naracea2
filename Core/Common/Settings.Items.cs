﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	public partial class Settings {
		#region Settings file location
		public static readonly string SettingsFile = "Naracea.Settings";
		#endregion

		#region Search and replace constants
		public const int MaximumLengthOfFindReplaceHistory = 16;
		#endregion

		#region Settings file tokens
		public static readonly string TimelineGranularityIndex = "Settings.Timeline.Granularity.Index";
		public static readonly string TimelineShowEmptyBars = "Settings.Timeline.ShowEmptyBars";
		public static readonly string TimelineShowTimelineOnly = "Settings.Timeline.ShowTimelineOnly";
		public static readonly string TimelineShowTimelineAndChangeStream = "Settings.Timeline.ShowTimelineAndChangeStream";
		public static readonly string TimelineShowChangeStreamOnly = "Settings.Timeline.ShowChangeStreamOnly";
		public static readonly string ViewMainQatItems = "Settings.View.Qat.Items";
		public static readonly string ViewWindowState = "Settings.View.Window.State";
		public static readonly string ViewWindowRect = "Settings.View.Window.Rect";
		public static readonly string ViewTextEditorFontFamilyName = "Settings.View.TextEditor.Font.FamilyName";
		public static readonly string ViewTextEditorFontSize = "Settings.View.TextEditor.Font.Size";
		public static readonly string ViewTextEditorWrapsLines = "Settings.View.TextEditor.WrapsLines";
		public static readonly string ViewTextEditorShowWhitespaces = "Settings.View.TextEditor.ShowWhitespaces";
		public static readonly string ViewTextEditorTabSize = "Settings.View.TextEditor.TabSize";
		public static readonly string ViewTextEditorKeepTabs = "Settings.View.TextEditor.KeepTabs";
		public static readonly string ViewTextEditorSyntaxHighlighting = "Settings.View.TextEditor.SyntaxHighlighting";
		public static readonly string ViewForegroundColor = "Settings.View.Color.Text";
		public static readonly string ViewBackgroundColor = "Settings.View.Color.Background";
		public static readonly string ViewDocumentContainerWidth = "Settings.View.DocumentContainer.Width";
		public static readonly string ViewBranchTimelineHeight = "Settings.View.Branch.Timeline.Height";
		public static readonly string MruList = "Settings.MruList";
		public static readonly string SearchType = "Settings.SearchAndReplace.SearchType";
		public static readonly string SearchScope = "Settings.SearchAndReplace.SearchScope";
		public static readonly string SearchClearResultsBeforeFindAll = "Settings.SearchAndReplace.ClearResultsBeforeFindAll";
		public static readonly string SearchMatchCase = "Settings.SearchAndReplace.MatchCase";
		public static readonly string SearchMatchWholeWords = "Settings.SearchAndReplace.MatchWholeWords";
		public static readonly string SearchUseRegularExpressions = "Settings.SearchAndReplace.UseRegEx";
		public static readonly string SearchSearchItems = "Settings.SearchAndReplace.SearchItems";
		public static readonly string SearchReplaceItems = "Settings.SearchAndReplace.ReplaceItems";
		public static readonly string SearchHistoryLength = "Settings.SearchAndReplace.HistoryLength";
		public static readonly string DocumentSavingAutosaveEnabled = "Settings.Document.Autosave.Enabled";
		public static readonly string DocumentSavingAutosavePeriod = "Settings.Document.Autosave.Period";
		public static readonly string DocumentSavingExportOnSaveNothing = "Settings.Document.ExportOnSave.Nothing";
		public static readonly string DocumentSavingExportOnSaveActiveBranch = "Settings.Document.ExportOnSave.ActiveBranch";
		public static readonly string DocumentSavingExportOnSaveAllTouchedBranches = "Settings.Document.ExportOnSave.AllTouchedBranches";
		public static readonly string BranchAutoExportFilename = "Settings.Branch.AutoExport.Filename";
		public static readonly string BranchAutoExportExtension = "Settings.Branch.AutoExport.Extension";
		public static readonly string BranchAutoExportFormat = "Settings.Branch.AutoExport.Format";
		public static readonly string BranchAutoExportAlwaysExport = "Settings.Branch.AutoExport.AlwaysExport";
		public static readonly string BranchVisualOrder = "Settings.Branch.VisualOrder";
		public static readonly string SpellcheckInstalledLanguages = "Settings.Spellcheck.InstalledLanguages";
		public static readonly string SpellcheckLanguage = "Settings.Spellcheck.Language";
		#endregion


		public static void ApplyDefaults(ISettings settings) {
			//timeline
#if DEBUG
			settings.Set(Settings.TimelineGranularityIndex, 4);
#else
			settings.Set(Settings.TimelineGranularityIndex, 3);
#endif
			settings.Set(Settings.TimelineShowEmptyBars, false);
			settings.Set(Settings.TimelineShowTimelineOnly, false);
			settings.Set(Settings.TimelineShowTimelineAndChangeStream, true);
			settings.Set(Settings.TimelineShowChangeStreamOnly, false);
			//by default, do autosave every 3 minutes
			settings.Set(Settings.DocumentSavingAutosaveEnabled, true);
			settings.Set(Settings.DocumentSavingAutosavePeriod, 3); 
			//by default, at least until we have stable version, export at least active branch
			settings.Set(Settings.DocumentSavingExportOnSaveNothing, false);
			settings.Set(Settings.DocumentSavingExportOnSaveActiveBranch, false);
			settings.Set(Settings.DocumentSavingExportOnSaveAllTouchedBranches, false);
			//various text editor settings
			settings.Set(Settings.ViewTextEditorWrapsLines, true);
			settings.Set(Settings.ViewTextEditorShowWhitespaces, false);
			settings.Set(Settings.ViewTextEditorTabSize, 4);
			settings.Set(Settings.ViewTextEditorKeepTabs, true);
			//search & replace
			settings.Set(Settings.SearchHistoryLength, Settings.MaximumLengthOfFindReplaceHistory);
			// default exporter is plain text
			settings.Set(Settings.BranchAutoExportFormat, "TXT");
		}		
	}
}
