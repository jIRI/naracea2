﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Common {
	public static class SmartFileNameShortener {
		// This method is taken from Joe Woodbury's article at: http://www.codeproject.com/KB/cs/mrutoolstripmenu.aspx
		/// <summary>
		/// Shortens a pathname for display purposes.
		/// </summary>
		/// <param labelName="pathname">The pathname to shorten.</param>
		/// <param labelName="maxLength">The maximum number of characters to be displayed.</param>
		/// <remarks>Shortens a pathname by either removing consecutive components of a path
		/// and/or by removing characters from the end of the filename and replacing
		/// then with three elipses (...)
		/// <para>In all cases, the root of the passed path will be preserved in it's entirety.</para>
		/// <para>If a UNC path is used or the pathname and maxLength are particularly short,
		/// the resulting path may be longer than maxLength.</para>
		/// <para>This method expects fully resolved pathnames to be passed to it.
		/// (Use Path.GetFullPath() to obtain this.)</para>
		/// </remarks>
		/// <returns></returns>
		public static string Shorten(string source, int lenHint) {
			if( source.Length <= lenHint )
				return source;

			string root = Path.GetPathRoot(source);
			if( root.Length > 3 )
				root += Path.DirectorySeparatorChar;

			string[] elements = source.Substring(root.Length).Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

			int filenameIndex = elements.GetLength(0) - 1;

			if( elements.GetLength(0) == 1 ) // pathname is just a root and filename
			{
				if( elements[0].Length > 5 ) // long enough to shorten
				{
					// if path is a UNC path, root may be rather long
					if( root.Length + 6 >= lenHint ) {
						return root + elements[0].Substring(0, 3) + "...";
					} else {
						return source.Substring(0, lenHint - 3) + "...";
					}
				}
			} else if( ( root.Length + 4 + elements[filenameIndex].Length ) > lenHint ) // pathname is just a root and filename
			{
				root += "..." + Path.DirectorySeparatorChar;

				int len = elements[filenameIndex].Length;
				if( len < 6 )
					return root + elements[filenameIndex];

				if( ( root.Length + 6 ) >= lenHint ) {
					len = 3;
				} else {
					len = lenHint - root.Length - 3;
				}
				return root + elements[filenameIndex].Substring(0, len) + "...";
			} else if( elements.GetLength(0) == 2 ) {
				return root + "..." + Path.DirectorySeparatorChar.ToString() + elements[1];
			} else {
				int len = 0;
				int begin = 0;

				for( int i = 0; i < filenameIndex; i++ ) {
					if( elements[i].Length > len ) {
						begin = i;
						len = elements[i].Length;
					}
				}

				int totalLength = source.Length - len + 3;
				int end = begin + 1;

				while( totalLength > lenHint ) {
					if( begin > 0 )
						totalLength -= elements[--begin].Length - 1;

					if( totalLength <= lenHint )
						break;

					if( end < filenameIndex )
						totalLength -= elements[++end].Length - 1;

					if( begin == 0 && end == filenameIndex )
						break;
				}

				// assemble final string

				for( int i = 0; i < begin; i++ ) {
					root += elements[i] + Path.DirectorySeparatorChar;
				}

				root += "..." + Path.DirectorySeparatorChar;

				for( int i = end; i < filenameIndex; i++ ) {
					root += elements[i] + Path.DirectorySeparatorChar;
				}

				return root + elements[filenameIndex];
			}
			return source;
		}
	}
}
