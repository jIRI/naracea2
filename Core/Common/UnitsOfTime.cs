﻿using System;
using System.Globalization;

namespace Naracea.Common
{
    public abstract class UnitOfTime : IUnitOfTime
    {
        public UnitOfTime()
        {
            this.Count = 1;
        }

        public int Count { get; set; }
        public abstract DateTimeOffset Trim(DateTimeOffset dateTime);
        public abstract DateTimeOffset Advance(DateTimeOffset dateTime);
    }

    public class Second : UnitOfTime
    {
        public Second()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, TimeSpan.Zero);
            return result;
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddSeconds(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Second : string.Format(Naracea.Core.Properties.Resources.Seconds, this.Count);
        }
    }

    public class Minute : UnitOfTime
    {
        public Minute()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, 0, TimeSpan.Zero);
            return result;
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddMinutes(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Minute : string.Format(Naracea.Core.Properties.Resources.Minutes, this.Count);
        }
    }

    public class Hour : UnitOfTime
    {
        public Hour()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, 0, 0, TimeSpan.Zero);
            return result;
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddHours(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Hour : string.Format(Naracea.Core.Properties.Resources.Hours, this.Count);
        }
    }


    public class Day : UnitOfTime
    {
        public Day()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0, TimeSpan.Zero);
            return result;
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddDays(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Day : string.Format(Naracea.Core.Properties.Resources.Days, this.Count);
        }
    }


    public class Week : UnitOfTime
    {
        public Week()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            int week = culture.Calendar.GetWeekOfYear(new DateTime(dateTime.Ticks), CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            var result = new DateTime(dateTime.Year, 1, 1);
            if (week > 1)
            {
                //for weeks after very first week of the year we need some corrections
                result = culture.Calendar.AddWeeks(result, week - 1);
                int inWeekDayCorrection = 0;
                switch (culture.Calendar.GetDayOfWeek(result))
                {
                    case DayOfWeek.Monday:
                        //mondays are ok, nice days :-)
                        inWeekDayCorrection = 0;
                        break;
                    case DayOfWeek.Tuesday:
                        inWeekDayCorrection = 1;
                        break;
                    case DayOfWeek.Wednesday:
                        inWeekDayCorrection = 2;
                        break;
                    case DayOfWeek.Thursday:
                        inWeekDayCorrection = 3;
                        break;
                    case DayOfWeek.Friday:
                        inWeekDayCorrection = 4;
                        break;
                    case DayOfWeek.Saturday:
                        inWeekDayCorrection = 5;
                        break;
                    case DayOfWeek.Sunday:
                        inWeekDayCorrection = 6;
                        break;
                }
                //now we align the date to monday, so it looks nice
                result = result.AddDays(-inWeekDayCorrection);
            }
            return new DateTimeOffset(result.ToUniversalTime());
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            return new DateTimeOffset(culture.Calendar.AddWeeks(new DateTime(dateTime.Ticks).ToUniversalTime(), this.Count));
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Week : string.Format(Naracea.Core.Properties.Resources.Week, this.Count);
        }
    }

    public class Month : UnitOfTime
    {
        public Month()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTime(dateTime.Year, dateTime.Month, 1);
            return new DateTimeOffset(result);
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddMonths(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Month : string.Format(Naracea.Core.Properties.Resources.Months, this.Count);
        }
    }

    public class Year : UnitOfTime
    {
        public Year()
        {
        }

        public override DateTimeOffset Trim(DateTimeOffset dateTime)
        {
            var result = new DateTime(dateTime.Year, 1, 1);
            return new DateTimeOffset(result);
        }

        public override DateTimeOffset Advance(DateTimeOffset dateTime)
        {
            return dateTime.AddYears(this.Count);
        }

        public override string ToString()
        {
            return this.Count == 1 ? Naracea.Core.Properties.Resources.Year : string.Format(Naracea.Core.Properties.Resources.Years, this.Count);
        }
    }


}
