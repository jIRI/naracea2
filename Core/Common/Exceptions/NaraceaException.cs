﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common.Exceptions {
	public class NaraceaException : Exception {
		public NaraceaException() {
		}

		public NaraceaException(string message) 
		: base(message) {			
		}

		public NaraceaException(string message, Exception innerException) 
		: base(message, innerException) {
		}
	}
}
