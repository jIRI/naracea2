﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	static public class Constants {
		public static readonly string AppDirectory = "Naracea";
		public static readonly string PluginDataDirectory = "Plugins";
		public static readonly string PythonLibraryDirectory = @"IronPython.StandardLibrary\Lib";
		public readonly static string NewLine = "\n";
	}
}
