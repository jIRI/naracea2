﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Common.Extensions {
	public static class CastingExtensions {
		public static T As<T>(this object o) where T : class {			
			Debug.Assert(o is T);
			return o as T;
		}

		public static bool TryAs<T>(this object o, out T result) where T : class {
			result = null;
			if( !(o is T) ) {
				return false;
			}

			result = o as T;
			return true;
		}
	}
}
