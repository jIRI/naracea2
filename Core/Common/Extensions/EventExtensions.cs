﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Common.Extensions {
	public static class EventExtensions {
		[DebuggerStepThrough]
		public static void Raise<T>(this EventHandler<T> handler, object sender, T args) where T: EventArgs {
			//this is here because of concurrent event handlers disposal...
			var tempHandler = handler;
			if( tempHandler != null ) {
				tempHandler(sender, args);
			}
		}

		[DebuggerStepThrough]
		public static void Raise(this EventHandler handler, object sender, EventArgs args) {
			//this is here because of concurrent event handlers disposal...
			var tempHandler = handler;
			if( tempHandler != null ) {
				tempHandler(sender, args);
			}
		}
	}
}
