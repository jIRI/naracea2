﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Naracea.Common.Extensions {
	public static class BackgroundWorkerExtensions {
		[DebuggerStepThrough]
		public static void RunWorkerAsyncIfNotBusy(this BackgroundWorker worker) {
			if( !worker.IsBusy ) {
				worker.RunWorkerAsync();
			}
		}

		[DebuggerStepThrough]
		public static void RunWorkerAsyncIfNotBusy(this BackgroundWorker worker, object context) {
			if( !worker.IsBusy ) {
				worker.RunWorkerAsync(context);
			}
		}
	}
}
