﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common.Extensions {
	public static class BaseTypesExtensions {
		public static int EnsureInRange(this int index, int lower, int upper) {
			int result = index;
			if( result < lower ) {
				result = lower;
			} else if( result > upper) {
				result = upper;
			}
			return result;
		}

		public static string NormalizeEols(this string text) {
			if( text == null ) {
				return null;
			}
			return text.Replace("\r", "").Replace("\n", Constants.NewLine);
		}

		public static bool IsNull<T>(this T o) where T : class {
			return o == null;
		}

		public static bool IsNotNull<T>(this T o) where T : class {
			return o != null;
		}
	}
}
