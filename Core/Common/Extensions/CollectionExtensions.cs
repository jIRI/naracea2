﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Naracea.Common.Extensions {
	public static class CollectionExtensions {
		public static void SetFrom<T>(this ObservableCollection<T> self, IEnumerable<T> source) {
			self.Clear();
			foreach( var item in source ) {
				self.Add(item);
			}
		}

		public static bool IsEqualTo<T>(this IList<T> self, IList<T> other) where T : class {
			if( self.Count != other.Count ) {
				return false;
			}

			int count = self.Count;
			for( int i = 0; i < count; i++ ) {
				if( !self[i].Equals(other[i]) ) {
					return false;
				}
			}

			return true;
		}
	}
}
