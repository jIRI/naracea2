﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	public interface ISettings {
		event EventHandler SettingsChanged;

		bool HasValue(string id);
		void Clear(string id);
		T Get<T>(string id);
		void Set<T>(string id, T value);
		IDictionary<string, string> Data { get; }
		ISettings Parent { get; set; }
	}
}
