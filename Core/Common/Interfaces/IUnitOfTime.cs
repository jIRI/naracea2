﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	public interface IUnitOfTime {
		int Count { get; set; }
		DateTimeOffset Trim(DateTimeOffset dateTime);
		DateTimeOffset Advance(DateTimeOffset dateTime);
	}
}
