﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	public interface IModule {
		void Configure(Autofac.ContainerBuilder builder);
	}
}
