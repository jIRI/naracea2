﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Common {
	public interface IHasDateTime {
		DateTimeOffset DateTime { get; }
	}
}
