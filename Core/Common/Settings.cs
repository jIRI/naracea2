﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using Naracea.Common.Extensions;

namespace Naracea.Common {
	public partial class Settings : ISettings {
		public Settings(ISettings parent, IDictionary<string, string> container) {
			this.Parent = parent;
			this.Data = container;
		}

		public Settings()
			: this(null, new Dictionary<string, string>()) {
			Settings.ApplyDefaults(this);
		}

		public event EventHandler SettingsChanged;
		public IDictionary<string, string> Data { get; private set; }
		public ISettings Parent { get; set; }

		public bool HasValue(string id) {
			if( this.Data.ContainsKey(id) ) {
				return true;
			}

			if( this.Parent != null ) {
				return this.Parent.HasValue(id);
			}

			return false;
		}

		public void Clear(string id) {
			if( this.Data.ContainsKey(id) ) {
				this.Data.Remove(id);
			} else if( this.Parent != null ) {
				this.Parent.Clear(id);
			}
			this.SettingsChanged.Raise(this, new EventArgs());
		}

		public T Get<T>(string id) {
			if( this.Data.ContainsKey(id) ) {
				try {
					var converter = TypeDescriptor.GetConverter(typeof(T));
					if( converter != null && converter.CanConvertFrom(typeof(string)) ) {
						return (T)( converter.ConvertFromInvariantString(this.Data[id]) );
					} else {
						var serializer = new XmlSerializer(typeof(T));
						using( var memStream = new MemoryStream(UnicodeEncoding.Default.GetBytes(this.Data[id])) ) {
							try {
								var result = (T)serializer.Deserialize(memStream);
								return result;
							} catch {
								//well, we failed
								return default(T);
							}
						}
					}
				} catch( NotSupportedException ) {
					return default(T);
				}
			} else if( this.Parent != null ) {
				return this.Parent.Get<T>(id);
			}
			return default(T);
		}

		public void Set<T>(string id, T value) {
			var converter = TypeDescriptor.GetConverter(value);
			if( converter != null && converter.CanConvertFrom(typeof(string)) ) {
				this.Data[id] = converter.ConvertToInvariantString(value);
			} else {
				var serializer = new XmlSerializer(typeof(T));
				using( var memStream = new MemoryStream() ) {
					serializer.Serialize(memStream, value);
					this.Data[id] = UnicodeEncoding.Default.GetString(memStream.ToArray());
				}
			}
			this.SettingsChanged.Raise(this, new EventArgs());
		}
	}
}
