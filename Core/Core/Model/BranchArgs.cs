﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public class BranchArgs : IBranchArgs {
		// This is needed because Func<> doesn't have enough params
		public delegate IBranchArgs BranchArgsFactory(IAuthor author, IBranch parent, int branchingChangeIndex, DateTimeOffset branchingDateTime);

		public BranchArgs(IAuthor author, IBranch parent, int branchingChangeIndex, DateTimeOffset branchingDateTime) {
			this.Author = author;
			this.Parent = parent;
			this.BranchingChangeIndex = branchingChangeIndex;
			this.BranchingDateTime = branchingDateTime;
		}

		public IAuthor Author { get; private set; }
		public IBranch Parent { get; private set; }
		public int BranchingChangeIndex { get; private set; }
		public DateTimeOffset BranchingDateTime { get; private set; }
	}
}
