﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Extensions;

namespace Naracea.Core.Model {
	internal class Document : IDocument {
		public static readonly string BranchCounterKey = "Document.BranchCounter";

		public Document() {
			this.Name = Properties.Resources.DefaultDocumentName;
			this.Comment = string.Empty;
			this.Branches = new List<IBranch>();
			this.Authors = new List<IAuthor>();
			this.Metadata = new Metadata();
		}

		#region IDocument Members
		public string Name { get; set; }
		public string Comment { get; set; }
		public IList<IBranch> Branches { get; private set; }
		public IList<IAuthor> Authors { get; private set; }
		public IBranch CurrentBranch { get; set; }
		public IMetadata Metadata { get; private set; }

		public int GetNextBranchId() {
			//try get counter
			var counterString = this.Metadata.Get(Document.BranchCounterKey);
			if( string.IsNullOrEmpty(counterString) ) {
				//no counter, set it to number of branches
				counterString = (this.Branches.Count + 1).ToString();
				this.Metadata.Set(Document.BranchCounterKey, counterString);
			}
			//parse counter
			var currentCounter = int.Parse(counterString);
			//update counter
			this.Metadata.Set(Document.BranchCounterKey, (currentCounter + 1).ToString());
			//return counter
			return currentCounter;
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as Document;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}	 
			return this.Name.Equals(o.Name)
				&& this.Comment.Equals(o.Comment)
				&& (this.CurrentBranch != null ? this.CurrentBranch.Equals(o.CurrentBranch) : o.CurrentBranch == null)
				&& this.Authors.IsEqualTo(o.Authors)
				&& this.Branches.IsEqualTo(o.Branches)
			;
		}

		public override int GetHashCode() {
			return this.Name.GetHashCode()
				^ this.Comment.GetHashCode()
				^ this.Branches.GetHashCode()
				^ this.Authors.GetHashCode()
				^ this.CurrentBranch.GetHashCode()
			;
		}
		#endregion 
	}
}
