﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IRedo {
		/// <summary>
		/// Change which was redone.
		/// </summary>
		IChange RedoneChange { get; }
	}
}
