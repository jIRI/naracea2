﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IChangeWithChange : IChange {
		IChange Change { get; }
	}
}
