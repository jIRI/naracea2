﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common;

namespace Naracea.Core.Model.Changes {
	public interface ICanSetShiftCount {
		void SetShiftCount(int count);
	}
}

