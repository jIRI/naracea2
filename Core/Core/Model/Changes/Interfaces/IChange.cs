﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common;

namespace Naracea.Core.Model.Changes {
	public interface IChange : IHasDateTime {
		int Id { get; set; }
		IAuthor Author { get; }
		int ShiftCount { get; }
		string VisualText { get; }
		bool ShouldShifterExecuteFollowingChange { get; }

		void ExecuteDo(ITextEditor editor);
		void ExecuteUndo(ITextEditor editor);

		void OnStoring(ITextEditor editor);
	}
}

