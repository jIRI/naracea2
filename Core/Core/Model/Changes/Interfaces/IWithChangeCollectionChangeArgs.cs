﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IWithChangeCollectionChangeArgs : IChangeArgs {
		IEnumerable<IChange> Changes { get; }
	}
}
