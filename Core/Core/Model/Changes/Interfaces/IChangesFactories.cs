﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Textacle.Core.Entities.Changes;

namespace Textacle.Core.Entities.Changes {
	public interface IChangesFactories {
		InsertTextFactory InsertText { get; }
		DeleteTextFactory DeleteText { get; }
		BackspaceTextFactory BackspaceText { get; }
		UndoFactory Undo { get; }
		RedoFactory Redo { get; }
	}
}
