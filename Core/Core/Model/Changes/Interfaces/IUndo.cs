﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IUndo {
		/// <summary>
		/// Returns change which was undoen, or for multi undo it returns first change (lowest index) which was undone.
		/// Basically, change preceding this change should be next undoable change (if not undo, of course).
		/// </summary>
		IChange UndoneChange { get; }
	}
}
