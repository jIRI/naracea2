﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IChangeArgs {
		IAuthor Author { get; }
		DateTimeOffset DateTime { get; }
	}
}
