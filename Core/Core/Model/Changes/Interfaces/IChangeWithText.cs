﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public interface IChangeWithText : IChange {
		string Text { get; }
		int Position { get; }
		int AddedTextLength { get; }
	}
}
