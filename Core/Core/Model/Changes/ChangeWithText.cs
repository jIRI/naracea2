﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal abstract class ChangeWithText : Change, IChangeWithText {
		#region ctor
		public ChangeWithText(IWithTextChangeArgs args)
			: base(args) {
			Debug.Assert(args.Text != null);

			this.Position = args.Position;
			this.Text = args.Text;
		}
		#endregion

		#region IChange Members
		public override string VisualText { get { return this.Text; } }
		#endregion

		#region IWithText Members
		public string Text { get; private set; }
		public int Position { get; private set; }
		public abstract int AddedTextLength { get; }

		public override void OnStoring(ITextEditor editor) {
			//Do nothing. Text changes are performed already, so it doesn't need to call Do()
		}
		#endregion

		#region Private methods
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as ChangeWithText;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj)
				&& (this.Text != null ? this.Text.Equals(o.Text) : o.Text == null)
				&& this.Position.Equals(o.Position);
		}

		public override int GetHashCode() {
			return base.GetHashCode()
				^ (this.Text != null ? this.Text.GetHashCode() : 0)
				^ this.Position.GetHashCode()
			;
		}

		public override string ToString() {
			return string.Format("{0}:{1}", this.Position, this.Text);
		}
		#endregion
	}
}
