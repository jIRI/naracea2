﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal class ChangeWithChange : Change, IChangeWithChange {
		#region ctor
		public ChangeWithChange(IWithChangeChangeArgs args)
			: base(args) {
			this.Change = args.Change;
		}
		#endregion

		#region IWithText Members
		public IChange Change { get; private set; }

		public override void OnStoring(ITextEditor editor) {
			// this kind of changes (like undo or redo) generally needs to be done before we can store it.
			// if not, change won't be performed in text editor (we e.g. handle undo/redo by ourselves here)
			this.ExecuteDo(editor);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as ChangeWithChange;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj)
				&& this.Change.Equals(o.Change)
			;
		}

		public override int GetHashCode() {
			return base.GetHashCode()
				^ this.Change.GetHashCode();
		}
		#endregion
	}
}
