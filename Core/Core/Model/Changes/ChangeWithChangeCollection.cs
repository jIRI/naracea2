﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Common.Extensions;

namespace Naracea.Core.Model.Changes {
	internal class ChangeWithChangeCollection : Change, IChangeWithChangeCollection {
		#region ctor
		public ChangeWithChangeCollection(IWithChangeCollectionChangeArgs args)
			: base(args) {
			this.Changes = args.Changes;
		}
		#endregion

		#region IChangeWithCount Members
		public IEnumerable<IChange> Changes { get; private set; }

		public override void OnStoring(ITextEditor editor) {
			// this kind of changes (like multiundo) generally needs to be done before we can store it.
			// if not, change won't be performed in text editor (we e.g. handle multiundo by ourselves here)
			this.ExecuteDo(editor);
		}
		#endregion

		#region Protected methods
		protected void ExecuteDoOnAllChanges(ITextEditor editor) {
			int skipCount = 0;
			bool execFollowingChange = false;
			foreach( var change in this.Changes ) {
				if( skipCount <= 0 ) {
					if( execFollowingChange ) {
						skipCount = 1;
						execFollowingChange = false;
					} else {
						skipCount = change.ShiftCount;
						execFollowingChange = change.ShouldShifterExecuteFollowingChange;
					}
					change.ExecuteDo(editor);
				}
				skipCount--;
			}
		}

		protected void ExecuteUndoOnAllChanges(ITextEditor editor) {
			int skipCount = 0;
			bool execFollowingChange = false;
			foreach( var change in this.Changes.Reverse() ) {
				if( skipCount <= 0 ) {
					if( execFollowingChange ) {
						skipCount = 1;
						execFollowingChange = false;
					} else {
						skipCount = change.ShiftCount;
						execFollowingChange = change.ShouldShifterExecuteFollowingChange;
					}
					change.ExecuteUndo(editor);
				}
				skipCount--;
			}
		}

		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as ChangeWithChangeCollection;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			var listSelf = new List<IChange>(this.Changes);
			return base.Equals(obj)
				&& listSelf.IsEqualTo(new List<IChange>(o.Changes));
			;
		}

		public override int GetHashCode() {
			return base.GetHashCode()
				^ this.Changes.GetHashCode();
		}
		#endregion
	}
}
