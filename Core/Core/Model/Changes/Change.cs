﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal class Change : IChange {
		#region ctors
		public Change(IChangeArgs args) {
			this.Author = args.Author;
			this.DateTime = args.DateTime;
			this.ShiftCount = 1;
		}
		#endregion

		#region IChange Members
		public int Id { get; set; }
		public DateTimeOffset DateTime { get; private set; }
		public IAuthor Author { get; private set; }
		public int ShiftCount { get; protected set; }

		static readonly string _visualText = "\u2022";
		public virtual string VisualText { get { return _visualText; } }

		public virtual bool ShouldShifterExecuteFollowingChange { get { return false; } }


		public virtual void ExecuteDo(ITextEditor editor) {
			throw new NotSupportedException("Generic Change doesn't have any Do()");
		}

		public virtual void ExecuteUndo(ITextEditor editor) {
			throw new NotSupportedException("Generic Change doesn't have any Undo()");
		}

		public virtual void OnStoring(ITextEditor editor) {
			this.ExecuteDo(editor);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as Change;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return this.Id == o.Id
				&& ( this.Author != null ? this.Author.Equals(o.Author) : o.Author == null )
				&& this.DateTime.Equals(o.DateTime)
				&& this.ShiftCount == o.ShiftCount;
			;
		}

		public override int GetHashCode() {
			return this.Id
				^ ( this.Author != null ? this.Author.GetHashCode() : 0 )
				^ this.DateTime.GetHashCode()
				^ this.ShiftCount.GetHashCode()
				^ base.GetHashCode()
			;
		}
		#endregion
	}
}
