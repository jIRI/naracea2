﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal class GroupEnd : ChangeWithChangeCollection {
		#region ctors
		public GroupEnd(IWithChangeCollectionChangeArgs args)
			: base(args) {
				this.ShiftCount = this.Changes.Count() + 1;
		}
		#endregion

		#region IChange Members
		static readonly string _visualText = "]";
		public override string VisualText { get { return _visualText; } }
		public override bool ShouldShifterExecuteFollowingChange { get { return true; } }
		#endregion

		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			this.ExecuteDoOnAllChanges(editor);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			this.ExecuteUndoOnAllChanges(editor);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as GroupEnd;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion
	}
}
