﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal class GroupBegin : Change, ICanSetShiftCount {
		#region ctors
		public GroupBegin(IChangeArgs args)
			: base(args) {
		}
		#endregion

		#region IChange Members
		static readonly string _visualText = "[";
		public override string VisualText { get { return _visualText; } }
		#endregion

		#region IChange overrides
		public override bool ShouldShifterExecuteFollowingChange { get { return true; } }
		#endregion

		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			// actual functionality is done in group end
		}

		public override void ExecuteUndo(ITextEditor editor) {
			// actual functionality is done in group end
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as GroupBegin;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion

		#region ICanSetShiftCount Members
		public void SetShiftCount(int count) {
			this.ShiftCount = count;
		}
		#endregion
	}
}
