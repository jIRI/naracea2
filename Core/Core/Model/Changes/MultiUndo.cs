﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	/// <summary>
	/// When using this class, remember: it is possible to undo only consecutive changes.
	/// If change collection contains out-of-order changes, it can have unpredictable results.
	/// </summary>
	internal class MultiUndo : ChangeWithChangeCollection, IUndo {
		#region ctors
		public MultiUndo(IWithChangeCollectionChangeArgs args)
			: base(args) {
			this.ShiftCount = 1;
		}
		#endregion

		#region IChange Members
		static readonly string _visualText = "\u2190";
		public override string VisualText { get { return _visualText; } }
		#endregion

		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			this.ExecuteUndoOnAllChanges(editor);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			this.ExecuteDoOnAllChanges(editor);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as MultiUndo;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion

		#region IUndo members
		public IChange UndoneChange {
			get { return this.Changes.First(); }
		}
		#endregion
	}
}
