﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Model.Changes {
	internal class Undo : ChangeWithChange, IUndo {
		#region ctors
		public Undo(IWithChangeChangeArgs args)
			: base(args) {
		}
		#endregion

		#region IChange Members
		static readonly string _visualText = "\u2190";
		public override string VisualText { get { return _visualText; } }
		#endregion
		
		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			this.Change.ExecuteUndo(editor);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			this.Change.ExecuteDo(editor);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as Undo;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion

		#region IUndo members
		public IChange UndoneChange {
			get { return this.Change; }
		}
		#endregion
	}
}
