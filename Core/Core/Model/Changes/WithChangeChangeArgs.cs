﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public class WithChangeChangeArgs : ChangeArgs, IWithChangeChangeArgs {
		// This is needed because Func<> doesn't have enough params
		public delegate IWithChangeChangeArgs WithChangeChangeArgsFactory(IAuthor author, DateTimeOffset now, IChange change);

		public WithChangeChangeArgs(IAuthor author, DateTimeOffset now, IChange change)
			: base(author, now) {
			this.Change = change;
		}

		#region IWithChangeChangeArgs Members
		public IChange Change { get; private set; }
		#endregion
	}
}
