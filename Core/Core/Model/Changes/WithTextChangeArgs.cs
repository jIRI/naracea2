﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public class WithTextChangeArgs : ChangeArgs, IWithTextChangeArgs {
		// This is needed because Func<> doesn't have enough params
		public delegate IWithTextChangeArgs WithTextChangeArgsFactory(IAuthor author, DateTimeOffset now, string text, int position);

		public WithTextChangeArgs(IAuthor author, DateTimeOffset now, string text, int position)
			: base(author, now) {
			this.Text = text;
			this.Position = position;
		}

		#region IWithChangeChangeArgs Members
		public string Text { get; private set; }
		public int Position { get; private set; }
		#endregion
	}
}
