﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	internal class DeleteText : ChangeWithText {
		#region ctors
		public DeleteText(IWithTextChangeArgs args)
			: base(args) {
		}
		#endregion

		#region Properties override
		public override int AddedTextLength {
			get {
				return -this.Text.Length;
			}
		}
		#endregion

		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			editor.DeleteText(this.Position, this.Text);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			editor.InsertTextBehindPosition(this.Position, this.Text);
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as DeleteText;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion
	}
}
