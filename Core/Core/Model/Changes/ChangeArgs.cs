﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	public class ChangeArgs : IChangeArgs {
		public ChangeArgs(IAuthor author, DateTimeOffset now) {
			this.Author = author;
			this.DateTime = now;
		}

		public IAuthor Author { get; private set; }
		public DateTimeOffset DateTime { get; private set; }
	}
}
