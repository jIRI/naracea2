﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	internal class BackspaceText : ChangeWithText {
		#region ctros
		public BackspaceText(IWithTextChangeArgs args)
			: base(args) {
		}
		#endregion

		#region Properties override
		public override int AddedTextLength {
			get {
				return -this.Text.Length;
			}
		}
		#endregion

		#region Do/Undo overrides
		public override void ExecuteDo(ITextEditor editor) {
			editor.BackspaceText(this.Position, this.Text);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			editor.InsertTextBeforePosition(this.Position, this.Text);
		}
		#endregion
	
		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as BackspaceText;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#endregion
	}
}
