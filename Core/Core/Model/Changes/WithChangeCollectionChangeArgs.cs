﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model.Changes {
	internal class WithChangeCollectionChangeArgs : ChangeArgs, IWithChangeCollectionChangeArgs {
		// This is needed because Func<> doesn't have enough params
		public delegate IWithChangeCollectionChangeArgs WithChangeCollectionChangeArgsFactory(IAuthor author, DateTimeOffset now, IEnumerable<IChange> changes);

		public WithChangeCollectionChangeArgs(IAuthor author, DateTimeOffset now, IEnumerable<IChange> changes)
			: base(author, now) {
			this.Changes = changes;
		}

		public IEnumerable<IChange> Changes { get; private set; }
	}
}
