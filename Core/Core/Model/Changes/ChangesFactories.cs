﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Textacle.Core.Entities.Changes;
using System.Diagnostics;

namespace Textacle.Core.Entities.Changes {
	public class ChangesFactories : IChangesFactories {
		#region ctor
		public ChangesFactories(
			InsertTextFactory insertText,
			DeleteTextFactory deleteText,
			BackspaceTextFactory backspaceText,
			UndoFactory undo,
			RedoFactory redo
		) {
			Debug.Assert(insertText != null);
			this.InsertText = insertText;
			Debug.Assert(deleteText != null);
			this.DeleteText = deleteText;
			Debug.Assert(backspaceText != null);
			this.BackspaceText = backspaceText;
			Debug.Assert(undo != null);
			this.Undo = undo;
			Debug.Assert(redo != null);
			this.Redo = redo;
		}
		#endregion

		#region IChangesFactories Members
		public InsertTextFactory InsertText { get; private set; }
		public DeleteTextFactory DeleteText { get; private set; }
		public BackspaceTextFactory BackspaceText { get; private set; }
		public UndoFactory Undo { get; private set; }
		public RedoFactory Redo { get; private set; }
		#endregion
	}
}
