﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	internal class IdProvider : IIdProvider {
		int _id = -1;
		public IdProvider() {
		}

		public IdProvider(int initialValue) {
			_id = initialValue;
		}

		public int GetNextId() {
			return ++_id;
		}
	}
}
