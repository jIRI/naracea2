﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Model {
	public class CurrentChangeChangedEventArgs : EventArgs {
		public int ChangeIndex { get; private set; }
		public CurrentChangeChangedEventArgs(int changeIndex) {
			this.ChangeIndex = changeIndex;
		}
	}

	public class ChangeAddedEventArgs : EventArgs {
		public IChange Change { get; private set; }

		public ChangeAddedEventArgs(IChange change) {
			this.Change = change;
		}
	}
}
