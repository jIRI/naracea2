﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	internal class Metadata : IMetadata {
		public Metadata() {
			this.Data = new Dictionary<string, string>();
		}

		public string Get(string key) {
			if( this.Data.ContainsKey(key) ) {
				return this.Data[key];
			} else {
				return null;
			}
		}

		public void Set(string key, string value) {
			if( this.Data.ContainsKey(key) ) {
				this.Data[key] = value;
			} else {
				this.Data.Add(key, value);
			}			
		}

		public IDictionary<string, string> Data { get; private set; }
	}
}
