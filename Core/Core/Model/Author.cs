﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	internal class Author : IAuthor {
		public Author() {
			this.Username = Properties.Resources.DefaultUserName;
			this.Name = string.Empty;
		}

		#region IAuthor Members
		public string Username {get;set;}
		public string Name { get; set; }
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as Author;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			var result = this.Username.Equals(o.Username)
				&& this.Name.Equals(o.Name)
			;
			return result;
		}

		public override int GetHashCode() {
			return this.Username.GetHashCode()
				^ this.Name.GetHashCode()
			;
		}
		#endregion
	}
}
