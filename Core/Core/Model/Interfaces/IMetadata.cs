﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public interface IMetadata {
		string Get(string key);
		void Set(string key, string value);
		IDictionary<string, string> Data { get; }
	}
}
