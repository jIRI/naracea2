﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Model {
	public interface IChangesCollection {
		int Count { get; }
		IChange At(int index);
		int IndexOf(IChange change);
		void Add(IChange change);
		IEnumerable<IChange> OwnedChanges { get; }

		event EventHandler<ChangeAddedEventArgs> ChangeAdded;
	}
}
