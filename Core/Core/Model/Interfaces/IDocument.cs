﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public interface IDocument {
		string Name { get; set; }
		string Comment { get; set; }
		IList<IBranch> Branches { get; }
		IList<IAuthor> Authors { get; }
		IBranch CurrentBranch { get; set; }
		IMetadata Metadata { get; }

		int GetNextBranchId();
	}
}
