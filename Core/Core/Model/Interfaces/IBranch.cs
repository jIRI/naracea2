﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Model {

	public interface IBranch {
		string Name { get; set; }
		string Comment { get; set; }
		/// <summary>
		/// Current change index. 
		/// If no change was unwinded, value is -1 (default value after empty root bracnch creation).
		/// Index starts at 0 at first change in root branch, and increments to the current change of current branch.
		/// </summary>
		int CurrentChangeIndex { get; set; }
		IAuthor Author { get; }
		/// <summary>
		/// Parent branch.
		/// For root branch this is null.
		/// </summary>
		IBranch Parent { get; }
		/// <summary>
		/// Change at which branching happend. 
		/// This is inclusive, i.e. change from parent with given index is included in current branch.
		/// For root branch, value is 0
		/// </summary>
		int BranchingChangeIndex { get; }
		DateTimeOffset BranchingDateTime { get; }
		IChangesCollection Changes { get; }
		IMetadata Metadata { get; }

		/// <summary>
		/// Sets current change value to -1 and does whatever is needed to reset branch state.
		/// </summary>
		void ResetState();

		event EventHandler<CurrentChangeChangedEventArgs> CurrentChangeChanged;
	}
}
