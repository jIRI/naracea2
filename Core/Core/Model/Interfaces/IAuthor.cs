﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public interface IAuthor {
		string Username { get; set; }
		string Name { get; set; }
	}
}
