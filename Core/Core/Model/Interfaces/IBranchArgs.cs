﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public interface IBranchArgs {
		IAuthor Author { get; }
		IBranch Parent { get; }
		int BranchingChangeIndex { get; }
		DateTimeOffset BranchingDateTime { get; }
	}
}
