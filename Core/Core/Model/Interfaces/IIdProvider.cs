﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Model {
	public interface IIdProvider {
		int GetNextId();
	}
}
