﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Common.Extensions;
using System.Diagnostics;

namespace Naracea.Core.Model {
	internal class Branch : IBranch {
		IList<IChange> _changeList = new List<IChange>();
		public Branch(IBranchArgs args) {
			this.Name = Properties.Resources.DefaulBranchName;
			this.Comment = string.Empty;
			this.BranchingChangeIndex = args.BranchingChangeIndex;
			this.BranchingDateTime = args.BranchingDateTime;
			var idProvider = new IdProvider(this.BranchingChangeIndex);
			this.Changes = new ChangesCollection(this, _changeList, idProvider);
			this.Author = args.Author;
			this.Parent = args.Parent;
			this.Metadata = new Metadata();
		}


		#region IBranch Members
		public string Name { get; set; }
		public string Comment { get; set; }
		public IAuthor Author { get; private set; }
		public IBranch Parent { get; private set; }
		public int BranchingChangeIndex { get; private set; }
		public DateTimeOffset BranchingDateTime { get; private set; }
		public IChangesCollection Changes { get; private set; }
		public IMetadata Metadata { get; private set; }

		int _currentChangeIndex = -1;
		object _currentChangeIndexPadlock = new object();
		public int CurrentChangeIndex {
			get {
				lock( _currentChangeIndexPadlock ) {
					return _currentChangeIndex;
				}
			}
			set {
				lock( _currentChangeIndexPadlock ) {
					if( _currentChangeIndex != value ) {
						_currentChangeIndex = value;
						this.CurrentChangeChanged.Raise(this, new CurrentChangeChangedEventArgs(_currentChangeIndex));
					}
				}
			}
		}

		public event EventHandler<CurrentChangeChangedEventArgs> CurrentChangeChanged;

		public void ResetState() {
			this.CurrentChangeIndex = -1;
		}
		#endregion

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as Branch;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			var result = this.Author.Equals(o.Author)
				&& this.BranchingChangeIndex.Equals(o.BranchingChangeIndex)
				&& (this.Comment != null ? this.Comment.Equals(o.Comment) : o.Comment == null)
				&& this.BranchingDateTime.Equals(o.BranchingDateTime)
				&& this.Name.Equals(o.Name)
				&& (this.Parent != null ? this.Parent.Equals(o.Parent) : o.Parent == null)
				&& this.CurrentChangeIndex.Equals(o.CurrentChangeIndex)
				&& _changeList.IsEqualTo(o._changeList)
			;

			return result;
		}

		public override int GetHashCode() {
			return this.Author.GetHashCode()
				^ this.BranchingChangeIndex.GetHashCode()
				^ (this.Comment != null ? this.Comment.GetHashCode() : 0)
				^ this.BranchingDateTime.GetHashCode()
				^ this.Name.GetHashCode()
				^ (this.Parent != null ? this.Parent.GetHashCode() : 0)
				^ _changeList.GetHashCode()
				^ this.CurrentChangeIndex.GetHashCode()
			;
		}
		#endregion
	}
}
