﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Common.Extensions;
using Naracea.Core.Model.Changes;
using System.Threading;

namespace Naracea.Core.Model {
	internal class ChangesCollection : IChangesCollection {
		IBranch _branch;
		IList<IChange> _changes;
		IIdProvider _idProvider;
		object _lock = new object();

		public ChangesCollection(IBranch branch, IList<IChange> changes, IIdProvider idProvider) {
			_branch = branch;
			_changes = changes;
			_idProvider = idProvider;
		}

		public int Count {
			get {
				lock( _lock ) {
					var count = _changes.Count;
					if( _branch.Parent != null ) {
						count += _branch.BranchingChangeIndex + 1;
					}
					return count;
				}
			}
		}

		public IChange At(int index) {
			IChange result = null;
			lock( _lock ) {
				if( _branch.Parent != null && index > _branch.BranchingChangeIndex ) {
					//this branch data and branch has has parent
					result = _changes[index - _branch.BranchingChangeIndex - 1];
				} else if( _branch.Parent != null ) {
					//index is somewhere in parent branches
					result = _branch.Parent.Changes.At(index);
				} else {
					//this handles root branch
					result = _changes[index];
				}
			}
			return result;
		}

		public int IndexOf(IChange change) {
			if( change == null ) {
				return -1;
			}

			lock( _lock ) {
				//this fills properly
				// a) when change is not in this branch (-1)
				// b) when we are in root branch
				var index = _changes.IndexOf(change);
				if( _branch.Parent != null && index != -1 ) {
					//this branch data and branch has has parent
					index += _branch.BranchingChangeIndex + 1;
				} else if( _branch.Parent != null ) {
					//change is somewhere in parent branches
					index = _branch.Parent.Changes.IndexOf(change);
				}
				return index;
			}
		}

		public event EventHandler<ChangeAddedEventArgs> ChangeAdded;

		public void Add(IChange change) {
			lock( _lock ) {
				change.Id = _idProvider.GetNextId();
				_changes.Add(change);
			}
			this.ChangeAdded.Raise(this, new ChangeAddedEventArgs(change));
		}

		public IEnumerable<IChange> OwnedChanges {
			get {
				IEnumerable<IChange> changes;
				lock( _lock ) {
					changes = _changes.ToArray();
				}
				return changes;
			}
		}
	}
}
