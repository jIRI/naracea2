﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;

namespace Naracea.Core.Shifter {
	public interface IShifter {
		void Shift(IBranch branch, ITextEditor editor);
	}
}
