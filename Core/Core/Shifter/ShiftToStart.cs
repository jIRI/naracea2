﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Common.Extensions;

namespace Naracea.Core.Shifter {
	internal class ShiftToStart : IShifter {
		#region ctors
		public ShiftToStart() {
		}
		#endregion

		#region IShifter Members
		public void Shift(IBranch branch, ITextEditor editor) {
			var shifter = new ShiftToChangeBackward(-1);
			shifter.Shift(branch, editor);
		}
		#endregion
	}
}
