﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using System.Diagnostics;

namespace Naracea.Core.Shifter {
	internal class ShiftToChangeForward : IShifter {
		int _targetChangeIndex;

		#region ctors
		public ShiftToChangeForward(int targetIndex) {
			_targetChangeIndex = targetIndex;
		}
		#endregion

		#region IShifter Members
		public void Shift(IBranch branch, ITextEditor editor) {
			Debug.Assert(_targetChangeIndex < branch.Changes.Count);
			int count = _targetChangeIndex - branch.CurrentChangeIndex;
			while( branch.CurrentChangeIndex < branch.Changes.Count
					&& branch.CurrentChangeIndex < _targetChangeIndex
					&& branch.CurrentChangeIndex < branch.Changes.Count - 1
			) {
				var change = branch.Changes.At(branch.CurrentChangeIndex + 1);
				change.ExecuteDo(editor);
				branch.CurrentChangeIndex += change.ShiftCount;
				if( change.ShouldShifterExecuteFollowingChange ) {
					branch.Changes.At(branch.CurrentChangeIndex + 1).ExecuteDo(editor);
					branch.CurrentChangeIndex++;
				}
			}
		}
		#endregion
	}
}
