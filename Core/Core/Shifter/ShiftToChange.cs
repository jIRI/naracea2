﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using System.Diagnostics;
using Naracea.Common.Extensions;

namespace Naracea.Core.Shifter {
	internal class ShiftToChange : IShifter {
		int _targetChangeIndex;

		#region ctors
		public ShiftToChange(int targetIndex) {
			_targetChangeIndex = targetIndex;
		}
		#endregion

		#region IShifter Members
		public void Shift(IBranch branch, ITextEditor editor) {
			Debug.Assert(_targetChangeIndex >= -1);
			Debug.Assert(_targetChangeIndex < branch.Changes.Count);
			IShifter shifter = null;
			if( _targetChangeIndex > branch.CurrentChangeIndex ) {
				shifter = new ShiftToChangeForward(_targetChangeIndex);
			} else if( _targetChangeIndex < branch.CurrentChangeIndex ) {
				shifter = new ShiftToChangeBackward(_targetChangeIndex);
			}

			if( shifter != null ) {
				shifter.Shift(branch, editor);
			}
		}
		#endregion
	}
}
