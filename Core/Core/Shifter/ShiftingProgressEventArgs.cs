﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Shifter {
	public class ShiftingProgressEventArgs : EventArgs {
		public int PercentCompleted { get; private set; }
		public bool Cancel { get; set; }
		public IChange Change { get; private set; } 

		public ShiftingProgressEventArgs(IChange change, int percentCompleted) {
			this.PercentCompleted = percentCompleted;
		}

		public ShiftingProgressEventArgs(IChange change, int currentValue, int totalCount) {
			this.PercentCompleted = (int)( 100.0 * (double)currentValue / (double)totalCount );
		}
	}
}
