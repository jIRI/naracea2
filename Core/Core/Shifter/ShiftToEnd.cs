﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Common.Extensions;

namespace Naracea.Core.Shifter {
	internal class ShiftToEnd :IShifter {
		#region ctors
		public ShiftToEnd() {
		}
		#endregion

		#region IShifter Members
		public void Shift(IBranch branch, ITextEditor editor) {
			var shifter = new ShiftToChangeForward(branch.Changes.Count - 1);
			shifter.Shift(branch, editor);
		}
		#endregion
	}
}
