﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using System.Diagnostics;

namespace Naracea.Core.Shifter {
	internal class ShiftToChangeBackward : IShifter {
		int _targetChangeIndex;

		#region ctors
		public ShiftToChangeBackward(int targetIndex) {
			_targetChangeIndex = targetIndex;
		}
		#endregion

		#region IShifter Members
		public void Shift(IBranch branch, ITextEditor editor) {
			Debug.Assert(_targetChangeIndex >= -1);
			int count = branch.CurrentChangeIndex - _targetChangeIndex;
			while( branch.CurrentChangeIndex < branch.Changes.Count 
				&& branch.CurrentChangeIndex > _targetChangeIndex 
				&& branch.CurrentChangeIndex >= 0 
			) {
				var change = branch.Changes.At(branch.CurrentChangeIndex);
				change.ExecuteUndo(editor);
				branch.CurrentChangeIndex -= change.ShiftCount;
				if( change.ShouldShifterExecuteFollowingChange ) {
					branch.Changes.At(branch.CurrentChangeIndex).ExecuteUndo(editor);
					branch.CurrentChangeIndex--;
				}
			}
		}
		#endregion
	}
}
