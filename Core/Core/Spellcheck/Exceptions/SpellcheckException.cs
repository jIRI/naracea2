﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;

namespace Naracea.Core.Spellcheck.Exceptions {
	public class SpellcheckException : NaraceaException {
		public SpellcheckException() {
		}

		public SpellcheckException(string message) 
		: base(message) {			
		}

		public SpellcheckException(string message, Exception innerException) 
		: base(message, innerException) {
		}
	}
}
