﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHunspell;
using System.Diagnostics;
using Naracea.Core.FileSystem;

namespace Naracea.Core.Spellcheck {
	internal class Spellchecker : ISpellchecker {
		Hunspell _spellchecker;
		Func<string, IStreamBuilder, ICustomDictionary> _customDictionaryFactory;
		IStreamBuilder _streamBuilder;

		public Spellchecker(Func<string, IStreamBuilder, ICustomDictionary> customDictionaryFactory, IStreamBuilder streamBuilder) {
			this.CurrentLanguage = DictionaryDescriptor.None;
			_customDictionaryFactory = customDictionaryFactory;
			_streamBuilder = streamBuilder;
		}

		#region ISpellchecker Members
		DictionaryDescriptor _currentLanguage;
		public DictionaryDescriptor CurrentLanguage {
			get {
				return _currentLanguage;
			}
			set {
				if( _currentLanguage != value ) {
					_currentLanguage = value;
					this.UpdateLanguage();
				}
			}
		}

		public ICustomDictionary CustomDictionary { get; private set; }

		public bool Spell(string word) {
			return _spellchecker.Spell(word);
		}

		public IEnumerable<string> Suggest(string word) {
			return _spellchecker.Suggest(word);
		}

		public void Add(string word) {
			_spellchecker.Add(word);
			this.CustomDictionary.Add(word);
		}

		public void Add(IEnumerable<string> words) {
			foreach( var word in words ) {
				this.Add(word);
			}
		}
		#endregion

		#region Private methods
		void UpdateLanguage() {
			// get rid of old spellchecker
			if( _spellchecker == null ) {
				// no language set so far, use empty spellchecker
				_spellchecker = new Hunspell();
				this.CustomDictionary = null;
			} else {
				_spellchecker.Dispose();
				_spellchecker = new Hunspell();
				this.CustomDictionary = null;
			}

			// create spellchecker
			if( this.CurrentLanguage != DictionaryDescriptor.None ) {
				_spellchecker.Load(this.CurrentLanguage.AffFileName, this.CurrentLanguage.DicFileName);
				// get custom dictionary
				this.CustomDictionary = _customDictionaryFactory(this.CurrentLanguage.CustomFileName, _streamBuilder);
				this.CustomDictionary.Load();

				// add custom dictionary to spellchecker
				foreach( var word in this.CustomDictionary.Words ) {
					_spellchecker.Add(word);
				}
			}

		}
		#endregion

		#region IDisposable Members
		~Spellchecker() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
			Debug.Assert(false);
		}

		private bool _disposed = false;
		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
					if( _spellchecker != null ) {
						_spellchecker.Dispose();
					}
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
