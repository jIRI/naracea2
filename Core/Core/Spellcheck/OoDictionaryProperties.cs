﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck {
	public class OoDictionaryProperties {
		public string DisplayName { get; set; }
		public string StorageName { get; set; }
		public string AffFileName { get; set; }
		public string DicFileName { get; set; }
	}
}
