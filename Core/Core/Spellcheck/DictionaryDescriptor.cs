﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Naracea.Core.Spellcheck {
	public class DictionaryDescriptor {
		static DictionaryDescriptor() {
			DictionaryDescriptor.None = new DictionaryDescriptor();
		}

		private DictionaryDescriptor() {
		}

		public DictionaryDescriptor(string folder, string name, string displayName, string aff, string dic, string custom) {
			this.DictionaryFolder = folder;
			this.DictionaryName = name;
			this.DisplayName = displayName;
			this.AffFileName = aff;
			this.DicFileName = dic;
			this.CustomFileName = custom;
		}

		public string DictionaryFolder { get; set; }
		public string DictionaryName { get; set; }
		public string DisplayName { get; set; }
		public string AffFileName { get; set; }
		public string DicFileName { get; set; }
		public string CustomFileName { get; set; }

		public static DictionaryDescriptor None { get; private set; }

		#region Object overrides
		public override bool Equals(object obj) {
			var o = obj as DictionaryDescriptor;
			if( o == null ) {
				return false;
			}
			if( ReferenceEquals(this, obj) ) {
				return true;
			}
			return this.AffFileName == o.AffFileName
				&& this.DicFileName == o.DicFileName
				&& this.CustomFileName == o.CustomFileName
				&& this.DictionaryFolder == o.DictionaryFolder
				&& this.DictionaryName == o.DictionaryName
				&& this.DisplayName == o.DisplayName
			;
		}

		public override int GetHashCode() {
			return this.AffFileName.GetHashCode()
				^ this.DicFileName.GetHashCode()
				^ this.CustomFileName.GetHashCode()
				^ this.DictionaryFolder.GetHashCode()
				^ this.DictionaryName.GetHashCode()
				^ this.DisplayName.GetHashCode()
			;
		}
		#endregion
	}
}
