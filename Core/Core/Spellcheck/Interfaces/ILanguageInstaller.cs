﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck {
	public interface ILanguageInstaller {
		DictionaryDescriptor Install(string name, string displayName, string affSource, string dicSource);
	}
}
