﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck {
	public interface ICustomDictionary {
		void Save();
		void Load();

		void Add(string word);
		void Add(IEnumerable<string> words);

		IEnumerable<string> Words { get; }

		string StorePath { get; }
	}
}
