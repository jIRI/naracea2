﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck {
	public interface IXcuParser {
		OoDictionaryProperties Parse(string xcuFile);
	}
}
