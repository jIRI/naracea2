﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck {
	public interface ISpellchecker : IDisposable {
		bool Spell(string word);
		IEnumerable<string> Suggest(string word);

		void Add(string word);
		void Add(IEnumerable<string> words);

		DictionaryDescriptor CurrentLanguage { get; set; }
		ICustomDictionary CustomDictionary { get; }
	}
}
