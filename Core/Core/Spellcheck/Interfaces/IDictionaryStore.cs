﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Spellcheck{
	public interface IDictionaryStore {
		void Install(string name, string affSource, string dicSource);
		void InstallOoDictionary(string oxtSource);

		List<DictionaryDescriptor> InstalledLanguages { get; }

		string StorePath { get; }
	}
}
