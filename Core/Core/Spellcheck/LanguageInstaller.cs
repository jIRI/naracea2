﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.FileSystem;
using System.IO;

namespace Naracea.Core.Spellcheck {
	internal class LanguageInstaller : ILanguageInstaller {
		static readonly string DefaultCustomLanguageFileName = "custom";
		string _storeFolder;
		IFileCopier _fileCopier;
		IFileChecker _fileChecker;
		IFileRemover _fileRemover;
		IDirectoryMaker _dirMaker;
		IDirectoryChecker _dirChecker;

		public LanguageInstaller(
			string storeFolder, 
			IFileChecker fileChecker, 
			IFileCopier fileCopier, 
			IFileRemover fileRemover, 
			IDirectoryMaker dirMaker, 
			IDirectoryChecker dirChecker
		) {
			_storeFolder = storeFolder;
			_fileChecker = fileChecker;
			_fileCopier = fileCopier;
			_fileRemover = fileRemover;
			_dirMaker = dirMaker;
			_dirChecker = dirChecker;
		}

		#region ILanguageInstaller Members
		public DictionaryDescriptor Install(string name, string displayName, string affSource, string dicSource) {
			var langPath = Path.Combine(_storeFolder, name);
			// create directory if neccessary
			if( !_dirChecker.Exists(langPath) ) {
				_dirMaker.MakeDir(langPath);
			}
			// copy aff file
			// remove it if it exists already
			var sourceAffFilename = Path.GetFileName(affSource);
			var targetAffFile = Path.Combine(langPath, sourceAffFilename);
			if( _fileChecker.Exists(targetAffFile) ) {
				_fileRemover.Remove(targetAffFile);
			}
			_fileCopier.Copy(affSource, targetAffFile);

			// copy dict file
			// remove it if it exists already
			var sourceDicFilename = Path.GetFileName(dicSource);
			var targetDicFile = Path.Combine(langPath, sourceDicFilename);
			if( _fileChecker.Exists(targetDicFile) ) {
				_fileRemover.Remove(targetDicFile);
			}
			_fileCopier.Copy(dicSource, targetDicFile);

			return new DictionaryDescriptor(
				langPath,
				name,
				displayName,
				Path.Combine(langPath, sourceAffFilename),
				Path.Combine(langPath, sourceDicFilename),
				Path.Combine(langPath, LanguageInstaller.DefaultCustomLanguageFileName)
			);
		}
		#endregion
	}
}
