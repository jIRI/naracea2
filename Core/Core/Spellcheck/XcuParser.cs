﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Globalization;

namespace Naracea.Core.Spellcheck {
	internal class XcuParser : IXcuParser {
		#region IXcuParser Members
		public OoDictionaryProperties Parse(string xcuFile) {
			var properties = new OoDictionaryProperties();

			var xcuDoc = XElement.Load(xcuFile);
			var hunspellDicNodeQuery = from x in xcuDoc.Descendants("node")
																 from a in x.Attributes()
																 where a.Name.LocalName == "name" && a.Value.StartsWith("HunSpellDic")
																 select x;
			var hunspellDicNode = hunspellDicNodeQuery.FirstOrDefault();
			if( hunspellDicNode != null ) {
				var locationsQuery = from x in hunspellDicNode.Elements()
														 from a in x.Attributes()
														 where a.Name.LocalName == "name" && a.Value == "Locations"
														 select x.Value;
				var locations = locationsQuery.FirstOrDefault();
				if( !string.IsNullOrWhiteSpace(locations) ) {
					var files = locations.Split(' ');
					var dicQuery = from file in files
												 where Path.GetExtension(file) == ".dic"
												 select file.Replace("%origin%/", "");
					properties.DicFileName = dicQuery.FirstOrDefault();
					var affQuery = from file in files
												 where Path.GetExtension(file) == ".aff"
												 select file.Replace("%origin%/", "");
					properties.AffFileName = affQuery.FirstOrDefault();
				}

				var localesQuery = from x in hunspellDicNode.Elements()
													 from a in x.Attributes()
													 where a.Name.LocalName == "name" && a.Value == "Locales"
													 select x.Value;
				var locales = localesQuery.FirstOrDefault();
				if( !string.IsNullOrWhiteSpace(locales) ) {
					var locale = new CultureInfo(locales);
					properties.DisplayName = locale.DisplayName;
					properties.StorageName = locales;
				}
			}
			return properties;
		}
		#endregion
	}
}
