﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.FileSystem;
using System.IO;
using System.Diagnostics;

namespace Naracea.Core.Spellcheck {
	internal class DictionaryStore : IDictionaryStore {
		public delegate IDictionaryStore DictionaryStoreFactory(
			string storeBasePath,
			IFileChecker fileChecker,
			IFileSerializer serializer,
			IFileCopier copier,
			IFileNameValidator validator,
			IDirectoryMaker dirMaker,
			IDirectoryRemover dirRemover,
			IXcuParser xcuParser,
			Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> langInstallerFactory,
			Func<string, IZipReader> zipReaderFactory
		);

		static readonly string DictionaryDesciptorFileName = "dictionaries.xcu";

		IFileChecker _fileChecker;
		IFileCopier _fileCopier;
		IFileRemover _fileRemover;
		IFileNameValidator _validator;
		IDirectoryMaker _dirMaker;
		IDirectoryRemover _dirRemover;
		IDirectoryChecker _dirChecker;
		IXcuParser _xcuParser;
		Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> _langInstallerFactory;
		Func<string, IZipReader> _zipReaderFactory;

		public DictionaryStore(
			IFileChecker fileChecker,
			IFileCopier fileCopier, 
			IFileRemover fileRemover, 
			IFileNameValidator validator,
			IDirectoryMaker dirMaker,
			IDirectoryRemover dirRemover,
			IDirectoryChecker dirChecker,
			IXcuParser xcuParser,
			Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> langInstallerFactory,
			Func<string, IZipReader> zipReaderFactory
		) {
			var savePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
			this.StorePath = System.IO.Path.Combine(savePath, Naracea.Common.Constants.AppDirectory);
			_fileChecker = fileChecker;
			_fileCopier = fileCopier;
			_fileRemover = fileRemover;
			_validator = validator;
			_dirMaker = dirMaker;
			_dirRemover = dirRemover;
			_dirChecker = dirChecker;
			_xcuParser = xcuParser;
			_langInstallerFactory = langInstallerFactory;
			_zipReaderFactory = zipReaderFactory;
		}

		#region IDictionaryStore Members
		public void Install(string name, string affSource, string dicSource) {
			var installer = _langInstallerFactory(this.StorePath, _fileChecker, _fileCopier, _fileRemover, _dirMaker, _dirChecker);
			var validName = _validator.GetValidFileName(name);
			try {
				var descriptor = installer.Install(validName, name, affSource, dicSource);
				_installedLanguages.Add(descriptor);
			} catch( Exception ex ) {
				throw new Exceptions.SpellcheckException(Properties.Resources.UnableToInstallLanguageException, ex);
			}
		}

		public void InstallOoDictionary(string oxtSource) {
			var installer = _langInstallerFactory(this.StorePath, _fileChecker, _fileCopier, _fileRemover, _dirMaker, _dirChecker);
			string tempDir = null;
			try {
				// get temp dir
				tempDir = _dirMaker.MakeTempDir();
				//extract dictionary descriptor from the oxf to temp dir
				using( var zipReader = _zipReaderFactory(oxtSource) ) {
					var xcuFile = Path.Combine(tempDir, DictionaryDesciptorFileName);
					zipReader.Extract(DictionaryDesciptorFileName, tempDir);
					// parse descriptor
					var properties = _xcuParser.Parse(xcuFile);
					// extract dictionary files
					var dicFile = Path.Combine(tempDir, properties.DicFileName);
					zipReader.Extract(properties.DicFileName, tempDir);
					var affFile = Path.Combine(tempDir, properties.AffFileName);
					zipReader.Extract(properties.AffFileName, tempDir);
					// do the installation
					var descriptor = installer.Install(properties.StorageName, properties.DisplayName, affFile, dicFile);
					_installedLanguages.Add(descriptor);
				}
			} catch( Exception ex ) {
				throw new Exceptions.SpellcheckException(Properties.Resources.UnableToInstallLanguageException, ex);
			} finally {
				try {
					//try remove temp dir
					if( !string.IsNullOrEmpty(tempDir) ) {
						_dirRemover.Remove(tempDir);
					}
				} catch( Exception ) {
					// do nothing - removing tempdir is not critical part of installation, it can be ignored
					Debug.WriteLine("Failed to remove temp directory {0}", tempDir ?? "<null>");
				}
			}
		}

		List<DictionaryDescriptor> _installedLanguages = new List<DictionaryDescriptor>();
		public List<DictionaryDescriptor> InstalledLanguages {
			get { return _installedLanguages; }
		}

		public string StorePath { get; private set; }
		#endregion
	}
}
