﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.FileSystem;
using System.IO;

namespace Naracea.Core.Spellcheck {
	internal class CustomDictionary : ICustomDictionary {
		IStreamBuilder _streamBuilder;

		public CustomDictionary(string storePath, IStreamBuilder streamBuilder) {
			this.StorePath = storePath;
			_streamBuilder = streamBuilder;
		}

		#region ICustomDictionary Members
		public void Save() {
			using( var fileStream = _streamBuilder.Create(this.StorePath, System.IO.FileMode.Create) ) 
			using(var writeStream = new StreamWriter(fileStream)) {
				foreach( var word in _words ) {
					writeStream.WriteLine(word);
				}
			}
		}

		public void Load() {
			using( var fileStream = _streamBuilder.Create(this.StorePath, System.IO.FileMode.OpenOrCreate) )
			using( var readStream = new StreamReader(fileStream) ) {
				_words.Clear();
				while( !readStream.EndOfStream ) {
					var word = readStream.ReadLine();
					if( word != null ) {
						_words.Add(word);
					} else {
						break;
					}
				}
			}
		}

		public void Add(string word) {
			_words.Add(word);
		}

		public void Add(IEnumerable<string> words) {
			foreach( var word in words ) {
				this.Add(word);
			}
		}

		List<string> _words = new List<string>();
		public IEnumerable<string> Words {
			get { return _words; }
		}

		public string StorePath { get; private set; }
		#endregion

	}
}
