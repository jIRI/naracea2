﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Repository;
using Naracea.Core.FileSystem;

namespace Naracea.Core.Configuration {
	internal class FileSystemModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<FileRemover>().As<IFileRemover>().As<FileRemover>();
			builder.RegisterType<FileRenamer>().As<IFileRenamer>().As<FileRenamer>();
			builder.RegisterType<FileAttributes>().As<IFileAttributes>().As<FileAttributes>();
			builder.RegisterType<FileCopier>().As<IFileCopier>().As<FileCopier>();
			builder.RegisterType<FileChecker>().As<IFileChecker>().As<FileChecker>();
			builder.RegisterType<FileStreamBuilder>().As<IStreamBuilder>().As<FileStreamBuilder>();
			builder.RegisterType<FileNameShortener>().As<IFileNameShortener>().As<FileNameShortener>();
			builder.RegisterType<FileNameValidator>().As<IFileNameValidator>().As<FileNameValidator>();
			builder.RegisterType<FileXmlSerializer>();
			builder.RegisterType<DirectoryChecker>().As<IDirectoryChecker>().As<DirectoryChecker>();
			builder.RegisterType<DirectoryMaker>().As<IDirectoryMaker>().As<DirectoryMaker>();
			builder.RegisterType<DirectoryRemover>().As<IDirectoryRemover>().As<DirectoryRemover>();
			builder.RegisterType<ZipReader>().As<IZipReader>().As<ZipReader>();
		}
		#endregion
	}
}
