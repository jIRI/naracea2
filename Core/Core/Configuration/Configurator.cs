﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;

namespace Naracea.Core.Configuration {
	public class Configurator : IDisposable {
		ContainerBuilder _builder;

		#region IConfigurator Members
		public Configurator() {
			_builder = new ContainerBuilder();
			new CoreModule().Configure(_builder);
			new ModelModule().Configure(_builder);
			new RepositoryModule().Configure(_builder);
			new PersistenceModule().Configure(_builder);
			new ShifterModule().Configure(_builder);
			new FindersModule().Configure(_builder);
			new BuildersModule().Configure(_builder);
			new FileSystemModule().Configure(_builder);
			new SpellcheckModule().Configure(_builder);
			new MarkupModule().Configure(_builder);
			new UpdaterModule().Configure(_builder);
			this.Container = _builder.Build();
		}

		public IContainer Container { get; private set; }
		#endregion

		#region IDisposable Members
		private bool disposed = false;
		public void Dispose() {
			Dispose(true);
		}

		private void Dispose(bool disposing) {
			if( !this.disposed ) {
				if( disposing ) {
					if( this.Container != null ) {
						this.Container.Dispose();
					}
				}
				disposed = true;
			}
		}
		#endregion
	}
}
