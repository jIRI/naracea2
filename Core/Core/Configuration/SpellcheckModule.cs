﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Spellcheck;

namespace Naracea.Core.Configuration {
	internal class SpellcheckModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<XcuParser>().As<IXcuParser>();
			builder.RegisterType<Spellchecker>().As<ISpellchecker>();
			builder.RegisterType<OoDictionaryProperties>();
			builder.RegisterType<LanguageInstaller>().As<ILanguageInstaller>();
			builder.RegisterType<CustomDictionary>().As<ICustomDictionary>();
			builder.RegisterType<DictionaryDescriptor>();
			builder.RegisterType<DictionaryStore>().As<IDictionaryStore>();
		}
		#endregion
	}
}
