﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Spellcheck;
using Naracea.Core.Markdown;

namespace Naracea.Core.Configuration {
	internal class MarkupModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<MarkupConverter>().As<IMarkupConverter>();
		}
		#endregion
	}
}
