﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;

namespace Naracea.Core.Configuration {
	internal class ShifterModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<ShiftToEnd>();
			builder.RegisterType<ShiftToStart>();
			builder.RegisterType<ShiftToChangeForward>();
			builder.RegisterType<ShiftToChangeBackward>();
			builder.RegisterType<ShiftToChange>();
		}
		#endregion
	}
}
