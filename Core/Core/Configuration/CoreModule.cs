﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Builders;
using Naracea.Core.Repository;

namespace Naracea.Core.Configuration {
	internal class CoreModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			//misc
			builder.RegisterType<RealDateTimeProvider>().As<IDateTimeProvider>().As<RealDateTimeProvider>().SingleInstance();
			builder.RegisterType<TextEditorProxy>().As<ITextEditorProxy>().As<TextEditorProxy>();

			//factories agregators
			builder.RegisterType<CoreFactories>().As<ICoreFactories>().SingleInstance();
		}
		#endregion

	}
}
