﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Builders;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Autofac.Core;

namespace Naracea.Core.Configuration {
	internal class BuildersModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<DocumentBuilder>().As<IDocumentBuilder>().As<DocumentBuilder>().SingleInstance();
			builder.RegisterType<BranchBuilder>().As<IBranchBuilder>().As<BranchBuilder>().SingleInstance();
		}
		#endregion

	}
}
