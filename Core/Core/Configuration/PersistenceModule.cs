﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Core.Repository.PersistenceModel.V1;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Configuration {
	internal class PersistenceModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			//builder
			builder.RegisterType<PersisterBuilder>().As<IPersisterBuilder>().As<PersisterBuilder>();
			//common
			builder.RegisterType<MetadataKeyValuePair>().As<IMetadataKeyValuePair>().As<MetadataKeyValuePair>();
			builder.RegisterType<FileMetadata>().As<IFileMetadata>().As<FileMetadata>();
			builder.RegisterType<Serializer>().As<ISerializer>().As<Serializer>();
			builder.RegisterType<FileHeader>().As<IFileHeader>().As<FileHeader>();
			builder.RegisterType<FileHeaderReader>().As<IFileHeaderReader>().As<FileHeaderReader>();
			builder.RegisterType<FileHeaderWriter>().As<IFileHeaderWriter>().As<FileHeaderWriter>();
			builder.RegisterType<FileMetadataReader>().As<IFileMetadataReader>().As<FileMetadataReader>();
			builder.RegisterType<FileMetadataWriter>().As<IFileMetadataWriter>().As<FileMetadataWriter>();
			builder.RegisterType<FileStreamBuilder>().As<IFileStreamBuilder>().As<FileStreamBuilder>();
			//V1
			builder.RegisterType<DocumentMapperV1>().As<IDocumentMapper<IDocumentDtoV1>>();
			builder.RegisterType<FileContentReaderV1>().As<IFileContentReader<IDocumentDtoV1>>();
			builder.RegisterType<FileContentWriterV1>().As<IFileContentWriter<IDocumentDtoV1>>();
			builder.RegisterType<FileReaderV1>().As<IFileReader>();
			builder.RegisterType<FileWriterV1>().As<IFileWriter>();
		}
		#endregion
	}
}
