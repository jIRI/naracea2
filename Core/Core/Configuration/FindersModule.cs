﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Configuration {
	internal class FindersModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<UndoRedoFinder>().As<IUndoRedoFinder>().As<UndoRedoFinder>();
			builder.RegisterType<ParagraphFinder>();
			builder.RegisterType<SentenceFinder>();
			builder.RegisterType<WordFinder>();
			builder.RegisterType<RegExTextFinder>();
			builder.RegisterType<PlainTextFinder>();
			builder.RegisterType<TextSurroundingsFinder>();
		}
		#endregion
	}
}
