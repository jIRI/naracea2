﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;

namespace Naracea.Core.Configuration {
	internal class UpdaterModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<Update.UpdateChecker>().As<Update.IUpdateChecker>().As<Update.UpdateChecker>().WithParameter(
#if DEBUG
				new NamedParameter("url","http://naracea.com/beta/VERSION.txt?app")
#else
				new NamedParameter("url","http://naracea.com/release/VERSION.txt?app")
#endif
			);
		}
		#endregion
	}
}
