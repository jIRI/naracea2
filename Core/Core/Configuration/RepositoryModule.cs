﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using System.IO;

namespace Naracea.Core.Configuration {
	internal class RepositoryModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			builder.RegisterType<RepositoryValidator>().As<IRepositoryValidator>().As<RepositoryValidator>().SingleInstance();
			builder.RegisterType<RepositoryContext>().As<IRepositoryContext>().As<RepositoryContext>();
			builder.RegisterType<DocumentRepository>().As<IRepository<IDocument>>().As<DocumentRepository>();
		}
		#endregion
	}
}
