﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Builders;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Autofac.Core;

namespace Naracea.Core.Configuration {
	internal class ModelModule : Naracea.Common.IModule {
		#region IConfigurator Members
		public void Configure(Autofac.ContainerBuilder builder) {
			//entities
			builder.RegisterType<Author>().As<IAuthor>();
			builder.RegisterType<Branch>().As<IBranch>();
			builder.RegisterType<BranchArgs>().As<BranchArgs>().As<IBranchArgs>();
			builder.RegisterType<Document>().As<IDocument>();
			//changes
			builder.RegisterType<InsertText>();
			builder.RegisterType<DeleteText>();
			builder.RegisterType<BackspaceText>();
			builder.RegisterType<Undo>();
			builder.RegisterType<Redo>();
			builder.RegisterType<MultiUndo>();
			builder.RegisterType<GroupBegin>();
			builder.RegisterType<GroupEnd>();
			//change args
			builder.RegisterType<ChangeArgs>().As<ChangeArgs>().As<IChangeArgs>();
			builder.RegisterType<WithTextChangeArgs>().As<WithTextChangeArgs>().As<IWithTextChangeArgs>();
			builder.RegisterType<WithChangeChangeArgs>().As<WithChangeChangeArgs>().As<IWithChangeChangeArgs>();
			builder.RegisterType<WithChangeCollectionChangeArgs>().As<WithChangeCollectionChangeArgs>().As<IWithChangeCollectionChangeArgs>();
			//stuff
			builder.RegisterType<IdProvider>().As<IIdProvider>();
		}
		#endregion
	}
}
