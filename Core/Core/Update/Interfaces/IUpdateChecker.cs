﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Core.Update {
	public interface IUpdateChecker {
		void Abort();
		bool Check(Version currentVerion, out Version availableVersion);
	}
}
