﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Core.Update {
	internal class UpdateChecker : IUpdateChecker {
		#region Private attributes
		string _updateDescriptorUrl;
		WebRequest _request;
		#endregion

		#region ctors
		public UpdateChecker(string url) {
			_updateDescriptorUrl = url;
		}
		#endregion

		#region IUpdater Members
		public void Abort() {
			var req = _request;
			if( req != null) {
				req.Abort();
			}
		}

		public bool Check(Version currentVersion, out Version availableVersion) {
			bool hasUpdate = false;
			availableVersion = null;
			try {
				_request = WebRequest.Create(_updateDescriptorUrl);

				using( var response = _request.GetResponse() )
				using( var responseStream = response.GetResponseStream() )
				using( var reader = new StreamReader(responseStream) ) {
					var fileContent = reader.ReadToEnd();
					if( Version.TryParse(fileContent, out availableVersion) ) {
						hasUpdate = availableVersion.CompareTo(currentVersion) > 0;
					}
				}
			} catch {
				// is anything goes wrong, there is no update available
			}
			return hasUpdate;
		}
		#endregion
	}
}
