﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core;
using System.Diagnostics;
using Naracea.Common.Extensions;
using System.Threading.Tasks;

namespace Naracea.Core {
	internal class TextEditorProxy : ITextEditorProxy {
		ITextEditor _textEditor;
		StringBuilder _textCache = new StringBuilder(1000);
		object _syncLock = new object();

		private bool IsEditorReady {
			get {
				return !this.IsEditing && _textEditor != null;
			}
		}

		public TextEditorProxy()
			: this(null) {
		}

		public TextEditorProxy(ITextEditor textEditor) {
			_textEditor = textEditor;
			this.IsEditing = false;
		}

		#region ITextEditor Members
		public event EventHandler<TextChangedEventArgs> ProxiedTextChanged;

		public string Text {
			get {
				lock( _syncLock ) {
					return _textCache.ToString();
				}
			}
			set {
				int oldLength = 0;
				lock( _syncLock ) {
					oldLength = _textCache.Length;
					_textCache.Clear();
					_textCache.Append(value);
					if( this.IsEditorReady ) {
						_textEditor.Text = value;
					}
				}
				this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(0, value.Length, oldLength));
			}
		}

		public int CaretPosition { get; set; }

		public void InsertTextBeforePosition(int pos, string text) {
			lock( _syncLock ) {
				if( pos == _textCache.Length ) {
					_textCache.Append(text);
				} else {
					_textCache.Insert(pos, text);
				}
				this.CaretPosition = pos + text.Length;
				if( this.IsEditorReady ) {
					_textEditor.InsertTextBeforePosition(pos, text);
				}
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, text.Length, 0));
		}

		public void InsertTextBehindPosition(int pos, string text) {
			lock( _syncLock ) {
				if( pos == _textCache.Length ) {
					_textCache.Append(text);
				} else {
					_textCache.Insert(pos, text);
				}
				this.CaretPosition = pos;
				if( this.IsEditorReady ) {
					_textEditor.InsertTextBehindPosition(pos, text);
				}
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, text.Length, 0));
		}

		public void DeleteText(int pos, string text) {
			lock( _syncLock ) {
				this.SanitizeRemovedText(pos, text);
				_textCache.Remove(pos, text.Length);
				this.CaretPosition = pos;
				if( this.IsEditorReady ) {
					_textEditor.DeleteText(pos, text);
				}
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, 0, text.Length));
		}

		public void BackspaceText(int pos, string text) {
			lock( _syncLock ) {
				this.SanitizeRemovedText(pos, text);
				_textCache.Remove(pos, text.Length);
				this.CaretPosition = pos;
				Debug.Assert(this.CaretPosition >= 0);
				if( this.IsEditorReady ) {
					_textEditor.BackspaceText(pos, text);
				}
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, 0, text.Length));
		}

		public void SetTextSilently(string text) {
			int oldLength = 0;
			lock( _syncLock ) {
				oldLength = _textCache.Length;
				_textCache.Clear();
				_textCache.Append(text);
				if( this.IsEditorReady ) {
					_textEditor.SetTextSilently(text);
				}
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(0, text.Length, oldLength));
		}

		public void StoringInsert(int pos, string text) {
			lock( _syncLock ) {
				_textCache.Insert(pos, text);
				this.CaretPosition = pos + text.Length;
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, text.Length, 0));
		}

		public void StoringDelete(int pos, string text) {
			lock( _syncLock ) {
				this.SanitizeRemovedText(pos, text);
				_textCache.Remove(pos, text.Length);
				this.CaretPosition = pos;
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, 0, text.Length));
		}

		public void StoringBackspace(int pos, string text) {
			lock( _syncLock ) {
				this.SanitizeRemovedText(pos, text);
				_textCache.Remove(pos, text.Length);
				this.CaretPosition = pos;
			}
			this.ProxiedTextChanged.Raise(this, new TextChangedEventArgs(pos, 0, text.Length));
		}

		public void BeginEdit() {
			this.IsEditing = true;
		}

		public void EndEdit() {
			this.IsEditing = false;
			lock( _syncLock ) {
				_textEditor.SetTextSilently(this.Text);
				_textEditor.CaretPosition = this.CaretPosition;
			}
		}
				
		public void BeginUpdate() {
			_textEditor.BeginUpdate();
		}

		public void EndUpdate() {
			_textEditor.EndUpdate();
		}

		public bool IsEditing { get; private set; }

		#endregion

		#region Private methods
		void SanitizeRemovedText(int pos, string text) {
			if( pos + text.Length > _textCache.Length ) {
				// we cannot do anything to correctthe state of the document at this point
				// since this situation can result in document corruption, thereofre we throw exception and let application die...
				throw new InvalidOperationException("Inconsistent document state.");
			}
		}
		#endregion
	}
}
