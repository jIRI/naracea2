﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Markdown {
	public interface IMarkupConverter {
		string ToHtmlFragment(string markupText);
		string ToHtmlDocument(string markupText, string styleFile);
		string ToXamlFragment(string markupText);
		string ToXamlDocument(string markupText);
	}
}
