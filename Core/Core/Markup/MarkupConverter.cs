﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Markdown {
	internal class MarkupConverter : IMarkupConverter {

		public MarkupConverter() {
		}

		#region IMarkupConverter Members
		public string ToHtmlFragment(string markupText) {
			return Textile.TextileFormatter.FormatString(markupText);
		}
		
		public string ToHtmlDocument(string markupText, string styleFile) {
			string html = Textile.TextileFormatter.FormatString(markupText);
			string docced = String.Format(
				"<html>\n" +
				"<head>\n" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
				"<link href=\"{0}\" rel=\"Stylesheet\" type=\"text/css\" />\n" +
				"</head>\n" +
				"<body>\n" +
					"{1}\n" +
				"</body>\n" +
			"</html>\n", 
			styleFile, html);
			return docced;
		}

		public string ToXamlFragment(string markupText) {
			string html = Textile.TextileFormatter.FormatString(markupText);
			return HTMLConverter.HtmlToXamlConverter.ConvertHtmlToXaml(html, false);
		}

		public string ToXamlDocument(string markupText) {
			string html = Textile.TextileFormatter.FormatString(markupText);
			return HTMLConverter.HtmlToXamlConverter.ConvertHtmlToXaml(html, true);
		}
		#endregion
	}
}
