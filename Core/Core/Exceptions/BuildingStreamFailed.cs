﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Exceptions {
	public class BuildingStreamFailed : Naracea.Common.Exceptions.NaraceaException {
		public BuildingStreamFailed(string file, Exception innerException)
			: base(string.Format(Properties.Resources.StreamBuilderFailedException, file), innerException) {
		}
	}
}
