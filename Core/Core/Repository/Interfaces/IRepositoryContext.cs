﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Repository {
	public delegate IRepositoryContext RepositoryContextFactory();

	public interface IRepositoryContext {
		string Path { get; set; }
		IRepositoryValidator Validator { get; }
		/// <summary>
		/// When version is not set explicitly, use latest version.
		/// </summary>
		Version RequiredFormatVersion { get; set; }
		IFileHeaderReader FileHeaderReader { get;}
		IFileStreamBuilder StreamBuilder { get; }
	}
}
