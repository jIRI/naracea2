﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;

namespace Naracea.Core.Repository {
	public interface IRepository<TDomain> : IDisposable where TDomain : class {
		void Save(TDomain entity);
		TDomain Load();
		Version FormatVersion { get; }
	}
}
