﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository {
	public interface IRepositoryValidator {
		bool Exists(string path);
	}
}
