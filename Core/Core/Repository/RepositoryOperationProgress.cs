﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository {
	public class RepositoryOperationProgress : EventArgs {
		public bool Cancel { get; set; }
		public int PercentDone { get; private set; }
		
		public RepositoryOperationProgress(int percentDone) {
			this.PercentDone = percentDone;
		}
	}
}
