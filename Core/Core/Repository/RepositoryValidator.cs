﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository {
	internal class RepositoryValidator : IRepositoryValidator{
		#region IRepositoryValidator Members
		public bool Exists(string path) {
			return File.Exists(path);
		}
		#endregion
	}
}
