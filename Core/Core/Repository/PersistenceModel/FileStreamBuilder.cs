﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ionic.Zlib;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileStreamBuilder : IFileStreamBuilder {
		public FileStreamBuilder() {
		}

		public System.IO.Stream CreatePlainStream(string path) {
			return new FileStream(path, FileMode.OpenOrCreate);
		}

		public System.IO.Stream CreateWriteStream(Stream stream) {
			return new DeflateStream(stream, CompressionMode.Compress, CompressionLevel.Level4, true);
		}

		public System.IO.Stream CreateReadStream(Stream stream) {
			return new DeflateStream(stream, CompressionMode.Decompress, true);
		}
	}
}
