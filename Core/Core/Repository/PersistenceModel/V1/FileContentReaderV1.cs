﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Repository.PersistenceModel.V1 {
	internal class FileContentReaderV1 : IFileContentReader<IDocumentDtoV1> {
		ISerializer _serializer;
		public FileContentReaderV1(ISerializer serializer) {
			_serializer = serializer;
		}

		public IDocumentDtoV1 Read(System.IO.Stream stream) {
			return _serializer.Deserialize<DocumentDtoV1>(stream);
		}
	}
}
