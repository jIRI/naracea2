﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using System.Diagnostics;

namespace Naracea.Core.Repository.PersistenceModel.V1 {
	internal class FileReaderV1 : IFileReader {
		IFileHeaderReader _headerReader;
		IFileMetadataReader _metadataReader;
		IFileContentReader<IDocumentDtoV1> _contentReader;
		IDocumentMapper<IDocumentDtoV1> _mapper;

		public FileReaderV1(
			IFileHeaderReader headerReader,
			IFileMetadataReader metadataReader,
			IFileContentReader<IDocumentDtoV1> contentReader,
			IDocumentMapper<IDocumentDtoV1> mapper
		) {
			_headerReader = headerReader;
			_metadataReader = metadataReader;
			_contentReader = contentReader;
			_mapper = mapper;
		}

		public IFileHeader ReadHeader(System.IO.Stream stream) {
			var header = _headerReader.Read(stream);
			return header;
		}

		public IDocument ReadBody(System.IO.Stream stream) {
			var metadata = _metadataReader.Read(stream);
			var content = _contentReader.Read(stream);
			var document = _mapper.DomainFromData(content, metadata);
			return document;
		}
	}
}
