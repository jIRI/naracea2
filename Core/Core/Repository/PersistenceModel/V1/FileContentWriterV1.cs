﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Repository.PersistenceModel.V1 {
	internal class FileContentWriterV1 : IFileContentWriter<IDocumentDtoV1> {
		ISerializer _serializer;
		public FileContentWriterV1(ISerializer serializer) {
			_serializer = serializer;
		}

		public void Write(System.IO.Stream stream, IDocumentDtoV1 content) {
			_serializer.Serialize<DocumentDtoV1>(stream, content as DocumentDtoV1);
		}
	}
}
