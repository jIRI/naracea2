﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Repository.PersistenceModel.V1 {
	internal class FileWriterV1 : IFileWriter {
		IFileHeader _header;
		IFileHeaderWriter _headerWriter;
		IFileMetadataWriter _metadataWriter;
		IFileContentWriter<IDocumentDtoV1> _contentWriter;
		IDocumentMapper<IDocumentDtoV1> _mapper;

		public FileWriterV1(
			IFileHeader header,
			IFileHeaderWriter headerWriter,
			IFileMetadataWriter metadataWriter,
			IFileContentWriter<IDocumentDtoV1> contentWriter,
			IDocumentMapper<IDocumentDtoV1> mapper
		) {
			_header = header;
			_headerWriter = headerWriter;
			_metadataWriter = metadataWriter;
			_contentWriter = contentWriter;
			_mapper = mapper;
		}

		public void WriteHeader(System.IO.Stream stream) {
			_headerWriter.Write(stream, _header);
		}

		public void WriteBody(System.IO.Stream stream, IDocument document) {
			IFileMetadata metadata;
			var dto = _mapper.DataFromDomain(document, out metadata);
			_metadataWriter.Write(stream, metadata);
			_contentWriter.Write(stream, dto);
		}
	}
}
