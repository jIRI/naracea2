﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Common.Collections;

namespace Naracea.Core.Repository.PersistenceModel.V1 {
	internal class DocumentMapperV1 : IDocumentMapper<IDocumentDtoV1> {
		#region Attributes
		ICoreFactories _coreFactories;
		Func<IFileMetadata> _metadataFactory;
		#endregion

		#region ctors
		public DocumentMapperV1(ICoreFactories coreFactories, Func<IFileMetadata> metadataFactory) {
			_coreFactories = coreFactories;
			_metadataFactory = metadataFactory;
		}
		#endregion

		#region Public members
		public Model.IDocument DomainFromData(IDocumentDtoV1 data, IFileMetadata metadata) {
			var domain = this.DoMappingDomainFromDto(data);
			this.RestoreMetadata(domain, metadata);
			return domain;
		}

		public IDocumentDtoV1 DataFromDomain(IDocument domain, out IFileMetadata metadata) {
			metadata = this.StoreMetadata(domain);
			return this.DoMappingDtoFromDomain(domain);
		}
		#endregion

		#region Dto from domain
		IFileMetadata StoreMetadata(IDocument domain) {
			IFileMetadata metadata = _metadataFactory();
			foreach( var key in domain.Metadata.Data.Keys ) {
				metadata.SetValue(key, domain.Metadata.Get(key));
			}
			return metadata;
		}

		IDocumentDtoV1 DoMappingDtoFromDomain(IDocument source) {
			IDocumentDtoV1 dest = new DocumentDtoV1() {
				Authors = new List<AuthorDtoV1>(
						from author in source.Authors
						select this.AuthorDtoFromDomain(author)
					),
				Branches = new List<BranchDtoV1>(
											from branch in source.Branches
											select this.BranchDtoFromDomain(source, branch)
					),
				Comment = source.Comment,
				CurrentBranchIndex = source.Branches.IndexOf(source.CurrentBranch),
				Name = source.Name
			};

			return dest;
		}

		AuthorDtoV1 AuthorDtoFromDomain(IAuthor author) {
			return new AuthorDtoV1 {
				Name = author.Name,
				Username = author.Username
			};
		}

		BranchDtoV1 BranchDtoFromDomain(IDocument source, IBranch branch) {
			return new BranchDtoV1 {
				AuthorIndex = source.Authors.IndexOf(branch.Author),
				BranchingDateTimeTicks = branch.BranchingDateTime.Ticks,
				BranchingChangeIndex = branch.BranchingChangeIndex,
				Comment = branch.Comment,
				CurrentChangeIndex = branch.CurrentChangeIndex,
				Changes = new List<ChangeDtoV1>(
					from change in branch.Changes.OwnedChanges select this.ChangeDtoFromDomain(source.Authors, branch.Changes, change)
				),
				Name = branch.Name,
				ParentBranchIndex = branch.Parent != null ? source.Branches.IndexOf(branch.Parent) : -1,
				Metadata = new SerializableDictionary<string, string>(branch.Metadata.Data)
			};
		}
		
		ChangeDtoV1 ChangeDtoFromDomain(IList<IAuthor> authors, IChangesCollection changes, IChange source) {
			ChangeDtoV1 dest = null;
			if( source is InsertText ) {
				var change = source as InsertText;
				dest = this.CreateChangeWithTextDto(authors, change, ChangeTypes.Insert);
			} else if( source is DeleteText ) {
				var change = source as DeleteText;
				dest = this.CreateChangeWithTextDto(authors, change, ChangeTypes.Delete);
			} else if( source is BackspaceText ) {
				var change = source as BackspaceText;
				dest = this.CreateChangeWithTextDto(authors, change, ChangeTypes.Backspace);
			} else if( source is Undo ) {
				var change = source as Undo;
				dest = this.CreateChangeWithChangeDto(authors, changes, change, ChangeTypes.Undo);
			} else if( source is Redo ) {
				var change = source as Redo;
				dest = this.CreateChangeWithChangeDto(authors, changes, change, ChangeTypes.Redo);
			} else if( source is MultiUndo ) {
				var change = source as MultiUndo;
				dest = this.CreateChangeWithChangeCollectionDto(authors, changes, change, ChangeTypes.MultiUndo);
			} else if( source is GroupBegin ) {
				var change = source as GroupBegin;
				dest = this.CreateChangeWithShiftCountDto(authors, change, ChangeTypes.GroupBegin);
			} else if( source is GroupEnd ) {
				var change = source as GroupEnd;
				dest = this.CreateChangeWithChangeCollectionDto(authors, changes, change, ChangeTypes.GroupEnd);
			} else {
				throw new NotSupportedException();
			}
			return dest;
		}

		ChangeDtoV1 CreateChangeWithShiftCountDto(IList<IAuthor> authors, IChange change, ChangeTypes type) {
			return new ChangeDtoV1 {
				Type = type,
				AuthorIndex = authors.IndexOf(change.Author),
				DateTimeTicks = change.DateTime.Ticks,
				IntData = change.ShiftCount,
			};
		}

		ChangeDtoV1 CreateChangeWithTextDto(IList<IAuthor> authors, IChangeWithText change, ChangeTypes type) {
			return new ChangeDtoV1 {
				Type = type,
				AuthorIndex = authors.IndexOf(change.Author),
				DateTimeTicks = change.DateTime.Ticks,
				IntData = change.Position,
				StringData = change.Text,
			};
		}

		ChangeDtoV1 CreateChangeWithChangeDto(IList<IAuthor> authors, IChangesCollection changes, IChangeWithChange change, ChangeTypes type) {
			return new ChangeDtoV1 {
				Type = type,
				AuthorIndex = authors.IndexOf(change.Author),
				DateTimeTicks = change.DateTime.Ticks,
				IntData = changes.IndexOf(change.Change),
			};
		}

		ChangeDtoV1 CreateChangeWithChangeCollectionDto(IList<IAuthor> authors, IChangesCollection changes, IChangeWithChangeCollection change, ChangeTypes type) {
			return new ChangeDtoV1 {
				Type = type,
				AuthorIndex = authors.IndexOf(change.Author),
				DateTimeTicks = change.DateTime.Ticks,
				IntData = changes.IndexOf(change.Changes.FirstOrDefault()),
				// i know, I'm quite dumb here, but this seems like reasonable solution for now.
				StringData = changes.IndexOf(change.Changes.LastOrDefault()).ToString()
			};
		}
		#endregion

		#region Domain from Dto
		void RestoreMetadata(IDocument domain, IFileMetadata metadata) {
			foreach( var pair in metadata.Data ) {
				domain.Metadata.Set(pair.Key, pair.Value);
			}
		}

		IDocument DoMappingDomainFromDto(IDocumentDtoV1 source) {
			IDocument dest = _coreFactories.CreateDocument();
			dest.Comment = source.Comment;
			dest.Name = source.Name;
			//authors
			foreach( var authorDto in source.Authors ) {
				var author = AuthorDomainFromDto(authorDto);
				dest.Authors.Add(author);
			}

			//branches
			foreach( var branchDto in source.Branches ) {
				var branch = BranchDomainFromDto(dest, branchDto);
				dest.Branches.Add(branch);
			}

			if( source.CurrentBranchIndex >= 0 ) {
				dest.CurrentBranch = dest.Branches[source.CurrentBranchIndex];
			}

			return dest;
		}

		IAuthor AuthorDomainFromDto(IAuthorDtoV1 authorDto) {
			var author = _coreFactories.CreateAuthor();
			author.Name = authorDto.Name;
			author.Username = authorDto.Username;
			return author;
		}

		IBranch BranchDomainFromDto(IDocument document, IBranchDtoV1 branchDto) {
			var branch = _coreFactories.CreateBranch(
				document.Authors[branchDto.AuthorIndex],
				branchDto.ParentBranchIndex != -1 ? document.Branches[branchDto.ParentBranchIndex] : null,
				branchDto.BranchingChangeIndex,
				new DateTimeOffset(branchDto.BranchingDateTimeTicks, new TimeSpan())
			);
			branch.Name = branchDto.Name;
			branch.Comment = branchDto.Comment;
			//changes
			foreach( var changeDto in branchDto.Changes ) {
				var change = this.ChangeDomainFromDto(document, branch.Changes, changeDto);
				branch.Changes.Add(change);
			}
			branch.CurrentChangeIndex = branchDto.CurrentChangeIndex;
			//metadata
			foreach(var metadataKey in branchDto.Metadata.Keys) {
				branch.Metadata.Set(metadataKey, branchDto.Metadata[metadataKey]);
			}
			return branch;
		}

		private IChange ChangeDomainFromDto(IDocument document, IChangesCollection changes, IChangeDtoV1 changeDto) {
			IChange change;
			var dto = changeDto as ChangeDtoV1;
			switch( changeDto.Type ) {
				case ChangeTypes.Insert:
					change = _coreFactories.CreateInsertText(
						dto.IntData,
						dto.StringData,
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.Delete:
					change = _coreFactories.CreateDeleteText(
						dto.IntData,
						dto.StringData,
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.Backspace:
					change = _coreFactories.CreateBackspaceText(
						dto.IntData,
						dto.StringData,
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.Undo:
					change = _coreFactories.CreateUndo(
						changes.At(dto.IntData),
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.Redo:
					change = _coreFactories.CreateRedo(
						changes.At(dto.IntData),
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.MultiUndo: 
					int firstUndone = dto.IntData;
					int lastUndone = int.Parse(dto.StringData);
					var undoneChanges = new List<IChange>();
					for( int i = firstUndone; i <= lastUndone; i++ ) {
						undoneChanges.Add(changes.At(i));
					}
					change = _coreFactories.CreateMultiUndo(
						undoneChanges,
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;
				case ChangeTypes.GroupBegin:
					change = _coreFactories.CreateGroupBegin(
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					(change as GroupBegin).SetShiftCount(dto.IntData);
					break;
				case ChangeTypes.GroupEnd:
					int firstInGroup = dto.IntData;
					int lastInGroup = int.Parse(dto.StringData);
					var groupedChanges = new List<IChange>();
					if( firstInGroup != -1 && lastInGroup != -1 ) {
						for( int i = firstInGroup; i <= lastInGroup; i++ ) {
							groupedChanges.Add(changes.At(i));
						}
					}
					change = _coreFactories.CreateGroupEnd(
						groupedChanges,
						document.Authors[dto.AuthorIndex],
						new DateTimeOffset(dto.DateTimeTicks, new TimeSpan())
					);
					break;

				default:
					throw new NotSupportedException();
			}
			return change;
		}

		#endregion
	}
}
