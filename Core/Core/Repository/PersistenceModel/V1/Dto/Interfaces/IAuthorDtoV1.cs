﻿using System;
namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	public interface IAuthorDtoV1 {
		string Name { get; set; }
		string Username { get; set; }
	}
}
