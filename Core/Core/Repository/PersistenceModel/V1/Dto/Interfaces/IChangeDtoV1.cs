﻿using System;
using System.Xml.Serialization;
namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	public enum ChangeTypes {
		[XmlEnum(Name = "i")]
		Insert,
		[XmlEnum(Name = "d")]
		Delete,
		[XmlEnum(Name = "b")]
		Backspace,
		[XmlEnum(Name = "u")]
		Undo,
		[XmlEnum(Name = "r")]
		Redo,
		[XmlEnum(Name = "mu")]
		MultiUndo,
		[XmlEnum(Name = "gb")]
		GroupBegin,
		[XmlEnum(Name = "ge")]
		GroupEnd,
	}

	public interface IChangeDtoV1 {
		int AuthorIndex { get; set; }
		long DateTimeTicks { get; set; }
		ChangeTypes Type { get; set; }
		int IntData { get; set; }
		string StringData { get; set; }
	}
}
