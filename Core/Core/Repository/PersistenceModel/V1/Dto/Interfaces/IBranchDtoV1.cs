﻿using System;
using System.Collections.Generic;
using Naracea.Common.Collections;
namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	public interface IBranchDtoV1 {
		int AuthorIndex { get; set; }
		long BranchingDateTimeTicks { get; set; }
		int BranchingChangeIndex { get; set; }
		string Comment { get; set; }
		int CurrentChangeIndex { get; set; }
		List<ChangeDtoV1> Changes { get; set; }
		SerializableDictionary<string, string> Metadata { get; set; }
		string Name { get; set; }
		int ParentBranchIndex { get; set; }
	}
}
