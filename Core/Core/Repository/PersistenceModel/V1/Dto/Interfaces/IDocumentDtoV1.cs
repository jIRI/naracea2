﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	public interface IDocumentDtoV1 {
		string Name { get; set; }
		string Comment { get; set; }
		List<BranchDtoV1> Branches { get; set; }
		List<AuthorDtoV1> Authors { get; set; }
		int CurrentBranchIndex { get; set; }
	}
}
