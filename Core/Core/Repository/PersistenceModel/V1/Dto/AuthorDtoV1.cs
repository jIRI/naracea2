﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	[Serializable()]
	public class AuthorDtoV1 : Naracea.Core.Repository.PersistenceModel.V1.Dto.IAuthorDtoV1 {
		public string Username { get; set; }
		public string Name { get; set; }
	}
}
