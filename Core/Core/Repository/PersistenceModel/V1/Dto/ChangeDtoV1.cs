﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	public class ChangeDtoV1 : Naracea.Core.Repository.PersistenceModel.V1.Dto.IChangeDtoV1 {
		[XmlElement(ElementName = "tp")]
		public ChangeTypes Type { get; set; }
		[XmlElement(ElementName = "t")]
		public long DateTimeTicks { get; set; }
		[XmlElement(ElementName = "a")]
		public int AuthorIndex { get; set; }
		[XmlElement(ElementName = "i")]
		public int IntData { get; set; }
		[XmlElement(ElementName = "s")]
		public string StringData { get; set; }
	}
}
