﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Collections;
using System.Xml.Serialization;

namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	[Serializable()]
	public class BranchDtoV1 : IBranchDtoV1 {
		public string Name { get; set; }
		public string Comment { get; set; }
		public int AuthorIndex { get; set; }
		public int ParentBranchIndex { get; set; }
		public int BranchingChangeIndex { get; set; }
		public long BranchingDateTimeTicks { get; set; }
		public int CurrentChangeIndex { get; set; }
		[XmlArrayItem(ElementName = "c")]
		public List<ChangeDtoV1> Changes { get; set; }
		public SerializableDictionary<string, string> Metadata { get; set; }
	}
}
