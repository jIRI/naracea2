﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel.V1.Dto {
	[Serializable()]
	public class DocumentDtoV1 : IDocumentDtoV1 {
		public string Name { get; set; }
		public string Comment { get; set; }
		public List<BranchDtoV1> Branches { get; set; }
		public List<AuthorDtoV1> Authors { get; set; }
		public int CurrentBranchIndex { get; set; }
	}
}
