﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileMetadataWriter : IFileMetadataWriter {
		ISerializer _serializer;
		public FileMetadataWriter(ISerializer serializer) {
			_serializer = serializer;
		}

		public void Write(System.IO.Stream stream, IFileMetadata metadata) {
			var list = from i in metadata.Data
								 select i as MetadataKeyValuePair;

			//we need to get the length of the metadata first, so we use the mem stream to serialize it separately
			using( var memStream = new MemoryStream() ) {
				_serializer.Serialize(memStream, new List<MetadataKeyValuePair>(list));
				var lengthBuff = BitConverter.GetBytes((UInt32)memStream.Length);
				stream.Write(lengthBuff, 0, lengthBuff.Length);
				var buffer = memStream.ToArray();
				stream.Write(buffer, 0, (int)memStream.Length);
			}
		}
	}
}
