﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using Naracea.Core.Repository.Exceptions;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class PersisterBuilder : IPersisterBuilder {
		class VersionDescriptor {
			public Version Version { get; set; }
			public Func<IFileReader> ReaderBuilder { get; set; }
			public Func<IFileWriter> WriterBuilder { get; set; }
		}

		//Serializer
		Func<ISerializer> _serializerFactory;
		//Header
		Func<Version, IFileHeader> _headerFactory;
		Func<IFileHeaderWriter> _headerWriterFactory;
		Func<Func<Version, IFileHeader>, IFileHeaderReader> _headerReaderFactory;
		//Metadata
		Func<ISerializer, IFileMetadataWriter> _metadataWriterFactory;
		Func<ISerializer, Func<IFileMetadata>, IFileMetadataReader> _metadataReaderFactory;
		Func<IFileMetadata> _metadataFactory;
		//V1
		Func<IDocumentMapper<IDocumentDtoV1>> _mapperV1Factory;
		Func<ISerializer, IFileContentWriter<IDocumentDtoV1>> _contentWriterV1Factory;
		Func<ISerializer, IFileContentReader<IDocumentDtoV1>> _contentReaderV1Factory;
		Func<IFileHeader, IFileHeaderWriter, IFileMetadataWriter, IFileContentWriter<IDocumentDtoV1>, IDocumentMapper<IDocumentDtoV1>, IFileWriter> _writerV1Factory;
		Func<IFileHeaderReader, IFileMetadataReader, IFileContentReader<IDocumentDtoV1>, IDocumentMapper<IDocumentDtoV1>, IFileReader> _readerV1Factory;

		Version _versionV1 = new Version(1, 0);

		public PersisterBuilder(
			//Serializer
			Func<ISerializer> serializerFactory,
			//Header
			Func<Version, IFileHeader> headerFactory,
			Func<IFileHeaderWriter> headerWriterFactory,
			Func<Func<Version, IFileHeader>, IFileHeaderReader> headerReaderFactory,
			//Metadata
			Func<ISerializer, IFileMetadataWriter> metadataWriterFactory,
			Func<ISerializer, Func<IFileMetadata>, IFileMetadataReader> metadataReaderFactory,
			Func<IFileMetadata> metadataFactory,
			//V1
			Func<IDocumentMapper<IDocumentDtoV1>> mapperV1Factory,
			Func<ISerializer, IFileContentWriter<IDocumentDtoV1>> contentWriterV1Factory,
			Func<ISerializer, IFileContentReader<IDocumentDtoV1>> contentReaderV1Factory,
			Func<IFileHeader, IFileHeaderWriter, IFileMetadataWriter, IFileContentWriter<IDocumentDtoV1>, IDocumentMapper<IDocumentDtoV1>, IFileWriter> writerV1Factory,
			Func<IFileHeaderReader, IFileMetadataReader, IFileContentReader<IDocumentDtoV1>, IDocumentMapper<IDocumentDtoV1>, IFileReader> readerV1Factory
		) {
			_serializerFactory = serializerFactory;
			_headerFactory = headerFactory;
			_headerWriterFactory = headerWriterFactory;
			_headerReaderFactory = headerReaderFactory;
			_metadataWriterFactory = metadataWriterFactory;
			_metadataReaderFactory = metadataReaderFactory;
			_metadataFactory = metadataFactory;
			_mapperV1Factory = mapperV1Factory;
			_contentWriterV1Factory = contentWriterV1Factory;
			_contentReaderV1Factory = contentReaderV1Factory;
			_writerV1Factory = writerV1Factory;
			_readerV1Factory = readerV1Factory;

			_suportedVersionDescriptors = new List<VersionDescriptor> {
				new VersionDescriptor {
					Version = _versionV1,
					ReaderBuilder = this.BuildReaderV1,
					WriterBuilder = this.BuildWriterV1
				},
			};
		}

		#region IPersisterBuilder methods
		public Version CurrentVersion {
			get {
				return _suportedVersionDescriptors.Last().Version;
			}
		}

		List<VersionDescriptor> _suportedVersionDescriptors;
		public IEnumerable<Version> SupportedVersions {
			get {
				return from vd in _suportedVersionDescriptors select vd.Version;
			}
		}

		public IFileWriter BuildWriter(Version version) {
			var descriptor = _suportedVersionDescriptors.Find(vd => vd.Version.Equals(version));
			if( descriptor == null ) {
				throw new NotSupportedException();
			}
			return descriptor.WriterBuilder();
		}

		public IFileReader BuildReader(Version version) {
			var descriptor = _suportedVersionDescriptors.Find(vd => vd.Version.Equals(version));
			if( descriptor == null ) {
				throw new NotSupportedException();
			}
			return descriptor.ReaderBuilder();
		}
		#endregion

		#region V1 builders
		IFileWriter BuildWriterV1() {
			var header = _headerFactory(_versionV1);
			var headerWriter = _headerWriterFactory();
			var serializer = _serializerFactory();
			var metadataWriter = _metadataWriterFactory(serializer);
			var contentWriter = _contentWriterV1Factory(serializer);
			var mapper = _mapperV1Factory();
			var writer = _writerV1Factory(header, headerWriter, metadataWriter, contentWriter, mapper);
			return writer;
		}

		IFileReader BuildReaderV1() {
			var headerReader = _headerReaderFactory(_headerFactory);
			var serializer = _serializerFactory();
			var metadataReader = _metadataReaderFactory(serializer, _metadataFactory);
			var contentReader = _contentReaderV1Factory(serializer);
			var mapper = _mapperV1Factory();
			var reader = _readerV1Factory(headerReader, metadataReader, contentReader, mapper);
			return reader;
		}
		#endregion
	}
}
