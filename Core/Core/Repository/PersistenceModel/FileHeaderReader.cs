﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileHeaderReader : IFileHeaderReader {
		Func<Version, IFileHeader> _fileHeaderFactory;
		public FileHeaderReader(Func<Version, IFileHeader> fileHeaderFactory) {
			_fileHeaderFactory = fileHeaderFactory;
		}

		public IFileHeader Read(System.IO.Stream stream) {
			int fileFormatIdLen = PersistenceModelConstants.FileFormatIdentifier.Length;
			int headerLen = fileFormatIdLen + PersistenceModelConstants.VersionDescriptorLength;
			byte[] bytes = new byte[headerLen];
			stream.Read(bytes, 0, headerLen);

			//check whether this is proper file format, and if not, return null
			int index = 0;
			foreach( var c in PersistenceModelConstants.FileFormatIdentifier ) {
				if( (byte)c != bytes[index++] ) {
					return null;
				}
			}

			//read version
			var versionBytes = bytes.Skip(fileFormatIdLen);
			var versionChars = from c in versionBytes select (char)c;
			var versionString = new string(versionChars.ToArray());
			Version ver = null;
			IFileHeader header = null;
			if( Version.TryParse(versionString, out ver) ) {
				header = _fileHeaderFactory(ver);
			}
			return header;
		}
	}
}
