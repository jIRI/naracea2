﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class Serializer : ISerializer {
		public Serializer() {
		}

		public void Serialize<T>(System.IO.Stream writer, T data) where T : class {
			XmlWriterSettings settings = new XmlWriterSettings {
				Indent = false,
				CheckCharacters = false
			};
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			using( var w = XmlWriter.Create(writer, settings) ) {
				serializer.Serialize(w, data);
			}
		}

		public T Deserialize<T>(System.IO.Stream reader) where T : class {
			var settings = new XmlReaderSettings { 
				CheckCharacters = false 
			};
			using( var r = XmlReader.Create(reader, settings) ) {
				var serializer = new XmlSerializer(typeof(T));
				T result = serializer.Deserialize(r) as T;
				return result;
			}
		}
	}
}
