﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	[Serializable()]
	public class MetadataKeyValuePair : IMetadataKeyValuePair {
		public MetadataKeyValuePair() {
		}

		public string Key { get; set; }
		public string Value {get; set;}

		// override object.Equals
		public override bool Equals(object obj) {
			if( obj == null || GetType() != obj.GetType() ) {
				return false;
			}
			
			var o = obj as MetadataKeyValuePair;
			return this.Key.Equals(o.Key) && this.Value.Equals(o.Value);
		}

		// override object.GetHashCode
		public override int GetHashCode() {
			return this.Key.GetHashCode() ^ this.Value.GetHashCode();
		}
	}
}
