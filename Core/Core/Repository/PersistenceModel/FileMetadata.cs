﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Common.Extensions;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileMetadata : IFileMetadata {
		Func<IMetadataKeyValuePair> _metadataKeyValuePairFactory;

		public FileMetadata(Func<IMetadataKeyValuePair> metadataKeyValuePairFactory) {
			_data = new Dictionary<string, string>();

			Debug.Assert(metadataKeyValuePairFactory != null);
			_metadataKeyValuePairFactory = metadataKeyValuePairFactory;
		}

		Dictionary<string, string> _data;
		public IEnumerable<IMetadataKeyValuePair> Data {
			get {
				var list = new List<IMetadataKeyValuePair>();
				foreach( var item in _data ) {
					var metadataPair = _metadataKeyValuePairFactory();
					metadataPair.Key = item.Key;
					metadataPair.Value = item.Value;
					list.Add(metadataPair);
				}
				return list;
			}
		}

		public bool TryGetValue(string key, out string value) {
			value = null;
			if( key == null || !_data.ContainsKey(key) ) {
				return false;
			}
			value = _data[key];
			return true;
		}

		public void SetValue(string key, string value) {
			if( !_data.ContainsKey(key) ) {
				_data.Add(key, value);
			} else {
				_data[key] = value;
			}
		}

		public void SetValues(IEnumerable<IMetadataKeyValuePair> values) {
			_data.Clear();
			foreach( var item in values ) {
				_data.Add(item.Key, item.Value);
			}
		}
	}
}
