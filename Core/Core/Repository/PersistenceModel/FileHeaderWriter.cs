﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileHeaderWriter : IFileHeaderWriter {
		public FileHeaderWriter() {
		}

		public void Write(System.IO.Stream stream, IFileHeader header) {
			var headerString = header.FormatIdentifier
				+ string.Format("{0}.{1}", header.Version.Major, header.Version.Minor)
						.PadRight(PersistenceModelConstants.VersionDescriptorLength);
			
			var byteBuffer = (from c in headerString select (byte)c).ToArray();
			
			stream.Write(byteBuffer, 0, byteBuffer.Length);
		}
	}
}
