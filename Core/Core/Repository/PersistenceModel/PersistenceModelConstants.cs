﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	public static class PersistenceModelConstants {
		public const int VersionDescriptorLength = 16;
		public readonly static string FileFormatIdentifier = "NCD";
	}
}
