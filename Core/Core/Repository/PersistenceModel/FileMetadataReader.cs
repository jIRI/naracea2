﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileMetadataReader : IFileMetadataReader {
		ISerializer _serializer;
		Func<IFileMetadata> _metadataFactory;

		public FileMetadataReader(ISerializer serializer, Func<IFileMetadata> metadataFactory) {
			_serializer = serializer;
			_metadataFactory = metadataFactory;
		}

		public IFileMetadata Read(System.IO.Stream stream) {
			//read length
			var lengthBuff = new byte[sizeof(UInt32)];
			stream.Read(lengthBuff, 0, lengthBuff.Length);
			var metadataLength = BitConverter.ToUInt32(lengthBuff, 0);
			//read actual metadata
			var metadataBuff = new byte[metadataLength];
			stream.Read(metadataBuff, 0, metadataBuff.Length);

			//now get data
			var metadata = _metadataFactory();
			using( var memStream = new MemoryStream(metadataBuff) ) {
				var metadataCollection = _serializer.Deserialize<List<MetadataKeyValuePair>>(memStream);
				metadata.SetValues(metadataCollection);
			}

			return metadata;
		}
	}
}
