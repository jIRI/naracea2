﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileWriter {
		void WriteHeader(Stream stream);
		void WriteBody(Stream stream, IDocument document);
	}
}
