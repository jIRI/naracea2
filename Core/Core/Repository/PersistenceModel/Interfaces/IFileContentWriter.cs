﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileContentWriter<T> where T : class {
		void Write(Stream stream, T content);
	}
}
