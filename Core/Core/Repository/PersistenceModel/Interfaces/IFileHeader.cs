﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileHeader {
		string FormatIdentifier { get; }
		Version Version { get; }
	}
}
