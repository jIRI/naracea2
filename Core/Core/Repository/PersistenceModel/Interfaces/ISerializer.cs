﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface ISerializer {
		void Serialize<T>(Stream writer, T data) where T : class;
		T Deserialize<T>(Stream reader) where T : class;
	}
}
