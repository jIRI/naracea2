﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileContentReader<T> where T : class {
		T Read(Stream stream);
	}
}
