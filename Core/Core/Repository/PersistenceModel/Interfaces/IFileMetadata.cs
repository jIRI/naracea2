﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IMetadataKeyValuePair {
		string Key { get; set; }
		string Value { get; set; }
	}

	public interface IFileMetadata {
		IEnumerable<IMetadataKeyValuePair> Data { get; }
		bool TryGetValue(string key, out string value);
		void SetValue(string key, string value);
		void SetValues(IEnumerable<IMetadataKeyValuePair> values);
	}
}
