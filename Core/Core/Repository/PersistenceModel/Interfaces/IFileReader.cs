﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileReader {
		IFileHeader ReadHeader(System.IO.Stream stream);
		IDocument ReadBody(Stream stream);
	}
}
