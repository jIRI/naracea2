﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileHeaderWriter {
		void Write(Stream stream, IFileHeader  header);
	}
}
