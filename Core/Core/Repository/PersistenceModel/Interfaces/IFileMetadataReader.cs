﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileMetadataReader {
		IFileMetadata Read(Stream stream);
	}
}
