﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IDocumentMapper<TData> where TData : class {
		IDocument DomainFromData(TData data, IFileMetadata metadata);
		TData DataFromDomain(IDocument domain, out IFileMetadata metadata);
	}
}
