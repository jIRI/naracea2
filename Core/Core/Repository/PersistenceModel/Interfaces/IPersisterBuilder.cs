﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IPersisterBuilder {
		Version CurrentVersion { get; }
		IEnumerable<Version> SupportedVersions { get; }

		IFileWriter BuildWriter(Version version);
		IFileReader BuildReader(Version version);
	}
}
