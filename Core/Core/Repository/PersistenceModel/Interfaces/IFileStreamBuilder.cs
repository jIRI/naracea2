﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.IO;

namespace Naracea.Core.Repository.PersistenceModel {
	public interface IFileStreamBuilder {
		Stream CreatePlainStream(string path);
		Stream CreateWriteStream(Stream stream);
		Stream CreateReadStream(Stream stream);
	}
}
