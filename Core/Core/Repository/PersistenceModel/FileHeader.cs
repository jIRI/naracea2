﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Repository.PersistenceModel {
	internal class FileHeader : IFileHeader {
		public FileHeader(Version version) {
			this.Version = version;
		}

		public string FormatIdentifier {
			get { 
				return PersistenceModelConstants.FileFormatIdentifier; 
			}
		}

		public Version Version { get; private set; }
	}
}
