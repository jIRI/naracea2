﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Repository {
	internal class RepositoryContext : IRepositoryContext {
		public RepositoryContext(
			IRepositoryValidator validator,
			IFileHeaderReader fileHeaderReader,
			IFileStreamBuilder streamBuilder
		) {
			Debug.Assert(validator != null);
			this.Validator = validator;
			Debug.Assert(fileHeaderReader != null);
			this.FileHeaderReader = fileHeaderReader;
			Debug.Assert(streamBuilder != null);
			this.StreamBuilder = streamBuilder;
		}

		#region IContext Members
		public string Path { get; set; }
		public IRepositoryValidator Validator { get; private set; }
		public Version RequiredFormatVersion { get; set; }
		public IFileHeaderReader FileHeaderReader { get; private set; }
		public IFileStreamBuilder StreamBuilder { get; private set; }
		#endregion
	}
}
