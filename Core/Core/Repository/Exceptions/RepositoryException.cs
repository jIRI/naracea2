﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Exceptions;

namespace Naracea.Core.Repository.Exceptions {
	public class RepositoryException : NaraceaException {
		public RepositoryException() {
		}

		public RepositoryException(string message) 
		: base(message) {			
		}

		public RepositoryException(string message, Exception innerException) 
		: base(message, innerException) {
		}
	}
}
