﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Repository.Exceptions;
using System.Diagnostics;
using System.IO;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Repository {
	internal class DocumentRepository : IRepository<IDocument> {
		IRepositoryContext _context;
		IPersisterBuilder _persisterBuilder;
#if DEBUG
		string _creationStackTrace;
#endif

		public DocumentRepository(IRepositoryContext context, IPersisterBuilder persisterBuilder) {
#if DEBUG
			_creationStackTrace = new System.Diagnostics.StackTrace().ToString();
#endif
			_context = context;
			_persisterBuilder = persisterBuilder;
			try {
				using( var stream = _context.StreamBuilder.CreatePlainStream(_context.Path) ) {
					this.DetectVersion(stream);
				}
			} catch( Exception ex ) {
				//needed, otherwise incomplete instance will try to finalize it self
				GC.SuppressFinalize(this);
				throw new RepositoryException(
					ExceptionMessage(Properties.Resources.RepositoryExceptionUnableToOpen),
					ex
				);
			}
		}

		#region IRepository<Document> Members
		public Version FormatVersion { get; private set; }

		public void Save(IDocument entity) {
			try {
				var writer = _persisterBuilder.BuildWriter(this.FormatVersion);
				using( var stream = _context.StreamBuilder.CreatePlainStream(_context.Path) ) {
					writer.WriteHeader(stream);
					using( var writeStream = _context.StreamBuilder.CreateWriteStream(stream) ) {
						writer.WriteBody(writeStream, entity);
					}
				}
			} catch( Exception ex ) {
				throw new RepositoryException(
					ExceptionMessage(Properties.Resources.RepositoryExceptionUnableToSave),
					ex
				);
			}
		}

		public IDocument Load() {
			IDocument document = null;
			try {
				var reader = _persisterBuilder.BuildReader(this.FormatVersion);
				using( var stream = _context.StreamBuilder.CreatePlainStream(_context.Path) ) {
					var header = reader.ReadHeader(stream);
					using( var readStream = _context.StreamBuilder.CreateReadStream(stream) ) {
						document = reader.ReadBody(readStream);
					}
				}
			} catch( Exception ex ) {
				throw new RepositoryException(
					ExceptionMessage(Properties.Resources.RepositoryExceptionUnableToObtainDocument),
					ex
				);
			}
			return document;
		}
		#endregion

		#region Private methods
		void DetectVersion(Stream stream) {
			if( stream.Length > 0 ) {
				//read header first so we know what file format we deal with
				var header = _context.FileHeaderReader.Read(stream);

				//seek to beginning of the stream, so file reader can read the whole file including header
				stream.Seek(0, SeekOrigin.Begin);
				if( header != null && _persisterBuilder.SupportedVersions.Contains(header.Version) ) {
					// it is ensured that header is well formed at this point
					this.FormatVersion = header.Version;
				} else {
					throw new RepositoryException(string.Format(Properties.Resources.RepositoryExceptionNotTextacleFile, _context.Path));
				}
			} else {
				if( _context.RequiredFormatVersion != null ) {
					if( _persisterBuilder.SupportedVersions.Contains(_context.RequiredFormatVersion) ) {
						this.FormatVersion = _context.RequiredFormatVersion;
					} else {
						throw new RepositoryException(string.Format(Properties.Resources.RepositoryExceptionFileformatNotSupported, _context.RequiredFormatVersion));
					}
				} else {
					this.FormatVersion = _persisterBuilder.CurrentVersion;
				}
			}
		}

		string ExceptionMessage(string message, params object[] args) {
			string result =
				string.Format(message, args)
				+ Environment.NewLine
				+ string.Format(Properties.Resources.RepositoryExceptionContextInfo, _context.Path);
			return result;
		}
		#endregion

		#region IDisposable Members
		~DocumentRepository() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
#if DEBUG
			Debug.WriteLine(_creationStackTrace);
#endif
			Debug.Assert(false);
		}

		private bool _disposed = false;
		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
				}
				_disposed = true;
			}
		}
		#endregion

		#region Configuration
		#endregion
	}
}
