﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.Diagnostics;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.FileSystem;
using Naracea.Core.Model.Changes;
using Naracea.Core.Builders;
using Naracea.Common.Exceptions;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Finders.Text;
using Naracea.Core.Spellcheck;
using Naracea.Core.Markdown;
using Naracea.Core.Update;

namespace Naracea.Core {
	internal class CoreFactories : ICoreFactories {
		Func<IRepositoryContext> _repositoryContext;
		IPersisterBuilder _persisterBuilder;
		Func<IRepositoryContext, IPersisterBuilder, IRepository<IDocument>> _documentRepository;
		Func<ShiftToEnd> _shifterAllForward;
		Func<ShiftToStart> _shifterAllBackward;
		Func<int, ShiftToChange> _shifterToChange;
		Func<IFileCopier> _fileCopier;
		Func<IFileRemover> _fileRemover;
		Func<IFileRenamer> _fileRenamer;
		Func<IFileAttributes> _fileAttributes;
		Func<IFileChecker> _fileChecker;
		Func<IFileNameShortener> _fileNameShortener;
		Func<IFileNameValidator> _fileNameValidator;
		Func<IDirectoryMaker> _directoryMaker;
		Func<IDirectoryRemover> _directoryRemover;
		Func<string, IZipReader> _zipReader;
		Func<IAuthor> _author;
		Func<IBranchArgs, IBranch> _branch;
		Func<IDocument> _document;
		Func<IWithTextChangeArgs, InsertText> _insertText;
		Func<IWithTextChangeArgs, DeleteText> _deleteText;
		Func<IWithTextChangeArgs, BackspaceText> _backspaceText;
		Func<IWithChangeChangeArgs, Undo> _undo;
		Func<IWithChangeChangeArgs, Redo> _redo;
		Naracea.Core.Model.BranchArgs.BranchArgsFactory _branchArgsFactory;
		Naracea.Core.Model.Changes.WithTextChangeArgs.WithTextChangeArgsFactory _withTextChangeArgsFactory;
		Naracea.Core.Model.Changes.WithChangeChangeArgs.WithChangeChangeArgsFactory _withChangeChangeArgsFactory;
		BranchBuilder _branchBuilder;
		DocumentBuilder _documentBuilder;
		Func<Func<string, object, IMetadataKeyValuePair>, IEnumerable<IMetadataKeyValuePair>, IFileMetadata> _fileMetadata;
		Func<string, object, IMetadataKeyValuePair> _fileMetadataKeyValuePair;
		Func<IUndoRedoFinder> _undoRedoFinder;
		Func<WordFinder> _wordFinder;
		Func<SentenceFinder> _sentenceFinder;
		Func<ParagraphFinder> _paragraphFinder;
		Func<PlainTextFinder> _plainTextFinder;
		Func<RegExTextFinder> _regexTextFinder;
		Func<TextSurroundingsFinder> _textSurroundingFinder;
		Naracea.Core.Model.Changes.WithChangeCollectionChangeArgs.WithChangeCollectionChangeArgsFactory _withChangeCollectionChangeArgsFactory;
		Func<IWithChangeCollectionChangeArgs, MultiUndo> _multiUndo;
		Func<FileXmlSerializer> _fileXmlSerializer;
		Func<IStreamBuilder> _fileStreamBuilder;
		Func<ITextEditor, ITextEditorProxy> _textEditorProxy;
		Func<IAuthor, DateTimeOffset, IChangeArgs> _changeArgs;
		Func<IChangeArgs, GroupBegin> _groupBegin;
		Func<IWithChangeCollectionChangeArgs, GroupEnd> _groupEnd;
		Func<string, IStreamBuilder, ICustomDictionary> _customDictionary;
		Func<IXcuParser> _xcuParser;
		Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> _langInstaller;
		Func<Func<string, IStreamBuilder, ICustomDictionary>, IStreamBuilder, ISpellchecker> _spellchecker;
		Naracea.Core.Spellcheck.DictionaryStore.DictionaryStoreFactory _dictionaryStore;
		Func<IMarkupConverter> _markupConverter;
		Func<IUpdateChecker> _updater;

		#region ctor
		public CoreFactories(
			Func<IRepositoryContext> repositoryContext,
			IPersisterBuilder persisterBuilder,
			Func<IRepositoryContext, IPersisterBuilder, IRepository<IDocument>> documentRepository,
			Func<ShiftToEnd> shifterAllForward,
			Func<ShiftToStart> shifterAllBackward,
			Func<int, ShiftToChange> shifterToChange,
			Func<IFileCopier> fileCopier,
			Func<IFileRemover> fileRemover,
			Func<IFileRenamer> fileRenamer,
			Func<IFileAttributes> fileAttributes,
			Func<IFileChecker> fileChecker,
			Func<IFileNameShortener> fileNameShortener,
			Func<IFileNameValidator> fileNameValidator,
			Func<IDirectoryMaker> directoryMaker,
			Func<IDirectoryRemover> directoryRemover,
			Func<string, IZipReader> zipReader,
			Func<IAuthor> author,
			Func<IBranchArgs, IBranch> branch,
			Func<IDocument> document,
			Func<IWithTextChangeArgs, InsertText> insertText,
			Func<IWithTextChangeArgs, DeleteText> deleteText,
			Func<IWithTextChangeArgs, BackspaceText> backspaceText,
			Func<IWithChangeChangeArgs, Undo> undo,
			Func<IWithChangeChangeArgs, Redo> redo,
			Func<IWithChangeCollectionChangeArgs, MultiUndo> multiUndo,
			Naracea.Core.Model.BranchArgs.BranchArgsFactory branchArgsFactory,
			Naracea.Core.Model.Changes.WithTextChangeArgs.WithTextChangeArgsFactory withTextChangeArgsFactory,
			Naracea.Core.Model.Changes.WithChangeChangeArgs.WithChangeChangeArgsFactory withChangeChangeArgsFactory,
			Naracea.Core.Model.Changes.WithChangeCollectionChangeArgs.WithChangeCollectionChangeArgsFactory withChangeCollectionChangeArgsFactory,
			BranchBuilder branchBuilder,
			DocumentBuilder documentBuilder,
			Func<Func<string, object, IMetadataKeyValuePair>, IEnumerable<IMetadataKeyValuePair>, IFileMetadata> fileMetadata,
			Func<string, object, IMetadataKeyValuePair> fileMetadataKeyValuePair,
			Func<IUndoRedoFinder> undoRedoFinder,
			Func<WordFinder> wordFinder,
			Func<SentenceFinder> sentenceFinder,
			Func<ParagraphFinder> paragraphFinder,
			Func<RegExTextFinder> regexTextFinder,
			Func<PlainTextFinder> plainTextFinder,
			Func<TextSurroundingsFinder> textSurroundingFinder,
			Func<FileXmlSerializer> fileXmlSerializer,
			Func<IStreamBuilder> fileStreamBuilder,
			Func<ITextEditor, ITextEditorProxy> textEditorProxy,
			Func<IAuthor, DateTimeOffset, IChangeArgs> changeArgs,
			Func<IChangeArgs, GroupBegin> groupBegin,
			Func<IWithChangeCollectionChangeArgs, GroupEnd> groupEnd,
			Func<string, IStreamBuilder, ICustomDictionary> customDictionary,
			Func<IXcuParser> xcuParser,
			Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> langInstaller,
			Func<Func<string, IStreamBuilder, ICustomDictionary>, IStreamBuilder, ISpellchecker> spellchecker,
			Naracea.Core.Spellcheck.DictionaryStore.DictionaryStoreFactory dictionaryStore,
			Func<IMarkupConverter> markupConverter,
			Func<IUpdateChecker> updater
		) {
			_insertText = insertText;
			_deleteText = deleteText;
			_backspaceText = backspaceText;
			_undo = undo;
			_redo = redo;
			_multiUndo = multiUndo;
			_groupBegin = groupBegin;
			_groupEnd = groupEnd;
			_changeArgs = changeArgs;
			_branchArgsFactory = branchArgsFactory;
			_withTextChangeArgsFactory = withTextChangeArgsFactory;
			_withChangeChangeArgsFactory = withChangeChangeArgsFactory;
			_withChangeCollectionChangeArgsFactory = withChangeCollectionChangeArgsFactory;
			_author = author;
			_branch = branch;
			_document = document;
			_fileCopier = fileCopier;
			_fileRemover = fileRemover;
			_fileRenamer = fileRenamer;
			_fileAttributes = fileAttributes;
			_fileChecker = fileChecker;
			_fileNameShortener = fileNameShortener;
			_fileNameValidator = fileNameValidator;
			_directoryMaker = directoryMaker;
			_directoryRemover = directoryRemover;
			_zipReader = zipReader;
			_shifterAllForward = shifterAllForward;
			_shifterAllBackward = shifterAllBackward;
			_shifterToChange = shifterToChange;
			_repositoryContext = repositoryContext;
			_persisterBuilder = persisterBuilder;
			_documentRepository = documentRepository;
			_branchBuilder = branchBuilder;
			_documentBuilder = documentBuilder;
			_fileMetadata = fileMetadata;
			_fileMetadataKeyValuePair = fileMetadataKeyValuePair;
			_undoRedoFinder = undoRedoFinder;
			_wordFinder = wordFinder;
			_sentenceFinder = sentenceFinder;
			_paragraphFinder = paragraphFinder;
			_regexTextFinder = regexTextFinder;
			_plainTextFinder = plainTextFinder;
			_textSurroundingFinder = textSurroundingFinder;
			_fileXmlSerializer = fileXmlSerializer;
			_fileStreamBuilder = fileStreamBuilder;
			_textEditorProxy = textEditorProxy;
			_customDictionary = customDictionary;
			_xcuParser = xcuParser;
			_langInstaller = langInstaller;
			_spellchecker = spellchecker;
			_dictionaryStore = dictionaryStore;
			_markupConverter = markupConverter;
			_updater = updater;

		}
		#endregion

		#region IFactories Members
		public IChange CreateInsertText(int position, string text, IAuthor author, DateTimeOffset when) {
			return _insertText(
				_withTextChangeArgsFactory(author, when, text, position)
			);
		}

		public IChange CreateDeleteText(int position, string text, IAuthor author, DateTimeOffset when) {
			return _deleteText(
				_withTextChangeArgsFactory(author, when, text, position)
			);
		}

		public IChange CreateBackspaceText(int position, string text, IAuthor author, DateTimeOffset when) {
			return _backspaceText(
				_withTextChangeArgsFactory(author, when, text, position)
			);
		}

		public IChange CreateUndo(IChange change, IAuthor author, DateTimeOffset when) {
			return _undo(
				_withChangeChangeArgsFactory(author, when, change)
			);
		}

		public IChange CreateRedo(IChange change, IAuthor author, DateTimeOffset when) {
			return _redo(
				_withChangeChangeArgsFactory(author, when, change)
				);
		}

		public IChange CreateMultiUndo(IEnumerable<IChange> changes, IAuthor author, DateTimeOffset when) {
			return _multiUndo(
				_withChangeCollectionChangeArgsFactory(author, when, changes)
				);
		}

		public IChange CreateGroupBegin(IAuthor author, DateTimeOffset when) {
			return _groupBegin(
				_changeArgs(author, when)
				);
		}

		public IChange CreateGroupEnd(IEnumerable<IChange> changes, IAuthor author, DateTimeOffset when) {
			return _groupEnd(
				_withChangeCollectionChangeArgsFactory(author, when, changes)
				);
		}


		public IAuthor CreateAuthor() {
			return _author();
		}

		public IBranch CreateBranch(IAuthor author, IBranch parent, int branchingChangeIndex, DateTimeOffset branchingDateTime) {
			var args = _branchArgsFactory(author, parent, branchingChangeIndex, branchingDateTime);
			return _branch(args);
		}

		public IDocument CreateDocument() {
			return _document();
		}

		public IFileCopier CreateFileCopier() {
			return _fileCopier();
		}

		public IFileRemover CreateFileRemover() {
			return _fileRemover();
		}

		public IFileRenamer CreateFileRenamer() {
			return _fileRenamer();
		}

		public IFileAttributes CreateFileAttributes() {
			return _fileAttributes();
		}

		public IFileChecker CreateFileChecker() {
			return _fileChecker();
		}

		public IFileNameShortener CreateFileNameShortener() {
			return _fileNameShortener();
		}

		public IFileNameValidator CreateFileNameValidator() {
			return _fileNameValidator();
		}

		public IFileSerializer CreateFileXmlSerializer() {
			return _fileXmlSerializer();
		}

		public IZipReader CreateZipReader(string file) {
			return _zipReader(file);
		}

		public IDirectoryMaker CreateDirectoryMaker() {
			return _directoryMaker();
		}

		public IDirectoryRemover CreateDirectoryRemover() {
			return _directoryRemover();
		}


		public IShifter CreateShifterToChange(int targetIndex) {
			return _shifterToChange(targetIndex);
		}

		public IShifter CreateShifterToEnd() {
			return _shifterAllForward();
		}

		public IShifter CreateShifterToStart() {
			return _shifterAllBackward();
		}

		public IUndoRedoFinder CreateUndoRedoFinder() {
			return _undoRedoFinder();
		}

		public ITextBoundaryFinder CreateWordFinder() {
			return _wordFinder();
		}

		public ITextBoundaryFinder CreateSentenceFinder() {
			return _sentenceFinder();
		}

		public ITextBoundaryFinder CreateParagraphFinder() {
			return _paragraphFinder();
		}

		public ITextFinder CreateRegExTextFinder() {
			return _regexTextFinder();
		}

		public ITextFinder CreatePlainTextFinder() {
			return _plainTextFinder();
		}

		public ITextSurroundingsFinder CreateTextSurroundingFinder() {
			return _textSurroundingFinder();
		}

		public IRepositoryContext CreateRepositoryContext() {
			return _repositoryContext();
		}

		public IRepository<IDocument> CreateDocumentRepository(IRepositoryContext context) {
			IRepository<IDocument> doc = null;
			try {
				doc = _documentRepository(context, _persisterBuilder);
			} catch( Exception ex ) {
				throw new NaraceaException(Properties.Resources.RepositoryExceptionUnableToOpen, ex);
			}
			return doc;
		}

		public IBranch BuildBranch(IDocument document, IBranch parent, IAuthor author) {
			return _branchBuilder.Build(document, parent, author);
		}

		public IBranch BuildEmptyBranch(IDocument document, IAuthor author) {
			return _branchBuilder.BuildEmpty(document, author);
		}

		public IDocument BuildDocument() {
			return _documentBuilder.Build();
		}

		public IStreamBuilder CreateStreamBuilder() {
			return _fileStreamBuilder();
		}

		public ITextEditorProxy CreateTextEditorProxy(ITextEditor textEditor) {
			return _textEditorProxy(textEditor);
		}

		public Spellcheck.ISpellchecker CreateSpellchecker() {
			return _spellchecker(_customDictionary, _fileStreamBuilder());
		}

		public Spellcheck.IDictionaryStore CreateDictionaryStore(string storePath) {
			return _dictionaryStore(storePath,
				_fileChecker(),
				_fileXmlSerializer(),
				_fileCopier(),
				_fileNameValidator(),
				_directoryMaker(),
				_directoryRemover(),
				_xcuParser(),
				_langInstaller,
				_zipReader
			);
		}

		public IMarkupConverter CreateMarkupConverter() {
			return _markupConverter();
		}

		public IUpdateChecker CreateUpdater() {
			return _updater();
		}
		#endregion

	}
}
