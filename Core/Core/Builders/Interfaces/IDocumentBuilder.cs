﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;

namespace Naracea.Core.Builders {
	public interface IDocumentBuilder {
		IDocument Build();
	}
}
