﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;

namespace Naracea.Core.Builders {
	public interface IBranchBuilder {
		IBranch Build(IDocument document, IBranch parent, IAuthor author);
	}
}
