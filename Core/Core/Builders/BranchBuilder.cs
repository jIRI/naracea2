﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.Diagnostics;

namespace Naracea.Core.Builders {
	internal class BranchBuilder : IBranchBuilder {
		Func<IBranchArgs, IBranch> _branchFactory;
		BranchArgs.BranchArgsFactory _branchArgsFactory;
		IDateTimeProvider _dateTimeProvider;

		public BranchBuilder(
			Func<IBranchArgs, IBranch> branchFactory,
			BranchArgs.BranchArgsFactory branchArgsFactory,
			IDateTimeProvider dateTimeProvider
		) {
			Debug.Assert(branchFactory != null);
			_branchFactory = branchFactory;
			Debug.Assert(dateTimeProvider != null);
			_dateTimeProvider = dateTimeProvider;
			Debug.Assert(branchArgsFactory != null);
			_branchArgsFactory = branchArgsFactory;
		}

		#region IBranchBuilder Members
		public IBranch Build(IDocument document, IBranch parent, IAuthor author) {
			var args = _branchArgsFactory(
				author, 
				parent,
				parent != null ? parent.CurrentChangeIndex : -1,
				_dateTimeProvider.Now
			);
			var branch = _branchFactory(args);
			branch.Name = string.Format("{0} {1}", Properties.Resources.DefaulBranchName, document.GetNextBranchId());
			document.Branches.Add(branch);
			return branch;
		}

		public IBranch BuildEmpty(IDocument document, IAuthor author) {
			var args = _branchArgsFactory(
				author,
				document.Branches[0],
				-1,
				_dateTimeProvider.Now
			);
			var branch = _branchFactory(args);
			branch.Name = string.Format("{0} {1}", Properties.Resources.DefaulBranchName, document.GetNextBranchId());
			document.Branches.Add(branch);
			return branch;
		}
		#endregion
	}
}
