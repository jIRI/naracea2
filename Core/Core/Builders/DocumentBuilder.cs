﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using System.Diagnostics;

namespace Naracea.Core.Builders {
	internal class DocumentBuilder : IDocumentBuilder {
		Func<IDocument> _documentFactory;
		Func<IAuthor> _authorFactory;
		IBranchBuilder _branchBuilder;
		int _newDocumentCount = 1;

		public DocumentBuilder(
				Func<IDocument> documentFactory,
				Func<IAuthor> authorFactory,
				IBranchBuilder branchBuilder
		) {
			Debug.Assert(documentFactory != null);
			_documentFactory = documentFactory;
			Debug.Assert(authorFactory != null);
			_authorFactory = authorFactory;
			Debug.Assert(branchBuilder != null);
			_branchBuilder = branchBuilder;
		}

		#region IDocumentBuilder Members
		public IDocument Build() {
			var document = _documentFactory();
			var defaultAuthor = _authorFactory();
			document.Name = string.Format("{0} {1}", Properties.Resources.DefaultDocumentName, _newDocumentCount++);
			document.Authors.Add((Author)defaultAuthor);
			var defaultBranch = _branchBuilder.Build(document, null, defaultAuthor);
			document.CurrentBranch = (Branch)defaultBranch;
			return document;
		}
		#endregion
	}
}
