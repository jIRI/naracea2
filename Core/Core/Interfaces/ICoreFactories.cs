﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.FileSystem;
using Naracea.Core.Builders;
using Naracea.Core.Model.Changes;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Finders.Text;
using Naracea.Core.Spellcheck;
using Naracea.Core.Markdown;
using Naracea.Core.Update;

namespace Naracea.Core {
	public interface ICoreFactories {
		#region IFactories Members
		IChange CreateInsertText(int position, string text, IAuthor author, DateTimeOffset when);
		IChange CreateDeleteText(int position, string text, IAuthor author, DateTimeOffset when);
		IChange CreateBackspaceText(int position, string text, IAuthor author, DateTimeOffset when);
		IChange CreateUndo(IChange change, IAuthor author, DateTimeOffset when);
		IChange CreateRedo(IChange change, IAuthor author, DateTimeOffset when);
		IChange CreateMultiUndo(IEnumerable<IChange> changes, IAuthor author, DateTimeOffset when);
		IChange CreateGroupBegin(IAuthor author, DateTimeOffset when);
		IChange CreateGroupEnd(IEnumerable<IChange> changes, IAuthor author, DateTimeOffset when);

		IAuthor CreateAuthor();
		IBranch CreateBranch(IAuthor author, IBranch parent, int branchingChangeIndex, DateTimeOffset branchingDateTime);
		IDocument CreateDocument();

		IFileChecker CreateFileChecker();
		IFileCopier CreateFileCopier();
		IFileRemover CreateFileRemover();
		IFileAttributes CreateFileAttributes();
		IFileRenamer CreateFileRenamer();
		IFileNameShortener CreateFileNameShortener();
		IFileNameValidator CreateFileNameValidator();
		IFileSerializer CreateFileXmlSerializer();
		IDirectoryMaker CreateDirectoryMaker();
		IDirectoryRemover CreateDirectoryRemover();
		IZipReader CreateZipReader(string file);

		IShifter CreateShifterToChange(int targetIndex);
		IShifter CreateShifterToEnd();
		IShifter CreateShifterToStart();

		IUndoRedoFinder CreateUndoRedoFinder();
		ITextBoundaryFinder CreateWordFinder();
		ITextBoundaryFinder CreateSentenceFinder();
		ITextBoundaryFinder CreateParagraphFinder();

		ITextFinder CreatePlainTextFinder();
		ITextFinder CreateRegExTextFinder();
		ITextSurroundingsFinder CreateTextSurroundingFinder();

		IRepositoryContext CreateRepositoryContext();
		IRepository<IDocument> CreateDocumentRepository(IRepositoryContext context);

		IBranch BuildBranch(IDocument document, IBranch parent, IAuthor author);
		IBranch BuildEmptyBranch(IDocument document, IAuthor author);
		IDocument BuildDocument();

		IStreamBuilder CreateStreamBuilder();

		ITextEditorProxy CreateTextEditorProxy(ITextEditor textEditor);

		ISpellchecker CreateSpellchecker();
		IDictionaryStore CreateDictionaryStore(string storePath);

		IMarkupConverter CreateMarkupConverter();

		IUpdateChecker CreateUpdater();
		#endregion
	}
}
