﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core {
	public interface ITextEditor {
		string Text { get; set; }
		int CaretPosition { get; set; }

		void InsertTextBeforePosition(int pos, string text);
		void InsertTextBehindPosition(int pos, string text);
		void DeleteText(int pos, string text);
		void BackspaceText(int pos, string text);
		void SetTextSilently(string text);

		void BeginUpdate();
		void EndUpdate();
	}
}
