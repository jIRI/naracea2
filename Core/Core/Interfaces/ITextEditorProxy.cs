﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Core;

namespace Naracea.Core {
	public class TextChangedEventArgs : EventArgs {
		public int Position { get; private set; }
		public int AddedLength { get; private set; }
		public int RemovedLength { get; private set; }
		public TextChangedEventArgs(int pos, int added, int removed) {
			this.Position = pos;
			this.AddedLength = added;
			this.RemovedLength = removed;
		}
	}

	public interface ITextEditorProxy : ITextEditor {
		void StoringInsert(int pos, string text);
		void StoringDelete(int pos, string text);
		void StoringBackspace(int pos, string text);
		bool IsEditing { get; }

		void BeginEdit();
		void EndEdit();

		event EventHandler<TextChangedEventArgs> ProxiedTextChanged;
	}
}
