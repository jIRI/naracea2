﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core {
	public interface IDateTimeProvider {
		DateTimeOffset Now { get; }
	}
}
