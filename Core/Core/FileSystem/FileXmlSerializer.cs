﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Naracea.Core.FileSystem {
	internal class FileXmlSerializer : IFileSerializer {
		public FileXmlSerializer() {
		}
		
		public void Serialize<T>(string path, T data) {
			XmlWriterSettings settings = new XmlWriterSettings {
				Indent = true
			};
			XmlSerializer serializer = new XmlSerializer(typeof(T));
			using( var w = XmlWriter.Create(path, settings) ) {
				serializer.Serialize(w, data);
			}
		}

		public T Deserialize<T>(string path) {
			var settings = new XmlReaderSettings {};
			using( var r = XmlReader.Create(path, settings) ) {
				var serializer = new XmlSerializer(typeof(T));
				T result = (T)serializer.Deserialize(r);
				return result;
			}
		}
	}
}
