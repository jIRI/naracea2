﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using System.Diagnostics;

namespace Naracea.Core.FileSystem {
	internal class ZipReader : IZipReader {
		ZipFile _zipFile;

		public ZipReader(string zipFile) {
			_zipFile = new ZipFile(zipFile);
		}

		#region IZipReader Members
		public IEnumerable<string> Files {
			get {
				return _zipFile.EntryFileNames;
			}
		}

		public bool Contains(string fileName) {
			return _zipFile.ContainsEntry(fileName);
		}

		public void Extract(string source, string destination) {
			var entry = _zipFile[source];
			entry.Extract(destination);
		}
		#endregion

		#region IDisposable Members
		~ZipReader() {
			//in reality this should never happen -- all repositores should be disposed
			//if they are not, we want to catch them here by failing.
			Debug.Assert(false);
		}

		private bool _disposed = false;
		public void Dispose() {
			Dispose(true);
			//must be here, otherwise finalizer gets called
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if( !_disposed ) {
				if( disposing ) {
					_zipFile.Dispose();
				}
				_disposed = true;
			}
		}
		#endregion
	}
}
