﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class DirectoryMaker : IDirectoryMaker {
		public DirectoryMaker() {
		}

		#region IDirectoryMaker Members
		public void MakeDir(string directory) {
			System.IO.Directory.CreateDirectory(directory);
		}

		public string MakeTempDir() {
			var dir = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetRandomFileName());
			this.MakeDir(dir);
			return dir;
		}
		#endregion
	}
}
