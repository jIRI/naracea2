﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class DirectoryChecker : IDirectoryChecker {
		public DirectoryChecker() {
		}

		#region IDirectoryChecker Members
		public bool Exists(string directory) {
			return System.IO.Directory.Exists(directory);
		}
		#endregion
	}
}
