﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IDirectoryChecker {
		bool Exists(string directory);
	}
}
