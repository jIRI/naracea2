﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IFileSerializer {
		void Serialize<T>(string path, T obj);
		T Deserialize<T>(string path);
	}
}
