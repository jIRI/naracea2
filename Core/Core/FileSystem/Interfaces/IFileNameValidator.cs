﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IFileNameValidator {
		bool IsValid(string path);
		string GetValidFileName(string fileName);
	}
}
