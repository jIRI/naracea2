﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IDirectoryMaker {
		void MakeDir(string directory);
		string MakeTempDir();
	}
}
