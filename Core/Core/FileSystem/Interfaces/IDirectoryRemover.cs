﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IDirectoryRemover {
		void Remove(string path);
	}
}
