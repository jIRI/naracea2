﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.FileSystem {
	public interface IStreamBuilder {
		Stream Create(string path, FileMode mode);
	}
}
