﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IZipReader : IDisposable {
		IEnumerable<string> Files { get; }

		bool Contains(string fileName);
		void Extract(string source, string destination);
	}
}
