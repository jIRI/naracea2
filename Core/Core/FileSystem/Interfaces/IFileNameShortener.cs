﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IFileNameShortener {
		string Shorten(string source, int lenHint);
	}
}
