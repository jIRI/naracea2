﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IFileCopier {
		void Copy(string source, string destination);
	}
}
