﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	public interface IFileAttributes {
		void Set(string path, System.IO.FileAttributes attribs);
	}
}
