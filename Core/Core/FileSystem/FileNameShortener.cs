﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Naracea.Core.FileSystem {
	internal class FileNameShortener : IFileNameShortener {
		#region IFileNameShortener Members
		public string Shorten(string source, int lenHint) {
			return Common.SmartFileNameShortener.Shorten(source, lenHint);
		}
		#endregion
	}
}
