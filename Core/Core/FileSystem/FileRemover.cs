﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileRemover : IFileRemover {
		#region IFileRemover Members
		public void Remove(string path) {
			System.IO.File.Delete(path);
		}
		#endregion
	}
}
