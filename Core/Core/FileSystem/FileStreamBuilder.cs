﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileStreamBuilder : IStreamBuilder {
		#region IStream Members
		public System.IO.Stream Create(string path, System.IO.FileMode mode) {
			try {
				return new System.IO.FileStream(path, mode);
			} catch( Exception ex ) {
				throw new Exceptions.BuildingStreamFailed(path, ex);
			}
		}
		#endregion
	}
}
