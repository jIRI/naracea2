﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Core.FileSystem {
	internal class FileNameValidator : IFileNameValidator {
		#region IFileNameValidator Members
		public bool IsValid(string path) {
			if( string.IsNullOrEmpty(path) ) {
				return false;
			}

			foreach( var c in Path.GetInvalidPathChars() ) {
				if( path.Contains(c) ) {
					return false;
				}
			}			
			var directory = Path.GetDirectoryName(path);
			if( directory.Length > 0 && !Directory.Exists(directory) ) {
				return false;
			}
			var fileName = Path.GetFileName(path);
			if( fileName.Length == 0 ) {
				return false;
			}
			return true;
		}

		public string GetValidFileName(string fileName) {
			string validFileName = fileName.Trim();
			foreach( char invalChar in Path.GetInvalidFileNameChars() ) {
				validFileName = validFileName.Replace(invalChar.ToString(), "");
			}
			foreach( char invalChar in Path.GetInvalidPathChars() ) {
				validFileName = validFileName.Replace(invalChar.ToString(), "");
			}

			if( validFileName.Length > 160 ) {
				validFileName = validFileName.Remove(156);
			}

			return validFileName;
		}

		#endregion
	}
}
