﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileAttributes : IFileAttributes {
		#region IFileAttributes Members
		public void Set(string path, System.IO.FileAttributes attribs) {
			System.IO.File.SetAttributes(path, attribs);
		}
		#endregion
	}
}
