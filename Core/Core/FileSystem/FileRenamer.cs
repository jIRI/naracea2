﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileRenamer : IFileRenamer {
		#region IFileRenamer Members

		public void Rename(string source, string destination) {
			System.IO.File.Move(source, destination);
		}

		#endregion
	}
}
