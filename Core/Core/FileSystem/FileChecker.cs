﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileChecker : IFileChecker {
		#region IFileChecker Members
		public bool Exists(string path) {
			return System.IO.File.Exists(path);
		}
		#endregion
	}
}
