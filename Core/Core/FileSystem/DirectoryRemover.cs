﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class DirectoryRemover : IDirectoryRemover {
		#region IDirectoryRemover Members
		public void Remove(string path) {
			System.IO.Directory.Delete(path, true);
		}
		#endregion
	}
}
