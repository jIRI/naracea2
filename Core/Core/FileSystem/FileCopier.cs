﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.FileSystem {
	internal class FileCopier : IFileCopier {
		#region IFileCopier Members
		public void Copy(string source, string destination) {
			System.IO.File.Copy(source, destination);
		}
		#endregion
	}
}
