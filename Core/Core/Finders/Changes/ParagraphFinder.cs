﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Common;

namespace Naracea.Core.Finders.Changes {
	internal class ParagraphFinder : TextBoundaryFinder {
		public ParagraphFinder() {
		}

		protected override bool ContainsDelimiter(Model.IChangesCollection changes, int index) {
			var change = changes.At(index) as IChangeWithText;
			if( change == null ) {
				return false;
			}

			return change.Text.Contains(Constants.NewLine);
		}
	}
}
