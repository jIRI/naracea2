﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Changes {
	public interface IUndoRedoFinder {
		/// <summary>
		/// Finds nearest undoable change.
		/// </summary>
		/// <param name="changes"></param>
		/// <param name="currentChange"></param>
		/// <returns>Undoable change, or null if there is none</returns>
		IChange FindNextUndoableChange(IChangesCollection changes);

		/// <summary>
		/// Finds nearest redoable change (ie. undo)
		/// </summary>
		/// <param name="changes"></param>
		/// <param name="currentChange"></param>
		/// <returns>Redoable change or null if there is none.</returns>
		IChange FindNextRedoableChange(IChangesCollection changes);
	}
}
