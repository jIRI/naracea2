﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Changes {
	public interface ITextBoundaryFinder {
		/// <summary>
		/// Finds next text boundary in given changes collection.
		/// </summary>
		/// <param name="changes">Changes.</param>
		/// <param name="start">Index of the change where search starts.</param>
		/// <returns>Index of change just after the boundary if delimiter was found; -1 otherwise. If changes collection is empty, returns -1.</returns>
		int FindNext(IChangesCollection changes, int start);

		/// <summary>
		/// Finds previous text boundary in given changes collection.
		/// </summary>
		/// <param name="changes">Changes.</param>
		/// <param name="start">Index of the change where search starts.</param>
		/// <returns>Index of change just before the boundary if delimiter was found; -1 otherwise.  If changes collection is empty, returns -1.</returns>
		int FindPrevious(IChangesCollection changes, int start);
	}
}
