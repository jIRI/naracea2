﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Changes {
	public interface IDateTimeFinder {
		/// <summary>
		/// Finds change with timestamp closest to given date/time
		/// </summary>
		/// <param name="changes">Changes.</param>
		/// <param name="currentIndex">Current change index.</param>
		/// <param name="dateTime">Date/time to be found.</param>
		/// <returns>Index of change which is closes to given date/time. If changes collection is empty, returns -1.</returns>
		int Find(IChangesCollection changes, int currentIndex, DateTimeOffset dateTime);
	}
}
