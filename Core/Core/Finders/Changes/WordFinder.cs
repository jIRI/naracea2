﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Changes {
	internal class WordFinder : TextBoundaryFinder {
		public WordFinder() {
		}

		protected override bool ContainsDelimiter(Model.IChangesCollection changes, int index) {
			var change = changes.At(index) as IChangeWithText;
			if( change == null ) {
				return false;
			}

			var text = change.Text;
			var query = from c in text
									where char.IsPunctuation(c)
										|| char.IsWhiteSpace(c)
										|| char.IsSeparator(c)
										|| char.IsControl(c)
										|| Naracea.Common.Constants.NewLine.Contains(c)
									select c;
			return query.Any();
		}
	}
}
