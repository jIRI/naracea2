﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Naracea.Common.Extensions;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;

namespace Naracea.Core.Finders.Changes {
	public abstract class TextBoundaryFinder : ITextBoundaryFinder {
		protected abstract bool ContainsDelimiter(Model.IChangesCollection changes, int index);

		/// <summary>
		/// This is a way for concrete finders to indicate, whether last delimiter found before rewind stops should be
		/// rewinded or not.
		/// </summary>
		protected virtual bool RewindLastDelimiter {
			get {
				return true;
			}
		}

		public int FindNext(Model.IChangesCollection changes, int start) {
			int count = changes.Count;
			if( start == -1 ) {
				//when start is -1 we are relaying branch which rewinded all changes (even the first one)
				//therefore we fix it here to 0, so replaying works for this case
				start = 0;
			}
			if( !this.IsChangeIdValid(start, count) || start >= count -1 ) {
				//no changes or on last change already
				return -1;
			}

			//list of triads of changeId/text positon/added text length
			var skippedChanges = new List<Tuple<int, int, int>>();
			//skip delimiters so we get to the real text
			var skippedCount = this.SkipDelimiters(changes, Enumerable.Range(start, count - start), skippedChanges);
			if( skippedChanges.Count == 0 ) {
				//there was no delimiter found, but we still skip at least one text change to prevent blocking on one change
				skippedCount += this.FindChangeWithText(changes, Enumerable.Range(start, count - start), skippedChanges);
			}

			if( skippedChanges.Count == 0 ) {
				//now it seems like there are no text changes available
				//stop processing and return last change available
				return count - 1;
			}

			//look for next delimiter
			int textStart = start + skippedCount;
			this.FindTextBoundary(changes, Enumerable.Range(textStart, count - textStart), skippedChanges);

			//quick case first
			if( skippedChanges.Count == 1 ) {
				//well, only one change was skipped, so return its id
				return skippedChanges[0].Item1;
			}

			//now solve interlacing issues
			int boundaryChangeId = this.CoerceInterlacingForward(skippedChanges);
			return boundaryChangeId;
		}

		public int FindPrevious(Model.IChangesCollection changes, int start) {
			var count = changes.Count;
			if( !this.IsChangeIdValid(start, count) || start <= 0) {
				//no items or on first item already
				return -1;
			}

			Debug.Assert(start < count);

			//list of triads of changeId/text positon/added text length
			var skippedChanges = new List<Tuple<int, int, int>>();
			//skip delimiters so we get to the real text
			var skippedCount = this.SkipDelimiters(changes, Enumerable.Range(0, start).Reverse(), skippedChanges);
			int skipCorrection = 0;
			if( skippedChanges.Count == 0 ) {
				//there was no delimiter found, but we still skip at least one text change to prevent blocking on one change
				skippedCount += this.FindChangeWithText(changes, Enumerable.Range(0, start).Reverse(), skippedChanges);
				skipCorrection = 1;
			}

			if( skippedChanges.Count == 0 ) {
				//now it seems like there are no text changes available
				//stop processing and return -1 which means rewind to beginning
				return -1;
			}

			//look for next delimiter
			int textStart = start - skippedCount;
			//here we need to correct the non-delimiter change skipped already
			this.FindTextBoundary(changes, Enumerable.Range(0, textStart + skipCorrection).Reverse(), skippedChanges);

			//quick case first
			if( skippedChanges.Count == 1 ) {
				//well, only one change was skipped, so return its id
				return skippedChanges[0].Item1;
			}

			//now solve interlacing issues
			int boundaryChangeId = this.CoerceInterlacingBackward(skippedChanges);
			
			if( boundaryChangeId == 0 ) {
				//when found change is the first one, rewind it as well. 
				//in most cases we want empty document in such a case.
				return -1;
			}

			//if deriving class signals, that last delimiterd should be removed
			//subtract -1 since we want that last change to be rewinded as well
			//it also causes ending up with empty document when rewinding very first change
			return boundaryChangeId - (this.RewindLastDelimiter ? 1 : 0);
		}

		#region Private methods
		bool IsChangeIdValid(int start, int count) {
			return count > 0 && start < count && start >= 0;
		}

		int SkipDelimiters(IChangesCollection changes, IEnumerable<int> indices, List<Tuple<int, int, int>> skippedChanges) {
			//skip all consecutive delimiters 
			int skippedCount = 0;
			foreach( var index in indices ) {
				var change = changes.At(index) as IChangeWithText;
				if( !this.ContainsDelimiter(changes, index) ) {
					//change without delimiter characters
					break;
				} else if( change != null && change.Text.Any(c => char.IsLetterOrDigit(c)) ) {
					//here we know, that change contains some delimiters
					//when it contains also some letters or digits, we better stop here to preven skipping several long text inserts with delimiters
					break;
				}

				//record walked changes
				if( change != null ) {
					skippedChanges.Add(new Tuple<int, int, int>(index, change.Position, change.AddedTextLength));
				}
				skippedCount++;
			}
			return skippedCount;
		}

		int FindChangeWithText(IChangesCollection changes, IEnumerable<int> indices, List<Tuple<int, int, int>> skippedChanges) {
			int skippedCount = 0;
			foreach( var index in indices ) {
				skippedCount++;
				var change = changes.At(index) as IChangeWithText;
				if( change != null ) {
					//stop on first text change
					skippedChanges.Add(new Tuple<int, int, int>(index, change.Position, change.AddedTextLength));
					break;
				}
			}
			return skippedCount;
		}

		int FindTextBoundary(IChangesCollection changes, IEnumerable<int> indices, List<Tuple<int, int, int>> skippedChanges) {
			int skippedCount = 0;
			bool wasDelimiter = false;
			foreach( var index in indices ) {
				var change = changes.At(index) as IChangeWithText;
				if( change != null ) {
					skippedChanges.Add(new Tuple<int, int, int>(index, change.Position, change.AddedTextLength));
				}
				skippedCount++;
				if( this.ContainsDelimiter(changes, index) ) {
					if( change != null && change.Text.Any(c => char.IsLetterOrDigit(c)) ) {
						//there is change with demiter and normal chars. 
						//it can contain more than one delimiter in different positions, so we better stop now.
						if( wasDelimiter ) {
							//because there already was delimiter, we want to remove last change
							//this helps handle cases like this one: "abc", " ", "123..."
							skippedChanges.RemoveAt(skippedChanges.Count - 1);
						}
						break;
					}
					//this will skip all consecutive delimiters
					//must be kept at this place, or it will break previous cases
					wasDelimiter = true;
				} else if( wasDelimiter ) {
					//there was delimiter, but there is none in current change. stop now.
					if( change != null ) {
						//if last change was text change, remove last recorded change, 
						//since it is the one without delimiter, preceded by delimiter
						//in case it is not text change, do not remove anything, otherwise replaying will get stucked
						skippedChanges.RemoveAt(skippedChanges.Count - 1);
					}
					break;
				} 

			}
			return skippedCount;
		}

		int CoerceInterlacingForward(List<Tuple<int, int, int>> skippedChanges) {
			var firstChangeTextPos = skippedChanges.Min(c => c.Item2);
			var lastChangeTextPos = skippedChanges.Max(c => c.Item2);
			var previous = skippedChanges.First();
			for( int i = 1; i < skippedChanges.Count; i++ ) {
				var current = skippedChanges[i];
				if( current.Item2 < firstChangeTextPos ) {
					//typing before start of current segment
					break;
				}
				if( current.Item2 > lastChangeTextPos ) {
					//probably typing after end of current segment
					break;
				}
				previous = current;
			}
			return previous.Item1;
		}

		int CoerceInterlacingBackward(List<Tuple<int, int, int>> skippedChanges) {
			var firstChangeTextPos = skippedChanges.Max(c => c.Item2);
			var lastChangeTextPos = skippedChanges.Min(c => c.Item2);
			var previous = skippedChanges.First();
			for( int i = 1; i < skippedChanges.Count; i++ ) {
				var current = skippedChanges[i];
				if( current.Item2 < lastChangeTextPos ) {
					//typing before start of current segment
					break;
				}
				if( current.Item2 > firstChangeTextPos ) {
					//probably typing after end of current segment
					break;
				}
				previous = current;
			}
			return previous.Item1;
		}
		#endregion

	}
}
