﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Naracea.Core.Finders.Changes {
	internal class DateTimeFinder : IDateTimeFinder {
		public DateTimeFinder() {
		}

		public int Find(Model.IChangesCollection changes, int currentIndex, DateTimeOffset dateTime) {
			if( changes.Count == 0 ) {
				return -1;
			}

			var change = changes.At(currentIndex);
			if( dateTime < change.DateTime ) {
				return this.FindBackward(changes, currentIndex, dateTime);
			} else if( dateTime > change.DateTime ) {
				return this.FindForward(changes, currentIndex, dateTime);
			} else {
				return currentIndex;
			}
		}

		int FindForward(Model.IChangesCollection changes, int currentIndex, DateTimeOffset dateTime) {
			int count = changes.Count;
			int result = count - 1;

			var lastDiff = Math.Abs(changes.At(currentIndex).DateTime.Subtract(dateTime).Ticks);
			for( int i = currentIndex + 1; i < count; i++ ) {
				var diff = Math.Abs(changes.At(i).DateTime.Subtract(dateTime).Ticks);
				if( lastDiff == diff ) {
					result = i - 1;
					break;
				} if( lastDiff < diff ) {
					result = i;
					break;
				}

				lastDiff = diff;
			}
			return result;
		}

		int FindBackward(Model.IChangesCollection changes, int currentIndex, DateTimeOffset dateTime) {
			int count = changes.Count;
			int result = 0;

			var lastDiff = Math.Abs(changes.At(currentIndex).DateTime.Subtract(dateTime).Ticks);
			for( int i = currentIndex - 1; i >= 0; i-- ) {
				var diff = Math.Abs(changes.At(i).DateTime.Subtract(dateTime).Ticks);
				if( lastDiff == diff ) {
					result = i + 1;
					break;
				} if( lastDiff < diff ) {
					result = i;
					break;
				}

				lastDiff = diff;
			}

			return result;
		}
	}
}
