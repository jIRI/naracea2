﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Changes {
	internal class SentenceFinder : TextBoundaryFinder {
		public SentenceFinder() {
		}

		protected override bool RewindLastDelimiter {
			get {
				return false;
			}
		}

		protected override bool ContainsDelimiter(Model.IChangesCollection changes, int index) {
			var change = changes.At(index) as IChangeWithText;
			if( change == null ) {
				return false;
			}

			var text = change.Text;
			var query = from c in text
									where char.IsPunctuation(c)
									|| char.IsControl(c)
									select c;
			return query.Any();
		}
	}
}
