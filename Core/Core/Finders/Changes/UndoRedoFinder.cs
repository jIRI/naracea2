﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Core.Finders.Changes;

namespace Naracea.Core.Finders.Changes {
	internal class UndoRedoFinder : IUndoRedoFinder {
		public UndoRedoFinder() {
		}

		public IChange FindNextUndoableChange(Model.IChangesCollection changes) {
			IChange result = null;
			// for changes from here to the beginning
			int currentChangeIndex = changes.Count - 1;
			for( int i = currentChangeIndex; i >= 0;) {
				var change = changes.At(i);
				var changeAsUndo = change as IUndo;
				if( changeAsUndo == null) {
					//this is actual change, return its index
					result = change;
					break;
				} else {
					//skip directly to change which was undone last and try again
					i = changes.IndexOf(changeAsUndo.UndoneChange) - changeAsUndo.UndoneChange.ShiftCount - ( changeAsUndo.UndoneChange.ShouldShifterExecuteFollowingChange ? 1 : 0 );
				}
			}
			return result;
		}

		public IChange FindNextRedoableChange(Model.IChangesCollection changes) {
			IChange result = null;
			// for changes from here to the beginning
			int currentChangeIndex = changes.Count - 1;
			for( int i = currentChangeIndex; i >= 0; i--) {
				var change = changes.At(i);
				if( change is IUndo ) {
					//we have a undo, let's redo it!
					result = change;
					break;
				} else if( change is IRedo ) {
					var changeAsRedo = change as IRedo;
					//skip to last redone change and try again
					i = changes.IndexOf(changeAsRedo.RedoneChange);
				} else {
					//no undo to redo? get out of here...
					break;
				}
			}

			return result;
		}
	}
}
