﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Finders.Text {
	public enum SearchDirection {
		TopToBottom, UpFromPosition, DownFromPosition
	}

	public class TextSearchOptions {
		public TextSearchOptions() {
			this.Direction = SearchDirection.TopToBottom;
			this.StartPosition = 0;
		}

		public bool UseRegEx { get; set; }
		public bool MatchCase {get; set;}
		public bool MatchWholeWords {get; set;}
		public SearchDirection Direction { get; set; }
		public int StartPosition { get; set; }
	}
}
