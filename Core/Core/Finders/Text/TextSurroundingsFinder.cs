﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Naracea.Core.Finders.Text {
	internal class TextSurroundingsFinder : ITextSurroundingsFinder {
		public TextSurroundingsFinder() {
		}

		#region ITextSurroundingsFinder Members
		public TextSurroundingsFindResult Find(string text, string substring, int position, int spanHint) {
			if( string.IsNullOrEmpty(text) ) {
				return new TextSurroundingsFindResult("", 0, 0);
			}

			int from = position;
			int minimumTo = position + substring.Length - 1;
			int to = minimumTo;

			int prefixWordCount = spanHint;
			while(from > 0 && prefixWordCount >= 0 && from < text.Length) {
				from--;
				char c = text[from];
				if( Common.Constants.NewLine.Contains(c)) {
					prefixWordCount = -1;
					from++;
				} else if( Char.IsWhiteSpace(c) ) {
					prefixWordCount--;
				} 
			}

			// remove whitespaces
			while( from < position && from < text.Length && Char.IsWhiteSpace(text[from]) ) {
				from++;
			}

			int suffixWordCount = spanHint;
			while( to < text.Length && suffixWordCount >= 0 ) {
				char c = text[to];
				if( Common.Constants.NewLine.Contains(c) ) {
					suffixWordCount = -1;
				} else if( Char.IsWhiteSpace(c) ) {
					suffixWordCount--;
				} 
				to++;
			}

			//take back last increment
			if( position < to && to > 0 ) {
				to--;
			}

			// remove whitespaces from the end
			while( position < to && to > minimumTo && Char.IsWhiteSpace(text[to]) ) {
				to--;
			}

			int substringFrom = position - from;
			var surroundedText = text.Substring(from, to - from + 1);
			return new TextSurroundingsFindResult(surroundedText, substringFrom, substringFrom + substring.Length);
		}
		#endregion
	}
}
