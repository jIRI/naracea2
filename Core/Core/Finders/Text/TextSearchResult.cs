﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Finders.Text {
	public class TextSearchResult {
		public int Position { get; private set; }
		public string MatchedText { get; private set; }

		public TextSearchResult(int position, string matchedText) {
			this.Position = position;
			this.MatchedText = matchedText;
		}

		// override object.Equals
		public override bool Equals(object obj) {
			if( obj == null || GetType() != obj.GetType() ) {
				return false;
			}
			var o = obj as TextSearchResult;
			return this.Position == o.Position
				&& this.MatchedText.Equals(o.MatchedText)
			;
		}

		// override object.GetHashCode
		public override int GetHashCode() {
			return this.Position.GetHashCode() ^ this.MatchedText.GetHashCode();
		}

		public override string ToString() {
			return string.Format("{0}:{1}", this.Position, this.MatchedText);
		}
	}
}
