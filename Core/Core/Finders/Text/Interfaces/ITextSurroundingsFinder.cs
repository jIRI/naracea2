﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Text {
	public interface ITextSurroundingsFinder {
		TextSurroundingsFindResult Find(string text, string substring, int position, int spanHint);
	}
}
