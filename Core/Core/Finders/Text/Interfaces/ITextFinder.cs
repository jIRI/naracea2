﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Finders.Text {
	public interface ITextFinder {
		IEnumerable<TextSearchResult> Find(string text, string pattern, TextSearchOptions options);
		/// <summary>
		/// This method is not meant for replacing in text, but rather to properly replace regexes.
		/// Therefore the whole input is replaced, not just part of it.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="searchPattern"></param>
		/// <param name="replacement"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		string Replace(string input, string searchPattern, string replacement, TextSearchOptions options);
	}
}
