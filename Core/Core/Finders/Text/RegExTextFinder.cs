﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Naracea.Core.Finders.Text {
	internal class RegExTextFinder : ITextFinder {
		public RegExTextFinder() {
		}

		#region ITextFinder Members
		public IEnumerable<TextSearchResult> Find(string text, string pattern, TextSearchOptions options) {
			try {
				var searchPattern = this.ConfigurePattern(pattern);
				RegexOptions regexOptions = this.BuildRegExOptions(options, ref searchPattern);
				switch( options.Direction ) {
					case SearchDirection.TopToBottom:
						return FindAllDo(text, searchPattern, regexOptions);
					case SearchDirection.DownFromPosition:
						return FindOneDown(text, searchPattern, options.StartPosition, regexOptions);
					case SearchDirection.UpFromPosition:
						return FindOneUp(text, searchPattern, options.StartPosition, regexOptions);
					default:
						throw new NotSupportedException("Unexpected search direction.");
				}
			} catch( Exception ) {
				//well, exception is intentionally ignored here to prevent app failure on invalid regex			
				// to prevent iossues upper in the foodchain, return empty array here
				return new TextSearchResult[] { };
			}
		}

		public virtual string Replace(string input, string searchPattern, string replacement, TextSearchOptions options) {
			return this.ReplaceUsingRegex(input, searchPattern, Regex.Unescape(replacement), options);
		}
		#endregion

		#region Private method
		IEnumerable<TextSearchResult> FindAllDo(string text, string searchPattern, RegexOptions regexOptions) {
			MatchCollection matches;
			try {
				matches = Regex.Matches(text, searchPattern, regexOptions);
			} catch( Exception ) {
				yield break;
			}
			foreach( Match match in matches ) {
				yield return new TextSearchResult(match.Index, match.Value);
			}
		}

		IEnumerable<TextSearchResult> FindOneUp(string text, string searchPattern, int startPosition, RegexOptions regexOptions) {
			bool wasMatch = false;
			bool wholeTextChecked = false;
			do {
				wasMatch = false;
				if( startPosition == text.Length ) {
					wholeTextChecked = true;
				}
				var matches = FindOneDo(text, searchPattern, startPosition, regexOptions | RegexOptions.RightToLeft);
				foreach( var match in matches ) {
					wasMatch = true;
					yield return match;
				}
				startPosition = text.Length;
			} while( wasMatch || !wholeTextChecked );
		}

		IEnumerable<TextSearchResult> FindOneDown(string text, string searchPattern, int startPosition, RegexOptions regexOptions) {
			bool wasMatch = false;
			bool wholeTextChecked = false;
			do {
				wasMatch = false;
				if( startPosition == 0 ) {
					wholeTextChecked = true;
				}
				var matches = FindOneDo(text, searchPattern, startPosition, regexOptions);
				foreach( var match in matches ) {
					wasMatch = true;
					yield return match;
				}
				startPosition = 0;
			} while( wasMatch || !wholeTextChecked );
		}

		IEnumerable<TextSearchResult> FindOneDo(string text, string searchPattern, int startPosition, RegexOptions regexOptions) {
			Regex regex;
			try {
				regex = new Regex(searchPattern, regexOptions);
			} catch( Exception ) {
				yield break;
			}
			for( var match = regex.Match(text, startPosition); match.Success; match = match.NextMatch() ) {
				yield return new TextSearchResult(match.Index, match.Value);
			}
		}

		string ReplaceUsingRegex(string input, string searchPattern, string replacement, TextSearchOptions options) {
			try {
				var regexOptions = this.BuildRegExOptions(options, ref searchPattern);
				return Regex.Replace(input, searchPattern, replacement, regexOptions);
			} catch( Exception ) {
				//well, exception is intentionally ignored here to prevent app failure on invalid regex
				//to prevent any unwanted replacements, return original string for this case
				return input;
			}
		}

		RegexOptions BuildRegExOptions(TextSearchOptions options, ref string searchPattern) {
			// remember: there is matching method in text replacer
			RegexOptions regexOptions = RegexOptions.None;
			if( !options.MatchCase ) {
				regexOptions |= RegexOptions.IgnoreCase;
			}
			if( options.MatchWholeWords ) {
				searchPattern = @"\b" + searchPattern + @"\b";
			}
			return regexOptions;
		}
		#endregion

		#region Protected methods
		protected virtual string ConfigurePattern(string pattern) {
			return pattern;
		}
		#endregion
	}
}
