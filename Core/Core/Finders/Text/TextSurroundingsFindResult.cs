﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core.Finders.Text {
	public class TextSurroundingsFindResult {
		public int SubstringBegin { get; private set; }
		public int SubstringEnd { get; private set; }
		public string Text { get; private set; }

		public TextSurroundingsFindResult(string text, int substringBegin, int substringEnd) {
			this.Text = text;
			this.SubstringBegin = substringBegin;
			this.SubstringEnd = substringEnd;
		}

		// override object.Equals
		public override bool Equals(object obj) {
			if( obj == null || GetType() != obj.GetType() ) {
				return false;
			}

			var o = obj as TextSurroundingsFindResult;
			return
				this.SubstringBegin == o.SubstringBegin
				&& this.SubstringEnd == o.SubstringEnd
				&& this.Text.Equals(o.Text)
			;
		}

		// override object.GetHashCode
		public override int GetHashCode() {
			return 
				this.SubstringBegin.GetHashCode()
				^ this.SubstringEnd.GetHashCode()
				^ this.Text.GetHashCode()
			;
		}

		public override string ToString() {
			return string.Format("[{0} - {1}]: {2}", this.SubstringBegin, this.SubstringEnd, this.Text);
		}
	}
}
