﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Naracea.Core.Finders.Text {
	internal class PlainTextFinder : RegExTextFinder {
		public PlainTextFinder() {
		}

		#region Overrides
		protected override string ConfigurePattern(string pattern) {
			return Regex.Escape(pattern);
		}

		public override string Replace(string input, string searchPattern, string replacement, TextSearchOptions options) {
			return replacement;
		}
		#endregion
	}
}
