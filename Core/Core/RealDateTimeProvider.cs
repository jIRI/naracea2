﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Core {
	internal class RealDateTimeProvider : IDateTimeProvider {
		#region IDateTimeProvider Members
		public DateTimeOffset Now {
			get { return DateTimeOffset.UtcNow; }
		}
		#endregion
	}
}
