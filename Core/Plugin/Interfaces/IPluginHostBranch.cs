﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core;
using Naracea.Plugin.Export;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;
using Naracea.Common;
using Naracea.Core.Shifter;

namespace Naracea.Plugin {
	public interface IPluginHostBranch {
		/// <summary>
		/// Get or set name of this instance.
		/// </summary>
		string Name { get; set; }
		/// <summary>
		/// Get or set comemnt of this instance.
		/// </summary>
		string Comment { get; set; }
		/// <summary>
		/// Get name of the file into which this branch exports.
		/// </summary>
		string ExportFileName { get; }
		/// <summary>
		/// Get whether document is dirty, i.e. whether there are unsaved changes.
		/// </summary>
		bool IsDirty { get; }
		/// <summary>
		/// Get whether this instance was activated already (branches are initializad lazily only when they are first activated). 
		/// </summary>
		bool IsActivated { get; }
		/// <summary>
		/// Get whether this instance is rewound, i.e. whether this instance is on other than very last change.
		/// </summary>
		bool IsRewound { get; }
		/// <summary>
		/// Get whether ownign document is currently active in UI.
		/// </summary>
		bool IsDocumentActive { get; }
		/// <summary>
		/// Get whether editing is ongoing on this instance and thus view updating is suspended.
		/// </summary>
		bool IsEditing { get; }
		/// <summary>
		/// Get whether this branch can be deleted.
		/// </summary>
		bool CanBeDeleted { get; }

		/// <summary>
		/// Get editor object attached to this instance.
		/// </summary>
		IPluginHostEditor Editor { get; }
		/// <summary>
		/// Get core Branch object used by this instance.
		/// </summary>
		IBranch Branch { get; }
		/// <summary>
		/// Get exporter plugin used by this instance for automatic exporting.
		/// </summary>
		IExportPlugin Exporter { get; }

		/// <summary>
		/// Get settings for this instance.
		/// </summary>
		ISettings Settings { get; }

		/// <summary>
		/// Begin long/complex editing which can gain performance from suspending view updates. View is updated EndEdit() method is called.
		/// Used e.g. when rewinding/replaying lots of changes.
		/// </summary>
		void BeginEdit();
		/// <summary>
		/// Ends long/complex editing and updates view.
		/// Used e.g. when rewinding/replaying lots of changes.
		/// </summary>
		void EndEdit();

		/// <summary>
		/// Stores change which marks begining of group of changes.
		/// To be used in operatins which need to rewind/replay/undo series of changes (e.g. replace-all operation).
		/// </summary>
		void StoreGroupBegin();
		/// <summary>
		/// Stores change which marks end of group of changes.
		/// To be used in operatins which need to rewind/replay/undo series of changes (e.g. replace-all operation).
		/// </summary>
		void StoreGroupEnd();		
		/// <summary>
		/// Stores change.
		/// Each change stored by this method is execfuted before storing, thus text is updated with change contents, 
		/// however by storing wrongly configured changes document might be corrupted beyond all repair
		/// (i.e. with great power, comes great responsibility).
		/// </summary>
		/// <param name="change">Change to be stored.</param>
		void Store(IChange change);

		/// <summary>
		/// Finds nearest undoable change.
		/// </summary>
		/// <returns>Change which can be undone or null if there are no changes which could be undone.</returns>
		IChange FindUndoableChange();
		/// <summary>
		/// Finds nearest redoable change, i.e. nearest undo change which was not redone yet.
		/// </summary>
		/// <returns>Change which can be redone or null when there is no undoable change available.</returns>
		IChange FindRedoableChange();

		/// <summary>
		/// Executes specified shifter (i.e. rewinds/replays branch according to shifter configuration).
		/// </summary>
		/// <param name="shifter">Object specifying how to rewind/replay.</param>
		void ExecuteShifter(IShifter shifter);
	}
}
