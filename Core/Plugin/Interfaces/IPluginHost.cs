﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using MemBus;
using Naracea.Core;
using Naracea.Plugin.Export;
using Naracea.View;

namespace Naracea.Plugin {
	public interface IPluginHost {
		/// <summary>
		/// Get version of the application.
		/// </summary>
		Version ApplicationVersion { get; }

		/// <summary>
		/// Get enumeration of all available plugins.
		/// </summary>
		IEnumerable<IPlugin> Plugins { get; }

		/// <summary>
		/// Get enumeration of all available export plugins.
		/// </summary>
		IEnumerable<IExportPlugin> Exporters { get; }

		/// <summary>
		/// Get core objects factory.
		/// </summary>
		ICoreFactories CoreFactories { get; }

		/// <summary>
		/// Get shared plugin message bus.
		/// </summary>
		IBus Bus { get; }

		/// <summary>
		/// Get path to plugins application data folder.
		/// It should be used to store additional plugin data.
		/// </summary>
		string PluginDataFolder { get; }

		/// <summary>
		/// Get document indicator area.
		/// </summary>
		Naracea.View.Compounds.IHasIndicators DocumentIndicators { get; }
		/// <summary>
		/// Get progress indicator area.
		/// </summary>
		Naracea.View.Compounds.IHasIndicators ProgressIndicators { get; }

		/// <summary>
		/// Get application interface.
		/// </summary>
		IPluginHostApplication Application { get; }

		/// <summary>
		/// Create specified view.
		/// </summary>
		/// <typeparam name="T">Type of the view to be created.</typeparam>
		/// <returns>Instance of requested view.</returns>
		T CreateView<T>() where T : class;


		/// <summary>
		/// Enqueues action for UI initialization.
		/// This is needed, because plugins can be loaded in different order each app start 
		/// and we need authority which will decide which will keep UI initialization in same order
		/// to prevent UI inconsistency.
		/// </summary>
		/// <param name="plugin">Plugin which requires UI initialization</param>
		/// <param name="initAction">Action to be executed to initialize plugin's UI</param>
		void EnqueuePluginUiInitialization(IPlugin plugin, Action initAction);
		/// <summary>
		/// Executes specified action on application's UI thread.
		/// Should be used for e.g. creating of plugin controls etc.
		/// </summary>
		/// <param name="action">Action to be executed on UI thread.</param>
		void ExecuteOnUiThread(Action action);
		/// <summary>
		/// Executes specified action on application's UI thread asynchrounously.
		/// Should be used for e.g. creating of plugin controls etc.
		/// </summary>
		/// <param name="action">Action to be executed on UI thread.</param>
		void ExecuteOnUiThreadAsync(Action action);

		/// <summary>
		/// Report error. Dialog describing error condition will be shown to user.
		/// </summary>
		/// <param name="shortMessage">Short message.</param>
		/// <param name="detail">Detailed message.</param>
		void ReportError(string shortMessage, string detail);

		/// <summary>
		/// Add new ribbon group to existing ribbon tabs.
		/// </summary>
		/// <param name="tab">Id of application ribbon tab.</param>
		/// <param name="group">RibbonGroup to be added to specified ribbon tab.</param>
		void AddGroupToRibbonTab(ApplicationRibbonTabIds tab, System.Windows.Controls.Ribbon.RibbonGroup group);
		/// <summary>
		/// Add new ribbon tab.
		/// </summary>
		/// <param name="tab">RibbonTab to be added to application ribbon.</param>
		void AddRibbonTab(System.Windows.Controls.Ribbon.RibbonTab tab);
		/// <summary>
		/// Notify application that plugin finished updating of UI.
		/// </summary>
		void NotifyUiUpdated();
		/// <summary>
		/// Registers WPF control name so it can be found by application code persisting and restoring ribbon's Quick Access Toolbar.
		/// Please note, that control's name and QAT id must be the same to make QAT persitence work.
		/// </summary>
		/// <param name="name">Name to be registered.</param>
		/// <param name="control">Control to be registered.</param>
		void RegisterControlName(string name, Control control);
		/// <summary>
		/// Unregisters control name when it is not needed anymore.
		/// </summary>
		/// <param name="name">Registered name of the control.</param>
		void UnregisterControlName(string name);
		/// <summary>
		/// Registers specified window as child window of application's main window.
		/// </summary>
		/// <param name="window">A window to be made child window of app's main window.</param>
		void RegisterPluginWindow(System.Windows.Window window);
	}
}
