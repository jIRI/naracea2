﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Naracea.Plugin {
	public interface IPlugin {
		IPluginHost PluginHost { get; }
		string PluginId { get; }
		string Name { get; }
		string ShortDescription { get; }
		string LongDescription { get; }
		BitmapImage SmallImage { get; }
		BitmapImage LargeImage { get; }
	}
}
