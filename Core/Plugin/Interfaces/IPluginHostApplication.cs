﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core;
using Naracea.Plugin.Export;
using Naracea.Common;

namespace Naracea.Plugin {
	public interface IPluginHostApplication {
		/// <summary>
		/// Get application wide settings.
		/// </summary>
		ISettings Settings { get; }
		/// <summary>
		/// Get enumeration of all open documents.
		/// </summary>
		IEnumerable<IPluginHostDocument> Documents { get; }
		/// <summary>
		/// Get enumeration of search results.
		/// </summary>
		IEnumerable<IPluginHostSearchResult> SearchResults { get; }
		/// <summary>
		/// Get or set active document
		/// </summary>
		IPluginHostDocument ActiveDocument { get; set; }
		
		/// <summary>
		/// Create new document.
		/// </summary>
		void CreateNewDocument();
		/// <summary>
		/// Open specified document.
		/// </summary>
		/// <param name="path">Filename of the document to be open.</param>
		void OpenDocument(string path);
		/// <summary>
		/// Save document. If document was not saved yet, dialog requesting user to specify file name will be shown.
		/// </summary>
		/// <param name="document">Document to be saved.</param>
		void SaveDocument(IPluginHostDocument document);
		/// <summary>
		/// Ask user for filename and save document.
		/// </summary>
		/// <param name="document">Document to be saved.</param>
		void SaveDocumentAs(IPluginHostDocument document);
		/// <summary>
		/// Save document to specified path.
		/// </summary>
		/// <param name="document">Document to be saved.</param>
		void SaveDocumentAs(IPluginHostDocument document, string path);
		/// <summary>
		/// Save all documents. Is there are any documents without filename, user will be requested to provide filenames.
		/// </summary>
		void SaveAllDocuments();
		/// <summary>
		/// Close specified document. If there are any unsaved changes, user will be asked to confirm.
		/// </summary>
		/// <param name="document"></param>
		void CloseDocument(IPluginHostDocument document);
		/// <summary>
		/// Print active branch of specified document.
		/// </summary>
		/// <param name="document">Document whose active branch should be printed.</param>
		void PrintActiveBranch(IPluginHostDocument document);
		/// <summary>
		/// Export specified branch of specified document using given export plugin.
		/// </summary>
		/// <param name="document">Document which contains branch which should be exported.</param>
		/// <param name="branch">Branch which should be exported.</param>
		/// <param name="exportPlugin">Export plugin to be used.</param>
		/// <param name="fileName">Name of the target file.</param>
		void ExportBranch(IPluginHostDocument document, IPluginHostBranch branch, IExportPlugin exportPlugin, string fileName);
		/// <summary>
		/// Clear all search result.
		/// Search results are automatically cleared when new search starts.
		/// </summary>
		void ClearSearchResults();
		/// <summary>
		/// Search active branch of active document.
		/// </summary>
		/// <param name="options">Search options.</param>
		/// <param name="pattern">Search pattern.</param>
		void SearchActiveBranch(Naracea.Core.Finders.Text.TextSearchOptions options, string pattern);
		/// <summary>
		/// Search history of active branch of active document.
		/// </summary>
		/// <param name="options">Search options.</param>
		/// <param name="pattern">Search pattern.</param>
		void SearchActiveBranchHistory(Naracea.Core.Finders.Text.TextSearchOptions options, string pattern);
		/// <summary>
		/// Replace specified search result using specified replace pattern and options.
		/// Replaceing history search reuslts will result in NotSupportedException.
		/// </summary>
		/// <param name="item">Search result to be replaced.</param>
		/// <param name="replacePattern">Replace pattern.</param>
		/// <param name="options">Replace options.</param>
		void ReplaceSearchResult(IPluginHostSearchResult item, string replacePattern, Naracea.Core.Finders.Text.TextSearchOptions options);
	}
}
