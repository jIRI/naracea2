﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core;
using Naracea.Common;

namespace Naracea.Plugin {
	public interface IPluginHostDocument {
		/// <summary>
		/// Get whether document is dirty, i.e. whether there are unsaved changes.
		/// </summary>
		bool IsDirty { get; }
		/// <summary>
		/// Get or sets name of the document (this is user firendly name, not filename).
		/// </summary>
		string Name { get; set; }
		/// <summary>
		/// Get or sets document comment.
		/// </summary>
		string Comment { get; set; }
		/// <summary>
		/// Get name of the file in which this documents is saved. Null if document was not saved yet.
		/// </summary>
		string Filename { get; }
		/// <summary>
		/// Get whether this document is active/current document.
		/// </summary>
		bool IsActive { get; }
		/// <summary>
		/// Get whether this document is locked for changes.
		/// </summary>
		bool IsLocked { get; }

		/// <summary>
		/// Get settings for this instance.
		/// </summary>
		ISettings Settings { get; }

		/// <summary>
		/// Get enumeration of branches in this instance.
		/// </summary>
		IEnumerable<IPluginHostBranch> Branches { get; }
		/// <summary>
		/// Get or set active branch of this instance.
		/// </summary>
		IPluginHostBranch ActiveBranch { get; set; }

		/// <summary>
		/// Creates new branch at current change of active branch. 
		/// </summary>
		void CreateBranch();
		/// <summary>
		/// Creates new empty branch (empty branch is based on initial state of default document branch)
		/// </summary>
		void CreateEmptyBranch();
		/// <summary>
		/// Deletes specified branch.
		/// Please note, that only branches without any child branches can be deleted.
		/// </summary>
		/// <param name="branch">Branch to be deleted.</param>
		void DeleteBranch(IPluginHostBranch branch);

		/// <summary>
		/// Lock this instance for editing.
		/// </summary>
		void Lock();
		/// <summary>
		/// Unlog this instance for editing.
		/// </summary>
		void Unlock();
	}
}
