﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core;
using Naracea.Common;
using Naracea.Core.Model.Changes;

namespace Naracea.Plugin {
	public interface IPluginHostEditor {
		/// <summary>
		/// Get or set current caret line .
		/// </summary>
		int CurrentLine { get; set; }
		/// <summary>
		/// Get or set current caret column.
		/// </summary>
		int CurrentColumn { get; }
		/// <summary>
		/// Get or set current caret offset from the beginning of the document.
		/// </summary>
		int CaretPosition { get; set; }
		/// <summary>
		/// Get length of the text.
		/// </summary>
		int TextLength { get; }
		/// <summary>
		/// Get whether overwrite mode is active.
		/// </summary>
		bool IsOverwriteMode { get; }
		/// <summary>
		/// Get or set current text.
		/// </summary>
		string Text { get; set; }
		/// <summary>
		/// Get selected text.
		/// </summary>
		string SelectedText { get; }
		
		/// <summary>
		/// Raised when text is changed.
		/// </summary>
		event EventHandler<TextChangedEventArgs> TextChanged;

		/// <summary>
		/// Get settings for this instance.
		/// </summary>
		ISettings Settings { get; }

		/// <summary>
		/// Select text.
		/// </summary>
		/// <param name="position">Offset from beginning of the document.</param>
		/// <param name="length">Length of the selection.</param>
		void Select(int position, int length);

		/// <summary>
		/// Relace selected text with specified text.
		/// </summary>
		/// <param name="replaceWith">New text replacing currently selected text.</param>
		void ReplaceSelection(string replaceWith);

		/// <summary>
		/// Insert text at specified position.
		/// New change is stored when this method is used.
		/// </summary>
		/// <param name="position">Offset from beginning of the text.</param>
		/// <param name="text">Text to be inserted.</param>
		void Insert(int position, string text);

		/// <summary>
		/// Insert text at current caret position.
		/// New change is stored when this method is used.
		/// </summary>
		/// <param name="text">Text to be inserted.</param>
		void Insert(string text);

		/// <summary>
		/// Delete text at specified position.
		/// New change is stored when this method is used.
		/// </summary>
		/// <param name="position">Offset from beginning of the text.</param>
		/// <param name="length">Number of characters to be removed.</param>
		void Delete(int position, int length);

		/// <summary>
		/// Delete text at current care position. This method works like pressing delete button (chars on right from caret are deleted).
		/// New change is stored when this method is used.
		/// </summary>
		/// <param name="length">Number of characters to be removed.</param>
		void Delete(int length);
	}
}
