﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Core;

namespace Naracea.Plugin {
	public interface IPluginHostSearchResult {
		string SearchPattern { get; }
		int PositionInText { get; }
		int PositionInPercents { get; }
		string SurroundedMatchText { get; }
		string MatchedText { get; }
		int MatchOffsetInSurroundedText { get; }
		IPluginHostDocument  Document { get; }
		IPluginHostBranch Branch { get; }
		bool IsHistorySearch { get; }
		int ChangeIndex { get; }
		DateTime DateTime { get; }
	}
}
