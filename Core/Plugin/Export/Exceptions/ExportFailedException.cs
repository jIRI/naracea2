﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Plugin.Export.Exceptions {
	public class ExportFailedException : Naracea.Common.Exceptions.NaraceaException {
		public ExportFailedException(Exception innerException)
			: base(Naracea.Core.Properties.Resources.ExportExceptionFailed, innerException) {

		}
	}
}
