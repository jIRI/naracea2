﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Naracea.Plugin.Export {
	public class ExportProgress : EventArgs {
		public int PercentDone { get; private set; }
		public ExportProgress(int percentDone) {
			this.PercentDone = percentDone;
		}
	}
}
