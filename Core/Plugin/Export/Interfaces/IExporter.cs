﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Naracea.Plugin.Export {
	public interface IExporter {
		event EventHandler<ExportProgress> Progress;

		void Export(Stream stream, string text);
	}
}
