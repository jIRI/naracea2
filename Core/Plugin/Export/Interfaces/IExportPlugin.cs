﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Naracea.View;

namespace Naracea.Plugin.Export {
	public interface IExportPlugin : IPlugin {
		string FileFilter { get; }
		string Extension { get; }
		string ExporterId { get; }

		IExporter CreateExporter();
		IExportBranchDialog CreateDialog();
	}
}
