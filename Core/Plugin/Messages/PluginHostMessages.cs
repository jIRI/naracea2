﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Plugin.Messages {
	#region Messages base classes
	/// <summary>
	/// Base for plugin host messages.
	/// </summary>
	public class PluginHostMessageBase {
		public IPluginHost PluginHost { get; private set; }
		public PluginHostMessageBase(IPluginHost pluginHost) {
			this.PluginHost = pluginHost;
		}
	}

	public class PluginHostDocumentMessageBase : PluginHostMessageBase {
		public IPluginHostDocument Document { get; private set; }
		public PluginHostDocumentMessageBase(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost) {
			this.Document = document;
		}
	}

	public class PluginHostBranchMessageBase : PluginHostDocumentMessageBase {
		public IPluginHostBranch Branch { get; private set; }
		public PluginHostBranchMessageBase(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document) {
			this.Branch = branch;
		}
	}

	public class PluginHostEditorMessageBase : PluginHostBranchMessageBase {
		public IPluginHostEditor Editor { get; private set; }
		public PluginHostEditorMessageBase(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch, IPluginHostEditor editor)
			: base(pluginHost, document, branch) {
			this.Editor = editor;
		}
	}
	#endregion


	#region Application messages
	public class Error : PluginHostMessageBase {
		public Error(IPluginHost pluginHost)
			: base(pluginHost) {

		}
	}


	public class Started : PluginHostMessageBase {
		public Started(IPluginHost pluginHost)
			: base(pluginHost) {

		}
	}

	public class StartFailed : PluginHostMessageBase {
		public StartFailed(IPluginHost pluginHost)
			: base(pluginHost) {

		}
	}

	public class Stopped : PluginHostMessageBase {
		public Stopped(IPluginHost pluginHost)
			: base(pluginHost) {

		}
	}

	public class StopCancelled : PluginHostMessageBase {
		public StopCancelled(IPluginHost pluginHost)
			: base(pluginHost) {

		}
	}
	#endregion

	#region Document messages
	public class DocumentOpenFailed : PluginHostMessageBase {
		public DocumentOpenFailed(IPluginHost pluginHost)
			: base(pluginHost) {
		}
	}

	public class DocumentCloseCancelled : PluginHostDocumentMessageBase {
		public DocumentCloseCancelled(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentSaveCancelled : PluginHostDocumentMessageBase {
		public DocumentSaveCancelled(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentSaveFailed : PluginHostDocumentMessageBase {
		public DocumentSaveFailed(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentSaveCompleted : PluginHostDocumentMessageBase {
		public DocumentSaveCompleted(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentActivated : PluginHostDocumentMessageBase {
		public DocumentActivated(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentDeactivated : PluginHostDocumentMessageBase {
		public DocumentDeactivated(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentClosed : PluginHostDocumentMessageBase {
		public DocumentClosed(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentSaved : PluginHostDocumentMessageBase {
		public DocumentSaved(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentOpened : PluginHostDocumentMessageBase {
		public DocumentOpened(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}

	public class DocumentExported : PluginHostDocumentMessageBase {
		public DocumentExported(IPluginHost pluginHost, IPluginHostDocument document)
			: base(pluginHost, document) {

		}
	}
	#endregion

	#region Branch messages
	public class BranchClosed : PluginHostBranchMessageBase {
		public BranchClosed(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchDirty : PluginHostBranchMessageBase {
		public BranchDirty(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class IndicatorsUpdateNeeded : PluginHostBranchMessageBase {
		public IndicatorsUpdateNeeded(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchShiftingFinished : PluginHostBranchMessageBase {
		public BranchShiftingFinished(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchOpened : PluginHostBranchMessageBase {
		public BranchOpened(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchDeleted : PluginHostBranchMessageBase {
		public BranchDeleted(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchDeleteCancelled : PluginHostBranchMessageBase {
		public BranchDeleteCancelled(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchActivated : PluginHostBranchMessageBase {
		public BranchActivated(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchDeactivated : PluginHostBranchMessageBase {
		public BranchDeactivated(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}

	public class BranchExported : PluginHostBranchMessageBase {
		public BranchExported(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch)
			: base(pluginHost, document, branch) {

		}
	}
	#endregion

	#region Editor messages
	public class TextEditorClosed : PluginHostEditorMessageBase {
		public TextEditorClosed(IPluginHost pluginHost, IPluginHostDocument document, IPluginHostBranch branch , IPluginHostEditor editor)
			: base(pluginHost, document, branch, editor) {
		}
	}
	#endregion
}
