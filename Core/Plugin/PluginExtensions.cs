﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naracea.Plugin {
	public static class PluginExtensions {
		public static string GetPluginFolderPath(this IPluginHost host, string folder) {
			return Path.Combine(host.PluginDataFolder, folder);
		}

		public static bool EnsurePluginFodlerExists(this IPluginHost host, string folder) {
			var path = host.GetPluginFolderPath(folder);
			if( !Directory.Exists(path) ) {
				try {
					Directory.CreateDirectory(path);
					return true;
				} catch( Exception ex ) {
                    host.ReportError(string.Format(Naracea.Core.Properties.Resources.ErrorCreatingPluginFolder, path), ex.Message);
				}
				return false;
			}
			return true;
		}
	}
}
