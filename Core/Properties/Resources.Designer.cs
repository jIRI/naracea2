﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Naracea.Core.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Naracea.Core.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adding changes.
        /// </summary>
        internal static string AddingChanges {
            get {
                return ResourceManager.GetString("AddingChanges", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add to dictionary.
        /// </summary>
        internal static string AddToDictionary {
            get {
                return ResourceManager.GetString("AddToDictionary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ch:.
        /// </summary>
        internal static string CaretPositionIndicatorDescription {
            get {
                return ResourceManager.GetString("CaretPositionIndicatorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Col:.
        /// </summary>
        internal static string ColumnIndicatorDescription {
            get {
                return ResourceManager.GetString("ColumnIndicatorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 day.
        /// </summary>
        internal static string Day {
            get {
                return ResourceManager.GetString("Day", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} days.
        /// </summary>
        internal static string Days {
            get {
                return ResourceManager.GetString("Days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch.
        /// </summary>
        internal static string DefaulBranchName {
            get {
                return ResourceManager.GetString("DefaulBranchName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document.
        /// </summary>
        internal static string DefaultDocumentName {
            get {
                return ResourceManager.GetString("DefaultDocumentName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Default.
        /// </summary>
        internal static string DefaultUserName {
            get {
                return ResourceManager.GetString("DefaultUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to create plugin folder..
        /// </summary>
        internal static string ErrorCreatingPluginFolder {
            get {
                return ResourceManager.GetString("ErrorCreatingPluginFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while writing your license file.
        ///Please check you have at least &quot;User&quot; permission assigned to your current user account or whether the file is not open by other application..
        /// </summary>
        internal static string ErrorUnableToSaveLicense {
            get {
                return ResourceManager.GetString("ErrorUnableToSaveLicense", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while creating new document..
        /// </summary>
        internal static string ErrorWhileCreatingNewDocument {
            get {
                return ResourceManager.GetString("ErrorWhileCreatingNewDocument", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while exporting the document.
        ///Please, check if location you are saving to is available (e.g. wasn&apos;t USB drive removed?) and whether you have permissins to write to it..
        /// </summary>
        internal static string ErrorWhileExporting {
            get {
                return ResourceManager.GetString("ErrorWhileExporting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while installing spell check dictionary.
        ///To install Hunspell dictionaries select both .aff and .dic files, to install OpenOffice dictionary select .oxt file..
        /// </summary>
        internal static string ErrorWhileInstallingDictionary {
            get {
                return ResourceManager.GetString("ErrorWhileInstallingDictionary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OpenOffice dictionaries can be found at 
        ///http://extensions.services.openoffice.org/en/dictionaries
        ///NOTE: Please, check license terms of each OpenOffice dictionary you are about to install carefully..
        /// </summary>
        internal static string ErrorWhileInstallingDictionaryDetail {
            get {
                return ResourceManager.GetString("ErrorWhileInstallingDictionaryDetail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while opening the document.
        ///Please, check if file you are opening is available (e.g. wasn&apos;t USB drive removed?) and whether it is Naracea document..
        /// </summary>
        internal static string ErrorWhileOpening {
            get {
                return ResourceManager.GetString("ErrorWhileOpening", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to overwrite file..
        /// </summary>
        internal static string ErrorWhileOverwritingFile {
            get {
                return ResourceManager.GetString("ErrorWhileOverwritingFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured while saving the document.
        ///Please, check if location you are saving to is available (e.g. wasn&apos;t USB drive removed?) and whether you have permissins to write to it..
        /// </summary>
        internal static string ErrorWhileSaving {
            get {
                return ResourceManager.GetString("ErrorWhileSaving", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to export document..
        /// </summary>
        internal static string ExportExceptionFailed {
            get {
                return ResourceManager.GetString("ExportExceptionFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exporting document.
        /// </summary>
        internal static string ExportingDocumentShort {
            get {
                return ResourceManager.GetString("ExportingDocumentShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 hour.
        /// </summary>
        internal static string Hour {
            get {
                return ResourceManager.GetString("Hour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}  hours.
        /// </summary>
        internal static string Hours {
            get {
                return ResourceManager.GetString("Hours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Indexing....
        /// </summary>
        internal static string IndexingBranch {
            get {
                return ResourceManager.GetString("IndexingBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unregistered.
        /// </summary>
        internal static string LicenseUnregistered {
            get {
                return ResourceManager.GetString("LicenseUnregistered", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ln:.
        /// </summary>
        internal static string LineIndicatorDescription {
            get {
                return ResourceManager.GetString("LineIndicatorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Found:.
        /// </summary>
        internal static string MatchesFoundSoFar {
            get {
                return ResourceManager.GetString("MatchesFoundSoFar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 minute.
        /// </summary>
        internal static string Minute {
            get {
                return ResourceManager.GetString("Minute", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}  minutes.
        /// </summary>
        internal static string Minutes {
            get {
                return ResourceManager.GetString("Minutes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 month.
        /// </summary>
        internal static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}  months.
        /// </summary>
        internal static string Months {
            get {
                return ResourceManager.GetString("Months", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (no suggestions).
        /// </summary>
        internal static string NoSuggestions {
            get {
                return ResourceManager.GetString("NoSuggestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} of {1}.
        /// </summary>
        internal static string OpeningDocumentsDetailed {
            get {
                return ResourceManager.GetString("OpeningDocumentsDetailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opening document.
        /// </summary>
        internal static string OpeningDocumentShort {
            get {
                return ResourceManager.GetString("OpeningDocumentShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opening documents.
        /// </summary>
        internal static string OpeningDocumentsShort {
            get {
                return ResourceManager.GetString("OpeningDocumentsShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Par:.
        /// </summary>
        internal static string ParagraphIndicatorDescription {
            get {
                return ResourceManager.GetString("ParagraphIndicatorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Path: {0}.
        /// </summary>
        internal static string RepositoryExceptionContextInfo {
            get {
                return ResourceManager.GetString("RepositoryExceptionContextInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File format version {0} is not supported..
        /// </summary>
        internal static string RepositoryExceptionFileformatNotSupported {
            get {
                return ResourceManager.GetString("RepositoryExceptionFileformatNotSupported", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File &quot;{0}&quot; is not a valid Naracea file..
        /// </summary>
        internal static string RepositoryExceptionNotTextacleFile {
            get {
                return ResourceManager.GetString("RepositoryExceptionNotTextacleFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to obtain document from the file..
        /// </summary>
        internal static string RepositoryExceptionUnableToObtainDocument {
            get {
                return ResourceManager.GetString("RepositoryExceptionUnableToObtainDocument", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to open file..
        /// </summary>
        internal static string RepositoryExceptionUnableToOpen {
            get {
                return ResourceManager.GetString("RepositoryExceptionUnableToOpen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to save to file..
        /// </summary>
        internal static string RepositoryExceptionUnableToSave {
            get {
                return ResourceManager.GetString("RepositoryExceptionUnableToSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Moving through history....
        /// </summary>
        internal static string RewindingReplaying {
            get {
                return ResourceManager.GetString("RewindingReplaying", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving document.
        /// </summary>
        internal static string SavingDocumentShort {
            get {
                return ResourceManager.GetString("SavingDocumentShort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search activated branches (Find All only).
        /// </summary>
        internal static string ScopeAllActiveBranches {
            get {
                return ResourceManager.GetString("ScopeAllActiveBranches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search all branches (Find All only).
        /// </summary>
        internal static string ScopeAllBranches {
            get {
                return ResourceManager.GetString("ScopeAllBranches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search current branch.
        /// </summary>
        internal static string ScopeCurrentBranch {
            get {
                return ResourceManager.GetString("ScopeCurrentBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Searching....
        /// </summary>
        internal static string SearchingBranch {
            get {
                return ResourceManager.GetString("SearchingBranch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 second.
        /// </summary>
        internal static string Second {
            get {
                return ResourceManager.GetString("Second", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}  seconds.
        /// </summary>
        internal static string Seconds {
            get {
                return ResourceManager.GetString("Seconds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to open file {0}..
        /// </summary>
        internal static string StreamBuilderFailedException {
            get {
                return ResourceManager.GetString("StreamBuilderFailedException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Len:.
        /// </summary>
        internal static string TextLengthDescription {
            get {
                return ResourceManager.GetString("TextLengthDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search the text and its history.
        /// </summary>
        internal static string TypeHistory {
            get {
                return ResourceManager.GetString("TypeHistory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search the text.
        /// </summary>
        internal static string TypeText {
            get {
                return ResourceManager.GetString("TypeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unable to install spellcheck dictionaries.
        ///Please check that dictionaries with the same name are not installed already..
        /// </summary>
        internal static string UnableToInstallLanguageException {
            get {
                return ResourceManager.GetString("UnableToInstallLanguageException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 week.
        /// </summary>
        internal static string Week {
            get {
                return ResourceManager.GetString("Week", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} weeks.
        /// </summary>
        internal static string Weeks {
            get {
                return ResourceManager.GetString("Weeks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string WritingIndicatorDescription {
            get {
                return ResourceManager.GetString("WritingIndicatorDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INS.
        /// </summary>
        internal static string WritingModeInsert {
            get {
                return ResourceManager.GetString("WritingModeInsert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OVR.
        /// </summary>
        internal static string WritingModeOverwrite {
            get {
                return ResourceManager.GetString("WritingModeOverwrite", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 year.
        /// </summary>
        internal static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} years.
        /// </summary>
        internal static string Years {
            get {
                return ResourceManager.GetString("Years", resourceCulture);
            }
        }
    }
}
