﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Naracea.Common.Extensions;
using Naracea.Core.Markdown;
using System.Windows.Documents;
using System.Windows.Markup;

namespace Naracea.Plugin.Export {
	internal class RtfExporter : IExporter {
		IMarkupConverter _converter;

		public RtfExporter(IMarkupConverter converter) {
			_converter = converter;
		}

		#region IExporter Members
		public event EventHandler<ExportProgress> Progress;

		public void Export(System.IO.Stream stream, string text) {
			this.Progress.Raise(this, new ExportProgress(0));
			try {
				string convertedText = _converter.ToXamlDocument(text);
				StringReader stringReader = new StringReader(convertedText);
				System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
				var flowDoc = XamlReader.Load(xmlReader) as FlowDocument;
				TextRange range = new TextRange(flowDoc.ContentStart, flowDoc.ContentEnd);
				range.Save(stream, System.Windows.DataFormats.Rtf);
			} catch( Exception ex ) {
				throw new Exceptions.ExportFailedException(ex);
			}
			this.Progress.Raise(this, new ExportProgress(100));
		}
		#endregion
	}
}
