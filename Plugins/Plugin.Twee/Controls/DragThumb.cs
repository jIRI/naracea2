﻿using Plugin.Twee.DiagramDesigner;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Plugin.Twee.Controls {
	public class DragThumb : Thumb {
		public DragThumb() {
			base.DragDelta += new DragDeltaEventHandler(DragThumb_DragDelta);
		}

		void DragThumb_DragDelta(object sender, DragDeltaEventArgs e) {
			DesignerItem designerItem = this.DataContext as DesignerItem;
			DesignerCanvas designer = VisualTreeHelper.GetParent(designerItem) as DesignerCanvas;
			if( designerItem != null && designer != null && designerItem.IsSelected ) {
				double minLeft = double.MaxValue;
				double minTop = double.MaxValue;

				// we only move DesignerItems
				var designerItems = designer.SelectionService.CurrentSelection.OfType<DesignerItem>();

				foreach( DesignerItem item in designerItems ) {
					double left = Canvas.GetLeft(item);
					double top = Canvas.GetTop(item);

					minLeft = double.IsNaN(left) ? 0 : Math.Min(left, minLeft);
					minTop = double.IsNaN(top) ? 0 : Math.Min(top, minTop);
				}

				double deltaHorizontal = Math.Max(-minLeft, e.HorizontalChange);
				double deltaVertical = Math.Max(-minTop, e.VerticalChange);

				foreach( DesignerItem item in designerItems ) {
					double left = Canvas.GetLeft(item);
					double top = Canvas.GetTop(item);

					if( double.IsNaN(left) ) left = 0;
					if( double.IsNaN(top) ) top = 0;

					var newLeft = left + deltaHorizontal;
					var newTop = top + deltaVertical;
					Canvas.SetLeft(item, newLeft);
					Canvas.SetTop(item, newTop);
					item.NotifyMovedToNewLocation(new Point(newLeft, newTop));
				}

				designer.InvalidateMeasure();
				e.Handled = true;
			}
		}
	}
}
