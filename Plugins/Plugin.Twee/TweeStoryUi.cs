﻿using Naracea.Plugin;
using Plugin.Twee.DiagramDesigner;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using Naracea.Common.Extensions;
using System.Windows;


namespace Plugin.Twee {
	class TweeStoryUi {
		const string SettingsItemPassageSize = "Settings.Twee.Passage.Size";
		const string SettingsItemPassageLocation = "Settings.Twee.Passage.Location";
		const string SettingsItemParentId = "Settings.Twee.Passage.ParentId";
		const string SettingsItemZIndex = "Settings.Twee.Passage.ZIndex";
		IPluginHost _pluginHost;
		IPluginHostDocument _document;
		StoryView _storyView;
		Dictionary<IPluginHostEditor, DesignerItem> _passagesMap = new Dictionary<IPluginHostEditor, DesignerItem>();

		public TweeStoryUi(IPluginHost pluginHost, IPluginHostDocument document) {
			_pluginHost = pluginHost;
			_document = document;
		}


		public void Build() {
			_pluginHost.ExecuteOnUiThread(() => {
				_storyView = new StoryView();
				_storyView.Tag = _document;
			});
		}

		public void ShowWindow() {
			_pluginHost.ExecuteOnUiThread(() => {
				_storyView.Show();
				_storyView.Activate();
			});
		}

		public void Update() {
			_pluginHost.ExecuteOnUiThread(() => this.DoUpdate());
		}

		public void ClosePassage(IPluginHostEditor editor) {
			_pluginHost.ExecuteOnUiThread(() => {
				DesignerItem item = null;
				if( _passagesMap.TryGetValue(editor, out item) ) {
					_storyView.designer.Children.Remove(item);
					_passagesMap.Remove(editor);
				}
			});
		}

		public void CloseWindow() {
			_pluginHost.ExecuteOnUiThread(() => {
				_storyView.Close();
				foreach( var branch in _document.Branches ) {
					_passagesMap.Remove(branch.Editor);
				}
			});
		}

		public void UpdatePassageContent(IPluginHostEditor editor) {
			_pluginHost.ExecuteOnUiThread(() => this.DoUpdatePassageContent(editor));
		}

		public void RestoreGroups() {
			_pluginHost.ExecuteOnUiThread(() => this.DoRestoreGroups());
		}

		public void AddConnection(IPluginHostEditor from, IPluginHostEditor to) {
			if( from == to ) {
				return;
			}

			_pluginHost.ExecuteOnUiThread(() => this.DoAddConnection(from, to));
		}

		public void DoAddConnection(IPluginHostEditor from, IPluginHostEditor to) {
			var designer = _storyView.designer;
			var sourceItem = _passagesMap[from];
			var sourceConnectorsList = new List<Connector>();
			designer.GetConnectors(sourceItem, sourceConnectorsList);
			var sinkItem = _passagesMap[to];
			var sinkConnectorsList = new List<Connector>();
			designer.GetConnectors(sinkItem, sinkConnectorsList);
			var connection = new Connection(sourceConnectorsList.First(), sinkConnectorsList.First());
			designer.Children.Add(connection);
			Canvas.SetZIndex(connection, -1);
		}

		public void RemoveConnectionsOf(IPluginHostEditor target) {
			_pluginHost.ExecuteOnUiThread(() => this.DoRemoveConnectionsOf(target));
		}

		private void DoRemoveConnectionsOf(IPluginHostEditor target) {
			var designer = _storyView.designer;
			var targetItem = _passagesMap[target];
			var itemConnectorsList = new List<Connector>();
			designer.GetConnectors(targetItem, itemConnectorsList);
			var targetConnector = itemConnectorsList.First();
			var children = new System.Collections.ArrayList(designer.Children);
			foreach( var child in children ) {
				var connection = child as Connection;
				if( connection != null && ( connection.Sink == targetConnector || connection.Source == targetConnector ) ) {
					designer.Children.Remove(connection);
				}
			}
		}

		private void DoUpdatePassageContent(IPluginHostEditor editor) {
			DesignerItem item = null;
			if( _passagesMap.TryGetValue(editor, out item) ) {
				var passage = item.Content as Controls.Passage;
				passage.text.Text = editor.Text;
			}
		}

		private void DoUpdate() {
			_storyView.Title = _document.Name + " - Twee story view";
			var x = 0.0;
			var y = 0.0;
			foreach( var branch in _document.Branches ) {
				DesignerItem item = null;
				if( !_passagesMap.TryGetValue(branch.Editor, out item) ) {
					item = this.BuildPassage(ref x, ref y, branch);
					_passagesMap.Add(branch.Editor, item);
				} else {
					( item.Content as Controls.Passage ).title.Text = branch.Name;
				}
				this.DoUpdatePassageContent(branch.Editor);
			}
		}

		private DesignerItem BuildPassage(ref double x, ref double y, IPluginHostBranch branch) {
			var windowWidth = _storyView.Width;
			var passage = new Controls.Passage();
			var itemWidth = passage.MinWidth;
			var itemHeight = passage.MinHeight;
			if( branch.Settings.HasValue(TweeStoryUi.SettingsItemPassageSize) ) {
				var size = branch.Settings.Get<System.Windows.Size>(TweeStoryUi.SettingsItemPassageSize);
				itemHeight = size.Height;
				itemWidth = size.Width;
			}
			bool hasStoredLocation = false;
			if( branch.Settings.HasValue(TweeStoryUi.SettingsItemPassageLocation) ) {
				var point = branch.Settings.Get<System.Windows.Point>(TweeStoryUi.SettingsItemPassageLocation);
				x = point.X;
				y = point.Y;
				hasStoredLocation = true;
			}

			passage.title.Text = branch.Name;
			var designerItem = new DesignerItem();
			designerItem.Content = passage;
			designerItem.Height = itemHeight;
			designerItem.Width = itemWidth;
			designerItem.Tag = branch;

			do {
				DesignerCanvas.SetLeft(designerItem, x);
				DesignerCanvas.SetTop(designerItem, y);
				x += itemWidth * 1.2;
				if( x > windowWidth - itemWidth ) {
					x = 0;
					y += itemHeight * 1.2;
				}
			} while( !hasStoredLocation && _passagesMap.Values.Any(di => di.CurrentBounds.IntersectsWith(designerItem.CurrentBounds)) );

			if( branch.Settings.HasValue(TweeStoryUi.SettingsItemZIndex) ) {
				var zIndex = branch.Settings.Get<int>(TweeStoryUi.SettingsItemZIndex);
				DesignerCanvas.SetZIndex(designerItem, zIndex);
			}

			_storyView.designer.Children.Add(designerItem);

			if( !hasStoredLocation ) {
				branch.Settings.Set(TweeStoryUi.SettingsItemPassageSize, new System.Windows.Size(designerItem.Width, designerItem.Height));
				branch.Settings.Set(TweeStoryUi.SettingsItemPassageLocation, new System.Windows.Point(x, y));
			}

			// keep this last, sot we do not call size & location update during setup phase
			designerItem.SizeChanged += (s, e) => {
				var item = s as DesignerItem;
				if( e.PreviousSize.Width != item.Width || e.PreviousSize.Height != item.Height ) {
					branch.Settings.Set(TweeStoryUi.SettingsItemPassageSize, e.NewSize);
				}
			};
			designerItem.MouseDoubleClick += DesignerItem_MouseDoubleClick;
			designerItem.MovedToNewLocation += (s, e) => branch.Settings.Set(TweeStoryUi.SettingsItemPassageLocation, e.NewLocation);
			designerItem.ParentIdChanged += (s, e) => {
				var id = s.As<DesignerItem>().ParentID;
				if( id != Guid.Empty ) {
					branch.Settings.Set(TweeStoryUi.SettingsItemParentId, id);
				} else {
					branch.Settings.Clear(TweeStoryUi.SettingsItemParentId);
				}
			};
			designerItem.ZIndexChanged += (s, e) => branch.Settings.Set(TweeStoryUi.SettingsItemZIndex, e.ZIndex);
			return designerItem;
		}

		void DesignerItem_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e) {
			_pluginHost.Application.ActiveDocument = _document;
			_document.ActiveBranch = sender.As<DesignerItem>().Tag.As<IPluginHostBranch>();
		}

		private void DoRestoreGroups() {
			var groupMap = new Dictionary<Guid, List<DesignerItem>>();
			foreach( var designerItem in _passagesMap.Values ) {
				var branch = designerItem.Tag as IPluginHostBranch;
				if( branch.Settings.HasValue(TweeStoryUi.SettingsItemParentId) ) {
					var parentId = branch.Settings.Get<Guid>(TweeStoryUi.SettingsItemParentId);
					List<DesignerItem> groupItems = null;
					if( !groupMap.TryGetValue(parentId, out groupItems) ) {
						groupItems = new List<DesignerItem>();
						groupMap.Add(parentId, groupItems);
					}
					groupItems.Add(designerItem);
				}
			}

			foreach( var pair in groupMap ) {
				var items = pair.Value;

				var rect = TweeStoryUi.GetBoundingRectangle(items);

				DesignerItem groupItem = new DesignerItem(pair.Key);
				groupItem.IsGroup = true;
				groupItem.Width = rect.Width;
				groupItem.Height = rect.Height;
				Canvas.SetLeft(groupItem, rect.Left);
				Canvas.SetTop(groupItem, rect.Top);
				Canvas groupCanvas = new Canvas();
				groupItem.Content = groupCanvas;
				Canvas.SetZIndex(groupItem, _storyView.designer.Children.Count);
				_storyView.designer.Children.Add(groupItem);

				foreach( DesignerItem item in items ) {
					item.ParentID = groupItem.ID;
				}
			}
		}

		private static Rect GetBoundingRectangle(IEnumerable<DesignerItem> items) {
			double x1 = Double.MaxValue;
			double y1 = Double.MaxValue;
			double x2 = Double.MinValue;
			double y2 = Double.MinValue;

			foreach( DesignerItem item in items ) {
				x1 = Math.Min(Canvas.GetLeft(item), x1);
				y1 = Math.Min(Canvas.GetTop(item), y1);

				x2 = Math.Max(Canvas.GetLeft(item) + item.Width, x2);
				y2 = Math.Max(Canvas.GetTop(item) + item.Height, y2);
			}

			return new Rect(new Point(x1, y1), new Point(x2, y2));
		}

	}
}
