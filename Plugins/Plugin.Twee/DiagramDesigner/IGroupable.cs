﻿using System;

namespace Plugin.Twee.DiagramDesigner
{
    public interface IGroupable
    {
        Guid ID { get; }
        Guid ParentID { get; set; }
        bool IsGroup { get; set; }
    }
}
