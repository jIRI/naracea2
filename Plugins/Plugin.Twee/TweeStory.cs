﻿using MemBus.Subscribing;
using Naracea.Plugin;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;

namespace Plugin.Twee
{
	internal class TweeStory
	{
		IPluginHost _pluginHost;
		IPluginHostDocument _document;
		TweeStoryUi _ui;
		System.Timers.Timer _textUpdateTimer = new System.Timers.Timer(3000);
		ConcurrentBag<IPluginHostEditor> _editorsScheduledForUpdate = new ConcurrentBag<IPluginHostEditor>();
		Regex _outgoingLinkRegex = new Regex(@"\[\[(\s*[^]]+\s*\|){0,1}\s*(((?<name>[^]]+)\])|((?<name>[^]]+)\]\s*\[(\s*[^]]+)\]))\]", RegexOptions.Compiled);
		Regex _implementedPassagesRegex = new Regex(@"(?m)^\:\:\s*(?<name>.+)\s*$", RegexOptions.Compiled);
		Dictionary<IPluginHostEditor, HashSet<string>> _outgoingLinksMap = new Dictionary<IPluginHostEditor, HashSet<string>>();
		Dictionary<IPluginHostEditor, List<string>> _implementedPassagesMap = new Dictionary<IPluginHostEditor, List<string>>();
		Dictionary<string, IPluginHostEditor> _reversePassageMap = new Dictionary<string, IPluginHostEditor>();
		List<IDisposable> _subscribers = new List<IDisposable>();

		public TweeStory(IPluginHost pluginHost, IPluginHostDocument document)
		{
			_pluginHost = pluginHost;
			_document = document;
			this.InitializeUi(_document);

			_textUpdateTimer.Elapsed += this.TextUpdateTimer_Elapsed;
			_subscribers.Add(_pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentClosed>(
				m => this.HandleDocumentClose(m.Document),
				new ShapeToFilter<Naracea.Plugin.Messages.DocumentClosed>(msg => msg.Document == _document)
			));
			_subscribers.Add(_pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.BranchOpened>(
				m => this.HandleBranchOpened(m.Document, m.Branch),
				new ShapeToFilter<Naracea.Plugin.Messages.BranchOpened>(msg => msg.Document == _document)
			));
			_subscribers.Add(_pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.BranchClosed>(
				m => this.HandleBranchClosed(m.Document, m.Branch),
				new ShapeToFilter<Naracea.Plugin.Messages.BranchClosed>(msg => msg.Document == _document)
			));
			_subscribers.Add(_pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.BranchShiftingFinished>(
				m => _ui.Update(),
				new ShapeToFilter<Naracea.Plugin.Messages.BranchShiftingFinished>(msg => msg.Document == _document)
			));
		}

		public void Open()
		{
			_ui.ShowWindow();
			_ui.Update();
			this.RebuildConnections(_document);
		}

		private void InitializeUi(IPluginHostDocument document)
		{
			_ui = new TweeStoryUi(_pluginHost, document);
			_ui.Build();
			foreach (var branch in document.Branches)
			{
				branch.Editor.TextChanged += this.Editor_TextChanged;
			}
		}

		private void RebuildConnections(IPluginHostDocument document)
		{
			this.UpdateAllConnections(document);
			_ui.RestoreGroups();

			foreach (var branch in document.Branches)
			{
				this.AddAllConnectionsFromPassage(branch.Editor);
			}
		}

		private void AddAllConnectionsFromPassage(IPluginHostEditor editor)
		{
			foreach (var linkedPassage in _outgoingLinksMap[editor])
			{
				if (_reversePassageMap.ContainsKey(linkedPassage))
				{
					IPluginHostEditor from = editor;
					IPluginHostEditor to = _reversePassageMap[linkedPassage];
					_ui.AddConnection(from, to);
				}
			}
		}

		private void AddAllConnectionsToPassage(IPluginHostEditor editor)
		{
			var implementedPassages = _implementedPassagesMap[editor];
			var incomingLinksQuery = from pair in _outgoingLinksMap
															 from passageName in implementedPassages
															 where pair.Value.Contains(passageName)
															 select pair.Key;
			foreach (var linkedPassage in incomingLinksQuery)
			{
				IPluginHostEditor from = linkedPassage;
				IPluginHostEditor to = editor;
				_ui.AddConnection(from, to);
			}
		}

		void TextUpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			while (_editorsScheduledForUpdate.Count > 0)
			{
				IPluginHostEditor editor = null;
				if (_editorsScheduledForUpdate.TryTake(out editor))
				{
					_ui.UpdatePassageContent(editor);
				}
				this.UpdateConnections(editor);
				_ui.RemoveConnectionsOf(editor);
				this.AddAllConnectionsFromPassage(editor);
				this.AddAllConnectionsToPassage(editor);
			}
		}

		void Editor_TextChanged(object sender, Naracea.Core.TextChangedEventArgs e)
		{
			var editor = sender as IPluginHostEditor;
			if (!_editorsScheduledForUpdate.Contains(editor))
			{
				_editorsScheduledForUpdate.Add(editor);
			}
			_textUpdateTimer.Enabled = true;
		}

		private void UpdateAllConnections(IPluginHostDocument document)
		{
			foreach (var branch in document.Branches)
			{
				this.UpdateConnections(branch.Editor);
			}
		}

		private void UpdateConnections(IPluginHostEditor editor)
		{
			var text = editor.Text;

			// collect referrenced/linked passages
			var referrencedPassages = _outgoingLinkRegex.Matches(text);
			var linkedPassageList = new HashSet<string>();
			foreach (Match match in referrencedPassages)
			{
				linkedPassageList.Add(match.Groups["name"].Value.Trim());
			}

			if (_outgoingLinksMap.ContainsKey(editor))
			{
				_outgoingLinksMap[editor] = linkedPassageList;
			}
			else
			{
				_outgoingLinksMap.Add(editor, linkedPassageList);
			}

			//collect implemented passages
			var implementedPassages = _implementedPassagesRegex.Matches(text);
			var implementedPassagesList = new List<string>();
			List<string> previousImplementedPassages = null;
			foreach (Match match in implementedPassages)
			{
				var passageName = match.Groups["name"].Value;
				var tagPosition = passageName.IndexOf('[');
				if (tagPosition > 0)
				{
					passageName = passageName.Substring(0, tagPosition - 1).Trim();
				}
				implementedPassagesList.Add(passageName);
			}

			if (_implementedPassagesMap.ContainsKey(editor))
			{
				previousImplementedPassages = _implementedPassagesMap[editor];
				_implementedPassagesMap[editor] = implementedPassagesList;
			}
			else
			{
				_implementedPassagesMap.Add(editor, implementedPassagesList);
			}

			//update passage reverse map
			if (previousImplementedPassages != null)
			{
				foreach (var passageName in previousImplementedPassages)
				{
					_reversePassageMap.Remove(passageName);
				}
			}
			foreach (var passageName in _implementedPassagesMap[editor])
			{
				if (!_reversePassageMap.ContainsKey(passageName))
				{
					_reversePassageMap.Add(passageName, editor);
				}
			}
		}

		private void HandleBranchOpened(IPluginHostDocument document, IPluginHostBranch branch)
		{
			_ui.Update();
			branch.Editor.TextChanged += this.Editor_TextChanged;
		}

		private void HandleBranchClosed(IPluginHostDocument document, IPluginHostBranch branch)
		{
			_ui.ClosePassage(branch.Editor);
			branch.Editor.TextChanged -= this.Editor_TextChanged;
		}

		private void HandleDocumentClose(IPluginHostDocument document)
		{
			_ui.CloseWindow();
			foreach (var branch in document.Branches)
			{
				branch.Editor.TextChanged -= this.Editor_TextChanged;
			}
			foreach (var subscriber in _subscribers)
			{
				subscriber.Dispose();
			}
		}
	}
}
