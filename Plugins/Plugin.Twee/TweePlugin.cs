﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Input;
using MemBus;
using Naracea.Plugin;
using Naracea.Common.Extensions;
using System.IO;
using Naracea.Common;
using System.Threading;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using System.Collections.Concurrent;
using System.Timers;

namespace Plugin.Twee {
	[Export(typeof(IPlugin))]
	public class TweePlugin : IPlugin {
		#region Attributes
		const string ImageLargeTweenify = @"pack://application:,,,/Plugin.Twee;component/Images/book_edit_large.png";
		const string ImageSmallTweenify = @"pack://application:,,,/Plugin.Twee;component/Images/book_edit_small.png";
		const string ImageLargeStoryView = @"pack://application:,,,/Plugin.Twee;component/Images/chart_organisation_large.png";
		const string ImageSmallStoryView = @"pack://application:,,,/Plugin.Twee;component/Images/chart_organisation_small.png";
		const string PluginFolder = "Twee";
		const string DefaultTemplate = "sugarcane";
		const string ScriptsZipFileName = "TweeScripts.zip";

		Dictionary<IPluginHostDocument, TweeStory> _stories = new Dictionary<IPluginHostDocument,TweeStory>();
		#endregion

		#region ctors
		[ImportingConstructor]
		public TweePlugin(IPluginHost host) {
			this.PluginHost = host;
			this.TweeScriptFolder = this.PluginHost.GetPluginFolderPath(TweePlugin.PluginFolder);

			var tweePath = Path.Combine(this.TweeScriptFolder, "twee");
			this.TweeScriptFile = Path.Combine(tweePath, "twee");
			this.SearchPaths = new[] {
				tweePath,
				Path.Combine(this.TweeScriptFolder, @"twee\lib"),
				Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Naracea.Common.Constants.PythonLibraryDirectory)
			};

			this.LoadPluginImages();

			// Build plugin UI on app's UI thread.
			// Never do any asumptions on from which thread messages to plugins are passed. Naracea is quite
			// async, so be careful about updating your own UI on UI thread by using plugin host method ExecuteOnUiThread().
			this.PluginHost.Bus.Subscribe<Naracea.Plugin.Messages.Started>(m => this.PluginHost.EnqueuePluginUiInitialization(this, this.InitializeUi));
			this.PluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentClosed>(m => _stories.Remove(m.Document));

		}
		#endregion

		#region IPlugin Members
		public IPluginHost PluginHost {
			get;
			private set;
		}

		public string PluginId {
			get { return "C0BB7C8E-A26C-45DA-AC3E-8650EA9C902A"; }
		}

		public string Name {
			get { return Properties.Resources.Name; }
		}

		public string ShortDescription {
			get { return Properties.Resources.ShortDescription; }
		}

		public string LongDescription {
			get { return Properties.Resources.LongDescription; }
		}

		public System.Windows.Media.Imaging.BitmapImage SmallImage {
			get;
			private set;
		}

		public System.Windows.Media.Imaging.BitmapImage LargeImage {
			get;
			private set;
		}
		#endregion

		#region Properties
		internal string TweeScriptFolder { get; private set; }
		internal string TweeScriptFile { get; private set; }
		internal string[] SearchPaths { get; private set; }
		#endregion

		#region Private methods
		void InitializeUi() {
			var tweeGroup = CreateGroup();
			this.CreateGroupContent(tweeGroup);
		}

		RibbonGroup CreateGroup() {
			var ribbonGroup = new RibbonGroup {
				Name = "pluginTweeGroup",
				Header = Properties.Resources.GroupName,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center
			};
			return ribbonGroup;
		}

		void CreateGroupContent(RibbonGroup ribbonGroup) {
			var buttonGenerate = new RibbonButton {
				Name = "pluginTweeConvert",
				QuickAccessToolBarId = "pluginTweeConvert",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.ConvertButton,
				SmallImageSource = this.SmallImage,
				LargeImageSource = this.LargeImage,
				KeyTip = "B",
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.ShortDescription,
					Description = Properties.Resources.LongDescription
				},
				Command = new Naracea.View.Command(this.ExecuteConvertCommand, this.CanExecuteConvertCommand)
			};
			ribbonGroup.Items.Add(buttonGenerate);

			var buttonTweenify = new RibbonButton {
				Name = "pluginTweeTweenify",
				QuickAccessToolBarId = "pluginTweeTweenify",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.ButtonTweenify,
				SmallImageSource = new BitmapImage(new Uri(TweePlugin.ImageSmallTweenify)),
				LargeImageSource = new BitmapImage(new Uri(TweePlugin.ImageLargeTweenify)),
				KeyTip = "T",
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.ButtonTweenify,
					Description = Properties.Resources.ButtonTweenifyDescription
				},
				Command = new Naracea.View.Command(this.ExecuteTweenifyCommand, this.CanExecuteTweenifyCommand)
			};
			ribbonGroup.Items.Add(buttonTweenify);

			var buttonStoryView = new RibbonButton {
				Name = "pluginTweeStoryView",
				QuickAccessToolBarId = "pluginTweeStoryView",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.ButtonStoryView,
				SmallImageSource = new BitmapImage(new Uri(TweePlugin.ImageSmallStoryView)),
				LargeImageSource = new BitmapImage(new Uri(TweePlugin.ImageLargeStoryView)),
				KeyTip = "V",
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.ButtonStoryView,
					Description = Properties.Resources.ButtonStoryViewDescription
				},
				Command = new Naracea.View.Command(this.ExecuteStoryViewCommand, this.CanExecuteStoryViewCommand)
			};
			ribbonGroup.Items.Add(buttonStoryView);

			this.PluginHost.AddGroupToRibbonTab(Naracea.View.ApplicationRibbonTabIds.Document, ribbonGroup);
			this.PluginHost.RegisterControlName(buttonGenerate.Name, buttonGenerate);
			this.PluginHost.RegisterControlName(buttonTweenify.Name, buttonTweenify);
			this.PluginHost.NotifyUiUpdated();
		}

		bool CanExecuteConvertCommand() {
			return this.PluginHost.Application.ActiveDocument != null
					&& !this.PluginHost.Application.ActiveDocument.IsLocked;
		}

		void ExecuteConvertCommand() {
			Task.Factory.StartNew(() => {
				if( this.EnsurePluginInstallation() ) {
					this.PerformConversion();
				}
			});
		}

		bool CanExecuteTweenifyCommand() {
			return this.PluginHost.Application.ActiveDocument != null
					&& this.PluginHost.Application.ActiveDocument.ActiveBranch != null
					&& !this.PluginHost.Application.ActiveDocument.IsLocked;
		}

		void ExecuteStoryViewCommand() {
			Task.Factory.StartNew(() => {
				this.EnsureBranchesAreActivated();
				var document = this.PluginHost.Application.ActiveDocument;
				TweeStory story = null;
				if(!_stories.TryGetValue(document, out story)) {
					story = new TweeStory(this.PluginHost, document);
					_stories.Add(document, story);
				}
				story.Open();
			});
		}

		bool CanExecuteStoryViewCommand() {
			return this.PluginHost.Application.ActiveDocument != null
					&& this.PluginHost.Application.ActiveDocument.ActiveBranch != null
					&& !this.PluginHost.Application.ActiveDocument.IsLocked;
		}

		void ExecuteTweenifyCommand() {
			this.SetupDocument();
		}

		bool EnsurePluginInstallation() {
			if( Directory.Exists(this.TweeScriptFolder) ) {
				return true;
			}

			// plugin folder missing -- unpack scripts
			if( !this.PluginHost.EnsurePluginFodlerExists(TweePlugin.PluginFolder) ) {
				// plugin folder creation failed
				this.PluginHost.ReportError(
						Properties.Resources.ScriptsInstallationFailed,
						string.Format(Properties.Resources.ScriptsInstallationCannotCreateFolder, TweePlugin.PluginFolder)
				);
				return false;
			}

			try {
				var scriptsZip = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TweePlugin.ScriptsZipFileName);
				using( var unzip = Ionic.Zip.ZipFile.Read(scriptsZip) ) {
					unzip.ExtractAll(this.TweeScriptFolder);
				}
			} catch( Exception ex ) {
				this.PluginHost.ReportError(Properties.Resources.ScriptsInstallationFailed, ex.Message);
			}

			return true;
		}

		private void PerformConversion() {
			var progressIndicator = this.PluginHost.CreateView<Naracea.View.Components.IProgressIndicator>();
			progressIndicator.ShortMessage = Properties.Resources.BuildProgress;
			progressIndicator.IsIndeterminate = true;
			progressIndicator.IsCancellable = false;
			progressIndicator.Open(this.PluginHost.ProgressIndicators);
			try {
				var text = this.GetMergedText();
				var sourcePath = this.GetSourcePath();

				var branchExportFilename = "tweeExport.tw";
				var sourceFile = Path.Combine(sourcePath, branchExportFilename);
				this.SaveSourceFile(text, sourceFile);

				var targetFile = Path.Combine(sourcePath, Path.GetFileNameWithoutExtension(sourceFile)) + ".html";

				var engineOptions = this.GetScriptEngineOptions(sourceFile);
				var engine = IronPython.Hosting.Python.CreateEngine(engineOptions);
				this.SetSearchPaths(engine);
				var scope = this.GetExecutionScope(engine);

				var scriptSource = engine.CreateScriptSourceFromFile(this.TweeScriptFile, Encoding.UTF8, Microsoft.Scripting.SourceCodeKind.Statements);

				using( var file = File.Open(targetFile, FileMode.Create) ) {
					engine.Runtime.IO.SetOutput(file, Encoding.UTF8);
					scriptSource.Execute(scope);
				}

				engine.Runtime.Shutdown();
				Process.Start(new ProcessStartInfo(targetFile));
			} catch( Exception ex ) {
				this.PluginHost.ReportError(Properties.Resources.BuildFailed, ex.Message);
			} finally {
				progressIndicator.Close();
			}
		}

		private Microsoft.Scripting.Hosting.ScriptScope GetExecutionScope(Microsoft.Scripting.Hosting.ScriptEngine engine) {
			var scope = engine.CreateScope();
			scope.SetVariable("__name__", "__main__");
			return scope;
		}

		private void SetSearchPaths(Microsoft.Scripting.Hosting.ScriptEngine engine) {
			var paths = engine.GetSearchPaths().ToList();
			paths.AddRange(this.SearchPaths);
			engine.SetSearchPaths(paths);
		}

		private Dictionary<string, object> GetScriptEngineOptions(string sourceFile) {
			var options = new Dictionary<string, object>();
			// uncomment to enable debugging
			//options["Debug"] = true;

			var targetTemplate = TweePlugin.DefaultTemplate;
			// try read template from document comments
			if( !string.IsNullOrWhiteSpace(this.PluginHost.Application.ActiveDocument.Comment) ) {
				var paramsMatch = Regex.Match(this.PluginHost.Application.ActiveDocument.Comment, @"\{\{(?<params>.*)\}\}");
				if( paramsMatch.Success && paramsMatch.Groups["params"].Success ) {
					targetTemplate = paramsMatch.Groups["params"].Value;
				}
			}

			// build script arguments
			options["Arguments"] = new[] { 
				this.TweeScriptFile,
				"-t",
				targetTemplate,
				sourceFile
			};

			return options;
		}

		private void SaveSourceFile(string text, string sourceFile) {
			using( var writer = new StreamWriter(sourceFile, false, Encoding.UTF8) ) {
				writer.Write(text);
			}
		}

		private string GetSourcePath() {
			var sourcePath = PluginHost.Application.ActiveDocument.Filename;
			if( sourcePath != null ) {
				sourcePath = Path.GetDirectoryName(sourcePath);
			} else {
				sourcePath = Path.GetTempPath();
			}
			return sourcePath;
		}

		private string GetMergedText() {
			this.EnsureBranchesAreActivated();
			var textBuilder = new StringBuilder();
			foreach( var branch in this.PluginHost.Application.ActiveDocument.Branches ) {
				if( Regex.IsMatch(branch.Comment, @"\{\{ignore\}\}") ) {
					continue;
				}

				textBuilder.Append(branch.Editor.Text);
				textBuilder.Append("\n\n");
			}
			return textBuilder.ToString();
		}

		private void EnsureBranchesAreActivated() {
			var activeBranch = this.PluginHost.Application.ActiveDocument.ActiveBranch;
			using( var branchActivatedEvent = new AutoResetEvent(false) ) {
				foreach( var branch in this.PluginHost.Application.ActiveDocument.Branches ) {
					if( Regex.IsMatch(branch.Comment, @"\{\{ignore\}\}") ) {
						continue;
					}

					var needsWaitForBranchRewind = !branch.IsActivated;
					if( needsWaitForBranchRewind ) {
						var subscription = this.PluginHost.Bus.Subscribe<Naracea.Plugin.Messages.BranchShiftingFinished>(m => {
							if( m.Branch == branch ) { branchActivatedEvent.Set(); }
						});

						using( subscription ) {
							this.PluginHost.Application.ActiveDocument.ActiveBranch = branch;
							branchActivatedEvent.WaitOne();
						}
					}
				}
			}

			this.PluginHost.Application.ActiveDocument.ActiveBranch = activeBranch;
		}

		void SetupDocument() {
			var document = this.PluginHost.Application.ActiveDocument;
			var currentSpellingLanguage = document.ActiveBranch.Settings.Get<Naracea.Core.Spellcheck.DictionaryDescriptor>(Settings.SpellcheckLanguage);

			if( currentSpellingLanguage != null ) {
				document.Settings.Set(Settings.SpellcheckLanguage, currentSpellingLanguage);
			}
			document.Settings.Set(Settings.BranchAutoExportFormat, "TXT");
			document.Settings.Set(Settings.BranchAutoExportExtension, ".tw");
			document.Settings.Set(Settings.ViewTextEditorSyntaxHighlighting, "Twee");

			foreach( var branch in document.Branches ) {
				branch.Settings.Set(Settings.BranchAutoExportFormat, "TXT");
				branch.Settings.Set(Settings.BranchAutoExportExtension, ".tw");
				branch.Editor.Settings.Set(Settings.ViewTextEditorSyntaxHighlighting, "Twee");
			}
		}

		private void LoadPluginImages() {
			try {
				this.SmallImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.Twee;component/Images/book_picture_small.png"));
				this.LargeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.Twee;component/Images/book_picture_large.png"));
			} catch( NotSupportedException ) {
				//this is kind of dumb, but when run from nunit, it fails to instantiate Uri because pack:// is not registered
			}
		}
		#endregion
	}
}
