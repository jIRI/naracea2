﻿using Naracea.Plugin;
using Naracea.Plugin.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace Plugin.Export.Txt {
	[Export(typeof(IExportPlugin))]
	public class TextFileExportPlugin : IExportPlugin {
		[ImportingConstructor]
		public TextFileExportPlugin(IPluginHost host) {
			this.PluginHost = host;
			try {
			this.SmallImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.Export.Txt;component/Images/file_extension_txt_small.png"));
			this.LargeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.Export.Txt;component/Images/file_extension_txt_large.png"));
			} catch( NotSupportedException ) {
				//this is kind of dumb, but when run from nunit, it fails to instantiate Uri because pack:// is not registered
			}
		}

		public string PluginId { get { return "D1098DC3-967B-490D-A970-15AAE7E69BC2"; } }

		public Naracea.Plugin.IPluginHost PluginHost { get; private set; }

		public string FileFilter { get { return Properties.Resources.FileFilter; } }

		public string Extension { get { return Properties.Resources.FileExtension; } }

		public string ExporterId { get { return "TXT"; } }

		public string Name { get { return Properties.Resources.Name; } }

		public string ShortDescription { get { return Properties.Resources.ShortDescription; } }
		
		public string LongDescription { get { return Properties.Resources.LongDescription; } }
		
		public System.Windows.Media.Imaging.BitmapImage SmallImage { get; private set; }
		
		public System.Windows.Media.Imaging.BitmapImage LargeImage { get; private set; }

		public IExporter CreateExporter() {
			return new TextFileExporter();
		}

		public Naracea.View.IExportBranchDialog CreateDialog() {
			var dialog = this.PluginHost.CreateView<Naracea.View.IExportBranchDialog>();
			dialog.Filter = Properties.Resources.FileFilter;
			dialog.Title = Properties.Resources.DialogTitle;
			dialog.DefaultExtension = Properties.Resources.FileExtension;
			return dialog;
		}
	}
}
