﻿using Naracea.Plugin;
using Naracea.Plugin.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace Plugin.Export.Html {
	[Export(typeof(IExportPlugin))]
	public class HtmlExportPlugin : IExportPlugin {
		[ImportingConstructor]
		public HtmlExportPlugin(IPluginHost host) {
			this.PluginHost = host;
			try {
				this.SmallImage = new System.Windows.Media.Imaging.BitmapImage(new Uri("pack://application:,,,/Plugin.Export.Html;component/Images/file_extension_html_small.png"));
				this.LargeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri("pack://application:,,,/Plugin.Export.Html;component/Images/file_extension_html_large.png"));
			} catch( NotSupportedException ) {
				//this is kind of dumb, but when run from nunit, it fails to instantiate Uri because pack:// is not registered
			}
		}

		public string PluginId { get { return "50054118-79AF-458A-939C-2F9136D9AE56"; } }

		public Naracea.Plugin.IPluginHost PluginHost { get; private set; }

		public string FileFilter { get { return Properties.Resources.FileFilter; } }

		public string Extension { get { return Properties.Resources.FileExtension; } }

		public string ExporterId { get { return "HTML"; } }

		public string Name { get { return Properties.Resources.Name; } }

		public string ShortDescription { get { return Properties.Resources.ShortDescription; } }
		
		public string LongDescription { get { return Properties.Resources.LongDescription; } }
		
		public System.Windows.Media.Imaging.BitmapImage SmallImage { get; private set; }
		
		public System.Windows.Media.Imaging.BitmapImage LargeImage { get; private set; }

		public IExporter CreateExporter() {
			return new HtmlExporter(this.PluginHost.CoreFactories.CreateMarkupConverter());
		}

		public Naracea.View.IExportBranchDialog CreateDialog() {
			var dialog = this.PluginHost.CreateView<Naracea.View.IExportBranchDialog>();
			dialog.Filter = Properties.Resources.FileFilter;
			dialog.Title = Properties.Resources.DialogTitle;
			dialog.DefaultExtension = Properties.Resources.FileExtension;
			return dialog;
		}
	}
}
