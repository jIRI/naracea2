﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Naracea.Common.Extensions;
using Naracea.Core.Markdown;

namespace Naracea.Plugin.Export {
	internal class HtmlExporter : IExporter {
		IMarkupConverter _converter;

		public HtmlExporter(IMarkupConverter converter) {
			_converter = converter;
		}

		#region IExporter Members
		public event EventHandler<ExportProgress> Progress;

		public void Export(System.IO.Stream stream, string text) {
			this.Progress.Raise(this, new ExportProgress(0));
			using( var writer = new StreamWriter(stream, new UTF8Encoding()) ) {
				try {
					string convertedText = _converter.ToHtmlDocument(text, "style.css");
					writer.Write(convertedText);
				} catch( Exception ex ) {
					throw new Naracea.Plugin.Export.Exceptions.ExportFailedException(ex);
				}
			}
			this.Progress.Raise(this, new ExportProgress(100));
		}
		#endregion
	}
}
