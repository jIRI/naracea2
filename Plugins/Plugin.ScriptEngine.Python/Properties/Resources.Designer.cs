﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Plugin.ScriptEngine.Python.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Plugin.ScriptEngine.Python.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Debug.
        /// </summary>
        internal static string DebugGroupHeader {
            get {
                return ResourceManager.GetString("DebugGroupHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        internal static string EditButton {
            get {
                return ResourceManager.GetString("EditButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open Naracea document containing scripts history. Saving document will update scripts by autoexport feature..
        /// </summary>
        internal static string EditButtonTooltipDescription {
            get {
                return ResourceManager.GetString("EditButtonTooltipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit scripts.
        /// </summary>
        internal static string EditButtonTooltipTitle {
            get {
                return ResourceManager.GetString("EditButtonTooltipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        internal static string EditGroupHeader {
            get {
                return ResourceManager.GetString("EditGroupHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Debug mode.
        /// </summary>
        internal static string EnableDebugButtton {
            get {
                return ResourceManager.GetString("EnableDebugButtton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toggle between normal and debug mode for scripts. Debug mode is less performant, however it allows to attach Visual Studio 2012 to Naracea and debug Python scripts as they are executed..
        /// </summary>
        internal static string EnableDebugButttonDescription {
            get {
                return ResourceManager.GetString("EnableDebugButttonDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enable script debugging.
        /// </summary>
        internal static string EnableDebugButttonTitle {
            get {
                return ResourceManager.GetString("EnableDebugButttonTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PyScript plugin: Execution of script &apos;{0}&apos; failed..
        /// </summary>
        internal static string ErrorScriptExecutionFailed {
            get {
                return ResourceManager.GetString("ErrorScriptExecutionFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PyScript plugin: Unable to create scripts document..
        /// </summary>
        internal static string ErrorUnableCreateScriptDocument {
            get {
                return ResourceManager.GetString("ErrorUnableCreateScriptDocument", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PyScript plugin: Unable to save scripts document..
        /// </summary>
        internal static string ErrorUnableToSaveScriptsFile {
            get {
                return ResourceManager.GetString("ErrorUnableToSaveScriptsFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show log.
        /// </summary>
        internal static string LogButton {
            get {
                return ResourceManager.GetString("LogButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open script output and error log window..
        /// </summary>
        internal static string LogButtonTooltipDescription {
            get {
                return ResourceManager.GetString("LogButtonTooltipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show script log.
        /// </summary>
        internal static string LogButtonTooltipTitle {
            get {
                return ResourceManager.GetString("LogButtonTooltipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Python script engine..
        /// </summary>
        internal static string LongDescription {
            get {
                return ResourceManager.GetString("LongDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Python script engine..
        /// </summary>
        internal static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open folder.
        /// </summary>
        internal static string OpenFolder {
            get {
                return ResourceManager.GetString("OpenFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open folder with all scripts. Usefull when you want to backup or add new scripts..
        /// </summary>
        internal static string OpenFolderTooltipDescription {
            get {
                return ResourceManager.GetString("OpenFolderTooltipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open folder containing scripts.
        /// </summary>
        internal static string OpenFolderTooltipTitle {
            get {
                return ResourceManager.GetString("OpenFolderTooltipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refresh.
        /// </summary>
        internal static string RefreshButton {
            get {
                return ResourceManager.GetString("RefreshButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Builds list of available scripts. This is useful when you add new scripts to scripts folder..
        /// </summary>
        internal static string RefreshButtonTooltipDescription {
            get {
                return ResourceManager.GetString("RefreshButtonTooltipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Refresh script list.
        /// </summary>
        internal static string RefreshButtonTooltipTitle {
            get {
                return ResourceManager.GetString("RefreshButtonTooltipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Run.
        /// </summary>
        internal static string RunButton {
            get {
                return ResourceManager.GetString("RunButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to NOTE: Scripts have full access to your computer. Be careful which scripts from what sources you run..
        /// </summary>
        internal static string RunToolTipDescription {
            get {
                return ResourceManager.GetString("RunToolTipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to run script &quot;{0}&quot;.
        /// </summary>
        internal static string RunToolTipTitle {
            get {
                return ResourceManager.GetString("RunToolTipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scripts.
        /// </summary>
        internal static string ScriptsGroupHeader {
            get {
                return ResourceManager.GetString("ScriptsGroupHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Python script engine..
        /// </summary>
        internal static string ShortDescription {
            get {
                return ResourceManager.GetString("ShortDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stop.
        /// </summary>
        internal static string StopButton {
            get {
                return ResourceManager.GetString("StopButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stop all running scripts..
        /// </summary>
        internal static string StopButtonToolTipDescription {
            get {
                return ResourceManager.GetString("StopButtonToolTipDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stop script execution.
        /// </summary>
        internal static string StopButtonToolTipTitle {
            get {
                return ResourceManager.GetString("StopButtonToolTipTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PyScript.
        /// </summary>
        internal static string TabHeader {
            get {
                return ResourceManager.GetString("TabHeader", resourceCulture);
            }
        }
    }
}
