﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Input;
using MemBus;
using Naracea.Plugin;
using Naracea.Common.Extensions;
using System.IO;
using Naracea.Common;
using System.Threading;

namespace Plugin.ScriptEngine.Python {
	[Export(typeof(IPlugin))]
	public class PythonScriptEnginePlugin : IPlugin {
		#region Attributes
		const string PluginFolder = "PyScript";
		const string ScritpDocumentName = "PyScripts.ncd";
		PyScriptUi _ui;
		PySriptRunner _runner;
		List<string> _scripts = new List<string>();
		bool _debugMode = false;
		#endregion

		#region ctors
		[ImportingConstructor]
		public PythonScriptEnginePlugin(IPluginHost host) {
			this.PluginHost = host;
			this.PyScriptFolder = this.PluginHost.GetPluginFolderPath(PythonScriptEnginePlugin.PluginFolder);
			this.PyScriptFile = Path.Combine(this.PyScriptFolder, PythonScriptEnginePlugin.ScritpDocumentName);

			this.LoadPluginImages();

			// Build plugin UI on app's UI thread.
			// Never do any assumptions on from which thread messages to plugins are passed. Naracea is quite
			// async, so be careful about updating your own UI on UI thread by using plugin host method ExecuteOnUiThread().
			this.PluginHost.Bus.Subscribe<Naracea.Plugin.Messages.Started>(m => this.PluginHost.EnqueuePluginUiInitialization(this, this.InitializeUi));
		}
		#endregion

		#region IPlugin Members
		public IPluginHost PluginHost {
			get;
			private set;
		}

		public string PluginId {
			get { return "4630FA05-EF07-4D80-A877-F4512F8CFCC3"; }
		}

		public string Name {
			get { return Properties.Resources.Name; }
		}

		public string ShortDescription {
			get { return Properties.Resources.ShortDescription; }
		}

		public string LongDescription {
			get { return Properties.Resources.LongDescription; }
		}

		public System.Windows.Media.Imaging.BitmapImage SmallImage {
			get;
			private set;
		}

		public System.Windows.Media.Imaging.BitmapImage LargeImage {
			get;
			private set;
		}
		#endregion

		#region Properties
		internal string PyScriptFolder { get; private set; }
		internal string PyScriptFile { get; private set; }
		#endregion

		#region Private methods
		void InitializeUi() {
			_ui = new PyScriptUi(this.PluginHost);
			_ui.Build();
			_ui.ScriptClicked += this.Ui_ScriptButtonClicked;
			_ui.EditClicked += this.Ui_EditClicked;
			_ui.OpenFolderClicked += this.Ui_OpenFolderClicked;
			_ui.RefreshClicked += this.Ui_RefreshClicked;
			_ui.ShowLogClicked += this.Ui_ShowLogClicked;
			_ui.StopClicked += this.Ui_StopClicked;
			_ui.EnableDebugModeClicked += this.Ui_EnableDebugModeClicked;
			_ui.DisableDebugModeClicked += this.Ui_DisableDebugModeClicked;
			this.InitializeScripts();
			this.RefreshAvailableScripts();
		}

		void Ui_StopClicked(object sender, EventArgs e) {
			this.CreateNewScriptRunner();
			this.InformUiScriptNotRunning();
		}

		void Ui_EnableDebugModeClicked(object sender, EventArgs e) {
			_debugMode = true;
			this.CreateNewScriptRunner();
		}

		void Ui_DisableDebugModeClicked(object sender, EventArgs e) {
			_debugMode = false;
			this.CreateNewScriptRunner();
		}

		void Ui_ShowLogClicked(object sender, EventArgs e) {
			_ui.ShowOutputWindow();
		}

		void Ui_RefreshClicked(object sender, EventArgs e) {
			this.RefreshAvailableScripts();
		}

		void Ui_OpenFolderClicked(object sender, EventArgs e) {
			this.PluginHost.EnsurePluginFodlerExists(PythonScriptEnginePlugin.PluginFolder);
			System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(this.PyScriptFolder));
		}

		void Ui_EditClicked(object sender, EventArgs e) {
			this.EditScriptDocument();
		}

		void Ui_ScriptButtonClicked(object sender, ScripButtonClickedEventArggs e) {
			this.RunScript(e.ScriptIndex);
		}

		private void InitializeScripts() {
			_runner = new PySriptRunner(this.PluginHost, _ui);
		}

		private void LoadPluginImages() {
			try {
				this.SmallImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_small.png"));
				this.LargeImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(@"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_large.png"));
			} catch( NotSupportedException ) {
				//this is kind of dumb, but when run from nunit, it fails to instantiate Uri because pack:// is not registered
			}
		}

		private void EditScriptDocument() {
			Task.Factory.StartNew(() => {
				if( File.Exists(this.PyScriptFile) ) {
					this.PluginHost.Application.OpenDocument(this.PyScriptFile);
				} else {
					// if folder cannot be created, do nothing
					if( this.PluginHost.EnsurePluginFodlerExists(PythonScriptEnginePlugin.PluginFolder) ) {
						var scriptFileBuilder = new ScriptFileBuilder(this.PluginHost, this.PyScriptFile);
						scriptFileBuilder.DocumentCreated += (s, e) => this.PluginHost.Application.OpenDocument(this.PyScriptFile);
						scriptFileBuilder.CreateScriptCodument();
					} else {
						this.PluginHost.ReportError(Properties.Resources.ErrorUnableToSaveScriptsFile, this.PyScriptFile);
					}
				}
			});
		}

		private void RefreshAvailableScripts() {
			// clear scripts buttons
			this.PluginHost.ExecuteOnUiThread(() => _ui.ClearScriptButtons());
			// build new list & buttons
			Task.Factory.StartNew(() => {
				// this doesn't need to be called from UI thread
				_ui.BeginScriptsUpdate();
				// do actual refresh
				this.RefreshAvailableScriptsDo();
				// this doesn't need to be called from UI thread
				_ui.EndScriptsUpdate();
				// and finally refresh UI
				this.PluginHost.NotifyUiUpdated();
			});
		}

		private void RefreshAvailableScriptsDo() {
			this.PluginHost.EnsurePluginFodlerExists(PythonScriptEnginePlugin.PluginFolder);

			var scriptList = new List<string>();
			foreach( var file in Directory.EnumerateFiles(this.PyScriptFolder, "*.py") ) {
				var scriptFileName = Path.GetFileName(file);
				scriptList.Add(scriptFileName);
			}

			_scripts = scriptList;

			//now build buttons on UI thread
			this.PluginHost.ExecuteOnUiThread(() => {
				for( int i = 0; i < _scripts.Count; i++ ) {
					var scriptDisplayName = Path.GetFileNameWithoutExtension(_scripts[i]);
					_ui.BuildAndAddScriptButton(scriptDisplayName, i);
				}
			});
		}

		private void RunScript(int scriptIndex) {
			Task.Factory.StartNew(() => {
				if( scriptIndex >= 0 && scriptIndex < _scripts.Count ) {
					this.InformUiScriptRunning();
					var script = Path.Combine(this.PyScriptFolder, _scripts[scriptIndex]);
					_runner.RunScript(script);
					this.InformUiScriptNotRunning();
				}
			});
		}

		private void InformUiScriptRunning() {
			this.PluginHost.ExecuteOnUiThreadAsync(() => _ui.BeginScriptExecution());
		}

		private void InformUiScriptNotRunning() {
			this.PluginHost.ExecuteOnUiThreadAsync(() => _ui.EndScriptExecution());
		}

		private void CreateNewScriptRunner() {
			if( _runner != null ) {
				_runner.Dispose();
			}
			_runner = new PySriptRunner(this.PluginHost, _ui, _debugMode);
		}
		#endregion
	}
}
