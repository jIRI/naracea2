﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plugin.ScriptEngine.Python {
	public interface IScriptOutput {
		void ShowOutputWindow();
		void HideOutputWindow();
		void ClearLogWindow();
		void LogToOutputWindow(string text);
	}
}
