﻿using MemBus.Subscribing;
using Naracea.Common;
using Naracea.Plugin;
using System;

namespace Plugin.ScriptEngine.Python
{
	internal class ScriptFileBuilder
	{
		#region Attributes
		IPluginHost _pluginHost;
		string _scriptFile;
		#endregion

		#region ctors
		public ScriptFileBuilder(IPluginHost pluginHost, string scriptFile)
		{
			_pluginHost = pluginHost;
			_scriptFile = scriptFile;
		}
		#endregion

		#region Events
		public event EventHandler DocumentCreated;
		#endregion

		#region Public members
		public void CreateScriptCodument()
		{
			// we will wait for doc creation to complete
			var waitForCompletionEvent = new System.Threading.AutoResetEvent(false);
			IPluginHostDocument newDocument = null;
			var subscription = _pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentOpened>(m =>
			{
				newDocument = m.Document;
				waitForCompletionEvent.Set();
			});
			using (subscription)
			{
				// start creating new document
				_pluginHost.Application.CreateNewDocument();
				// wait 10sec for new document
				if (!waitForCompletionEvent.WaitOne(10000))
				{
					// no new doc? fail....
					_pluginHost.ReportError(Properties.Resources.ErrorUnableCreateScriptDocument, "");
					return;
				}
			}
			this.HandleNewDocumentOpened(newDocument);
		}
		#endregion

		#region Private methods
		void HandleNewDocumentOpened(IPluginHostDocument document)
		{
			// check whether this is really new docuemtn (it is not changed and wasn't saved before)
			if (!document.IsDirty && document.Filename == null)
			{
				// setup document
				document.Settings.Set(Settings.DocumentSavingAutosaveEnabled, true);
				document.Settings.Set(Settings.DocumentSavingExportOnSaveAllTouchedBranches, true);
				// setup branches - these are default document settings valid for all branches
				document.Settings.Set(Settings.BranchAutoExportFormat, "TXT");
				document.Settings.Set(Settings.BranchAutoExportExtension, ".py");
				document.Settings.Set(Settings.ViewTextEditorSyntaxHighlighting, "Python");
				// prepare to save configured document
				this.SaveNewScriptsDocument(document);
			}
		}

		private void SaveNewScriptsDocument(IPluginHostDocument document)
		{
			// we will wait for save to complete
			var waitForCompletionEvent = new System.Threading.AutoResetEvent(false);
			// subscribe for message signalling save ended
			var saveSubscription = _pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentSaveCompleted>(
				m => waitForCompletionEvent.Set(),
				new ShapeToFilter<Naracea.Plugin.Messages.DocumentSaveCompleted>(msg => msg.Document == document)
			);
			bool wasFailure = false;
			var saveFailSubscription = _pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentSaveFailed>(
				m =>
				{
					wasFailure = true;
					waitForCompletionEvent.Set();
				},
				new ShapeToFilter<Naracea.Plugin.Messages.DocumentSaveFailed>(msg => msg.Document == document)
			);
			_pluginHost.Application.SaveDocumentAs(document, _scriptFile);
			using (saveSubscription)
			{
				// wait for event, but at maximum 10sec
				if (!waitForCompletionEvent.WaitOne(10000))
				{
					// wait timeout => failure
					wasFailure = true;
				}
				saveFailSubscription.Dispose();
			}

			//check whether there was error while saving and fail if there was
			if (wasFailure)
			{
				_pluginHost.ReportError(Properties.Resources.ErrorUnableToSaveScriptsFile, _scriptFile);
				return;
			}

			// no error, now we need to close and open document to get highlighter and other stuff initialize properly
			// document isn't dirty at this point, so we can subscribe only to close
			var closeSubscription = _pluginHost.Bus.Subscribe<Naracea.Plugin.Messages.DocumentClosed>(
				m => waitForCompletionEvent.Set(),
				new ShapeToFilter<Naracea.Plugin.Messages.DocumentClosed>(msg => msg.Document == document)
			);
			_pluginHost.Application.CloseDocument(document);
			using (closeSubscription)
			{
				// timeout the wait after 10secs
				if (waitForCompletionEvent.WaitOne(10000))
				{
					// it went well, document is closed, now we can open it again
					if (this.DocumentCreated != null)
					{
						this.DocumentCreated(this, new EventArgs());
					}
				}
			}
		}
		#endregion
	}
}
