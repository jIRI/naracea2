﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Plugin;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Utils;
using System.IO;
using Microsoft.Scripting;
using System.Threading;
using System.Reflection;

namespace Plugin.ScriptEngine.Python {
	public class PySriptRunner : IDisposable {
		#region Attributes
		IPluginHost _pluginHost;
		Lazy<Microsoft.Scripting.Hosting.ScriptEngine> _pyEngine;
		IScriptOutput _output;
		Thread _scriptThread;
		#endregion

		#region ctor
		public PySriptRunner(IPluginHost pluginHost, IScriptOutput output)
			: this(pluginHost, output, false) {
		}

		public PySriptRunner(IPluginHost pluginHost, IScriptOutput output, bool enableDebug) {
			_pluginHost = pluginHost;
			_output = output;
			_pyEngine = new Lazy<Microsoft.Scripting.Hosting.ScriptEngine>(() => {
				Dictionary<string, object> options = new Dictionary<string, object>();
				if( enableDebug ) {
					options["Debug"] = true;
				}
				var engine = IronPython.Hosting.Python.CreateEngine(options);
				var paths = engine.GetSearchPaths();				
				paths.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Naracea.Common.Constants.PythonLibraryDirectory));
				engine.SetSearchPaths(paths);
				return engine;
			});
		}
		#endregion

		#region Public methods
		public void RunScript(string scriptFile) {
			_output.ClearLogWindow();
			try {
				_scriptThread = Thread.CurrentThread;
				using( var memStream = new MemoryStream() )
				using( var outStream = new OutputStream(memStream) ) {
					outStream.StreamWritten += (s, e) => _output.LogToOutputWindow(e.Text);
					var engine = _pyEngine.Value;
					engine.Runtime.IO.SetOutput(memStream, outStream);
					engine.Runtime.IO.SetErrorOutput(memStream, outStream);
					var scope = engine.CreateScope();
					scope.SetVariable("PluginHost", _pluginHost);
					var scriptSource = engine.CreateScriptSourceFromFile(scriptFile);
					scriptSource.Execute(scope);
				}
			} catch( SyntaxErrorException ex ) {
				_pluginHost.ReportError(string.Format(Properties.Resources.ErrorScriptExecutionFailed, Path.GetFileName(scriptFile)), ex.Message);
				_output.LogToOutputWindow(Environment.NewLine + string.Format("{0}: {1}", ex.RawSpan, ex.Message));
				_output.ShowOutputWindow();
			} catch( Exception ex ) {
				_pluginHost.ReportError(string.Format(Properties.Resources.ErrorScriptExecutionFailed, Path.GetFileName(scriptFile)), ex.Message);
				_output.LogToOutputWindow(Environment.NewLine + ex.Message);
				_output.ShowOutputWindow();
			}
		}

		public void StopScript() {
			if( _scriptThread != null ) {
				_scriptThread.Abort();
				_scriptThread = null;
			}
		}
		#endregion

		#region Private methods
		#endregion

		#region IDisposable Members
		public void Dispose() {
			if( _pyEngine.IsValueCreated ) {
				this.StopScript();
				_pyEngine.Value.Runtime.Shutdown();
			}
		}
		#endregion
	}
}

