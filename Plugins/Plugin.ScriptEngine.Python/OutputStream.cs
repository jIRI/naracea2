﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Naracea.Common.Extensions;

namespace Plugin.ScriptEngine.Python {
	public class OutputStreamWrittenEventArgs : EventArgs {
		public string Text { get; private set; }
		public OutputStreamWrittenEventArgs(string text) {
			this.Text = text;
		}
	}

	public class OutputStream : StreamWriter {
		public OutputStream(Stream stream)
			: base(stream) {
		}

		public event EventHandler<OutputStreamWrittenEventArgs> StreamWritten;

		public override void Write(char value) {
			base.Write(value);
			this.StreamWritten.Raise(this, new OutputStreamWrittenEventArgs(value.ToString()));
		}
	
		public override void Write(string value) {
			base.Write(value);
			this.StreamWritten.Raise(this, new OutputStreamWrittenEventArgs(value));
		}

		public override void WriteLine(string value) {
			base.WriteLine(value);
			this.StreamWritten.Raise(this, new OutputStreamWrittenEventArgs(value + Environment.NewLine));
		}
	}
}
