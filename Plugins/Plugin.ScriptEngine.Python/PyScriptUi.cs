﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Media.Imaging;
using Naracea.Plugin;

namespace Plugin.ScriptEngine.Python {
	public class ScripButtonClickedEventArggs : EventArgs {
		public int ScriptIndex { get; private set; }
		public ScripButtonClickedEventArggs(int scriptIndex) {
			this.ScriptIndex = scriptIndex;
		}
	}

	internal class PyScriptUi : IScriptOutput {
		#region Attributes
		const string ImageLargeRun = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_go_large.png";
		const string ImageSmallRun = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_go_small.png";
		const string ImageLargeEdit = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_edit_large.png";
		const string ImageSmallEdit = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_edit_small.png";
		const string ImageLargeOpenFolder = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/folder_large.png";
		const string ImageSmallOpenFolder = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/folder_small.png";
		const string ImageLargeRefresh = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/folder_explore_large.png";
		const string ImageSmallRefresh = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/folder_explore_small.png";
		const string ImageSmallShowLog = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_error_small.png";
		const string ImageLargeShowLog = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_error_large.png";
		const string ImageSmallEnableDebug = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/scripts_small.png";
		const string ImageLargeEnableDebug = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/scripts_large.png";
		const string ImageSmallStop = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_delete_small.png";
		const string ImageLargeStop = @"pack://application:,,,/Plugin.ScriptEngine.Python;component/Images/script_delete_large.png";
		IPluginHost _pluginHost;
		bool _updatingScripts = false;
		Forms.ScriptOutput _outputWindow;
		RibbonToggleButton _debugModeToggleButton;
		RibbonButton _stopButton;
		#endregion

		#region ctors
		public PyScriptUi(IPluginHost pluginHost) {
			_pluginHost = pluginHost;
		}
		#endregion

		#region Events
		public event EventHandler<ScripButtonClickedEventArggs> ScriptClicked;
		public event EventHandler EditClicked;
		public event EventHandler OpenFolderClicked;
		public event EventHandler RefreshClicked;
		public event EventHandler ShowLogClicked;
		public event EventHandler StopClicked;
		public event EventHandler EnableDebugModeClicked;
		public event EventHandler DisableDebugModeClicked;
		#endregion

		#region Public methods
		public void Build() {
			this.BuildPyScriptTab();
			var controlsToRegister = new List<Tuple<string, RibbonButton>>();
			controlsToRegister.AddRange(this.BuildEditGroup());
			controlsToRegister.AddRange(this.BuilScriptsGroup());
			controlsToRegister.AddRange(this.BuildDebugGroup());
			_pluginHost.AddRibbonTab(this.Tab);
			foreach( var tuple in controlsToRegister ) {
				_pluginHost.RegisterControlName(tuple.Item1, tuple.Item2);
			}

			//initialize output window
			_outputWindow = new Forms.ScriptOutput();
			_pluginHost.RegisterPluginWindow(_outputWindow);
		}

		public void ShowOutputWindow() {
			_pluginHost.ExecuteOnUiThread(() => {
				_outputWindow.Show();
				_outputWindow.BringIntoView();
				_outputWindow.Activate();
			});
		}

		public void HideOutputWindow() {
			_pluginHost.ExecuteOnUiThread(() => _outputWindow.Hide());
		}

		public void ClearLogWindow() {
			_pluginHost.ExecuteOnUiThread(() => _outputWindow.log.Text = "");
		}

		public void LogToOutputWindow(string text) {
			_pluginHost.ExecuteOnUiThread(() => _outputWindow.log.AppendText(text));
		}

		public RibbonButton BuildScriptButton(string scriptName, int scriptIndex) {
			var uiName = this.MakeControlNameFromScriptName(scriptName);
			var button = new RibbonButton {
				Name = "pyscriptRun_" + uiName,
				QuickAccessToolBarId = "pyscriptRun_" + uiName,
				CanAddToQuickAccessToolBarDirectly = true,
				Label = scriptName,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallRun)),
				LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeRun)),
				KeyTip = scriptIndex < 9 ? (scriptIndex + 1).ToString() : null,
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = string.Format(Properties.Resources.RunToolTipTitle, scriptName),
					Description = Properties.Resources.RunToolTipDescription
				},
				Command = new Naracea.View.Command(this.ExecuteRunCommand, this.CanExecuteRunCommand)
			};
			button.CommandParameter = scriptIndex;
			return button;
		}

		public RibbonButton BuildAndAddScriptButton(string scriptName, int scriptIndex) {
			var button = this.BuildScriptButton(scriptName, scriptIndex);
			this.ScriptsGroup.Items.Add(button);
			_pluginHost.RegisterControlName(button.Name, button);
			return button;
		}

		public void ClearScriptButtons() {
			foreach( var item in this.ScriptsGroup.Items ) {
				var ctrl = item as Control;
				if( ctrl != null ) {
					_pluginHost.UnregisterControlName(ctrl.Name);
				}
			}
			this.ScriptsGroup.Items.Clear();
		}

		public void BeginScriptsUpdate() {
			_updatingScripts = true;
		}

		public void EndScriptsUpdate() {
			_updatingScripts = false;
		}

		public void BeginScriptExecution() {
			_stopButton.IsEnabled = true;
			this.ScriptsGroup.IsEnabled = false;
		}

		public void EndScriptExecution() {
			_stopButton.IsEnabled = false;
			this.ScriptsGroup.IsEnabled = true;
		}
		#endregion

		#region Properties
		public RibbonTab Tab { get; private set; }
		public RibbonGroup ScriptsGroup { get; private set; }
		#endregion

		#region Private methods
		private void BuildPyScriptTab() {
			this.Tab = new RibbonTab {
				Name = "tabPyScript",
				Header = Properties.Resources.TabHeader,
				KeyTip = "P"
			};
		}

		private IEnumerable<Tuple<string, RibbonButton>> BuildEditGroup() {
			var group = new RibbonGroup {
				Name = "groupPyScriptEdit",
				Header = Properties.Resources.EditGroupHeader,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				GroupSizeDefinitions = new RibbonGroupSizeDefinitionBaseCollection() {
					new RibbonGroupSizeDefinition() {
						IsCollapsed = false
					}
				},
			};
			var editButton = new RibbonButton {
				Name = "pyscriptEdit",
				QuickAccessToolBarId = "pyscriptEdit",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.EditButton,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallEdit)),
				LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeEdit)),
				KeyTip = "E",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.EditButtonTooltipTitle,
					Description = Properties.Resources.EditButtonTooltipDescription
				},
				Command = new Naracea.View.Command(this.ExecuteEditCommand, this.CanExecuteEditCommand)
			};
			group.Items.Add(editButton);
			var logButton = new RibbonButton {
				Name = "pyscriptShowLog",
				QuickAccessToolBarId = "pyscriptShowLog",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.LogButton,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallShowLog)),
				//LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeShowLog)),
				KeyTip = "L",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.LogButtonTooltipTitle,
					Description = Properties.Resources.LogButtonTooltipDescription
				},
				Command = new Naracea.View.Command(this.ExecuteShowLogCommand, this.CanExecuteShowLogCommand)
			};
			group.Items.Add(logButton);
			var openFolderButton = new RibbonButton {
				Name = "pyscriptOpenFolder",
				QuickAccessToolBarId = "pyscriptOpenFolder",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.OpenFolder,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallOpenFolder)),
				//LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeOpenFolder)),
				KeyTip = "O",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.OpenFolderTooltipTitle,
					Description = Properties.Resources.OpenFolderTooltipDescription,
				},
				Command = new Naracea.View.Command(this.ExecuteOpenFolderCommand, this.CanExecuteOpenFolderCommand)
			};
			group.Items.Add(openFolderButton);
			var refreshButton = new RibbonButton {
				Name = "pyscriptRefresh",
				QuickAccessToolBarId = "pyscriptRefresh",
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.RefreshButton,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallRefresh)),
				//LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeRefresh)),
				KeyTip = "R",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.RefreshButtonTooltipTitle,
					Description = Properties.Resources.RefreshButtonTooltipDescription
				},
				Command = new Naracea.View.Command(this.ExecuteRefreshCommand, this.CanExecuteRefreshCommand)
			};
			group.Items.Add(refreshButton);
			this.Tab.Items.Add(group);
			return new Tuple<string, RibbonButton>[] {
				Tuple.Create(editButton.Name, editButton),
				Tuple.Create(openFolderButton.Name, editButton),
				Tuple.Create(refreshButton.Name, refreshButton),
			};
		}

		private IEnumerable<Tuple<string, RibbonButton>> BuilScriptsGroup() {
			this.ScriptsGroup = new RibbonGroup {
				Name = "groupPyScriptScripts",
				Header = Properties.Resources.ScriptsGroupHeader,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallRun)),
				LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeRun)),				
			};
			this.Tab.Items.Add(this.ScriptsGroup);
			return new Tuple<string, RibbonButton>[] {
			};
		}

		private IEnumerable<Tuple<string, RibbonButton>> BuildDebugGroup() {
			var group = new RibbonGroup {
				Name = "groupPyScriptDebug",
				Header = Properties.Resources.DebugGroupHeader,
				HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center,
				GroupSizeDefinitions = new RibbonGroupSizeDefinitionBaseCollection() {
					new RibbonGroupSizeDefinition() {
						IsCollapsed = false
					}
				},
			};
			_stopButton = new RibbonButton {
				Name = "pyscriptStop",
				QuickAccessToolBarId = "pyscriptStop",
				IsEnabled = false,
				CanAddToQuickAccessToolBarDirectly = true,
				Label = Properties.Resources.StopButton,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallStop)),
				LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeStop)),
				KeyTip = "S",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.StopButtonToolTipTitle,
					Description = Properties.Resources.StopButtonToolTipDescription
				},
				Command = new Naracea.View.Command(this.ExecuteStopCommand, this.CanExecuteStopCommand)
			};
			group.Items.Add(_stopButton);
			_debugModeToggleButton = new RibbonToggleButton {
				Name = "pyscriptEnableDebug",
				QuickAccessToolBarId = "pyscriptEnableDebug",
				CanAddToQuickAccessToolBarDirectly = false,
				Label = Properties.Resources.EnableDebugButtton,
				SmallImageSource = new BitmapImage(new Uri(PyScriptUi.ImageSmallEnableDebug)),
				LargeImageSource = new BitmapImage(new Uri(PyScriptUi.ImageLargeEnableDebug)),
				KeyTip = "D",
				VerticalAlignment = System.Windows.VerticalAlignment.Top,
				ToolTip = new RibbonToolTip {
					Title = Properties.Resources.EnableDebugButttonTitle,
					Description = Properties.Resources.EnableDebugButttonDescription
				},
				Command = new Naracea.View.Command(this.ExecuteToggleDebugModeCommand, this.CanExecuteToggleDebugModeCommand)
			};
			group.Items.Add(_debugModeToggleButton);
			this.Tab.Items.Add(group);
			return new Tuple<string, RibbonButton>[] {
				Tuple.Create(_stopButton.Name, _stopButton),
			};
		}

		private bool CanExecuteRunCommand() {
			return true;
		}

		private void ExecuteRunCommand(object param) {
			if( this.ScriptClicked != null ) {
				this.ScriptClicked(this, new ScripButtonClickedEventArggs((int)param));
			}
		}

		private bool CanExecuteEditCommand() {
			return true;
		}

		private void ExecuteEditCommand() {
			if( this.EditClicked != null ) {
				this.EditClicked(this, new EventArgs());
			}
		}

		private bool CanExecuteRefreshCommand() {
			return !_updatingScripts;
		}

		private void ExecuteRefreshCommand() {
			if( this.RefreshClicked != null ) {
				this.RefreshClicked(this, new EventArgs());
			}
		}

		private bool CanExecuteOpenFolderCommand() {
			return true;
		}

		private void ExecuteOpenFolderCommand() {
			if( this.OpenFolderClicked != null ) {
				this.OpenFolderClicked(this, new EventArgs());
			}
		}

		private bool CanExecuteShowLogCommand() {
			return true;
		}

		private void ExecuteShowLogCommand() {
			if( this.ShowLogClicked != null ) {
				this.ShowLogClicked(this, new EventArgs());
			}
		}

		private bool CanExecuteStopCommand() {
			return true;
		}

		private void ExecuteStopCommand() {
			if( this.StopClicked != null ) {
				this.StopClicked(this, new EventArgs());
			}
		}

		private bool CanExecuteToggleDebugModeCommand() {
			return true;
		}

		private void ExecuteToggleDebugModeCommand() {
			if( _debugModeToggleButton.IsChecked.HasValue ) {
				if( _debugModeToggleButton.IsChecked.Value ) {
					if( this.EnableDebugModeClicked != null ) {
						this.EnableDebugModeClicked(this, new EventArgs());
					}
				} else {
					if( this.DisableDebugModeClicked != null ) {
						this.DisableDebugModeClicked(this, new EventArgs());
					}
				}
			}
		}

		private string MakeControlNameFromScriptName(string scriptName) {
			var validChars = from c in scriptName
											 where char.IsLetterOrDigit(c)
											 select c;
			return new string(validChars.ToArray());
		}
		#endregion
	}
}

