﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.AvalonEdit.Document {
	public class SpellingErrorRange {
		public int Offset { get; set; }
		public int Length { get; set; }
		public SpellingErrorRange(int offset, int len) {
			this.Offset = offset;
			this.Length = len;
		}
	}
}
