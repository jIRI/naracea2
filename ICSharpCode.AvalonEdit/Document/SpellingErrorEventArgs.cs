﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ICSharpCode.AvalonEdit.Document {
	public class SpellingErrorEventArgs : EventArgs {
		public SpellingErrorRange Range { get; private set; }
		public SpellingErrorEventArgs(SpellingErrorRange range) {
			this.Range = range;
		}
	}
}
