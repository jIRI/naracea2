﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ICSharpCode.AvalonEdit.Rendering {
	public class TextLinesCountsCache {
		List<int> _textLinesCounts = new List<int>();

		public TextLinesCountsCache() {
		}

		public void Clear() {
			_textLinesCounts.Clear();
		}

		public bool HasTextLinesCountForDocumentLineNumber(int documentLineNumber) {
			return documentLineNumber < _textLinesCounts.Count;
		}

		public int GetTextLinesCountForDocumentLineNumber(int documentLineNumber) {
			Debug.Assert(this.HasTextLinesCountForDocumentLineNumber(documentLineNumber));
			return _textLinesCounts[documentLineNumber];
		}

		public void SetTextLinesCountForDocumentLineNumber(int documentLineNumber, int textLinesCount) {
			if( this.HasTextLinesCountForDocumentLineNumber(documentLineNumber) ) {
				_textLinesCounts[documentLineNumber] = textLinesCount;
			} else {
				for( int i = _textLinesCounts.Count; i < documentLineNumber; i++ ) {
					_textLinesCounts.Add(1);
				}
				_textLinesCounts.Add(textLinesCount);
			}
		}
	}
}
