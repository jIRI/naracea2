﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ICSharpCode.AvalonEdit.Utils;
using ICSharpCode.AvalonEdit.Document;
using System.Windows;
using System.Windows.Media;

namespace ICSharpCode.AvalonEdit.Rendering {
	/// <summary>
	/// Underlines given spelling errors.
	/// </summary>
	/// <remarks>
	/// This element generator can be easily enabled and configured using the
	/// <see cref="TextEditorOptions"/>.
	/// </remarks>
	internal class SpellingErrorElementGenerator : VisualLineElementGenerator, IBuiltinElementGenerator {
		/// <summary>
		/// Creates a new SpellingErrorElementGenerator.
		/// </summary>
		public SpellingErrorElementGenerator() {
		}

		void IBuiltinElementGenerator.FetchOptions(TextEditorOptions options) {
		}

		IEnumerator<SpellingErrorRange> cachedSpellingErrorEnumerator = null;

		/// <inheritdoc/>
		public override int GetFirstInterestedOffset(int startOffset) {
			if( !this.CurrentContext.Document.SpellingErrors.Any() ) {
				return -1;
			}

			int docLength = this.CurrentContext.Document.TextLength;
			var spellingErrorQuery = from err in this.CurrentContext.Document.SpellingErrors
															 orderby err.Offset ascending
															 where err.Offset >= startOffset
															 select err;
			var first = spellingErrorQuery.FirstOrDefault();
			if( first != null ) {
				cachedSpellingErrorEnumerator = spellingErrorQuery.GetEnumerator();
				return first.Offset;
			} else {
				return -1;
			}
		}

		/// <inheritdoc/>
		public override VisualLineElement ConstructElement(int offset) {
			if( cachedSpellingErrorEnumerator == null || !cachedSpellingErrorEnumerator.MoveNext() ) {
				cachedSpellingErrorEnumerator = null;
				return null;
			}

			var currentSpellingError = cachedSpellingErrorEnumerator.Current;
			int endOffset = CurrentContext.VisualLine.LastDocumentLine.EndOffset;
			int startOffset = currentSpellingError.Offset;			
			bool peekBack = false;
			if( startOffset > 0 ) {
				startOffset--;
				peekBack = true;
			}
			
			StringSegment relevantText = CurrentContext.GetText(startOffset, endOffset - startOffset);
			
			// we are doing little trickery here - while rewinding, due to asynchronicity, we sometimes end up with 
			// a bit shifted spelling intervals, which are not removed by spelling controller.
			// here we remove such interval.
			if( peekBack && !IsWhiteSpaceOrPunctuation(relevantText.Text[relevantText.Offset]) ) {
				this.CurrentContext.Document.SpellingErrors.Remove(currentSpellingError);
				return null;
			}

			int nextSpaceIndex = 0;
			int baseOffset = relevantText.Offset + ( peekBack ? 1 : 0 );
			int count = relevantText.Count - ( peekBack ? 1 : 0 );
			while( nextSpaceIndex < count && !IsWhiteSpaceOrPunctuation(relevantText.Text[baseOffset + nextSpaceIndex]) ) {
				nextSpaceIndex++;
			}
			currentSpellingError.Length = nextSpaceIndex;
			VisualLineSpellingErrorText element = null;
			if( nextSpaceIndex > 0 ) {
				element = new VisualLineSpellingErrorText(this.CurrentContext.VisualLine, nextSpaceIndex);
			}
			return element;
		}

		private static bool IsWhiteSpaceOrPunctuation(char c) {
			return char.IsWhiteSpace(c) || char.IsPunctuation(c);
		}
	}
}
