﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;

namespace ICSharpCode.AvalonEdit.Rendering
{
	/// <summary>
	/// VisualLineElement that represents a piece of text and is a clickable link.
	/// </summary>
	public class VisualLineSpellingErrorText : VisualLineText
	{
		public static Brush UnderlineBrush { get; private set; }
		public static Pen UnderlinePen { get; private set; }

		static VisualLineSpellingErrorText () {
			VisualLineSpellingErrorText.UnderlineBrush = new SolidColorBrush(Colors.Red);
			VisualLineSpellingErrorText.UnderlinePen = new Pen(VisualLineSpellingErrorText.UnderlineBrush, 1.2f);
			VisualLineSpellingErrorText.UnderlinePen.DashStyle = DashStyles.Dash;
		}

		/// <summary>
		/// Creates a visual line text element with the specified length.
		/// It uses the <see cref="ITextRunConstructionContext.VisualLine"/> and its
		/// <see cref="VisualLineElement.RelativeTextOffset"/> to find the actual text string.
		/// </summary>
		public VisualLineSpellingErrorText(VisualLine parentVisualLine, int length)
			: base(parentVisualLine, length)
		{
		}
		
		/// <inheritdoc/>
		public override TextRun CreateTextRun(int startVisualColumn, ITextRunConstructionContext context)
		{
			var decorations = new TextDecorationCollection {
				new TextDecoration(
					TextDecorationLocation.Underline,
					VisualLineSpellingErrorText.UnderlinePen,
					0,
					TextDecorationUnit.Pixel,
					TextDecorationUnit.Pixel
				)
			};
			decorations.Freeze();
			this.TextRunProperties.SetTextDecorations(decorations);
			return base.CreateTextRun(startVisualColumn, context);
		}
		
		/// <inheritdoc/>
		protected override VisualLineText CreateInstance(int length)
		{
			return new VisualLineSpellingErrorText(ParentVisualLine, length);
		}
	}
}
