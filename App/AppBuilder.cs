﻿using System.Diagnostics;
using System.Windows;
using Autofac;
using Naracea.Controller;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using Naracea.View.Context;

namespace Naracea {
	internal class AppBuilder {
		App _app;

		public AppBuilder(App app) {
			Debug.Assert(app != null);
			_app = app;
		}

		public void Build() {
			//core configuration
			var coreConfigurator = new Naracea.Core.Configuration.Configurator();
			//controller configuration
			var controllerConfigurator = new Naracea.Controller.Configuration.Configurator();
			//ui configuration
			var viewConfigurator = new Naracea.Core.Ui.Wpf.Configuration.Configurator(controllerConfigurator.Bus);
			//give app the bus so it can publish args for new instance start
			_app.Bus = controllerConfigurator.Bus;
			//application view
			var viewContext = viewConfigurator.Container.Resolve<IViewContext>();

			//controller context
			var controllerContext = controllerConfigurator.Container.Resolve<IControllerContext>(
				new TypedParameter(typeof(Application), _app),
				new TypedParameter(typeof(ICoreFactories), coreConfigurator.Container.Resolve<ICoreFactories>()),
				new TypedParameter(typeof(IControllerFactories), controllerConfigurator.Container.Resolve<IControllerFactories>()),
				new TypedParameter(typeof(IDateTimeProvider), coreConfigurator.Container.Resolve<IDateTimeProvider>()),
				new TypedParameter(typeof(IViewContext), viewContext),
				new TypedParameter(typeof(IDictionaryStore), coreConfigurator.Container.Resolve<IDictionaryStore>())
			);

			//app controller
			var controller = controllerConfigurator.Container.Resolve<IController>(
				new TypedParameter(typeof(IControllerContext), controllerContext)
			);
		}
	}
}
