﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Shell;

namespace Naracea {
	static public class Program {
		static readonly string _appGuid = "D7F31019-428A-465f-9F6A-DB400F987F4A";

		[STAThread]
		static void Main(string[] args) {
			if( SingleInstance<App>.InitializeAsFirstInstance(_appGuid) ) {
				//Splashscreen first...
				SplashScreen splashScreen = new SplashScreen("SplashScreen.png");
				splashScreen.Show(true);

				//start!
				var app = new App();

				//build the app
				var appBuilder = new AppBuilder(app);
				appBuilder.Build();

				//run the app
				app.Run();

				// Allow single instance code to perform cleanup operations
				SingleInstance<App>.Cleanup();
			}
		}
	}
}
