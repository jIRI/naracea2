﻿using System.Collections.Generic;
using System.Windows;
using Microsoft.Shell;
using MemBus;

namespace Naracea {
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application, ISingleInstanceApp {
		public IBus Bus { get; set; }

		#region ISingleInstanceApp Members
		public bool SignalExternalCommandLineArgs(IList<string> args) {
			this.Bus.Publish(new Controller.Requests.SecondInstanceStartAttempt(args));
			return true;
		}
		#endregion
	}
}
