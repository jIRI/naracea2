﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Autofac;
using System.Diagnostics;
using System.Windows;
using System.Threading;
using System.IO.Packaging;

namespace Naracea.Common.Tests {
	public class FakeApp : Application {
		static FakeApp _instance;
		static Thread _thread = null;
		public static readonly AutoResetEvent Started = new AutoResetEvent(false);

		public static FakeApp Instance() {
			if( _instance == null ) {
				if( _thread == null ) {
					_thread = new Thread(() => {
						_instance = new FakeApp();
						Started.Set();
						_instance.Run();
					});
				}
				_thread.Start();
				Started.WaitOne();
				//this looks strange, but we do not really need running app, we just need object to be passed to controller
				//and since app cannot be restarted, and there can be only one instance in appdomain, we kill it just after start
				//what we lose are "start" events, but we are forcing messages anyway, so it is not a big issue
				_instance.Dispatcher.Invoke(new Action(() => _instance.Shutdown()));
			}
			return _instance;
		}
	}

	public abstract class Context {
		protected ContainerBuilder _builder = null;
		protected IContainer _container;
		protected IList<IDisposable> _disposables = new List<IDisposable>();

		static Context() {
			FakeApp.Instance();
		}

		[TestFixtureSetUp]
		public void FixtureSetup() {
			Debug.WriteLine("+++Setting up test: " + this.GetType().FullName);
			_builder = new ContainerBuilder();
			this.SetUp();
			Debug.WriteLine("***Invoking because of: " + this.GetType().FullName);
			this.BecauseOf();
		}

		[TestFixtureTearDown]
		public void FixtureTearDown() {
			Debug.WriteLine("---Tearing down test: " + this.GetType().FullName);
			this.CustomTearDown();
			if( _container != null ) {
				_container.Dispose();
				_container = null;
				_builder = null;
			}
			foreach( var item in _disposables ) {
				item.Dispose();
			}
			_disposables.Clear();
			Debug.WriteLine("---Test finished: " + this.GetType().FullName);
		}


		protected virtual void CustomTearDown() {
		}

		protected abstract void SetUp();
		protected abstract void BecauseOf();
	}
}
