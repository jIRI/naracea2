﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Extensions;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;

namespace Naracea.Common.Tests.Fakes {
	public class FakeChangeCollection : IChangesCollection {
		List<IChange> _changes;

		public FakeChangeCollection(List<IChange> changes) {
			_changes = changes;
		}

		public int Count {
			get { return _changes.Count; }
		}

		public Core.Model.Changes.IChange At(int index) {
			return _changes[index];
		}

		public int IndexOf(Core.Model.Changes.IChange change) {
			return _changes.IndexOf(change);
		}

		public void Add(Core.Model.Changes.IChange change) {
			_changes.Add(change);
			this.ChangeAdded.Raise(this, new ChangeAddedEventArgs(change));
		}

		public void RemoveLastChange() {
			_changes.RemoveAt(_changes.Count - 1);
		}

		public IEnumerable<Core.Model.Changes.IChange> OwnedChanges {
			get { return _changes; }
		}

		public event EventHandler<ChangeAddedEventArgs> ChangeAdded;

	}
}
