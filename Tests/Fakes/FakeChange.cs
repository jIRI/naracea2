﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core;

namespace Naracea.Common.Tests.Fakes {
	public class FakeChange : IChange {
		static int _automaticId = 0;
		public bool DoCalled { get; private set; }
		public bool UndoCalled { get; private set; }
		public int DoCalledCount { get; private set; }
		public int UndoCalledCount { get; private set; }

		public FakeChange() {
			this.DateTime = DateTimeOffset.UtcNow;
			this.Author = MockRepository.GenerateStub<IAuthor>();
			this.Id = _automaticId++;
			this.ShiftCount = 1;
		}

		public FakeChange(DateTimeOffset now, int id) {
			this.DateTime = now;
			this.Id = id;
			this.Author = MockRepository.GenerateStub<IAuthor>();
		}

		public virtual void ExecuteDo(ITextEditor editor) {
			this.DoCalled = true;
			this.DoCalledCount++;
		}

		public virtual void ExecuteUndo(ITextEditor editor) {
			this.UndoCalled = true;
			this.UndoCalledCount++;
		}

		public int Id { get; set; }
		public DateTimeOffset DateTime { get; private set; }
		public Core.Model.IAuthor Author { get; private set; }
		public bool ShouldShifterExecuteFollowingChange { get; set; }
		public int ShiftCount { get; set; }

		public void OnStoring(ITextEditor editor) {
		}


		#region IChange Members


		public string VisualText {
			get { return "*"; }
		}

		#endregion
	}
}
