﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core;

namespace Naracea.Common.Tests.Fakes {
	public class FakeChangeInsertText : FakeChange {
		public FakeChangeInsertText(string text, int pos) 
		: base() {
			this.Text = text;
			this.Position = pos;
		}

		public override void ExecuteDo(ITextEditor editor) {
			base.ExecuteDo(editor);
			editor.InsertTextBehindPosition(this.Position, this.Text);
		}

		public override void ExecuteUndo(ITextEditor editor) {
			base.ExecuteUndo(editor);
			editor.DeleteText(this.Position, this.Text);
		}


		public string Text { get; private set; }
		public int Position { get; private set; }
	}
}
