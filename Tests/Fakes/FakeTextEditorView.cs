﻿using MemBus;
using Naracea.Common.Extensions;
using Naracea.View;
using Naracea.View.Events;
using System;
using System.Collections.Generic;


#pragma warning disable 0067
namespace Naracea.Common.Tests.Fakes {
	public class FakeTextEditorView : ITextEditorView {
		IBus _bus;

		public FakeTextEditorView(IBus bus) {
			_bus = bus;
		}

		public void EditInsertText(IEnumerable<string> paragraphs) {
			int position = 0;
			foreach (var text in paragraphs) {
				for (int i = 0; i < text.Length; i++) {
					this.EditInsertText(position++, text[i].ToString());
				}
				this.EditInsertText(position, Naracea.Common.Constants.NewLine);
				position += Naracea.Common.Constants.NewLine.Length;
			}
		}

		public void EditInsertText(string text) {
			for (int i = 0; i < text.Length; i++) {
				this.EditInsertText(i, text[i].ToString());
			}
		}

		public void EditInsertText(IEnumerable<Tuple<int, string>> edits) {
			foreach (var edit in edits) {
				this.EditInsertText(edit.Item1, edit.Item2);
			}
		}

		public void EditInsertText(int pos, string text) {
			this.StoreTextInserted.Raise(this, new StoreTextInsertedArgs(this, pos, text));
			this.RawTextChanged.Raise(this, new RawTextChangedArgs(this, pos, text.Length, 0));
			this.InsertTextBeforePosition(pos, text);
		}

		public void EditDeleteText(int pos, string text) {
			this.StoreTextDeleted.Raise(this, new StoreTextDeletedArgs(this, pos, text));
			this.RawTextChanged.Raise(this, new RawTextChangedArgs(this, pos, 0, text.Length));
			this.DeleteText(pos, text);
		}

		public void EditBackspaceText(int pos, string text) {
			this.StoreTextBackspaced.Raise(this, new StoreTextBackspacedArgs(this, pos, text));
			this.RawTextChanged.Raise(this, new RawTextChangedArgs(this, pos, 0, text.Length));
			this.BackspaceText(pos, text);
		}

		public void EditUndo() {
			this.StoreUndo(this, new StoreUndoArgs(this));
		}

		public void EditUndoWord() {
			this.StoreUndoWord.Raise(this, new StoreUndoWordArgs(this));
		}

		public void EditUndoSentence() {
			this.StoreUndoSentence.Raise(this, new StoreUndoSentenceArgs(this));
		}

		public void EditRedo() {
			this.StoreRedo.Raise(this, new StoreRedoArgs(this));
		}

		public void EditPasteCharsAsChanges(int pos, string text) {
			this.PasteCharsAsChanges.Raise(this, new PasteCharsAsChangesArgs(this, pos, text));
			this.RawTextChanged.Raise(this, new RawTextChangedArgs(this, pos, text.Length, 0));
		}

		public event EventHandler CanEditChanged;
		bool _canEdit;
		public bool CanEdit {
			get {
				return _canEdit;
			}
			set {
				_canEdit = value;
				this.CanEditChanged.Raise(this, new EventArgs());
			}
		}
		public bool CanUndo { get; set; }
		public bool CanRedo { get; set; }

		public string Text { get; set; }
		public int CaretPosition { get; set; }
		public int CurrentLine { get; set; }
		public int CurrentColumn { get; set; }
		public int TextLength { get; set; }
		public bool IsOverwriteMode { get; set; }
		public bool WrapsLines { get; set; }

		public string SelectedText { get; set; }

		public event EventHandler<RawTextChangedArgs> RawTextChanged;
		public event EventHandler<StoreTextInsertedArgs> StoreTextInserted;
		public event EventHandler<StoreTextDeletedArgs> StoreTextDeleted;
		public event EventHandler<StoreTextBackspacedArgs> StoreTextBackspaced;
		public event EventHandler<StoreGroupBeginArgs> StoreGroupBegin;
		public event EventHandler<StoreGroupEndArgs> StoreGroupEnd;
		public event EventHandler<StoreUndoArgs> StoreUndo;
		public event EventHandler<StoreUndoWordArgs> StoreUndoWord;
		public event EventHandler<StoreUndoSentenceArgs> StoreUndoSentence;
		public event EventHandler<StoreRedoArgs> StoreRedo;
		public event EventHandler<PasteCharsAsChangesArgs> PasteCharsAsChanges;
		public event EventHandler<UpdateIndicatorsArgs> UpdateIndicators;

		public void InsertTextBeforePosition(int pos, string text) {
			if (this.Text == null) {
				this.Text = string.Empty;
			}
			this.Text = this.Text.Insert(pos, text);
			this.CaretPosition = pos + text.Length;
		}

		public void InsertTextBehindPosition(int pos, string text) {
			this.Text = this.Text.Insert(pos, text);
			this.CaretPosition = pos;
		}

		public void DeleteText(int pos, string text) {
			this.Text = this.Text.Remove(pos, text.Length);
			this.CaretPosition = pos;
		}

		public void BackspaceText(int pos, string text) {
			this.Text = this.Text.Remove(pos, text.Length);
			this.CaretPosition = pos;
		}

		public void SetTextSilently(string text) {
			this.Text = text;
		}

		public bool IsBusy { get; private set; }

		public void BeginBusy() {
			this.IsBusy = true;
		}

		public void EndBusy() {
			this.IsBusy = false;
		}

		public void Focus() {
		}

		public object Controller { get; set; }

		public object Control {
			get { return null; }
		}

		public void UpdateEditorProperties() {
			_bus.Publish(new View.Requests.UpdateIndicators(this));
		}

		public void Open(ISettings settings) {
		}

		public void Close() {
		}

		public void Select(int position, int length) {
			this.SelectedText = this.Text.Substring(position, length);
			this.CaretPosition = position;
		}

		public void ReplaceSelection(string replaceWith) {
			var text = this.Text.Remove(this.CaretPosition, this.SelectedText.Length);
			this.Text = text.Insert(this.CaretPosition, replaceWith);
		}

		public void Exec(Action action) {
			action();
		}

		#region ITextEditorView Members
		public void BeginUpdate() {
		}

		public void EndUpdate() {
		}

		public int CurrentParagraph { get; set; }
		public bool ShowWhitespaces { get; set; }
		public int TabSize { get; set; }
		public bool KeepTabs { get; set; }

		public string SyntaxHighlighting { get; set; }
		public string SpellcheckLanguageName { get; set; }


		public void SetSpellingErrors(IEnumerable<Tuple<int, int>> errors) {
		}

		public void ClearSpellingErrors() {
		}

		public void SetSpellingSuggestions(IEnumerable<string> suggestions) {
		}
		#endregion

		#region ITextEditorView Members


		public void Insert(int position, string text) {
			throw new NotImplementedException();
		}

		public void Delete(int position, int length) {
			throw new NotImplementedException();
		}

		#endregion

		#region ITextEditorView Members


		public void Insert(string text) {
			throw new NotImplementedException();
		}

		public void Delete(int length) {
			throw new NotImplementedException();
		}

		#endregion
	}
}
