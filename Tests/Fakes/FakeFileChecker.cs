﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core;

namespace Naracea.Common.Tests.Fakes {
	public class FakeFileChecker : Core.FileSystem.IFileChecker {
		#region IFileChecker Members
		public bool Exists(string path) {
			return true;
		}
		#endregion
	}
}
