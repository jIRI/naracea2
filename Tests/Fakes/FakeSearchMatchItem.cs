﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.View;

namespace Tests.Fakes {
	public class FakeSearchMatchItem : ISearchMatchItem {
		public FakeSearchMatchItem() {
		}

		#region ISearchMatchItem Members
		public string SearchPattern { get; set; }
		public int PositionInText { get; set; }
		public int PositionInPercents { get; set; }
		public string SurroundedMatchText { get; set; }
		public string MatchedText { get; set; }
		public int MatchOffsetInText { get; set; }
		public IDocumentView DocumentView { get; set; }
		public IBranchView BranchView { get; set; }
		public string DocumentName { get; set; }
		public string BranchName { get; set; }
		public int CurrentChangeIndex { get; set; }
		public bool IsHistorySearch { get; set; }
		public DateTime DateTime{ get; set; }
		#endregion
	}
}
