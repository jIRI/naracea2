﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Diagnostics;


namespace Naracea.Common.Tests.BddAsserts {
	public static class ObjectExtensions {

		[DebuggerStepThrough]
		public static void ShouldBeNull(this object obj) {
			Assert.That(obj, Is.Null);
		}

		[DebuggerStepThrough]
		public static void ShouldBeSameAs(this object actual, object expected) {
			Assert.That(actual, Is.SameAs(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldBeType(this object obj,Type expectedType) {
			Assert.That(obj, Is.TypeOf(expectedType));
		}

		[DebuggerStepThrough]
		public static void ShouldEqualTo<T>(this T actual, T expected) {
			Assert.That(actual, Is.EqualTo(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldNotBeNull(this object obj) {
			Assert.That(obj, Is.Not.Null);
		}

		[DebuggerStepThrough]
		public static void ShouldNotBeSameAs(this object actual, object expected) {
			Assert.That(actual, Is.Not.SameAs(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldNotBeType(this object obj, Type expectedType) {
			Assert.That(obj, Is.Not.TypeOf(expectedType));
		}

		[DebuggerStepThrough]
		public static void ShouldNotEqual<T>(this T actual, T expected) {
			Assert.That(actual, Is.Not.EqualTo(expected));
		}
	}
}
