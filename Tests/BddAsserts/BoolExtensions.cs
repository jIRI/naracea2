﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Diagnostics;

namespace Naracea.Common.Tests.BddAsserts {
	public static class BoolExtensions {
		[DebuggerStepThrough]
		public static void ShouldBeFalse(this bool condition) {
			Assert.That(condition, Is.False);
		}

		[DebuggerStepThrough]
		public static void ShouldBeTrue(this bool condition) {
			Assert.That(condition, Is.True);
		}
	}
}
