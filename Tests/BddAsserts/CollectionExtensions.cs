﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Diagnostics;


namespace Naracea.Common.Tests.BddAsserts {
	public static class CollectionExtensions {
		[DebuggerStepThrough]
		public static void ShouldBeEmpty<T>(this IEnumerable<T> collection) {
			Assert.That(collection, Is.Empty);
		}

		[DebuggerStepThrough]
		public static void ShouldContain<T>(this IEnumerable<T> collection, T expected) {
			Assert.That(collection, Has.Member(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldNotBeEmpty<T>(this IEnumerable<T> collection) {
			Assert.That(collection, Is.Not.Empty);
		}

		[DebuggerStepThrough]
		public static void ShouldNotContain<T>(this IEnumerable<T> collection, T expected) {
			Assert.That(collection, Has.No.Member(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldBeEqualTo<T>(this IEnumerable<T> collection, IEnumerable<T> expected) {
			Assert.That(collection, Is.EqualTo(expected));
		}

		[DebuggerStepThrough]
		public static void ShouldBeEquivalentTo<T>(this IEnumerable<T> collection, IEnumerable<T> expected) {
			Assert.That(collection, Is.EquivalentTo(expected));
		}
	}
}
