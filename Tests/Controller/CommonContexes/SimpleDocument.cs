﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.View.Context;
using Naracea.View;
using Autofac;
using Naracea.Core;
using Naracea.Core.Builders;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.CommonContexes {
	public class SimpleDocument {
		public IList<IBranch> Branches { get; private set; }
		public IAuthor Author { get; private set; }
		public IDocument Document { get; private set; }

		public void SetUp(IContainer container) {
			this.Author = container.Resolve<IAuthor>();
			this.Document = container.Resolve<IDocument>();

			var authors = new List<IAuthor>();
			authors.Add(this.Author);
			this.Document.Stub(a => a.Authors).Return(authors);

			this.Branches = new List<IBranch>();
			this.Document.Stub(a => a.Branches).Return(this.Branches);

			var branchController = container.Resolve<IBranchController>();
			branchController.Stub(a => a.Branch).Return(container.Resolve<IBranch>());
			branchController.Stub(a => a.View).Return(container.Resolve<IBranchView>());
		}

		public void NoBranchControllerSetUp(IContainer container) {
			this.Author = container.Resolve<IAuthor>();
			this.Document = container.Resolve<IDocument>();

			var authors = new List<IAuthor>();
			authors.Add(this.Author);
			this.Document.Stub(a => a.Authors).Return(authors);

			this.Branches = new List<IBranch>();
			this.Document.Stub(a => a.Branches).Return(this.Branches);
		}

		public void AddFirstBranch() {
			Assert.That(this.Branches, Is.Not.Null);
			var branch = MockRepository.GenerateStub<IBranch>();
			branch.Stub(a => a.Author).Return(this.Author);
			branch.Name = "Branch 1";
			branch.Stub(a => a.BranchingDateTime).Return(DateTimeOffset.UtcNow);
			branch.Comment = "Comment 1";
			var changes = MockRepository.GenerateStub<IChangesCollection>();
			changes.Stub(a => a.OwnedChanges).Return(this.GetChanges1(this.Author));
			branch.Stub(a => a.Changes).Return(changes);
			var metadata = MockRepository.GenerateStub<IMetadata>();
			branch.Stub(a => a.Metadata).Return(metadata);
			this.Branches.Add(branch);

			this.Document.CurrentBranch = this.Branches[0];
		}

		public void AddSecondBranch() {
			Assert.That(this.Branches.Count, Is.EqualTo(1));
			var branch = MockRepository.GenerateStub<IBranch>();
			branch.Stub(a => a.Author).Return(this.Author);
			branch.Name = "Branch 2";
			branch.Stub(a => a.BranchingDateTime).Return(DateTimeOffset.UtcNow);
			branch.Comment = "Comment 2";
			branch.Stub(a => a.BranchingChangeIndex).Return(this.Branches[0].Changes.Count - 1);
			branch.Stub(a => a.Parent).Return(this.Branches[0]);
			var changes = MockRepository.GenerateStub<IChangesCollection>();
			changes.Stub(a => a.OwnedChanges).Return(this.GetChanges2(this.Author));
			branch.Stub(a => a.Changes).Return(changes);
			var metadata = MockRepository.GenerateStub<IMetadata>();
			branch.Stub(a => a.Metadata).Return(metadata);
			this.Document.CurrentBranch = this.Branches[1];
		}

		IList<IChange> GetChanges1(IAuthor author) {
			var changes = new List<IChange>() {
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "a",0)),
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "b",1)),
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "c",2)),
				new DeleteText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "a",0)),
				new BackspaceText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "b", 1)),
			};
			return changes;
		}

		IList<IChange> GetChanges2(IAuthor author) {
			var changes = new List<IChange>() {
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "x", 1)),
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "y", 1)),
				new InsertText(new WithTextChangeArgs(author, DateTimeOffset.UtcNow, "z", 1)),
			};
			return changes;
		}
	}
}
