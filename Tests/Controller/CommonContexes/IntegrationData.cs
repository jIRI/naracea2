﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Autofac;
using Naracea.Core.Repository;
using Naracea.Core;

namespace Naracea.Controller.Tests.CommonContexes {
	public class IntegrationData {
		public IDocument CreateDefaultDocument() {
			var expectedDocument = new Document {
			};
			expectedDocument.Authors.Add(new Author());
			expectedDocument.Branches.Add(
				new Naracea.Core.Model.Branch(
					new BranchArgs(expectedDocument.Authors[0], null, 0, DateTimeOffset.UtcNow)
			));

			return expectedDocument;
		}

		public IDocument CreateAndSaveSimpleDocument(string filename, ICoreFactories factories) {
			var expectedDocument = CreateSimpleDocument();
			//save
			var repoContext = factories.CreateRepositoryContext();
			repoContext.Path = filename;
			using( var repository = factories.CreateDocumentRepository(repoContext) ) {
				repository.Save(expectedDocument);
			}
			return expectedDocument;
		}

		public IDocument CreateSimpleDocument() {
			//create document with branch and save it
			var expectedDocument = new Document {
				Name = "Expected document name",
				Comment = "Expected comment",
			};
			expectedDocument.Authors.Add(new Author());
			expectedDocument.Branches.Add(
				new Branch(
					new BranchArgs(expectedDocument.Authors[0], null, 0, DateTimeOffset.UtcNow)
				) {
				Name = "Expected branch name",
				Comment = "Expected branch comment"
			});
			expectedDocument.CurrentBranch = expectedDocument.Branches[0];

			return expectedDocument;
		}
	}
}
