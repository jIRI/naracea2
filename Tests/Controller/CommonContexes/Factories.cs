﻿using System;
using System.Collections.Generic;
using Autofac;
using Naracea.Common;
using Naracea.Core;
using Naracea.Core.Builders;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Finders.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Spellcheck;
using Rhino.Mocks;

namespace Naracea.Controller.Tests.CommonContexes {
	public class Factories {
		public IControllerFactories StubbedControllerFactories { get; private set; }
		public ICoreFactories StubbedCoreFactories { get; private set; }

		public Factories Register(ContainerBuilder builder) {
			//controllers
			builder.Register<IControllerFactories>(c => MockRepository.GenerateStub<IControllerFactories>()).SingleInstance();
			builder.Register<IController>(c => MockRepository.GenerateStub<IController>()).SingleInstance();
			builder.Register<IDocumentController>(c => MockRepository.GenerateStub<IDocumentController>()).SingleInstance();
			builder.Register<IBranchController>(c => MockRepository.GenerateStub<IBranchController>()).SingleInstance();
			builder.Register<IAuthorController>(c => MockRepository.GenerateStub<IAuthorController>()).SingleInstance();
			builder.Register<ITextEditorController>(c => MockRepository.GenerateStub<ITextEditorController>()).SingleInstance();
			builder.Register<ISpellcheckController>(c => MockRepository.GenerateStub<ISpellcheckController>()).SingleInstance();
			builder.Register<ITimelineController>(c => MockRepository.GenerateStub<ITimelineController>()).SingleInstance();
			builder.Register<IChangeStreamController>(c => MockRepository.GenerateStub<IChangeStreamController>()).SingleInstance();
			builder.Register<ITextEditor>(c => MockRepository.GenerateStub<ITextEditor>()).SingleInstance();
			builder.Register<ITextEditorProxy>(c => MockRepository.GenerateStub<ITextEditorProxy>()).SingleInstance();
			builder.Register<ISettings>(c => MockRepository.GenerateStub<ISettings>()).SingleInstance();

			//core
			builder.Register<ICoreFactories>(c => MockRepository.GenerateStub<ICoreFactories>()).SingleInstance();
			builder.Register<IAuthor>(c => MockRepository.GenerateStub<IAuthor>()).SingleInstance();
			builder.Register<IBranch>(c => MockRepository.GenerateStub<IBranch>()).SingleInstance();
			builder.Register<IDocument>(c => MockRepository.GenerateStub<IDocument>()).SingleInstance();
			builder.Register<IDocumentBuilder>(c => MockRepository.GenerateStub<IDocumentBuilder>()).SingleInstance();
			builder.Register<IBranchBuilder>(c => MockRepository.GenerateStub<IBranchBuilder>()).SingleInstance();
			builder.Register<IRepository<IDocument>>(c => MockRepository.GenerateStub<IRepository<IDocument>>()).SingleInstance();
			builder.Register<IRepositoryContext>(c => MockRepository.GenerateStub<IRepositoryContext>()).SingleInstance();
			builder.Register<IShifter>(c => MockRepository.GenerateStub<IShifter>()).SingleInstance();
			builder.Register<IUndoRedoFinder>(c => MockRepository.GenerateStub<IUndoRedoFinder>()).SingleInstance();
			builder.Register<ITextFinder>(c => MockRepository.GenerateStub<ITextFinder>()).SingleInstance();
			builder.Register<IMetadata>(c => MockRepository.GenerateStub<IMetadata>()).SingleInstance();
			builder.Register<ISpellchecker>(c => MockRepository.GenerateStub<ISpellchecker>()).SingleInstance();

			return this;
		}

		public void Stub(IContainer container) {
			//controller
			this.StubbedControllerFactories = container.Resolve<IControllerFactories>();
			this.StubbedControllerFactories.Stub(a => a.CreateAuthorController(null, null)).Return(container.Resolve<IAuthorController>());

			//core factories
			this.StubbedCoreFactories = container.Resolve<ICoreFactories>();
			
			//core-entites
			this.StubbedCoreFactories.Stub(a => a.CreateAuthor()).Return(container.Resolve<IAuthor>());
			this.StubbedCoreFactories.Stub(a => a.CreateDocument()).Return(container.Resolve<IDocument>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateBranch(null, null, 0, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(container.Resolve<IBranch>());
			//core-entities-changes
			this.StubbedCoreFactories
				.Stub(a => a.CreateBackspaceText(0, null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithText>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateDeleteText(0, null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithText>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateInsertText(0, null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithText>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateRedo(null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithChange>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateUndo(null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithChange>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateMultiUndo(null, null, DateTimeOffset.UtcNow))
				.IgnoreArguments()
				.Return(MockRepository.GenerateStub<IChangeWithChange>());
			this.StubbedCoreFactories
				.Stub(a => a.CreateTextEditorProxy(null))
				.IgnoreArguments()
				.Return(container.Resolve<ITextEditorProxy>());

			//core-repository
			this.StubbedCoreFactories.Stub(a => a.CreateDocumentRepository(null))
				.IgnoreArguments()
				.Return(container.Resolve<IRepository<IDocument>>());
			this.StubbedCoreFactories.Stub(a => a.CreateRepositoryContext())
				.Return(container.Resolve<IRepositoryContext>());
		
			//core-shifter
			this.StubbedCoreFactories.Stub(a => a.CreateShifterToStart())
				.Return(container.Resolve<IShifter>());
			this.StubbedCoreFactories.Stub(a => a.CreateShifterToEnd())
				.Return(container.Resolve<IShifter>());
			this.StubbedCoreFactories.Stub(a => a.CreateShifterToChange(0))
				.IgnoreArguments()
				.Return(container.Resolve<IShifter>());
			
			//finders
			this.StubbedCoreFactories.Stub(a => a.CreateUndoRedoFinder())
				.Return(container.Resolve<IUndoRedoFinder>());
			var undoRedoFinder = container.Resolve<IUndoRedoFinder>();
			undoRedoFinder.Stub(a => a.FindNextUndoableChange(null)).IgnoreArguments().Return(null);
			undoRedoFinder.Stub(a => a.FindNextRedoableChange(null)).IgnoreArguments().Return(null);
			this.StubbedCoreFactories.Stub(a => a.CreatePlainTextFinder())
				.Return(container.Resolve<ITextFinder>());

			//document
			var doc = container.Resolve<IDocument>();
			doc.Stub(a => a.Authors).Return(new List<IAuthor> { new Author() });
			doc.Stub(a => a.Metadata).Return(container.Resolve<IMetadata>());

			//branch
			var branch = container.Resolve<IBranch>();
			branch.Stub(a => a.Metadata).Return(container.Resolve<IMetadata>());

			//core builders
			this.StubbedCoreFactories.Stub(a => a.BuildBranch(null, null, null))
				.IgnoreArguments()
				.Return(container.Resolve<IBranch>());
			this.StubbedCoreFactories.Stub(a => a.BuildDocument()).Return(container.Resolve<IDocument>());

		}
	}
}
