﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.View.Context;
using Naracea.View;
using Autofac;
using Naracea.Core;
using Naracea.Core.Builders;
using Naracea.Core.Model;
using Naracea.Core.Repository;

namespace Naracea.Controller.Tests.CommonContexes {
	public class EmptyDocument {
		public void SetUp(IContainer container) {
			var branches = new List<IBranch>();
			this.SetUp(container, branches);
		}

		public void SetUp(IContainer container, IList<IBranch> branches) {
			var document = container.Resolve<IDocument>();

			document.Stub(a => a.Branches).Return(branches);

			var authors = new List<IAuthor>();
			document.Stub(a => a.Authors).Return(authors);

			var branchController = container.Resolve<IBranchController>();
			branchController.Stub(a => a.Branch).Return(container.Resolve<IBranch>());
			branchController.Stub(a => a.View).Return(container.Resolve<IBranchView>());

			var authorControler = container.Resolve<IAuthorController>();
			authorControler.Stub(a => a.CurrentAuthor).Return(container.Resolve<Core.Model.IAuthor>());
		}
	}
}
