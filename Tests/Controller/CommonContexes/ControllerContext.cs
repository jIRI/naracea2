﻿using Autofac;
using Naracea.Common;
using Naracea.Core;
using Naracea.Core.FileSystem;
using Naracea.Plugin.Export;
using Naracea.View.Context;
using Rhino.Mocks;
using System.Collections.Generic;

namespace Naracea.Controller.Tests.CommonContexes {
	public class ControllerContext {
		public IControllerContext StubbedControllerContext { get; private set; }

		public ControllerContext Register(ContainerBuilder builder) {
			builder.Register<IControllerContext>(c => MockRepository.GenerateStub<IControllerContext>()).SingleInstance();
			builder.Register<IDateTimeProvider>(c => MockRepository.GenerateStub<IDateTimeProvider>()).SingleInstance();
			builder.Register<IFileNameValidator>(c => MockRepository.GenerateStub<IFileNameValidator>()).SingleInstance();
			builder.Register<ICurrentVersion>(c => MockRepository.GenerateStub<ICurrentVersion>()).SingleInstance();
			return this;
		}

		public IControllerContext Stub(IContainer container) {
			this.StubbedControllerContext = container.Resolve<IControllerContext>();
			//controller
			this.StubbedControllerContext.Stub(a => a.Controller).Return(container.Resolve<IControllerFactories>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateAuthorController(null, null))
				.IgnoreArguments()
				.Return(container.Resolve<IAuthorController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateBranchController(null, null, null))
				.IgnoreArguments()
				.Return(container.Resolve<IBranchController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateDocumentController(null))
				.IgnoreArguments()
				.Return(container.Resolve<IDocumentController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateTextEditorBridge(null))
				.IgnoreArguments()
				.Return(container.Resolve<ITextEditor>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateTextEditorController(null, null, null))
				.IgnoreArguments()
				.Return(container.Resolve<ITextEditorController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateSpellcheckController(null, null, null, null))
				.IgnoreArguments()
				.Return(container.Resolve<ISpellcheckController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateTimelineController(null, null))
				.IgnoreArguments()
				.Return(container.Resolve<ITimelineController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateChangeStreamController(null, null))
				.IgnoreArguments()
				.Return(container.Resolve<IChangeStreamController>());
			this.StubbedControllerContext.Controller.Stub(a => a.CreateSettings(null, null))
				.IgnoreArguments()
				.Return(container.Resolve<ISettings>());

			//others
			this.StubbedControllerContext.Stub(a => a.Core).Return(container.Resolve<ICoreFactories>());
			this.StubbedControllerContext.Stub(a => a.DateTimeProvider).Return(container.Resolve<IDateTimeProvider>());
			this.StubbedControllerContext.Stub(a => a.FileNameValidator).Return(container.Resolve<IFileNameValidator>());
			this.StubbedControllerContext.Stub(a => a.View).Return(container.Resolve<IViewContext>());
			this.StubbedControllerContext.Stub(a => a.Settings).Return(container.Resolve<ISettings>());
			this.StubbedControllerContext.Stub(a => a.Bus).Return(MemBus.BusSetup.StartWith<MemBus.Configurators.Conservative>().Construct());

			this.StubbedControllerContext.Stub(a => a.Exporters).Return(new Dictionary<string, IExportPlugin> {
				{"dummy", MockRepository.GenerateStub<IExportPlugin>()}
			});		

			//settings
			var branchController = container.Resolve<IBranchController>();
			branchController.Stub(a => a.Settings).Return(container.Resolve<ISettings>());
			var timelineController = container.Resolve<ITimelineController>();
			timelineController.Stub(a => a.Settings).Return(container.Resolve<ISettings>());
			var changeStreamController = container.Resolve<IChangeStreamController>();
			changeStreamController.Stub(a => a.Settings).Return(container.Resolve<ISettings>());

			return this.StubbedControllerContext;
		}
	}
}
