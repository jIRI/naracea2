﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.View.Context;
using Naracea.View;
using Autofac;
using Naracea.View.Components;
using Naracea.View.Compounds;
using Tests.Fakes;

namespace Naracea.Controller.Tests.CommonContexes {
	public class ViewContext {
		public IViewContext StubbedViewContext { get; private set; }

		public ViewContext Register(ContainerBuilder builder) {
			builder.Register<IViewContext>(c => MockRepository.GenerateStub<IViewContext>()).SingleInstance();
			builder.Register<IApplicationView>(c => MockRepository.GenerateStub<IApplicationView>()).SingleInstance();
			builder.Register<IConfirmCloseDocumentDialog>(c => MockRepository.GenerateStub<IConfirmCloseDocumentDialog>()).SingleInstance();
			builder.Register<IConfirmBranchDeletionDialog>(c => MockRepository.GenerateStub<IConfirmBranchDeletionDialog>()).SingleInstance();
			builder.Register<IErrorOccuredDialog>(c => MockRepository.GenerateStub<IErrorOccuredDialog>()).SingleInstance();
			builder.Register<IOpenDocumentDialog>(c => MockRepository.GenerateStub<IOpenDocumentDialog>()).SingleInstance();
			builder.Register<ISaveDocumentDialog>(c => MockRepository.GenerateStub<ISaveDocumentDialog>()).SingleInstance();
			builder.Register<IFileOperationsAreLockedDialog>(c => MockRepository.GenerateStub<IFileOperationsAreLockedDialog>()).SingleInstance();
			builder.Register<IBranchView>(c => MockRepository.GenerateStub<IBranchView>());
			builder.Register<IDocumentView>(c => MockRepository.GenerateStub<IDocumentView>()).SingleInstance();
			builder.Register<IApplicationView>(c => MockRepository.GenerateStub<IApplicationView>()).SingleInstance();
			builder.Register<ITextEditorView>(c => MockRepository.GenerateStub<ITextEditorView>()).SingleInstance();
			builder.Register<ITimelineView>(c => MockRepository.GenerateStub<ITimelineView>()).SingleInstance();
			builder.Register<IChangeStreamView>(c => MockRepository.GenerateStub<IChangeStreamView>()).SingleInstance();
			builder.Register<IFindReplaceTextView>(c => MockRepository.GenerateStub<IFindReplaceTextView>()).SingleInstance();
			builder.Register<IProgressIndicator>(c => MockRepository.GenerateStub<IProgressIndicator>()).SingleInstance();
			builder.Register<IUpdateAvailableIndicator>(c => MockRepository.GenerateStub<IUpdateAvailableIndicator>()).SingleInstance();
			builder.Register<IHasIndicators>(c => MockRepository.GenerateStub<IHasIndicators>()).SingleInstance();
			builder.Register<ISearchMatchItem>(c => new FakeSearchMatchItem());

			builder.Register<ICurrentColumnIndicator>(c => MockRepository.GenerateStub<ICurrentColumnIndicator>()).SingleInstance();
			builder.Register<ICurrentLineIndicator>(c => MockRepository.GenerateStub<ICurrentLineIndicator>()).SingleInstance();
			builder.Register<IWritingModeIndicator>(c => MockRepository.GenerateStub<IWritingModeIndicator>()).SingleInstance();
			builder.Register<ICompositeIndicator>(c => MockRepository.GenerateStub<ICompositeIndicator>()).SingleInstance();
			builder.Register<IEditorIndicators>(c => MockRepository.GenerateStub<IEditorIndicators>()).SingleInstance();

			return this;
		}

		public IViewContext Stub(IContainer container) {
			this.StubbedViewContext = container.Resolve<IViewContext>();
			this.StubbedViewContext.Stub(a => a.ApplicationView).Return(container.Resolve<IApplicationView>());
			this.StubbedViewContext.ApplicationView.Stub(a => a.DocumentStatusBar).Return(container.Resolve<IHasIndicators>());
			this.StubbedViewContext.Stub(a => a.FindReplaceTextView).Return(container.Resolve<IFindReplaceTextView>());
			this.StubbedViewContext.FindReplaceTextView.Stub(a => a.Exec(Arg<Action>.Is.Anything)).Repeat.Any().WhenCalled(x => ( (Action)x.Arguments[0] )());

			this.StubbedViewContext.Stub(a => a.CreateView<IBranchView>()).Return(container.Resolve<IBranchView>()).Repeat.Any();
			this.StubbedViewContext.Stub(a => a.CreateView<IDocumentView>()).Return(container.Resolve<IDocumentView>());
			this.StubbedViewContext.Stub(a => a.CreateView<IApplicationView>()).Return(container.Resolve<IApplicationView>());
			this.StubbedViewContext.Stub(a => a.CreateView<ITextEditorView>()).Return(container.Resolve<ITextEditorView>());
			this.StubbedViewContext.Stub(a => a.CreateView<ITimelineView>()).Return(container.Resolve<ITimelineView>());
			this.StubbedViewContext.Stub(a => a.CreateView<IChangeStreamView>()).Return(container.Resolve<IChangeStreamView>());
			this.StubbedViewContext.Stub(a => a.CreateView<IOpenDocumentDialog>()).Return(container.Resolve<IOpenDocumentDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<ISaveDocumentDialog>()).Return(container.Resolve<ISaveDocumentDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<IConfirmCloseDocumentDialog>()).Return(container.Resolve<IConfirmCloseDocumentDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<IConfirmBranchDeletionDialog>()).Return(container.Resolve<IConfirmBranchDeletionDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<IErrorOccuredDialog>()).Return(container.Resolve<IErrorOccuredDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<IFileOperationsAreLockedDialog>()).Return(container.Resolve<IFileOperationsAreLockedDialog>());
			this.StubbedViewContext.Stub(a => a.CreateView<IProgressIndicator>()).Return(container.Resolve<IProgressIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<IUpdateAvailableIndicator>()).Return(container.Resolve<IUpdateAvailableIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<ICurrentColumnIndicator>()).Return(container.Resolve<ICurrentColumnIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<ICurrentLineIndicator>()).Return(container.Resolve<ICurrentLineIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<IWritingModeIndicator>()).Return(container.Resolve<IWritingModeIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<ICompositeIndicator>()).Return(container.Resolve<ICompositeIndicator>());
			this.StubbedViewContext.Stub(a => a.CreateView<IEditorIndicators>()).Return(container.Resolve<IEditorIndicators>());
			this.StubbedViewContext.Stub(a => a.CreateView<IFindReplaceTextView>()).Return(container.Resolve<IFindReplaceTextView>());

			this.StubbedViewContext.Stub(a => a.CreateSearchMatchItem()).Return(container.Resolve<ISearchMatchItem>());

			return this.StubbedViewContext;
		}
	}
}
