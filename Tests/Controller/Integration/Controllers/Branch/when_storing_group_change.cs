﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	[TestFixture]
	public class when_storing_group_change : with_branch_with_fake_text_editor_view {
		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_branchController.StoreGroupBegin();
			_branchController.Store(new FakeChange());
			_branchController.StoreGroupEnd();
		}

		[Test]
		public void it_should_add_the_group_begin_to_the_branch() {
			_branchController.Branch.Changes.At(0).ShouldBeType(typeof(GroupBegin));
		}

		[Test]
		public void it_should_add_the_grouped_changes_to_the_branch() {
			_branchController.Branch.Changes.At(1).ShouldBeType(typeof(FakeChange));
		}

		[Test]
		public void it_should_add_the_group_end_to_the_branch() {
			_branchController.Branch.Changes.At(2).ShouldBeType(typeof(GroupEnd));
		}

		[Test]
		public void it_should_setup_group_end_properly() {
			var groupEnd = _branchController.Branch.Changes.At(2) as GroupEnd;
			groupEnd.Changes.ShouldNotBeEmpty();
			groupEnd.Changes.Count().ShouldEqualTo(1);
			groupEnd.Changes.ElementAt(0).ShouldBeSameAs(_branchController.Branch.Changes.At(1));
		}
	}
}
