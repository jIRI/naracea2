﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;
using Tests.Fakes;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	public abstract class with_find_expectations : with_branch_with_fake_text_editor_view {
		protected IFindReplaceTextView _findTextView;
		protected IList<ISearchMatchItem> _addedMatches = new List<ISearchMatchItem>();
		protected int _expectedMatchesCreated;
		protected int _matchesCreated = 0;

		protected override void SetUp() {
			base.SetUp();
			_controller.ActiveDocument = _documentController;
			_documentController.ActiveBranch = _branchController;
			_findTextView = _container.Resolve<IFindReplaceTextView>();
			_controller.FindTextView.Stub(a => a.EndBusy()).WhenCalled(invocation => _sync.Set());
			_controller.Context.View.Stub(a => a.CreateSearchMatchItem()).Return(new FakeSearchMatchItem()).WhenCalled(x => _matchesCreated++).Repeat.Any();
		}

		[Test]
		public void it_should_find_proper_matches() {
			//lame, of course, but doing it the right way is way to complicated with rhino mocks
			//background: it the find match needs to be added though view's exec method, but with rhinomocks only the first object passed is used
			_matchesCreated.ShouldEqualTo(_expectedMatchesCreated);
		}
	}
}
