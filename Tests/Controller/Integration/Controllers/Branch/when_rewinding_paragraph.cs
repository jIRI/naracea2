﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	[TestFixture]
	public class when_rewinding_paragraph : with_branch_with_fake_text_editor_view {
		List<string> _paragraphs = new List<string> {
			"Guns. Lots of guns.",
			"As you wish, Neo!"
		};

		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText(_paragraphs);
			_textEditorView.Text = string.Join(Naracea.Common.Constants.NewLine, _paragraphs) + Naracea.Common.Constants.NewLine;
			_disposables.Add(_branchController.Bus.Subscribe<Naracea.Controller.Messages.BranchShiftingFinished>(m => _sync.Set()));
		}

		protected override void BecauseOf() {
			_branchController.Bus.Publish(new View.Requests.RewindParagraph(_branchController.View));
			_sync.WaitOne(15000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_remove_last_paragraph_from_the_text_editor() {
			_textEditorView.Text.ShouldEqualTo(_paragraphs[0]);			
		}
	}
}
