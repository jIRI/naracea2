﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	[TestFixture]
	public class when_rewinding_all : with_branch_with_fake_text_editor_view {
		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("abc");
			_textEditorView.Text = "abc";
			_disposables.Add(_branchController.Bus.Subscribe<Naracea.Controller.Messages.BranchShiftingFinished>(m => _sync.Set()));
		}

		protected override void BecauseOf() {
			_branchController.Bus.Publish(new View.Requests.RewindAll(_branchController.View));
			_sync.WaitOne(15000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_remove_all_edits_from_the_text_editor() {
			_textEditorView.Text.ShouldEqualTo("");			
		}
	}
}
