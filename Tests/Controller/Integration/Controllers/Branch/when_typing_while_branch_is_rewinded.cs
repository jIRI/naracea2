﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	[TestFixture]
	public class when_typing_while_branch_is_rewinded : with_branch_with_fake_text_editor_view {
		int _expectedInsertedTextPosition = 3;
		string _expectedInsertedText = "*";
		int _expectedMultiUndoPosition;
		int _expectedMultiUndoCount;

		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("Lorem ipsum dolor sit amet.");
			_textEditorView.Text = "Lorem ipsum dolor sit amet.";
			_disposables.Add(_branchController.Bus.Subscribe<Naracea.Controller.Messages.BranchShiftingFinished>(m => _sync.Set()));
			//rewind one word
			_branchController.Bus.Publish(new View.Requests.RewindWord(_branchController.View));
			_sync.WaitOne(15000, false).ShouldBeTrue();
			_expectedMultiUndoPosition = _branchController.Branch.CurrentChangeIndex + 1;
			_expectedMultiUndoCount = _branchController.Branch.Changes.Count - _expectedMultiUndoPosition;
		}

		protected override void BecauseOf() {
			_textEditorView.EditInsertText(_expectedInsertedTextPosition, _expectedInsertedText);
		}

		[Test]
		public void it_should_store_the_text_insertion() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			change.ShouldBeType(typeof(InsertText));
		}

		[Test]
		public void it_should_store_the_text_insertion_with_expected_text() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			var insertText = change as InsertText;
			insertText.Text.ShouldBeEqualTo(_expectedInsertedText);
		}

		[Test]
		public void it_should_store_the_text_insertion_with_expected_position() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			var insertText = change as InsertText;
			insertText.Position.ShouldEqualTo(_expectedInsertedTextPosition);
		}

		[Test]
		public void it_should_store_the_multiundo_undoing_the_rewinded_word() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 2);
			change.ShouldBeType(typeof(MultiUndo));
		}

		[Test]
		public void it_should_store_the_multiundo_undoing_the_rewinded_word_with_rewinded_changes() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 2);
			var multiUndo = change as MultiUndo;
			int positon = _expectedMultiUndoPosition;
			int count = 0;
			foreach( var c in multiUndo.Changes ) {
				_branchController.Branch.Changes.At(positon).ShouldBeSameAs(c);
				positon++;
				count++;
			}
			count.ShouldEqualTo(_expectedMultiUndoCount);
		}
	}
}
