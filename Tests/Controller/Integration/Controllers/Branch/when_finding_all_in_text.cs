﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;
using Tests.Fakes;

namespace Naracea.Controller.Tests.Integration.Controllers.Branch {
	[TestFixture]
	public class when_finding_all_in_text : with_find_expectations {
		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("0123056709");
			_expectedMatchesCreated = 3;
		}

		protected override void BecauseOf() {
			_branchController.Bus.Publish(
				new View.Requests.FindAll(
					new View.ViewFindOptions {
						MatchCase = false,
						MatchWholeWords = false,
						FindPattern = "0"
					}
			));
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}
	}
}
