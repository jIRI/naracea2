﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_undoing_word : with_branch_with_fake_text_editor_view {
		int _expectedMultiUndoPosition = 5;
		int _expectedMultiUndoCount = 5;

		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("12345 6789");
			_textEditorView.Text = "12345 6789";
		}

		protected override void BecauseOf() {
			_textEditorView.EditUndoWord();
		}

		[Test]
		public void it_should_store_the_multiundo_for_undo_word() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			change.ShouldBeType(typeof(MultiUndo));
		}

		[Test]
		public void it_should_store_the_multiundo_for_undo_word_with_expected_changes() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			this.VerifyMultiUndoChanges(change as MultiUndo, _expectedMultiUndoPosition, _expectedMultiUndoCount);
		}	
	}
}
