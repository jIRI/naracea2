﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_pasting_chars_as_changes_without_selection : with_branch_with_fake_text_editor_view {
		string _expectedText = "Lorem ipsum dolor sit amet.";
		protected override void SetUp() {
			base.SetUp();
			_textEditorView.Text = string.Empty;
		}

		protected override void BecauseOf() {
			_textEditorView.EditPasteCharsAsChanges(0, _expectedText);
		}

		[Test]
		public void it_should_store_proper_number_of_changes() {
			_branchController.Branch.Changes.Count.ShouldEqualTo(_expectedText.Length);
		}

		[Test]
		public void it_should_store_all_chars_as_inserts() {
			int changeIndex = 0;
			foreach( var ch in _expectedText ) {
				var change = _branchController.Branch.Changes.At(changeIndex) as InsertText;
				//ok, here we have more than one assert, but it tests the same thing
				change.ShouldNotBeNull();
				change.Position.ShouldEqualTo(changeIndex);
				change.Text.ShouldBeEqualTo(ch.ToString());
				changeIndex++;
			}
		}

		[Test]
		public void it_should_change_editors_text_accordingly() {
			_textEditorView.Text.ShouldEqualTo(_expectedText);
		}

	}
}
