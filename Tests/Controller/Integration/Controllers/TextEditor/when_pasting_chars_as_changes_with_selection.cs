﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_pasting_chars_as_changes_with_selection : with_branch_with_fake_text_editor_view {
		string _expectedText = "0abcd9";
		string _textToPaste = "abcd";
		int _changeCountBeforePaste;

		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("0123456789");
			_textEditorView.Select(1, 8);
			_changeCountBeforePaste = _branchController.Branch.Changes.Count;
		}

		protected override void BecauseOf() {
			_textEditorView.EditPasteCharsAsChanges(1, _textToPaste);
		}

		[Test]
		public void it_should_store_proper_number_of_changes() {
			_branchController.Branch.Changes.Count.ShouldEqualTo(_changeCountBeforePaste + _textToPaste.Length);
		}

		[Test]
		public void it_should_store_all_chars_as_inserts() {
			int changeIndex = _changeCountBeforePaste;
			for( int inTextPosition = 1; inTextPosition < _textToPaste.Length; inTextPosition++ ) {
				var ch = _expectedText[inTextPosition];
				var change = _branchController.Branch.Changes.At(changeIndex) as InsertText;
				//ok, here we have more than one assert, but it tests the same thing
				change.ShouldNotBeNull();
				change.Position.ShouldEqualTo(inTextPosition);
				change.Text.ShouldBeEqualTo(ch.ToString());
				changeIndex++;
			}
		}

		[Test]
		public void it_should_change_editors_text_accordingly() {
			_textEditorView.Text.ShouldEqualTo(_expectedText);
		}

	}
}
