﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_adding_inserts : with_branch_with_fake_text_editor_view {
		List<Tuple<int, string>> _edits = new List<Tuple<int, string>> {
			new Tuple<int, string>(0, "a"),
			new Tuple<int, string>(1, "b"),
			new Tuple<int, string>(2, "c"),
			new Tuple<int, string>(3, "x"),
			new Tuple<int, string>(4, "y"),
			new Tuple<int, string>(3, "123"),
			new Tuple<int, string>(8, "z"),
		};

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			foreach( var edit in _edits ) {
				_textEditorView.EditInsertText(edit.Item1, edit.Item2);
			}
		}

		[Test]
		public void it_should_store_proper_number_of_changes() {
			_branchController.Branch.Changes.Count.ShouldEqualTo(_edits.Count);
		}

		[Test]
		public void it_should_keep_editor_editable() {
			_textEditorView.CanEdit.ShouldBeTrue();
		}

		[Test]
		public void it_should_allow_undo() {
			_textEditorView.CanUndo.ShouldBeTrue();
		}

		[Test]
		public void it_should_forbid_redo() {
			_textEditorView.CanRedo.ShouldBeFalse();
		}

		[Test]
		public void it_should_store_all_inserts() {
			int changeIndex = 0;
			foreach( var edit in _edits ) {
				var change = _branchController.Branch.Changes.At(changeIndex++) as InsertText;
				//ok, here we have more than one assert, but it tests the same thing
				change.ShouldNotBeNull();
				change.Position.ShouldEqualTo(edit.Item1);
				change.Text.ShouldBeEqualTo(edit.Item2);
			}
		}
	}
}
