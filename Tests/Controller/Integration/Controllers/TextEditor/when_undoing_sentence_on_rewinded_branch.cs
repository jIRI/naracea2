﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_undoing_sentence_on_rewinded_branch: with_branch_with_fake_text_editor_view {
		int _expectedMultiUndoPosition = 0;
		int _expectedMultiUndoCount = 11;
		int _expectedCoercingMultiUndoPosition = 11;
		int _expectedCoercingMultiUndoCount = 11;

		protected override void SetUp() {
			base.SetUp();
			_textEditorView.EditInsertText("12345 6789. abcd efgh.");
			_textEditorView.Text = "12345 6789. abcd efgh.";
			//rewind one word
			_branchController.Bus.Publish(new View.Requests.RewindSentence(_branchController.View));
			_sync.WaitOne(5000, false);
		}

		protected override void BecauseOf() {
			_textEditorView.EditUndoSentence();
		}

		[Test]
		public void it_should_store_the_multiundo_for_undo_sentence() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			change.ShouldBeType(typeof(MultiUndo));
		}

		[Test]
		public void it_should_store_the_multiundo_for_undo_sentence_with_expected_changes() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 1);
			this.VerifyMultiUndoChanges(change as MultiUndo, _expectedMultiUndoPosition, _expectedMultiUndoCount);
		}

		[Test]
		public void it_should_store_the_multiundo_undoing_the_rewinded_sentence() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 2);
			change.ShouldBeType(typeof(MultiUndo));
		}

		[Test]
		public void it_should_store_the_multiundo_undoing_the_rewinded_sentence_with_rewinded_changes() {
			var change = _branchController.Branch.Changes.At(_branchController.Branch.Changes.Count - 2);
			this.VerifyMultiUndoChanges(change as MultiUndo, _expectedCoercingMultiUndoPosition, _expectedCoercingMultiUndoCount);
		}
	}
}
