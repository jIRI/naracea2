﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_adding_insert_delete_and_backspace : with_branch_with_fake_text_editor_view {
		List<Tuple<int, string>> _edits = new List<Tuple<int, string>> {
			new Tuple<int, string>(0, "abcd"),
			new Tuple<int, string>(1, "b"),
			new Tuple<int, string>(2, "d"),
		};

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_textEditorView.EditInsertText(_edits[0].Item1, _edits[0].Item2);
			_textEditorView.EditDeleteText(_edits[1].Item1, _edits[1].Item2);
			_textEditorView.EditBackspaceText(_edits[2].Item1, _edits[2].Item2);
		}

		[Test]
		public void it_should_store_proper_number_of_changes() {
			_branchController.Branch.Changes.Count.ShouldEqualTo(_edits.Count);
		}

		[Test]
		public void it_should_store_insert_at_position_0() {
			var change = _branchController.Branch.Changes.At(0) as InsertText;
			change.ShouldNotBeNull();
			change.Position.ShouldEqualTo(_edits[0].Item1);
			change.Text.ShouldBeEqualTo(_edits[0].Item2);
		}

		[Test]
		public void it_should_store_delete_at_position_1() {
			var change = _branchController.Branch.Changes.At(1) as DeleteText;
			change.ShouldNotBeNull();
			change.Position.ShouldEqualTo(_edits[1].Item1);
			change.Text.ShouldBeEqualTo(_edits[1].Item2);
		}

		[Test]
		public void it_should_store_backspace_at_position_2() {
			var change = _branchController.Branch.Changes.At(2) as BackspaceText;
			change.ShouldNotBeNull();
			change.Position.ShouldEqualTo(_edits[2].Item1);
			change.Text.ShouldBeEqualTo(_edits[2].Item2);
		}

		[Test]
		public void it_should_keep_editor_editable() {
			_textEditorView.CanEdit.ShouldBeTrue();
		}

		[Test]
		public void it_should_allow_undo_on_branch_view() {
			_textEditorView.CanUndo.ShouldBeTrue();
		}

		[Test]
		public void it_should_forbid_redo_on_branch_view() {
			_textEditorView.CanRedo.ShouldBeFalse();
		}
	}
}
