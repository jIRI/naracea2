﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Integration.Controllers.TextEditor {
	[TestFixture]
	public class when_adding_inserts_and_then_undoing_everything : with_branch_with_fake_text_editor_view {
		List<Tuple<int, string>> _inserts = new List<Tuple<int, string>> {
			new Tuple<int, string>(0, "a"),
			new Tuple<int, string>(1, "123"),
			new Tuple<int, string>(4, "b"),
		};
		List<int> _undoIndices = new List<int> {
			2, 1, 0
		};


		protected override void SetUp() {
			base.SetUp();
			_textEditorView.Text = "";
		}

		protected override void BecauseOf() {
			foreach( var edit in _inserts ) {
				_textEditorView.EditInsertText(edit.Item1, edit.Item2);
			}
			_textEditorView.EditUndo();
			_textEditorView.EditUndo();
			_textEditorView.EditUndo();
			//this undo is ignored
			_textEditorView.EditUndo();
		}

		[Test]
		public void it_should_store_proper_number_of_changes() {
			_branchController.Branch.Changes.Count.ShouldEqualTo(_inserts.Count + _undoIndices.Count);
		}

		[Test]
		public void it_should_store_appropriate_number_of_undos() {
			int changeIndex = _inserts.Count;
			foreach( var expectedIndex in _undoIndices ) {
				var change = _branchController.Branch.Changes.At(changeIndex++) as Undo;
				change.ShouldNotBeNull();
				change.Change.ShouldBeSameAs(_branchController.Branch.Changes.At(expectedIndex));
			}
		}

		[Test]
		public void it_should_change_the_editor_view_text_so_it_is_empty() {
			_textEditorView.Text.ShouldBeEqualTo(string.Empty);
		}

		[Test]
		public void it_should_keep_editor_editable() {
			_textEditorView.CanEdit.ShouldBeTrue();
		}

		[Test]
		public void it_should_forbid_undo() {
			_textEditorView.CanUndo.ShouldBeFalse();
		}

		[Test]
		public void it_should_allow_redo() {
			_textEditorView.CanRedo.ShouldBeTrue();
		}


	}
}
