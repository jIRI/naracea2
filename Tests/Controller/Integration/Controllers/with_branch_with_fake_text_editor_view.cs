﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;
using MemBus;

namespace Naracea.Controller.Tests.Integration.Controllers {
	public abstract class with_branch_with_fake_text_editor_view : with_controller_setup {
		protected IDocumentController _documentController;
		protected IBranchController _branchController;
		protected FakeTextEditorView _textEditorView;
		protected AutoResetEvent _sync = new AutoResetEvent(false);

		protected override void ApplyCustomConfiguration() {
			_builder.Register<ITextEditorView>(c => {
				var fakeView = new FakeTextEditorView(_container.Resolve<IBus>());
				_textEditorView = fakeView;
				return fakeView;
			});
		}

		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_documentController = m.DocumentController;
			}));
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _sync.Set()) ) {
				//create document
				_controller.Bus.Publish(new Requests.CreateDocument());
				_sync.WaitOne(60000, false).ShouldBeTrue();
			}
			//WARNING: here we cannot create more than one branch, because view context is stubbed, and ctor is called exactly once
			_branchController = _documentController.Branches.First();
			_branchController.EditModeThreshold = 1000;
			_textEditorView.Controller = _branchController.Editor;
		}

		protected void VerifyMultiUndoChanges(IChangeWithChangeCollection multiUndo, int multiUndoPositon, int multiUndoCount) {
			int count = 0;
			int position = multiUndoPositon;
			foreach( var c in multiUndo.Changes ) {
				_branchController.Branch.Changes.At(position).ShouldBeSameAs(c);
				position++;
				count++;
			}
			count.ShouldEqualTo(multiUndoCount);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
