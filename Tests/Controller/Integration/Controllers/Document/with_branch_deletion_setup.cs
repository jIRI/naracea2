﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Document {
	public abstract class with_branch_deletion_setup : with_controller_setup {
		protected IDocumentController _documentController;
		protected IBranchController _branchController;
		protected AutoResetEvent _sync = new AutoResetEvent(false);
		protected bool _confirmed = false;
		protected bool _cancelled = false;

		protected override void SetUp() {
			base.SetUp();
			//create document
			_subscriptions.Add(
				_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
					_documentController = m.DocumentController;
				}));
			//create branch (we have one already)
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(
				m => {
					_branchController = m.BranchController;
					_sync.Set();
				}
			) ) {
				_controller.Bus.Publish(new Requests.CreateDocument());
				// wait for default branch creation
				_sync.WaitOne(5000, false).ShouldBeTrue();
				// create additional branch
				_branchController = _documentController.CreateBranch();
				// wait for it
				_sync.WaitOne(5000, false).ShouldBeTrue();
			}

			_subscriptions.Add(_controller.Bus.Subscribe<Messages.BranchDeleted>(m => {
				_confirmed = true;
				_sync.Set();
			}));
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.BranchDeleteCancelled>(m => {
				_cancelled = true;
				_sync.Set();
			}));
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
