﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;

namespace Naracea.Controller.Tests.Integration.Controllers.Document {
	[TestFixture]
	public class when_creating_new_branch : with_controller_setup {
		IDocumentController _documentController;
		IBranchController _branchController;
		AutoResetEvent _sync = new AutoResetEvent(false);

		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(
				_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_documentController = m.DocumentController;
				_sync.Set();
			}));
			_controller.Bus.Publish(new Requests.CreateDocument());
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}

		protected override void BecauseOf() {
			_branchController = _documentController.CreateBranch();
		}
		
		[Test]
		public void it_should_create_branch() {
			_branchController.ShouldNotBeNull();
		}

		[Test]
		public void it_should_register_branch_with_document() {
			_documentController.Branches.ShouldContain(_branchController);
		}

		[Test]
		public void it_should_activate_the_new_branch() {
			_documentController.ActiveBranch.ShouldBeSameAs(_branchController);
		}

		[Test]
		public void it_should_mark_branch_view_as_deletable() {
			_documentController.ActiveBranch.View.CanBeDeleted.ShouldBeTrue();
		}
	}
}
