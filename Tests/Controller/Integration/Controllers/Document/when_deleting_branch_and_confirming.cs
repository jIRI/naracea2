﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.View.Events;

namespace Naracea.Controller.Tests.Integration.Controllers.Document {
	[TestFixture]
	public class when_deleting_branch_and_confirming : with_branch_deletion_setup {
		protected override void SetUp() {
			base.SetUp();
			//prepare confirmation dialog
			var confirmDialog = _container.Resolve<IConfirmBranchDeletionDialog>();
			confirmDialog.Stub(a => a.ShowDialog())
				.WhenCalled(a => confirmDialog.Raise(
					x => x.OkSelected += null,
					_controller,
					new DialogResultEventArgs()
			));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new View.Requests.DeleteBranch(_documentController.View, _branchController.View));
			//we confirmed removal, wait for message confirming it
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_request_confirmation() {
			_container.Resolve<IConfirmBranchDeletionDialog>().AssertWasCalled(a => a.ShowDialog());
		}

		[Test]
		public void it_should_post_delete_message() {
			_confirmed.ShouldBeTrue();
		}

		[Test]
		public void it_should_remove_the_branch_from_branch_list() {
			_documentController.Branches.Contains(_branchController).ShouldBeFalse();
		}
	}
}
