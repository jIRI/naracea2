﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using Autofac;
using System.Windows;
using Naracea.Core;
using Naracea.View.Context;
using Naracea.Core.Builders;
using Naracea.View;
using System.Threading.Tasks;
using System.Threading;
using Naracea.Core.Spellcheck;

namespace Naracea.Controller.Tests.Integration.Controllers {
	public abstract class with_controller_setup : Context {
		protected List<string> _fileCleanup = new List<string>();
		protected IController _controller;
		protected IControllerContext _controllerContext;
		protected IViewContext _viewContext;
		protected List<IDisposable> _subscriptions = new List<IDisposable>();

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var coreConfigurator = new Naracea.Core.Configuration.Configurator();
			var controllerConfigurator = new Naracea.Controller.Configuration.Configurator();
			_builder.RegisterInstance(controllerConfigurator.Bus);

			this.ApplyCustomConfiguration();

			_container = _builder.Build();

			viewContextBuilder.Stub(_container);
			_viewContext = viewContextBuilder.StubbedViewContext;
			var started = new AutoResetEvent(false);
			//controller
			var app = FakeApp.Instance();
			_controllerContext = controllerConfigurator.Container.Resolve<IControllerContext>(
				new TypedParameter(typeof(Application), app),
				new TypedParameter(typeof(ICoreFactories), coreConfigurator.Container.Resolve<ICoreFactories>()),
				new TypedParameter(typeof(IControllerFactories), controllerConfigurator.Container.Resolve<IControllerFactories>()),
				new TypedParameter(typeof(IDateTimeProvider), coreConfigurator.Container.Resolve<IDateTimeProvider>()),
				new TypedParameter(typeof(IViewContext), _viewContext),
				new TypedParameter(typeof(IDictionaryStore), coreConfigurator.Container.Resolve<IDictionaryStore>())
			);
			//app controller
			_controller = controllerConfigurator.Container.Resolve<IController>(
				new TypedParameter(typeof(IControllerContext), _controllerContext)
			);
		}

		protected virtual void ApplyCustomConfiguration() {
		}

		protected override void CustomTearDown() {
			foreach( var subscription in _subscriptions ) {
				subscription.Dispose();
			}

			var docs = new List<IDocumentController>(_controller.Documents);
			foreach( var doc in docs ) {
				doc.Close();
			}
			_controller.Close();
			base.CustomTearDown();
			foreach( var file in _fileCleanup ) {
				System.IO.File.Delete(file);
			}
		}
	}
}
