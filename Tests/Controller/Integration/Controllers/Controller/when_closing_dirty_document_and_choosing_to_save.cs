﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Naracea.Core.Repository;
using Autofac;
using System.Diagnostics;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_closing_dirty_document_and_choosing_to_save : with_close_testing_setup {
		static readonly string _fileName = "file.ncd";

		protected override void SetUp() {
			base.SetUp();
			//setup dialog for save
			var dialog = _container.Resolve<IConfirmCloseDocumentDialog>();
			dialog.Stub(a => a.ShowDialog()).WhenCalled(a =>
				dialog.Raise(
					x => x.YesSelected += null,
					_controller,
					new DialogResultEventArgs()
			));
			//create branch to make it dirty...
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _branchCreationSync.Set()) ) {
				_docController.CreateBranch();
				_branchCreationSync.WaitOne(10000, false).ShouldBeTrue();
			}
			//setup repo so we are not asked about filename
			var repoContext = _controllerContext.Core.CreateRepositoryContext();
			repoContext.Path = _fileName;
			_fileCleanup.Add(_fileName);
			_docController.ChangeRepository(repoContext);
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.CloseDocument(_docController));
			//long timeout needed here, because saving takes quite a lot of time
			_documentClosedSync.WaitOne(100000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_close_event() {
			_closedEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_close_the_documents() {
			_controller.Documents.Any(d => d == _docController).ShouldBeFalse();
		}
	}
}
