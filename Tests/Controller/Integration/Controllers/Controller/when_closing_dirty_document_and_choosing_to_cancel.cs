﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_closing_dirty_document_and_choosing_to_cancel : with_close_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _branchCreationSync.Set()) ) {
				_docController.CreateBranch();
				_branchCreationSync.WaitOne(10000, false).ShouldBeTrue();
			}
			var dialog = _container.Resolve<IConfirmCloseDocumentDialog>();
			dialog.Stub(a => a.ShowDialog()).WhenCalled(a =>
				dialog.Raise(
					x => x.CancelSelected += null,
					_controller,
					new DialogResultEventArgs()
			));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.CloseDocument(_docController));
			_documentCloseCancelledSync.WaitOne(7000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_close_cancelled_event() {
			_closeCancelEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_not_close_any_documents() {
			_controller.Documents.Any(d => d == _docController).ShouldBeTrue();
		}
	}
}
