﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_stopping_controller_with_dirty_documents_and_choosing_not_to_save : with_stop_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			var dialog = _container.Resolve<IConfirmCloseDocumentDialog>();
			dialog.Stub(a => a.ShowDialog()).WhenCalled(a =>
				dialog.Raise(
					x => x.NoSelected += null,
					_controller,
					new DialogResultEventArgs()
			));
			this.CreateControllers();
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _sync.Set()) ) {
				_docController.CreateBranch();
				_sync.WaitOne(10000, false).ShouldBeTrue();
			}			
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.Stop());
			_sync.WaitOne(7000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_stop_event() {
			_stopEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_close_all_documents() {
			_controller.Documents.Count().ShouldEqualTo(0);
		}
	}
}
