﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.Controller;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_starting_controller_without_params : with_controller_setup {
		protected AutoResetEvent _sync = new AutoResetEvent(false);
		protected IDocumentController _docController;
		
		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _sync.Set()));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Naracea.Controller.Requests.Start(null));
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_create_new_document() {
			_controller.Documents.ShouldNotBeEmpty();
		}
	}
}
