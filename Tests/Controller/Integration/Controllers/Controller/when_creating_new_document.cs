﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.Controller;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_creating_new_document : with_expectations_on_new_document_creation {
		protected override void SetUp() {
			base.SetUp();
			_expectedDocument = new IntegrationData().CreateDefaultDocument();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(
				m => {
				_docController = m.DocumentController;
				_sync.Set();
			}));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new  Requests.CreateDocument());
			_eventReceived = _sync.WaitOne(5000, false);
		}
	}
}
