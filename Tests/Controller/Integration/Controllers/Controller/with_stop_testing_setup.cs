﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {	
	public abstract class with_stop_testing_setup : with_controller_setup {
		protected AutoResetEvent _sync = new AutoResetEvent(false);
		protected IDocumentController _docController;
		protected bool _stopEventReceived;
		protected bool _stopCancelledEventReceived;

		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_docController = m.DocumentController;
			}));
			//setup to receive stopped event
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.Stopped>(m => {
				_stopEventReceived = true;
				_sync.Set();
			}));
			//setup to receive stop cancelled event
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.StopCancelled>(m => {
				_stopCancelledEventReceived = true;
				_sync.Set();
			}));
		}

		protected void CreateControllers() {
			//create document
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _sync.Set()) ) {
				_controller.Bus.Publish(new Requests.CreateDocument());
				_sync.WaitOne(60000, false).ShouldBeTrue();
			}
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
