﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	public abstract class with_close_testing_setup : with_controller_setup {
		protected AutoResetEvent _documentClosedSync = new AutoResetEvent(false);
		protected AutoResetEvent _documentCloseCancelledSync = new AutoResetEvent(false);
		protected AutoResetEvent _branchCreationSync = new AutoResetEvent(false);
		protected IDocumentController _docController;
		protected ISaveDocumentDialog _saveDialog;
		protected bool _closedEventReceived;
		protected bool _closeCancelEventReceived;

		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_docController = m.DocumentController;
			}));
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _branchCreationSync.Set()) ) {
				//create document
				_controller.Bus.Publish(new Requests.CreateDocument());
				_branchCreationSync.WaitOne(60000, false).ShouldBeTrue();
			}
			_saveDialog = _container.Resolve<ISaveDocumentDialog>();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentClosed>(m => {
				_closedEventReceived = true;
				_documentClosedSync.Set();
			}));
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentCloseCancelled>(m => {
				_closeCancelEventReceived = true;
				_documentCloseCancelledSync.Set();
			}));
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			//_docController.Close();
			try {
				_controllerContext.Application.Dispatcher.Invoke(new Action(() => _controllerContext.Application.Shutdown()));
			} catch(System.Threading.Tasks.TaskCanceledException) {
				// do nothing just die. this occurs because task wich runs shutdown gets cancelled.
			}
		}
	}
}
