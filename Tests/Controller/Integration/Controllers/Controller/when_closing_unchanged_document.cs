﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_closing_unchanged_document : with_close_testing_setup {
		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.CloseDocument(_docController));
			_documentClosedSync.WaitOne(7000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_closed_event() {
			_closedEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_close_the_document() {
			_controller.Documents.Any(d => d == _docController).ShouldBeFalse();
		}
	}
}
