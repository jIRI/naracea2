﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_stopping_controller_without_dirty_documents : with_stop_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			this.CreateControllers();
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.Stop());
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_stop_event() {
			_stopEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_close_all_documents() {
			_controller.Documents.Count().ShouldEqualTo(0);
		}
	}
}
