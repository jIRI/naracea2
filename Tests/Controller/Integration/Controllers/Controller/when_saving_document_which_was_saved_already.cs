﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Naracea.Core.Repository;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_saving_document_which_was_saved_already : with_save_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			this.CreateControllers();
			var repoContext = _controllerContext.Core.CreateRepositoryContext();
			repoContext.Path = _fileName;
			_docController.ChangeRepository(repoContext);
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.SaveDocument(_docController));
			_sync.WaitOne(40000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_save_event() {
			_saveEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_save_the_document_to_the_file() {
			System.IO.File.Exists(_fileName).ShouldBeTrue();
		}
	}
}
