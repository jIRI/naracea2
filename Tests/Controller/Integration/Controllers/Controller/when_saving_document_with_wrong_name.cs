﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Naracea.Core.Repository;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_saving_document_with_wrong_name : with_save_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			_saveDialog.Stub(a => a.ShowDialog()).WhenCalled(a =>
				_saveDialog.Raise(
					x => x.FileSelected += null,
					_controller,
					new FileSelectedEventArgs("")
			));
			this.CreateControllers();
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.SaveDocument(_docController));
			_sync.WaitOne(20000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_save_failed_event() {
			_saveFailEventReceived.ShouldBeTrue();
		}
	}
}
