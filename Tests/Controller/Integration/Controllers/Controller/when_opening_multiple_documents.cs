﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using Naracea.View.Events;
using System.Threading;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_opening_multiple_documents : with_controller_setup {
		static readonly string _fileNameTemplate = "fileToBeOpened{0}.ncd";
		const int _fileCount = 4;
		List<IDocument> _expectedDocuments = new List<IDocument>();
		IEnumerable<string> _names;
		IList<IDocumentController> _docControllers = new List<IDocumentController>();
		AutoResetEvent _sync = new AutoResetEvent(false);

		protected override void SetUp() {
			base.SetUp();
			var names = from i in Enumerable.Range(0, _fileCount)
									select string.Format(_fileNameTemplate, i);
			_names = names.ToArray();
			var openDialog = _container.Resolve<IOpenDocumentDialog>();
			openDialog.Stub(a => a.ShowDialog())
				.WhenCalled(a => openDialog.Raise(
					x => x.FilesSelected += null,
					_controller,
					new FilesSelectedEventArgs(_names)
			));
			_fileCleanup.AddRange(_names);
			foreach(var name in _names) {
				_expectedDocuments.Add(new IntegrationData().CreateAndSaveSimpleDocument(name, _controllerContext.Core));
			}
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_docControllers.Add(m.DocumentController);
				_sync.Set();
			}));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.OpenDocumentOrDocuments());
			for( int i = 0; i < _fileCount; i++ ) {
				_sync.WaitOne(10000, false).ShouldBeTrue();
			}
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}

		[Test]
		public void it_should_return_proper_number_of_documents() {
			_docControllers.Count().ShouldEqualTo(_names.Count());
		}
	}
}
