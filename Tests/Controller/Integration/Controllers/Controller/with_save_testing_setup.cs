﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	public abstract class with_save_testing_setup : with_controller_setup {
		protected static readonly string _fileName = "file.ncd";
		protected AutoResetEvent _sync = new AutoResetEvent(false);
		protected IDocumentController _docController;
		protected ISaveDocumentDialog _saveDialog;
		protected bool _saveEventReceived;
		protected bool _saveCancelEventReceived;
		protected bool _saveFailEventReceived;

		protected override void SetUp() {
			base.SetUp();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_docController = m.DocumentController;
			}));
			_fileCleanup.Add(_fileName);
			_saveDialog = _container.Resolve<ISaveDocumentDialog>();
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentSaved>(m => {
				_saveEventReceived = true;
				_sync.Set();
			}));
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentSaveCancelled>(m => {
				_saveCancelEventReceived = true;
				_sync.Set();
			}));
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentSaveFailed>(m => {
				_saveFailEventReceived = true;
				_sync.Set();
			}));
		}

		protected void CreateControllers() {
			//create document
			using( _controller.Bus.Subscribe<Messages.BranchShiftingFinished>(m => _sync.Set()) ) {
				_controller.Bus.Publish(new Requests.CreateDocument());
				_sync.WaitOne(9000, false).ShouldBeTrue();
				//create branch to make it dirty...
				_docController.CreateBranch();
				_sync.WaitOne(9000, false).ShouldBeTrue();
			}
		}

		protected override void CustomTearDown() {
			_docController.Close();
			base.CustomTearDown();
			try {
				_controllerContext.Application.Dispatcher.Invoke(new Action(() => _controllerContext.Application.Shutdown()));
			} catch( System.Threading.Tasks.TaskCanceledException ) {
				// do nothing just die. this occurs because task wich runs shutdown gets cancelled.
			}
		}
	}
}
