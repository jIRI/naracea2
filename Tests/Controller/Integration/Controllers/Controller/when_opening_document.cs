﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using Naracea.View.Events;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_opening_document : with_expectations_on_new_document_creation {
		static readonly string _fileName = "fileToBeOpened.ncd";

		protected override void SetUp() {
			base.SetUp();
			var openDialog = _container.Resolve<IOpenDocumentDialog>();
			openDialog.Stub(a => a.ShowDialog())
				.WhenCalled(a => openDialog.Raise(
					x => x.FileSelected += null, 
					_controller, 
					new FileSelectedEventArgs(_fileName)
			));
			_fileCleanup.Add(_fileName);
			_expectedDocument = new IntegrationData().CreateAndSaveSimpleDocument(_fileName, _controllerContext.Core);
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpened>(m => {
				_docController = m.DocumentController;
				_sync.Set();
			}));
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.OpenDocumentOrDocuments());
			_eventReceived = _sync.WaitOne(5000, false);
		}
	}
}
