﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_exiting_app_when_file_ops_are_locked : with_stop_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			_controller.LockFileOperations();
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Naracea.View.Requests.Exit());
		}

		[Test]
		public void it_should_not_receive_stop_event() {
			_stopEventReceived.ShouldBeFalse();
		}

		[Test]
		public void it_should_show_the_notification_dialog() {
			_container.Resolve<IFileOperationsAreLockedDialog>().AssertWasCalled(a => a.ShowDialog());
		}
	}
}
