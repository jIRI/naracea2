﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	public abstract class with_expectations_on_new_document_creation : with_controller_setup {
		protected AutoResetEvent _sync = new AutoResetEvent(false);
		protected bool _eventReceived;
		protected IDocumentController _docController;
		protected IDocumentView _docView;
		protected IBranchView _branchView;
		protected IDocument _expectedDocument;

		protected override void SetUp() {
			base.SetUp();
			_docView = _container.Resolve<IDocumentView>();
			_branchView = _container.Resolve<IBranchView>();
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}

		[Test]
		public void it_should_receive_document_opened_event() {
			_eventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_return_document_controller() {
			_docController.ShouldNotBeNull();
		}

		[Test]
		public void it_should_create_new_view_for_document() {
			_docController.View.ShouldNotBeNull();
		}

		[Test]
		public void it_should_set_documents_view_context_to_itself() {
			_docController.View.Controller.ShouldBeSameAs(_docController);
		}

		[Test]
		public void it_should_create_view_for_document() {
			_viewContext.AssertWasCalled(a => a.CreateView<IDocumentView>());
		}

		[Test]
		public void it_should_add_document_view_to_application_view() {
			_viewContext.ApplicationView.AssertWasCalled(a => a.AddDocument(_docView));
		}

		[Test]
		public void it_should_activate_new_document_view() {
			_viewContext.ApplicationView.AssertWasCalled(a => a.ActivateDocument(_docView));
		}

		[Test]
		public void it_should_update_application_view() {
			_viewContext.ApplicationView.AssertWasCalled(a => a.UpdateDocument(_docView));
		}

		[Test]
		public void it_should_create_view_for_new_branch() {
			_viewContext.AssertWasCalled(a => a.CreateView<IBranchView>());
		}

		[Test]
		public void it_should_add_branch_view_to_document_view() {
			_docView.AssertWasCalled(a => a.AddBranch(_docController.ActiveBranch.View));
		}

		[Test]
		public void it_should_activate_new_branch_view() {
			_docView.AssertWasCalled(a => a.ActivateBranch(_docController.ActiveBranch.View));
		}

		[Test]
		public void it_should_mark_branch_view_as_not_deletable() {
			_docController.ActiveBranch.View.CanBeDeleted.ShouldBeFalse();
		}
	}
}
