﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Autofac;
using Naracea.Controller;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.FileSystem;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_starting_controller_with_params : with_controller_setup {
		AutoResetEvent _sync = new AutoResetEvent(false);
		string[] _expectedFileOpenRequests;
		IEnumerable<string> _actualFileOpenRequests;
		
		protected override void SetUp() {
			base.SetUp();
			_expectedFileOpenRequests = new string[] { "file.tmp"};
			_fileCleanup.AddRange(_expectedFileOpenRequests);
			using( var file = System.IO.File.Create(_expectedFileOpenRequests[0]) ) {
				file.Close();
			}

			_subscriptions.Add(_controller.Bus.Subscribe<Requests.OpenDocuments>(m => {
				_actualFileOpenRequests = m.Paths;
			}));
			_subscriptions.Add(_controller.Bus.Subscribe<Messages.DocumentOpenFailed>(m => {
				_sync.Set();
			}));
		}

		protected override void ApplyCustomConfiguration() {
			_builder.Register<IFileChecker>(c => new FakeFileChecker());
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Naracea.Controller.Requests.Start(_expectedFileOpenRequests));
			_sync.WaitOne(5000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_request_opening_files() {
			_actualFileOpenRequests.ShouldBeEqualTo(_expectedFileOpenRequests);
		}
	}
}
