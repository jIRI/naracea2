﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Common.Tests.BddAsserts;
using Naracea.View;
using Naracea.Controller.Tests.CommonContexes;
using System.Threading;
using Naracea.View.Events;
using Naracea.Core.Repository;

namespace Naracea.Controller.Tests.Integration.Controllers.Controller {
	[TestFixture]
	public class when_saving_document_but_cancelling_the_dialog : with_save_testing_setup {
		protected override void SetUp() {
			base.SetUp();
			_saveDialog.Stub(a => a.ShowDialog());
			this.CreateControllers();
		}

		protected override void BecauseOf() {
			_controller.Bus.Publish(new Requests.SaveDocument(_docController));
			_sync.WaitOne(7000, false).ShouldBeTrue();
		}

		[Test]
		public void it_should_receive_save_cancelled_event() {
			_saveCancelEventReceived.ShouldBeTrue();
		}

		[Test]
		public void it_should_ask_for_filename() {
			_saveDialog.AssertWasCalled(a => a.ShowDialog());
		}

		[Test]
		public void it_should_not_save_the_document_to_the_file() {
			System.IO.File.Exists(_fileName).ShouldBeFalse();
		}
	}
}
