﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Author {
	[TestFixture]
	public class when_new_author_controller_is_created : with_all_author_prerequisities {
		IAuthorController _authorController;

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_authorController = new AuthorController(_authors, _context);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_authorController.Close();
		}

		[Test]
		public void it_should_set_first_author_from_the_list_as_current_author() {
			_authorController.CurrentAuthor.ShouldBeSameAs(_authors[0]);
		}
	}
}
