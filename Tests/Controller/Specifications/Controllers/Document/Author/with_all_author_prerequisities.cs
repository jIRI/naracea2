﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core.Model;
using Autofac;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Author {
	public abstract class with_all_author_prerequisities : Context {
		protected IControllerContext _context;
		protected IList<Core.Model.IAuthor> _authors;

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var factoriesBuilder = new CommonContexes.Factories().Register(_builder);
			var controllerContextBuilder = new CommonContexes.ControllerContext().Register(_builder);
			_container = _builder.Build();
			_context = _container.Resolve<IControllerContext>();
			_authors = new List<Core.Model.IAuthor> {
				_container.Resolve<Core.Model.IAuthor>()
			};
		}
	}
}
