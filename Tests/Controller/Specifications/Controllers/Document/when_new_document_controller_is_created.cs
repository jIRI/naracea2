﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Autofac;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	[TestFixture]
	public class when_new_document_controller_is_created : with_all_document_prerequisities {
		IDocumentController _documentController;
		IBranchView _branchView;

		protected override void SetUp() {
			base.SetUp();
			var emptyDocumentBuilder = new CommonContexes.EmptyDocument();
			emptyDocumentBuilder.SetUp(_container);
			_branchView = _container.Resolve<IBranchView>();
		}

		protected override void BecauseOf() {
			_documentController = new DocumentController(_controller);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_documentController.Close();
		}

		[Test]
		public void it_should_not_fail() {
			true.ShouldBeTrue();
		}
	}
}
