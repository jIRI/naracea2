﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.TextEditor {
	[TestFixture]
	public class when_new_text_editor_controller_stores_change : with_all_text_editor_prerequisities {
		ITextEditorController _editorController;
		IChange _expectedChange;

		protected override void SetUp() {
			base.SetUp();
			_expectedChange = MockRepository.GenerateStub<IChange>();
			_editorController = new TextEditorController(_documentController, _branchController, _context);
		}

		protected override void BecauseOf() {
			_editorController.Store(_expectedChange);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_editorController.Close();
		}

		[Test]
		public void it_should_call_onstoring_on_change() {
			_expectedChange.AssertWasCalled(a => a.OnStoring(_editorProxy));
		}

		[Test]
		public void it_should_store_pass_the_change_to_branch_controller_for_storing() {
			_branchController.AssertWasCalled(a => a.Store(_expectedChange));
		}
	}
}
