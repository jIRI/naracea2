﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.TextEditor {
	public abstract class with_all_text_editor_prerequisities : Context {
		protected IBranchController _branchController;
		protected ITextEditorView _editorView;
		protected ITextEditor _editorBridge;
		protected ITextEditorProxy _editorProxy;
		protected IControllerContext _context;
		protected IDocumentController _documentController; 

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var factoriesBuilder = new CommonContexes.Factories().Register(_builder);
			var controllerContextBuilder = new CommonContexes.ControllerContext().Register(_builder);

			_container = _builder.Build();

			_editorView = _container.Resolve<ITextEditorView>();
			_editorBridge = _container.Resolve<ITextEditor>();
			_editorProxy = _container.Resolve<ITextEditorProxy>();
			_documentController = _container.Resolve<IDocumentController>();
			_branchController = _container.Resolve<IBranchController>();
			viewContextBuilder.Stub(_container);
			factoriesBuilder.Stub(_container);
			_context = controllerContextBuilder.Stub(_container);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_branchController.Close();
			_documentController.Close();
		}
	}
}
