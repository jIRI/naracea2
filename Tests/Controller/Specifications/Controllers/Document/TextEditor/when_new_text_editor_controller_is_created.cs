﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.TextEditor {
	[TestFixture]
	public class when_new_text_editor_controller_is_created : with_all_text_editor_prerequisities {
		ITextEditorController _editorController;

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_editorController = new TextEditorController(_documentController, _branchController, _context);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_editorController.Close();
		}

		[Test]
		public void it_should_set_view_context() {
			_editorView.Controller.ShouldBeSameAs(_editorController);
		}
	}
}
