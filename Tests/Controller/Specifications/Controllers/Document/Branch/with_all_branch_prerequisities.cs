﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core.Model;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Branch {
	public abstract class with_all_branch_prerequisities : Context {
		protected IBranch _branch;
		protected IControllerContext _context;
		protected IDocumentController _documentController;
		protected ITextEditorController _textEditor;

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var factoriesBuilder = new CommonContexes.Factories().Register(_builder);
			var controllerContextBuilder = new CommonContexes.ControllerContext().Register(_builder);
			
			_container = _builder.Build();

			_documentController = _container.Resolve<IDocumentController>();
			_documentController.AuthorController = _container.Resolve<IAuthorController>();
			//_documentController.AuthorController.CurrentAuthor = _container.Resolve<IAuthor>();
			_branch = _container.Resolve<IBranch>();
			_textEditor = _container.Resolve<ITextEditorController>();
			_textEditor.Stub(a => a.View).Return(_container.Resolve<ITextEditorView>());
			var timeline = _container.Resolve<ITimelineController>();
			timeline.Stub(a => a.View).Return(_container.Resolve<ITimelineView>());
			var changeStream = _container.Resolve<IChangeStreamController>();
			changeStream.Stub(a => a.View).Return(_container.Resolve<IChangeStreamView>());
			viewContextBuilder.Stub(_container);
			factoriesBuilder.Stub(_container);
			_context = controllerContextBuilder.Stub(_container);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_documentController.Close();
		}
	}
}
