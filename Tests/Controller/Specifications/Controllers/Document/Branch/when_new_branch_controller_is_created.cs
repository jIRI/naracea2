﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.View.Components;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Branch {
	[TestFixture]
	public class when_new_branch_controller_is_created : with_all_branch_prerequisities {
		IBranchController _branchController;

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_branchController = new BranchController(_documentController, _branch, _context);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_branchController.Close();
		}

		[Test]
		public void it_should_set_its_view_context() {
			_branchController.View.Controller.ShouldBeSameAs(_branchController);
		}

		[Test]
		public void it_should_set_its_view() {
			_branchController.View.ShouldNotBeNull();
		}

		[Test]
		public void it_should_set_its_editor() {
			_branchController.Editor.ShouldBeSameAs(_textEditor);
		}

		[Test]
		public void it_should_create_new_text_editor_controller() {
			_context.Controller.AssertWasCalled(a => a.CreateTextEditorController(_documentController, _branchController, _context));
		}

		[Test]
		public void it_should_not_activate() {
			_branchController.IsActivated.ShouldBeFalse();
		}
	}
}
