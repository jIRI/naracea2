﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.View.Components;
using Autofac;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Branch {
	[TestFixture]
	public class when_new_branch_controller_is_activated_for_the_first_time : with_all_branch_prerequisities {
		IBranchController _branchController;
		IEditorIndicators _indicators;
		IChangesCollection _changes;

		protected override void SetUp() {
			base.SetUp();
			var author = MockRepository.GenerateStub<IAuthor>();
			var list = new List<IChange> {
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
			};

			_changes = new FakeChangeCollection(list);
			_branch.Stub(a => a.Changes).Return(_changes);
			_indicators = _container.Resolve<IEditorIndicators>();
			_branchController = new BranchController(_documentController, _branch, _context);
		}

		protected override void BecauseOf() {
			_context.Bus.Publish(new Messages.BranchActivated(_documentController, _branchController));
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_branchController.Close();
		}

		[Test]
		public void it_should_activate() {
			_branchController.IsActivated.ShouldBeTrue();
		}
	}
}
