﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Branch {
	[TestFixture]
	public class when_branch_controller_is_storing_change_on_rewinded_branch : with_all_branch_prerequisities {
		IBranchController _branchController;
		IChange _expectedChange;
		IChange _expectedMultiUndoChange;
		IChangesCollection _changeCollection;
		int _expectedChangeCount = 10;
		int _expectedCurrentChangeIndex = 5;

		protected override void SetUp() {
			base.SetUp();
			_expectedChange = MockRepository.GenerateStub<IChange>();
			_expectedMultiUndoChange = _context.Core.CreateMultiUndo(null, null, DateTimeOffset.UtcNow);
			_changeCollection = MockRepository.GenerateStub<IChangesCollection>();
			_changeCollection.Stub(a => a.Count).Return(_expectedChangeCount);
			for(int i = _expectedCurrentChangeIndex; i < _expectedChangeCount; i++) {
				_changeCollection.Stub(a => a.At(i)).Return(MockRepository.GenerateStub<IChange>());
			}
			_branch.Stub(a => a.Changes).Return(_changeCollection);
			_branch.CurrentChangeIndex = _expectedCurrentChangeIndex;
			_documentController.AuthorController = MockRepository.GenerateStub<IAuthorController>();
			_documentController.AuthorController.Stub(a => a.CurrentAuthor).Return(MockRepository.GenerateStub<IAuthor>());
			_branchController = new BranchController(_documentController, _branch, _context);
		}

		protected override void BecauseOf() {
			_branchController.Store(_expectedChange);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_branchController.Close();
		}

		[Test]
		public void it_should_add_the_change_to_the_branch() {
			_changeCollection.AssertWasCalled(a => a.Add(_expectedChange));
		}

		[Test]
		public void it_should_add_the_multiundo_change_to_the_branch() {
			_changeCollection.AssertWasCalled(a => a.Add(_expectedMultiUndoChange));
		}

		[Test]
		public void it_should_set_the_dirty_flag_on_branch() {
			_branchController.IsDirty.ShouldBeTrue();
		}
	}
}
