﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Repository;
using Naracea.Controller.Tests.CommonContexes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	[TestFixture]
	public class when_document_controller_saves_after_open : with_all_document_prerequisities {
		IDocumentController _documentController;
		static readonly string _expectedFilemane = "document.ncd";
		IRepositoryContext _repositoryContext;

		protected override void SetUp() {
			base.SetUp();
			var simpleDocBuilder = new SimpleDocument();
			simpleDocBuilder.SetUp(_container);
			simpleDocBuilder.AddFirstBranch();

			_repositoryContext = MockRepository.GenerateStub<IRepositoryContext>();
			_repositoryContext.Path = _expectedFilemane;
			_documentController = new DocumentController(_controller);
			_documentController.Open(_repositoryContext);
		}

		protected override void BecauseOf() {
			_documentController.Save();
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_documentController.Close();
		}

		[Test]
		public void it_should_save_the_file() {
			_repository.AssertWasCalled(a => a.Save(_document));
		}

		[Test]
		public void it_should_reset_the_dirty_flag_on_document() {
			_documentController.IsDirty.ShouldBeFalse();
		}
	}
}
