﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using Autofac;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	[TestFixture]
	public class when_new_document_controller_opens_with_valid_repo_context : with_all_document_prerequisities {
		IDocumentController _documentController;
		static readonly string _expectedFilemane = "document.ncd";
		IRepositoryContext _repositoryContext;

		protected override void SetUp() {
			base.SetUp();
			var simpleDocBuilder = new CommonContexes.SimpleDocument();
			simpleDocBuilder.NoBranchControllerSetUp(_container);
			simpleDocBuilder.AddFirstBranch();
			var branchController = _container.Resolve<IBranchController>();
			branchController.Stub(a => a.Branch).Return(simpleDocBuilder.Branches[0]);
			branchController.Stub(a => a.View).Return(_container.Resolve<IBranchView>());

			_repositoryContext = MockRepository.GenerateStub<IRepositoryContext>();
			_repositoryContext.Path = _expectedFilemane;
			_documentController = new DocumentController(_controller);
		}

		protected override void BecauseOf() {
			_documentController.Open(_repositoryContext);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_documentController.Close();
		}

		[Test]
		public void it_should_load_the_document() {
			_context.Core.AssertWasCalled(a => a.CreateDocumentRepository(_repositoryContext));
		}

		[Test]
		public void it_should_add_branch_view_to_document_view() {
			_documentView.AssertWasCalled(a => a.AddBranch(_documentController.Branches.First().View));
		}

		[Test]
		public void it_should_activate_the_new_branch() {
			_documentView.AssertWasCalled(a => a.ActivateBranch(_documentController.Branches.First().View));
		}
	}
}
