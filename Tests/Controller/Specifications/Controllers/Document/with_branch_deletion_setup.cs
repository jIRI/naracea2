﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Autofac;
using Naracea.Core.Model;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	public abstract class with_branch_deletion_setup : with_all_document_prerequisities {
		protected IDocumentController _documentController;
		protected List<IBranch> _branches = new List<IBranch>();
		
		protected override void SetUp() {
			base.SetUp();
			var emptyDocumentBuilder = new CommonContexes.EmptyDocument();
			
			var defaultBranch = MockRepository.GenerateStub<IBranch>();
			defaultBranch.Stub(a => a.Parent).Return(null);
			_branches.Add(defaultBranch);
			
			var firstBranch = MockRepository.GenerateStub<IBranch>();
			firstBranch.Stub(a => a.Parent).Return(defaultBranch);
			_branches.Add(firstBranch);
			
			var secondBranch = MockRepository.GenerateStub<IBranch>();
			secondBranch.Stub(a => a.Parent).Return(firstBranch);
			_branches.Add(secondBranch);
			
			emptyDocumentBuilder.SetUp(_container, _branches);
			_documentController = new DocumentController(_controller);
			_documentController.Initialize();
		}
	
		protected override void CustomTearDown() {
			base.CustomTearDown();
			_documentController.Close();
		}
	}
}
