﻿using Autofac;
using Naracea.Common;
using Naracea.Common.Tests;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.View;
using Rhino.Mocks;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	public abstract class with_all_document_prerequisities : Context {
		protected IController _controller;
		protected IDocumentView _documentView;
		protected IControllerContext _context;
		protected IDocument _document;
		protected ISettings _settings;
		protected IRepository<IDocument> _repository;

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var factoriesBuilder = new CommonContexes.Factories().Register(_builder);
			var controllerContextBuilder = new CommonContexes.ControllerContext().Register(_builder);

			_container = _builder.Build();

			_controller = _container.Resolve<IController>();
			_repository = _container.Resolve<IRepository<IDocument>>();
			_document = _container.Resolve<IDocument>();
			_documentView = _container.Resolve<IDocumentView>();
			_settings = _container.Resolve<ISettings>();
			var viewContext = viewContextBuilder.Stub(_container);
			factoriesBuilder.Stub(_container);
			_context = controllerContextBuilder.Stub(_container);
			_controller.Stub(a => a.Context).Return(_context);
			_controller.Stub(a => a.View).Return(_context.View.ApplicationView);
			_repository.Stub(a => a.Load()).Return(_container.Resolve<IDocument>());
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_controller.Close();
		}
	}
}
