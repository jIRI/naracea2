﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Timeline {
	[TestFixture]
	public class when_timeline_is_created_on_empty_branch : with_all_timeline_prerequisities {
		ITimelineController _timelineController;

		protected override void SetUp() {
			base.SetUp();
		}

		public override List<IChange> Changes {
			get {
				return new List<IChange>();
			}
		}

		protected override void BecauseOf() {
			_timelineController = new TimelineController(_branchController, _granularities, _context);
			_timelineController.Initialize();
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_timelineController.Close();
		}

		[Test]
		public void it_should_create_view() {
			_timelineController.View.ShouldBeSameAs(_timelineView);
		}

		[Test]
		public void it_should_create_one_bar_on_view() {
			_addedBars.Count.ShouldEqualTo(1);
			_addedBars[0].Ids.Count().ShouldEqualTo(0);
			_addedBars[0].From.ShouldEqualTo(_granularities[0].Trim(_now));
			_addedBars[0].To.ShouldEqualTo(_granularities[0].Advance(_granularities[0].Trim(_now)));
		}
	}
}
