﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Timeline {
	[TestFixture]
	public class when_timeline_is_created_on_branch_with_changes_inside_of_one_bin : with_all_timeline_prerequisities {
		ITimelineController _timelineController;
		List<IChange> _changeList;


		protected override void SetUp() {
			base.SetUp();
		}

		public override List<IChange> Changes {
			get {
				if( _changeList == null ) {
					_changeList = new List<IChange> {
						new FakeChange(_now.AddSeconds(-5), 0),
						new FakeChange(_now.AddSeconds(-4), 1),
						new FakeChange(_now.AddSeconds(-3), 2),
						new FakeChange(_now.AddSeconds(-2), 3),
						new FakeChange(_now.AddSeconds(-1), 4),
					};
				}
				return _changeList;
			}
		}

		protected override void BecauseOf() {
			_timelineController = new TimelineController(_branchController, _granularities, _context);
			_timelineController.Initialize();
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_timelineController.Close();
		}

		[Test]
		public void it_should_create_one_bar_on_view() {
			_addedBars.Count.ShouldEqualTo(1);
		}

		[Test]
		public void it_should_create_one_bar_starting_with_datetime_of_first_change() {
			_addedBars[0].From.ShouldEqualTo(_granularities[0].Trim(_changeList[0].DateTime));
		}

		[Test]
		public void it_should_create_one_bar_with_span_equal_to_controllers_granularity() {
			_addedBars[0].To.ShouldEqualTo(_granularities[0].Advance(_granularities[0].Trim(_changeList[0].DateTime)));
		}

		[Test]
		public void it_should_create_one_bar_containing_all_ids() {
			var ids = from ch in _changeList
								select ch.Id;
			_addedBars[0].Ids.ShouldEqualTo(ids);
		}
	}
}
