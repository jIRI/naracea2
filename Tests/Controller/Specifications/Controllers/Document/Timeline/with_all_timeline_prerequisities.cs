﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core.Model;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Model.Changes;
using Naracea.Common;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Timeline {
	public abstract class with_all_timeline_prerequisities : Context {
		protected IBranchController _branchController;
		protected IBranch _branch;
		protected IChangesCollection _changes;
		protected IControllerContext _context;
		protected DateTimeOffset _now = DateTimeOffset.UtcNow;
		protected ITimelineView _timelineView;
		protected List<TimelineBar> _addedBars = new List<TimelineBar>();
		protected List<TimelineBar> _updatedBars = new List<TimelineBar>();
		protected List<IUnitOfTime> _granularities;

		protected override void SetUp() {
			var viewContextBuilder = new CommonContexes.ViewContext().Register(_builder);
			var factoriesBuilder = new CommonContexes.Factories().Register(_builder);
			var controllerContextBuilder = new CommonContexes.ControllerContext().Register(_builder);

			_now = new DateTimeOffset(_now.Year, _now.Month, _now.Day, _now.Hour, _now.Minute, 30, TimeSpan.Zero);
			_container = _builder.Build();

			_branchController = _container.Resolve<IBranchController>();
			_branch = _container.Resolve<IBranch>();
			_changes = new FakeChangeCollection(this.Changes);
			_branchController.Stub(a => a.Branch).Return(_branch);
			_branch.Stub(a => a.Changes).Return(_changes);
			viewContextBuilder.Stub(_container);
			_granularities = new List<IUnitOfTime> { 
				new Minute()
			};
			_timelineView = _container.Resolve<ITimelineView>();
			_timelineView.Stub(a => a.AddBar(Arg<TimelineBar>.Is.Anything))
				.WhenCalled(i =>
					_addedBars.Add((TimelineBar)i.Arguments[0])
				);

			_timelineView.Stub(a => a.SetBars(Arg<IList<TimelineBar>>.Is.Anything))
				.WhenCalled(i => {
						foreach( var bar in (IList<TimelineBar>)i.Arguments[0] ) {
							_addedBars.Add(bar);
						}
					}
				);

			_timelineView.Stub(a => a.UpdateLastBar(Arg<TimelineBar>.Is.Anything))
				.WhenCalled(i =>
					_updatedBars.Add((TimelineBar)i.Arguments[0])
				);

			factoriesBuilder.Stub(_container);
			_context = controllerContextBuilder.Stub(_container);
			_context.DateTimeProvider.Stub(a => a.Now).Return(_now);
		}

		public abstract List<IChange> Changes { get; }

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_branchController.Close();
		}

	}
}
