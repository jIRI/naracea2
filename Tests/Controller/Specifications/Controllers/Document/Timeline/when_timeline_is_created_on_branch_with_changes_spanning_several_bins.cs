﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Common;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Timeline {
	[TestFixture]
	public class when_timeline_is_created_on_branch_with_changes_spanning_several_bins : with_all_timeline_prerequisities {
		ITimelineController _timelineController;
		List<IChange> _changeList;
		DateTimeOffset _bin1;
		DateTimeOffset _bin2;
		DateTimeOffset _bin3;
		DateTimeOffset _bin4;
		int offset = 10;


		protected override void SetUp() {
			base.SetUp();
			_timelineController = new TimelineController(_branchController, _granularities, _context);
			_timelineController.Initialize();
			_addedBars.Clear();
		}

		public override List<IChange> Changes {
			get {
				if( _changeList == null ) {
					_bin4 = _now;
					var span = (new Minute()).Advance(_now) - _now;
					_bin3 = _bin4.Subtract(span);
					_bin2 = _bin3.Subtract(span);
					_bin1 = _bin2.Subtract(span);
					_changeList = new List<IChange> {
						new FakeChange(_bin1.AddSeconds(-offset + 0), 0),
						new FakeChange(_bin1.AddSeconds(-offset + 1), 1),
						new FakeChange(_bin1.AddSeconds(-offset + 2), 2),
						new FakeChange(_bin1.AddSeconds(-offset + 3), 3),
						new FakeChange(_bin1.AddSeconds(-offset + 4), 4),
																						
						new FakeChange(_bin3.AddSeconds(-offset + 0), 5),
						new FakeChange(_bin3.AddSeconds(-offset + 1), 6),
						new FakeChange(_bin3.AddSeconds(-offset + 2), 7),
						new FakeChange(_bin3.AddSeconds(-offset + 3), 8),
						new FakeChange(_bin3.AddSeconds(-offset + 4), 9),
																						
						new FakeChange(_bin4.AddSeconds(-offset + 0), 10),
						new FakeChange(_bin4.AddSeconds(-offset + 1), 11),
						new FakeChange(_bin4.AddSeconds(-offset + 2), 12),
						new FakeChange(_bin4.AddSeconds(-offset + 3), 13),
						new FakeChange(_bin4.AddSeconds(-offset + 4), 14),
					};
				}
				return _changeList;
			}
		}

		protected override void BecauseOf() {
			_timelineController.ShowEmptyBars = true;
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_timelineController.Close();
		}

		[Test]
		public void it_should_create_4_bars_on_view() {
			_addedBars.Count.ShouldEqualTo(4);
		}

		[Test]
		public void it_should_create_1st_bar_starting_with_datetime_of_bin1() {
			_addedBars[0].From.ShouldEqualTo(_granularities[0].Trim(_bin1.AddSeconds(-offset)));
		}

		[Test]
		public void it_should_create_2nd_bar_starting_with_datetime_of_bin2() {
			_addedBars[1].From.ShouldEqualTo(_granularities[0].Trim(_bin2.AddSeconds(-offset)));
		}

		[Test]
		public void it_should_create_3rd_bar_starting_with_datetime_of_bin3() {
			_addedBars[2].From.ShouldEqualTo(_granularities[0].Trim(_bin3.AddSeconds(-offset)));
		}

		[Test]
		public void it_should_create_4th_bar_starting_with_datetime_of_bin4() {
			_addedBars[3].From.ShouldEqualTo(_granularities[0].Trim(_bin4.AddSeconds(-offset)));
		}

		[Test]
		public void it_should_create_1st_bar_containing_all_ids_belonging_to_bin1() {
			var ids = from ch in _changeList
								where ch.DateTime <= _bin1
								select ch.Id;
			_addedBars[0].Ids.ShouldEqualTo(ids);
		}

		[Test]
		public void it_should_create_2nd_bar_empty() {
			_addedBars[1].Ids.ShouldBeEmpty();
		}

		[Test]
		public void it_should_create_3rd_bar_containing_all_ids_belonging_to_bin3() {
			var ids = from ch in _changeList
								where ch.DateTime >= _bin2 && ch.DateTime <= _bin3
								select ch.Id;
			_addedBars[2].Ids.ShouldEqualTo(ids);
		}

		[Test]
		public void it_should_create_4th_bar_containing_all_ids_belonging_to_bin4() {
			var ids = from ch in _changeList
								where ch.DateTime >= _bin3 && ch.DateTime <= _bin4
								select ch.Id;
			_addedBars[3].Ids.ShouldEqualTo(ids);
		}
	}
}
