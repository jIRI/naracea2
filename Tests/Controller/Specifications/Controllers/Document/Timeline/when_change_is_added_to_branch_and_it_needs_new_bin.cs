﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document.Timeline {
	[TestFixture]
	public class when_change_is_added_to_branch_and_it_needs_new_bin: with_all_timeline_prerequisities {
		ITimelineController _timelineController;
		List<IChange> _changeList;
		int _addedChangeId = 3;
		DateTimeOffset _addedChangeDateTime;

		protected override void SetUp() {
			base.SetUp();
			_timelineController = new TimelineController(_branchController, _granularities, _context);
			_timelineController.Initialize();
			_addedChangeDateTime = _granularities[0].Advance(_now).AddSeconds(1);
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
			_timelineController.Close();
		}

		public override List<IChange> Changes {
			get {
				if( _changeList == null ) {
					_changeList = new List<IChange> {
						new FakeChange(_now.AddSeconds(-4), 0),
						new FakeChange(_now.AddSeconds(-3), 1),
						new FakeChange(_now.AddSeconds(-2), 2),
					};
				}
				return _changeList;
			}
		}

		protected override void BecauseOf() {
			_changes.Add(new FakeChange(_addedChangeDateTime, _addedChangeId));
		}

		[Test]
		public void it_should_create_new_bin() {
			_addedBars.Count.ShouldEqualTo(2);
		}

		[Test]
		public void it_should_keep_original_bin_untouched() {
			var ids = from ch in _changeList
								where ch.DateTime < _addedChangeDateTime
								select ch.Id;
			_addedBars[0].Ids.ShouldEqualTo(ids);
		}

		[Test]
		public void it_should_add_id_of_added_change_to_the_new_bin() {
			_addedBars[1].Ids.ShouldEqualTo(new int[] { _addedChangeId });
		}
	}
}
