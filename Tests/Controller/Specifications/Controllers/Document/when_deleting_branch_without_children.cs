﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Autofac;
using Naracea.Core.Model;

namespace Naracea.Controller.Tests.Specifications.Controllers.Document {
	[TestFixture]
	public class when_deleting_branch_without_children : with_branch_deletion_setup {
		IBranchController _branchController;

		protected override void SetUp() {
			base.SetUp();
			_branchController = MockRepository.GenerateMock<IBranchController>();
			_branchController.Stub(a => a.Branch).Return(_branches[2]);
		}

		protected override void BecauseOf() {
			_documentController.DeleteBranch(_branchController);
		}

		[Test]
		public void it_should_not_close_branch_controller() {
			_branchController.AssertWasCalled(a => a.Close());
		}

		[Test]
		public void it_should_not_delete_the_branch() {
			_branches.Contains(_branchController.Branch).ShouldBeFalse();
		}

		[Test]
		public void it_should_remove_branch_view_from_document_view() {
			_documentView.AssertWasCalled(a => a.RemoveBranch(_branchController.View));
		}
	}
}
