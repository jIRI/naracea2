﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	[TestFixture]
	public class when_using_to_change_backward_shifter : with_expectations_on_shifter {
		protected override void SetupShifter() {
			_branch.CurrentChangeIndex = _expectedChangeCount - 1;
			_shifter = new ShiftToChangeBackward(-1);
		}
	}
}
