﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	[TestFixture]
	public class when_using_to_change_forward_shifter_and_current_change_is_last : with_branch_with_some_changes {
		IShifter _shifter;
		int _expectedChangeIndex;

		protected override void SetUp() {
			base.SetUp();
			_branch.CurrentChangeIndex = _branch.Changes.Count - 1;
			_expectedChangeIndex = _branch.Changes.Count / 2;
			_shifter = new ShiftToChangeForward(_expectedChangeIndex);
		}

		protected override void BecauseOf() {
			_shifter.Shift(_branch, _textEditor);
		}

		[Test]
		public void it_should_keep_current_change_on_last() {
			//since all changes are done already
			_branch.CurrentChangeIndex.ShouldEqualTo(_branch.Changes.Count - 1);
		}
	}
}
