﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	[TestFixture]
	public class when_using_all_backward_shifter_and_current_change_is_null : with_branch_with_some_changes {
		IShifter _shifter;

		protected override void SetUp() {
			base.SetUp();
			_branch.CurrentChangeIndex = -1;
			_shifter = new ShiftToStart();
		}

		protected override void BecauseOf() {
			_shifter.Shift(_branch, _textEditor);
		}

		[Test]
		public void it_should_keep_current_change_to_null() {
			_branch.CurrentChangeIndex.ShouldEqualTo(-1);
		}
	}
}
