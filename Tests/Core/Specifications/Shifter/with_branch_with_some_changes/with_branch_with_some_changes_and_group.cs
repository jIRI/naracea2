﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Configuration;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	public abstract class with_branch_with_some_changes_and_group : Context {
		protected IBranch _branch;
		protected ITextEditor _textEditor;
		protected IChangesCollection _changes;

		protected override void SetUp() {
			_textEditor = MockRepository.GenerateStub<ITextEditor>();
			_branch = MockRepository.GenerateStub<IBranch>();
			_changes = new FakeChangeCollection(this.ChangeList);
			_branch.Stub(b => b.Changes).Return(_changes);
		}

		protected List<IChange> ChangeList { get; set; }
	}
}
