﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	[TestFixture]
	public class when_using_to_change_forward_shifter_with_just_a_group_and_shifting_over_it : with_branch_with_some_changes_and_group {
		FakeChange _groupBegin;
		FakeChange _groupEnd;
		IShifter _shifter;

		protected override void SetUp() {
			// prepare data
			_groupBegin = new FakeChange() {
				ShiftCount = 4, //it is 4 - 3 changes to skip + 1 for group begin itself
				ShouldShifterExecuteFollowingChange = true
			};
			_groupEnd = new FakeChange() {
				ShiftCount = 4, //it is 4 - 3 changes to skip + 1 for group end itself
				ShouldShifterExecuteFollowingChange = true
			};
			this.ChangeList = new List<IChange> {
				_groupBegin,
				new FakeChange(),
				new FakeChange(),
				new FakeChange(),
				_groupEnd,
			};
			// setup
			base.SetUp();
			_branch.CurrentChangeIndex = -1;
			_shifter = new ShiftToChangeForward(0);
		}

		protected override void BecauseOf() {
			_shifter.Shift(_branch, _textEditor);
		}

		[Test]
		public void it_should_shift_the_whole_group() {
			_branch.CurrentChangeIndex.ShouldEqualTo(4);
		}
	}
}
