﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Configuration;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	public abstract class with_branch_with_some_changes : Context {
		protected IBranch _branch;
		protected ITextEditor _textEditor;
		protected IChangesCollection _changes;
		protected IChange _change;
		protected int _expectedChangeCount = 23;
		protected int _changeExecuteCallCount = 0;

		protected override void SetUp() {
			_textEditor = MockRepository.GenerateStub<ITextEditor>();
			_branch = MockRepository.GenerateStub<IBranch>();
			_changes = MockRepository.GenerateStub<IChangesCollection>();
			_changes.Stub(ch => ch.Count).Return(_expectedChangeCount);
			_change = MockRepository.GenerateStub<IChange>();
			_change.Stub(c => c.ShiftCount).Return(1);
			_change.Stub(c => c.ShouldShifterExecuteFollowingChange).Return(false);
			_change.Stub(c => c.ExecuteDo(null)).IgnoreArguments().Do(new Action<ITextEditor>((x) => _changeExecuteCallCount++));
			_change.Stub(c => c.ExecuteUndo(null)).IgnoreArguments().Do(new Action<ITextEditor>((x) => _changeExecuteCallCount++));
			_changes.Stub(ch => ch.At(0)).IgnoreArguments().Return(_change);
			_branch.Stub(b => b.Changes).Return(_changes);
		}
	}
}
