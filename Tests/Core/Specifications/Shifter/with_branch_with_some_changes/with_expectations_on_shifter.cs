﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Shifter.with_branch_with_some_changes {
	public abstract class with_expectations_on_shifter : with_branch_with_some_changes {
		protected IShifter _shifter;

		protected override void SetUp() {
			base.SetUp();
			this.SetupShifter();
		}

		protected override void BecauseOf() {
			_shifter.Shift(_branch, _textEditor);
		}

		abstract protected void SetupShifter();

		[Test]
		public void it_should_call_execute_do_on_processed_changes_expected_number_of_times() {
			_changeExecuteCallCount.ShouldEqualTo(_expectedChangeCount);
		}
	}
}
