﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Configuration;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Shifter.with_empty_branch {
	public abstract class with_empty_branch : Context {
		protected IBranch _branch;
		protected ITextEditor _textEditor;

		protected override void SetUp() {
			_branch = MockRepository.GenerateStub<IBranch>();
			var changes = MockRepository.GenerateStub<IChangesCollection>();
			changes.Stub(a => a.Count).Return(0);
			_branch.Stub(a => a.Changes).Return(changes);
			_branch.CurrentChangeIndex = -1;
			_textEditor = MockRepository.GenerateStub<ITextEditor>();
		}
	}
}
