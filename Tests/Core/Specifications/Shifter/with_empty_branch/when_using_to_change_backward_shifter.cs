﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;

namespace Naracea.Core.Tests.Specifications.Shifter.with_empty_branch {
	[TestFixture]
	public class when_using_to_change_backward_shifter : with_empty_branch {
		IShifter _shifter;

		protected override void SetUp() {
			base.SetUp();
			_shifter = new ShiftToChangeBackward(-1);
		}

		protected override void BecauseOf() {
			_shifter.Shift(_branch, _textEditor);
		}

		[Test]
		public void it_should_keep_current_change_null() {
			_branch.CurrentChangeIndex.ShouldEqualTo(-1);
		}
	}
}
