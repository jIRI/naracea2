﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.SampleData {
	internal class EmptyChanges {
		public IList<IChange> Changes {
			get {
				return new List<IChange> {
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithText>(), 
					MockRepository.GenerateStub<IChangeWithChange>(), 
					MockRepository.GenerateStub<IChangeWithChange>(), 
				};
			}
		}
	}
}
