﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.SampleData {
	internal class BranchWithParents {
		static IBranch _branchWithParents = null;
		static public IBranch GetData() {
			if( _branchWithParents == null ) {
				var parent1 = MockRepository.GenerateStub<IBranch>();
				parent1.Stub(a => a.BranchingChangeIndex).Return(0);
				parent1.Stub(a => a.Parent).Return(null);
				var list1 = new List<IChange> {
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>()
				};
				var changes1 = new ChangesCollection(parent1, list1, new IdProvider());
				parent1.Stub(a => a.Changes).Return(changes1);

				var parent2 = MockRepository.GenerateStub<IBranch>();
				parent2.Stub(a => a.BranchingChangeIndex).Return(4);
				parent2.Stub(a => a.Parent).Return(parent1);
				var list2 = new List<IChange> {
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>()
				};
				var changes2 = new ChangesCollection(parent2, list2, new IdProvider());
				parent2.Stub(a => a.Changes).Return(changes2);

				_branchWithParents = MockRepository.GenerateStub<IBranch>();
				_branchWithParents.Stub(a => a.BranchingChangeIndex).Return(7);
				_branchWithParents.CurrentChangeIndex = 11;
				_branchWithParents.Stub(a => a.Parent).Return(parent2);
				var list3 = new List<IChange> {
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>(),
 						MockRepository.GenerateStub<IChangeWithText>()
				};
				var changes3 = new ChangesCollection(_branchWithParents, list3, new IdProvider());
				_branchWithParents.Stub(a => a.Changes).Return(changes3);
			}
			return _branchWithParents;
		}
	}
}
