﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Model.Changes;
using Naracea.Core.Model;

namespace Naracea.Core.Tests.Specifications.SampleData {
	public class ChangesWithUndoRedo {
		IAuthor _author = new Author();

		public int TextChangesIndex { get; private set; }

		public int ConsecutiveUndosIndex { get; private set; }
		public int ConsecutiveRedosIndex { get; private set; }

		public int SingleUndoIndex { get; private set; }
		public int SingleRedoIndex { get; private set; }

		public List<IChange> Changes {
			get {
				var list = new List<IChange>();
				int pos = -1;
				//beware: this is carefully crafted set. any changes can break some test.
				//if you wish, add new items, but for god's sake, don't change existing ones...
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "a", 0)));
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "b", 1)));
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "c", 2)));
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "d", 3)));
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "e", 4))); this.TextChangesIndex = pos;
				pos++; list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 1])));
				pos++; list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 3]))); this.ConsecutiveUndosIndex = pos;
				pos++; list.Add(new Redo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 1])));
				pos++; list.Add(new Redo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 3]))); this.ConsecutiveRedosIndex = pos;
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "1", 5)));
				pos++; list.Add(new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "2", 5)));
				pos++; list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 1]))); this.SingleUndoIndex = pos;
				pos++; list.Add(new Redo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[pos - 1]))); this.SingleRedoIndex = pos;

				return list;
			}
		}

	}
}
