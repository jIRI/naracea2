﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;


using Naracea.View.Context;
using Naracea.View;
using Naracea.Core.Model;
using System.IO;
namespace Naracea.Core.Tests.Specifications.Utility.FileNameValidator {
	public abstract class with_context : Context {
		protected static readonly string _path = "TestFile.bin";

		protected override void SetUp() {
			_container = _builder.Build();
			this.RemoveFile();
		}

		protected override void CustomTearDown() {
			this.RemoveFile();
		}

		protected void CreateFile() {
			using( var file = File.Create(_path) ) {
				var data = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
				file.Write(data, 0, data.Length);
			}
		}

		private void RemoveFile() {
			if( File.Exists(_path) ) {
				File.Delete(_path);
			}
		}
	}
}
