﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core.FileSystem;

namespace Naracea.Core.Tests.Specifications.Utility.FileNameValidator {
	[TestFixture]
	public class when_file_does_exists : with_context {
		IFileNameValidator _validator;
		bool _result;

		protected override void SetUp() {
			base.SetUp();
			this.CreateFile();
			_validator = new Naracea.Core.FileSystem.FileNameValidator();
		}

		protected override void BecauseOf() {
			_result = _validator.IsValid(_path);
		}

		[Test]
		public void it_should_return_true() {
			Assert.That(_result, Is.True);
		}
	}
}
