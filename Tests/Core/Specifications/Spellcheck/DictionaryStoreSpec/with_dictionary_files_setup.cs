﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.DictionaryStoreSpec {
	public abstract class with_dictionary_files_setup : with_dictionary_store_setup {
		protected IDictionaryStore _store;
		protected static readonly string _storePath = "path";
		protected static readonly string _tempPath = "temp";
		protected static readonly string _expectedName = "dictionary";
		protected static readonly string _expectedDisplayName = "*:dictionary?!";
		protected static readonly string _affFile = @"dictPath\file.aff";
		protected static readonly string _dicFile = @"dictPath\file.dic";
		protected static readonly string _customFile = @"dictPath\custom";
		protected DictionaryDescriptor _expectedDictionaryDescriptor;

		protected override void SetUp() {
			base.SetUp();
			_store = new DictionaryStore(
				_fileChecker,
				_fileCopier,
				_fileRemover,
				_validator,
				_dirMaker,
				_dirRemover,
				_dirChecker,
				_xcuParser,
				_langInstallerFactory,
				_zipReaderFactory
			);
			_expectedDictionaryDescriptor = new DictionaryDescriptor(
				_storePath,
				_expectedName,
				_expectedDisplayName,
				_affFile,
				_dicFile,
				_customFile
			);
			_validator.Stub(a => a.GetValidFileName(_expectedDisplayName)).Return(_expectedName);
			_installer.Stub(a => a.Install(_expectedName, _expectedDisplayName, _affFile, _dicFile)).Return(_expectedDictionaryDescriptor);
		}
	}
}
