﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.DictionaryStoreSpec {
	[TestFixture]
	public class when_clean_installing_oo_dictionary : with_oo_dictionary_setup {
		protected override void SetUp() {
			base.SetUp();		
		}

		protected override void BecauseOf() {
			_store.InstallOoDictionary(_oxtFile);
		}

		[Test]
		public void it_should_add_installed_dictionary_to_the_list_of_installed_directories() {
			_store.InstalledLanguages.ShouldContain(_expectedDictionaryDescriptor);
		}

		[Test]
		public void it_should_try_to_cleanup() {
			_dirRemover.AssertWasCalled(a => a.Remove(_tempPath));
		}
	}
}
