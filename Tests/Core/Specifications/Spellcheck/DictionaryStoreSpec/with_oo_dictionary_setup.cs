﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.DictionaryStoreSpec {
	public abstract class with_oo_dictionary_setup : with_dictionary_store_setup {
		protected IDictionaryStore _store;
		protected static readonly string _storePath = "path";
		protected static readonly string _tempPath = "temp";
		protected static readonly string _expectedName = "en-us";
		protected static readonly string _expectedDisplayName = "English dictionary";
		protected static readonly string _oxtFile = @"dictPath\dictionary.oxt";
		protected static readonly string _affFile = "en_us.aff";
		protected static readonly string _dicFile = "en_us.dic";
		protected static readonly string _customFile = "custom";
		protected DictionaryDescriptor _expectedDictionaryDescriptor;
		protected OoDictionaryProperties _dictProperties;

		protected override void SetUp() {
			base.SetUp();
			_store = new DictionaryStore(
				_fileChecker,
				_fileCopier,
				_fileRemover,
				_validator,
				_dirMaker,
				_dirRemover,
				_dirChecker,
				_xcuParser,
				_langInstallerFactory,
				_zipReaderFactory
			);
			_dictProperties = new OoDictionaryProperties {
				DisplayName = _expectedDisplayName,
				StorageName = _expectedName,
				AffFileName = _affFile,
				DicFileName = _dicFile,
			};
			_expectedDictionaryDescriptor = new DictionaryDescriptor(
				_storePath,
				_expectedName,
				_expectedDisplayName,
				_affFile,
				_dicFile,
				_customFile
			);
			_dirMaker.Stub(a => a.MakeTempDir()).Return(_tempPath);
			_xcuParser.Stub(a => a.Parse(Path.Combine(_tempPath, "dictionaries.xcu"))).Return(_dictProperties);
			_installer.Stub(a => a.Install(
				_expectedName, 
				_expectedDisplayName, 
				Path.Combine(_tempPath, _affFile), 
				Path.Combine(_tempPath, _dicFile)
			)).Return(_expectedDictionaryDescriptor);
		}
	}
}
