﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using Naracea.Core.FileSystem;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.DictionaryStoreSpec {
	public abstract class with_dictionary_store_setup : Context {
		protected IFileChecker _fileChecker;
		protected IFileSerializer _serializer;
		protected IFileCopier _fileCopier;
		protected IFileRemover _fileRemover;
		protected IFileNameValidator _validator;
		protected IDirectoryMaker _dirMaker;
		protected IDirectoryRemover _dirRemover;
		protected IDirectoryChecker _dirChecker;
		protected IXcuParser _xcuParser;
		protected ILanguageInstaller _installer;
		protected IZipReader _zipReader;
		protected Func<string, IFileChecker, IFileCopier, IFileRemover, IDirectoryMaker, IDirectoryChecker, ILanguageInstaller> _langInstallerFactory;
		protected Func<string, IZipReader> _zipReaderFactory;

		protected override void SetUp() {
			_fileChecker = MockRepository.GenerateStub<IFileChecker>();
			_fileRemover = MockRepository.GenerateStub<IFileRemover>();
			_serializer = MockRepository.GenerateStub<IFileSerializer>();
			_fileCopier = MockRepository.GenerateStub<IFileCopier>();
			_validator = MockRepository.GenerateStub<IFileNameValidator>();
			_dirMaker = MockRepository.GenerateStub<IDirectoryMaker>();
			_dirRemover = MockRepository.GenerateStub<IDirectoryRemover>();
			_dirChecker = MockRepository.GenerateStub<IDirectoryChecker>();
			_xcuParser = MockRepository.GenerateStub<IXcuParser>();
			_installer = MockRepository.GenerateStub<ILanguageInstaller>();
			_langInstallerFactory = (a, b, c, d, e, f) => _installer;
			_zipReader = MockRepository.GenerateStub<IZipReader>();
			_zipReaderFactory = (a) => _zipReader;
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
