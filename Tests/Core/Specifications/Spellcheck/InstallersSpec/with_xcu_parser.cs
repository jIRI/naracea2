﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;

namespace Naracea.Core.Tests.Specifications.Spellcheck.InstallersSpec {
	public abstract class with_xcu_parser : Context {
		protected IXcuParser _parser;

		protected override void SetUp() {
			_parser = new XcuParser();			
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
