﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;

namespace Naracea.Core.Tests.Specifications.Spellcheck.InstallersSpec {
	[TestFixture]
	public class when_xcu_parser_parses_cz_descriptor : with_xcu_parser {
		OoDictionaryProperties _actualResult;

		protected override void SetUp() {
			base.SetUp();
		}

		protected override void BecauseOf() {
			_actualResult = _parser.Parse(@"Files\dictionariesCZ.xcu");
		}

		[Test]
		public void it_should_set_storage_dictionary_from_language_id() {
			_actualResult.StorageName.ToLowerInvariant().ShouldEqualTo("cs-cz");
		}

		[Test]
		public void it_should_set_dic_filename_properly() {
			_actualResult.DicFileName.ToLowerInvariant().ShouldEqualTo("cs_cz.dic");
		}

		[Test]
		public void it_should_set_aff_filename_properly() {
			_actualResult.DicFileName.ToLowerInvariant().ShouldEqualTo("cs_cz.dic");
		}

		[Test]
		public void it_should_set_display_name_properly() {
			_actualResult.DisplayName.ShouldEqualTo(new System.Globalization.CultureInfo("cs-cz").DisplayName);
		}
	}
}
