﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.CustomDictionarySpec {
	[TestFixture]
	public class when_saving_custom_dictionary : with_custom_dictionary_setup {
		ICustomDictionary _dict;
		List<string> _expectedWords;

		protected override void SetUp() {
			base.SetUp();
			_dict = new CustomDictionary(_storePath, _streamBuilder);
			_expectedWords = new List<string> {
				"no",
				"cars",
				"go"
			};
			_dict.Add(_expectedWords);
			_streamBuilder.Stub(a => a.Create(Arg<string>.Is.Anything, Arg<System.IO.FileMode>.Is.Anything)).Return(new MemoryStream(_memStreamStorage));
		}

		protected override void BecauseOf() {
			_dict.Save();
		}

		[Test]
		public void it_should_load_all_words() {
			var actualWords = new List<string>();
			using(var memStream = new MemoryStream(_memStreamStorage))
			using( var reader = new StreamReader(memStream) ) {
				foreach( var word in _expectedWords ) {
					actualWords.Add(reader.ReadLine());
				}
			}
			actualWords.ShouldBeEqualTo(_expectedWords);
		}
	}
}
