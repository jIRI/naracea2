﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.CustomDictionarySpec {
	[TestFixture]
	public class when_loading_custom_dictionary : with_custom_dictionary_setup {
		ICustomDictionary _dict;
		List<string> _expectedWords;

		protected override void SetUp() {
			base.SetUp();
			_dict = new CustomDictionary(_storePath, _streamBuilder);
			_expectedWords = new List<string> {
				"no",
				"cars",
				"go"
			};
			using(var memStream = new MemoryStream())
			using(var writer = new StreamWriter(memStream)) {
				foreach( var word in _expectedWords ) {
					writer.WriteLine(word);
				}
				writer.Flush();
				_memStreamStorage = memStream.GetBuffer();
				_streamBuilder.Stub(a => a.Create(Arg<string>.Is.Anything, Arg<System.IO.FileMode>.Is.Anything)).Return(new MemoryStream(_memStreamStorage, 0, (int)memStream.Length));
			}
		}

		protected override void BecauseOf() {
			_dict.Load();
		}

		[Test]
		public void it_should_load_all_words() {
			_dict.Words.ShouldBeEqualTo(_expectedWords);
		}
	}
}
