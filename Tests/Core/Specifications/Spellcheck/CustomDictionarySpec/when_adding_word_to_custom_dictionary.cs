﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;

namespace Naracea.Core.Tests.Specifications.Spellcheck.CustomDictionarySpec {
	[TestFixture]
	public class when_adding_word_to_custom_dictionary : with_custom_dictionary_setup {
		ICustomDictionary _dict;

		protected override void SetUp() {
			base.SetUp();
			_dict = new CustomDictionary(_storePath, _streamBuilder);
		}

		protected override void BecauseOf() {
			_dict.Add("word");
		}

		[Test]
		public void it_should_add_word() {
			_dict.Words.ShouldContain("word");
		}
	}
}
