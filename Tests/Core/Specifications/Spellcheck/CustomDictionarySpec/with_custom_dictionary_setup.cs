﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core;
using Naracea.Core.Spellcheck;
using Naracea.Core.FileSystem;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Spellcheck.CustomDictionarySpec {
	public abstract class with_custom_dictionary_setup : Context {
		protected static readonly string _storePath = "storage";
		protected IStreamBuilder _streamBuilder;
		protected byte[] _memStreamStorage = new byte[1024];

		protected override void SetUp() {
			_streamBuilder = MockRepository.GenerateMock<IStreamBuilder>();
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
