﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_undo_group_begin : with_change_setup {
		IChange _change;

		protected override void SetUp() {
			base.SetUp();
			_change = new GroupBegin(new ChangeArgs(
				_author,
				_now
			));
		}

		protected override void BecauseOf() {
			_change.ExecuteUndo(_textEditor);
		}

		[Test]
		public void it_should_do_nothing() {
			_textEditor.AssertWasNotCalled(a => a.BackspaceText(0, null));
			_textEditor.AssertWasNotCalled(a => a.DeleteText(0, null));
			_textEditor.AssertWasNotCalled(a => a.InsertTextBeforePosition(0, null));
			_textEditor.AssertWasNotCalled(a => a.InsertTextBehindPosition(0, null));
			_textEditor.AssertWasNotCalled(a => a.SetTextSilently(null));
		}
	}
}
