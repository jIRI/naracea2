﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_do_delete_text : with_change_setup {
		IChange _change;
		int _expectedPosition = 17;
		string _expectedText = "text";


		protected override void SetUp() {
			base.SetUp();
			_change = new DeleteText(new WithTextChangeArgs(
				_author,
				_now,
				_expectedText,
				_expectedPosition
			));
		}

		protected override void BecauseOf() {
			_change.ExecuteDo(_textEditor);
		}

		[Test]
		public void it_should_delete_text_from_text_editor() {
			_textEditor.AssertWasCalled(a => a.DeleteText(_expectedPosition, _expectedText));
		}
	}
}
