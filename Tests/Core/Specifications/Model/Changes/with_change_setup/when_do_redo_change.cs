﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_do_redo_change : with_change_setup {
		IChange _change;
		FakeChange _expectedChange = new FakeChange();

		protected override void SetUp() {
			base.SetUp();
			_change = new Redo(new WithChangeChangeArgs(
				_author,
				_now,
				_expectedChange
			));
		}

		protected override void BecauseOf() {
			_change.ExecuteDo(_textEditor);
		}

		[Test]
		public void it_should_execute_do_on_change() {
			_expectedChange.UndoCalled.ShouldBeTrue();
		}
	}
}
