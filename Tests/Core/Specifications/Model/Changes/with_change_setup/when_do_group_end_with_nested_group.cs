﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_do_group_end_with_nested_group : with_change_setup {
		List<IChange> _changes;
		IChange _change;

		protected override void SetUp() {
			base.SetUp();
			_changes = new List<IChange>();
			_changes.AddRange(new IChange[] { 
				new FakeChange(),
				new FakeChange(),
			});
			var groupBegin = new GroupBegin(new ChangeArgs(_author, _now));
			groupBegin.SetShiftCount(3);
			_changes.Add(groupBegin);
			var groupedChanges = new IChange[] {
				new FakeChange(),
				new FakeChange(),
			};
			_changes.AddRange(groupedChanges);
			_changes.Add(new GroupEnd(new WithChangeCollectionChangeArgs(_author, _now, groupedChanges)));
			_changes.AddRange(new IChange[] { 
				new FakeChange(),
				new FakeChange(),
			});
			_change = new GroupEnd(
				new WithChangeCollectionChangeArgs(
					_author,
					_now,
					_changes
				)
			);
		}

		protected override void BecauseOf() {
			_change.ExecuteDo(_textEditor);
		}

		[Test]
		public void it_should_call_do_on_all_changes_in_collection() {
			foreach( var change in _changes ) {
				if( change is FakeChange ) {
					( change as FakeChange ).DoCalledCount.ShouldEqualTo(1);
				}
			}
		}
	}
}
