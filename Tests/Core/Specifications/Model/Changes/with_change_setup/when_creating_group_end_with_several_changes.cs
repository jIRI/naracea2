﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_creating_group_end_with_several_changes: with_change_setup {
		IEnumerable<IChange> _changes;
		IChange _change;

		protected override void SetUp() {
			base.SetUp();
			_changes = new List<IChange> {
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
			};
		}

		protected override void BecauseOf() {
			_change = new GroupEnd(
				new WithChangeCollectionChangeArgs(
					_author,
					_now,
					_changes
				)
			);
		}

		[Test]
		public void it_should_set_shift_count_to_number_of_attached_changes_plus_one_for_groupend_itself() {
			_change.ShiftCount.ShouldEqualTo(_changes.Count() + 1);
		}
	}
}
