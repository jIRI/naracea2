﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	public abstract class with_change_setup : Context {
		protected IAuthor _author;
		protected DateTimeOffset _now;
		protected ITextEditor _textEditor;

		protected override void SetUp() {
			_author = MockRepository.GenerateStub<IAuthor>();
			_textEditor = MockRepository.GenerateStub<ITextEditor>();
			_now = DateTimeOffset.UtcNow;
		}
	}
}
