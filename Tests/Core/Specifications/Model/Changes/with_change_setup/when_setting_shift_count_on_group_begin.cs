﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_setting_shift_count_on_group_begin : with_change_setup {
		IChange _change;
		
		protected override void SetUp() {
			base.SetUp();
			_change = new GroupBegin(new ChangeArgs(
				_author,
				_now
			));
		}

		protected override void BecauseOf() {
			(_change as GroupBegin).SetShiftCount(17);
		}

		[Test]
		public void it_should_set_shift_count() {
			_change.ShiftCount.ShouldEqualTo(17);
		}
	}
}
