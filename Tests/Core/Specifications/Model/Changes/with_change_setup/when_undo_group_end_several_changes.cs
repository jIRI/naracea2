﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Changes.with_change_setup {
	[TestFixture]
	public class when_undo_group_end_several_changes : with_change_setup {
		IEnumerable<IChange> _changes;
		IChange _change;

		protected override void SetUp() {
			base.SetUp();
			_changes = new List<IChange> {
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
			};
			_change = new GroupEnd(
				new WithChangeCollectionChangeArgs(
					_author,
					_now,
					_changes
				)
			);
		}

		protected override void BecauseOf() {
			_change.ExecuteUndo(_textEditor);
		}

		[Test]
		public void it_should_call_undo_on_all_changes_in_collection() {
			foreach( var change in _changes ) {
				change.AssertWasCalled(a => a.ExecuteUndo(_textEditor));
			}
		}
	}
}
