﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Model.Branches.with_expectations_on_branch_changes_when_getting_indices_of_changes {
	[TestFixture]
	public class when_getting_indexof_changes_in_3rd_level_branch : with_expectations_on_branch_changes_when_getting_indices_of_changes {
		protected override void SetupBranch() {
			var data = SampleData.BranchWithParents.GetData();
			_branch = data;
			int pos = 0;
			for( int i = 0; i <= _branch.Parent.BranchingChangeIndex; i++ ) {
				_changes.Add(_branch.Parent.Parent.Changes.OwnedChanges.ElementAt(pos++));
			}
			pos = 0;
			for( int i = 0; i < _branch.BranchingChangeIndex - _branch.Parent.BranchingChangeIndex; i++ ) {
				_changes.Add(_branch.Parent.Changes.OwnedChanges.ElementAt(pos++));
			}
			foreach( var change in _branch.Changes.OwnedChanges ) {
				_changes.Add(change);
			}
		}
	}
}
