﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests;

namespace Naracea.Core.Tests.Specifications.Model.Branches.with_expectations_on_branch_changes_when_getting_indices_of_changes {
	public abstract class with_expectations_on_branch_changes_when_getting_indices_of_changes : Context {
		protected IBranch _branch;
		protected List<IChange> _changes = new List<IChange>();
		protected List<int> _actualIndices = new List<int>();


		protected override void SetUp() {
			this.SetupBranch();
		}

		protected override void BecauseOf() {
			foreach( var change in _changes) {
				_actualIndices.Add(_branch.Changes.IndexOf(change));
			}
		}

		abstract protected void SetupBranch();

		[Test]
		public void it_should_return_indices_in_ascending_order() {
			_actualIndices.ShouldBeEqualTo(Enumerable.Range(0, _changes.Count).ToList());
		}
	}
}
