﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Model.Branches.with_expectations_on_branch_changes_when_crawling_changes {
	[TestFixture]
	public class when_iterating_through_changes_in_2nd_level_branch : with_expectations_on_branch_changes_when_crawling_changes {
		protected override void SetupBranch() {
			var data = SampleData.BranchWithParents.GetData();
			_branch = data.Parent;
			for( int i = 0; i <= _branch.BranchingChangeIndex; i++ ) {
				_expectedChanges.Add(_branch.Parent.Changes.OwnedChanges.ElementAt(i));			
			}
			foreach( var change in _branch.Changes.OwnedChanges ) {
				_expectedChanges.Add(change);
			}
		}
	}
}
