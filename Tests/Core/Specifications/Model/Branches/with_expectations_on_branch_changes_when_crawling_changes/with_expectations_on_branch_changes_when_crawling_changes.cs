﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;
using Naracea.Core.Shifter;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests;

namespace Naracea.Core.Tests.Specifications.Model.Branches.with_expectations_on_branch_changes_when_crawling_changes {
	public abstract class with_expectations_on_branch_changes_when_crawling_changes : Context {
		protected IBranch _branch;
		protected List<IChange> _expectedChanges = new List<IChange>();
		protected List<IChange> _actualChanges = new List<IChange>();


		protected override void SetUp() {
			this.SetupBranch();
		}

		protected override void BecauseOf() {
			for( int i = 0; i < _branch.Changes.Count; i++ ) {
				_actualChanges.Add(_branch.Changes.At(i));
			}
		}

		abstract protected void SetupBranch();

		[Test]
		public void it_should_walk_though_changes_properly() {
			_actualChanges.ShouldBeEqualTo(_expectedChanges);
		}
	}
}
