﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Model.Changes;
using Rhino.Mocks;

namespace Naracea.Core.Tests.Specifications.Model.Documents {
	[TestFixture]
	public class when_getting_next_branch_id_on_document_with_preset_counter : with_document_setup {
		Naracea.Core.Model.Document _document;
		const int _expectedStart = 7;
		const int _expectedCount = 10;
		List<int> _expectedIds;
		List<int> _actualIds = new List<int>();

		protected override void SetUp() {
			base.SetUp();
			_document = new Naracea.Core.Model.Document();
			_document.Metadata.Set(Naracea.Core.Model.Document.BranchCounterKey, _expectedStart.ToString());
			_expectedIds = Enumerable.Range(_expectedStart, _expectedCount).ToList();
		}

		protected override void BecauseOf() {
			for( int i = 0; i < _expectedCount; i++ ) {
				_actualIds.Add(_document.GetNextBranchId());
			}
		}

		[Test]
		public void it_should_generate_proper_sequence() {
			_actualIds.ShouldBeEqualTo(_expectedIds);
		}
	}
}
