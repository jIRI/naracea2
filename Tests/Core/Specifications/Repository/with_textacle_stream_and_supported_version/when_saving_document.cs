﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;
using System.IO;


namespace Naracea.Core.Tests.Specifications.Repository.with_textacle_stream_and_supported_version {
	[TestFixture]
	public class when_saving_document : with_textacle_stream_and_supported_version {
		protected override void SetUp() {
			base.SetUp();
			_repo = new DocumentRepository(_repoContext, _persisterBuilder);
		}

		protected override void BecauseOf() {
			_repo.Save(_expectedDocument);
		}

		[Test]
		public void it_should_use_file_writer_to_write_the_document() {
			_writer.AssertWasCalled(a => a.WriteBody(Arg<Stream>.Is.Anything, Arg.Is(_expectedDocument)));
		}
	}
}
