﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using System.IO;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Tests.Specifications.Repository.with_textacle_stream_and_supported_version {
	public abstract class with_textacle_stream_and_supported_version : Context {
		protected MemoryStream _stream;
		protected IRepositoryContext _repoContext;
		protected string _expectedFilename;
		protected Version _expectedVersion = new Version(1, 0);
		protected IPersisterBuilder _persisterBuilder;
		protected IDocument _expectedDocument;
		protected IFileReader _reader;
		protected IFileWriter _writer;
		protected IRepository<IDocument> _repo;


		protected override void SetUp() {
			//non-empty stream
			_stream = new MemoryStream(new byte[] {0});
			_expectedFilename = "file.ncd";
			
			//stub repo context
			_repoContext = MockRepository.GenerateStub<IRepositoryContext>();
			_repoContext.Path = _expectedFilename;
			//stub stream builder
			var streamBuilder = MockRepository.GenerateStub<IFileStreamBuilder>();
			streamBuilder.Stub(a => a.CreatePlainStream(_expectedFilename)).Return(_stream);
			_repoContext.Stub(a => a.StreamBuilder).Return(streamBuilder);
			//stub header reader
			var header = MockRepository.GenerateStub<IFileHeader>();
			header.Stub(a => a.Version).Return(_expectedVersion);
			var headerReader = MockRepository.GenerateStub<IFileHeaderReader>();
			headerReader.Stub(a => a.Read(Arg<Stream>.Is.Anything)).Return(header);
			_repoContext.Stub(a => a.FileHeaderReader).Return(headerReader);
			_repoContext.RequiredFormatVersion = null;
			//document
			_expectedDocument = MockRepository.GenerateStub<IDocument>();
			//file reader
			_reader = MockRepository.GenerateStub<IFileReader>();
			_reader.Stub(a => a.ReadBody(Arg<Stream>.Is.Anything)).Return(_expectedDocument);
			//file writer
			_writer = MockRepository.GenerateStub<IFileWriter>();
			_writer.Stub(a => a.WriteBody(Arg<Stream>.Is.Anything, Arg.Is(_expectedDocument)));
			//persister builder
			_persisterBuilder = MockRepository.GenerateStub<IPersisterBuilder>();
			_persisterBuilder.Stub(a => a.CurrentVersion).Return(_expectedVersion);
			_persisterBuilder.Stub(a => a.SupportedVersions).Return(new Version[] { _expectedVersion });
			_persisterBuilder.Stub(a => a.BuildReader(Arg<Version>.Is.Anything)).Return(_reader);
			_persisterBuilder.Stub(a => a.BuildWriter(Arg<Version>.Is.Anything)).Return(_writer);
		}

		protected override void CustomTearDown() {
			if( _stream != null ) {
				_stream.Dispose();
			}
			if( _repo != null ) {
				_repo.Dispose();
			}
		}
	}
}
