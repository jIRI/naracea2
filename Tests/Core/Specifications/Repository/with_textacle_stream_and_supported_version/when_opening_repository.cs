﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;


namespace Naracea.Core.Tests.Specifications.Repository.with_textacle_stream_and_supported_version {
	[TestFixture]
	public class when_opening_repository : with_textacle_stream_and_supported_version {
		protected override void BecauseOf() {
			_repo = new DocumentRepository(_repoContext, _persisterBuilder);
		}

		[Test]
		public void it_should_request_stream_from_builder() {
			_repoContext.StreamBuilder.AssertWasCalled(a => a.CreatePlainStream(_expectedFilename));
		}

		[Test]
		public void it_should_use_read_version() {
			_repo.FormatVersion.ShouldEqualTo(_expectedVersion);
		}
	}
}
