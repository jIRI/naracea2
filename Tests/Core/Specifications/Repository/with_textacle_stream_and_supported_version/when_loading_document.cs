﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;
using System.IO;


namespace Naracea.Core.Tests.Specifications.Repository.with_textacle_stream_and_supported_version {
	[TestFixture]
	public class when_loading_document : with_textacle_stream_and_supported_version {
		IDocument _actualDocument;

		protected override void SetUp() {
			base.SetUp();
			_repo = new DocumentRepository(_repoContext, _persisterBuilder);
		}

		protected override void BecauseOf() {
			_actualDocument = _repo.Load();
		}

		[Test]
		public void it_should_use_file_reader_to_read_the_document() {
			_reader.AssertWasCalled(a => a.ReadBody(Arg<Stream>.Is.Anything));
		}

		[Test]
		public void it_should_return_expected_document() {
			_actualDocument.ShouldBeSameAs(_expectedDocument);
		}
	}
}
