﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using Naracea.Core.Repository.PersistenceModel.V1;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_V1contentreader_reads_content : with_stream {
		IFileContentReader<IDocumentDtoV1> _reader;
		ISerializer _serializer;
		DocumentDtoV1 _expectedContent;
		IDocumentDtoV1 _actualContent;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_expectedContent = new DocumentDtoV1();
			_serializer = MockRepository.GenerateStub<ISerializer>();
			_serializer.Stub(a => a.Deserialize<DocumentDtoV1>(_stream)).Return(_expectedContent);
			_reader = new FileContentReaderV1(_serializer);
		}

		protected override void BecauseOf() {
			_actualContent = _reader.Read(_stream);
		}

		[Test]
		public void it_should_use_deserializer_to_deserialize_content_from_the_stream() {
			_serializer.AssertWasCalled(a => a.Deserialize<DocumentDtoV1>(_stream));
		}

		[Test]
		public void it_should_use_return_deserialized_content() {
			_actualContent.ShouldBeSameAs(_expectedContent);
		}
	}
}
