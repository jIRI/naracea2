﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_metadatareader_reads_metadata : with_stream {
		IFileMetadataReader _reader;
		IFileMetadata _actualMetadata;
		IFileMetadata _expectedMetadata;
		ISerializer _serializer;
		List<MetadataKeyValuePair> _expectedMetadaCollection;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			//expected collection of metadata
			_expectedMetadaCollection = new List<MetadataKeyValuePair>();
			_expectedMetadata = MockRepository.GenerateStub<IFileMetadata>();
			//serializer
			_serializer = MockRepository.GenerateStub<ISerializer>();
			_serializer.Stub(a => a.Deserialize<List<MetadataKeyValuePair>>(Arg<MemoryStream>.Is.Anything)).Return(_expectedMetadaCollection);
			//reader
			_reader = new FileMetadataReader(_serializer, () => _expectedMetadata);
		}

		protected override void BecauseOf() {
			_actualMetadata = _reader.Read(_stream);
		}

		[Test]
		public void it_should_use_serializer_to_deserialize_all_items_from_the_stream() {
			_serializer.AssertWasCalled(a => a.Deserialize<List<MetadataKeyValuePair>>(Arg<MemoryStream>.Is.Anything));
		}

		[Test]
		public void it_should_return_deserialized_metadata() {
			_actualMetadata.ShouldBeSameAs(_expectedMetadata);
		}

		[Test]
		public void it_should_add_values_to_the_metadata_collection() {
			_actualMetadata.AssertWasCalled(a => a.SetValues(_expectedMetadaCollection));
		}
	}
}
