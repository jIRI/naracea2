﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_headerreader_reads_invalid_file : with_stream {
		IFileHeaderReader _headerReader;
		IFileHeader _actualHeader;
		bool _factoryCalled;

		protected override void SetUp() {
			base.SetUp();
			//setup buffer
			var buffer = "This is some sample text.";
			var byteBuffer = from c in buffer
											 select (byte)c;
			//create stream
			_stream = new MemoryStream(byteBuffer.ToArray());
			//create heared reader
			_headerReader = new FileHeaderReader((ver) => {
				_factoryCalled = true;
				return _actualHeader;
			});
		}

		protected override void BecauseOf() {
			_actualHeader = _headerReader.Read(_stream);
		}

		[Test]
		public void it_should_not_create_new_fileheader() {
			_factoryCalled.ShouldBeFalse();
		}

		[Test]
		public void it_should_return_null() {
			_actualHeader.ShouldBeNull();
		}
	}
}
