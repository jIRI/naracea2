﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using Naracea.Core.Repository.PersistenceModel.V1;
using Naracea.Core.Model;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_V1filereader_reads_file : with_stream {
		IFileReader _reader;
		IFileHeaderReader _headerReader;
		IFileMetadataReader _metadataReader;
		IFileContentReader<IDocumentDtoV1> _contentReader;
		IDocumentMapper<IDocumentDtoV1> _mapper;
		IDocument _expectedDocument;
		IDocument _actualDocument;
		IFileMetadata _expectedMetadata;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_expectedDocument = MockRepository.GenerateStub<IDocument>();
			//header
			_headerReader = MockRepository.GenerateStub<IFileHeaderReader>();
			var header = MockRepository.GenerateStub<IFileHeader>();
			_headerReader.Stub(a => a.Read(Arg<Stream>.Is.Anything)).Return(header);
			//metadata
			_metadataReader = MockRepository.GenerateStub<IFileMetadataReader>();
			_expectedMetadata = MockRepository.GenerateStub<IFileMetadata>();
			_metadataReader.Stub(a => a.Read(_stream)).Return(_expectedMetadata);
			//content
			_contentReader = MockRepository.GenerateStub<IFileContentReader<IDocumentDtoV1>>();
			var docDto = MockRepository.GenerateStub<IDocumentDtoV1>();
			_contentReader.Stub(a => a.Read(_stream)).Return(docDto);
			//mapper
			_mapper = MockRepository.GenerateStub<IDocumentMapper<IDocumentDtoV1>>();
			_mapper.Stub(a => a.DomainFromData(docDto, _expectedMetadata)).Return(_expectedDocument);
			//reader
			_reader = new FileReaderV1(_headerReader, _metadataReader, _contentReader, _mapper);
		}

		protected override void BecauseOf() {
			_actualDocument = _reader.ReadBody(_stream);
		}

		[Test]
		public void it_should_return_deserialized_document() {
			_actualDocument.ShouldBeSameAs(_expectedDocument);
		}
	}
}
