﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	public abstract class with_stream : Context {
		protected MemoryStream _stream;

		protected override void SetUp() {
		}

		protected override void CustomTearDown() {
			if( _stream != null ) {
				_stream.Dispose();
			}
		}
	}
}
