﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;
using Naracea.Core.Repository.PersistenceModel.V1;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_V1contentwriter_writes_content : with_stream {
		IFileContentWriter<IDocumentDtoV1> _writer;
		ISerializer _serializer;
		IDocumentDtoV1 _expectedContent;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_expectedContent = MockRepository.GenerateStub<IDocumentDtoV1>();
			_serializer = MockRepository.GenerateStub<ISerializer>();
			//here it gets tricky. since we need to pass real type to serializer now, we will get null for content because casting the mock to DTO will return null
			_serializer.Stub(a => a.Serialize(Arg<MemoryStream>.Is.Same(_stream), Arg<DocumentDtoV1>.Is.Null));
			_writer = new FileContentWriterV1(_serializer);
		}

		protected override void BecauseOf() {
			_writer.Write(_stream, _expectedContent);
		}

		[Test]
		public void it_should_use_serializer_to_serialize_content_into_the_stream() {
			//here it gets tricky. since we need to pass real type to serializer now, we will get null for content because casting the mock to DTO will return null
			_serializer.AssertWasCalled(a => a.Serialize(Arg<MemoryStream>.Is.Same(_stream), Arg<DocumentDtoV1>.Is.Null));
		}
	}
}
