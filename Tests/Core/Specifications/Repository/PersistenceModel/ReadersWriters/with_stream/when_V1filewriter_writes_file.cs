﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;
using Naracea.Core.Repository.PersistenceModel.V1;
using Naracea.Core.Model;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_V1filewriter_writes_file : with_stream {
		IFileWriter _writer;
		IFileHeader _header;
		IFileHeaderWriter _headerWriter;
		IFileMetadataWriter _metadataWriter;
		IFileContentWriter<IDocumentDtoV1> _contentWriter;
		IDocumentMapper<IDocumentDtoV1> _mapper;
		IDocument _document;
		IDocumentDtoV1 _documentDto;
		IFileMetadata _metadata;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_document = MockRepository.GenerateStub<IDocument>();
			//header
			_header = MockRepository.GenerateStub<IFileHeader>();
			_headerWriter = MockRepository.GenerateStub<IFileHeaderWriter>();
			//metadata
			_metadataWriter = MockRepository.GenerateStub<IFileMetadataWriter>();
			_metadata = MockRepository.GenerateStub<IFileMetadata>();
			//content
			_contentWriter = MockRepository.GenerateStub<IFileContentWriter<IDocumentDtoV1>>();
			var docDto = MockRepository.GenerateStub<IDocumentDtoV1>();
			_contentWriter.Stub(a => a.Write(_stream, docDto));
			//mapper
			_mapper = MockRepository.GenerateStub<IDocumentMapper<IDocumentDtoV1>>();
			_documentDto = MockRepository.GenerateStub<IDocumentDtoV1>();
			_mapper.Stub(a => a.DataFromDomain(Arg.Is(_document), out Arg<IFileMetadata>.Out(_metadata).Dummy)).Return(_documentDto);
			//writer
			_writer = new FileWriterV1(_header, _headerWriter, _metadataWriter, _contentWriter, _mapper);
		}

		protected override void BecauseOf() {
			_writer.WriteHeader(_stream);
			_writer.WriteBody(_stream, _document);
		}

		[Test]
		public void it_should_write_header() {
			_headerWriter.AssertWasCalled(a => a.Write(_stream, _header));
		}

		[Test]
		public void it_should_write_metadata() {
			_metadataWriter.AssertWasCalled(a => a.Write(_stream, _metadata));
		}

		[Test]
		public void it_should_write_filecontent() {
			_contentWriter.AssertWasCalled(a => a.Write(_stream, _documentDto));
		}
	}
}
