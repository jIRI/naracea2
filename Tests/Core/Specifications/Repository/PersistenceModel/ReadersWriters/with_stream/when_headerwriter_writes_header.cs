﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_headerwriter_writes_header : with_stream {
		IFileHeaderWriter _headerWriter;
		IFileHeader _header;
		string _expectedFormatIdentifier;
		Version _expectedVersion;
		byte[] _buffer;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_expectedFormatIdentifier = PersistenceModelConstants.FileFormatIdentifier;
			_expectedVersion = new Version(13, 17);
			_header = MockRepository.GenerateStub<IFileHeader>();
			_header.Stub(a => a.FormatIdentifier).Return(_expectedFormatIdentifier);
			_header.Stub(a => a.Version).Return(_expectedVersion);
			_headerWriter = new FileHeaderWriter();
		}

		protected override void BecauseOf() {
			_headerWriter.Write(_stream, _header);
			_buffer = _stream.ToArray();
		}

		[Test]
		public void it_should_write_fileformat_descriptor() {
			int _formatIdentifierPosition = 0;
			foreach( var c in _expectedFormatIdentifier ) {
				var buffc = (char)_buffer[_formatIdentifierPosition];
				buffc.ShouldEqualTo(_expectedFormatIdentifier[_formatIdentifierPosition]);
				_formatIdentifierPosition++;
			}
		}

		[Test]
		public void it_should_write_fileformat_version() {
			int _versionPosition = _expectedFormatIdentifier.Length;
			char[] _versionChars = new char[PersistenceModelConstants.VersionDescriptorLength];
			for( int i = 0; i < PersistenceModelConstants.VersionDescriptorLength; i++ ) {
				_versionChars[i] = (char)_buffer[_versionPosition + i];
			}
			var actualVersionString = new string(_versionChars);
			var actualVersion = new Version(actualVersionString);
			actualVersion.ShouldEqualTo(_expectedVersion);
		}
	}
}
