﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_headerreader_reads_header : with_stream {
		IFileHeaderReader _headerReader;
		IFileHeader _actualHeader;
		string _expectedFormatIdentifier;
		Version _expectedVersion;
		Version _actualVersion;

		protected override void SetUp() {
			base.SetUp();
			_expectedFormatIdentifier = PersistenceModelConstants.FileFormatIdentifier;
			_expectedVersion = new Version(13, 17);

			//setup buffer
			var buffer = _expectedFormatIdentifier
				+ string.Format("{0}.{1}", _expectedVersion.Major, _expectedVersion.Minor)
						.PadRight(PersistenceModelConstants.VersionDescriptorLength);
			var byteBuffer = from c in buffer
											 select (byte)c;
			//create stream
			_stream = new MemoryStream(byteBuffer.ToArray());
			//create heared reader
			_headerReader = new FileHeaderReader((ver) => {
				_actualVersion = ver;
				return _actualHeader;
			});
		}

		protected override void BecauseOf() {
			_actualHeader = _headerReader.Read(_stream);
		}

		[Test]
		public void it_should_create_new_fileheader_with_correct_version() {
			_actualVersion.ShouldEqualTo(_expectedVersion);
		}
	}
}
