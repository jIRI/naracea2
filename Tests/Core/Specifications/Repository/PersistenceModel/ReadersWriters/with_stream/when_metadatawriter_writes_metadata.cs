﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using System.IO;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.ReadersWriters.with_stream {
	[TestFixture]
	public class when_metadatawriter_writes_metadata : with_stream {
		IFileMetadataWriter _writer;
		IFileMetadata _metadata;
		ISerializer _serializer;
		List<MetadataKeyValuePair> _expectedMetadaCollection;

		protected override void SetUp() {
			base.SetUp();
			_stream = new MemoryStream();
			_expectedMetadaCollection = new List<MetadataKeyValuePair>();
			_metadata = MockRepository.GenerateStub<IFileMetadata>();
			_metadata.Stub(a => a.Data).Return(_expectedMetadaCollection);
			_serializer = MockRepository.GenerateStub<ISerializer>();
			//since we pre-serialize metadata, and we need real object, we don't use the _stream and _expectedMetadaCollection in call to serializer
			_serializer.Stub(a => a.Serialize(Arg<MemoryStream>.Is.Anything, Arg<List<MetadataKeyValuePair>>.Is.NotNull));
			_writer = new FileMetadataWriter(_serializer);
		}

		protected override void BecauseOf() {
			_writer.Write(_stream, _metadata);
		}

		[Test]
		public void it_should_use_serializer_to_serialize_all_items_into_the_stream() {
			//since we pre-serialize metadata, and we need real object, we don't use the _stream and _expectedMetadaCollection in call to serializer
			_serializer.AssertWasCalled(a => a.Serialize(Arg<MemoryStream>.Is.Anything, Arg<List<MetadataKeyValuePair>>.Is.NotNull));
		}
	}
}
