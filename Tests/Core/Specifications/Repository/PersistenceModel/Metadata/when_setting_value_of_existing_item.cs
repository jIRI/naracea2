﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Extensions;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_setting_value_of_existing_item : with_metadata_setup {
		IFileMetadata _metadata;
		string _expectedKey;
		string _expectedValue;

		protected override void SetUp() {
			base.SetUp();
			_expectedKey = "key";
			_expectedValue = "new value";
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
			_metadata.SetValues(new IMetadataKeyValuePair[] {
				new MetadataKeyValuePair {
					Key = _expectedKey,
					Value = "original value"
				}
			});
		}

		protected override void BecauseOf() {
			_metadata.SetValue(_expectedKey, _expectedValue);
		}

		[Test]
		public void it_should_change_value_for_given_key() {
			_metadata.Data.Any(a => a.Key == _expectedKey && a.Value == _expectedValue).ShouldBeTrue();
		}
	}
}
