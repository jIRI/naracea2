﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_trying_to_get_existing_item : with_metadata_setup {
		IFileMetadata _metadata;
		bool _result;
		string _expectedKey;
		string _expectedValue;
		string _actualValue;

		protected override void SetUp() {
			base.SetUp();
			_expectedKey = "key";
			_expectedValue = "value";
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
			_metadata.SetValue(_expectedKey, _expectedValue);
		}

		protected override void BecauseOf() {
			_result = _metadata.TryGetValue(_expectedKey, out _actualValue);
		}

		[Test]
		public void it_should_return_true() {
			_result.ShouldBeTrue();
		}

		[Test]
		public void it_should_set_value_to_expected_value() {
			_actualValue.ShouldBeEqualTo(_expectedValue);
		}
	}
}
