﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_trying_to_get_item_with_null_key : with_metadata_setup {
		IFileMetadata _metadata;
		bool _result;
		string _expectedValue;

		protected override void SetUp() {
			base.SetUp();
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
		}

		protected override void BecauseOf() {
			_result = _metadata.TryGetValue(null, out _expectedValue);
		}

		[Test]
		public void it_should_return_false() {
			_result.ShouldBeFalse();
		}

		[Test]
		public void it_should_set_value_to_null() {
			_expectedValue.ShouldBeNull();
		}
	}
}
