﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_getting_data_property : with_metadata_setup {
		IFileMetadata _metadata;
		IEnumerable<IMetadataKeyValuePair> _result;
		IEnumerable<IMetadataKeyValuePair> _expectedResult;

		protected override void SetUp() {
			base.SetUp();
			_expectedResult = new IMetadataKeyValuePair[] {
				new MetadataKeyValuePair{
					Key = "1", 
					Value = "text"
				},
				new MetadataKeyValuePair {
					Key = "2",
					Value = 42.ToString()
				},
				new MetadataKeyValuePair {
					Key = "3", 
					Value = 23.5.ToString()
				}
			};
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
			_metadata.SetValues(_expectedResult);
		}

		protected override void BecauseOf() {
			_result = _metadata.Data;
		}

		[Test]
		public void it_should_return_false() {
			_result.ShouldBeEquivalentTo(_expectedResult);
		}
	}
}
