﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_setting_value_with_null_key : with_metadata_setup {
		IFileMetadata _metadata;
		bool _wasArgumentNullException = false;

		protected override void SetUp() {
			base.SetUp();
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
		}

		protected override void BecauseOf() {
			try {
				_metadata.SetValue(null, "this will be ignored");
			} catch( ArgumentNullException ) {
				_wasArgumentNullException = true;
			}
		}

		[Test]
		public void it_should_throw_argument_null_exception() {
			_wasArgumentNullException.ShouldBeTrue();
		}
	}
}
