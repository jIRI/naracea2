﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	public abstract class with_metadata_setup : Context {
		protected Func<IMetadataKeyValuePair> _metadataKeyValuePairFactory;
			
		protected override void  SetUp() {
			_metadataKeyValuePairFactory = () => new MetadataKeyValuePair();
		}
	}
}
