﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Extensions;

namespace Naracea.Core.Tests.Specifications.Repository.PersistenceModel.Metadata {
	[TestFixture]
	public class when_setting_value_of_missing_item : with_metadata_setup {
		IFileMetadata _metadata;
		string _expectedKey;
		string _expectedValue;

		protected override void SetUp() {
			base.SetUp();
			_metadata = new FileMetadata(_metadataKeyValuePairFactory);
			_expectedKey = "new key";
			_expectedValue = "some string value";
		}

		protected override void BecauseOf() {
			_metadata.SetValue(_expectedKey, _expectedValue);
		}

		[Test]
		public void it_should_add_key_and_value_to_the_data() {
			_metadata.Data.Any(a => a.Key == _expectedKey && a.Value.As<string>() == _expectedValue).ShouldBeTrue();
		}
	}
}
