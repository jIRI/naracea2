﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository.Exceptions;


namespace Naracea.Core.Tests.Specifications.Repository.with_non_textacle_stream {
	[TestFixture]
	public class when_opening_repository : with_non_textacle_stream {
		bool _exceptionThrown;

		protected override void BecauseOf() {
			try {
				_repo = new DocumentRepository(_repoContext, _persisterBuilder);
			} catch( RepositoryException) {
				_exceptionThrown = true;
			}
		}

		[Test]
		public void it_should_request_stream_from_builder() {
			_repoContext.StreamBuilder.AssertWasCalled(a => a.CreatePlainStream(_expectedFilename));
		}

		[Test]
		public void it_should_throw_exception() {
			_exceptionThrown.ShouldBeTrue();
		}
	}
}
