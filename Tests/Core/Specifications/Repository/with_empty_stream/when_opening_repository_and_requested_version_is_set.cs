﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;

namespace Naracea.Core.Tests.Specifications.Repository.with_empty_stream {
	[TestFixture]
	public class when_opening_repository_and_requested_version_is_set : with_empty_stream {
		protected override void SetUp() {
			base.SetUp();
			//stub header reader
			_repoContext.RequiredFormatVersion = _expectedVersion;
		}

		protected override void BecauseOf() {
			_repo = new DocumentRepository(_repoContext, _persisterBuilder);
		}

		[Test]
		public void it_should_request_stream_from_builder() {
			_repoContext.StreamBuilder.AssertWasCalled(a => a.CreatePlainStream(_expectedFilename));
		}

		[Test]
		public void it_should_use_requested_version() {
			_repo.FormatVersion.ShouldEqualTo(_expectedVersion);
		}
	}
}
