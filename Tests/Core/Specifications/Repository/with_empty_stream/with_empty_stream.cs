﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using System.IO;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;

namespace Naracea.Core.Tests.Specifications.Repository.with_empty_stream  {
	public abstract class with_empty_stream : Context {
		protected MemoryStream _stream;
		protected IRepositoryContext _repoContext;
		protected string _expectedFilename;
		protected Version _expectedVersion = new Version(1, 0);
		protected IPersisterBuilder _persisterBuilder;
		protected IRepository<IDocument> _repo;

		protected override void SetUp() {
			_stream = new MemoryStream();
			_expectedFilename = "file.ncd";
			//stub repo context
			_repoContext = MockRepository.GenerateStub<IRepositoryContext>();
			_repoContext.Path = _expectedFilename;
			//stub stream builder
			var streamBuilder = MockRepository.GenerateStub<IFileStreamBuilder>();
			streamBuilder.Stub(a => a.CreatePlainStream(_expectedFilename)).Return(_stream);
			_repoContext.Stub(a => a.StreamBuilder).Return(streamBuilder);
			//persister builder
			_persisterBuilder = MockRepository.GenerateStub<IPersisterBuilder>();
			_persisterBuilder.Stub(a => a.CurrentVersion).Return(_expectedVersion);
			_persisterBuilder.Stub(a => a.SupportedVersions).Return(new Version[] {_expectedVersion});
		}

		protected override void CustomTearDown() {
			if( _stream != null ) {
				_stream.Dispose();
			}
			if( _repo != null ) {
				_repo.Dispose();
			}
		}
	}
}
