﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Repository;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository.Exceptions;


namespace Naracea.Core.Tests.Specifications.Repository.with_textacle_stream_and_unsupported_version {
	[TestFixture]
	public class when_opening_repository : with_textacle_stream_and_unsupported_version {
		IRepository<IDocument> _repo;
		bool _exceptionThrown;

		protected override void BecauseOf() {
			try {
				_repo = new DocumentRepository(_repoContext, _persisterBuilder);
			} catch( RepositoryException) {
				_exceptionThrown = true;
			}
		}

		[Test]
		public void it_should_request_stream_from_builder() {
			_repoContext.StreamBuilder.AssertWasCalled(a => a.CreatePlainStream(_expectedFilename));
		}

		[Test]
		public void it_should_throw_exception() {
			_exceptionThrown.ShouldBeTrue();
		}
	}
}
