﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using NUnit.Framework;
using Rhino.Mocks;
using Autofac;
using Naracea.View.Context;
using Naracea.View;
using Naracea.Core;

namespace Naracea.Core.Tests.Specifications.TextEditorProxyTests {
	public abstract class with_text_editor_proxy : Context {
		protected ITextEditor _textEditor;

		protected override void SetUp() {
			_textEditor = MockRepository.GenerateStub<ITextEditor>();			
		}

		protected override void CustomTearDown() {
			base.CustomTearDown();
		}
	}
}
