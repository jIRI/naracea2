﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;


namespace Naracea.Core.Tests.Specifications.TextEditorProxyTests {
	[TestFixture]
	public class when_handles_text_backspace_with_editing_mode : with_text_editor_proxy {
		ITextEditorProxy _proxy;
		protected override void SetUp() {
			base.SetUp();
			_proxy = new TextEditorProxy(_textEditor);
			_proxy.Text = "123";
		}

		protected override void BecauseOf() {
			_proxy.BeginEdit();
			_proxy.BackspaceText(1, "2");
			_proxy.EndEdit();
		}

		[Test]
		public void it_should_insert_text_to_proper_places() {
			_proxy.Text.ShouldEqualTo("13");
		}

		[Test]
		public void it_should_set_caret_position() {
			_proxy.CaretPosition.ShouldEqualTo(1);
		}

		[Test]
		public void it_should_not_be_in_edition_mode_after() {
			_proxy.IsEditing.ShouldEqualTo(false);
		}

		[Test]
		public void it_should_commit_the_text_to_the_editor() {
			_textEditor.AssertWasCalled(a => a.SetTextSilently("13"));
		}

		[Test]
		public void it_should_commit_the_caret_position_to_the_editor() {
			_textEditor.CaretPosition.ShouldEqualTo(1);
		}
	}
}
