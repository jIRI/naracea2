﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;

namespace Naracea.Core.Tests.Specifications.TextEditorProxyTests {
	[TestFixture]
	public class when_handles_text_insert_with_editing_mode : with_text_editor_proxy {
		ITextEditorProxy _proxy;
		int _caretPos1;
		int _caretPos2;

		protected override void SetUp() {
			base.SetUp();
			_proxy = new TextEditorProxy(_textEditor);
			_proxy.Text = "123";
		}

		protected override void BecauseOf() {
			_proxy.BeginEdit();
			_proxy.InsertTextBeforePosition(1, "a");
			_caretPos1 = _proxy.CaretPosition;
			_proxy.InsertTextBehindPosition(2, "b");
			_caretPos2 = _proxy.CaretPosition;
			_proxy.EndEdit();
		}

		[Test]
		public void it_should_insert_text_to_proper_places() {
			_proxy.Text.ShouldBeEqualTo("1ab23");
		}

		[Test]
		public void it_should_set_caret_position() {
			_caretPos1.ShouldEqualTo(2);
			_caretPos2.ShouldEqualTo(2);
		}

		[Test]
		public void it_should_not_be_in_edition_mode_after() {
			_proxy.IsEditing.ShouldEqualTo(false);
		}

		[Test]
		public void it_should_not_update_text_editor() {
			_textEditor.AssertWasNotCalled(a => a.InsertTextBeforePosition(1, "a"));
			_textEditor.AssertWasNotCalled(a => a.InsertTextBehindPosition(2, "b"));
		}

		[Test]
		public void it_should_commit_the_text_to_the_editor() {
			_textEditor.AssertWasCalled(a => a.SetTextSilently("1ab23"));
		}

		[Test]
		public void it_should_commit_the_caret_position_to_the_editor() {
			_textEditor.CaretPosition.ShouldEqualTo(2);
		}
	}
}
