﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;

namespace Naracea.Core.Tests.Specifications.TextEditorProxyTests {
	[TestFixture]
	public class when_handles_text_delete_without_editing_mode : with_text_editor_proxy {
		ITextEditorProxy _proxy;
		protected override void SetUp() {
			base.SetUp();
			_proxy = new TextEditorProxy(_textEditor);
			_proxy.Text = "123";
		}

		protected override void BecauseOf() {
			_proxy.DeleteText(1, "2");
		}

		[Test]
		public void it_should_delete_given_text() {
			_proxy.Text.ShouldEqualTo("13");
		}

		[Test]
		public void it_should_set_caret_position() {
			_proxy.CaretPosition.ShouldEqualTo(1);
		}

		[Test]
		public void it_should_update_text_editor() {
			_textEditor.AssertWasCalled(a => a.DeleteText(1, "2"));
		}
	}
}
