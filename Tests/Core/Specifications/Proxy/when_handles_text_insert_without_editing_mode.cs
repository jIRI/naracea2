﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.View;
using Naracea.Core;

namespace Naracea.Core.Tests.Specifications.TextEditorProxyTests {
	[TestFixture]
	public class when_handles_text_insert_without_editing_mode : with_text_editor_proxy {
		ITextEditorProxy _proxy;
		int _caretPos1;
		int _caretPos2;

		protected override void SetUp() {
			base.SetUp();
			_proxy = new TextEditorProxy(_textEditor);
			_proxy.Text = "123";
		}

		protected override void BecauseOf() {
			_proxy.InsertTextBeforePosition(1, "a");
			_caretPos1 = _proxy.CaretPosition;
			_proxy.InsertTextBehindPosition(2, "b");
			_caretPos2 = _proxy.CaretPosition;
		}

		[Test]
		public void it_should_insert_text_to_proper_places() {
			_proxy.Text.ShouldBeEqualTo("1ab23");
		}

		[Test]
		public void it_should_set_caret_position() {
			_caretPos1.ShouldEqualTo(2);
			_caretPos2.ShouldEqualTo(2);
		}

		[Test]
		public void it_should_update_text_editor() {
			_textEditor.AssertWasCalled(a => a.InsertTextBeforePosition(1, "a"));
			_textEditor.AssertWasCalled(a => a.InsertTextBehindPosition(2, "b"));
		}
	}
}
