﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	[TestFixture]
	public class when_finding_datetime_with_time_slightly_lower : with_some_changes_with_timestamps {
		IDateTimeFinder _finder;
		int _actualResult;

		protected override void SetUp() {
			base.SetUp();

			_finder = new DateTimeFinder();
		}

		protected override void BecauseOf() {
			_actualResult = _finder.Find(_changes, _changes.Count - 1, _changes.At(_expectedResult).DateTime.Subtract(new TimeSpan(0, 0, 1)));
		}

		[Test]
		public void it_should_find_proper_change() {
			_actualResult.ShouldEqualTo(_expectedResult);
		}
	}
}
