﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	public class when_finding_sentence_boundaries : with_expectations_on_text_boundary_finder {
		protected override void SetUp() {
			base.SetUp();
			
			var list = new List<IChange> {
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "a", 0)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "b", 1)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "c", 2)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, ".", 3)),
				new DeleteText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, ".", 3)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "1", 3)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "2", 4)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, " ", 5)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "3", 6)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "4", 7)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, Naracea.Common.Constants.NewLine, 8)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "x", 10)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "y", 11)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, Naracea.Common.Constants.NewLine, 12)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "q", 14)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "w. uiop", 15)),
				new BackspaceText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "op", 18)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "r", 19)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "t", 20)),
			};

			_finder = new SentenceFinder();

			_changesForward = new FakeChangeCollection(list);
			_expectedResultsForward = new List<int> {
				4, 10, 13, 15, 18
			};

			_changesBackward = new FakeChangeCollection(list);
			_expectedResultsBackward = new List<int> {
				15, 13, 10, 3, -1
			};
		}
	}
}
