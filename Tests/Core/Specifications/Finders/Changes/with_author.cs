﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	public abstract class with_author : Context {
		protected IAuthor _author;

		protected override void SetUp() {
			_author = MockRepository.GenerateStub<IAuthor>();
		}
	}
}
