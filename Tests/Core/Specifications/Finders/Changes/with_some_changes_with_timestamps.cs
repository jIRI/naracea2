﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	public abstract class with_some_changes_with_timestamps : Context {
		protected IAuthor _author;
		protected int _expectedResult;
		protected IChangesCollection _changes;


		protected override void SetUp() {
			_author = MockRepository.GenerateStub<IAuthor>();
			
			var list = new List<IChange> {
				new InsertText(new WithTextChangeArgs(_author, new DateTime(2010, 6, 13, 1, 14, 10), "a", 0)),
				new InsertText(new WithTextChangeArgs(_author, new DateTime(2010, 6, 13, 1, 14, 12), "b", 1)),
				new InsertText(new WithTextChangeArgs(_author, new DateTime(2010, 6, 13, 1, 14, 14), "c", 2)),
				new InsertText(new WithTextChangeArgs(_author, new DateTime(2010, 6, 13, 1, 14, 16), "d", 3)),
				new InsertText(new WithTextChangeArgs(_author, new DateTime(2010, 6, 13, 1, 14, 18), "e", 4)),
			};

			_changes = new FakeChangeCollection(list);
			_expectedResult = 2;
		}
	}
}
