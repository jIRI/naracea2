﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	[TestFixture]
	public class when_finding_undoable_change_in_undo_changes : with_author {
		IUndoRedoFinder _finder;
		IChange _expectedResult;
		IChange _actualResult;
		IChangesCollection _changes;

		protected override void SetUp() {
			base.SetUp();
			var list = new List<IChange> {
				new FakeChange(),
				new FakeChange(),
				new FakeChange(),
			};
			list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[2])));
			list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list[1])));

			_changes = new FakeChangeCollection(list);
			_finder = new UndoRedoFinder();
			_expectedResult = list[0];
		}

		protected override void BecauseOf() {
			_actualResult = _finder.FindNextUndoableChange(_changes);
		}

		[Test]
		public void it_should_find_nearest_change() {
			_actualResult.ShouldBeSameAs(_expectedResult);
		}
	}
}
