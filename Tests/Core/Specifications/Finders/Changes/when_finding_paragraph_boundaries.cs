﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	public class when_finding_paragraph_boundaries : with_expectations_on_text_boundary_finder {
		protected override void SetUp() {
			base.SetUp();
			
			var list = new List<IChange> {
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "a", 0)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "b", 1)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "c", 2)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, ".", 3)),
				new DeleteText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, ".", 3)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "1", 4)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "2", 5)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, " ", 6)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "3", 7)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "4", 8)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, Naracea.Common.Constants.NewLine, 9)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "x", 11)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "y", 12)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, Naracea.Common.Constants.NewLine, 13)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "q", 15)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "w. uiop", 16)),
				new BackspaceText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "op", 19)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "r", 20)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "t", 21)),
			};

			_finder = new ParagraphFinder();

			_changesForward = new FakeChangeCollection(list);
			_expectedResultsForward = new List<int> {
				10, 13, 18
			};

			_changesBackward = new FakeChangeCollection(list);
			_expectedResultsBackward = new List<int> {
				12, 9, -1
			};
		}
	}
}
