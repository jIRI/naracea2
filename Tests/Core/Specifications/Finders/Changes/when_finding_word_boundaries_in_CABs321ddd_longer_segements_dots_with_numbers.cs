﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	public class when_finding_word_boundaries_in_CABs321ddd_longer_segements_dots_with_numbers : with_expectations_on_text_boundary_finder {
		protected override void SetUp() {
			base.SetUp();
			
			var list = new List<IChange> {
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "c", 0)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "ab", 0)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, " ", 0)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "3...", 4)),
				new InsertText(new WithTextChangeArgs(_author, DateTimeOffset.UtcNow, "12", 4)),
			};

			_finder = new WordFinder();

			_changesForward = new FakeChangeCollection(list);
			_expectedResultsForward = new List<int> {
				2, 3, 4
			};

			_changesBackward = new FakeChangeCollection(list);
			_expectedResultsBackward = new List<int> {
				2, -1
			};
		}
	}
}
