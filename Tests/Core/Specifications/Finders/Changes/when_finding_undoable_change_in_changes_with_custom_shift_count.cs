﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	[TestFixture]
	public class when_finding_undoable_change_in_changes_with_custom_shift_count : with_author {
		IUndoRedoFinder _finder;
		IChange _expectedResult;
		IChange _actualResult;
		IChangesCollection _changes;

		protected override void SetUp() {
			base.SetUp();
			var list = new List<IChange> {
				MockRepository.GenerateStub<IChange>(),
				new FakeChange {
					ShiftCount = 4,
					ShouldShifterExecuteFollowingChange = true
				},
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				new FakeChange {
					ShiftCount = 4,
					ShouldShifterExecuteFollowingChange = true
				},
			};
			list.Add(new Undo(new WithChangeChangeArgs(_author, DateTimeOffset.UtcNow, list.Last())));
			_changes = new FakeChangeCollection(list);
			_finder = new UndoRedoFinder();
			_expectedResult = list[0];
		}

		protected override void BecauseOf() {
			_actualResult = _finder.FindNextUndoableChange(_changes);
		}

		[Test]
		public void it_should_skip_the_group_and_find_the_change() {
			_actualResult.ShouldEqualTo(_expectedResult);
		}
	}
}
