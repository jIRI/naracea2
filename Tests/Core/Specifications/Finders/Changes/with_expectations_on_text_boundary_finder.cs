﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.BddAsserts;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	[TestFixture]
	public abstract class with_expectations_on_text_boundary_finder : Context {
		protected IAuthor _author;
		protected List<int> _expectedResultsForward;
		protected List<int> _expectedResultsBackward;
		protected List<int> _actualResultsForward;
		protected List<int> _actualResultsBackward;
		protected IChangesCollection _changesForward;
		protected IChangesCollection _changesBackward;
		protected ITextBoundaryFinder _finder;

		protected override void SetUp() {
			_author = MockRepository.GenerateStub<IAuthor>();
			_actualResultsForward = new List<int>();
			_actualResultsBackward = new List<int>();
		}

		protected override void BecauseOf() {
			//do nothing, it is better to implement this test classical way
		}

		[Test]
		public void it_should_find_all_boundaries_when_moving_forward() {
			int changeIndexForward = 0;
			//find forward
			while( changeIndexForward < _changesForward.Count && changeIndexForward != -1 ) {
				changeIndexForward = _finder.FindNext(_changesForward, changeIndexForward);
				if( changeIndexForward != -1 ) {
					_actualResultsForward.Add(changeIndexForward);
				}
			}

			_actualResultsForward.ShouldBeEqualTo(_expectedResultsForward);
		}

		[Test]
		public void it_should_find_all_boundaries_when_moving_backward() {
			int changeIndexBackward = _changesBackward.Count - 1;
			//find backward
			while( changeIndexBackward >= 0 && changeIndexBackward != -1 ) {
				changeIndexBackward = _finder.FindPrevious(_changesBackward, changeIndexBackward);
				_actualResultsBackward.Add(changeIndexBackward);
			}

			_actualResultsBackward.ShouldBeEqualTo(_expectedResultsBackward);
		}
	}
}
