﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Finders.Changes;
using Naracea.Core.Model;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.Fakes;

namespace Naracea.Core.Tests.Specifications.Finders.Changes {
	[TestFixture]
	public class when_finding_redoable_change_in_text_changes : with_author {
		IUndoRedoFinder _finder;
		IChange _actualResult;
		IChangesCollection _changes;

		protected override void SetUp() {
			base.SetUp();
			var list = new List<IChange> {
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
				MockRepository.GenerateStub<IChange>(),
			};
			_changes = new FakeChangeCollection(list);
			_finder = new UndoRedoFinder();
		}

		protected override void BecauseOf() {
			_actualResult = _finder.FindNextRedoableChange(_changes);
		}

		[Test]
		public void it_should_return_null() {
			_actualResult.ShouldBeNull();
		}
	}
}
