﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.plain_text {
	[TestFixture]
	public class when_finding_substring_when_matching_case : with_plain_text_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "This is where I live, this is my neighbourhood.";
			_pattern = "this";
			_options = new TextSearchOptions {
				MatchCase = true,
				MatchWholeWords = false
			};
			_expectedResults = new List<TextSearchResult> {
				new TextSearchResult(22, "this"),
			};
		}
	}
}
