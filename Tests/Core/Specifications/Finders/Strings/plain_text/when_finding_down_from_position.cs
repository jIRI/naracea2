﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.plain_text {
	[TestFixture]
	public class when_finding_down_from_position : with_plain_text_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "We will skip 1st match, then accept 2nd match and also 3d match.";
			_pattern = "match";
			_options = new TextSearchOptions {
				MatchCase = false,
				MatchWholeWords = true,
				Direction = SearchDirection.DownFromPosition,
				StartPosition = 22
			};
			_expectedResults = new List<TextSearchResult> {
				new TextSearchResult(40, "match"),
				new TextSearchResult(58, "match"),
			};
		}
	}
}
