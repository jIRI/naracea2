﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.TextSurroundings {
	public class when_finding_surroundings_on_the_start_of_the_text : with_text_surroundings_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "This is text. This is another text.";
			_substring = "This";
			_spanHint = 3;
			_substringPosition = 0;
			_expectedResult = new TextSurroundingsFindResult("This is text. This", 0, _substring.Length);
		}
	}
}
