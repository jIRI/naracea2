﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.TextSurroundings {
	public class when_finding_surroundings_and_reaching_sentence_boundary : with_text_surroundings_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "Here it starts. Here is the middle. Here it ends";
			_substring = "is the";
			_spanHint = 3;
			_substringPosition = 21;
			_expectedResult = new TextSurroundingsFindResult("it starts. Here is the middle. Here it", 16, 16 + _substring.Length);
		}
	}
}
