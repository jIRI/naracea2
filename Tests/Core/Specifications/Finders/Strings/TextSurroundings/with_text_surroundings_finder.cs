﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.TextSurroundings {
	[TestFixture]
	public abstract class with_text_surroundings_finder : Context {
		protected ITextSurroundingsFinder _finder;
		protected TextSurroundingsFindResult _expectedResult;
		protected TextSurroundingsFindResult _actualResult;
		protected string _text;
		protected string _substring;
		protected int _substringPosition;
		protected int _spanHint;

		protected override void SetUp() {
			_finder = new TextSurroundingsFinder();
		}

		protected override void BecauseOf() {
			_actualResult = _finder.Find(_text, _substring, _substringPosition, _spanHint);
		}

		[Test]
		public void it_should_find_properly_surrounded_text() {
			_actualResult.ShouldEqualTo(_expectedResult);
		}
	}
}
