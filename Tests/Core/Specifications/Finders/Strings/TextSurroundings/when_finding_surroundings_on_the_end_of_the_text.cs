﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.TextSurroundings {
	public class when_finding_surroundings_on_the_end_of_the_text : with_text_surroundings_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "Prefix text. This is text.";
			_substring = "text";
			_spanHint = 3;
			_substringPosition = 21;
			_expectedResult = new TextSurroundingsFindResult("text. This is text.", 14, 14 + _substring.Length);
		}
	}
}
