﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.TextSurroundings {
	public class when_finding_surroundings_and_having_enough_text : with_text_surroundings_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "Here it starts. Here is long enough text in the middle and surrounded by other text. Here it ends";
			_substring = "text";
			_spanHint = 3;
			_substringPosition = 37;
			_expectedResult = new TextSurroundingsFindResult("is long enough text in the middle", 16, 16 + _substring.Length);
		}
	}
}
