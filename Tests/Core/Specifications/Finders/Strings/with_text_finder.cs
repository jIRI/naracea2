﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings {
	public abstract class with_text_finder : Context {
		protected IEnumerable<TextSearchResult> _expectedResults;
		protected IEnumerable<TextSearchResult> _actualResults;
		protected ITextFinder _finder;
		protected string _text;
		protected string _pattern;
		protected TextSearchOptions _options;

		protected override void SetUp() {
		}

		protected override void BecauseOf() {
			_actualResults = _finder.Find(_text, _pattern, _options);
		}

		[Test]
		public void it_should_find_all_instances_of_the_substring() {
			var actualResultsEnumerator = _actualResults.GetEnumerator();
			foreach( var expectedResult in _expectedResults ) {
				actualResultsEnumerator.MoveNext();
				actualResultsEnumerator.Current.ShouldEqualTo(expectedResult);
			}
		}
	}
}
