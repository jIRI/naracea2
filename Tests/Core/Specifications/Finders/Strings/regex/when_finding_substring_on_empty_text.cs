﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.regex {
	[TestFixture]
	public class when_finding_substring_on_empty_text : with_regex_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "";
			_pattern = "match";
			_options = new TextSearchOptions {
				MatchCase = false,
				MatchWholeWords = false
			};
			_expectedResults = new List<TextSearchResult> {
			};
		}
	}
}
