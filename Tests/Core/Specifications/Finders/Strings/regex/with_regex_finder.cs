﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.regex {
	public abstract class with_regex_finder : with_text_finder {
		protected override void SetUp() {
			base.SetUp();		
			_finder = new RegExTextFinder();
		}
	}
}
