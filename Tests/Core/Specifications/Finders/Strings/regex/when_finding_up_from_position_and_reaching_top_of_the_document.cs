﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests;
using Rhino.Mocks;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Autofac;
using Autofac.Builder;
using Naracea.Core.Finders.Changes;
using Naracea.Common.Tests.Fakes;
using Naracea.Core.Finders.Text;

namespace Naracea.Core.Tests.Specifications.Finders.Strings.regex {
	[TestFixture]
	public class when_finding_up_from_position_and_reaching_top_of_the_document : with_regex_finder {
		protected override void SetUp() {
			base.SetUp();
			_text = "012340123401234";
			_pattern = "4";
			_options = new TextSearchOptions {
				MatchCase = false,
				MatchWholeWords = false,
				Direction = SearchDirection.UpFromPosition,
				StartPosition = 2
			};
			_expectedResults = new List<TextSearchResult> {
				new TextSearchResult(14, "4"),
				new TextSearchResult(9, "4"),
			};
		}
	}
}
