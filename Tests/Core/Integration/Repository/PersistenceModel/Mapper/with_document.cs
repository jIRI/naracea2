﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Common.Tests;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Core.Model;
using Rhino.Mocks;
using Autofac;
using Naracea.Core.Model.Changes;
using Naracea.Common.Extensions;

namespace Naracea.Core.Tests.Integration.Repository.PersistenceModel.Mapper {
	public abstract class with_document : Context {
		protected IDocument _expectedDocument;
		protected IFileMetadata _expectedMetadata;
		protected IContainer _coreIoC;

		protected override void SetUp() {
			_coreIoC = new Naracea.Core.Configuration.Configurator().Container;
			_expectedMetadata = new FileMetadata(() => new MetadataKeyValuePair());
			_expectedDocument = new Document();
			this.AddAuthors();
			this.AddBranches();
		}

		void AddBranches() {
			var author = _expectedDocument.Authors[0];
			for( int i = 0; i < 5; i++ ) {
				var branch = new Branch(
					new BranchArgs(
						author,
						i > 0 ? _expectedDocument.Branches[i - 1] : null,
						i > 0 ? _expectedDocument.Branches[i - 1].Changes.Count - 30 : 0,
						DateTimeOffset.UtcNow
				));
				branch.Name = "Branch name " + i;
				branch.Comment = "Branch comment " + i;

				int currentPos = 0;
				int counter = 0;
				for( int j = 0; j < 100; j++ ) {
					IChange change;
					var text = "T" + i + ":" + j;
					switch( counter++ ) {
						// we let couple of matches to pass to default, so there is enough text to delete it :-)
						case 2:
							change = new DeleteText(
								new WithTextChangeArgs(author, DateTimeOffset.UtcNow, text, currentPos)
							);
							currentPos -= text.Length;
							break;
						case 5:
							change = new BackspaceText(
								new WithTextChangeArgs(author, DateTimeOffset.UtcNow, text, currentPos)
							);
							currentPos -= text.Length;
							break;

						//keep these 3 together, so undo undoes text insertion and redo redoes undo...
						case 6:
							goto default;
						case 7:
							change = new Undo(
								new WithChangeChangeArgs(author, DateTimeOffset.UtcNow, branch.Changes.OwnedChanges.ElementAt(j - 1))
							);
							currentPos -= change.As<IChangeWithChange>().Change.As<IChangeWithText>().Text.Length;
							break;
						case 8:
							change = new Redo(
								new WithChangeChangeArgs(author, DateTimeOffset.UtcNow, branch.Changes.OwnedChanges.ElementAt(j - 1))
							);
							currentPos += change.As<IChangeWithChange>()
															.Change.As<IChangeWithChange>()
															.Change.As<IChangeWithText>()
															.Text.Length;
							break;

						case 11:
							var subList = new List<IChange>();
							for( int l = 0; l < 10; l++ ) {
								change = new InsertText(
									new WithTextChangeArgs(author, DateTimeOffset.UtcNow, text, currentPos)
								);
								branch.Changes.Add(change);
								subList.Add(change);
								j++;
							}
							change = new MultiUndo(
								new WithChangeCollectionChangeArgs(author, DateTimeOffset.UtcNow, subList)
							);
							break;
						case 12:
							change = new GroupBegin(
								new ChangeArgs(author, DateTimeOffset.UtcNow)
							);
							(change as GroupBegin).SetShiftCount(10);
							break;
						case 13:
							var groupList = new List<IChange>();
							for( int l = 0; l < 10; l++ ) {
								change = new InsertText(
									new WithTextChangeArgs(author, DateTimeOffset.UtcNow, text, currentPos)
								);
								branch.Changes.Add(change);
								groupList.Add(change);
								j++;
							}
							change = new GroupEnd(
								new WithChangeCollectionChangeArgs(author, DateTimeOffset.UtcNow, groupList)
							);
							break;
						case 15:
							counter = 0;
							goto default;
						default:
							change = new InsertText(
								new WithTextChangeArgs(author, DateTimeOffset.UtcNow, text, currentPos)
							);
							currentPos += text.Length;
							break;
					}
					branch.Changes.Add(change);
				}

				branch.CurrentChangeIndex = branch.Changes.Count - branch.Changes.OwnedChanges.Count() / 2;
				_expectedDocument.Branches.Add(branch);
			}
		}

		void AddAuthors() {
			var author = new Author();
			author.Name = "Author name";
			author.Username = "Author username";
			_expectedDocument.Authors.Add(author);
		}
	}
}
