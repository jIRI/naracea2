﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Common.Tests.BddAsserts;
using Rhino.Mocks;
using Naracea.Core.Repository.PersistenceModel;
using Naracea.Common.Tests;
using Naracea.Core.Model;
using Naracea.Core.Repository.PersistenceModel.V1.Dto;
using Naracea.Core.Repository.PersistenceModel.V1;
using Autofac;

namespace Naracea.Core.Tests.Integration.Repository.PersistenceModel.Mapper {
	[TestFixture]
	public class when_mapping_document_to_dtoV1_and_back : with_document {
		IDocument _actualDocument;
		IDocumentMapper<IDocumentDtoV1> _mapper;
		IFileMetadata _actualMetadata;

		protected override void SetUp() {
			base.SetUp();
			_mapper = _coreIoC.Resolve<IDocumentMapper<IDocumentDtoV1>>();
		}

		protected override void BecauseOf() {
			var dto = _mapper.DataFromDomain(_expectedDocument, out _actualMetadata);
			_actualDocument = _mapper.DomainFromData(dto, _actualMetadata);
		}

		[Test]
		public void it_should_return_resulting_document_equal_to_source_document() {
			_actualDocument.ShouldEqualTo(_expectedDocument);
		}

		[Test]
		public void it_should_return_same_author_list() {
			_actualDocument.Authors.ShouldEqualTo(_expectedDocument.Authors);
		}

		[Test]
		public void it_should_return_same_branch_list() {
			_actualDocument.Branches.ShouldEqualTo(_expectedDocument.Branches);
		}

		[Test]
		public void it_should_return_metadata() {
			_actualMetadata.ShouldNotBeNull();
		}

		[Test]
		public void it_should_return_metadata_same_as_expected_metadata() {
			_actualMetadata.Data.ShouldBeEquivalentTo(_expectedMetadata.Data);
		}
	}
}
