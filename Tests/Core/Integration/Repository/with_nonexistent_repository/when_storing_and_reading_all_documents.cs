﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;
using Naracea.Core.Repository;

namespace Naracea.Core.Tests.Integration.Repository.with_nonexistent_repository {
	[TestFixture]
	public class when_storing_and_reading_all_documents : with_nonexistent_repository {
		IDocument _expectedDocument;
		IDocument _actualDocument;

		protected override void SetUp() {
			base.SetUp();
			_expectedDocument = new Document();
			_expectedDocument.Name = "My name is Legion.";
			_expectedDocument.Comment = "That's my least vulnerable spot.";
		}

		protected override void BecauseOf() {
			_repository.Save(_expectedDocument);
			_actualDocument = _repository.Load();
		}

		[Test]
		public void it_should_return_document() {
			_actualDocument.ShouldNotBeNull();
		}

		[Test]
		public void it_should_return_document_equal_to_stored_one() {
			_actualDocument.ShouldEqualTo(_expectedDocument);
		}
	}
}
