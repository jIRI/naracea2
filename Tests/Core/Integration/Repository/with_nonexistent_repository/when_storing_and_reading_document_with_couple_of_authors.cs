﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Common.Tests.BddAsserts;

namespace Naracea.Core.Tests.Integration.Repository.with_nonexistent_repository {
	[TestFixture]
	public class when_storing_and_reading_document_with_couple_of_authors : with_nonexistent_repository {
		IDocument _expectedDocument;
		IDocument _actualDocument;

		protected override void SetUp() {
			base.SetUp();
			_expectedDocument = new Document();
			_expectedDocument.Name = "My name is Legion.";
			_expectedDocument.Comment = "That's my least vulnerable spot.";
			for( int i = 0; i < 10; i++ ) {
				_expectedDocument.Authors.Add(new Author {
					Name = "author " + i,
					Username = "Joe Doe " + i
				});
				_expectedDocument.Branches.Add(new Branch(
					new BranchArgs(
						_expectedDocument.Authors[i],
						i == 0 ? null : _expectedDocument.Branches[i - 1],
						0, DateTimeOffset.UtcNow
					)
				) {
					Comment = "Comment " + i,
					Name = "Branch " + i
				});
			}
		}

		protected override void BecauseOf() {
			_repository.Save(_expectedDocument);
			_actualDocument = _repository.Load();
		}

		[Test]
		public void it_should_return_document_which_is_equal_to_stored_one() {
			_actualDocument.ShouldEqualTo(_expectedDocument);
		}
	}
}
