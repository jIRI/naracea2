﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Configuration;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using NUnit.Framework;
using Naracea.Common.Tests;
using Autofac;

namespace Naracea.Core.Tests.Integration.Repository.with_nonexistent_repository {
	public abstract class with_nonexistent_repository : Context {
		protected static readonly string _documentPath = "document.db";
		protected IRepository<IDocument> _repository;
		protected IRepositoryContext _repoContext;

		protected override void SetUp() {
			var config = new Configurator();
			_container = config.Container;
			_repoContext = _container.Resolve<IRepositoryContext>();
			_repoContext.Path = _documentPath;
			_repository = _container.Resolve<IRepository<IDocument>>(new TypedParameter(typeof(IRepositoryContext), _repoContext));
		}

		protected override void CustomTearDown() {
			if( _repository != null ) {
				_repository.Dispose();
			}
			RemoveStorage();
		}

		private static void RemoveStorage() {
			if( System.IO.File.Exists(_documentPath) ) {
				System.IO.File.Delete(_documentPath);
				Assert.That(System.IO.File.Exists(_documentPath), Is.False);
			}
		}
	}
}
