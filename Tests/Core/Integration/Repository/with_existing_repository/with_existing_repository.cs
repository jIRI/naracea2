﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Naracea.Core.Configuration;
using Naracea.Core.Repository;
using Naracea.Core.Model;
using NUnit.Framework;
using Naracea.Common.Tests;
using Autofac;
using Naracea.Core.Model.Changes;

namespace Naracea.Core.Tests.Integration.Repository.with_existing_repository {
	public abstract class with_existing_repository : Context {
		protected static readonly string _documentPath = "document.ncd";
		protected IRepository<IDocument> _repository;
		protected IRepositoryContext _repoContext;

		protected override void SetUp() {
			var config = new Configurator();
			_container = config.Container;
			_repoContext = _container.Resolve<IRepositoryContext>();
			_repoContext.Path = _documentPath;
			this.CreateRepository();

			var doc = new Document();
			doc.Name = "Docusoap";
			doc.Authors.Add(new Author {
				Name = "author",
				Username = "Joe Doe"
			});
			doc.Branches.Add(new Branch( new BranchArgs(doc.Authors[0], null, 0, DateTimeOffset.UtcNow)) {
				Name = "Branch"
			});
			for( int j = 0; j < 200; j++ ) {
				doc.Branches[0].Changes.Add(
					new InsertText(
						new  WithTextChangeArgs( 
							doc.Authors[0], DateTimeOffset.UtcNow,"text A " + j, j
				)));
			}
			_repository.Save(doc);
			_repository.Dispose();
			this.CreateRepository();
		}

		protected void CreateRepository() {
			if( _repository != null ) {
				_repository.Dispose();
			}
			_repository = _container.Resolve<IRepository<IDocument>>(new TypedParameter(typeof(IRepositoryContext), _repoContext));
		}

		protected override void CustomTearDown() {
			if( _repository != null ) {
				_repository.Dispose();
				_repository = null;
			}
			RemoveStorage();
		}

		private static void RemoveStorage() {
			if( System.IO.File.Exists(_documentPath) ) {
				System.IO.File.Delete(_documentPath);
				Assert.That(System.IO.File.Exists(_documentPath), Is.False);
			}
		}
	}
}
