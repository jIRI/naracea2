﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Naracea.Core.Model;
using Naracea.Core.Model.Changes;
using Naracea.Common.Tests.BddAsserts;
using Autofac;
using Naracea.Core.Repository;

namespace Naracea.Core.Tests.Integration.Repository.with_existing_repository {
	[TestFixture]
	public class when_adding_some_more_changes : with_existing_repository {
		IDocument _expectedDocument;
		IDocument _actualDocument;

		protected override void SetUp() {
			base.SetUp();
			_expectedDocument = _repository.Load();
			var branch = _expectedDocument.Branches[0];
			branch.ResetState();
			branch.CurrentChangeIndex = branch.Changes.Count - 1;
			for( int j = 0; j < 200; j++ ) {
				branch.Changes.Add(
					new InsertText(
						new WithTextChangeArgs(
							_expectedDocument.Authors[0],
							DateTimeOffset.UtcNow,
							j.ToString(),
							j
				)));
			}
			this.CreateRepository();
			_repository.Save(_expectedDocument);
			this.CreateRepository();
		}

		protected override void BecauseOf() {
			_actualDocument = _repository.Load();
		}

		[Test]
		public void it_should_return_document_which_is_equal_to_stored_one() {
			_actualDocument.ShouldEqualTo(_expectedDocument);
		}
	}
}
